﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mCBFEB52E648A5B44C67BF45A9477100DE7C549F8 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_mF3D5598C7168D0599B09FA11CCB6826470FFF86E (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m4E6F5D0EDF01F454FA95F67ABA1E1572D08ED5DC (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_mD05457C07F76C71E92D933B236410CB953F4CF8C (void);
// 0x00000005 System.Void System.Runtime.CompilerServices.NullablePublicOnlyAttribute::.ctor(System.Boolean)
extern void NullablePublicOnlyAttribute__ctor_mA40CF3164668BCCB7E13E2314543F0848C43A927 (void);
// 0x00000006 System.Char System.HexConverter::ToCharUpper(System.Int32)
extern void HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0 (void);
// 0x00000007 System.Void System.Diagnostics.CodeAnalysis.NotNullAttribute::.ctor()
extern void NotNullAttribute__ctor_mBC655B204641AE9C42B930C6D7E18B1C66EE3821 (void);
// 0x00000008 System.UInt32 System.Text.UnicodeUtility::GetScalarFromUtf16SurrogatePair(System.UInt32,System.UInt32)
extern void UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6866E0E09B45BF1CDE2BB7865AF20F6460C752D0 (void);
// 0x00000009 System.Int32 System.Text.UnicodeUtility::GetUtf16SequenceLength(System.UInt32)
extern void UnicodeUtility_GetUtf16SequenceLength_mBAB104741499D1D4454BF674A1A4B380E3DA9A9E (void);
// 0x0000000A System.Boolean System.Text.UnicodeUtility::IsAsciiCodePoint(System.UInt32)
extern void UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D (void);
// 0x0000000B System.Boolean System.Text.UnicodeUtility::IsHighSurrogateCodePoint(System.UInt32)
extern void UnicodeUtility_IsHighSurrogateCodePoint_mC6FC7E1678F3638CDDE48C9DE27C7ECE51D16654 (void);
// 0x0000000C System.Boolean System.Text.UnicodeUtility::IsInRangeInclusive(System.UInt32,System.UInt32,System.UInt32)
extern void UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C (void);
// 0x0000000D System.Boolean System.Text.UnicodeUtility::IsLowSurrogateCodePoint(System.UInt32)
extern void UnicodeUtility_IsLowSurrogateCodePoint_mFC9196588FB51D463621246A555CDF8EBD7C4F89 (void);
// 0x0000000E System.Boolean System.Text.UnicodeUtility::IsSurrogateCodePoint(System.UInt32)
extern void UnicodeUtility_IsSurrogateCodePoint_m1C61245755AFFC5F88AEFDE2511BD42EC2E35471 (void);
// 0x0000000F System.UInt32[] System.Text.Unicode.UnicodeHelpers::CreateDefinedCharacterBitmapMachineEndian()
extern void UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m5276C6DA38121F0033E91B53041F9CBFAC7FD9CA (void);
// 0x00000010 System.Buffers.OperationStatus System.Text.Unicode.UnicodeHelpers::DecodeScalarValueFromUtf8(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
extern void UnicodeHelpers_DecodeScalarValueFromUtf8_m12AC2FF4FA54E2207D7005136C5EF4ECAC281576 (void);
// 0x00000011 System.ReadOnlySpan`1<System.UInt32> System.Text.Unicode.UnicodeHelpers::GetDefinedCharacterBitmap()
extern void UnicodeHelpers_GetDefinedCharacterBitmap_mC09168E637ACEFF8DC6986A529B31C0352C8D60B (void);
// 0x00000012 System.Void System.Text.Unicode.UnicodeHelpers::GetUtf16SurrogatePairFromAstralScalarValue(System.Int32,System.Char&,System.Char&)
extern void UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC7309772F2EDE8846DFBFFC499EFBBA3A40E4586 (void);
// 0x00000013 System.Boolean System.Text.Unicode.UnicodeHelpers::IsSupplementaryCodePoint(System.Int32)
extern void UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456 (void);
// 0x00000014 System.ReadOnlySpan`1<System.Byte> System.Text.Unicode.UnicodeHelpers::get_DefinedCharsBitmapSpan()
extern void UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B (void);
// 0x00000015 System.Void System.Text.Unicode.UnicodeHelpers::.cctor()
extern void UnicodeHelpers__cctor_mA13B36CB9011BAAB221BAFB70DD4F943E31BAF61 (void);
// 0x00000016 System.Void System.Text.Unicode.UnicodeRange::.ctor(System.Int32,System.Int32)
extern void UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2 (void);
// 0x00000017 System.Int32 System.Text.Unicode.UnicodeRange::get_FirstCodePoint()
extern void UnicodeRange_get_FirstCodePoint_mE2ECE0C7C54A3047DF4DBA68271FBF8067BCEB26 (void);
// 0x00000018 System.Void System.Text.Unicode.UnicodeRange::set_FirstCodePoint(System.Int32)
extern void UnicodeRange_set_FirstCodePoint_m529C6CC5756D4A2AC163B3A6D274C3651CEBA345 (void);
// 0x00000019 System.Int32 System.Text.Unicode.UnicodeRange::get_Length()
extern void UnicodeRange_get_Length_mD690963E9ABE4A2F5731E5CF27C00292C7FFC696 (void);
// 0x0000001A System.Void System.Text.Unicode.UnicodeRange::set_Length(System.Int32)
extern void UnicodeRange_set_Length_m8994FAD9DDDC18EE1F2757E5444584EA3DDF89E4 (void);
// 0x0000001B System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRange::Create(System.Char,System.Char)
extern void UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B (void);
// 0x0000001C System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::CreateRange(System.Text.Unicode.UnicodeRange&,System.Char,System.Char)
extern void UnicodeRanges_CreateRange_mDD642B176A42F1DE3EEE990E612E116293DE340C (void);
// 0x0000001D System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::get_BasicLatin()
extern void UnicodeRanges_get_BasicLatin_mA46E04A614BC261AFCE0DB4A27734D38BE369F7B (void);
// 0x0000001E System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::CreateNew()
extern void AllowedCharactersBitmap_CreateNew_m881B89C811D45EBD8281DA9FF3CA7957833A9252 (void);
// 0x0000001F System.Void System.Text.Internal.AllowedCharactersBitmap::.ctor(System.UInt32[])
extern void AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8 (void);
// 0x00000020 System.Void System.Text.Internal.AllowedCharactersBitmap::AllowCharacter(System.Char)
extern void AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3 (void);
// 0x00000021 System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidCharacter(System.Char)
extern void AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB (void);
// 0x00000022 System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidUndefinedCharacters()
extern void AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95 (void);
// 0x00000023 System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::Clone()
extern void AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B (void);
// 0x00000024 System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsCharacterAllowed(System.Char)
extern void AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393 (void);
// 0x00000025 System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsUnicodeScalarAllowed(System.Int32)
extern void AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373 (void);
// 0x00000026 System.Int32 System.Text.Internal.AllowedCharactersBitmap::FindFirstCharacterToEncode(System.Char*,System.Int32)
extern void AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6 (void);
// 0x00000027 System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.ctor()
extern void DefaultJavaScriptEncoderBasicLatin__ctor_mA96626B6D6F2C7A9FAC381D4D93D908D8F249874 (void);
// 0x00000028 System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::WillEncode(System.Int32)
extern void DefaultJavaScriptEncoderBasicLatin_WillEncode_m6C9AF873C95324CB96E3A0948E4D760BBA164DDD (void);
// 0x00000029 System.Int32 System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::FindFirstCharacterToEncode(System.Char*,System.Int32)
extern void DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_m42BB18A673C9EA96D3D1465626CB033FA8F586D7 (void);
// 0x0000002A System.Int32 System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::FindFirstCharacterToEncodeUtf8(System.ReadOnlySpan`1<System.Byte>)
extern void DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncodeUtf8_m2B4293BB09F1802C5D0746BEF198D5D4EFB94B8B (void);
// 0x0000002B System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m9FB3F0515CDD56A7B116A9582DA0277B58672770 (void);
// 0x0000002C System.ReadOnlySpan`1<System.Byte> System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::get_AllowList()
extern void DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5 (void);
// 0x0000002D System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::NeedsEscaping(System.Char)
extern void DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mBCD92F26CD88B2855FA734E26B0DBB18F0763482 (void);
// 0x0000002E System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.cctor()
extern void DefaultJavaScriptEncoderBasicLatin__cctor_m222E9373820AD2A1B7C409102319A2E108988A08 (void);
// 0x0000002F System.Void System.Text.Encodings.Web.HtmlEncoder::.ctor()
extern void HtmlEncoder__ctor_m7A58A67ABD105671C4D6B8CF1CB4A06C5BD61FE5 (void);
// 0x00000030 System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.ctor(System.Text.Encodings.Web.TextEncoderSettings)
extern void DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31 (void);
// 0x00000031 System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::ForbidHtmlCharacters(System.Text.Internal.AllowedCharactersBitmap)
extern void DefaultHtmlEncoder_ForbidHtmlCharacters_m79ED10B3D6F5D04E799E20AFAE7ABEE53537FFAB (void);
// 0x00000032 System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::WillEncode(System.Int32)
extern void DefaultHtmlEncoder_WillEncode_m9DFF3D842B2D43F4ADA5003007490809FBEA36F5 (void);
// 0x00000033 System.Int32 System.Text.Encodings.Web.DefaultHtmlEncoder::FindFirstCharacterToEncode(System.Char*,System.Int32)
extern void DefaultHtmlEncoder_FindFirstCharacterToEncode_m1EB2DC500055222C3CE781D106E76674730CAF74 (void);
// 0x00000034 System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void DefaultHtmlEncoder_TryEncodeUnicodeScalar_m68F407599404602732C186AD2A2BD84E058C2065 (void);
// 0x00000035 System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m3390D134471CC26D9E0855BE7352D2056DB8C566 (void);
// 0x00000036 System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.cctor()
extern void DefaultHtmlEncoder__cctor_m78F8CF003B0AD87F7780B6201FC6E5C5222EA321 (void);
// 0x00000037 System.Text.Encodings.Web.JavaScriptEncoder System.Text.Encodings.Web.JavaScriptEncoder::get_Default()
extern void JavaScriptEncoder_get_Default_mB84D9CC3B4DD9BD204B79DFA01B45CC66DE263EE (void);
// 0x00000038 System.Void System.Text.Encodings.Web.JavaScriptEncoder::.ctor()
extern void JavaScriptEncoder__ctor_mEC23CDF138E68E96490421C2A6C9A5C8E34595E8 (void);
// 0x00000039 System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_m128A8D90D65C5C6E0F1953E7EAE26D9636139172 (void);
// 0x0000003A System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedSingleCharacter(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA (void);
// 0x0000003B System.Boolean System.Text.Encodings.Web.TextEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
// 0x0000003C System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.Char*,System.Int32)
// 0x0000003D System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32)
// 0x0000003E System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::EncodeUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&,System.Boolean)
extern void TextEncoder_EncodeUtf8_m809B5C38F1121A085BCB1C18AEA3C6A28DFD0AED (void);
// 0x0000003F System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::Encode(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
extern void TextEncoder_Encode_m6AF81F7F7A8545803441E9F0D443D4EEB793AC83 (void);
// 0x00000040 System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.ReadOnlySpan`1<System.Char>)
extern void TextEncoder_FindFirstCharacterToEncode_m2423424CDE31B11CA2840777CD0C0422CD829F71 (void);
// 0x00000041 System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncodeUtf8(System.ReadOnlySpan`1<System.Byte>)
extern void TextEncoder_FindFirstCharacterToEncodeUtf8_m7F5E427A71A6A13427292FEA421F58AAB2EB3A35 (void);
// 0x00000042 System.Boolean System.Text.Encodings.Web.TextEncoder::TryCopyCharacters(System.Char[],System.Char*,System.Int32,System.Int32&)
extern void TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3 (void);
// 0x00000043 System.Boolean System.Text.Encodings.Web.TextEncoder::TryWriteScalarAsChar(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5 (void);
// 0x00000044 System.Byte[] System.Text.Encodings.Web.TextEncoder::GetAsciiEncoding(System.Byte)
extern void TextEncoder_GetAsciiEncoding_m6FF6E556B2432F8F8FB0E9568B6E9C398BBD0829 (void);
// 0x00000045 System.Void System.Text.Encodings.Web.TextEncoder::InitializeAsciiCache()
extern void TextEncoder_InitializeAsciiCache_m1D9E9EDE89AA59C2A406239D1C722284E25B5398 (void);
// 0x00000046 System.Boolean System.Text.Encodings.Web.TextEncoder::DoesAsciiNeedEncoding(System.UInt32)
extern void TextEncoder_DoesAsciiNeedEncoding_m0525D9CBBC3FA4494ECD7D440EE01384F64DF27F (void);
// 0x00000047 System.Void System.Text.Encodings.Web.TextEncoder::.ctor()
extern void TextEncoder__ctor_m07027CA56CA490B41348EC294735935086AAE640 (void);
// 0x00000048 System.Void System.Text.Encodings.Web.TextEncoder::.cctor()
extern void TextEncoder__cctor_mA398909671E057BBEF55604F2AC2B31C545500D4 (void);
// 0x00000049 System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::<Encode>g__EncodeCore|15_0(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
extern void TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_m97E09AEBAFA1C02707FF382DB82B6A2E30BF4931 (void);
// 0x0000004A System.Void System.Text.Encodings.Web.TextEncoderSettings::.ctor(System.Text.Unicode.UnicodeRange[])
extern void TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB (void);
// 0x0000004B System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRange(System.Text.Unicode.UnicodeRange)
extern void TextEncoderSettings_AllowRange_mA179A5E04975AF16B07145F78965DA63087B18EB (void);
// 0x0000004C System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRanges(System.Text.Unicode.UnicodeRange[])
extern void TextEncoderSettings_AllowRanges_m60F42E23315096E4143FB35868A805ECC4660409 (void);
// 0x0000004D System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.TextEncoderSettings::GetAllowedCharacters()
extern void TextEncoderSettings_GetAllowedCharacters_m2A3EB325C9034B4C0A61E78D85B8FDCAAC866727 (void);
static Il2CppMethodPointer s_methodPointers[77] = 
{
	EmbeddedAttribute__ctor_mCBFEB52E648A5B44C67BF45A9477100DE7C549F8,
	IsReadOnlyAttribute__ctor_mF3D5598C7168D0599B09FA11CCB6826470FFF86E,
	NullableAttribute__ctor_m4E6F5D0EDF01F454FA95F67ABA1E1572D08ED5DC,
	NullableContextAttribute__ctor_mD05457C07F76C71E92D933B236410CB953F4CF8C,
	NullablePublicOnlyAttribute__ctor_mA40CF3164668BCCB7E13E2314543F0848C43A927,
	HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0,
	NotNullAttribute__ctor_mBC655B204641AE9C42B930C6D7E18B1C66EE3821,
	UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6866E0E09B45BF1CDE2BB7865AF20F6460C752D0,
	UnicodeUtility_GetUtf16SequenceLength_mBAB104741499D1D4454BF674A1A4B380E3DA9A9E,
	UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D,
	UnicodeUtility_IsHighSurrogateCodePoint_mC6FC7E1678F3638CDDE48C9DE27C7ECE51D16654,
	UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C,
	UnicodeUtility_IsLowSurrogateCodePoint_mFC9196588FB51D463621246A555CDF8EBD7C4F89,
	UnicodeUtility_IsSurrogateCodePoint_m1C61245755AFFC5F88AEFDE2511BD42EC2E35471,
	UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m5276C6DA38121F0033E91B53041F9CBFAC7FD9CA,
	UnicodeHelpers_DecodeScalarValueFromUtf8_m12AC2FF4FA54E2207D7005136C5EF4ECAC281576,
	UnicodeHelpers_GetDefinedCharacterBitmap_mC09168E637ACEFF8DC6986A529B31C0352C8D60B,
	UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC7309772F2EDE8846DFBFFC499EFBBA3A40E4586,
	UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456,
	UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B,
	UnicodeHelpers__cctor_mA13B36CB9011BAAB221BAFB70DD4F943E31BAF61,
	UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2,
	UnicodeRange_get_FirstCodePoint_mE2ECE0C7C54A3047DF4DBA68271FBF8067BCEB26,
	UnicodeRange_set_FirstCodePoint_m529C6CC5756D4A2AC163B3A6D274C3651CEBA345,
	UnicodeRange_get_Length_mD690963E9ABE4A2F5731E5CF27C00292C7FFC696,
	UnicodeRange_set_Length_m8994FAD9DDDC18EE1F2757E5444584EA3DDF89E4,
	UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B,
	UnicodeRanges_CreateRange_mDD642B176A42F1DE3EEE990E612E116293DE340C,
	UnicodeRanges_get_BasicLatin_mA46E04A614BC261AFCE0DB4A27734D38BE369F7B,
	AllowedCharactersBitmap_CreateNew_m881B89C811D45EBD8281DA9FF3CA7957833A9252,
	AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8,
	AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3,
	AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB,
	AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95,
	AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B,
	AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393,
	AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373,
	AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6,
	DefaultJavaScriptEncoderBasicLatin__ctor_mA96626B6D6F2C7A9FAC381D4D93D908D8F249874,
	DefaultJavaScriptEncoderBasicLatin_WillEncode_m6C9AF873C95324CB96E3A0948E4D760BBA164DDD,
	DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_m42BB18A673C9EA96D3D1465626CB033FA8F586D7,
	DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncodeUtf8_m2B4293BB09F1802C5D0746BEF198D5D4EFB94B8B,
	DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m9FB3F0515CDD56A7B116A9582DA0277B58672770,
	DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5,
	DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mBCD92F26CD88B2855FA734E26B0DBB18F0763482,
	DefaultJavaScriptEncoderBasicLatin__cctor_m222E9373820AD2A1B7C409102319A2E108988A08,
	HtmlEncoder__ctor_m7A58A67ABD105671C4D6B8CF1CB4A06C5BD61FE5,
	DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31,
	DefaultHtmlEncoder_ForbidHtmlCharacters_m79ED10B3D6F5D04E799E20AFAE7ABEE53537FFAB,
	DefaultHtmlEncoder_WillEncode_m9DFF3D842B2D43F4ADA5003007490809FBEA36F5,
	DefaultHtmlEncoder_FindFirstCharacterToEncode_m1EB2DC500055222C3CE781D106E76674730CAF74,
	DefaultHtmlEncoder_TryEncodeUnicodeScalar_m68F407599404602732C186AD2A2BD84E058C2065,
	DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m3390D134471CC26D9E0855BE7352D2056DB8C566,
	DefaultHtmlEncoder__cctor_m78F8CF003B0AD87F7780B6201FC6E5C5222EA321,
	JavaScriptEncoder_get_Default_mB84D9CC3B4DD9BD204B79DFA01B45CC66DE263EE,
	JavaScriptEncoder__ctor_mEC23CDF138E68E96490421C2A6C9A5C8E34595E8,
	JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_m128A8D90D65C5C6E0F1953E7EAE26D9636139172,
	JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA,
	NULL,
	NULL,
	NULL,
	TextEncoder_EncodeUtf8_m809B5C38F1121A085BCB1C18AEA3C6A28DFD0AED,
	TextEncoder_Encode_m6AF81F7F7A8545803441E9F0D443D4EEB793AC83,
	TextEncoder_FindFirstCharacterToEncode_m2423424CDE31B11CA2840777CD0C0422CD829F71,
	TextEncoder_FindFirstCharacterToEncodeUtf8_m7F5E427A71A6A13427292FEA421F58AAB2EB3A35,
	TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3,
	TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5,
	TextEncoder_GetAsciiEncoding_m6FF6E556B2432F8F8FB0E9568B6E9C398BBD0829,
	TextEncoder_InitializeAsciiCache_m1D9E9EDE89AA59C2A406239D1C722284E25B5398,
	TextEncoder_DoesAsciiNeedEncoding_m0525D9CBBC3FA4494ECD7D440EE01384F64DF27F,
	TextEncoder__ctor_m07027CA56CA490B41348EC294735935086AAE640,
	TextEncoder__cctor_mA398909671E057BBEF55604F2AC2B31C545500D4,
	TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_m97E09AEBAFA1C02707FF382DB82B6A2E30BF4931,
	TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB,
	TextEncoderSettings_AllowRange_mA179A5E04975AF16B07145F78965DA63087B18EB,
	TextEncoderSettings_AllowRanges_m60F42E23315096E4143FB35868A805ECC4660409,
	TextEncoderSettings_GetAllowedCharacters_m2A3EB325C9034B4C0A61E78D85B8FDCAAC866727,
};
extern void AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8_AdjustorThunk (void);
extern void AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3_AdjustorThunk (void);
extern void AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB_AdjustorThunk (void);
extern void AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95_AdjustorThunk (void);
extern void AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B_AdjustorThunk (void);
extern void AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393_AdjustorThunk (void);
extern void AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_AdjustorThunk (void);
extern void AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x0600001F, AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8_AdjustorThunk },
	{ 0x06000020, AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3_AdjustorThunk },
	{ 0x06000021, AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB_AdjustorThunk },
	{ 0x06000022, AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95_AdjustorThunk },
	{ 0x06000023, AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B_AdjustorThunk },
	{ 0x06000024, AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393_AdjustorThunk },
	{ 0x06000025, AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_AdjustorThunk },
	{ 0x06000026, AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6_AdjustorThunk },
};
static const int32_t s_InvokerIndices[77] = 
{
	2411,
	2411,
	2038,
	2038,
	2038,
	3631,
	2411,
	3246,
	3651,
	3742,
	3742,
	3074,
	3742,
	3742,
	3862,
	2955,
	3834,
	3143,
	3742,
	3830,
	3876,
	1087,
	2341,
	1999,
	2341,
	1999,
	3289,
	3005,
	3862,
	3845,
	2015,
	1998,
	1998,
	2411,
	2313,
	1739,
	1740,
	807,
	2411,
	1740,
	807,
	1408,
	356,
	3830,
	3741,
	3876,
	2411,
	2015,
	3788,
	1740,
	807,
	356,
	2849,
	3876,
	3862,
	2411,
	2849,
	2849,
	356,
	807,
	1740,
	157,
	158,
	1409,
	1408,
	2856,
	2849,
	1576,
	2411,
	1740,
	2411,
	3876,
	158,
	2015,
	2015,
	2015,
	2313,
};
extern const CustomAttributesCacheGenerator g_System_Text_Encodings_Web_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Text_Encodings_Web_CodeGenModule;
const Il2CppCodeGenModule g_System_Text_Encodings_Web_CodeGenModule = 
{
	"System.Text.Encodings.Web.dll",
	77,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_System_Text_Encodings_Web_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
