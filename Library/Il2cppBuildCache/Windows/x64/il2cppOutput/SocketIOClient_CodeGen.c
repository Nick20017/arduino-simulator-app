﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SocketIOClient.DisconnectReason::.cctor()
extern void DisconnectReason__cctor_m8EC057917EA9F6D5856A53BF7FA38A1147FD05BA (void);
// 0x00000002 System.Void SocketIOClient.OnAnyHandler::.ctor(System.Object,System.IntPtr)
extern void OnAnyHandler__ctor_mB745ECC893CD627A99639B329A786FE396461A49 (void);
// 0x00000003 System.Void SocketIOClient.OnAnyHandler::Invoke(System.String,SocketIOClient.SocketIOResponse)
extern void OnAnyHandler_Invoke_m46706F4492B6F25C439D75E72B01194FD76F7C93 (void);
// 0x00000004 System.Void SocketIOClient.SocketIO::.ctor(System.String)
extern void SocketIO__ctor_mEC07253956FA33641E29F3767FB12622EC9CC880 (void);
// 0x00000005 System.Void SocketIOClient.SocketIO::.ctor(System.Uri)
extern void SocketIO__ctor_m04BB240327A691B496DEEDBCE2BF8FB32F95A584 (void);
// 0x00000006 System.Void SocketIOClient.SocketIO::.ctor(System.Uri,SocketIOClient.SocketIOOptions)
extern void SocketIO__ctor_mB9D49023C182956102981EE2D080641D3F22B6E3 (void);
// 0x00000007 System.Uri SocketIOClient.SocketIO::get_ServerUri()
extern void SocketIO_get_ServerUri_mF49380061B2CE9628AE1F6EF9E332DC7E39C5DE8 (void);
// 0x00000008 System.Void SocketIOClient.SocketIO::set_ServerUri(System.Uri)
extern void SocketIO_set_ServerUri_m2D8C8CEB989653C259E42EDC39C6040D4D22158C (void);
// 0x00000009 SocketIOClient.Transport.TransportRouter SocketIOClient.SocketIO::get_Router()
extern void SocketIO_get_Router_mDD45FFAA3E2375AB1453CF1FEF1EBAFF04EC2C36 (void);
// 0x0000000A System.Void SocketIOClient.SocketIO::set_Router(SocketIOClient.Transport.TransportRouter)
extern void SocketIO_set_Router_m0FAF923817200C24A0C11D52916D305FC4E83598 (void);
// 0x0000000B System.Void SocketIOClient.SocketIO::set_Id(System.String)
extern void SocketIO_set_Id_m4A76C984F9DEDCFA1581EA13521476502407A191 (void);
// 0x0000000C System.String SocketIOClient.SocketIO::get_Namespace()
extern void SocketIO_get_Namespace_mFF9357291747A0DA8E839E9A1881FA6B88AF508D (void);
// 0x0000000D System.Void SocketIOClient.SocketIO::set_Namespace(System.String)
extern void SocketIO_set_Namespace_m6B8326534BD2D5A7D8DDC39C9BEE34C1150A6AC0 (void);
// 0x0000000E System.Boolean SocketIOClient.SocketIO::get_Connected()
extern void SocketIO_get_Connected_m7C107DF1177FAC56963CD4763F19ABAA1B996822 (void);
// 0x0000000F System.Void SocketIOClient.SocketIO::set_Connected(System.Boolean)
extern void SocketIO_set_Connected_m0AFE2343FD42A779FCBC37DCE77182F584A844DC (void);
// 0x00000010 System.Int32 SocketIOClient.SocketIO::get_Attempts()
extern void SocketIO_get_Attempts_m1A54D69B54AFB5EB2FDF8767B89C0EA67D18354A (void);
// 0x00000011 System.Void SocketIOClient.SocketIO::set_Attempts(System.Int32)
extern void SocketIO_set_Attempts_mD2C1453050CA459794420DC171F3387A9C81DAD0 (void);
// 0x00000012 SocketIOClient.SocketIOOptions SocketIOClient.SocketIO::get_Options()
extern void SocketIO_get_Options_m86CB03192102E0C6879AD39377BC57B6A0221A8B (void);
// 0x00000013 SocketIOClient.JsonSerializer.IJsonSerializer SocketIOClient.SocketIO::get_JsonSerializer()
extern void SocketIO_get_JsonSerializer_m3C41A32E2B80151E68566937F30408EAA2E1CEEA (void);
// 0x00000014 System.Void SocketIOClient.SocketIO::set_JsonSerializer(SocketIOClient.JsonSerializer.IJsonSerializer)
extern void SocketIO_set_JsonSerializer_mD1D7C846218A09DA9DC4B74DAEC5B7BC340EA255 (void);
// 0x00000015 System.Net.Http.HttpClient SocketIOClient.SocketIO::get_HttpClient()
extern void SocketIO_get_HttpClient_m4C50BBCEFF741BDC01B3A2466285A22FE428E759 (void);
// 0x00000016 System.Void SocketIOClient.SocketIO::set_HttpClient(System.Net.Http.HttpClient)
extern void SocketIO_set_HttpClient_mB42A5639BCEC711DF82157F1D8B88D9E239A8490 (void);
// 0x00000017 System.Func`1<SocketIOClient.Transport.IClientWebSocket> SocketIOClient.SocketIO::get_ClientWebSocketProvider()
extern void SocketIO_get_ClientWebSocketProvider_mA66C593DEA05F6004930AF3DBB2617FE487D87F8 (void);
// 0x00000018 System.Void SocketIOClient.SocketIO::set_ClientWebSocketProvider(System.Func`1<SocketIOClient.Transport.IClientWebSocket>)
extern void SocketIO_set_ClientWebSocketProvider_m3F2082D8E14113B6EB79A3EAE98885F2DA4F9053 (void);
// 0x00000019 System.Void SocketIOClient.SocketIO::add_OnConnected(System.EventHandler)
extern void SocketIO_add_OnConnected_m8C3D5DFB18C2DCC42BFC0E31D50D27C1B942BA39 (void);
// 0x0000001A System.Void SocketIOClient.SocketIO::remove_OnConnected(System.EventHandler)
extern void SocketIO_remove_OnConnected_m60DBEBC0A3CB61B6D7E0435E29BD2A7F59925F2F (void);
// 0x0000001B System.Void SocketIOClient.SocketIO::add_OnError(System.EventHandler`1<System.String>)
extern void SocketIO_add_OnError_mE4F5A0D01D36956BF4F84F71EA89E83B5B0FE76C (void);
// 0x0000001C System.Void SocketIOClient.SocketIO::remove_OnError(System.EventHandler`1<System.String>)
extern void SocketIO_remove_OnError_mE622D17481DE041B0A2CB152F83FEC8BB1EE9A08 (void);
// 0x0000001D System.Void SocketIOClient.SocketIO::add_OnDisconnected(System.EventHandler`1<System.String>)
extern void SocketIO_add_OnDisconnected_mB2910F22013D2B8805CD34D14EDA1DD741BDB7A1 (void);
// 0x0000001E System.Void SocketIOClient.SocketIO::remove_OnDisconnected(System.EventHandler`1<System.String>)
extern void SocketIO_remove_OnDisconnected_mED2BB005D80B81241C1F5348C8309908AE13E338 (void);
// 0x0000001F System.Void SocketIOClient.SocketIO::Initialize()
extern void SocketIO_Initialize_m4699C51B1A8DF696A72A2E3B486603898A7EE3AB (void);
// 0x00000020 System.Void SocketIOClient.SocketIO::CreateRouterIfNull()
extern void SocketIO_CreateRouterIfNull_mEBFDEA3F73ED535D50A4C583BCC4F75DFBAC5880 (void);
// 0x00000021 System.Threading.Tasks.Task SocketIOClient.SocketIO::ConnectAsync()
extern void SocketIO_ConnectAsync_mF024D2306601A545290846FAD9CF9A6787FCE7FD (void);
// 0x00000022 System.Void SocketIOClient.SocketIO::PingHandler()
extern void SocketIO_PingHandler_m314FA276DC0FDE78495DD64A78CF05AAEA49350A (void);
// 0x00000023 System.Void SocketIOClient.SocketIO::PongHandler(SocketIOClient.Messages.PongMessage)
extern void SocketIO_PongHandler_m50D21AFBE77AAD2BD595FE71EF183AEBED71AC77 (void);
// 0x00000024 System.Void SocketIOClient.SocketIO::ConnectedHandler(SocketIOClient.Messages.ConnectedMessage)
extern void SocketIO_ConnectedHandler_mB6CDB660313CEEC80D6EF98E50CCFFD8892F55A5 (void);
// 0x00000025 System.Void SocketIOClient.SocketIO::DisconnectedHandler()
extern void SocketIO_DisconnectedHandler_m83DA09D06CB70C9FF4FE0A18008B60B044F64C65 (void);
// 0x00000026 System.Void SocketIOClient.SocketIO::EventMessageHandler(SocketIOClient.Messages.EventMessage)
extern void SocketIO_EventMessageHandler_m68136113717876AD5371777536624CC4F79486A5 (void);
// 0x00000027 System.Void SocketIOClient.SocketIO::AckMessageHandler(SocketIOClient.Messages.ClientAckMessage)
extern void SocketIO_AckMessageHandler_m4F8E0C5E1812B3955685FFBB1A5CCA1F7C7D91F7 (void);
// 0x00000028 System.Void SocketIOClient.SocketIO::ErrorMessageHandler(SocketIOClient.Messages.ErrorMessage)
extern void SocketIO_ErrorMessageHandler_m67528961DEE035E3CEA5F0E4790671920EFB202A (void);
// 0x00000029 System.Void SocketIOClient.SocketIO::BinaryMessageHandler(SocketIOClient.Messages.BinaryMessage)
extern void SocketIO_BinaryMessageHandler_mCC86C02F482D3939DD6499E95C611250187B1201 (void);
// 0x0000002A System.Void SocketIOClient.SocketIO::BinaryAckMessageHandler(SocketIOClient.Messages.ClientBinaryAckMessage)
extern void SocketIO_BinaryAckMessageHandler_mEB4B389E3B8080FFF138F2A0FDA160EACD5F0B30 (void);
// 0x0000002B System.Void SocketIOClient.SocketIO::OnMessageReceived(SocketIOClient.Messages.IMessage)
extern void SocketIO_OnMessageReceived_m23679B9D01930A01B9D50FFE033E0FD0FA1F6C96 (void);
// 0x0000002C System.Void SocketIOClient.SocketIO::OnTransportClosed()
extern void SocketIO_OnTransportClosed_m8D68B8DA5D49D9C3D927223798EA5482B6A9BD2F (void);
// 0x0000002D System.Void SocketIOClient.SocketIO::On(System.String,System.Action`1<SocketIOClient.SocketIOResponse>)
extern void SocketIO_On_m17F868A6A67275DDF18DDD6537ACDDFCF01F7C58 (void);
// 0x0000002E System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitAsync(System.String,System.Object[])
extern void SocketIO_EmitAsync_m671FB6EADF9481835AD0C81F90F47D49955305C8 (void);
// 0x0000002F System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitAsync(System.String,System.Threading.CancellationToken,System.Object[])
extern void SocketIO_EmitAsync_mB0F21924EB56E3822698F52CFFDEF79A97D355E1 (void);
// 0x00000030 System.Void SocketIOClient.SocketIO::InvokeDisconnect(System.String)
extern void SocketIO_InvokeDisconnect_m52F6E016A6DEB7F701A1E6D162EF4D6F38A2F43B (void);
// 0x00000031 System.Void SocketIOClient.SocketIO::Dispose()
extern void SocketIO_Dispose_m5FDD93F548F9DB6578815733A485F5AB461F2094 (void);
// 0x00000032 System.Void SocketIOClient.SocketIO/<>c::.cctor()
extern void U3CU3Ec__cctor_mF5F5C245E94519B3E1E7BFFF7058D5DDF229B09B (void);
// 0x00000033 System.Void SocketIOClient.SocketIO/<>c::.ctor()
extern void U3CU3Ec__ctor_m46F798975117BBEB817559CEB509A44382A2E805 (void);
// 0x00000034 SocketIOClient.Transport.IClientWebSocket SocketIOClient.SocketIO/<>c::<Initialize>b__79_0()
extern void U3CU3Ec_U3CInitializeU3Eb__79_0_m84DD1930A89A2274A57D67DD285835E9A2BB9976 (void);
// 0x00000035 System.Void SocketIOClient.SocketIO/<ConnectAsync>d__81::MoveNext()
extern void U3CConnectAsyncU3Ed__81_MoveNext_m03FD8CC7F89AB39CE4D3F7EE5C485304C30980CE (void);
// 0x00000036 System.Void SocketIOClient.SocketIO/<ConnectAsync>d__81::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectAsyncU3Ed__81_SetStateMachine_m1ECCE93BD645592AA838CC0DAA79961A910639F5 (void);
// 0x00000037 System.Void SocketIOClient.SocketIO/<EmitAsync>d__101::MoveNext()
extern void U3CEmitAsyncU3Ed__101_MoveNext_m61BDCFC0DCBDD98BFB903E8F98FC837AB61A6724 (void);
// 0x00000038 System.Void SocketIOClient.SocketIO/<EmitAsync>d__101::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitAsyncU3Ed__101_SetStateMachine_m82FDB3356A487BDFBB32BE08AC8B04DA1A1C1C67 (void);
// 0x00000039 System.Void SocketIOClient.SocketIO/<EmitAsync>d__102::MoveNext()
extern void U3CEmitAsyncU3Ed__102_MoveNext_m8B7ABDB6694D54F5AE0851E58272282C9D851A19 (void);
// 0x0000003A System.Void SocketIOClient.SocketIO/<EmitAsync>d__102::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitAsyncU3Ed__102_SetStateMachine_mFE7510E343DBC01A0B53EBC8EED3EF255919C940 (void);
// 0x0000003B System.Void SocketIOClient.SocketIO/<InvokeDisconnect>d__105::MoveNext()
extern void U3CInvokeDisconnectU3Ed__105_MoveNext_mE6FC3092DC881E8E13CF6A98AC1498889AD2E5D0 (void);
// 0x0000003C System.Void SocketIOClient.SocketIO/<InvokeDisconnect>d__105::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInvokeDisconnectU3Ed__105_SetStateMachine_m04BF0AC0D285F30E692F1EB9064013F6F8D34ECA (void);
// 0x0000003D System.Void SocketIOClient.SocketIOOptions::.ctor()
extern void SocketIOOptions__ctor_mE9C28F3528090B7A77131DBF40C9A627AEBCC0A6 (void);
// 0x0000003E System.String SocketIOClient.SocketIOOptions::get_Path()
extern void SocketIOOptions_get_Path_m1FBC6F3D54F28D6AEFAF8BC34CE8AB83521BFD22 (void);
// 0x0000003F System.Void SocketIOClient.SocketIOOptions::set_Path(System.String)
extern void SocketIOOptions_set_Path_mF10633E51D73A179E9C13DE98858877C01880F5A (void);
// 0x00000040 System.TimeSpan SocketIOClient.SocketIOOptions::get_ConnectionTimeout()
extern void SocketIOOptions_get_ConnectionTimeout_m42D3C8D3D6D02020137D14416430ED2A72A751F0 (void);
// 0x00000041 System.Void SocketIOClient.SocketIOOptions::set_ConnectionTimeout(System.TimeSpan)
extern void SocketIOOptions_set_ConnectionTimeout_mCBB08504FAD849BC46274DE73825CB22D5B8CD00 (void);
// 0x00000042 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> SocketIOClient.SocketIOOptions::get_Query()
extern void SocketIOOptions_get_Query_mFA11FB20F1BF0F85E248DBE76C1E32D3FB98FAA4 (void);
// 0x00000043 System.Boolean SocketIOClient.SocketIOOptions::get_Reconnection()
extern void SocketIOOptions_get_Reconnection_mD04D1F2BF70F0AD7530BB503EE5107B63B8C7E05 (void);
// 0x00000044 System.Void SocketIOClient.SocketIOOptions::set_Reconnection(System.Boolean)
extern void SocketIOOptions_set_Reconnection_mB136EA9D898F774248A5A98CB955F984FE6D7F83 (void);
// 0x00000045 System.Double SocketIOClient.SocketIOOptions::get_ReconnectionDelay()
extern void SocketIOOptions_get_ReconnectionDelay_m37AECC4F9CB91B1C1F54502B7D418E8D1947DF11 (void);
// 0x00000046 System.Void SocketIOClient.SocketIOOptions::set_ReconnectionDelay(System.Double)
extern void SocketIOOptions_set_ReconnectionDelay_m82D0995BD1EEC7BE8B4638E922DAA79AD7814A39 (void);
// 0x00000047 System.Int32 SocketIOClient.SocketIOOptions::get_ReconnectionDelayMax()
extern void SocketIOOptions_get_ReconnectionDelayMax_m72FFED7A33E94DAA89D19BA954A5731580C4B47F (void);
// 0x00000048 System.Void SocketIOClient.SocketIOOptions::set_ReconnectionDelayMax(System.Int32)
extern void SocketIOOptions_set_ReconnectionDelayMax_mE4BEC23B941B5F7E4489B81DEBF91BD320D46387 (void);
// 0x00000049 System.Int32 SocketIOClient.SocketIOOptions::get_ReconnectionAttempts()
extern void SocketIOOptions_get_ReconnectionAttempts_m475F99729FA0F7C0CE98D92E1586AF21F5C02A3E (void);
// 0x0000004A System.Void SocketIOClient.SocketIOOptions::set_ReconnectionAttempts(System.Int32)
extern void SocketIOOptions_set_ReconnectionAttempts_m57223BAF3247069041AC79EBD52170484CE29F7C (void);
// 0x0000004B System.Double SocketIOClient.SocketIOOptions::get_RandomizationFactor()
extern void SocketIOOptions_get_RandomizationFactor_m6CD85264F9BD430E8F5DDB9F7705EB5B57B52A19 (void);
// 0x0000004C System.Void SocketIOClient.SocketIOOptions::set_RandomizationFactor(System.Double)
extern void SocketIOOptions_set_RandomizationFactor_m8708E361E48E9786AAF9BEEAD9254C592972D4D5 (void);
// 0x0000004D System.Collections.Generic.Dictionary`2<System.String,System.String> SocketIOClient.SocketIOOptions::get_ExtraHeaders()
extern void SocketIOOptions_get_ExtraHeaders_mB57D5270503BC51BFF3B6B5FC32A433CAE43ACA7 (void);
// 0x0000004E SocketIOClient.Transport.TransportProtocol SocketIOClient.SocketIOOptions::get_Transport()
extern void SocketIOOptions_get_Transport_m9FA7D6F68B7B38E11B0354A29E6521C654F83B02 (void);
// 0x0000004F System.Void SocketIOClient.SocketIOOptions::set_Transport(SocketIOClient.Transport.TransportProtocol)
extern void SocketIOOptions_set_Transport_m04CEA24437BE96E7A5452F26A8A6015A66CB0884 (void);
// 0x00000050 System.Int32 SocketIOClient.SocketIOOptions::get_EIO()
extern void SocketIOOptions_get_EIO_mD417CA97149DFF2BE4A2AB3ABE8E0CAC43C79AA9 (void);
// 0x00000051 System.Void SocketIOClient.SocketIOOptions::set_EIO(System.Int32)
extern void SocketIOOptions_set_EIO_mE6719F4103690097238584098D0E24CDCF0699A1 (void);
// 0x00000052 System.Void SocketIOClient.SocketIOResponse::.ctor(System.Collections.Generic.IList`1<System.Text.Json.JsonElement>,SocketIOClient.SocketIO)
extern void SocketIOResponse__ctor_m4447C545F798B099A93F8F586AA92B74ED0FD388 (void);
// 0x00000053 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.SocketIOResponse::get_InComingBytes()
extern void SocketIOResponse_get_InComingBytes_mD5D41E0AC1EA34453CE9DF5A373CE1BB4C31971E (void);
// 0x00000054 SocketIOClient.SocketIO SocketIOClient.SocketIOResponse::get_SocketIO()
extern void SocketIOResponse_get_SocketIO_m5B71B6FA3F2A4D38B42813E966DFAF116E7FB08F (void);
// 0x00000055 System.Void SocketIOClient.SocketIOResponse::set_PacketId(System.Int32)
extern void SocketIOResponse_set_PacketId_mA5D00FA08CA5BE3CFC7AFBD2F974D84476AF9495 (void);
// 0x00000056 T SocketIOClient.SocketIOResponse::GetValue(System.Int32)
// 0x00000057 System.Text.Json.JsonElement SocketIOClient.SocketIOResponse::GetValue(System.Int32)
extern void SocketIOResponse_GetValue_m498376247E5789957CF3770088C4579549BE0385 (void);
// 0x00000058 System.String SocketIOClient.SocketIOResponse::ToString()
extern void SocketIOResponse_ToString_m190E97E1AA0570ABF0C1C02C54FF1E168AA57ACF (void);
// 0x00000059 System.Uri SocketIOClient.UriConverters.IUriConverter::GetServerUri(System.Boolean,System.Uri,System.Int32,System.String,System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
// 0x0000005A System.Uri SocketIOClient.UriConverters.UriConverter::GetServerUri(System.Boolean,System.Uri,System.Int32,System.String,System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void UriConverter_GetServerUri_mAE42810A5B16E42CAD6F28F21322693D9E0E4B22 (void);
// 0x0000005B System.Void SocketIOClient.UriConverters.UriConverter::.ctor()
extern void UriConverter__ctor_mF635AE23F7E036A7011120F6DD4061FEFDCE4337 (void);
// 0x0000005C System.Void SocketIOClient.Transport.DefaultClientWebSocket::.ctor()
extern void DefaultClientWebSocket__ctor_m79746088FDB79F75A013EAA8A52ED2269C7CC23F (void);
// 0x0000005D System.Action`1<System.Object> SocketIOClient.Transport.DefaultClientWebSocket::get_ConfigOptions()
extern void DefaultClientWebSocket_get_ConfigOptions_m0FFE3A15E07BC02B600221B1436F198C3C469AD1 (void);
// 0x0000005E System.Net.WebSockets.WebSocketState SocketIOClient.Transport.DefaultClientWebSocket::get_State()
extern void DefaultClientWebSocket_get_State_m61DBE2D0D70B8DB40164539DCF6B33B91BF22D4A (void);
// 0x0000005F System.Threading.Tasks.Task SocketIOClient.Transport.DefaultClientWebSocket::CloseAsync(System.Net.WebSockets.WebSocketCloseStatus,System.String,System.Threading.CancellationToken)
extern void DefaultClientWebSocket_CloseAsync_m9E183F55F9C327EB8835C7C5EF11775EFD2EDF81 (void);
// 0x00000060 System.Threading.Tasks.Task SocketIOClient.Transport.DefaultClientWebSocket::ConnectAsync(System.Uri,System.Threading.CancellationToken)
extern void DefaultClientWebSocket_ConnectAsync_m64C51C977FF8A51221EA2562864A63D000BBCD13 (void);
// 0x00000061 System.Threading.Tasks.Task`1<System.Net.WebSockets.WebSocketReceiveResult> SocketIOClient.Transport.DefaultClientWebSocket::ReceiveAsync(System.ArraySegment`1<System.Byte>,System.Threading.CancellationToken)
extern void DefaultClientWebSocket_ReceiveAsync_m648B2AACE8C9D93EF572D52706821146B5955069 (void);
// 0x00000062 System.Threading.Tasks.Task SocketIOClient.Transport.DefaultClientWebSocket::SendAsync(System.ArraySegment`1<System.Byte>,System.Net.WebSockets.WebSocketMessageType,System.Boolean,System.Threading.CancellationToken)
extern void DefaultClientWebSocket_SendAsync_m6A1809AC61900AB4A24437BD226FEEEFAEFD89B8 (void);
// 0x00000063 System.Void SocketIOClient.Transport.DefaultClientWebSocket::SetRequestHeader(System.String,System.String)
extern void DefaultClientWebSocket_SetRequestHeader_m247C8417212B570E9F50B3C279BE9768286EEF91 (void);
// 0x00000064 System.Void SocketIOClient.Transport.DefaultClientWebSocket::Dispose()
extern void DefaultClientWebSocket_Dispose_m1D59A850CA89D9B3871F3BFE9E4AA1D6E9E38A31 (void);
// 0x00000065 System.Void SocketIOClient.Transport.DefaultClientWebSocket/<CloseAsync>d__9::MoveNext()
extern void U3CCloseAsyncU3Ed__9_MoveNext_mE4CDE4BD7BDA53916E8CE90BE16F8A514776244E (void);
// 0x00000066 System.Void SocketIOClient.Transport.DefaultClientWebSocket/<CloseAsync>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCloseAsyncU3Ed__9_SetStateMachine_mD148CE0C89031C23EA867D2FF34BB81F6FBB8984 (void);
// 0x00000067 System.Void SocketIOClient.Transport.DefaultClientWebSocket/<ConnectAsync>d__10::MoveNext()
extern void U3CConnectAsyncU3Ed__10_MoveNext_m6A62A732246F8E9D22ED0AEABB9D789F4875B321 (void);
// 0x00000068 System.Void SocketIOClient.Transport.DefaultClientWebSocket/<ConnectAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectAsyncU3Ed__10_SetStateMachine_m437EFC3CEDEB097994C3197EFE243E90FB0F0E53 (void);
// 0x00000069 System.Void SocketIOClient.Transport.DefaultClientWebSocket/<ReceiveAsync>d__11::MoveNext()
extern void U3CReceiveAsyncU3Ed__11_MoveNext_m3566D421D11AB075C58A344D833590990D9554D1 (void);
// 0x0000006A System.Void SocketIOClient.Transport.DefaultClientWebSocket/<ReceiveAsync>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CReceiveAsyncU3Ed__11_SetStateMachine_m66A5AF498F96EFF912632912CD93F2D36043A027 (void);
// 0x0000006B System.Void SocketIOClient.Transport.DefaultClientWebSocket/<SendAsync>d__12::MoveNext()
extern void U3CSendAsyncU3Ed__12_MoveNext_mE78EE0C77C86D6575E6BE71122FBE12BE16A8864 (void);
// 0x0000006C System.Void SocketIOClient.Transport.DefaultClientWebSocket/<SendAsync>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__12_SetStateMachine_m64CD752CE62EA1350A8057CFA24ADAAF7C3C8FD5 (void);
// 0x0000006D System.Void SocketIOClient.Transport.HttpTransport::.ctor(System.Net.Http.HttpClient,System.Int32)
extern void HttpTransport__ctor_mAA745776252A2AE56C9E17A1527B9AE3B2D7BE67 (void);
// 0x0000006E System.Action`1<System.String> SocketIOClient.Transport.HttpTransport::get_OnTextReceived()
extern void HttpTransport_get_OnTextReceived_m15CF89073B7DA2790586FAE9400128815DCAC7C5 (void);
// 0x0000006F System.Void SocketIOClient.Transport.HttpTransport::set_OnTextReceived(System.Action`1<System.String>)
extern void HttpTransport_set_OnTextReceived_mAEA01E64C138127591AAAD75A2DBF1AEBE3314FD (void);
// 0x00000070 System.Action`1<System.Byte[]> SocketIOClient.Transport.HttpTransport::get_OnBinaryReceived()
extern void HttpTransport_get_OnBinaryReceived_mB99F51EE3345D6202EF7337B52FB40A2D7FF5D41 (void);
// 0x00000071 System.Void SocketIOClient.Transport.HttpTransport::set_OnBinaryReceived(System.Action`1<System.Byte[]>)
extern void HttpTransport_set_OnBinaryReceived_m965D579FDA010E4F69943F9C9EB3335AE4B2A6C4 (void);
// 0x00000072 System.String SocketIOClient.Transport.HttpTransport::AppendRandom(System.String)
extern void HttpTransport_AppendRandom_mE058E60B0302806410CCC0639852A6EA790F4AFA (void);
// 0x00000073 System.Threading.Tasks.Task SocketIOClient.Transport.HttpTransport::GetAsync(System.String,System.Threading.CancellationToken)
extern void HttpTransport_GetAsync_m0EBB5C5F7C47931030A70A6DB24EF1B269EF68F1 (void);
// 0x00000074 System.Threading.Tasks.Task SocketIOClient.Transport.HttpTransport::SendAsync(System.Net.Http.HttpRequestMessage,System.Threading.CancellationToken)
extern void HttpTransport_SendAsync_m2C7673A37302B7AEE79CD307BF0CFF3B1EA95508 (void);
// 0x00000075 System.Threading.Tasks.Task SocketIOClient.Transport.HttpTransport::PostAsync(System.String,System.String,System.Threading.CancellationToken)
extern void HttpTransport_PostAsync_mBACF830DEAC2474E755F9B9ACC46FC5D8D7C526A (void);
// 0x00000076 System.Threading.Tasks.Task SocketIOClient.Transport.HttpTransport::PostAsync(System.String,System.Collections.Generic.IEnumerable`1<System.Byte[]>,System.Threading.CancellationToken)
extern void HttpTransport_PostAsync_m3D2C65490C2C391161ED0E7D320E3CC8EBF33AA7 (void);
// 0x00000077 System.Collections.Generic.List`1<System.Int32> SocketIOClient.Transport.HttpTransport::SplitInt(System.Int32)
extern void HttpTransport_SplitInt_mC970F6589AEBA8EAD212527C756667AB029C8652 (void);
// 0x00000078 System.Threading.Tasks.Task SocketIOClient.Transport.HttpTransport::ProduceMessageAsync(System.Net.Http.HttpResponseMessage)
extern void HttpTransport_ProduceMessageAsync_m97A6135E853A5F2A3BE2A3EB439C2087A7DDD42E (void);
// 0x00000079 System.Void SocketIOClient.Transport.HttpTransport::ProduceText(System.String)
extern void HttpTransport_ProduceText_m89B8A6967ED59CCC741B1AC73654B57428F6AFF8 (void);
// 0x0000007A System.Void SocketIOClient.Transport.HttpTransport::ProduceBytes(System.Byte[])
extern void HttpTransport_ProduceBytes_mC55B05BE79F8081AEDC7475DE3720BA7287D96F1 (void);
// 0x0000007B System.Void SocketIOClient.Transport.HttpTransport/<GetAsync>d__12::MoveNext()
extern void U3CGetAsyncU3Ed__12_MoveNext_mC641C4B62670248D906EF7514054F66E7E63E4A1 (void);
// 0x0000007C System.Void SocketIOClient.Transport.HttpTransport/<GetAsync>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetAsyncU3Ed__12_SetStateMachine_mCF888DD1BB02D2EB5E362AC40CEE07D81E519E7A (void);
// 0x0000007D System.Void SocketIOClient.Transport.HttpTransport/<SendAsync>d__13::MoveNext()
extern void U3CSendAsyncU3Ed__13_MoveNext_m4E93C60052E04660F288B2C5F2EA7CD622EFCF1D (void);
// 0x0000007E System.Void SocketIOClient.Transport.HttpTransport/<SendAsync>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__13_SetStateMachine_m024A5A18084436D890DAB736AE15C8572355A482 (void);
// 0x0000007F System.Void SocketIOClient.Transport.HttpTransport/<PostAsync>d__14::MoveNext()
extern void U3CPostAsyncU3Ed__14_MoveNext_mDA369269CBD65510DD420EADE79EBFD47AA18036 (void);
// 0x00000080 System.Void SocketIOClient.Transport.HttpTransport/<PostAsync>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostAsyncU3Ed__14_SetStateMachine_m3BE227D571DCB3C1149853D5363EDFB3012D06D1 (void);
// 0x00000081 System.Void SocketIOClient.Transport.HttpTransport/<>c::.cctor()
extern void U3CU3Ec__cctor_mFFFD9900E1093D56E30D597AA3A8A73DE9597B30 (void);
// 0x00000082 System.Void SocketIOClient.Transport.HttpTransport/<>c::.ctor()
extern void U3CU3Ec__ctor_m4EE5D37498E7E12A80DCCB8C391D1DCA46182880 (void);
// 0x00000083 System.Byte SocketIOClient.Transport.HttpTransport/<>c::<PostAsync>b__15_0(System.Int32)
extern void U3CU3Ec_U3CPostAsyncU3Eb__15_0_mE2A9AFF2A4F971C4AF8A14A55B0764B454E2BECF (void);
// 0x00000084 System.Void SocketIOClient.Transport.HttpTransport/<PostAsync>d__15::MoveNext()
extern void U3CPostAsyncU3Ed__15_MoveNext_m16D7E48B7E366F62287D117CC9D998833129C438 (void);
// 0x00000085 System.Void SocketIOClient.Transport.HttpTransport/<PostAsync>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostAsyncU3Ed__15_SetStateMachine_mC519448B2884FD4C0A877B254582828AC538BCB9 (void);
// 0x00000086 System.Void SocketIOClient.Transport.HttpTransport/<ProduceMessageAsync>d__17::MoveNext()
extern void U3CProduceMessageAsyncU3Ed__17_MoveNext_m5BDBB12B463982C8D3DCAF88662843960361D635 (void);
// 0x00000087 System.Void SocketIOClient.Transport.HttpTransport/<ProduceMessageAsync>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CProduceMessageAsyncU3Ed__17_SetStateMachine_m85ADDF619E98B828A3AD77EFE309E8EE0E6DCC5A (void);
// 0x00000088 System.Net.WebSockets.WebSocketState SocketIOClient.Transport.IClientWebSocket::get_State()
// 0x00000089 System.Threading.Tasks.Task SocketIOClient.Transport.IClientWebSocket::ConnectAsync(System.Uri,System.Threading.CancellationToken)
// 0x0000008A System.Threading.Tasks.Task SocketIOClient.Transport.IClientWebSocket::CloseAsync(System.Net.WebSockets.WebSocketCloseStatus,System.String,System.Threading.CancellationToken)
// 0x0000008B System.Threading.Tasks.Task SocketIOClient.Transport.IClientWebSocket::SendAsync(System.ArraySegment`1<System.Byte>,System.Net.WebSockets.WebSocketMessageType,System.Boolean,System.Threading.CancellationToken)
// 0x0000008C System.Threading.Tasks.Task`1<System.Net.WebSockets.WebSocketReceiveResult> SocketIOClient.Transport.IClientWebSocket::ReceiveAsync(System.ArraySegment`1<System.Byte>,System.Threading.CancellationToken)
// 0x0000008D System.Void SocketIOClient.Transport.IClientWebSocket::SetRequestHeader(System.String,System.String)
// 0x0000008E System.Void SocketIOClient.Transport.TransportRouter::.ctor(System.Net.Http.HttpClient,System.Func`1<SocketIOClient.Transport.IClientWebSocket>,SocketIOClient.SocketIOOptions)
extern void TransportRouter__ctor_m96001D3476C8D4DDF9EAFF8B02E7BE8F9C5F3C04 (void);
// 0x0000008F System.Uri SocketIOClient.Transport.TransportRouter::get_ServerUri()
extern void TransportRouter_get_ServerUri_m76DD5F6D1F3CE15C77E08B5F3E14184DC5889764 (void);
// 0x00000090 System.Void SocketIOClient.Transport.TransportRouter::set_ServerUri(System.Uri)
extern void TransportRouter_set_ServerUri_mEE683013DBB0F35BCFEC5DBE625DDF099F8A878D (void);
// 0x00000091 System.String SocketIOClient.Transport.TransportRouter::get_Namespace()
extern void TransportRouter_get_Namespace_m1EFE88CA37B3F58535288CA4E2F8464310056B30 (void);
// 0x00000092 System.Void SocketIOClient.Transport.TransportRouter::set_Namespace(System.String)
extern void TransportRouter_set_Namespace_mD055A954C5FF6298D49A8143EFB7EA93AB90E4F3 (void);
// 0x00000093 SocketIOClient.Transport.TransportProtocol SocketIOClient.Transport.TransportRouter::get_Protocol()
extern void TransportRouter_get_Protocol_m8138CD8D64707ABDAB1EA4527E10622E496A3A04 (void);
// 0x00000094 System.Void SocketIOClient.Transport.TransportRouter::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void TransportRouter_set_Protocol_mD60882D5BF23AAF4C1E38A15BCD14174824517E1 (void);
// 0x00000095 System.Int32 SocketIOClient.Transport.TransportRouter::get_EIO()
extern void TransportRouter_get_EIO_mC86D14482EB4E63F3802B103188DC42F1A96B677 (void);
// 0x00000096 System.Void SocketIOClient.Transport.TransportRouter::set_EIO(System.Int32)
extern void TransportRouter_set_EIO_mE9713AC8626966E10E044F0646C307B1613CC500 (void);
// 0x00000097 SocketIOClient.UriConverters.IUriConverter SocketIOClient.Transport.TransportRouter::get_UriConverter()
extern void TransportRouter_get_UriConverter_mBB3506ECF9A1916623F14F8E3793E0A195012B5D (void);
// 0x00000098 System.Void SocketIOClient.Transport.TransportRouter::set_UriConverter(SocketIOClient.UriConverters.IUriConverter)
extern void TransportRouter_set_UriConverter_m2941F574F55685D9365E939FF0B4FFD687E973D8 (void);
// 0x00000099 System.Action`1<SocketIOClient.Messages.IMessage> SocketIOClient.Transport.TransportRouter::get_OnMessageReceived()
extern void TransportRouter_get_OnMessageReceived_mD238146DE5B5BA3A69FBEF3D45901A4CDFAFF44E (void);
// 0x0000009A System.Void SocketIOClient.Transport.TransportRouter::set_OnMessageReceived(System.Action`1<SocketIOClient.Messages.IMessage>)
extern void TransportRouter_set_OnMessageReceived_m0CAFAEC2DE52000DEE5CA7A96F73246E3CFA69F1 (void);
// 0x0000009B System.Action SocketIOClient.Transport.TransportRouter::get_OnTransportClosed()
extern void TransportRouter_get_OnTransportClosed_mB5E39DCA53D6C88CD8D4BA0E7A241B3256AB5D7D (void);
// 0x0000009C System.Void SocketIOClient.Transport.TransportRouter::set_OnTransportClosed(System.Action)
extern void TransportRouter_set_OnTransportClosed_mB4D7DE54D0F47B81D7C6E6B783A317C004EEB876 (void);
// 0x0000009D System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::ConnectAsync()
extern void TransportRouter_ConnectAsync_m2427AB13C13D5667E53D2BC80D9BF53B0FF98034 (void);
// 0x0000009E System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::ConnectByWebsocketAsync()
extern void TransportRouter_ConnectByWebsocketAsync_m5E61A3B5E272E5F83206A8D021E002D31E7C1D82 (void);
// 0x0000009F System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::ConnectByPollingAsync()
extern void TransportRouter_ConnectByPollingAsync_mE44BDC34D0F4CBFCCF74F16237B66A1292A9D650 (void);
// 0x000000A0 System.Void SocketIOClient.Transport.TransportRouter::StartPolling(System.Threading.CancellationToken)
extern void TransportRouter_StartPolling_mEFCC686E0E4E1086D1DC905E41AE2104D6CB0259 (void);
// 0x000000A1 System.Void SocketIOClient.Transport.TransportRouter::StartPing(System.Threading.CancellationToken)
extern void TransportRouter_StartPing_m57A6DF60D143878A1B7A80E7C6F8087DF19323A1 (void);
// 0x000000A2 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::OnOpened(SocketIOClient.Messages.OpenedMessage)
extern void TransportRouter_OnOpened_mF81F97FDD32B87BA06CAB553E4592F5A17361B67 (void);
// 0x000000A3 System.Void SocketIOClient.Transport.TransportRouter::OnTextReceived(System.String)
extern void TransportRouter_OnTextReceived_mCA059D8F291380A10E94F37F81FE437A8C26FF28 (void);
// 0x000000A4 System.Void SocketIOClient.Transport.TransportRouter::OnBinaryReceived(System.Byte[])
extern void TransportRouter_OnBinaryReceived_m1F0A0F7823553FC7572649A2A2F6C460D253EFA4 (void);
// 0x000000A5 System.Void SocketIOClient.Transport.TransportRouter::OnAborted(System.Exception)
extern void TransportRouter_OnAborted_m42A6C4E5972AD93974128A77AB060245C6A8D8D6 (void);
// 0x000000A6 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::SendAsync(SocketIOClient.Messages.IMessage,System.Threading.CancellationToken)
extern void TransportRouter_SendAsync_m6CCD8FE5CD3547DD657D25B0B9E90CA78B76640F (void);
// 0x000000A7 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::DisconnectAsync()
extern void TransportRouter_DisconnectAsync_m5F372E1D46A2C1283A015AC6130E80D78E783DB4 (void);
// 0x000000A8 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::SendAsync(System.String,System.Threading.CancellationToken)
extern void TransportRouter_SendAsync_m5370779C7885AB5A5842E53DB896F3F2D26241B6 (void);
// 0x000000A9 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter::SendAsync(System.Collections.Generic.IEnumerable`1<System.Byte[]>,System.Threading.CancellationToken)
extern void TransportRouter_SendAsync_mD1B98253A50DF7675ABE1A5D29419B081294B4C9 (void);
// 0x000000AA System.Void SocketIOClient.Transport.TransportRouter::Dispose()
extern void TransportRouter_Dispose_mBB4AE4F893CA0210575BBEDE9B3B6D07F8992B44 (void);
// 0x000000AB System.Void SocketIOClient.Transport.TransportRouter/<ConnectAsync>d__41::MoveNext()
extern void U3CConnectAsyncU3Ed__41_MoveNext_mB584FA2DE02307EDE154C27292CE30B07E06DEAA (void);
// 0x000000AC System.Void SocketIOClient.Transport.TransportRouter/<ConnectAsync>d__41::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectAsyncU3Ed__41_SetStateMachine_mABAF1145E76B3C806CD1DC24224EE718C0FE4526 (void);
// 0x000000AD System.Void SocketIOClient.Transport.TransportRouter/<ConnectByWebsocketAsync>d__42::MoveNext()
extern void U3CConnectByWebsocketAsyncU3Ed__42_MoveNext_mB24CC63EF53BB731A142C8488D73562313F8920C (void);
// 0x000000AE System.Void SocketIOClient.Transport.TransportRouter/<ConnectByWebsocketAsync>d__42::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectByWebsocketAsyncU3Ed__42_SetStateMachine_m3294DBB8241985C904860D8C910296D0AE1EF4BA (void);
// 0x000000AF System.Void SocketIOClient.Transport.TransportRouter/<ConnectByPollingAsync>d__43::MoveNext()
extern void U3CConnectByPollingAsyncU3Ed__43_MoveNext_m65122FB1A8D148F0F74455C893EC8303878782DD (void);
// 0x000000B0 System.Void SocketIOClient.Transport.TransportRouter/<ConnectByPollingAsync>d__43::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectByPollingAsyncU3Ed__43_SetStateMachine_m37FECED5E2C8F12E3FC6308915571A7789E48339 (void);
// 0x000000B1 System.Void SocketIOClient.Transport.TransportRouter/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m000CAD0BF1757FEAC7B4B8FCC964CF0C7AFC2C71 (void);
// 0x000000B2 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter/<>c__DisplayClass44_0::<StartPolling>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CStartPollingU3Eb__0_mF8DE4F95BFD4770AA2743B6041AB854968ECFBC8 (void);
// 0x000000B3 System.Void SocketIOClient.Transport.TransportRouter/<>c__DisplayClass44_0/<<StartPolling>b__0>d::MoveNext()
extern void U3CU3CStartPollingU3Eb__0U3Ed_MoveNext_m77A6B657D52AD87B8870737A49D46476CD153A0B (void);
// 0x000000B4 System.Void SocketIOClient.Transport.TransportRouter/<>c__DisplayClass44_0/<<StartPolling>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CStartPollingU3Eb__0U3Ed_SetStateMachine_mE5328B25B4712C524080DCAE323D9FD3C4263794 (void);
// 0x000000B5 System.Void SocketIOClient.Transport.TransportRouter/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mB6E0A029FEC217E9E6236648370AA33A6F8B2173 (void);
// 0x000000B6 System.Threading.Tasks.Task SocketIOClient.Transport.TransportRouter/<>c__DisplayClass45_0::<StartPing>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CStartPingU3Eb__0_mC8876E143A7683FADD043024F9F92910936F1D6D (void);
// 0x000000B7 System.Void SocketIOClient.Transport.TransportRouter/<>c__DisplayClass45_0/<<StartPing>b__0>d::MoveNext()
extern void U3CU3CStartPingU3Eb__0U3Ed_MoveNext_mD8B3D0A6E1B187A5019F1F9456A3E285ABC7BDD6 (void);
// 0x000000B8 System.Void SocketIOClient.Transport.TransportRouter/<>c__DisplayClass45_0/<<StartPing>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CStartPingU3Eb__0U3Ed_SetStateMachine_mE910C8499A852C5A9AE83B20C285F1B5226A63A2 (void);
// 0x000000B9 System.Void SocketIOClient.Transport.TransportRouter/<OnOpened>d__46::MoveNext()
extern void U3COnOpenedU3Ed__46_MoveNext_m437285B06D1F08C0CA03A535D157F8FD3ECCDAAB (void);
// 0x000000BA System.Void SocketIOClient.Transport.TransportRouter/<OnOpened>d__46::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnOpenedU3Ed__46_SetStateMachine_mAED54523AB8B9D9861736CCEA7A47D1EE7022413 (void);
// 0x000000BB System.Void SocketIOClient.Transport.TransportRouter/<OnTextReceived>d__47::MoveNext()
extern void U3COnTextReceivedU3Ed__47_MoveNext_mC18AD7621DFCA76DA92EF7AAA9464EAE5F0D550D (void);
// 0x000000BC System.Void SocketIOClient.Transport.TransportRouter/<OnTextReceived>d__47::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnTextReceivedU3Ed__47_SetStateMachine_m75171F6AEA5BEBA0568157A3D74819D9786CEE73 (void);
// 0x000000BD System.Void SocketIOClient.Transport.TransportRouter/<SendAsync>d__50::MoveNext()
extern void U3CSendAsyncU3Ed__50_MoveNext_mB7D9B2014E46114ED396AAFFD656146A36FE24A1 (void);
// 0x000000BE System.Void SocketIOClient.Transport.TransportRouter/<SendAsync>d__50::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__50_SetStateMachine_mECC493E4D4C5433A4B2620F29E2BF856DC2FAFFB (void);
// 0x000000BF System.Void SocketIOClient.Transport.TransportRouter/<DisconnectAsync>d__51::MoveNext()
extern void U3CDisconnectAsyncU3Ed__51_MoveNext_m904D5D3C8F3E813544A005F3031D42F2196E4491 (void);
// 0x000000C0 System.Void SocketIOClient.Transport.TransportRouter/<DisconnectAsync>d__51::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDisconnectAsyncU3Ed__51_SetStateMachine_mE795A2BF18086096E7B38E6CADA5BDBFBFC96D30 (void);
// 0x000000C1 System.Void SocketIOClient.Transport.TransportRouter/<SendAsync>d__52::MoveNext()
extern void U3CSendAsyncU3Ed__52_MoveNext_m4A032EFFD4227BD48CD62F2A2CE7DC04DD3D5648 (void);
// 0x000000C2 System.Void SocketIOClient.Transport.TransportRouter/<SendAsync>d__52::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__52_SetStateMachine_mC80CFA8D89B2E8846F2F18BBC81A9ED45370A481 (void);
// 0x000000C3 System.Void SocketIOClient.Transport.TransportRouter/<SendAsync>d__53::MoveNext()
extern void U3CSendAsyncU3Ed__53_MoveNext_m85EAF9F02BE4959E62FEDD20E41521FE19525555 (void);
// 0x000000C4 System.Void SocketIOClient.Transport.TransportRouter/<SendAsync>d__53::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__53_SetStateMachine_mC93F378EE76C870A8B0DDD34A803ADE568CBAB48 (void);
// 0x000000C5 System.Void SocketIOClient.Transport.WebSocketTransport::.ctor(SocketIOClient.Transport.IClientWebSocket,System.Int32)
extern void WebSocketTransport__ctor_mD059A8D285789D308453DD7316C62B9671665508 (void);
// 0x000000C6 System.Int32 SocketIOClient.Transport.WebSocketTransport::get_ReceiveChunkSize()
extern void WebSocketTransport_get_ReceiveChunkSize_m7423C6ABF999816047CBBE5091421C083583BB53 (void);
// 0x000000C7 System.Void SocketIOClient.Transport.WebSocketTransport::set_ReceiveChunkSize(System.Int32)
extern void WebSocketTransport_set_ReceiveChunkSize_m4039BD765C6687FA791AA26D54BAA52BD8D4B39E (void);
// 0x000000C8 System.Int32 SocketIOClient.Transport.WebSocketTransport::get_SendChunkSize()
extern void WebSocketTransport_get_SendChunkSize_mF03112851A9A4B9EC8B53FA9DCB4C01177AC138E (void);
// 0x000000C9 System.Void SocketIOClient.Transport.WebSocketTransport::set_SendChunkSize(System.Int32)
extern void WebSocketTransport_set_SendChunkSize_mDF22C5AA6380D76D2C03F8F5B1BE613F56657F7C (void);
// 0x000000CA System.TimeSpan SocketIOClient.Transport.WebSocketTransport::get_ConnectionTimeout()
extern void WebSocketTransport_get_ConnectionTimeout_m805685CAF6A21DAA5B4581E9115BF5DF7A638A6F (void);
// 0x000000CB System.Void SocketIOClient.Transport.WebSocketTransport::set_ConnectionTimeout(System.TimeSpan)
extern void WebSocketTransport_set_ConnectionTimeout_m9B7C2A72B06C3EB53EFF499EF49D757505461D99 (void);
// 0x000000CC System.Void SocketIOClient.Transport.WebSocketTransport::set_ReceiveWait(System.TimeSpan)
extern void WebSocketTransport_set_ReceiveWait_mD2A1F60C8F17900ACF6B9DC77B2398894772876A (void);
// 0x000000CD System.Action`1<System.String> SocketIOClient.Transport.WebSocketTransport::get_OnTextReceived()
extern void WebSocketTransport_get_OnTextReceived_m80A4849545A1B10B03397447C624C2B69B603859 (void);
// 0x000000CE System.Void SocketIOClient.Transport.WebSocketTransport::set_OnTextReceived(System.Action`1<System.String>)
extern void WebSocketTransport_set_OnTextReceived_mDA981BF42B62CB4AE52C3011F996031E36EFA51A (void);
// 0x000000CF System.Action`1<System.Byte[]> SocketIOClient.Transport.WebSocketTransport::get_OnBinaryReceived()
extern void WebSocketTransport_get_OnBinaryReceived_m12078E3CE2369D226DB519AB8EC7858D912A7308 (void);
// 0x000000D0 System.Void SocketIOClient.Transport.WebSocketTransport::set_OnBinaryReceived(System.Action`1<System.Byte[]>)
extern void WebSocketTransport_set_OnBinaryReceived_mD8C6635BB91FD7D6158BA90281356FF2BEA0B260 (void);
// 0x000000D1 System.Action`1<System.Exception> SocketIOClient.Transport.WebSocketTransport::get_OnAborted()
extern void WebSocketTransport_get_OnAborted_mF078CCDAAF48AE1F1E12275CF8E213756FF093FF (void);
// 0x000000D2 System.Void SocketIOClient.Transport.WebSocketTransport::set_OnAborted(System.Action`1<System.Exception>)
extern void WebSocketTransport_set_OnAborted_mBB91B48B2DF92743FCCA2255932BE9D67E55AD81 (void);
// 0x000000D3 System.Threading.Tasks.Task SocketIOClient.Transport.WebSocketTransport::ConnectAsync(System.Uri)
extern void WebSocketTransport_ConnectAsync_mDD02270F7B94ED91A84ADAE0775BA32698BE7EDF (void);
// 0x000000D4 System.Threading.Tasks.Task SocketIOClient.Transport.WebSocketTransport::SendAsync(System.Byte[],System.Threading.CancellationToken)
extern void WebSocketTransport_SendAsync_mCBCA4E1F380CA76309F844B9D7BD356B37E6B636 (void);
// 0x000000D5 System.Threading.Tasks.Task SocketIOClient.Transport.WebSocketTransport::SendAsync(System.Net.WebSockets.WebSocketMessageType,System.Byte[],System.Threading.CancellationToken)
extern void WebSocketTransport_SendAsync_mE057E9C97004675A5303352DB450032FDDBA19D6 (void);
// 0x000000D6 System.Threading.Tasks.Task SocketIOClient.Transport.WebSocketTransport::SendAsync(System.String,System.Threading.CancellationToken)
extern void WebSocketTransport_SendAsync_mECEA08E3E7437DAD3170C2BCA9C51AD5F07EF349 (void);
// 0x000000D7 System.Threading.Tasks.Task SocketIOClient.Transport.WebSocketTransport::ListenAsync()
extern void WebSocketTransport_ListenAsync_mB0C9B98540C9A632DEA0267D818FD1EEA03F6E7B (void);
// 0x000000D8 System.Void SocketIOClient.Transport.WebSocketTransport::Dispose()
extern void WebSocketTransport_Dispose_m9ABD975D34150BA3230326996DB8DC2352DCC623 (void);
// 0x000000D9 System.Void SocketIOClient.Transport.WebSocketTransport/<ConnectAsync>d__32::MoveNext()
extern void U3CConnectAsyncU3Ed__32_MoveNext_mA2D1147A27E11555D5BA80C46463376DD4D95D28 (void);
// 0x000000DA System.Void SocketIOClient.Transport.WebSocketTransport/<ConnectAsync>d__32::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectAsyncU3Ed__32_SetStateMachine_mC9095D635C19D612D42B3374031B330C1DAE74A9 (void);
// 0x000000DB System.Void SocketIOClient.Transport.WebSocketTransport/<SendAsync>d__34::MoveNext()
extern void U3CSendAsyncU3Ed__34_MoveNext_m0233519070E68092BB9317B35727B2C6E908E7C0 (void);
// 0x000000DC System.Void SocketIOClient.Transport.WebSocketTransport/<SendAsync>d__34::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__34_SetStateMachine_mFB8DA5CD9392EE2E24757CB72F3F922B3843E502 (void);
// 0x000000DD System.Void SocketIOClient.Transport.WebSocketTransport/<SendAsync>d__35::MoveNext()
extern void U3CSendAsyncU3Ed__35_MoveNext_mE7D1F3F54C7EC423D212B5A446E69D11BB4EFBA2 (void);
// 0x000000DE System.Void SocketIOClient.Transport.WebSocketTransport/<SendAsync>d__35::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__35_SetStateMachine_m03876D9EA87154470B66213C6C917DA9288593B3 (void);
// 0x000000DF System.Void SocketIOClient.Transport.WebSocketTransport/<SendAsync>d__36::MoveNext()
extern void U3CSendAsyncU3Ed__36_MoveNext_mFF1EFFB259EDA3C4ECCE0B41B72457AA284AA362 (void);
// 0x000000E0 System.Void SocketIOClient.Transport.WebSocketTransport/<SendAsync>d__36::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendAsyncU3Ed__36_SetStateMachine_m4A84217F6A869CA3E24C7909B818F4B37FA67C7E (void);
// 0x000000E1 System.Void SocketIOClient.Transport.WebSocketTransport/<ListenAsync>d__37::MoveNext()
extern void U3CListenAsyncU3Ed__37_MoveNext_m37BB23F759DA7952F83681F5902D7E1AE0BA8D69 (void);
// 0x000000E2 System.Void SocketIOClient.Transport.WebSocketTransport/<ListenAsync>d__37::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CListenAsyncU3Ed__37_SetStateMachine_mCB6D898F64A30EBAFEE9F5D18346C8C403B85460 (void);
// 0x000000E3 SocketIOClient.Messages.MessageType SocketIOClient.Messages.BinaryMessage::get_Type()
extern void BinaryMessage_get_Type_mC2B43D014799852655C16BD2E958AA3A1EA4DA4A (void);
// 0x000000E4 System.String SocketIOClient.Messages.BinaryMessage::get_Namespace()
extern void BinaryMessage_get_Namespace_mAA8F0EDECFAF3E10B0DBECF08F653C1BFDBE8C39 (void);
// 0x000000E5 System.Void SocketIOClient.Messages.BinaryMessage::set_Namespace(System.String)
extern void BinaryMessage_set_Namespace_m646059378AD2E597BEF2CD530131C6A3FB7C5367 (void);
// 0x000000E6 System.String SocketIOClient.Messages.BinaryMessage::get_Event()
extern void BinaryMessage_get_Event_mA5C2557061343D628095E49FC95B01F89B76A3A5 (void);
// 0x000000E7 System.Void SocketIOClient.Messages.BinaryMessage::set_Event(System.String)
extern void BinaryMessage_set_Event_m8029DA632FCD8CE8FA190756ED4B6C0EEA6DEF0A (void);
// 0x000000E8 System.Int32 SocketIOClient.Messages.BinaryMessage::get_Id()
extern void BinaryMessage_get_Id_m97035E0F4A2949EA52F3F073D456BF9488D29E33 (void);
// 0x000000E9 System.Void SocketIOClient.Messages.BinaryMessage::set_Id(System.Int32)
extern void BinaryMessage_set_Id_m22A1C8842F872CAC13EC07D4D7BE5D5F03A11555 (void);
// 0x000000EA System.Collections.Generic.List`1<System.Text.Json.JsonElement> SocketIOClient.Messages.BinaryMessage::get_JsonElements()
extern void BinaryMessage_get_JsonElements_m22A4CAD588EB7F51469D0EC607EC1517FB9242B9 (void);
// 0x000000EB System.Void SocketIOClient.Messages.BinaryMessage::set_JsonElements(System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void BinaryMessage_set_JsonElements_mE1BEFAD769DD0E5A18F7D4A792A8BBE6A308B944 (void);
// 0x000000EC System.String SocketIOClient.Messages.BinaryMessage::get_Json()
extern void BinaryMessage_get_Json_m7BDB54A72D4B384A282EEFEE3FC20ADD8E386006 (void);
// 0x000000ED System.Void SocketIOClient.Messages.BinaryMessage::set_Json(System.String)
extern void BinaryMessage_set_Json_mD9768CB381F44A08DF584D94BABC0F2AE54845E5 (void);
// 0x000000EE System.Int32 SocketIOClient.Messages.BinaryMessage::get_BinaryCount()
extern void BinaryMessage_get_BinaryCount_mCAD5E1F353E75D1CD65F7E56171D7308456098C5 (void);
// 0x000000EF System.Void SocketIOClient.Messages.BinaryMessage::set_BinaryCount(System.Int32)
extern void BinaryMessage_set_BinaryCount_mFB6806B814207CD244A17A422839451BA3EEE00D (void);
// 0x000000F0 System.Void SocketIOClient.Messages.BinaryMessage::set_Eio(System.Int32)
extern void BinaryMessage_set_Eio_mA5F0D55BB0AA5B36FC12DC93555DBB37E61BC798 (void);
// 0x000000F1 System.Void SocketIOClient.Messages.BinaryMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void BinaryMessage_set_Protocol_m7D775B3886C401B5485E9E00C0B1588FD37D53DD (void);
// 0x000000F2 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.BinaryMessage::get_OutgoingBytes()
extern void BinaryMessage_get_OutgoingBytes_m9AF39913F8652D5805D6DF76A6BB5DDA94865B49 (void);
// 0x000000F3 System.Void SocketIOClient.Messages.BinaryMessage::set_OutgoingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void BinaryMessage_set_OutgoingBytes_m0AEF3EF6974784DB66FF5328C4810A0C4FBDBEAB (void);
// 0x000000F4 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.BinaryMessage::get_IncomingBytes()
extern void BinaryMessage_get_IncomingBytes_m6176E430798B6B981ACF0EDE3E8B46186906F8CE (void);
// 0x000000F5 System.Void SocketIOClient.Messages.BinaryMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void BinaryMessage_set_IncomingBytes_m1092FBA649646F7815529C8BAC29791429906284 (void);
// 0x000000F6 System.Void SocketIOClient.Messages.BinaryMessage::Read(System.String)
extern void BinaryMessage_Read_m57D8645A684D3A0C652833561BFF90EE9D441823 (void);
// 0x000000F7 System.String SocketIOClient.Messages.BinaryMessage::Write()
extern void BinaryMessage_Write_m299FBD5EF6ACCEE855CBB329EAFE6ACFDC7052EE (void);
// 0x000000F8 System.Void SocketIOClient.Messages.BinaryMessage::.ctor()
extern void BinaryMessage__ctor_m6AF1DBE76C382D67D35B42E1B3C2B635495ED4F7 (void);
// 0x000000F9 SocketIOClient.Messages.MessageType SocketIOClient.Messages.ClientAckMessage::get_Type()
extern void ClientAckMessage_get_Type_mDD77EDF5269C88D48BEB39F6CD46F57524D4D39C (void);
// 0x000000FA System.String SocketIOClient.Messages.ClientAckMessage::get_Namespace()
extern void ClientAckMessage_get_Namespace_m63EC8D1F5F4A3FD033A21A9A7736E1BBCB8E6BB6 (void);
// 0x000000FB System.Void SocketIOClient.Messages.ClientAckMessage::set_Namespace(System.String)
extern void ClientAckMessage_set_Namespace_m0E308A032CFAE0C1E9A7AECEFEA8F3122B1EB3E7 (void);
// 0x000000FC System.String SocketIOClient.Messages.ClientAckMessage::get_Event()
extern void ClientAckMessage_get_Event_m4E1029DC9E74D4791827C070DF1BE7229E74564C (void);
// 0x000000FD System.Collections.Generic.List`1<System.Text.Json.JsonElement> SocketIOClient.Messages.ClientAckMessage::get_JsonElements()
extern void ClientAckMessage_get_JsonElements_m4EBE99C0156E918C12946B8801BBF4532E13F2BF (void);
// 0x000000FE System.Void SocketIOClient.Messages.ClientAckMessage::set_JsonElements(System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void ClientAckMessage_set_JsonElements_m3A410CA3471CD989D75DFFAB35A79858A5953A88 (void);
// 0x000000FF System.String SocketIOClient.Messages.ClientAckMessage::get_Json()
extern void ClientAckMessage_get_Json_m1D2D03D72410C66DA620FA77B772E0661D4EADFD (void);
// 0x00000100 System.Int32 SocketIOClient.Messages.ClientAckMessage::get_Id()
extern void ClientAckMessage_get_Id_m7B65941B3858ACCB71050DB455C09B83710CD4B2 (void);
// 0x00000101 System.Void SocketIOClient.Messages.ClientAckMessage::set_Id(System.Int32)
extern void ClientAckMessage_set_Id_m552EB2A26BC0AE3A67177B0D3DBE174F690AF824 (void);
// 0x00000102 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ClientAckMessage::get_OutgoingBytes()
extern void ClientAckMessage_get_OutgoingBytes_m17FAB47D528E3B2C40E407B974F77F121E5902B7 (void);
// 0x00000103 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ClientAckMessage::get_IncomingBytes()
extern void ClientAckMessage_get_IncomingBytes_mD5D10FF092216467AC85772D7DF2C89158499DAD (void);
// 0x00000104 System.Void SocketIOClient.Messages.ClientAckMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void ClientAckMessage_set_IncomingBytes_m61174B457A26958E141D260B3BF1508A146BD976 (void);
// 0x00000105 System.Int32 SocketIOClient.Messages.ClientAckMessage::get_BinaryCount()
extern void ClientAckMessage_get_BinaryCount_mCB5AE12314E9677CA289F008B341891041778DAE (void);
// 0x00000106 System.Void SocketIOClient.Messages.ClientAckMessage::set_Eio(System.Int32)
extern void ClientAckMessage_set_Eio_mB675DDC19D4BC87F0983349E6C1DBF3E84FBC843 (void);
// 0x00000107 System.Void SocketIOClient.Messages.ClientAckMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void ClientAckMessage_set_Protocol_mE2EC5B7E221B3493A7063810BBBD538FFF724069 (void);
// 0x00000108 System.Void SocketIOClient.Messages.ClientAckMessage::Read(System.String)
extern void ClientAckMessage_Read_mDBD41122F7002925C0CA930857DC08D57FA95DBC (void);
// 0x00000109 System.String SocketIOClient.Messages.ClientAckMessage::Write()
extern void ClientAckMessage_Write_m0BDFF03EE260CA0393BC4B8D4875094363F610F6 (void);
// 0x0000010A System.Void SocketIOClient.Messages.ClientAckMessage::.ctor()
extern void ClientAckMessage__ctor_m4A32FBBD2D1664D1D007755A52D553193FC21BFD (void);
// 0x0000010B SocketIOClient.Messages.MessageType SocketIOClient.Messages.ClientBinaryAckMessage::get_Type()
extern void ClientBinaryAckMessage_get_Type_m65CBCC265E17593D36AE1DDFA3E21A1F9CC30CF7 (void);
// 0x0000010C System.String SocketIOClient.Messages.ClientBinaryAckMessage::get_Namespace()
extern void ClientBinaryAckMessage_get_Namespace_mF3061BBC4D59B4A4647697ACBC0CC30000E5A559 (void);
// 0x0000010D System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_Namespace(System.String)
extern void ClientBinaryAckMessage_set_Namespace_mC27A209BE8B51508A386C97A6CAA58EE2F695D53 (void);
// 0x0000010E System.String SocketIOClient.Messages.ClientBinaryAckMessage::get_Event()
extern void ClientBinaryAckMessage_get_Event_m51C6D5CD0A4A594CD8485498498B23F344D71EC9 (void);
// 0x0000010F System.Collections.Generic.List`1<System.Text.Json.JsonElement> SocketIOClient.Messages.ClientBinaryAckMessage::get_JsonElements()
extern void ClientBinaryAckMessage_get_JsonElements_m89379F5E3191C45DD20BEC5B41486663A8247FE9 (void);
// 0x00000110 System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_JsonElements(System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void ClientBinaryAckMessage_set_JsonElements_mAF9ADD78439515C24E55DF9D654A69BA97F51BB8 (void);
// 0x00000111 System.String SocketIOClient.Messages.ClientBinaryAckMessage::get_Json()
extern void ClientBinaryAckMessage_get_Json_mC12CDDE2EC5E404E8869AA0C4B7A2B5705B0A468 (void);
// 0x00000112 System.Int32 SocketIOClient.Messages.ClientBinaryAckMessage::get_Id()
extern void ClientBinaryAckMessage_get_Id_m5FDB4ECB225E66B9A372CE532ED6A0573DF2B626 (void);
// 0x00000113 System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_Id(System.Int32)
extern void ClientBinaryAckMessage_set_Id_mBAD409A433D3E03028AB31C3B7D6946D1B853F4D (void);
// 0x00000114 System.Int32 SocketIOClient.Messages.ClientBinaryAckMessage::get_BinaryCount()
extern void ClientBinaryAckMessage_get_BinaryCount_m7FAC63EADBBE1A1119FA3D02AA8413EA9D38B7A0 (void);
// 0x00000115 System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_BinaryCount(System.Int32)
extern void ClientBinaryAckMessage_set_BinaryCount_m8DE802695E94A4DC67C1EDED8A58E68443432575 (void);
// 0x00000116 System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_Eio(System.Int32)
extern void ClientBinaryAckMessage_set_Eio_mC80168F7A70621DA9819ABD2A3BD062812BA3FA6 (void);
// 0x00000117 System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void ClientBinaryAckMessage_set_Protocol_mDCC45C47884C9084000C140F6543A0187DAF426E (void);
// 0x00000118 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ClientBinaryAckMessage::get_OutgoingBytes()
extern void ClientBinaryAckMessage_get_OutgoingBytes_mB1E8E08B93D7E4D6255E215C0DB3B72D6B692A06 (void);
// 0x00000119 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ClientBinaryAckMessage::get_IncomingBytes()
extern void ClientBinaryAckMessage_get_IncomingBytes_mA30B44355FB52052C70AC0987A2BA0C7AA2B96BF (void);
// 0x0000011A System.Void SocketIOClient.Messages.ClientBinaryAckMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void ClientBinaryAckMessage_set_IncomingBytes_mE598B123B5E9B1A32DB1E75DB2EDA6647AB3159C (void);
// 0x0000011B System.Void SocketIOClient.Messages.ClientBinaryAckMessage::Read(System.String)
extern void ClientBinaryAckMessage_Read_m14B1634CA311FBFAA58272DDAB681DBA3BB69D1D (void);
// 0x0000011C System.String SocketIOClient.Messages.ClientBinaryAckMessage::Write()
extern void ClientBinaryAckMessage_Write_mFB5927BEE86A0920F4E892AB4052983D9D0DB434 (void);
// 0x0000011D System.Void SocketIOClient.Messages.ClientBinaryAckMessage::.ctor()
extern void ClientBinaryAckMessage__ctor_mB34AEA49FF827B404979FC67D178CC2947802C66 (void);
// 0x0000011E SocketIOClient.Messages.MessageType SocketIOClient.Messages.ConnectedMessage::get_Type()
extern void ConnectedMessage_get_Type_m9B681280282AD53FCC2512C0CF5790C5D3709672 (void);
// 0x0000011F System.String SocketIOClient.Messages.ConnectedMessage::get_Namespace()
extern void ConnectedMessage_get_Namespace_mBB9748CE4C8C74F0C4A5D149A7BC761709665E95 (void);
// 0x00000120 System.Void SocketIOClient.Messages.ConnectedMessage::set_Namespace(System.String)
extern void ConnectedMessage_set_Namespace_mA7FDE6E95A56A0C4DDCDC02127D102B482D3D6FA (void);
// 0x00000121 System.String SocketIOClient.Messages.ConnectedMessage::get_Sid()
extern void ConnectedMessage_get_Sid_m4D121694E0C88D04E5A99433C953CE1585B2BF29 (void);
// 0x00000122 System.Void SocketIOClient.Messages.ConnectedMessage::set_Sid(System.String)
extern void ConnectedMessage_set_Sid_m80379E0AC6A06BB24F3FEC26BB9563C8CD59E93C (void);
// 0x00000123 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ConnectedMessage::get_OutgoingBytes()
extern void ConnectedMessage_get_OutgoingBytes_mFAC5CCE676B708DFE7EFCA8B6845E0B55CA34704 (void);
// 0x00000124 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ConnectedMessage::get_IncomingBytes()
extern void ConnectedMessage_get_IncomingBytes_mCE4223CDA74C273D269F1E6D04C6B3E5BCDB108F (void);
// 0x00000125 System.Void SocketIOClient.Messages.ConnectedMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void ConnectedMessage_set_IncomingBytes_mB3582A4D805FFB5E600AAE3E113534B5C2BA28E8 (void);
// 0x00000126 System.Int32 SocketIOClient.Messages.ConnectedMessage::get_BinaryCount()
extern void ConnectedMessage_get_BinaryCount_m1C3D0F041C7BCE37A2007B2CAFD89FE4D5726946 (void);
// 0x00000127 System.Int32 SocketIOClient.Messages.ConnectedMessage::get_Eio()
extern void ConnectedMessage_get_Eio_mE24F9D9F65069643E7A512BB0611448FE892F0C2 (void);
// 0x00000128 System.Void SocketIOClient.Messages.ConnectedMessage::set_Eio(System.Int32)
extern void ConnectedMessage_set_Eio_mCA4D23CABE3F03CA5F2D48A4929155903607621E (void);
// 0x00000129 System.Void SocketIOClient.Messages.ConnectedMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void ConnectedMessage_set_Protocol_m05896E4D86A5D4F7D4DBFFC6CFF73482322B0654 (void);
// 0x0000012A System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> SocketIOClient.Messages.ConnectedMessage::get_Query()
extern void ConnectedMessage_get_Query_m45D8A90C909B5954183B9AACBBFD2908F17C1692 (void);
// 0x0000012B System.Void SocketIOClient.Messages.ConnectedMessage::set_Query(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void ConnectedMessage_set_Query_mDDE08088D7D65E1896B3DDF467CF66D4CBF9C1B2 (void);
// 0x0000012C System.Void SocketIOClient.Messages.ConnectedMessage::Read(System.String)
extern void ConnectedMessage_Read_mBBEE6EF47B68F1F2AD4E60AD8E8E34A998079A3A (void);
// 0x0000012D System.String SocketIOClient.Messages.ConnectedMessage::Write()
extern void ConnectedMessage_Write_m76DA157CF9C30F591D54A2012B7CE33227ACDE33 (void);
// 0x0000012E System.Void SocketIOClient.Messages.ConnectedMessage::Eio4Read(System.String)
extern void ConnectedMessage_Eio4Read_mA69A245C3830F1F30751BBA5C59A53522FB35904 (void);
// 0x0000012F System.String SocketIOClient.Messages.ConnectedMessage::Eio4Write()
extern void ConnectedMessage_Eio4Write_mCAE5F367C6CF6A1271AC781EABCCA45B9A6FC3A8 (void);
// 0x00000130 System.Void SocketIOClient.Messages.ConnectedMessage::Eio3Read(System.String)
extern void ConnectedMessage_Eio3Read_m2FFBB30649728AE101642441B3974D5ADAB92A4F (void);
// 0x00000131 System.String SocketIOClient.Messages.ConnectedMessage::Eio3Write()
extern void ConnectedMessage_Eio3Write_m7071001C216992734A9B19E051EAE42F5366A255 (void);
// 0x00000132 System.Void SocketIOClient.Messages.ConnectedMessage::.ctor()
extern void ConnectedMessage__ctor_m7B00E3238A54C7A1D615CD24744F8BD8FC0326E5 (void);
// 0x00000133 SocketIOClient.Messages.MessageType SocketIOClient.Messages.DisconnectedMessage::get_Type()
extern void DisconnectedMessage_get_Type_m89E627971B8E8039DBA97989D989FC3695D0E52F (void);
// 0x00000134 System.String SocketIOClient.Messages.DisconnectedMessage::get_Namespace()
extern void DisconnectedMessage_get_Namespace_mBC654DF14D562AACD4678E23EFF3A5AC69640D59 (void);
// 0x00000135 System.Void SocketIOClient.Messages.DisconnectedMessage::set_Namespace(System.String)
extern void DisconnectedMessage_set_Namespace_m137124B9A6AD089CD1819267FC45348711C220C4 (void);
// 0x00000136 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.DisconnectedMessage::get_OutgoingBytes()
extern void DisconnectedMessage_get_OutgoingBytes_m4B4FE93340DDDF8301A0A6CB5ED17C8C5470ED17 (void);
// 0x00000137 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.DisconnectedMessage::get_IncomingBytes()
extern void DisconnectedMessage_get_IncomingBytes_mC6CF82CBBD5D15443DE29F95E76844F09CE831AC (void);
// 0x00000138 System.Void SocketIOClient.Messages.DisconnectedMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void DisconnectedMessage_set_IncomingBytes_m7BBCB930B5C7FD1A9EA060E32B6FF5F54E1CC1C7 (void);
// 0x00000139 System.Int32 SocketIOClient.Messages.DisconnectedMessage::get_BinaryCount()
extern void DisconnectedMessage_get_BinaryCount_m87D1D38C6DBAC669C1714C9E08CC3E3D020F7C04 (void);
// 0x0000013A System.Void SocketIOClient.Messages.DisconnectedMessage::set_Eio(System.Int32)
extern void DisconnectedMessage_set_Eio_m7D809F9D4C4F6B3DA317012FD4915F95113F3F90 (void);
// 0x0000013B System.Void SocketIOClient.Messages.DisconnectedMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void DisconnectedMessage_set_Protocol_mAC0B93C5BC99FDCCA8917275B3FAD679CB40C0EE (void);
// 0x0000013C System.Void SocketIOClient.Messages.DisconnectedMessage::Read(System.String)
extern void DisconnectedMessage_Read_mF220177ADB484FA3886EA4C087768CAFA69115BF (void);
// 0x0000013D System.String SocketIOClient.Messages.DisconnectedMessage::Write()
extern void DisconnectedMessage_Write_mD6B38FDC0ABDCE0BD00E789D443531EB08BDC04D (void);
// 0x0000013E System.Void SocketIOClient.Messages.DisconnectedMessage::.ctor()
extern void DisconnectedMessage__ctor_mF4D04405FF5F17A52A6CBA58B034862A10633CD5 (void);
// 0x0000013F SocketIOClient.Messages.MessageType SocketIOClient.Messages.ErrorMessage::get_Type()
extern void ErrorMessage_get_Type_m97A024A71590D58A53650049C282742A277D7D2D (void);
// 0x00000140 System.String SocketIOClient.Messages.ErrorMessage::get_Message()
extern void ErrorMessage_get_Message_m2FA4EC941DBB4D885911E9B6A08206CCC6EAE05C (void);
// 0x00000141 System.Void SocketIOClient.Messages.ErrorMessage::set_Message(System.String)
extern void ErrorMessage_set_Message_m1C676E1D517B0B82E65977F394E66A0C23BDA2FD (void);
// 0x00000142 System.Void SocketIOClient.Messages.ErrorMessage::set_Namespace(System.String)
extern void ErrorMessage_set_Namespace_m1621D5622A176D55983D56ACBF3BDF9225391D43 (void);
// 0x00000143 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ErrorMessage::get_OutgoingBytes()
extern void ErrorMessage_get_OutgoingBytes_mB7064FB185F60EF9688241881E7A548EA4FEE8B8 (void);
// 0x00000144 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.ErrorMessage::get_IncomingBytes()
extern void ErrorMessage_get_IncomingBytes_mD9E5D721243093414A3BC2A673642D1D3F3930FB (void);
// 0x00000145 System.Void SocketIOClient.Messages.ErrorMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void ErrorMessage_set_IncomingBytes_mF36F79779BC64A1B7AE435DFFA050428A35F689A (void);
// 0x00000146 System.Int32 SocketIOClient.Messages.ErrorMessage::get_BinaryCount()
extern void ErrorMessage_get_BinaryCount_mD3A9C112BB333B628E3F160DCD82B651971A9DC3 (void);
// 0x00000147 System.Int32 SocketIOClient.Messages.ErrorMessage::get_Eio()
extern void ErrorMessage_get_Eio_mEA9E313693DB15A377C002395ECFA1AF273BC29E (void);
// 0x00000148 System.Void SocketIOClient.Messages.ErrorMessage::set_Eio(System.Int32)
extern void ErrorMessage_set_Eio_m9A26978230A37C6746E2EFA8BD7CBD28E45B68E7 (void);
// 0x00000149 System.Void SocketIOClient.Messages.ErrorMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void ErrorMessage_set_Protocol_mD3210142031FD98E9A9080037014A9DC9AD2D325 (void);
// 0x0000014A System.Void SocketIOClient.Messages.ErrorMessage::Read(System.String)
extern void ErrorMessage_Read_m8F4C7D80DBE856CCA850144B57A2A0C64B9BE1A0 (void);
// 0x0000014B System.String SocketIOClient.Messages.ErrorMessage::Write()
extern void ErrorMessage_Write_m14360C4B068946B02E2BB18288421228997BB448 (void);
// 0x0000014C System.Void SocketIOClient.Messages.ErrorMessage::.ctor()
extern void ErrorMessage__ctor_mF15E287DE5BE37E637906637A71A912A01A27104 (void);
// 0x0000014D SocketIOClient.Messages.MessageType SocketIOClient.Messages.EventMessage::get_Type()
extern void EventMessage_get_Type_m1CFC83E740328B4218A1BE2D820284B80A253664 (void);
// 0x0000014E System.String SocketIOClient.Messages.EventMessage::get_Namespace()
extern void EventMessage_get_Namespace_mA2473D71D774C869AB286DFA44A5BFAD6AAE3D97 (void);
// 0x0000014F System.Void SocketIOClient.Messages.EventMessage::set_Namespace(System.String)
extern void EventMessage_set_Namespace_m262295490C730CAC637DF2A0ED3E5222171253D4 (void);
// 0x00000150 System.String SocketIOClient.Messages.EventMessage::get_Event()
extern void EventMessage_get_Event_m1B6110DACCD1175CCA546189CA5BC6D31B852E99 (void);
// 0x00000151 System.Void SocketIOClient.Messages.EventMessage::set_Event(System.String)
extern void EventMessage_set_Event_mFD9DA13A75C0D0C1F4926558FA4D78F808F76C39 (void);
// 0x00000152 System.Int32 SocketIOClient.Messages.EventMessage::get_Id()
extern void EventMessage_get_Id_m67179AADC6C01AE12374E1940C861770727F9ED5 (void);
// 0x00000153 System.Void SocketIOClient.Messages.EventMessage::set_Id(System.Int32)
extern void EventMessage_set_Id_m806A84CA037CCBD53F96C5C31632AA70F152C1E1 (void);
// 0x00000154 System.Collections.Generic.List`1<System.Text.Json.JsonElement> SocketIOClient.Messages.EventMessage::get_JsonElements()
extern void EventMessage_get_JsonElements_m5F76EDD056F8AFD32C049EF08816ACAE644F5017 (void);
// 0x00000155 System.Void SocketIOClient.Messages.EventMessage::set_JsonElements(System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void EventMessage_set_JsonElements_mF40A41BCE4C89E3EA46B74519DE5EE213355D03D (void);
// 0x00000156 System.String SocketIOClient.Messages.EventMessage::get_Json()
extern void EventMessage_get_Json_mCDB73487936C56C04AE0AAA0855B57A25F68BF41 (void);
// 0x00000157 System.Void SocketIOClient.Messages.EventMessage::set_Json(System.String)
extern void EventMessage_set_Json_m4386F4B466346D2538B8AF0696D729521C9D00D6 (void);
// 0x00000158 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.EventMessage::get_OutgoingBytes()
extern void EventMessage_get_OutgoingBytes_mEE2B105F54ECACF5F024B33B6DE55FBB8B4A2C05 (void);
// 0x00000159 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.EventMessage::get_IncomingBytes()
extern void EventMessage_get_IncomingBytes_mA4330EBE622AF968FD4C3863787567891BADA0FD (void);
// 0x0000015A System.Void SocketIOClient.Messages.EventMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void EventMessage_set_IncomingBytes_m3A6B20C6F5C764CE1974A6DB4C6E8C2EF5D5AE34 (void);
// 0x0000015B System.Int32 SocketIOClient.Messages.EventMessage::get_BinaryCount()
extern void EventMessage_get_BinaryCount_m77F298305163FA7315EEFEF117D8A9FBEF798B06 (void);
// 0x0000015C System.Void SocketIOClient.Messages.EventMessage::set_Eio(System.Int32)
extern void EventMessage_set_Eio_m695EDB9FFB1E5724ED628F6827E2E9D60DA40943 (void);
// 0x0000015D System.Void SocketIOClient.Messages.EventMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void EventMessage_set_Protocol_m714D79DD63FECC93AC5CF26366C0FCD3CC61845D (void);
// 0x0000015E System.Void SocketIOClient.Messages.EventMessage::Read(System.String)
extern void EventMessage_Read_mF26944D1C918BA93F54EE56A8F343BC881E17FFA (void);
// 0x0000015F System.String SocketIOClient.Messages.EventMessage::Write()
extern void EventMessage_Write_m020DD8C979F3049302769D5785E19EDF4C9FB124 (void);
// 0x00000160 System.Void SocketIOClient.Messages.EventMessage::.ctor()
extern void EventMessage__ctor_m1A9EC3C6252BBBEA87B8E2728FF81118E77FB9BF (void);
// 0x00000161 SocketIOClient.Messages.MessageType SocketIOClient.Messages.IMessage::get_Type()
// 0x00000162 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.IMessage::get_OutgoingBytes()
// 0x00000163 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.IMessage::get_IncomingBytes()
// 0x00000164 System.Void SocketIOClient.Messages.IMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
// 0x00000165 System.Int32 SocketIOClient.Messages.IMessage::get_BinaryCount()
// 0x00000166 System.Void SocketIOClient.Messages.IMessage::set_Eio(System.Int32)
// 0x00000167 System.Void SocketIOClient.Messages.IMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
// 0x00000168 System.Void SocketIOClient.Messages.IMessage::Read(System.String)
// 0x00000169 System.String SocketIOClient.Messages.IMessage::Write()
// 0x0000016A SocketIOClient.Messages.IMessage SocketIOClient.Messages.MessageFactory::CreateMessage(SocketIOClient.Messages.MessageType)
extern void MessageFactory_CreateMessage_m41EF5FE83024FC9910A15146F92B7B40F4FBAA3B (void);
// 0x0000016B SocketIOClient.Messages.IMessage SocketIOClient.Messages.MessageFactory::CreateMessage(System.Int32,System.String)
extern void MessageFactory_CreateMessage_m02159BEC7B481B1AE143D788B4662C7A82D54907 (void);
// 0x0000016C SocketIOClient.Messages.MessageType SocketIOClient.Messages.OpenedMessage::get_Type()
extern void OpenedMessage_get_Type_m9DA1E36A0ECA24E2E7F70992DFB31393061C4FE0 (void);
// 0x0000016D System.String SocketIOClient.Messages.OpenedMessage::get_Sid()
extern void OpenedMessage_get_Sid_m1C2DFD9ABF2841ECC72816888C39F53B90A123BB (void);
// 0x0000016E System.Void SocketIOClient.Messages.OpenedMessage::set_Sid(System.String)
extern void OpenedMessage_set_Sid_m91D157ACA4F17CE6F04F912C5E6576EFA7ED1E3E (void);
// 0x0000016F System.Collections.Generic.List`1<System.String> SocketIOClient.Messages.OpenedMessage::get_Upgrades()
extern void OpenedMessage_get_Upgrades_mF713B2623F775FA9E340303DB033C263E826C9C1 (void);
// 0x00000170 System.Void SocketIOClient.Messages.OpenedMessage::set_Upgrades(System.Collections.Generic.List`1<System.String>)
extern void OpenedMessage_set_Upgrades_m57F2B66A6F46EFAEFE0DB04833C058834D058076 (void);
// 0x00000171 System.Int32 SocketIOClient.Messages.OpenedMessage::get_PingInterval()
extern void OpenedMessage_get_PingInterval_m6BAFB5A01580F69D4BDFABBBB2453F38DC6B0774 (void);
// 0x00000172 System.Void SocketIOClient.Messages.OpenedMessage::set_PingInterval(System.Int32)
extern void OpenedMessage_set_PingInterval_m6F90759D62F4B9F106213211CBEB729BD77516FD (void);
// 0x00000173 System.Void SocketIOClient.Messages.OpenedMessage::set_PingTimeout(System.Int32)
extern void OpenedMessage_set_PingTimeout_mA06D177C1911170D70B6C871074BB8751A257F1C (void);
// 0x00000174 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.OpenedMessage::get_OutgoingBytes()
extern void OpenedMessage_get_OutgoingBytes_m84828C6286305F9254089FE27036FD61DED71E07 (void);
// 0x00000175 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.OpenedMessage::get_IncomingBytes()
extern void OpenedMessage_get_IncomingBytes_mEBB3DA727A3E6E1B5102B2BF2C79839E5452A6E4 (void);
// 0x00000176 System.Void SocketIOClient.Messages.OpenedMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void OpenedMessage_set_IncomingBytes_m55D0D0FBA7764E86BFDDD74A6B5DB3642258EC4F (void);
// 0x00000177 System.Int32 SocketIOClient.Messages.OpenedMessage::get_BinaryCount()
extern void OpenedMessage_get_BinaryCount_m3B7FEAE705FFBE0FB050E7DD20FF4173D0BDD811 (void);
// 0x00000178 System.Void SocketIOClient.Messages.OpenedMessage::set_Eio(System.Int32)
extern void OpenedMessage_set_Eio_m27947BFE1007703F0AAF867D632D461E99252612 (void);
// 0x00000179 System.Void SocketIOClient.Messages.OpenedMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void OpenedMessage_set_Protocol_m896FE4E58F36070B60C381736E34B8385D79BD56 (void);
// 0x0000017A System.Void SocketIOClient.Messages.OpenedMessage::Read(System.String)
extern void OpenedMessage_Read_m7FE67F089124FFF7955F74D4423090CE6D5672D2 (void);
// 0x0000017B System.String SocketIOClient.Messages.OpenedMessage::Write()
extern void OpenedMessage_Write_m2C60B177CA1B9546A1E15459DD62E52ADD0EF602 (void);
// 0x0000017C System.Void SocketIOClient.Messages.OpenedMessage::.ctor()
extern void OpenedMessage__ctor_mF01ECD7347F4407A810AD624450A093367B6ABCA (void);
// 0x0000017D SocketIOClient.Messages.MessageType SocketIOClient.Messages.PingMessage::get_Type()
extern void PingMessage_get_Type_m5B2D9C8F58502C86D75052453D4B2BBF63E8EFDE (void);
// 0x0000017E System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.PingMessage::get_OutgoingBytes()
extern void PingMessage_get_OutgoingBytes_mA33B556D75B63546FE1680B93906C4DDACC16F54 (void);
// 0x0000017F System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.PingMessage::get_IncomingBytes()
extern void PingMessage_get_IncomingBytes_mAB4CB776A307E0C1627ECC173704ACB7DEB005CB (void);
// 0x00000180 System.Void SocketIOClient.Messages.PingMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void PingMessage_set_IncomingBytes_m02799D45BFCC060407A5DADA8EE815B3BDB7E597 (void);
// 0x00000181 System.Int32 SocketIOClient.Messages.PingMessage::get_BinaryCount()
extern void PingMessage_get_BinaryCount_m5A1C66215FB119E85F93E4F59D34C1DAD2F6CD33 (void);
// 0x00000182 System.Void SocketIOClient.Messages.PingMessage::set_Eio(System.Int32)
extern void PingMessage_set_Eio_mB6C5A7B749780F1CB30A671BCE98CC7BF033A26A (void);
// 0x00000183 System.Void SocketIOClient.Messages.PingMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void PingMessage_set_Protocol_mE38DF0C7211E1389CE1D252151F33FD54965404F (void);
// 0x00000184 System.Void SocketIOClient.Messages.PingMessage::Read(System.String)
extern void PingMessage_Read_mD2F19855C1BAE5FF5011F1E0BE89BE8D27C6E97D (void);
// 0x00000185 System.String SocketIOClient.Messages.PingMessage::Write()
extern void PingMessage_Write_m841E392144E32D096D95A3C3446D4D61437F14E9 (void);
// 0x00000186 System.Void SocketIOClient.Messages.PingMessage::.ctor()
extern void PingMessage__ctor_m1E228E96BA772528D21CE3AEA8E40853623EF2C0 (void);
// 0x00000187 SocketIOClient.Messages.MessageType SocketIOClient.Messages.PongMessage::get_Type()
extern void PongMessage_get_Type_m7FF6CDF2744BEF21BDC7F7603B0A792EBE68655D (void);
// 0x00000188 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.PongMessage::get_OutgoingBytes()
extern void PongMessage_get_OutgoingBytes_m92CBE117D20EFD12A4A8674566CE185D76B16690 (void);
// 0x00000189 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.Messages.PongMessage::get_IncomingBytes()
extern void PongMessage_get_IncomingBytes_mC2E61D002A47797E8379978F60B8FA39EB89E072 (void);
// 0x0000018A System.Void SocketIOClient.Messages.PongMessage::set_IncomingBytes(System.Collections.Generic.List`1<System.Byte[]>)
extern void PongMessage_set_IncomingBytes_m9B6B09A1AA7AB28699B3C0AA5E1EFC4A5973711D (void);
// 0x0000018B System.Int32 SocketIOClient.Messages.PongMessage::get_BinaryCount()
extern void PongMessage_get_BinaryCount_m8568D4D84A7BC394DDE4CDA945DCB1A1BB9B1695 (void);
// 0x0000018C System.Void SocketIOClient.Messages.PongMessage::set_Eio(System.Int32)
extern void PongMessage_set_Eio_m5ADF0E510ECCBBFBF803155E9392544C5D8F9CEF (void);
// 0x0000018D System.Void SocketIOClient.Messages.PongMessage::set_Protocol(SocketIOClient.Transport.TransportProtocol)
extern void PongMessage_set_Protocol_m19FE10462749E7A401EAA6FFAD950ACB492E6C7F (void);
// 0x0000018E System.TimeSpan SocketIOClient.Messages.PongMessage::get_Duration()
extern void PongMessage_get_Duration_m678F36A9284497BF1D8B738AE321D6C0EEEBA9F8 (void);
// 0x0000018F System.Void SocketIOClient.Messages.PongMessage::set_Duration(System.TimeSpan)
extern void PongMessage_set_Duration_m3F8BA41944161F752771D65A9AD307460068B6F0 (void);
// 0x00000190 System.Void SocketIOClient.Messages.PongMessage::Read(System.String)
extern void PongMessage_Read_mACA9466D7A3E56B5D6568D98358095B816744E88 (void);
// 0x00000191 System.String SocketIOClient.Messages.PongMessage::Write()
extern void PongMessage_Write_m118684351CE5E63F446D3F57A326297C366B8371 (void);
// 0x00000192 System.Void SocketIOClient.Messages.PongMessage::.ctor()
extern void PongMessage__ctor_m3493A77F2AD1E2BCF52F768C9A33A08BAD9658ED (void);
// 0x00000193 System.Void SocketIOClient.JsonSerializer.ByteArrayConverter::.ctor()
extern void ByteArrayConverter__ctor_m42010C9AC114C4E5A81A34005C410971137EB52F (void);
// 0x00000194 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.JsonSerializer.ByteArrayConverter::get_Bytes()
extern void ByteArrayConverter_get_Bytes_mCF080CE5AFFFCB4649F40F3E469A297420BAFC28 (void);
// 0x00000195 System.Byte[] SocketIOClient.JsonSerializer.ByteArrayConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Read_m6CA157B19F9AEBF3D846326A9F7434C80EC21C44 (void);
// 0x00000196 System.Void SocketIOClient.JsonSerializer.ByteArrayConverter::Write(System.Text.Json.Utf8JsonWriter,System.Byte[],System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Write_mC21A0EE1A5E51C098CF0C4DA1FDBE4E52A4BECA4 (void);
// 0x00000197 SocketIOClient.JsonSerializer.JsonSerializeResult SocketIOClient.JsonSerializer.IJsonSerializer::Serialize(System.Object[])
// 0x00000198 T SocketIOClient.JsonSerializer.IJsonSerializer::Deserialize(System.String,System.Collections.Generic.IList`1<System.Byte[]>)
// 0x00000199 System.String SocketIOClient.JsonSerializer.JsonSerializeResult::get_Json()
extern void JsonSerializeResult_get_Json_mB0DB437258A308B7C3631B24D12457F813CE5000 (void);
// 0x0000019A System.Void SocketIOClient.JsonSerializer.JsonSerializeResult::set_Json(System.String)
extern void JsonSerializeResult_set_Json_m00057D5060370A218B3659E161AB15FF0E0AFAC4 (void);
// 0x0000019B System.Collections.Generic.IList`1<System.Byte[]> SocketIOClient.JsonSerializer.JsonSerializeResult::get_Bytes()
extern void JsonSerializeResult_get_Bytes_mB4255CBA3832D483011FE9A4462BDCE1FE65D052 (void);
// 0x0000019C System.Void SocketIOClient.JsonSerializer.JsonSerializeResult::set_Bytes(System.Collections.Generic.IList`1<System.Byte[]>)
extern void JsonSerializeResult_set_Bytes_mB09A845EA53D94853444F111F8A114E041FA0602 (void);
// 0x0000019D System.Void SocketIOClient.JsonSerializer.JsonSerializeResult::.ctor()
extern void JsonSerializeResult__ctor_m84F1048A19CF55A7525EA303001987CCBD46151C (void);
// 0x0000019E SocketIOClient.JsonSerializer.JsonSerializeResult SocketIOClient.JsonSerializer.SystemTextJsonSerializer::Serialize(System.Object[])
extern void SystemTextJsonSerializer_Serialize_mBD99A40D03904E43867BB5CEF5A8918A31DAEB0A (void);
// 0x0000019F T SocketIOClient.JsonSerializer.SystemTextJsonSerializer::Deserialize(System.String,System.Collections.Generic.IList`1<System.Byte[]>)
// 0x000001A0 System.Text.Json.JsonSerializerOptions SocketIOClient.JsonSerializer.SystemTextJsonSerializer::GetOptions()
extern void SystemTextJsonSerializer_GetOptions_m62A65B992250F4C3279903EB04ECA3829E4B60AB (void);
// 0x000001A1 System.Text.Json.JsonSerializerOptions SocketIOClient.JsonSerializer.SystemTextJsonSerializer::CreateOptions()
extern void SystemTextJsonSerializer_CreateOptions_m284D599E8AA27E39CBDB33800DCF85488E6DFC61 (void);
// 0x000001A2 System.Func`1<System.Text.Json.JsonSerializerOptions> SocketIOClient.JsonSerializer.SystemTextJsonSerializer::get_OptionsProvider()
extern void SystemTextJsonSerializer_get_OptionsProvider_m78ECE00826C3253812E11D9BBBE1CB0374896AD5 (void);
// 0x000001A3 System.Void SocketIOClient.JsonSerializer.SystemTextJsonSerializer::.ctor()
extern void SystemTextJsonSerializer__ctor_m62045199E591D78BEE032ED7F1889F8FBB8D6EFE (void);
static Il2CppMethodPointer s_methodPointers[419] = 
{
	DisconnectReason__cctor_m8EC057917EA9F6D5856A53BF7FA38A1147FD05BA,
	OnAnyHandler__ctor_mB745ECC893CD627A99639B329A786FE396461A49,
	OnAnyHandler_Invoke_m46706F4492B6F25C439D75E72B01194FD76F7C93,
	SocketIO__ctor_mEC07253956FA33641E29F3767FB12622EC9CC880,
	SocketIO__ctor_m04BB240327A691B496DEEDBCE2BF8FB32F95A584,
	SocketIO__ctor_mB9D49023C182956102981EE2D080641D3F22B6E3,
	SocketIO_get_ServerUri_mF49380061B2CE9628AE1F6EF9E332DC7E39C5DE8,
	SocketIO_set_ServerUri_m2D8C8CEB989653C259E42EDC39C6040D4D22158C,
	SocketIO_get_Router_mDD45FFAA3E2375AB1453CF1FEF1EBAFF04EC2C36,
	SocketIO_set_Router_m0FAF923817200C24A0C11D52916D305FC4E83598,
	SocketIO_set_Id_m4A76C984F9DEDCFA1581EA13521476502407A191,
	SocketIO_get_Namespace_mFF9357291747A0DA8E839E9A1881FA6B88AF508D,
	SocketIO_set_Namespace_m6B8326534BD2D5A7D8DDC39C9BEE34C1150A6AC0,
	SocketIO_get_Connected_m7C107DF1177FAC56963CD4763F19ABAA1B996822,
	SocketIO_set_Connected_m0AFE2343FD42A779FCBC37DCE77182F584A844DC,
	SocketIO_get_Attempts_m1A54D69B54AFB5EB2FDF8767B89C0EA67D18354A,
	SocketIO_set_Attempts_mD2C1453050CA459794420DC171F3387A9C81DAD0,
	SocketIO_get_Options_m86CB03192102E0C6879AD39377BC57B6A0221A8B,
	SocketIO_get_JsonSerializer_m3C41A32E2B80151E68566937F30408EAA2E1CEEA,
	SocketIO_set_JsonSerializer_mD1D7C846218A09DA9DC4B74DAEC5B7BC340EA255,
	SocketIO_get_HttpClient_m4C50BBCEFF741BDC01B3A2466285A22FE428E759,
	SocketIO_set_HttpClient_mB42A5639BCEC711DF82157F1D8B88D9E239A8490,
	SocketIO_get_ClientWebSocketProvider_mA66C593DEA05F6004930AF3DBB2617FE487D87F8,
	SocketIO_set_ClientWebSocketProvider_m3F2082D8E14113B6EB79A3EAE98885F2DA4F9053,
	SocketIO_add_OnConnected_m8C3D5DFB18C2DCC42BFC0E31D50D27C1B942BA39,
	SocketIO_remove_OnConnected_m60DBEBC0A3CB61B6D7E0435E29BD2A7F59925F2F,
	SocketIO_add_OnError_mE4F5A0D01D36956BF4F84F71EA89E83B5B0FE76C,
	SocketIO_remove_OnError_mE622D17481DE041B0A2CB152F83FEC8BB1EE9A08,
	SocketIO_add_OnDisconnected_mB2910F22013D2B8805CD34D14EDA1DD741BDB7A1,
	SocketIO_remove_OnDisconnected_mED2BB005D80B81241C1F5348C8309908AE13E338,
	SocketIO_Initialize_m4699C51B1A8DF696A72A2E3B486603898A7EE3AB,
	SocketIO_CreateRouterIfNull_mEBFDEA3F73ED535D50A4C583BCC4F75DFBAC5880,
	SocketIO_ConnectAsync_mF024D2306601A545290846FAD9CF9A6787FCE7FD,
	SocketIO_PingHandler_m314FA276DC0FDE78495DD64A78CF05AAEA49350A,
	SocketIO_PongHandler_m50D21AFBE77AAD2BD595FE71EF183AEBED71AC77,
	SocketIO_ConnectedHandler_mB6CDB660313CEEC80D6EF98E50CCFFD8892F55A5,
	SocketIO_DisconnectedHandler_m83DA09D06CB70C9FF4FE0A18008B60B044F64C65,
	SocketIO_EventMessageHandler_m68136113717876AD5371777536624CC4F79486A5,
	SocketIO_AckMessageHandler_m4F8E0C5E1812B3955685FFBB1A5CCA1F7C7D91F7,
	SocketIO_ErrorMessageHandler_m67528961DEE035E3CEA5F0E4790671920EFB202A,
	SocketIO_BinaryMessageHandler_mCC86C02F482D3939DD6499E95C611250187B1201,
	SocketIO_BinaryAckMessageHandler_mEB4B389E3B8080FFF138F2A0FDA160EACD5F0B30,
	SocketIO_OnMessageReceived_m23679B9D01930A01B9D50FFE033E0FD0FA1F6C96,
	SocketIO_OnTransportClosed_m8D68B8DA5D49D9C3D927223798EA5482B6A9BD2F,
	SocketIO_On_m17F868A6A67275DDF18DDD6537ACDDFCF01F7C58,
	SocketIO_EmitAsync_m671FB6EADF9481835AD0C81F90F47D49955305C8,
	SocketIO_EmitAsync_mB0F21924EB56E3822698F52CFFDEF79A97D355E1,
	SocketIO_InvokeDisconnect_m52F6E016A6DEB7F701A1E6D162EF4D6F38A2F43B,
	SocketIO_Dispose_m5FDD93F548F9DB6578815733A485F5AB461F2094,
	U3CU3Ec__cctor_mF5F5C245E94519B3E1E7BFFF7058D5DDF229B09B,
	U3CU3Ec__ctor_m46F798975117BBEB817559CEB509A44382A2E805,
	U3CU3Ec_U3CInitializeU3Eb__79_0_m84DD1930A89A2274A57D67DD285835E9A2BB9976,
	U3CConnectAsyncU3Ed__81_MoveNext_m03FD8CC7F89AB39CE4D3F7EE5C485304C30980CE,
	U3CConnectAsyncU3Ed__81_SetStateMachine_m1ECCE93BD645592AA838CC0DAA79961A910639F5,
	U3CEmitAsyncU3Ed__101_MoveNext_m61BDCFC0DCBDD98BFB903E8F98FC837AB61A6724,
	U3CEmitAsyncU3Ed__101_SetStateMachine_m82FDB3356A487BDFBB32BE08AC8B04DA1A1C1C67,
	U3CEmitAsyncU3Ed__102_MoveNext_m8B7ABDB6694D54F5AE0851E58272282C9D851A19,
	U3CEmitAsyncU3Ed__102_SetStateMachine_mFE7510E343DBC01A0B53EBC8EED3EF255919C940,
	U3CInvokeDisconnectU3Ed__105_MoveNext_mE6FC3092DC881E8E13CF6A98AC1498889AD2E5D0,
	U3CInvokeDisconnectU3Ed__105_SetStateMachine_m04BF0AC0D285F30E692F1EB9064013F6F8D34ECA,
	SocketIOOptions__ctor_mE9C28F3528090B7A77131DBF40C9A627AEBCC0A6,
	SocketIOOptions_get_Path_m1FBC6F3D54F28D6AEFAF8BC34CE8AB83521BFD22,
	SocketIOOptions_set_Path_mF10633E51D73A179E9C13DE98858877C01880F5A,
	SocketIOOptions_get_ConnectionTimeout_m42D3C8D3D6D02020137D14416430ED2A72A751F0,
	SocketIOOptions_set_ConnectionTimeout_mCBB08504FAD849BC46274DE73825CB22D5B8CD00,
	SocketIOOptions_get_Query_mFA11FB20F1BF0F85E248DBE76C1E32D3FB98FAA4,
	SocketIOOptions_get_Reconnection_mD04D1F2BF70F0AD7530BB503EE5107B63B8C7E05,
	SocketIOOptions_set_Reconnection_mB136EA9D898F774248A5A98CB955F984FE6D7F83,
	SocketIOOptions_get_ReconnectionDelay_m37AECC4F9CB91B1C1F54502B7D418E8D1947DF11,
	SocketIOOptions_set_ReconnectionDelay_m82D0995BD1EEC7BE8B4638E922DAA79AD7814A39,
	SocketIOOptions_get_ReconnectionDelayMax_m72FFED7A33E94DAA89D19BA954A5731580C4B47F,
	SocketIOOptions_set_ReconnectionDelayMax_mE4BEC23B941B5F7E4489B81DEBF91BD320D46387,
	SocketIOOptions_get_ReconnectionAttempts_m475F99729FA0F7C0CE98D92E1586AF21F5C02A3E,
	SocketIOOptions_set_ReconnectionAttempts_m57223BAF3247069041AC79EBD52170484CE29F7C,
	SocketIOOptions_get_RandomizationFactor_m6CD85264F9BD430E8F5DDB9F7705EB5B57B52A19,
	SocketIOOptions_set_RandomizationFactor_m8708E361E48E9786AAF9BEEAD9254C592972D4D5,
	SocketIOOptions_get_ExtraHeaders_mB57D5270503BC51BFF3B6B5FC32A433CAE43ACA7,
	SocketIOOptions_get_Transport_m9FA7D6F68B7B38E11B0354A29E6521C654F83B02,
	SocketIOOptions_set_Transport_m04CEA24437BE96E7A5452F26A8A6015A66CB0884,
	SocketIOOptions_get_EIO_mD417CA97149DFF2BE4A2AB3ABE8E0CAC43C79AA9,
	SocketIOOptions_set_EIO_mE6719F4103690097238584098D0E24CDCF0699A1,
	SocketIOResponse__ctor_m4447C545F798B099A93F8F586AA92B74ED0FD388,
	SocketIOResponse_get_InComingBytes_mD5D41E0AC1EA34453CE9DF5A373CE1BB4C31971E,
	SocketIOResponse_get_SocketIO_m5B71B6FA3F2A4D38B42813E966DFAF116E7FB08F,
	SocketIOResponse_set_PacketId_mA5D00FA08CA5BE3CFC7AFBD2F974D84476AF9495,
	NULL,
	SocketIOResponse_GetValue_m498376247E5789957CF3770088C4579549BE0385,
	SocketIOResponse_ToString_m190E97E1AA0570ABF0C1C02C54FF1E168AA57ACF,
	NULL,
	UriConverter_GetServerUri_mAE42810A5B16E42CAD6F28F21322693D9E0E4B22,
	UriConverter__ctor_mF635AE23F7E036A7011120F6DD4061FEFDCE4337,
	DefaultClientWebSocket__ctor_m79746088FDB79F75A013EAA8A52ED2269C7CC23F,
	DefaultClientWebSocket_get_ConfigOptions_m0FFE3A15E07BC02B600221B1436F198C3C469AD1,
	DefaultClientWebSocket_get_State_m61DBE2D0D70B8DB40164539DCF6B33B91BF22D4A,
	DefaultClientWebSocket_CloseAsync_m9E183F55F9C327EB8835C7C5EF11775EFD2EDF81,
	DefaultClientWebSocket_ConnectAsync_m64C51C977FF8A51221EA2562864A63D000BBCD13,
	DefaultClientWebSocket_ReceiveAsync_m648B2AACE8C9D93EF572D52706821146B5955069,
	DefaultClientWebSocket_SendAsync_m6A1809AC61900AB4A24437BD226FEEEFAEFD89B8,
	DefaultClientWebSocket_SetRequestHeader_m247C8417212B570E9F50B3C279BE9768286EEF91,
	DefaultClientWebSocket_Dispose_m1D59A850CA89D9B3871F3BFE9E4AA1D6E9E38A31,
	U3CCloseAsyncU3Ed__9_MoveNext_mE4CDE4BD7BDA53916E8CE90BE16F8A514776244E,
	U3CCloseAsyncU3Ed__9_SetStateMachine_mD148CE0C89031C23EA867D2FF34BB81F6FBB8984,
	U3CConnectAsyncU3Ed__10_MoveNext_m6A62A732246F8E9D22ED0AEABB9D789F4875B321,
	U3CConnectAsyncU3Ed__10_SetStateMachine_m437EFC3CEDEB097994C3197EFE243E90FB0F0E53,
	U3CReceiveAsyncU3Ed__11_MoveNext_m3566D421D11AB075C58A344D833590990D9554D1,
	U3CReceiveAsyncU3Ed__11_SetStateMachine_m66A5AF498F96EFF912632912CD93F2D36043A027,
	U3CSendAsyncU3Ed__12_MoveNext_mE78EE0C77C86D6575E6BE71122FBE12BE16A8864,
	U3CSendAsyncU3Ed__12_SetStateMachine_m64CD752CE62EA1350A8057CFA24ADAAF7C3C8FD5,
	HttpTransport__ctor_mAA745776252A2AE56C9E17A1527B9AE3B2D7BE67,
	HttpTransport_get_OnTextReceived_m15CF89073B7DA2790586FAE9400128815DCAC7C5,
	HttpTransport_set_OnTextReceived_mAEA01E64C138127591AAAD75A2DBF1AEBE3314FD,
	HttpTransport_get_OnBinaryReceived_mB99F51EE3345D6202EF7337B52FB40A2D7FF5D41,
	HttpTransport_set_OnBinaryReceived_m965D579FDA010E4F69943F9C9EB3335AE4B2A6C4,
	HttpTransport_AppendRandom_mE058E60B0302806410CCC0639852A6EA790F4AFA,
	HttpTransport_GetAsync_m0EBB5C5F7C47931030A70A6DB24EF1B269EF68F1,
	HttpTransport_SendAsync_m2C7673A37302B7AEE79CD307BF0CFF3B1EA95508,
	HttpTransport_PostAsync_mBACF830DEAC2474E755F9B9ACC46FC5D8D7C526A,
	HttpTransport_PostAsync_m3D2C65490C2C391161ED0E7D320E3CC8EBF33AA7,
	HttpTransport_SplitInt_mC970F6589AEBA8EAD212527C756667AB029C8652,
	HttpTransport_ProduceMessageAsync_m97A6135E853A5F2A3BE2A3EB439C2087A7DDD42E,
	HttpTransport_ProduceText_m89B8A6967ED59CCC741B1AC73654B57428F6AFF8,
	HttpTransport_ProduceBytes_mC55B05BE79F8081AEDC7475DE3720BA7287D96F1,
	U3CGetAsyncU3Ed__12_MoveNext_mC641C4B62670248D906EF7514054F66E7E63E4A1,
	U3CGetAsyncU3Ed__12_SetStateMachine_mCF888DD1BB02D2EB5E362AC40CEE07D81E519E7A,
	U3CSendAsyncU3Ed__13_MoveNext_m4E93C60052E04660F288B2C5F2EA7CD622EFCF1D,
	U3CSendAsyncU3Ed__13_SetStateMachine_m024A5A18084436D890DAB736AE15C8572355A482,
	U3CPostAsyncU3Ed__14_MoveNext_mDA369269CBD65510DD420EADE79EBFD47AA18036,
	U3CPostAsyncU3Ed__14_SetStateMachine_m3BE227D571DCB3C1149853D5363EDFB3012D06D1,
	U3CU3Ec__cctor_mFFFD9900E1093D56E30D597AA3A8A73DE9597B30,
	U3CU3Ec__ctor_m4EE5D37498E7E12A80DCCB8C391D1DCA46182880,
	U3CU3Ec_U3CPostAsyncU3Eb__15_0_mE2A9AFF2A4F971C4AF8A14A55B0764B454E2BECF,
	U3CPostAsyncU3Ed__15_MoveNext_m16D7E48B7E366F62287D117CC9D998833129C438,
	U3CPostAsyncU3Ed__15_SetStateMachine_mC519448B2884FD4C0A877B254582828AC538BCB9,
	U3CProduceMessageAsyncU3Ed__17_MoveNext_m5BDBB12B463982C8D3DCAF88662843960361D635,
	U3CProduceMessageAsyncU3Ed__17_SetStateMachine_m85ADDF619E98B828A3AD77EFE309E8EE0E6DCC5A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TransportRouter__ctor_m96001D3476C8D4DDF9EAFF8B02E7BE8F9C5F3C04,
	TransportRouter_get_ServerUri_m76DD5F6D1F3CE15C77E08B5F3E14184DC5889764,
	TransportRouter_set_ServerUri_mEE683013DBB0F35BCFEC5DBE625DDF099F8A878D,
	TransportRouter_get_Namespace_m1EFE88CA37B3F58535288CA4E2F8464310056B30,
	TransportRouter_set_Namespace_mD055A954C5FF6298D49A8143EFB7EA93AB90E4F3,
	TransportRouter_get_Protocol_m8138CD8D64707ABDAB1EA4527E10622E496A3A04,
	TransportRouter_set_Protocol_mD60882D5BF23AAF4C1E38A15BCD14174824517E1,
	TransportRouter_get_EIO_mC86D14482EB4E63F3802B103188DC42F1A96B677,
	TransportRouter_set_EIO_mE9713AC8626966E10E044F0646C307B1613CC500,
	TransportRouter_get_UriConverter_mBB3506ECF9A1916623F14F8E3793E0A195012B5D,
	TransportRouter_set_UriConverter_m2941F574F55685D9365E939FF0B4FFD687E973D8,
	TransportRouter_get_OnMessageReceived_mD238146DE5B5BA3A69FBEF3D45901A4CDFAFF44E,
	TransportRouter_set_OnMessageReceived_m0CAFAEC2DE52000DEE5CA7A96F73246E3CFA69F1,
	TransportRouter_get_OnTransportClosed_mB5E39DCA53D6C88CD8D4BA0E7A241B3256AB5D7D,
	TransportRouter_set_OnTransportClosed_mB4D7DE54D0F47B81D7C6E6B783A317C004EEB876,
	TransportRouter_ConnectAsync_m2427AB13C13D5667E53D2BC80D9BF53B0FF98034,
	TransportRouter_ConnectByWebsocketAsync_m5E61A3B5E272E5F83206A8D021E002D31E7C1D82,
	TransportRouter_ConnectByPollingAsync_mE44BDC34D0F4CBFCCF74F16237B66A1292A9D650,
	TransportRouter_StartPolling_mEFCC686E0E4E1086D1DC905E41AE2104D6CB0259,
	TransportRouter_StartPing_m57A6DF60D143878A1B7A80E7C6F8087DF19323A1,
	TransportRouter_OnOpened_mF81F97FDD32B87BA06CAB553E4592F5A17361B67,
	TransportRouter_OnTextReceived_mCA059D8F291380A10E94F37F81FE437A8C26FF28,
	TransportRouter_OnBinaryReceived_m1F0A0F7823553FC7572649A2A2F6C460D253EFA4,
	TransportRouter_OnAborted_m42A6C4E5972AD93974128A77AB060245C6A8D8D6,
	TransportRouter_SendAsync_m6CCD8FE5CD3547DD657D25B0B9E90CA78B76640F,
	TransportRouter_DisconnectAsync_m5F372E1D46A2C1283A015AC6130E80D78E783DB4,
	TransportRouter_SendAsync_m5370779C7885AB5A5842E53DB896F3F2D26241B6,
	TransportRouter_SendAsync_mD1B98253A50DF7675ABE1A5D29419B081294B4C9,
	TransportRouter_Dispose_mBB4AE4F893CA0210575BBEDE9B3B6D07F8992B44,
	U3CConnectAsyncU3Ed__41_MoveNext_mB584FA2DE02307EDE154C27292CE30B07E06DEAA,
	U3CConnectAsyncU3Ed__41_SetStateMachine_mABAF1145E76B3C806CD1DC24224EE718C0FE4526,
	U3CConnectByWebsocketAsyncU3Ed__42_MoveNext_mB24CC63EF53BB731A142C8488D73562313F8920C,
	U3CConnectByWebsocketAsyncU3Ed__42_SetStateMachine_m3294DBB8241985C904860D8C910296D0AE1EF4BA,
	U3CConnectByPollingAsyncU3Ed__43_MoveNext_m65122FB1A8D148F0F74455C893EC8303878782DD,
	U3CConnectByPollingAsyncU3Ed__43_SetStateMachine_m37FECED5E2C8F12E3FC6308915571A7789E48339,
	U3CU3Ec__DisplayClass44_0__ctor_m000CAD0BF1757FEAC7B4B8FCC964CF0C7AFC2C71,
	U3CU3Ec__DisplayClass44_0_U3CStartPollingU3Eb__0_mF8DE4F95BFD4770AA2743B6041AB854968ECFBC8,
	U3CU3CStartPollingU3Eb__0U3Ed_MoveNext_m77A6B657D52AD87B8870737A49D46476CD153A0B,
	U3CU3CStartPollingU3Eb__0U3Ed_SetStateMachine_mE5328B25B4712C524080DCAE323D9FD3C4263794,
	U3CU3Ec__DisplayClass45_0__ctor_mB6E0A029FEC217E9E6236648370AA33A6F8B2173,
	U3CU3Ec__DisplayClass45_0_U3CStartPingU3Eb__0_mC8876E143A7683FADD043024F9F92910936F1D6D,
	U3CU3CStartPingU3Eb__0U3Ed_MoveNext_mD8B3D0A6E1B187A5019F1F9456A3E285ABC7BDD6,
	U3CU3CStartPingU3Eb__0U3Ed_SetStateMachine_mE910C8499A852C5A9AE83B20C285F1B5226A63A2,
	U3COnOpenedU3Ed__46_MoveNext_m437285B06D1F08C0CA03A535D157F8FD3ECCDAAB,
	U3COnOpenedU3Ed__46_SetStateMachine_mAED54523AB8B9D9861736CCEA7A47D1EE7022413,
	U3COnTextReceivedU3Ed__47_MoveNext_mC18AD7621DFCA76DA92EF7AAA9464EAE5F0D550D,
	U3COnTextReceivedU3Ed__47_SetStateMachine_m75171F6AEA5BEBA0568157A3D74819D9786CEE73,
	U3CSendAsyncU3Ed__50_MoveNext_mB7D9B2014E46114ED396AAFFD656146A36FE24A1,
	U3CSendAsyncU3Ed__50_SetStateMachine_mECC493E4D4C5433A4B2620F29E2BF856DC2FAFFB,
	U3CDisconnectAsyncU3Ed__51_MoveNext_m904D5D3C8F3E813544A005F3031D42F2196E4491,
	U3CDisconnectAsyncU3Ed__51_SetStateMachine_mE795A2BF18086096E7B38E6CADA5BDBFBFC96D30,
	U3CSendAsyncU3Ed__52_MoveNext_m4A032EFFD4227BD48CD62F2A2CE7DC04DD3D5648,
	U3CSendAsyncU3Ed__52_SetStateMachine_mC80CFA8D89B2E8846F2F18BBC81A9ED45370A481,
	U3CSendAsyncU3Ed__53_MoveNext_m85EAF9F02BE4959E62FEDD20E41521FE19525555,
	U3CSendAsyncU3Ed__53_SetStateMachine_mC93F378EE76C870A8B0DDD34A803ADE568CBAB48,
	WebSocketTransport__ctor_mD059A8D285789D308453DD7316C62B9671665508,
	WebSocketTransport_get_ReceiveChunkSize_m7423C6ABF999816047CBBE5091421C083583BB53,
	WebSocketTransport_set_ReceiveChunkSize_m4039BD765C6687FA791AA26D54BAA52BD8D4B39E,
	WebSocketTransport_get_SendChunkSize_mF03112851A9A4B9EC8B53FA9DCB4C01177AC138E,
	WebSocketTransport_set_SendChunkSize_mDF22C5AA6380D76D2C03F8F5B1BE613F56657F7C,
	WebSocketTransport_get_ConnectionTimeout_m805685CAF6A21DAA5B4581E9115BF5DF7A638A6F,
	WebSocketTransport_set_ConnectionTimeout_m9B7C2A72B06C3EB53EFF499EF49D757505461D99,
	WebSocketTransport_set_ReceiveWait_mD2A1F60C8F17900ACF6B9DC77B2398894772876A,
	WebSocketTransport_get_OnTextReceived_m80A4849545A1B10B03397447C624C2B69B603859,
	WebSocketTransport_set_OnTextReceived_mDA981BF42B62CB4AE52C3011F996031E36EFA51A,
	WebSocketTransport_get_OnBinaryReceived_m12078E3CE2369D226DB519AB8EC7858D912A7308,
	WebSocketTransport_set_OnBinaryReceived_mD8C6635BB91FD7D6158BA90281356FF2BEA0B260,
	WebSocketTransport_get_OnAborted_mF078CCDAAF48AE1F1E12275CF8E213756FF093FF,
	WebSocketTransport_set_OnAborted_mBB91B48B2DF92743FCCA2255932BE9D67E55AD81,
	WebSocketTransport_ConnectAsync_mDD02270F7B94ED91A84ADAE0775BA32698BE7EDF,
	WebSocketTransport_SendAsync_mCBCA4E1F380CA76309F844B9D7BD356B37E6B636,
	WebSocketTransport_SendAsync_mE057E9C97004675A5303352DB450032FDDBA19D6,
	WebSocketTransport_SendAsync_mECEA08E3E7437DAD3170C2BCA9C51AD5F07EF349,
	WebSocketTransport_ListenAsync_mB0C9B98540C9A632DEA0267D818FD1EEA03F6E7B,
	WebSocketTransport_Dispose_m9ABD975D34150BA3230326996DB8DC2352DCC623,
	U3CConnectAsyncU3Ed__32_MoveNext_mA2D1147A27E11555D5BA80C46463376DD4D95D28,
	U3CConnectAsyncU3Ed__32_SetStateMachine_mC9095D635C19D612D42B3374031B330C1DAE74A9,
	U3CSendAsyncU3Ed__34_MoveNext_m0233519070E68092BB9317B35727B2C6E908E7C0,
	U3CSendAsyncU3Ed__34_SetStateMachine_mFB8DA5CD9392EE2E24757CB72F3F922B3843E502,
	U3CSendAsyncU3Ed__35_MoveNext_mE7D1F3F54C7EC423D212B5A446E69D11BB4EFBA2,
	U3CSendAsyncU3Ed__35_SetStateMachine_m03876D9EA87154470B66213C6C917DA9288593B3,
	U3CSendAsyncU3Ed__36_MoveNext_mFF1EFFB259EDA3C4ECCE0B41B72457AA284AA362,
	U3CSendAsyncU3Ed__36_SetStateMachine_m4A84217F6A869CA3E24C7909B818F4B37FA67C7E,
	U3CListenAsyncU3Ed__37_MoveNext_m37BB23F759DA7952F83681F5902D7E1AE0BA8D69,
	U3CListenAsyncU3Ed__37_SetStateMachine_mCB6D898F64A30EBAFEE9F5D18346C8C403B85460,
	BinaryMessage_get_Type_mC2B43D014799852655C16BD2E958AA3A1EA4DA4A,
	BinaryMessage_get_Namespace_mAA8F0EDECFAF3E10B0DBECF08F653C1BFDBE8C39,
	BinaryMessage_set_Namespace_m646059378AD2E597BEF2CD530131C6A3FB7C5367,
	BinaryMessage_get_Event_mA5C2557061343D628095E49FC95B01F89B76A3A5,
	BinaryMessage_set_Event_m8029DA632FCD8CE8FA190756ED4B6C0EEA6DEF0A,
	BinaryMessage_get_Id_m97035E0F4A2949EA52F3F073D456BF9488D29E33,
	BinaryMessage_set_Id_m22A1C8842F872CAC13EC07D4D7BE5D5F03A11555,
	BinaryMessage_get_JsonElements_m22A4CAD588EB7F51469D0EC607EC1517FB9242B9,
	BinaryMessage_set_JsonElements_mE1BEFAD769DD0E5A18F7D4A792A8BBE6A308B944,
	BinaryMessage_get_Json_m7BDB54A72D4B384A282EEFEE3FC20ADD8E386006,
	BinaryMessage_set_Json_mD9768CB381F44A08DF584D94BABC0F2AE54845E5,
	BinaryMessage_get_BinaryCount_mCAD5E1F353E75D1CD65F7E56171D7308456098C5,
	BinaryMessage_set_BinaryCount_mFB6806B814207CD244A17A422839451BA3EEE00D,
	BinaryMessage_set_Eio_mA5F0D55BB0AA5B36FC12DC93555DBB37E61BC798,
	BinaryMessage_set_Protocol_m7D775B3886C401B5485E9E00C0B1588FD37D53DD,
	BinaryMessage_get_OutgoingBytes_m9AF39913F8652D5805D6DF76A6BB5DDA94865B49,
	BinaryMessage_set_OutgoingBytes_m0AEF3EF6974784DB66FF5328C4810A0C4FBDBEAB,
	BinaryMessage_get_IncomingBytes_m6176E430798B6B981ACF0EDE3E8B46186906F8CE,
	BinaryMessage_set_IncomingBytes_m1092FBA649646F7815529C8BAC29791429906284,
	BinaryMessage_Read_m57D8645A684D3A0C652833561BFF90EE9D441823,
	BinaryMessage_Write_m299FBD5EF6ACCEE855CBB329EAFE6ACFDC7052EE,
	BinaryMessage__ctor_m6AF1DBE76C382D67D35B42E1B3C2B635495ED4F7,
	ClientAckMessage_get_Type_mDD77EDF5269C88D48BEB39F6CD46F57524D4D39C,
	ClientAckMessage_get_Namespace_m63EC8D1F5F4A3FD033A21A9A7736E1BBCB8E6BB6,
	ClientAckMessage_set_Namespace_m0E308A032CFAE0C1E9A7AECEFEA8F3122B1EB3E7,
	ClientAckMessage_get_Event_m4E1029DC9E74D4791827C070DF1BE7229E74564C,
	ClientAckMessage_get_JsonElements_m4EBE99C0156E918C12946B8801BBF4532E13F2BF,
	ClientAckMessage_set_JsonElements_m3A410CA3471CD989D75DFFAB35A79858A5953A88,
	ClientAckMessage_get_Json_m1D2D03D72410C66DA620FA77B772E0661D4EADFD,
	ClientAckMessage_get_Id_m7B65941B3858ACCB71050DB455C09B83710CD4B2,
	ClientAckMessage_set_Id_m552EB2A26BC0AE3A67177B0D3DBE174F690AF824,
	ClientAckMessage_get_OutgoingBytes_m17FAB47D528E3B2C40E407B974F77F121E5902B7,
	ClientAckMessage_get_IncomingBytes_mD5D10FF092216467AC85772D7DF2C89158499DAD,
	ClientAckMessage_set_IncomingBytes_m61174B457A26958E141D260B3BF1508A146BD976,
	ClientAckMessage_get_BinaryCount_mCB5AE12314E9677CA289F008B341891041778DAE,
	ClientAckMessage_set_Eio_mB675DDC19D4BC87F0983349E6C1DBF3E84FBC843,
	ClientAckMessage_set_Protocol_mE2EC5B7E221B3493A7063810BBBD538FFF724069,
	ClientAckMessage_Read_mDBD41122F7002925C0CA930857DC08D57FA95DBC,
	ClientAckMessage_Write_m0BDFF03EE260CA0393BC4B8D4875094363F610F6,
	ClientAckMessage__ctor_m4A32FBBD2D1664D1D007755A52D553193FC21BFD,
	ClientBinaryAckMessage_get_Type_m65CBCC265E17593D36AE1DDFA3E21A1F9CC30CF7,
	ClientBinaryAckMessage_get_Namespace_mF3061BBC4D59B4A4647697ACBC0CC30000E5A559,
	ClientBinaryAckMessage_set_Namespace_mC27A209BE8B51508A386C97A6CAA58EE2F695D53,
	ClientBinaryAckMessage_get_Event_m51C6D5CD0A4A594CD8485498498B23F344D71EC9,
	ClientBinaryAckMessage_get_JsonElements_m89379F5E3191C45DD20BEC5B41486663A8247FE9,
	ClientBinaryAckMessage_set_JsonElements_mAF9ADD78439515C24E55DF9D654A69BA97F51BB8,
	ClientBinaryAckMessage_get_Json_mC12CDDE2EC5E404E8869AA0C4B7A2B5705B0A468,
	ClientBinaryAckMessage_get_Id_m5FDB4ECB225E66B9A372CE532ED6A0573DF2B626,
	ClientBinaryAckMessage_set_Id_mBAD409A433D3E03028AB31C3B7D6946D1B853F4D,
	ClientBinaryAckMessage_get_BinaryCount_m7FAC63EADBBE1A1119FA3D02AA8413EA9D38B7A0,
	ClientBinaryAckMessage_set_BinaryCount_m8DE802695E94A4DC67C1EDED8A58E68443432575,
	ClientBinaryAckMessage_set_Eio_mC80168F7A70621DA9819ABD2A3BD062812BA3FA6,
	ClientBinaryAckMessage_set_Protocol_mDCC45C47884C9084000C140F6543A0187DAF426E,
	ClientBinaryAckMessage_get_OutgoingBytes_mB1E8E08B93D7E4D6255E215C0DB3B72D6B692A06,
	ClientBinaryAckMessage_get_IncomingBytes_mA30B44355FB52052C70AC0987A2BA0C7AA2B96BF,
	ClientBinaryAckMessage_set_IncomingBytes_mE598B123B5E9B1A32DB1E75DB2EDA6647AB3159C,
	ClientBinaryAckMessage_Read_m14B1634CA311FBFAA58272DDAB681DBA3BB69D1D,
	ClientBinaryAckMessage_Write_mFB5927BEE86A0920F4E892AB4052983D9D0DB434,
	ClientBinaryAckMessage__ctor_mB34AEA49FF827B404979FC67D178CC2947802C66,
	ConnectedMessage_get_Type_m9B681280282AD53FCC2512C0CF5790C5D3709672,
	ConnectedMessage_get_Namespace_mBB9748CE4C8C74F0C4A5D149A7BC761709665E95,
	ConnectedMessage_set_Namespace_mA7FDE6E95A56A0C4DDCDC02127D102B482D3D6FA,
	ConnectedMessage_get_Sid_m4D121694E0C88D04E5A99433C953CE1585B2BF29,
	ConnectedMessage_set_Sid_m80379E0AC6A06BB24F3FEC26BB9563C8CD59E93C,
	ConnectedMessage_get_OutgoingBytes_mFAC5CCE676B708DFE7EFCA8B6845E0B55CA34704,
	ConnectedMessage_get_IncomingBytes_mCE4223CDA74C273D269F1E6D04C6B3E5BCDB108F,
	ConnectedMessage_set_IncomingBytes_mB3582A4D805FFB5E600AAE3E113534B5C2BA28E8,
	ConnectedMessage_get_BinaryCount_m1C3D0F041C7BCE37A2007B2CAFD89FE4D5726946,
	ConnectedMessage_get_Eio_mE24F9D9F65069643E7A512BB0611448FE892F0C2,
	ConnectedMessage_set_Eio_mCA4D23CABE3F03CA5F2D48A4929155903607621E,
	ConnectedMessage_set_Protocol_m05896E4D86A5D4F7D4DBFFC6CFF73482322B0654,
	ConnectedMessage_get_Query_m45D8A90C909B5954183B9AACBBFD2908F17C1692,
	ConnectedMessage_set_Query_mDDE08088D7D65E1896B3DDF467CF66D4CBF9C1B2,
	ConnectedMessage_Read_mBBEE6EF47B68F1F2AD4E60AD8E8E34A998079A3A,
	ConnectedMessage_Write_m76DA157CF9C30F591D54A2012B7CE33227ACDE33,
	ConnectedMessage_Eio4Read_mA69A245C3830F1F30751BBA5C59A53522FB35904,
	ConnectedMessage_Eio4Write_mCAE5F367C6CF6A1271AC781EABCCA45B9A6FC3A8,
	ConnectedMessage_Eio3Read_m2FFBB30649728AE101642441B3974D5ADAB92A4F,
	ConnectedMessage_Eio3Write_m7071001C216992734A9B19E051EAE42F5366A255,
	ConnectedMessage__ctor_m7B00E3238A54C7A1D615CD24744F8BD8FC0326E5,
	DisconnectedMessage_get_Type_m89E627971B8E8039DBA97989D989FC3695D0E52F,
	DisconnectedMessage_get_Namespace_mBC654DF14D562AACD4678E23EFF3A5AC69640D59,
	DisconnectedMessage_set_Namespace_m137124B9A6AD089CD1819267FC45348711C220C4,
	DisconnectedMessage_get_OutgoingBytes_m4B4FE93340DDDF8301A0A6CB5ED17C8C5470ED17,
	DisconnectedMessage_get_IncomingBytes_mC6CF82CBBD5D15443DE29F95E76844F09CE831AC,
	DisconnectedMessage_set_IncomingBytes_m7BBCB930B5C7FD1A9EA060E32B6FF5F54E1CC1C7,
	DisconnectedMessage_get_BinaryCount_m87D1D38C6DBAC669C1714C9E08CC3E3D020F7C04,
	DisconnectedMessage_set_Eio_m7D809F9D4C4F6B3DA317012FD4915F95113F3F90,
	DisconnectedMessage_set_Protocol_mAC0B93C5BC99FDCCA8917275B3FAD679CB40C0EE,
	DisconnectedMessage_Read_mF220177ADB484FA3886EA4C087768CAFA69115BF,
	DisconnectedMessage_Write_mD6B38FDC0ABDCE0BD00E789D443531EB08BDC04D,
	DisconnectedMessage__ctor_mF4D04405FF5F17A52A6CBA58B034862A10633CD5,
	ErrorMessage_get_Type_m97A024A71590D58A53650049C282742A277D7D2D,
	ErrorMessage_get_Message_m2FA4EC941DBB4D885911E9B6A08206CCC6EAE05C,
	ErrorMessage_set_Message_m1C676E1D517B0B82E65977F394E66A0C23BDA2FD,
	ErrorMessage_set_Namespace_m1621D5622A176D55983D56ACBF3BDF9225391D43,
	ErrorMessage_get_OutgoingBytes_mB7064FB185F60EF9688241881E7A548EA4FEE8B8,
	ErrorMessage_get_IncomingBytes_mD9E5D721243093414A3BC2A673642D1D3F3930FB,
	ErrorMessage_set_IncomingBytes_mF36F79779BC64A1B7AE435DFFA050428A35F689A,
	ErrorMessage_get_BinaryCount_mD3A9C112BB333B628E3F160DCD82B651971A9DC3,
	ErrorMessage_get_Eio_mEA9E313693DB15A377C002395ECFA1AF273BC29E,
	ErrorMessage_set_Eio_m9A26978230A37C6746E2EFA8BD7CBD28E45B68E7,
	ErrorMessage_set_Protocol_mD3210142031FD98E9A9080037014A9DC9AD2D325,
	ErrorMessage_Read_m8F4C7D80DBE856CCA850144B57A2A0C64B9BE1A0,
	ErrorMessage_Write_m14360C4B068946B02E2BB18288421228997BB448,
	ErrorMessage__ctor_mF15E287DE5BE37E637906637A71A912A01A27104,
	EventMessage_get_Type_m1CFC83E740328B4218A1BE2D820284B80A253664,
	EventMessage_get_Namespace_mA2473D71D774C869AB286DFA44A5BFAD6AAE3D97,
	EventMessage_set_Namespace_m262295490C730CAC637DF2A0ED3E5222171253D4,
	EventMessage_get_Event_m1B6110DACCD1175CCA546189CA5BC6D31B852E99,
	EventMessage_set_Event_mFD9DA13A75C0D0C1F4926558FA4D78F808F76C39,
	EventMessage_get_Id_m67179AADC6C01AE12374E1940C861770727F9ED5,
	EventMessage_set_Id_m806A84CA037CCBD53F96C5C31632AA70F152C1E1,
	EventMessage_get_JsonElements_m5F76EDD056F8AFD32C049EF08816ACAE644F5017,
	EventMessage_set_JsonElements_mF40A41BCE4C89E3EA46B74519DE5EE213355D03D,
	EventMessage_get_Json_mCDB73487936C56C04AE0AAA0855B57A25F68BF41,
	EventMessage_set_Json_m4386F4B466346D2538B8AF0696D729521C9D00D6,
	EventMessage_get_OutgoingBytes_mEE2B105F54ECACF5F024B33B6DE55FBB8B4A2C05,
	EventMessage_get_IncomingBytes_mA4330EBE622AF968FD4C3863787567891BADA0FD,
	EventMessage_set_IncomingBytes_m3A6B20C6F5C764CE1974A6DB4C6E8C2EF5D5AE34,
	EventMessage_get_BinaryCount_m77F298305163FA7315EEFEF117D8A9FBEF798B06,
	EventMessage_set_Eio_m695EDB9FFB1E5724ED628F6827E2E9D60DA40943,
	EventMessage_set_Protocol_m714D79DD63FECC93AC5CF26366C0FCD3CC61845D,
	EventMessage_Read_mF26944D1C918BA93F54EE56A8F343BC881E17FFA,
	EventMessage_Write_m020DD8C979F3049302769D5785E19EDF4C9FB124,
	EventMessage__ctor_m1A9EC3C6252BBBEA87B8E2728FF81118E77FB9BF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageFactory_CreateMessage_m41EF5FE83024FC9910A15146F92B7B40F4FBAA3B,
	MessageFactory_CreateMessage_m02159BEC7B481B1AE143D788B4662C7A82D54907,
	OpenedMessage_get_Type_m9DA1E36A0ECA24E2E7F70992DFB31393061C4FE0,
	OpenedMessage_get_Sid_m1C2DFD9ABF2841ECC72816888C39F53B90A123BB,
	OpenedMessage_set_Sid_m91D157ACA4F17CE6F04F912C5E6576EFA7ED1E3E,
	OpenedMessage_get_Upgrades_mF713B2623F775FA9E340303DB033C263E826C9C1,
	OpenedMessage_set_Upgrades_m57F2B66A6F46EFAEFE0DB04833C058834D058076,
	OpenedMessage_get_PingInterval_m6BAFB5A01580F69D4BDFABBBB2453F38DC6B0774,
	OpenedMessage_set_PingInterval_m6F90759D62F4B9F106213211CBEB729BD77516FD,
	OpenedMessage_set_PingTimeout_mA06D177C1911170D70B6C871074BB8751A257F1C,
	OpenedMessage_get_OutgoingBytes_m84828C6286305F9254089FE27036FD61DED71E07,
	OpenedMessage_get_IncomingBytes_mEBB3DA727A3E6E1B5102B2BF2C79839E5452A6E4,
	OpenedMessage_set_IncomingBytes_m55D0D0FBA7764E86BFDDD74A6B5DB3642258EC4F,
	OpenedMessage_get_BinaryCount_m3B7FEAE705FFBE0FB050E7DD20FF4173D0BDD811,
	OpenedMessage_set_Eio_m27947BFE1007703F0AAF867D632D461E99252612,
	OpenedMessage_set_Protocol_m896FE4E58F36070B60C381736E34B8385D79BD56,
	OpenedMessage_Read_m7FE67F089124FFF7955F74D4423090CE6D5672D2,
	OpenedMessage_Write_m2C60B177CA1B9546A1E15459DD62E52ADD0EF602,
	OpenedMessage__ctor_mF01ECD7347F4407A810AD624450A093367B6ABCA,
	PingMessage_get_Type_m5B2D9C8F58502C86D75052453D4B2BBF63E8EFDE,
	PingMessage_get_OutgoingBytes_mA33B556D75B63546FE1680B93906C4DDACC16F54,
	PingMessage_get_IncomingBytes_mAB4CB776A307E0C1627ECC173704ACB7DEB005CB,
	PingMessage_set_IncomingBytes_m02799D45BFCC060407A5DADA8EE815B3BDB7E597,
	PingMessage_get_BinaryCount_m5A1C66215FB119E85F93E4F59D34C1DAD2F6CD33,
	PingMessage_set_Eio_mB6C5A7B749780F1CB30A671BCE98CC7BF033A26A,
	PingMessage_set_Protocol_mE38DF0C7211E1389CE1D252151F33FD54965404F,
	PingMessage_Read_mD2F19855C1BAE5FF5011F1E0BE89BE8D27C6E97D,
	PingMessage_Write_m841E392144E32D096D95A3C3446D4D61437F14E9,
	PingMessage__ctor_m1E228E96BA772528D21CE3AEA8E40853623EF2C0,
	PongMessage_get_Type_m7FF6CDF2744BEF21BDC7F7603B0A792EBE68655D,
	PongMessage_get_OutgoingBytes_m92CBE117D20EFD12A4A8674566CE185D76B16690,
	PongMessage_get_IncomingBytes_mC2E61D002A47797E8379978F60B8FA39EB89E072,
	PongMessage_set_IncomingBytes_m9B6B09A1AA7AB28699B3C0AA5E1EFC4A5973711D,
	PongMessage_get_BinaryCount_m8568D4D84A7BC394DDE4CDA945DCB1A1BB9B1695,
	PongMessage_set_Eio_m5ADF0E510ECCBBFBF803155E9392544C5D8F9CEF,
	PongMessage_set_Protocol_m19FE10462749E7A401EAA6FFAD950ACB492E6C7F,
	PongMessage_get_Duration_m678F36A9284497BF1D8B738AE321D6C0EEEBA9F8,
	PongMessage_set_Duration_m3F8BA41944161F752771D65A9AD307460068B6F0,
	PongMessage_Read_mACA9466D7A3E56B5D6568D98358095B816744E88,
	PongMessage_Write_m118684351CE5E63F446D3F57A326297C366B8371,
	PongMessage__ctor_m3493A77F2AD1E2BCF52F768C9A33A08BAD9658ED,
	ByteArrayConverter__ctor_m42010C9AC114C4E5A81A34005C410971137EB52F,
	ByteArrayConverter_get_Bytes_mCF080CE5AFFFCB4649F40F3E469A297420BAFC28,
	ByteArrayConverter_Read_m6CA157B19F9AEBF3D846326A9F7434C80EC21C44,
	ByteArrayConverter_Write_mC21A0EE1A5E51C098CF0C4DA1FDBE4E52A4BECA4,
	NULL,
	NULL,
	JsonSerializeResult_get_Json_mB0DB437258A308B7C3631B24D12457F813CE5000,
	JsonSerializeResult_set_Json_m00057D5060370A218B3659E161AB15FF0E0AFAC4,
	JsonSerializeResult_get_Bytes_mB4255CBA3832D483011FE9A4462BDCE1FE65D052,
	JsonSerializeResult_set_Bytes_mB09A845EA53D94853444F111F8A114E041FA0602,
	JsonSerializeResult__ctor_m84F1048A19CF55A7525EA303001987CCBD46151C,
	SystemTextJsonSerializer_Serialize_mBD99A40D03904E43867BB5CEF5A8918A31DAEB0A,
	NULL,
	SystemTextJsonSerializer_GetOptions_m62A65B992250F4C3279903EB04ECA3829E4B60AB,
	SystemTextJsonSerializer_CreateOptions_m284D599E8AA27E39CBDB33800DCF85488E6DFC61,
	SystemTextJsonSerializer_get_OptionsProvider_m78ECE00826C3253812E11D9BBBE1CB0374896AD5,
	SystemTextJsonSerializer__ctor_m62045199E591D78BEE032ED7F1889F8FBB8D6EFE,
};
extern void U3CConnectAsyncU3Ed__81_MoveNext_m03FD8CC7F89AB39CE4D3F7EE5C485304C30980CE_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__81_SetStateMachine_m1ECCE93BD645592AA838CC0DAA79961A910639F5_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__101_MoveNext_m61BDCFC0DCBDD98BFB903E8F98FC837AB61A6724_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__101_SetStateMachine_m82FDB3356A487BDFBB32BE08AC8B04DA1A1C1C67_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__102_MoveNext_m8B7ABDB6694D54F5AE0851E58272282C9D851A19_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__102_SetStateMachine_mFE7510E343DBC01A0B53EBC8EED3EF255919C940_AdjustorThunk (void);
extern void U3CInvokeDisconnectU3Ed__105_MoveNext_mE6FC3092DC881E8E13CF6A98AC1498889AD2E5D0_AdjustorThunk (void);
extern void U3CInvokeDisconnectU3Ed__105_SetStateMachine_m04BF0AC0D285F30E692F1EB9064013F6F8D34ECA_AdjustorThunk (void);
extern void U3CCloseAsyncU3Ed__9_MoveNext_mE4CDE4BD7BDA53916E8CE90BE16F8A514776244E_AdjustorThunk (void);
extern void U3CCloseAsyncU3Ed__9_SetStateMachine_mD148CE0C89031C23EA867D2FF34BB81F6FBB8984_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__10_MoveNext_m6A62A732246F8E9D22ED0AEABB9D789F4875B321_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__10_SetStateMachine_m437EFC3CEDEB097994C3197EFE243E90FB0F0E53_AdjustorThunk (void);
extern void U3CReceiveAsyncU3Ed__11_MoveNext_m3566D421D11AB075C58A344D833590990D9554D1_AdjustorThunk (void);
extern void U3CReceiveAsyncU3Ed__11_SetStateMachine_m66A5AF498F96EFF912632912CD93F2D36043A027_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__12_MoveNext_mE78EE0C77C86D6575E6BE71122FBE12BE16A8864_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__12_SetStateMachine_m64CD752CE62EA1350A8057CFA24ADAAF7C3C8FD5_AdjustorThunk (void);
extern void U3CGetAsyncU3Ed__12_MoveNext_mC641C4B62670248D906EF7514054F66E7E63E4A1_AdjustorThunk (void);
extern void U3CGetAsyncU3Ed__12_SetStateMachine_mCF888DD1BB02D2EB5E362AC40CEE07D81E519E7A_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__13_MoveNext_m4E93C60052E04660F288B2C5F2EA7CD622EFCF1D_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__13_SetStateMachine_m024A5A18084436D890DAB736AE15C8572355A482_AdjustorThunk (void);
extern void U3CPostAsyncU3Ed__14_MoveNext_mDA369269CBD65510DD420EADE79EBFD47AA18036_AdjustorThunk (void);
extern void U3CPostAsyncU3Ed__14_SetStateMachine_m3BE227D571DCB3C1149853D5363EDFB3012D06D1_AdjustorThunk (void);
extern void U3CPostAsyncU3Ed__15_MoveNext_m16D7E48B7E366F62287D117CC9D998833129C438_AdjustorThunk (void);
extern void U3CPostAsyncU3Ed__15_SetStateMachine_mC519448B2884FD4C0A877B254582828AC538BCB9_AdjustorThunk (void);
extern void U3CProduceMessageAsyncU3Ed__17_MoveNext_m5BDBB12B463982C8D3DCAF88662843960361D635_AdjustorThunk (void);
extern void U3CProduceMessageAsyncU3Ed__17_SetStateMachine_m85ADDF619E98B828A3AD77EFE309E8EE0E6DCC5A_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__41_MoveNext_mB584FA2DE02307EDE154C27292CE30B07E06DEAA_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__41_SetStateMachine_mABAF1145E76B3C806CD1DC24224EE718C0FE4526_AdjustorThunk (void);
extern void U3CConnectByWebsocketAsyncU3Ed__42_MoveNext_mB24CC63EF53BB731A142C8488D73562313F8920C_AdjustorThunk (void);
extern void U3CConnectByWebsocketAsyncU3Ed__42_SetStateMachine_m3294DBB8241985C904860D8C910296D0AE1EF4BA_AdjustorThunk (void);
extern void U3CConnectByPollingAsyncU3Ed__43_MoveNext_m65122FB1A8D148F0F74455C893EC8303878782DD_AdjustorThunk (void);
extern void U3CConnectByPollingAsyncU3Ed__43_SetStateMachine_m37FECED5E2C8F12E3FC6308915571A7789E48339_AdjustorThunk (void);
extern void U3CU3CStartPollingU3Eb__0U3Ed_MoveNext_m77A6B657D52AD87B8870737A49D46476CD153A0B_AdjustorThunk (void);
extern void U3CU3CStartPollingU3Eb__0U3Ed_SetStateMachine_mE5328B25B4712C524080DCAE323D9FD3C4263794_AdjustorThunk (void);
extern void U3CU3CStartPingU3Eb__0U3Ed_MoveNext_mD8B3D0A6E1B187A5019F1F9456A3E285ABC7BDD6_AdjustorThunk (void);
extern void U3CU3CStartPingU3Eb__0U3Ed_SetStateMachine_mE910C8499A852C5A9AE83B20C285F1B5226A63A2_AdjustorThunk (void);
extern void U3COnOpenedU3Ed__46_MoveNext_m437285B06D1F08C0CA03A535D157F8FD3ECCDAAB_AdjustorThunk (void);
extern void U3COnOpenedU3Ed__46_SetStateMachine_mAED54523AB8B9D9861736CCEA7A47D1EE7022413_AdjustorThunk (void);
extern void U3COnTextReceivedU3Ed__47_MoveNext_mC18AD7621DFCA76DA92EF7AAA9464EAE5F0D550D_AdjustorThunk (void);
extern void U3COnTextReceivedU3Ed__47_SetStateMachine_m75171F6AEA5BEBA0568157A3D74819D9786CEE73_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__50_MoveNext_mB7D9B2014E46114ED396AAFFD656146A36FE24A1_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__50_SetStateMachine_mECC493E4D4C5433A4B2620F29E2BF856DC2FAFFB_AdjustorThunk (void);
extern void U3CDisconnectAsyncU3Ed__51_MoveNext_m904D5D3C8F3E813544A005F3031D42F2196E4491_AdjustorThunk (void);
extern void U3CDisconnectAsyncU3Ed__51_SetStateMachine_mE795A2BF18086096E7B38E6CADA5BDBFBFC96D30_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__52_MoveNext_m4A032EFFD4227BD48CD62F2A2CE7DC04DD3D5648_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__52_SetStateMachine_mC80CFA8D89B2E8846F2F18BBC81A9ED45370A481_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__53_MoveNext_m85EAF9F02BE4959E62FEDD20E41521FE19525555_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__53_SetStateMachine_mC93F378EE76C870A8B0DDD34A803ADE568CBAB48_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__32_MoveNext_mA2D1147A27E11555D5BA80C46463376DD4D95D28_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__32_SetStateMachine_mC9095D635C19D612D42B3374031B330C1DAE74A9_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__34_MoveNext_m0233519070E68092BB9317B35727B2C6E908E7C0_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__34_SetStateMachine_mFB8DA5CD9392EE2E24757CB72F3F922B3843E502_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__35_MoveNext_mE7D1F3F54C7EC423D212B5A446E69D11BB4EFBA2_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__35_SetStateMachine_m03876D9EA87154470B66213C6C917DA9288593B3_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__36_MoveNext_mFF1EFFB259EDA3C4ECCE0B41B72457AA284AA362_AdjustorThunk (void);
extern void U3CSendAsyncU3Ed__36_SetStateMachine_m4A84217F6A869CA3E24C7909B818F4B37FA67C7E_AdjustorThunk (void);
extern void U3CListenAsyncU3Ed__37_MoveNext_m37BB23F759DA7952F83681F5902D7E1AE0BA8D69_AdjustorThunk (void);
extern void U3CListenAsyncU3Ed__37_SetStateMachine_mCB6D898F64A30EBAFEE9F5D18346C8C403B85460_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[58] = 
{
	{ 0x06000035, U3CConnectAsyncU3Ed__81_MoveNext_m03FD8CC7F89AB39CE4D3F7EE5C485304C30980CE_AdjustorThunk },
	{ 0x06000036, U3CConnectAsyncU3Ed__81_SetStateMachine_m1ECCE93BD645592AA838CC0DAA79961A910639F5_AdjustorThunk },
	{ 0x06000037, U3CEmitAsyncU3Ed__101_MoveNext_m61BDCFC0DCBDD98BFB903E8F98FC837AB61A6724_AdjustorThunk },
	{ 0x06000038, U3CEmitAsyncU3Ed__101_SetStateMachine_m82FDB3356A487BDFBB32BE08AC8B04DA1A1C1C67_AdjustorThunk },
	{ 0x06000039, U3CEmitAsyncU3Ed__102_MoveNext_m8B7ABDB6694D54F5AE0851E58272282C9D851A19_AdjustorThunk },
	{ 0x0600003A, U3CEmitAsyncU3Ed__102_SetStateMachine_mFE7510E343DBC01A0B53EBC8EED3EF255919C940_AdjustorThunk },
	{ 0x0600003B, U3CInvokeDisconnectU3Ed__105_MoveNext_mE6FC3092DC881E8E13CF6A98AC1498889AD2E5D0_AdjustorThunk },
	{ 0x0600003C, U3CInvokeDisconnectU3Ed__105_SetStateMachine_m04BF0AC0D285F30E692F1EB9064013F6F8D34ECA_AdjustorThunk },
	{ 0x06000065, U3CCloseAsyncU3Ed__9_MoveNext_mE4CDE4BD7BDA53916E8CE90BE16F8A514776244E_AdjustorThunk },
	{ 0x06000066, U3CCloseAsyncU3Ed__9_SetStateMachine_mD148CE0C89031C23EA867D2FF34BB81F6FBB8984_AdjustorThunk },
	{ 0x06000067, U3CConnectAsyncU3Ed__10_MoveNext_m6A62A732246F8E9D22ED0AEABB9D789F4875B321_AdjustorThunk },
	{ 0x06000068, U3CConnectAsyncU3Ed__10_SetStateMachine_m437EFC3CEDEB097994C3197EFE243E90FB0F0E53_AdjustorThunk },
	{ 0x06000069, U3CReceiveAsyncU3Ed__11_MoveNext_m3566D421D11AB075C58A344D833590990D9554D1_AdjustorThunk },
	{ 0x0600006A, U3CReceiveAsyncU3Ed__11_SetStateMachine_m66A5AF498F96EFF912632912CD93F2D36043A027_AdjustorThunk },
	{ 0x0600006B, U3CSendAsyncU3Ed__12_MoveNext_mE78EE0C77C86D6575E6BE71122FBE12BE16A8864_AdjustorThunk },
	{ 0x0600006C, U3CSendAsyncU3Ed__12_SetStateMachine_m64CD752CE62EA1350A8057CFA24ADAAF7C3C8FD5_AdjustorThunk },
	{ 0x0600007B, U3CGetAsyncU3Ed__12_MoveNext_mC641C4B62670248D906EF7514054F66E7E63E4A1_AdjustorThunk },
	{ 0x0600007C, U3CGetAsyncU3Ed__12_SetStateMachine_mCF888DD1BB02D2EB5E362AC40CEE07D81E519E7A_AdjustorThunk },
	{ 0x0600007D, U3CSendAsyncU3Ed__13_MoveNext_m4E93C60052E04660F288B2C5F2EA7CD622EFCF1D_AdjustorThunk },
	{ 0x0600007E, U3CSendAsyncU3Ed__13_SetStateMachine_m024A5A18084436D890DAB736AE15C8572355A482_AdjustorThunk },
	{ 0x0600007F, U3CPostAsyncU3Ed__14_MoveNext_mDA369269CBD65510DD420EADE79EBFD47AA18036_AdjustorThunk },
	{ 0x06000080, U3CPostAsyncU3Ed__14_SetStateMachine_m3BE227D571DCB3C1149853D5363EDFB3012D06D1_AdjustorThunk },
	{ 0x06000084, U3CPostAsyncU3Ed__15_MoveNext_m16D7E48B7E366F62287D117CC9D998833129C438_AdjustorThunk },
	{ 0x06000085, U3CPostAsyncU3Ed__15_SetStateMachine_mC519448B2884FD4C0A877B254582828AC538BCB9_AdjustorThunk },
	{ 0x06000086, U3CProduceMessageAsyncU3Ed__17_MoveNext_m5BDBB12B463982C8D3DCAF88662843960361D635_AdjustorThunk },
	{ 0x06000087, U3CProduceMessageAsyncU3Ed__17_SetStateMachine_m85ADDF619E98B828A3AD77EFE309E8EE0E6DCC5A_AdjustorThunk },
	{ 0x060000AB, U3CConnectAsyncU3Ed__41_MoveNext_mB584FA2DE02307EDE154C27292CE30B07E06DEAA_AdjustorThunk },
	{ 0x060000AC, U3CConnectAsyncU3Ed__41_SetStateMachine_mABAF1145E76B3C806CD1DC24224EE718C0FE4526_AdjustorThunk },
	{ 0x060000AD, U3CConnectByWebsocketAsyncU3Ed__42_MoveNext_mB24CC63EF53BB731A142C8488D73562313F8920C_AdjustorThunk },
	{ 0x060000AE, U3CConnectByWebsocketAsyncU3Ed__42_SetStateMachine_m3294DBB8241985C904860D8C910296D0AE1EF4BA_AdjustorThunk },
	{ 0x060000AF, U3CConnectByPollingAsyncU3Ed__43_MoveNext_m65122FB1A8D148F0F74455C893EC8303878782DD_AdjustorThunk },
	{ 0x060000B0, U3CConnectByPollingAsyncU3Ed__43_SetStateMachine_m37FECED5E2C8F12E3FC6308915571A7789E48339_AdjustorThunk },
	{ 0x060000B3, U3CU3CStartPollingU3Eb__0U3Ed_MoveNext_m77A6B657D52AD87B8870737A49D46476CD153A0B_AdjustorThunk },
	{ 0x060000B4, U3CU3CStartPollingU3Eb__0U3Ed_SetStateMachine_mE5328B25B4712C524080DCAE323D9FD3C4263794_AdjustorThunk },
	{ 0x060000B7, U3CU3CStartPingU3Eb__0U3Ed_MoveNext_mD8B3D0A6E1B187A5019F1F9456A3E285ABC7BDD6_AdjustorThunk },
	{ 0x060000B8, U3CU3CStartPingU3Eb__0U3Ed_SetStateMachine_mE910C8499A852C5A9AE83B20C285F1B5226A63A2_AdjustorThunk },
	{ 0x060000B9, U3COnOpenedU3Ed__46_MoveNext_m437285B06D1F08C0CA03A535D157F8FD3ECCDAAB_AdjustorThunk },
	{ 0x060000BA, U3COnOpenedU3Ed__46_SetStateMachine_mAED54523AB8B9D9861736CCEA7A47D1EE7022413_AdjustorThunk },
	{ 0x060000BB, U3COnTextReceivedU3Ed__47_MoveNext_mC18AD7621DFCA76DA92EF7AAA9464EAE5F0D550D_AdjustorThunk },
	{ 0x060000BC, U3COnTextReceivedU3Ed__47_SetStateMachine_m75171F6AEA5BEBA0568157A3D74819D9786CEE73_AdjustorThunk },
	{ 0x060000BD, U3CSendAsyncU3Ed__50_MoveNext_mB7D9B2014E46114ED396AAFFD656146A36FE24A1_AdjustorThunk },
	{ 0x060000BE, U3CSendAsyncU3Ed__50_SetStateMachine_mECC493E4D4C5433A4B2620F29E2BF856DC2FAFFB_AdjustorThunk },
	{ 0x060000BF, U3CDisconnectAsyncU3Ed__51_MoveNext_m904D5D3C8F3E813544A005F3031D42F2196E4491_AdjustorThunk },
	{ 0x060000C0, U3CDisconnectAsyncU3Ed__51_SetStateMachine_mE795A2BF18086096E7B38E6CADA5BDBFBFC96D30_AdjustorThunk },
	{ 0x060000C1, U3CSendAsyncU3Ed__52_MoveNext_m4A032EFFD4227BD48CD62F2A2CE7DC04DD3D5648_AdjustorThunk },
	{ 0x060000C2, U3CSendAsyncU3Ed__52_SetStateMachine_mC80CFA8D89B2E8846F2F18BBC81A9ED45370A481_AdjustorThunk },
	{ 0x060000C3, U3CSendAsyncU3Ed__53_MoveNext_m85EAF9F02BE4959E62FEDD20E41521FE19525555_AdjustorThunk },
	{ 0x060000C4, U3CSendAsyncU3Ed__53_SetStateMachine_mC93F378EE76C870A8B0DDD34A803ADE568CBAB48_AdjustorThunk },
	{ 0x060000D9, U3CConnectAsyncU3Ed__32_MoveNext_mA2D1147A27E11555D5BA80C46463376DD4D95D28_AdjustorThunk },
	{ 0x060000DA, U3CConnectAsyncU3Ed__32_SetStateMachine_mC9095D635C19D612D42B3374031B330C1DAE74A9_AdjustorThunk },
	{ 0x060000DB, U3CSendAsyncU3Ed__34_MoveNext_m0233519070E68092BB9317B35727B2C6E908E7C0_AdjustorThunk },
	{ 0x060000DC, U3CSendAsyncU3Ed__34_SetStateMachine_mFB8DA5CD9392EE2E24757CB72F3F922B3843E502_AdjustorThunk },
	{ 0x060000DD, U3CSendAsyncU3Ed__35_MoveNext_mE7D1F3F54C7EC423D212B5A446E69D11BB4EFBA2_AdjustorThunk },
	{ 0x060000DE, U3CSendAsyncU3Ed__35_SetStateMachine_m03876D9EA87154470B66213C6C917DA9288593B3_AdjustorThunk },
	{ 0x060000DF, U3CSendAsyncU3Ed__36_MoveNext_mFF1EFFB259EDA3C4ECCE0B41B72457AA284AA362_AdjustorThunk },
	{ 0x060000E0, U3CSendAsyncU3Ed__36_SetStateMachine_m4A84217F6A869CA3E24C7909B818F4B37FA67C7E_AdjustorThunk },
	{ 0x060000E1, U3CListenAsyncU3Ed__37_MoveNext_m37BB23F759DA7952F83681F5902D7E1AE0BA8D69_AdjustorThunk },
	{ 0x060000E2, U3CListenAsyncU3Ed__37_SetStateMachine_mCB6D898F64A30EBAFEE9F5D18346C8C403B85460_AdjustorThunk },
};
static const int32_t s_InvokerIndices[419] = 
{
	3876,
	1185,
	1188,
	2015,
	2015,
	1188,
	2360,
	2015,
	2360,
	2015,
	2015,
	2360,
	2015,
	2385,
	2038,
	2341,
	1999,
	2360,
	2360,
	2015,
	2360,
	2015,
	2360,
	2015,
	2015,
	2015,
	2015,
	2015,
	2015,
	2015,
	2411,
	2411,
	2360,
	2411,
	2015,
	2015,
	2411,
	2015,
	2015,
	2015,
	2015,
	2015,
	2015,
	2411,
	1188,
	885,
	562,
	2015,
	2411,
	3876,
	2411,
	2360,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2360,
	2015,
	2400,
	2052,
	2360,
	2385,
	2038,
	2332,
	1989,
	2341,
	1999,
	2341,
	1999,
	2332,
	1989,
	2360,
	2341,
	1999,
	2341,
	1999,
	1188,
	2360,
	2360,
	1999,
	-1,
	1543,
	2360,
	181,
	181,
	2411,
	2411,
	2360,
	2341,
	555,
	883,
	864,
	313,
	1188,
	2411,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	1183,
	2360,
	2015,
	2360,
	2015,
	1575,
	883,
	883,
	567,
	567,
	1572,
	1575,
	2015,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	3876,
	2411,
	1740,
	2411,
	2015,
	2411,
	2015,
	2341,
	883,
	555,
	313,
	864,
	1188,
	709,
	2360,
	2015,
	2360,
	2015,
	2341,
	1999,
	2341,
	1999,
	2360,
	2015,
	2360,
	2015,
	2360,
	2015,
	2360,
	2360,
	2360,
	1970,
	1970,
	1575,
	2015,
	2015,
	2015,
	883,
	2360,
	883,
	883,
	2411,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2360,
	2411,
	2015,
	2411,
	2360,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	1183,
	2341,
	1999,
	2341,
	1999,
	2400,
	2052,
	2052,
	2360,
	2015,
	2360,
	2015,
	2360,
	2015,
	1575,
	883,
	555,
	883,
	2360,
	2411,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2411,
	2015,
	2341,
	2360,
	2015,
	2360,
	2015,
	2341,
	1999,
	2360,
	2015,
	2360,
	2015,
	2341,
	1999,
	1999,
	1999,
	2360,
	2015,
	2360,
	2015,
	2015,
	2360,
	2411,
	2341,
	2360,
	2015,
	2360,
	2360,
	2015,
	2360,
	2341,
	1999,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2015,
	2360,
	2411,
	2341,
	2360,
	2015,
	2360,
	2360,
	2015,
	2360,
	2341,
	1999,
	2341,
	1999,
	1999,
	1999,
	2360,
	2360,
	2015,
	2015,
	2360,
	2411,
	2341,
	2360,
	2015,
	2360,
	2015,
	2360,
	2360,
	2015,
	2341,
	2341,
	1999,
	1999,
	2360,
	2015,
	2015,
	2360,
	2015,
	2360,
	2015,
	2360,
	2411,
	2341,
	2360,
	2015,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2015,
	2360,
	2411,
	2341,
	2360,
	2015,
	2015,
	2360,
	2360,
	2015,
	2341,
	2341,
	1999,
	1999,
	2015,
	2360,
	2411,
	2341,
	2360,
	2015,
	2360,
	2015,
	2341,
	1999,
	2360,
	2015,
	2360,
	2015,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2015,
	2360,
	2411,
	2341,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2015,
	2360,
	3712,
	3293,
	2341,
	2360,
	2015,
	2360,
	2015,
	2341,
	1999,
	1999,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2015,
	2360,
	2411,
	2341,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2015,
	2360,
	2411,
	2341,
	2360,
	2360,
	2015,
	2341,
	1999,
	1999,
	2400,
	2052,
	2015,
	2360,
	2411,
	2411,
	2360,
	547,
	709,
	1575,
	-1,
	2360,
	2015,
	2360,
	2015,
	2411,
	1575,
	-1,
	2360,
	2360,
	2360,
	2411,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000056, { 0, 1 } },
	{ 0x0600019F, { 1, 1 } },
};
extern const uint32_t g_rgctx_IJsonSerializer_Deserialize_TisT_tD3531D6FFA777D2867282D9B1F7D0CE2F6877F50_mAC97BE34AB9C9521CAD4E682CD20B6D315A97318;
extern const uint32_t g_rgctx_JsonSerializer_Deserialize_TisT_tF6E38A6B7CE6BF6F8E65147A6C7A11D95DA64FF8_m230C8B3E22000DC4CE46DD7C684C6AC2F3D100EF;
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IJsonSerializer_Deserialize_TisT_tD3531D6FFA777D2867282D9B1F7D0CE2F6877F50_mAC97BE34AB9C9521CAD4E682CD20B6D315A97318 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_Deserialize_TisT_tF6E38A6B7CE6BF6F8E65147A6C7A11D95DA64FF8_m230C8B3E22000DC4CE46DD7C684C6AC2F3D100EF },
};
extern const CustomAttributesCacheGenerator g_SocketIOClient_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SocketIOClient_CodeGenModule;
const Il2CppCodeGenModule g_SocketIOClient_CodeGenModule = 
{
	"SocketIOClient.dll",
	419,
	s_methodPointers,
	58,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_SocketIOClient_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
