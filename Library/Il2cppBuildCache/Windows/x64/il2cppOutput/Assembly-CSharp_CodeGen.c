﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CarController::Start()
extern void CarController_Start_m10BA7CB090F1049BB7AC3D4C4BCFA943380B7882 (void);
// 0x00000002 System.Void CarController::Update()
extern void CarController_Update_m69DDDE13273382E3C25AD2D77EC70DFFE3A70739 (void);
// 0x00000003 System.Void CarController::FixedUpdate()
extern void CarController_FixedUpdate_m62A2DA259A4FA667F2A78CC2701E1A4601C4A4F7 (void);
// 0x00000004 System.Void CarController::.ctor()
extern void CarController__ctor_m180A0B729C25B52327B05362FB3A7AA7A53A9739 (void);
// 0x00000005 System.Void AnimationController::Start()
extern void AnimationController_Start_mC4FA2010CE819DE174F849DF8E31BD919545D818 (void);
// 0x00000006 System.Void AnimationController::Move(System.Int32,System.Int32,System.Int32)
extern void AnimationController_Move_mF7066AB49D94C7A97801448C1898BCE24A33F4BF (void);
// 0x00000007 System.Void AnimationController::Stop()
extern void AnimationController_Stop_m9C16393FC9BAD291EDBF2B93B4735509AC5E634A (void);
// 0x00000008 System.Void AnimationController::Stop(System.Int32)
extern void AnimationController_Stop_m92E70272C0ABE67FE4E343689ABF91F433760737 (void);
// 0x00000009 System.Void AnimationController::FixedUpdate()
extern void AnimationController_FixedUpdate_m62D4ABAC648A3979A13B7397D52254C8E6F6179F (void);
// 0x0000000A System.Void AnimationController::.ctor()
extern void AnimationController__ctor_mD70B26881DF564CDFD37D6E5EEABFC975DC2368C (void);
// 0x0000000B MoveWheels Arduino::get_MoveWheels()
extern void Arduino_get_MoveWheels_mD550A38043E38D061BCAC881643471BBA11D4993 (void);
// 0x0000000C System.Void Arduino::set_MoveWheels(MoveWheels)
extern void Arduino_set_MoveWheels_m425C7BDEE149210D9A4F97116F1C44AAA444FA26 (void);
// 0x0000000D System.Void Arduino::Update()
extern void Arduino_Update_mC5726F6C36A0B8A7C1FBBA4E23DFA066FA8C2888 (void);
// 0x0000000E System.Void Arduino::Run(System.String)
extern void Arduino_Run_m7CCA0C0EC351CD043B30D86A16B8FFFA30A68231 (void);
// 0x0000000F System.Collections.IEnumerator Arduino::Run()
extern void Arduino_Run_m7D6745240363F3DB699F607CF7218CA635679225 (void);
// 0x00000010 System.Void Arduino::.ctor()
extern void Arduino__ctor_m8693B0A9AD5993AFE0E50B9E3CE2058609D79D56 (void);
// 0x00000011 System.Void Arduino/<Run>d__14::.ctor(System.Int32)
extern void U3CRunU3Ed__14__ctor_mD38DEEA5C1CB9279DB0564AD4F4B6FA4743A954C (void);
// 0x00000012 System.Void Arduino/<Run>d__14::System.IDisposable.Dispose()
extern void U3CRunU3Ed__14_System_IDisposable_Dispose_m796B59DBF8A331587615DDDD3045228465E9EFA6 (void);
// 0x00000013 System.Boolean Arduino/<Run>d__14::MoveNext()
extern void U3CRunU3Ed__14_MoveNext_m69897A7DDB7696B9D4137D443C5D619E0B9B5BE3 (void);
// 0x00000014 System.Object Arduino/<Run>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCFF543B582F4EB43E088D4B12DEEEAA64777DD10 (void);
// 0x00000015 System.Void Arduino/<Run>d__14::System.Collections.IEnumerator.Reset()
extern void U3CRunU3Ed__14_System_Collections_IEnumerator_Reset_mBC64C5E8D99639053CA223F825D80EDFB2D05FA1 (void);
// 0x00000016 System.Object Arduino/<Run>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CRunU3Ed__14_System_Collections_IEnumerator_get_Current_m7C4773393032D2FD8222D2505A5702510163D4A1 (void);
// 0x00000017 System.Void CameraController::Update()
extern void CameraController_Update_m3C257AC762117CFDDAD03C9C4FBBFDE51C61D534 (void);
// 0x00000018 System.Void CameraController::.ctor()
extern void CameraController__ctor_m07EC5A8C82742876097619BE7DD9043F47327DAE (void);
// 0x00000019 System.Void CarWheelEnginePreviewController::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void CarWheelEnginePreviewController_OnDrag_mF14E6C7542EB8254011865857B8659DF1D2F1273 (void);
// 0x0000001A System.Void CarWheelEnginePreviewController::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void CarWheelEnginePreviewController_OnEndDrag_mE8B99DB9F33D4107C04BF63AE9D072F00A1DB0D0 (void);
// 0x0000001B System.Void CarWheelEnginePreviewController::Start()
extern void CarWheelEnginePreviewController_Start_m6DAE2772411F2050D97D8AD150BC33F52952CD43 (void);
// 0x0000001C System.Void CarWheelEnginePreviewController::SpawnEngine()
extern void CarWheelEnginePreviewController_SpawnEngine_m09D506A05FFEFF3B3A76CC10AD1DD13338352381 (void);
// 0x0000001D System.Void CarWheelEnginePreviewController::.ctor()
extern void CarWheelEnginePreviewController__ctor_mAA3A774178F23D2A5A669166398B36068C3B58B0 (void);
// 0x0000001E System.Void CodeController::Start()
extern void CodeController_Start_mB235037A92B5E793DB5EA0F81121AF0E7729BFD6 (void);
// 0x0000001F System.Void CodeController::SendCode()
extern void CodeController_SendCode_m6BCCF870626D390A7419825D72D70443629F949D (void);
// 0x00000020 System.Void CodeController::showCodePanel()
extern void CodeController_showCodePanel_m46400B5EA5AC899371619780F843EF348CCCB7F0 (void);
// 0x00000021 System.Void CodeController::.ctor()
extern void CodeController__ctor_m1F61CDB23433D5380A7EF90CFC4082E1F9BEC269 (void);
// 0x00000022 System.Void CodeController::<Start>b__6_3(SocketIOClient.SocketIOResponse)
extern void CodeController_U3CStartU3Eb__6_3_m9227E3FF4E1A14FB9C998CF5D290E054DEEE08D6 (void);
// 0x00000023 System.Void CodeController/<>c::.cctor()
extern void U3CU3Ec__cctor_mD20518DCEB98DD4550617722464031E4808A8D2E (void);
// 0x00000024 System.Void CodeController/<>c::.ctor()
extern void U3CU3Ec__ctor_mB12043FDF5F3258D0AC5F7F0B7CA5DF9EE825D04 (void);
// 0x00000025 System.Void CodeController/<>c::<Start>b__6_0(System.Object,System.EventArgs)
extern void U3CU3Ec_U3CStartU3Eb__6_0_m72015029A7864816F45F1BCFF64C6236D10A9B43 (void);
// 0x00000026 System.Void CodeController/<>c::<Start>b__6_1(System.Object,System.String)
extern void U3CU3Ec_U3CStartU3Eb__6_1_mF62F4ABB3F62A88704AE5C13C12F32C2542D647F (void);
// 0x00000027 System.Void CodeController/<>c::<Start>b__6_2(System.Object,System.String)
extern void U3CU3Ec_U3CStartU3Eb__6_2_m7EA77DF3BCC491E6F6DB5AF5CE85AC7AB926BAF4 (void);
// 0x00000028 System.Void CodeController/<>c::<Start>b__6_4(SocketIOClient.SocketIOResponse)
extern void U3CU3Ec_U3CStartU3Eb__6_4_mA7F641A80517D4D5B43B4DE091B9B0F4145B7875 (void);
// 0x00000029 System.Void CodeController/<Start>d__6::.ctor()
extern void U3CStartU3Ed__6__ctor_m15E5D00F35909C16AD644DD54FD310A6013E0BF2 (void);
// 0x0000002A System.Void CodeController/<Start>d__6::MoveNext()
extern void U3CStartU3Ed__6_MoveNext_m959A71771D56177804847D8066A0C3BF5DA16ED3 (void);
// 0x0000002B System.Void CodeController/<Start>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__6_SetStateMachine_m5A019A5041355DDC2E4FD430188DD6E0F133D1DB (void);
// 0x0000002C System.Void CodeController/<SendCode>d__7::.ctor()
extern void U3CSendCodeU3Ed__7__ctor_mA0E9B645AE46D88312F52E2E437884F30AC32D1C (void);
// 0x0000002D System.Void CodeController/<SendCode>d__7::MoveNext()
extern void U3CSendCodeU3Ed__7_MoveNext_m90861DDB1DBCE03DEF44A0633E1A1CF4D3B5DCFE (void);
// 0x0000002E System.Void CodeController/<SendCode>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendCodeU3Ed__7_SetStateMachine_mE404E805A570E58AE3B25C434C0560656F854982 (void);
// 0x0000002F System.Void ConnectedItem::Start()
extern void ConnectedItem_Start_mC044B4946EDD622CDB474BD6FF7992BEF1FB229D (void);
// 0x00000030 System.Void ConnectedItem::.ctor()
extern void ConnectedItem__ctor_m1515426A04C95C5A095219F38F2B12AF5047DF0F (void);
// 0x00000031 System.Void ErrorPanel::ShowPanel()
extern void ErrorPanel_ShowPanel_m9F092F43B2B8E31789C83DEA0BF44A47571EE473 (void);
// 0x00000032 System.Void ErrorPanel::ClosePanel()
extern void ErrorPanel_ClosePanel_m5A446E04E8EE08D622A60DC20021D10A22B1920C (void);
// 0x00000033 System.Void ErrorPanel::.ctor()
extern void ErrorPanel__ctor_mD067E52121A8B7A43289463DE62814A001AE6093 (void);
// 0x00000034 System.Void InfoPanel::ShowInfo()
extern void InfoPanel_ShowInfo_m6E44C633EABD9E9560DB38B47FD735B9525E5898 (void);
// 0x00000035 System.Void InfoPanel::HideInfo()
extern void InfoPanel_HideInfo_m268540B524BEBEA8EC0D57F2D036501656230363 (void);
// 0x00000036 System.Void InfoPanel::Update()
extern void InfoPanel_Update_m7640887405710BA1A32F77FCCED3AEBC7E8368C8 (void);
// 0x00000037 System.Void InfoPanel::.ctor()
extern void InfoPanel__ctor_mDB3109BE0D519300E31668519BF68754EB7A4381 (void);
// 0x00000038 System.Void ItemDragHandler::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ItemDragHandler_OnDrag_m688DF94AB378B13AED170B5E6750B3934EB0B15E (void);
// 0x00000039 System.Void ItemDragHandler::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void ItemDragHandler_OnEndDrag_m2CB4991F4305243589BE6C3152FA249174561B32 (void);
// 0x0000003A System.Void ItemDragHandler::Start()
extern void ItemDragHandler_Start_mA3AC11F676872D544ADB7269F0119896123B2D89 (void);
// 0x0000003B System.Void ItemDragHandler::.ctor()
extern void ItemDragHandler__ctor_m47607098DAB467744054B7046702AF6C93D47A53 (void);
// 0x0000003C System.Void ItemToMoveProperties::.ctor()
extern void ItemToMoveProperties__ctor_m2F9390F53A0D143857877A0C648B37F7FE4433EC (void);
// 0x0000003D System.Void MovableObjectOptions::RemoveMovableObject()
extern void MovableObjectOptions_RemoveMovableObject_m1414A88F43C0B6DF597C5E86C0ACEC67F8E7C7A3 (void);
// 0x0000003E System.Void MovableObjectOptions::SetParams(System.String,System.Int32,System.String,System.String,UnityEngine.GameObject)
extern void MovableObjectOptions_SetParams_m596BD136747E88321101B614970BA5C9AC741AE2 (void);
// 0x0000003F System.Void MovableObjectOptions::EditEngineParams()
extern void MovableObjectOptions_EditEngineParams_m5F7D1325E77C09892613D75275CED09AC7298065 (void);
// 0x00000040 System.Int16 MovableObjectOptions::ChangeEnginePosition()
extern void MovableObjectOptions_ChangeEnginePosition_m8566086558BD0548EEA4D4A683930C927402DB11 (void);
// 0x00000041 System.Void MovableObjectOptions::Awake()
extern void MovableObjectOptions_Awake_m2E46B926F454A10B173855111D8CBFFCBEA637BE (void);
// 0x00000042 System.Void MovableObjectOptions::.ctor()
extern void MovableObjectOptions__ctor_m7ECACB8E58CD4965CB7C1F8EDA91490DB3C8B2E6 (void);
// 0x00000043 System.Void MovableObjectProperties::Awake()
extern void MovableObjectProperties_Awake_m509CF10696986F9980EBC2D66A8A3BC5459DBDA9 (void);
// 0x00000044 System.Void MovableObjectProperties::.ctor()
extern void MovableObjectProperties__ctor_mCE1653ABC1316DC50FA12F032A987CD9CB5FDF21 (void);
// 0x00000045 System.Void MoveWheels::.ctor(UnityEngine.GameObject,UnityEngine.WheelCollider,System.Int32,System.Int32,System.Int32)
extern void MoveWheels__ctor_mE7F867A6C211322B9AAE00FDD5E5DA8B5912F59B (void);
// 0x00000046 System.Void MoveWheels::.ctor(WheelData,WheelData,WheelData,WheelData)
extern void MoveWheels__ctor_mE08B511AFCBDFA09254C0966D141AF92FA5555BB (void);
// 0x00000047 System.Void MoveWheels::_Move(System.Int32)
extern void MoveWheels__Move_m6E3693E3B7A33E5CC0AA665174EE84B99CBB2DDF (void);
// 0x00000048 System.Void MoveWheels::_Stop()
extern void MoveWheels__Stop_m2824FBE20D158AEF37905097E061E5A793D60614 (void);
// 0x00000049 System.Void MoveWheels::_Stop(System.Int32)
extern void MoveWheels__Stop_mBA048A12EB1BDB521ECE44BEB4B3C4E00DA1C67C (void);
// 0x0000004A System.Void ObjectOptions::RemoveObject()
extern void ObjectOptions_RemoveObject_mBD7A04F3A544A301A9EDC6DA72DB896682CDA0E5 (void);
// 0x0000004B System.Void ObjectOptions::ClosePopUp()
extern void ObjectOptions_ClosePopUp_m4D74188219C82641D61E4119F7E10197825E287B (void);
// 0x0000004C System.Void ObjectOptions::.ctor()
extern void ObjectOptions__ctor_mBA250A54ACA4B44D2E39CC9D148B1BF902B1F53C (void);
// 0x0000004D System.Void ObjectsStorage::Awake()
extern void ObjectsStorage_Awake_m6761EF053312ADB00916BC68015E54DD30A55CF7 (void);
// 0x0000004E System.Void ObjectsStorage::.ctor()
extern void ObjectsStorage__ctor_mF6CB5BAB0D99120D03AA33B6CD0577641F584C1C (void);
// 0x0000004F System.Void PinPlaceholder::.ctor()
extern void PinPlaceholder__ctor_m35077F0AA6446CA0DE6E9564CE3F3326BF32FFE5 (void);
// 0x00000050 System.Void RemoveItem::Start()
extern void RemoveItem_Start_mD0C8CA1805E53557381A9DECD3431DD80F4C723C (void);
// 0x00000051 System.Void RemoveItem::OnMouseDown()
extern void RemoveItem_OnMouseDown_mE5D276F0AEFC6A88B4B3686C3A6D0D7D542AD560 (void);
// 0x00000052 System.Void RemoveItem::.ctor()
extern void RemoveItem__ctor_mAB6482306BE0210F820830A13DB467B422ABB98D (void);
// 0x00000053 System.Void RemovePanel::Start()
extern void RemovePanel_Start_mFA68035B1F1118C8629B257880D268B23319748A (void);
// 0x00000054 System.Void RemovePanel::Update()
extern void RemovePanel_Update_mECF11441184567462B9D3FCBAB5A6E22F772E5DA (void);
// 0x00000055 System.Void RemovePanel::.ctor()
extern void RemovePanel__ctor_m0E171596B7785281DC1A493E360B2D6C8C7D95F9 (void);
// 0x00000056 System.Void WheelData::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.GameObject,UnityEngine.WheelCollider)
extern void WheelData__ctor_m075B79F515B1CECB9EF4C8FE081DEED2EE4E1604 (void);
// 0x00000057 System.Void WheelData::Rotate()
extern void WheelData_Rotate_m1D7CC8ABB763CB63F389E474213CEC20D4B75639 (void);
static Il2CppMethodPointer s_methodPointers[87] = 
{
	CarController_Start_m10BA7CB090F1049BB7AC3D4C4BCFA943380B7882,
	CarController_Update_m69DDDE13273382E3C25AD2D77EC70DFFE3A70739,
	CarController_FixedUpdate_m62A2DA259A4FA667F2A78CC2701E1A4601C4A4F7,
	CarController__ctor_m180A0B729C25B52327B05362FB3A7AA7A53A9739,
	AnimationController_Start_mC4FA2010CE819DE174F849DF8E31BD919545D818,
	AnimationController_Move_mF7066AB49D94C7A97801448C1898BCE24A33F4BF,
	AnimationController_Stop_m9C16393FC9BAD291EDBF2B93B4735509AC5E634A,
	AnimationController_Stop_m92E70272C0ABE67FE4E343689ABF91F433760737,
	AnimationController_FixedUpdate_m62D4ABAC648A3979A13B7397D52254C8E6F6179F,
	AnimationController__ctor_mD70B26881DF564CDFD37D6E5EEABFC975DC2368C,
	Arduino_get_MoveWheels_mD550A38043E38D061BCAC881643471BBA11D4993,
	Arduino_set_MoveWheels_m425C7BDEE149210D9A4F97116F1C44AAA444FA26,
	Arduino_Update_mC5726F6C36A0B8A7C1FBBA4E23DFA066FA8C2888,
	Arduino_Run_m7CCA0C0EC351CD043B30D86A16B8FFFA30A68231,
	Arduino_Run_m7D6745240363F3DB699F607CF7218CA635679225,
	Arduino__ctor_m8693B0A9AD5993AFE0E50B9E3CE2058609D79D56,
	U3CRunU3Ed__14__ctor_mD38DEEA5C1CB9279DB0564AD4F4B6FA4743A954C,
	U3CRunU3Ed__14_System_IDisposable_Dispose_m796B59DBF8A331587615DDDD3045228465E9EFA6,
	U3CRunU3Ed__14_MoveNext_m69897A7DDB7696B9D4137D443C5D619E0B9B5BE3,
	U3CRunU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCFF543B582F4EB43E088D4B12DEEEAA64777DD10,
	U3CRunU3Ed__14_System_Collections_IEnumerator_Reset_mBC64C5E8D99639053CA223F825D80EDFB2D05FA1,
	U3CRunU3Ed__14_System_Collections_IEnumerator_get_Current_m7C4773393032D2FD8222D2505A5702510163D4A1,
	CameraController_Update_m3C257AC762117CFDDAD03C9C4FBBFDE51C61D534,
	CameraController__ctor_m07EC5A8C82742876097619BE7DD9043F47327DAE,
	CarWheelEnginePreviewController_OnDrag_mF14E6C7542EB8254011865857B8659DF1D2F1273,
	CarWheelEnginePreviewController_OnEndDrag_mE8B99DB9F33D4107C04BF63AE9D072F00A1DB0D0,
	CarWheelEnginePreviewController_Start_m6DAE2772411F2050D97D8AD150BC33F52952CD43,
	CarWheelEnginePreviewController_SpawnEngine_m09D506A05FFEFF3B3A76CC10AD1DD13338352381,
	CarWheelEnginePreviewController__ctor_mAA3A774178F23D2A5A669166398B36068C3B58B0,
	CodeController_Start_mB235037A92B5E793DB5EA0F81121AF0E7729BFD6,
	CodeController_SendCode_m6BCCF870626D390A7419825D72D70443629F949D,
	CodeController_showCodePanel_m46400B5EA5AC899371619780F843EF348CCCB7F0,
	CodeController__ctor_m1F61CDB23433D5380A7EF90CFC4082E1F9BEC269,
	CodeController_U3CStartU3Eb__6_3_m9227E3FF4E1A14FB9C998CF5D290E054DEEE08D6,
	U3CU3Ec__cctor_mD20518DCEB98DD4550617722464031E4808A8D2E,
	U3CU3Ec__ctor_mB12043FDF5F3258D0AC5F7F0B7CA5DF9EE825D04,
	U3CU3Ec_U3CStartU3Eb__6_0_m72015029A7864816F45F1BCFF64C6236D10A9B43,
	U3CU3Ec_U3CStartU3Eb__6_1_mF62F4ABB3F62A88704AE5C13C12F32C2542D647F,
	U3CU3Ec_U3CStartU3Eb__6_2_m7EA77DF3BCC491E6F6DB5AF5CE85AC7AB926BAF4,
	U3CU3Ec_U3CStartU3Eb__6_4_mA7F641A80517D4D5B43B4DE091B9B0F4145B7875,
	U3CStartU3Ed__6__ctor_m15E5D00F35909C16AD644DD54FD310A6013E0BF2,
	U3CStartU3Ed__6_MoveNext_m959A71771D56177804847D8066A0C3BF5DA16ED3,
	U3CStartU3Ed__6_SetStateMachine_m5A019A5041355DDC2E4FD430188DD6E0F133D1DB,
	U3CSendCodeU3Ed__7__ctor_mA0E9B645AE46D88312F52E2E437884F30AC32D1C,
	U3CSendCodeU3Ed__7_MoveNext_m90861DDB1DBCE03DEF44A0633E1A1CF4D3B5DCFE,
	U3CSendCodeU3Ed__7_SetStateMachine_mE404E805A570E58AE3B25C434C0560656F854982,
	ConnectedItem_Start_mC044B4946EDD622CDB474BD6FF7992BEF1FB229D,
	ConnectedItem__ctor_m1515426A04C95C5A095219F38F2B12AF5047DF0F,
	ErrorPanel_ShowPanel_m9F092F43B2B8E31789C83DEA0BF44A47571EE473,
	ErrorPanel_ClosePanel_m5A446E04E8EE08D622A60DC20021D10A22B1920C,
	ErrorPanel__ctor_mD067E52121A8B7A43289463DE62814A001AE6093,
	InfoPanel_ShowInfo_m6E44C633EABD9E9560DB38B47FD735B9525E5898,
	InfoPanel_HideInfo_m268540B524BEBEA8EC0D57F2D036501656230363,
	InfoPanel_Update_m7640887405710BA1A32F77FCCED3AEBC7E8368C8,
	InfoPanel__ctor_mDB3109BE0D519300E31668519BF68754EB7A4381,
	ItemDragHandler_OnDrag_m688DF94AB378B13AED170B5E6750B3934EB0B15E,
	ItemDragHandler_OnEndDrag_m2CB4991F4305243589BE6C3152FA249174561B32,
	ItemDragHandler_Start_mA3AC11F676872D544ADB7269F0119896123B2D89,
	ItemDragHandler__ctor_m47607098DAB467744054B7046702AF6C93D47A53,
	ItemToMoveProperties__ctor_m2F9390F53A0D143857877A0C648B37F7FE4433EC,
	MovableObjectOptions_RemoveMovableObject_m1414A88F43C0B6DF597C5E86C0ACEC67F8E7C7A3,
	MovableObjectOptions_SetParams_m596BD136747E88321101B614970BA5C9AC741AE2,
	MovableObjectOptions_EditEngineParams_m5F7D1325E77C09892613D75275CED09AC7298065,
	MovableObjectOptions_ChangeEnginePosition_m8566086558BD0548EEA4D4A683930C927402DB11,
	MovableObjectOptions_Awake_m2E46B926F454A10B173855111D8CBFFCBEA637BE,
	MovableObjectOptions__ctor_m7ECACB8E58CD4965CB7C1F8EDA91490DB3C8B2E6,
	MovableObjectProperties_Awake_m509CF10696986F9980EBC2D66A8A3BC5459DBDA9,
	MovableObjectProperties__ctor_mCE1653ABC1316DC50FA12F032A987CD9CB5FDF21,
	MoveWheels__ctor_mE7F867A6C211322B9AAE00FDD5E5DA8B5912F59B,
	MoveWheels__ctor_mE08B511AFCBDFA09254C0966D141AF92FA5555BB,
	MoveWheels__Move_m6E3693E3B7A33E5CC0AA665174EE84B99CBB2DDF,
	MoveWheels__Stop_m2824FBE20D158AEF37905097E061E5A793D60614,
	MoveWheels__Stop_mBA048A12EB1BDB521ECE44BEB4B3C4E00DA1C67C,
	ObjectOptions_RemoveObject_mBD7A04F3A544A301A9EDC6DA72DB896682CDA0E5,
	ObjectOptions_ClosePopUp_m4D74188219C82641D61E4119F7E10197825E287B,
	ObjectOptions__ctor_mBA250A54ACA4B44D2E39CC9D148B1BF902B1F53C,
	ObjectsStorage_Awake_m6761EF053312ADB00916BC68015E54DD30A55CF7,
	ObjectsStorage__ctor_mF6CB5BAB0D99120D03AA33B6CD0577641F584C1C,
	PinPlaceholder__ctor_m35077F0AA6446CA0DE6E9564CE3F3326BF32FFE5,
	RemoveItem_Start_mD0C8CA1805E53557381A9DECD3431DD80F4C723C,
	RemoveItem_OnMouseDown_mE5D276F0AEFC6A88B4B3686C3A6D0D7D542AD560,
	RemoveItem__ctor_mAB6482306BE0210F820830A13DB467B422ABB98D,
	RemovePanel_Start_mFA68035B1F1118C8629B257880D268B23319748A,
	RemovePanel_Update_mECF11441184567462B9D3FCBAB5A6E22F772E5DA,
	RemovePanel__ctor_m0E171596B7785281DC1A493E360B2D6C8C7D95F9,
	WheelData__ctor_m075B79F515B1CECB9EF4C8FE081DEED2EE4E1604,
	WheelData_Rotate_m1D7CC8ABB763CB63F389E474213CEC20D4B75639,
};
static const int32_t s_InvokerIndices[87] = 
{
	2411,
	2411,
	2411,
	2411,
	2411,
	657,
	2411,
	1999,
	2411,
	2411,
	2360,
	2015,
	2411,
	2015,
	2360,
	2411,
	1999,
	2411,
	2385,
	2360,
	2411,
	2360,
	2411,
	2411,
	2015,
	2015,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2015,
	3876,
	2411,
	1188,
	1188,
	1188,
	2015,
	2411,
	2411,
	2015,
	2411,
	2411,
	2015,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2015,
	2015,
	2411,
	2411,
	2411,
	2411,
	215,
	2411,
	2340,
	2411,
	2411,
	2411,
	2411,
	221,
	458,
	1999,
	2411,
	1999,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	196,
	2411,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	87,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
