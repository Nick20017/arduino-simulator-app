﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Resources.ResourceManager System.SR::get_ResourceManager()
extern void SR_get_ResourceManager_mEC93DC61FEB777324823B4D16BE4B54480955947 (void);
// 0x00000002 System.String System.SR::GetResourceString(System.String,System.String)
extern void SR_GetResourceString_m80874FE1DCB8E1B2F7B25D15F51219F5CADE4AD7 (void);
// 0x00000003 System.Type System.SR::get_ResourceType()
extern void SR_get_ResourceType_mF148E26A9D2AA3BA0B98C21963449A5BA4E3CCA7 (void);
// 0x00000004 System.String System.SR::get_ArgumentException_BufferNotFromPool()
extern void SR_get_ArgumentException_BufferNotFromPool_mF7E9FA1998CFF097C3844036F63685CE4927FD35 (void);
// 0x00000005 System.Void System.SR::.cctor()
extern void SR__cctor_m18CAEA8A36502BED2DF824DBAE84AD86782EF211 (void);
// 0x00000006 System.Buffers.ArrayPool`1<T> System.Buffers.ArrayPool`1::get_Shared()
// 0x00000007 System.Buffers.ArrayPool`1<T> System.Buffers.ArrayPool`1::EnsureSharedCreated()
// 0x00000008 System.Buffers.ArrayPool`1<T> System.Buffers.ArrayPool`1::Create()
// 0x00000009 T[] System.Buffers.ArrayPool`1::Rent(System.Int32)
// 0x0000000A System.Void System.Buffers.ArrayPool`1::Return(T[],System.Boolean)
// 0x0000000B System.Void System.Buffers.ArrayPool`1::.ctor()
// 0x0000000C System.Void System.Buffers.ArrayPoolEventSource::BufferRented(System.Int32,System.Int32,System.Int32,System.Int32)
extern void ArrayPoolEventSource_BufferRented_m5CE9BCB29BD945F1FD67DE08C639408661C59290 (void);
// 0x0000000D System.Void System.Buffers.ArrayPoolEventSource::BufferAllocated(System.Int32,System.Int32,System.Int32,System.Int32,System.Buffers.ArrayPoolEventSource/BufferAllocatedReason)
extern void ArrayPoolEventSource_BufferAllocated_m214A0B4602D23E352E0F4B6BE05C93D3A43F9C7B (void);
// 0x0000000E System.Void System.Buffers.ArrayPoolEventSource::BufferReturned(System.Int32,System.Int32,System.Int32)
extern void ArrayPoolEventSource_BufferReturned_m13CAD12A690C7CB337141C7AC3577CEE3647DF81 (void);
// 0x0000000F System.Void System.Buffers.ArrayPoolEventSource::.ctor()
extern void ArrayPoolEventSource__ctor_mD1104A3392C287DC1B37430D004E36CCED62DC2E (void);
// 0x00000010 System.Void System.Buffers.ArrayPoolEventSource::.cctor()
extern void ArrayPoolEventSource__cctor_m0C90DD30C25E1937DDCA282D02D3B1CD9A9B46EE (void);
// 0x00000011 System.Void System.Buffers.DefaultArrayPool`1::.ctor()
// 0x00000012 System.Void System.Buffers.DefaultArrayPool`1::.ctor(System.Int32,System.Int32)
// 0x00000013 System.Int32 System.Buffers.DefaultArrayPool`1::get_Id()
// 0x00000014 T[] System.Buffers.DefaultArrayPool`1::Rent(System.Int32)
// 0x00000015 System.Void System.Buffers.DefaultArrayPool`1::Return(T[],System.Boolean)
// 0x00000016 System.Void System.Buffers.DefaultArrayPool`1/Bucket::.ctor(System.Int32,System.Int32,System.Int32)
// 0x00000017 System.Int32 System.Buffers.DefaultArrayPool`1/Bucket::get_Id()
// 0x00000018 T[] System.Buffers.DefaultArrayPool`1/Bucket::Rent()
// 0x00000019 System.Void System.Buffers.DefaultArrayPool`1/Bucket::Return(T[])
// 0x0000001A System.Int32 System.Buffers.Utilities::SelectBucketIndex(System.Int32)
extern void Utilities_SelectBucketIndex_m8A1B5269010C90973FDB9538193CBBBE26091969 (void);
// 0x0000001B System.Int32 System.Buffers.Utilities::GetMaxSizeForBucket(System.Int32)
extern void Utilities_GetMaxSizeForBucket_m63505BFBAC122CFFE8E4D2EA257020DF5EDADBE7 (void);
static Il2CppMethodPointer s_methodPointers[27] = 
{
	SR_get_ResourceManager_mEC93DC61FEB777324823B4D16BE4B54480955947,
	SR_GetResourceString_m80874FE1DCB8E1B2F7B25D15F51219F5CADE4AD7,
	SR_get_ResourceType_mF148E26A9D2AA3BA0B98C21963449A5BA4E3CCA7,
	SR_get_ArgumentException_BufferNotFromPool_mF7E9FA1998CFF097C3844036F63685CE4927FD35,
	SR__cctor_m18CAEA8A36502BED2DF824DBAE84AD86782EF211,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ArrayPoolEventSource_BufferRented_m5CE9BCB29BD945F1FD67DE08C639408661C59290,
	ArrayPoolEventSource_BufferAllocated_m214A0B4602D23E352E0F4B6BE05C93D3A43F9C7B,
	ArrayPoolEventSource_BufferReturned_m13CAD12A690C7CB337141C7AC3577CEE3647DF81,
	ArrayPoolEventSource__ctor_mD1104A3392C287DC1B37430D004E36CCED62DC2E,
	ArrayPoolEventSource__cctor_m0C90DD30C25E1937DDCA282D02D3B1CD9A9B46EE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Utilities_SelectBucketIndex_m8A1B5269010C90973FDB9538193CBBBE26091969,
	Utilities_GetMaxSizeForBucket_m63505BFBAC122CFFE8E4D2EA257020DF5EDADBE7,
};
static const int32_t s_InvokerIndices[27] = 
{
	3862,
	3305,
	3862,
	3862,
	3876,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	403,
	194,
	657,
	2411,
	3876,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3651,
	3651,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000004, { 0, 7 } },
	{ 0x02000007, { 7, 12 } },
	{ 0x02000008, { 19, 3 } },
};
extern const uint32_t g_rgctx_ArrayPool_1_t19D1D56C55CD3C72FA054BD34E4AEF1C5590AE16;
extern const uint32_t g_rgctx_Volatile_Read_TisArrayPool_1_t19D1D56C55CD3C72FA054BD34E4AEF1C5590AE16_mBF6BF79FBD75692592B59BA4592C48A50399FF8F;
extern const uint32_t g_rgctx_ArrayPool_1_EnsureSharedCreated_m36D06769FBCF293B8991CF826068077F0C76104A;
extern const uint32_t g_rgctx_ArrayPool_1_Create_mA37720A24BCFA1887283CAE9778C42608DB10468;
extern const uint32_t g_rgctx_Interlocked_CompareExchange_TisArrayPool_1_t19D1D56C55CD3C72FA054BD34E4AEF1C5590AE16_m83AB0529C348D0F93AA6C815CD4E0B2BF77B61B1;
extern const uint32_t g_rgctx_DefaultArrayPool_1_tE19E4AC9872FE9B4266CE379FC53D8BA3C3112CD;
extern const uint32_t g_rgctx_DefaultArrayPool_1__ctor_m1538EB590C02028895EE0A3425AB1DDA5C88BFE2;
extern const uint32_t g_rgctx_DefaultArrayPool_1__ctor_mDEF96D7C086E91A76B3CD0F181A63A35A362F277;
extern const uint32_t g_rgctx_ArrayPool_1__ctor_m76A53744501387BB41E652BBD87E1EC7184EBCF8;
extern const uint32_t g_rgctx_ArrayPool_1_t12E85BC6FA014A0A02B6526BFB013D346377EFCB;
extern const uint32_t g_rgctx_DefaultArrayPool_1_get_Id_m453F38BC8EE4EC2F7D8926795401AF8C8C6BDFC9;
extern const uint32_t g_rgctx_BucketU5BU5D_tEFD328A066F95D3D79523BA8CFA14942C5329451;
extern const uint32_t g_rgctx_Bucket_t0A9AC9B7BC4F3436FC042F4BDCC57A57F52AA21D;
extern const uint32_t g_rgctx_Bucket__ctor_m9DFAFFCD6E0E9258A13256F1DD9C87A200BBD918;
extern const uint32_t g_rgctx_DefaultArrayPool_1_t4CFFE8C2873BFAF4B76094ED1C91363271320CD2;
extern const uint32_t g_rgctx_TU5BU5D_t184326304D1BD5D0C2213915047FF2838CA38C95;
extern const uint32_t g_rgctx_Bucket_Rent_m90CE7EE24A00BC65C7A6C8BD43B243A3D46E7BBA;
extern const uint32_t g_rgctx_Bucket_get_Id_m1238BC8CF070EA46838573C6905611892BFCC531;
extern const uint32_t g_rgctx_Bucket_Return_m4B88F44EFB9EDF3D95A8D733B6BE519392C194DA;
extern const uint32_t g_rgctx_TU5BU5DU5BU5D_t09E8EE1713E7675FE2ABA28AA87784571F1DA6B0;
extern const uint32_t g_rgctx_TU5BU5D_tDBD0CBE50168C12392694D2FA593D4B4ABDFEEA6;
extern const uint32_t g_rgctx_Bucket_get_Id_m6746020D8F4824AFA81A0B57058F50166525B841;
static const Il2CppRGCTXDefinition s_rgctxValues[22] = 
{
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayPool_1_t19D1D56C55CD3C72FA054BD34E4AEF1C5590AE16 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Volatile_Read_TisArrayPool_1_t19D1D56C55CD3C72FA054BD34E4AEF1C5590AE16_mBF6BF79FBD75692592B59BA4592C48A50399FF8F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayPool_1_EnsureSharedCreated_m36D06769FBCF293B8991CF826068077F0C76104A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayPool_1_Create_mA37720A24BCFA1887283CAE9778C42608DB10468 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Interlocked_CompareExchange_TisArrayPool_1_t19D1D56C55CD3C72FA054BD34E4AEF1C5590AE16_m83AB0529C348D0F93AA6C815CD4E0B2BF77B61B1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DefaultArrayPool_1_tE19E4AC9872FE9B4266CE379FC53D8BA3C3112CD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DefaultArrayPool_1__ctor_m1538EB590C02028895EE0A3425AB1DDA5C88BFE2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DefaultArrayPool_1__ctor_mDEF96D7C086E91A76B3CD0F181A63A35A362F277 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayPool_1__ctor_m76A53744501387BB41E652BBD87E1EC7184EBCF8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayPool_1_t12E85BC6FA014A0A02B6526BFB013D346377EFCB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DefaultArrayPool_1_get_Id_m453F38BC8EE4EC2F7D8926795401AF8C8C6BDFC9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BucketU5BU5D_tEFD328A066F95D3D79523BA8CFA14942C5329451 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Bucket_t0A9AC9B7BC4F3436FC042F4BDCC57A57F52AA21D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Bucket__ctor_m9DFAFFCD6E0E9258A13256F1DD9C87A200BBD918 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DefaultArrayPool_1_t4CFFE8C2873BFAF4B76094ED1C91363271320CD2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t184326304D1BD5D0C2213915047FF2838CA38C95 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Bucket_Rent_m90CE7EE24A00BC65C7A6C8BD43B243A3D46E7BBA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Bucket_get_Id_m1238BC8CF070EA46838573C6905611892BFCC531 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Bucket_Return_m4B88F44EFB9EDF3D95A8D733B6BE519392C194DA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5DU5BU5D_t09E8EE1713E7675FE2ABA28AA87784571F1DA6B0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_tDBD0CBE50168C12392694D2FA593D4B4ABDFEEA6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Bucket_get_Id_m6746020D8F4824AFA81A0B57058F50166525B841 },
};
extern const CustomAttributesCacheGenerator g_System_Buffers_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Buffers_CodeGenModule;
const Il2CppCodeGenModule g_System_Buffers_CodeGenModule = 
{
	"System.Buffers.dll",
	27,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	22,
	s_rgctxValues,
	NULL,
	g_System_Buffers_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
