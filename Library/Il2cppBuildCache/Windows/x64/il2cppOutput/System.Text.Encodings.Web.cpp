﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct VirtualFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};

// System.Pinnable`1<System.Byte>
struct Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110;
// System.Pinnable`1<System.Char>
struct Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C;
// System.Pinnable`1<System.UInt32>
struct Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// System.Text.Unicode.UnicodeRange[]
struct UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8;
// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056;
// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E;
// System.Text.DecoderFallback
struct DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D;
// System.Text.Encodings.Web.DefaultHtmlEncoder
struct DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351;
// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin
struct DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t69546BC39F6CF6333844CB63739D9E618500F639;
// System.Text.EncoderFallback
struct EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// System.Text.Encodings.Web.HtmlEncoder
struct HtmlEncoder_t09B67E830AECB24356C3BBFBD9AD44952F581217;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tF5095FA09BDCB5CFD77CA99BB1889138DBCF597E;
// System.Text.Encodings.Web.JavaScriptEncoder
struct JavaScriptEncoder_tEE2A7276ABD8379AD6317965D32A05CF2AD7B118;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Diagnostics.CodeAnalysis.NotNullAttribute
struct NotNullAttribute_t7D38250DB89F63E61FBF1E7B00EA3A8DEF82B5C8;
// System.Runtime.CompilerServices.NullableAttribute
struct NullableAttribute_tDA80C6E82A02D3C2964FD7A383FC3D8479F169BD;
// System.Runtime.CompilerServices.NullableContextAttribute
struct NullableContextAttribute_tC39978867FAA8D5C736BC102E44BFEC48B5A93A3;
// System.Runtime.CompilerServices.NullablePublicOnlyAttribute
struct NullablePublicOnlyAttribute_t96F87E83DDB1878DEC7B7862F687101AD0C0D7FB;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// System.Text.Encodings.Web.TextEncoder
struct TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04;
// System.Text.Encodings.Web.TextEncoderSettings
struct TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366;
// System.Type
struct Type_t;
// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15;
// System.Text.Unicode.UnicodeRange
struct UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3;

IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30____8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30____EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral09EAD6A50C87B14995000A914300979F01096C97;
IL2CPP_EXTERN_C String_t* _stringLiteral202D5AEF51C14B19CFE144337C7FDB3B9A7C7387;
IL2CPP_EXTERN_C String_t* _stringLiteral4845015737DC41475709911228278216EE4DC3AF;
IL2CPP_EXTERN_C String_t* _stringLiteral4FC0613DB074A9C5DAB592FE3F86B3EDD439F7E5;
IL2CPP_EXTERN_C String_t* _stringLiteral7AE05DE7E37F7C0C46151B22648E1D7156C0F837;
IL2CPP_EXTERN_C String_t* _stringLiteral7DF882FBCC2A230A62D22FF65024431A34A858A2;
IL2CPP_EXTERN_C String_t* _stringLiteral81FECCD01231D97EE6D7C17B8F5531FE1A6D533E;
IL2CPP_EXTERN_C String_t* _stringLiteralA7724F58887AE658863220F8D9138F5AC5532B2C;
IL2CPP_EXTERN_C String_t* _stringLiteralBFCC6EE94F1B7AA05A04750903E25F93A7188AE0;
IL2CPP_EXTERN_C String_t* _stringLiteralC263EA29ADF3548CFEBC57B532EED28451A56C10;
IL2CPP_EXTERN_C String_t* _stringLiteralCDB258E32AF5134A2B31FE4D1EE6C0E30C9B5E29;
IL2CPP_EXTERN_C String_t* _stringLiteralDE7270C80B176C288F0786D9BFAC99EED86E77F8;
IL2CPP_EXTERN_C String_t* _stringLiteralE3D7E554C2FD3D52D9690E3D5BB7B7321C3FA52B;
IL2CPP_EXTERN_C String_t* _stringLiteralE8744A8B8BD390EB66CA0CAE2376C973E6904FFB;
IL2CPP_EXTERN_C const RuntimeMethod* AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DefaultHtmlEncoder_TryEncodeUnicodeScalar_m68F407599404602732C186AD2A2BD84E058C2065_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_m42BB18A673C9EA96D3D1465626CB033FA8F586D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m9FB3F0515CDD56A7B116A9582DA0277B58672770_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TextEncoderSettings_AllowRange_mA179A5E04975AF16B07145F78965DA63087B18EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TextEncoderSettings_AllowRanges_m60F42E23315096E4143FB35868A805ECC4660409_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3;;
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com;
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com;;
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke;
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke;;

struct ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D;
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
struct UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t911F0CF805100FD40BC4E026D78DAC86F975CA4E 
{
public:

public:
};


// System.Object


// System.EmptyArray`1<System.Byte>
struct EmptyArray_1_tB2402F7A8151EE5618C0BCC8815C169E00142333  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tB2402F7A8151EE5618C0BCC8815C169E00142333_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tB2402F7A8151EE5618C0BCC8815C169E00142333_StaticFields, ___Value_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_Value_0() const { return ___Value_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.EmptyArray`1<System.Char>
struct EmptyArray_1_t8C9D46673F64ABE360DE6F02C2BA0A5566DC9FDC  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_t8C9D46673F64ABE360DE6F02C2BA0A5566DC9FDC_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_t8C9D46673F64ABE360DE6F02C2BA0A5566DC9FDC_StaticFields, ___Value_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_Value_0() const { return ___Value_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.Pinnable`1<System.Byte>
struct Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110  : public RuntimeObject
{
public:
	// T System.Pinnable`1::Data
	uint8_t ___Data_0;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110, ___Data_0)); }
	inline uint8_t get_Data_0() const { return ___Data_0; }
	inline uint8_t* get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(uint8_t value)
	{
		___Data_0 = value;
	}
};


// System.Pinnable`1<System.Char>
struct Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C  : public RuntimeObject
{
public:
	// T System.Pinnable`1::Data
	Il2CppChar ___Data_0;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C, ___Data_0)); }
	inline Il2CppChar get_Data_0() const { return ___Data_0; }
	inline Il2CppChar* get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(Il2CppChar value)
	{
		___Data_0 = value;
	}
};


// System.Pinnable`1<System.UInt32>
struct Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424  : public RuntimeObject
{
public:
	// T System.Pinnable`1::Data
	uint32_t ___Data_0;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424, ___Data_0)); }
	inline uint32_t get_Data_0() const { return ___Data_0; }
	inline uint32_t* get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(uint32_t value)
	{
		___Data_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.BitConverter
struct BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654  : public RuntimeObject
{
public:

public:
};

struct BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_StaticFields
{
public:
	// System.Boolean System.BitConverter::IsLittleEndian
	bool ___IsLittleEndian_0;

public:
	inline static int32_t get_offset_of_IsLittleEndian_0() { return static_cast<int32_t>(offsetof(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_StaticFields, ___IsLittleEndian_0)); }
	inline bool get_IsLittleEndian_0() const { return ___IsLittleEndian_0; }
	inline bool* get_address_of_IsLittleEndian_0() { return &___IsLittleEndian_0; }
	inline void set_IsLittleEndian_0(bool value)
	{
		___IsLittleEndian_0 = value;
	}
};


// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___dataItem_10)); }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___encoderFallback_13)); }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___decoderFallback_14)); }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___encodings_8)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// System.HexConverter
struct HexConverter_tA758F5CE09BA0B4F9F24E1EEAD7473A93DB48A8F  : public RuntimeObject
{
public:

public:
};


// System.Text.Encodings.Web.JavaScriptEncoderHelper
struct JavaScriptEncoderHelper_t9237F84978CD6C8A3A8643E36AC33D6FD457850E  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.Unicode.UnicodeHelpers
struct UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C  : public RuntimeObject
{
public:

public:
};

struct UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_StaticFields
{
public:
	// System.UInt32[] System.Text.Unicode.UnicodeHelpers::_definedCharacterBitmapBigEndian
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ____definedCharacterBitmapBigEndian_0;

public:
	inline static int32_t get_offset_of__definedCharacterBitmapBigEndian_0() { return static_cast<int32_t>(offsetof(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_StaticFields, ____definedCharacterBitmapBigEndian_0)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get__definedCharacterBitmapBigEndian_0() const { return ____definedCharacterBitmapBigEndian_0; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of__definedCharacterBitmapBigEndian_0() { return &____definedCharacterBitmapBigEndian_0; }
	inline void set__definedCharacterBitmapBigEndian_0(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		____definedCharacterBitmapBigEndian_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____definedCharacterBitmapBigEndian_0), (void*)value);
	}
};


// System.Text.Unicode.UnicodeRange
struct UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Unicode.UnicodeRange::<FirstCodePoint>k__BackingField
	int32_t ___U3CFirstCodePointU3Ek__BackingField_0;
	// System.Int32 System.Text.Unicode.UnicodeRange::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFirstCodePointU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1, ___U3CFirstCodePointU3Ek__BackingField_0)); }
	inline int32_t get_U3CFirstCodePointU3Ek__BackingField_0() const { return ___U3CFirstCodePointU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFirstCodePointU3Ek__BackingField_0() { return &___U3CFirstCodePointU3Ek__BackingField_0; }
	inline void set_U3CFirstCodePointU3Ek__BackingField_0(int32_t value)
	{
		___U3CFirstCodePointU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1, ___U3CLengthU3Ek__BackingField_1)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_1() const { return ___U3CLengthU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_1() { return &___U3CLengthU3Ek__BackingField_1; }
	inline void set_U3CLengthU3Ek__BackingField_1(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_1 = value;
	}
};


// System.Text.Unicode.UnicodeRanges
struct UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2  : public RuntimeObject
{
public:

public:
};

struct UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_StaticFields
{
public:
	// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::_u0000
	UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * ____u0000_0;

public:
	inline static int32_t get_offset_of__u0000_0() { return static_cast<int32_t>(offsetof(UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_StaticFields, ____u0000_0)); }
	inline UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * get__u0000_0() const { return ____u0000_0; }
	inline UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** get_address_of__u0000_0() { return &____u0000_0; }
	inline void set__u0000_0(UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * value)
	{
		____u0000_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____u0000_0), (void*)value);
	}
};


// System.Text.UnicodeUtility
struct UnicodeUtility_tE148E221AFFB29B7856A8EB871EE844FA1360FF9  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Text.Internal.AllowedCharactersBitmap
struct AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC 
{
public:
	// System.UInt32[] System.Text.Internal.AllowedCharactersBitmap::_allowedCharacters
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ____allowedCharacters_0;

public:
	inline static int32_t get_offset_of__allowedCharacters_0() { return static_cast<int32_t>(offsetof(AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC, ____allowedCharacters_0)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get__allowedCharacters_0() const { return ____allowedCharacters_0; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of__allowedCharacters_0() { return &____allowedCharacters_0; }
	inline void set__allowedCharacters_0(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		____allowedCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____allowedCharacters_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Text.Internal.AllowedCharactersBitmap
struct AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ____allowedCharacters_0;
};
// Native definition for COM marshalling of System.Text.Internal.AllowedCharactersBitmap
struct AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ____allowedCharacters_0;
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t69546BC39F6CF6333844CB63739D9E618500F639  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int16
struct Int16_tD0F031114106263BB459DA1F099FF9F42691295A 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_tD0F031114106263BB459DA1F099FF9F42691295A, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tF5095FA09BDCB5CFD77CA99BB1889138DBCF597E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.CodeAnalysis.NotNullAttribute
struct NotNullAttribute_t7D38250DB89F63E61FBF1E7B00EA3A8DEF82B5C8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.NullableAttribute
struct NullableAttribute_tDA80C6E82A02D3C2964FD7A383FC3D8479F169BD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Byte[] System.Runtime.CompilerServices.NullableAttribute::NullableFlags
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___NullableFlags_0;

public:
	inline static int32_t get_offset_of_NullableFlags_0() { return static_cast<int32_t>(offsetof(NullableAttribute_tDA80C6E82A02D3C2964FD7A383FC3D8479F169BD, ___NullableFlags_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_NullableFlags_0() const { return ___NullableFlags_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_NullableFlags_0() { return &___NullableFlags_0; }
	inline void set_NullableFlags_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___NullableFlags_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NullableFlags_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.NullableContextAttribute
struct NullableContextAttribute_tC39978867FAA8D5C736BC102E44BFEC48B5A93A3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Byte System.Runtime.CompilerServices.NullableContextAttribute::Flag
	uint8_t ___Flag_0;

public:
	inline static int32_t get_offset_of_Flag_0() { return static_cast<int32_t>(offsetof(NullableContextAttribute_tC39978867FAA8D5C736BC102E44BFEC48B5A93A3, ___Flag_0)); }
	inline uint8_t get_Flag_0() const { return ___Flag_0; }
	inline uint8_t* get_address_of_Flag_0() { return &___Flag_0; }
	inline void set_Flag_0(uint8_t value)
	{
		___Flag_0 = value;
	}
};


// System.Runtime.CompilerServices.NullablePublicOnlyAttribute
struct NullablePublicOnlyAttribute_t96F87E83DDB1878DEC7B7862F687101AD0C0D7FB  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.NullablePublicOnlyAttribute::IncludesInternals
	bool ___IncludesInternals_0;

public:
	inline static int32_t get_offset_of_IncludesInternals_0() { return static_cast<int32_t>(offsetof(NullablePublicOnlyAttribute_t96F87E83DDB1878DEC7B7862F687101AD0C0D7FB, ___IncludesInternals_0)); }
	inline bool get_IncludesInternals_0() const { return ___IncludesInternals_0; }
	inline bool* get_address_of_IncludesInternals_0() { return &___IncludesInternals_0; }
	inline void set_IncludesInternals_0(bool value)
	{
		___IncludesInternals_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct __StaticArrayInitTypeSizeU3D256_tCEE300537DB6B7ABD119288B61B9D34EBC4B2F98 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_tCEE300537DB6B7ABD119288B61B9D34EBC4B2F98__padding[256];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=8192
struct __StaticArrayInitTypeSizeU3D8192_tD4D01DBB09C06F57972752169C7B3716F53BAC27 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D8192_tD4D01DBB09C06F57972752169C7B3716F53BAC27__padding[8192];
	};

public:
};


// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3 
{
public:
	union
	{
		struct
		{
			// System.Boolean System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer::FixedElementField
			bool ___FixedElementField_0;
		};
		uint8_t U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3__padding[128];
	};

public:
	inline static int32_t get_offset_of_FixedElementField_0() { return static_cast<int32_t>(offsetof(U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3, ___FixedElementField_0)); }
	inline bool get_FixedElementField_0() const { return ___FixedElementField_0; }
	inline bool* get_address_of_FixedElementField_0() { return &___FixedElementField_0; }
	inline void set_FixedElementField_0(bool value)
	{
		___FixedElementField_0 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke
{
	union
	{
		struct
		{
			int32_t ___FixedElementField_0;
		};
		uint8_t U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3__padding[128];
	};
};
// Native definition for COM marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com
{
	union
	{
		struct
		{
			int32_t ___FixedElementField_0;
		};
		uint8_t U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3__padding[128];
	};
};

// System.ReadOnlySpan`1<System.Byte>
struct ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 
{
public:
	// System.Pinnable`1<T> System.ReadOnlySpan`1::_pinnable
	Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ____pinnable_0;
	// System.IntPtr System.ReadOnlySpan`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.ReadOnlySpan`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726, ____pinnable_0)); }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// System.ReadOnlySpan`1<System.Char>
struct ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 
{
public:
	// System.Pinnable`1<T> System.ReadOnlySpan`1::_pinnable
	Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ____pinnable_0;
	// System.IntPtr System.ReadOnlySpan`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.ReadOnlySpan`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3, ____pinnable_0)); }
	inline Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// System.ReadOnlySpan`1<System.UInt32>
struct ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8 
{
public:
	// System.Pinnable`1<T> System.ReadOnlySpan`1::_pinnable
	Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 * ____pinnable_0;
	// System.IntPtr System.ReadOnlySpan`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.ReadOnlySpan`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8, ____pinnable_0)); }
	inline Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// System.Span`1<System.Byte>
struct Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 
{
public:
	// System.Pinnable`1<T> System.Span`1::_pinnable
	Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ____pinnable_0;
	// System.IntPtr System.Span`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.Span`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83, ____pinnable_0)); }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// System.Span`1<System.Char>
struct Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D 
{
public:
	// System.Pinnable`1<T> System.Span`1::_pinnable
	Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ____pinnable_0;
	// System.IntPtr System.Span`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.Span`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D, ____pinnable_0)); }
	inline Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=8192 <PrivateImplementationDetails>::8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6
	__StaticArrayInitTypeSizeU3D8192_tD4D01DBB09C06F57972752169C7B3716F53BAC27  ___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E
	__StaticArrayInitTypeSizeU3D256_tCEE300537DB6B7ABD119288B61B9D34EBC4B2F98  ___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1;

public:
	inline static int32_t get_offset_of_U38B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30_StaticFields, ___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0)); }
	inline __StaticArrayInitTypeSizeU3D8192_tD4D01DBB09C06F57972752169C7B3716F53BAC27  get_U38B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0() const { return ___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0; }
	inline __StaticArrayInitTypeSizeU3D8192_tD4D01DBB09C06F57972752169C7B3716F53BAC27 * get_address_of_U38B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0() { return &___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0; }
	inline void set_U38B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0(__StaticArrayInitTypeSizeU3D8192_tD4D01DBB09C06F57972752169C7B3716F53BAC27  value)
	{
		___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0 = value;
	}

	inline static int32_t get_offset_of_EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30_StaticFields, ___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1)); }
	inline __StaticArrayInitTypeSizeU3D256_tCEE300537DB6B7ABD119288B61B9D34EBC4B2F98  get_EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1() const { return ___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1; }
	inline __StaticArrayInitTypeSizeU3D256_tCEE300537DB6B7ABD119288B61B9D34EBC4B2F98 * get_address_of_EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1() { return &___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1; }
	inline void set_EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1(__StaticArrayInitTypeSizeU3D256_tCEE300537DB6B7ABD119288B61B9D34EBC4B2F98  value)
	{
		___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.ExceptionArgument
struct ExceptionArgument_t11B098DD9AD21AB015871A94DB7DC9A70A10509F 
{
public:
	// System.Int32 System.ExceptionArgument::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionArgument_t11B098DD9AD21AB015871A94DB7DC9A70A10509F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Buffers.OperationStatus
struct OperationStatus_tA6C99AAB10F117093813373E98AA420F44108A47 
{
public:
	// System.Int32 System.Buffers.OperationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperationStatus_tA6C99AAB10F117093813373E98AA420F44108A47, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Text.Encodings.Web.TextEncoderSettings
struct TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366  : public RuntimeObject
{
public:
	// System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.TextEncoderSettings::_allowedCharactersBitmap
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  ____allowedCharactersBitmap_0;

public:
	inline static int32_t get_offset_of__allowedCharactersBitmap_0() { return static_cast<int32_t>(offsetof(TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366, ____allowedCharactersBitmap_0)); }
	inline AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  get__allowedCharactersBitmap_0() const { return ____allowedCharactersBitmap_0; }
	inline AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * get_address_of__allowedCharactersBitmap_0() { return &____allowedCharactersBitmap_0; }
	inline void set__allowedCharactersBitmap_0(AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  value)
	{
		____allowedCharactersBitmap_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____allowedCharactersBitmap_0))->____allowedCharacters_0), (void*)NULL);
	}
};


// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
struct AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964 
{
public:
	// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData::Data
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3  ___Data_0;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964, ___Data_0)); }
	inline U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3  get_Data_0() const { return ___Data_0; }
	inline U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3 * get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3  value)
	{
		___Data_0 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
struct AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_pinvoke
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke ___Data_0;
};
// Native definition for COM marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
struct AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_com
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com ___Data_0;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Text.Encodings.Web.TextEncoder
struct TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04  : public RuntimeObject
{
public:
	// System.Byte[][] System.Text.Encodings.Web.TextEncoder::_asciiEscape
	ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* ____asciiEscape_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encodings.Web.TextEncoder::_isAsciiCacheInitialized
	bool ____isAsciiCacheInitialized_1;
	// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData System.Text.Encodings.Web.TextEncoder::_asciiNeedsEscaping
	AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964  ____asciiNeedsEscaping_2;

public:
	inline static int32_t get_offset_of__asciiEscape_0() { return static_cast<int32_t>(offsetof(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04, ____asciiEscape_0)); }
	inline ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* get__asciiEscape_0() const { return ____asciiEscape_0; }
	inline ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D** get_address_of__asciiEscape_0() { return &____asciiEscape_0; }
	inline void set__asciiEscape_0(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* value)
	{
		____asciiEscape_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asciiEscape_0), (void*)value);
	}

	inline static int32_t get_offset_of__isAsciiCacheInitialized_1() { return static_cast<int32_t>(offsetof(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04, ____isAsciiCacheInitialized_1)); }
	inline bool get__isAsciiCacheInitialized_1() const { return ____isAsciiCacheInitialized_1; }
	inline bool* get_address_of__isAsciiCacheInitialized_1() { return &____isAsciiCacheInitialized_1; }
	inline void set__isAsciiCacheInitialized_1(bool value)
	{
		____isAsciiCacheInitialized_1 = value;
	}

	inline static int32_t get_offset_of__asciiNeedsEscaping_2() { return static_cast<int32_t>(offsetof(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04, ____asciiNeedsEscaping_2)); }
	inline AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964  get__asciiNeedsEscaping_2() const { return ____asciiNeedsEscaping_2; }
	inline AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964 * get_address_of__asciiNeedsEscaping_2() { return &____asciiNeedsEscaping_2; }
	inline void set__asciiNeedsEscaping_2(AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964  value)
	{
		____asciiNeedsEscaping_2 = value;
	}
};

struct TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_StaticFields
{
public:
	// System.Byte[] System.Text.Encodings.Web.TextEncoder::s_noEscape
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___s_noEscape_3;

public:
	inline static int32_t get_offset_of_s_noEscape_3() { return static_cast<int32_t>(offsetof(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_StaticFields, ___s_noEscape_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_s_noEscape_3() const { return ___s_noEscape_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_s_noEscape_3() { return &___s_noEscape_3; }
	inline void set_s_noEscape_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___s_noEscape_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_noEscape_3), (void*)value);
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.Text.Encodings.Web.HtmlEncoder
struct HtmlEncoder_t09B67E830AECB24356C3BBFBD9AD44952F581217  : public TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04
{
public:

public:
};


// System.Text.Encodings.Web.JavaScriptEncoder
struct JavaScriptEncoder_tEE2A7276ABD8379AD6317965D32A05CF2AD7B118  : public TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04
{
public:

public:
};


// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// System.Text.Encodings.Web.DefaultHtmlEncoder
struct DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351  : public HtmlEncoder_t09B67E830AECB24356C3BBFBD9AD44952F581217
{
public:
	// System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.DefaultHtmlEncoder::_allowedCharacters
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  ____allowedCharacters_4;

public:
	inline static int32_t get_offset_of__allowedCharacters_4() { return static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351, ____allowedCharacters_4)); }
	inline AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  get__allowedCharacters_4() const { return ____allowedCharacters_4; }
	inline AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * get_address_of__allowedCharacters_4() { return &____allowedCharacters_4; }
	inline void set__allowedCharacters_4(AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  value)
	{
		____allowedCharacters_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____allowedCharacters_4))->____allowedCharacters_0), (void*)NULL);
	}
};

struct DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields
{
public:
	// System.Text.Encodings.Web.DefaultHtmlEncoder System.Text.Encodings.Web.DefaultHtmlEncoder::Singleton
	DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * ___Singleton_5;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_quote
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_quote_6;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_ampersand
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_ampersand_7;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_lessthan
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_lessthan_8;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_greaterthan
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_greaterthan_9;

public:
	inline static int32_t get_offset_of_Singleton_5() { return static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields, ___Singleton_5)); }
	inline DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * get_Singleton_5() const { return ___Singleton_5; }
	inline DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 ** get_address_of_Singleton_5() { return &___Singleton_5; }
	inline void set_Singleton_5(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * value)
	{
		___Singleton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Singleton_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_quote_6() { return static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields, ___s_quote_6)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_quote_6() const { return ___s_quote_6; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_quote_6() { return &___s_quote_6; }
	inline void set_s_quote_6(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_quote_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_quote_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_ampersand_7() { return static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields, ___s_ampersand_7)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_ampersand_7() const { return ___s_ampersand_7; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_ampersand_7() { return &___s_ampersand_7; }
	inline void set_s_ampersand_7(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_ampersand_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ampersand_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_lessthan_8() { return static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields, ___s_lessthan_8)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_lessthan_8() const { return ___s_lessthan_8; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_lessthan_8() { return &___s_lessthan_8; }
	inline void set_s_lessthan_8(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_lessthan_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_lessthan_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_greaterthan_9() { return static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields, ___s_greaterthan_9)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_greaterthan_9() const { return ___s_greaterthan_9; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_greaterthan_9() { return &___s_greaterthan_9; }
	inline void set_s_greaterthan_9(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_greaterthan_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_greaterthan_9), (void*)value);
	}
};


// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin
struct DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A  : public JavaScriptEncoder_tEE2A7276ABD8379AD6317965D32A05CF2AD7B118
{
public:

public:
};

struct DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields
{
public:
	// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_singleton
	DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * ___s_singleton_4;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_b
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_b_5;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_t
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_t_6;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_n
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_n_7;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_f
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_f_8;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_r
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_r_9;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_back
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___s_back_10;

public:
	inline static int32_t get_offset_of_s_singleton_4() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_singleton_4)); }
	inline DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * get_s_singleton_4() const { return ___s_singleton_4; }
	inline DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A ** get_address_of_s_singleton_4() { return &___s_singleton_4; }
	inline void set_s_singleton_4(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * value)
	{
		___s_singleton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_singleton_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_b_5() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_b_5)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_b_5() const { return ___s_b_5; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_b_5() { return &___s_b_5; }
	inline void set_s_b_5(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_b_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_b_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_t_6() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_t_6)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_t_6() const { return ___s_t_6; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_t_6() { return &___s_t_6; }
	inline void set_s_t_6(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_t_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_t_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_n_7() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_n_7)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_n_7() const { return ___s_n_7; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_n_7() { return &___s_n_7; }
	inline void set_s_n_7(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_n_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_n_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_f_8() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_f_8)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_f_8() const { return ___s_f_8; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_f_8() { return &___s_f_8; }
	inline void set_s_f_8(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_f_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_f_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_r_9() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_r_9)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_r_9() const { return ___s_r_9; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_r_9() { return &___s_r_9; }
	inline void set_s_r_9(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_r_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_r_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_back_10() { return static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields, ___s_back_10)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_s_back_10() const { return ___s_back_10; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_s_back_10() { return &___s_back_10; }
	inline void set_s_back_10(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___s_back_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_back_10), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Text.Unicode.UnicodeRange[]
struct UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * m_Items[1];

public:
	inline UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* m_Items[1];

public:
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};

IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke_back(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke& marshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled);
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke_cleanup(U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com& marshaled);
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com_back(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com& marshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled);
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com_cleanup(U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com& marshaled);

// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.UInt32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t* ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_gshared_inline (ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.Byte>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t* ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.Byte>::GetPinnableReference()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t* ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method);
// System.Int32 System.ReadOnlySpan`1<System.Byte>::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Byte>::.ctor(System.Void*,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, void* ___pointer0, int32_t ___length1, const RuntimeMethod* method);
// System.Int32 System.Span`1<System.Byte>::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_gshared_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Byte>::Slice(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___start0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Byte>::CopyTo(System.Span`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___destination0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Byte>::Slice(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___start0, const RuntimeMethod* method);
// System.Span`1<!0> System.Span`1<System.Byte>::Slice(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_gshared_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, int32_t ___start0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Byte>::op_Implicit(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14_gshared (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___array0, const RuntimeMethod* method);
// System.Boolean System.ReadOnlySpan`1<System.Byte>::TryCopyTo(System.Span`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___destination0, const RuntimeMethod* method);
// System.Boolean System.ReadOnlySpan`1<System.Byte>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method);
// !0[] System.ReadOnlySpan`1<System.Byte>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method);
// System.Boolean System.ReadOnlySpan`1<System.Char>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D_gshared (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, const RuntimeMethod* method);
// System.Int32 System.Span`1<System.Char>::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, const RuntimeMethod* method);
// System.Int32 System.ReadOnlySpan`1<System.Char>::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Char>::Slice(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___start0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Char>::CopyTo(System.Span`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F_gshared (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___destination0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Char>::Slice(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___start0, const RuntimeMethod* method);
// System.Span`1<!0> System.Span`1<System.Char>::Slice(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, int32_t ___start0, const RuntimeMethod* method);
// !!0& System.Runtime.InteropServices.MemoryMarshal::GetReference<System.Char>(System.ReadOnlySpan`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar* MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742_gshared (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___span0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Byte>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_gshared_inline (const RuntimeMethod* method);
// System.Boolean System.Span`1<System.Char>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6_gshared (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Char>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_gshared_inline (const RuntimeMethod* method);
// System.Span`1<!0> System.Span`1<System.Char>::op_Implicit(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2_gshared (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___array0, const RuntimeMethod* method);
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.Char>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar* ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0& System.Span`1<System.Char>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar* Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0& System.Runtime.InteropServices.MemoryMarshal::GetReference<System.Char>(System.Span`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar* MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361_gshared (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___span0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!!1> System.Runtime.InteropServices.MemoryMarshal::Cast<System.Byte,System.UInt32>(System.ReadOnlySpan`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___span0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.UInt32>::op_Implicit(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_gshared (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___array0, const RuntimeMethod* method);
// T System.Runtime.InteropServices.MemoryMarshal::Read<System.UInt32>(System.ReadOnlySpan`1<System.Byte>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Byte>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ReadOnlySpan_1__ctor_m19A753A110C19315A508935C46EA96BA81A69AC7_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.Span`1<System.Byte>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Span_1__ctor_m373EA84BF632F6408591B525142C56CAC893C040_gshared_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Char>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ReadOnlySpan_1__ctor_m0D025D7B51DA191FC6D80F997B27442A9F967A1D_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.Span`1<System.Char>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Span_1__ctor_m72C2E4C6E7A998608E6F19E69D922E7A70F65B86_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method);
// T& System.Runtime.InteropServices.MemoryMarshal::GetReference<System.Byte>(System.ReadOnlySpan`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t* MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93_gshared (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___span0, const RuntimeMethod* method);

// System.Void System.Text.Internal.AllowedCharactersBitmap::.ctor(System.UInt32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___allowedCharacters0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.Text.Internal.AllowedCharactersBitmap::AllowCharacter(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar ___character0, const RuntimeMethod* method);
// System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidCharacter(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar ___character0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<System.UInt32> System.Text.Unicode.UnicodeHelpers::GetDefinedCharacterBitmap()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  UnicodeHelpers_GetDefinedCharacterBitmap_mC09168E637ACEFF8DC6986A529B31C0352C8D60B_inline (const RuntimeMethod* method);
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.UInt32>::get_Item(System.Int32)
inline uint32_t* ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_inline (ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  uint32_t* (*) (ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8 *, int32_t, const RuntimeMethod*))ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidUndefinedCharacters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, const RuntimeMethod* method);
// System.Object System.Array::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Array_Clone_m3C566B3D3F4333212411BD7C3B61D798BADB3F3C (RuntimeArray * __this, const RuntimeMethod* method);
// System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, const RuntimeMethod* method);
// System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsUnicodeScalarAllowed(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_inline (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, int32_t ___unicodeScalar0, const RuntimeMethod* method);
// System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsCharacterAllowed(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar ___character0, const RuntimeMethod* method);
// System.Int32 System.Text.Internal.AllowedCharactersBitmap::FindFirstCharacterToEncode(System.Char*,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar* ___text0, int32_t ___textLength1, const RuntimeMethod* method);
// System.Void System.Text.Encodings.Web.HtmlEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlEncoder__ctor_m7A58A67ABD105671C4D6B8CF1CB4A06C5BD61FE5 (HtmlEncoder_t09B67E830AECB24356C3BBFBD9AD44952F581217 * __this, const RuntimeMethod* method);
// System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.TextEncoderSettings::GetAllowedCharacters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  TextEncoderSettings_GetAllowedCharacters_m2A3EB325C9034B4C0A61E78D85B8FDCAAC866727 (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * __this, const RuntimeMethod* method);
// System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::ForbidHtmlCharacters(System.Text.Internal.AllowedCharactersBitmap)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultHtmlEncoder_ForbidHtmlCharacters_m79ED10B3D6F5D04E799E20AFAE7ABEE53537FFAB (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  ___allowedCharacters0, const RuntimeMethod* method);
// System.Boolean System.Text.Unicode.UnicodeHelpers::IsSupplementaryCodePoint(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456_inline (int32_t ___scalar0, const RuntimeMethod* method);
// System.Boolean System.Text.Encodings.Web.TextEncoder::TryWriteScalarAsChar(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5_inline (int32_t ___unicodeScalar0, Il2CppChar* ___destination1, int32_t ___destinationLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method);
// System.Boolean System.Text.Encodings.Web.TextEncoder::TryCopyCharacters(System.Char[],System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3 (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___source0, Il2CppChar* ___destination1, int32_t ___destinationLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method);
// System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m3390D134471CC26D9E0855BE7352D2056DB8C566 (int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___bufferLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method);
// System.Char System.HexConverter::ToCharUpper(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline (int32_t ___value0, const RuntimeMethod* method);
// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::get_BasicLatin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * UnicodeRanges_get_BasicLatin_mA46E04A614BC261AFCE0DB4A27734D38BE369F7B (const RuntimeMethod* method);
// System.Void System.Text.Encodings.Web.TextEncoderSettings::.ctor(System.Text.Unicode.UnicodeRange[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * __this, UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* ___allowedRanges0, const RuntimeMethod* method);
// System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.ctor(System.Text.Encodings.Web.TextEncoderSettings)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31 (DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * __this, TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * ___settings0, const RuntimeMethod* method);
// System.Char[] System.String::ToCharArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C (String_t* __this, const RuntimeMethod* method);
// System.Void System.Text.Encodings.Web.JavaScriptEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JavaScriptEncoder__ctor_mEC23CDF138E68E96490421C2A6C9A5C8E34595E8 (JavaScriptEncoder_tEE2A7276ABD8379AD6317965D32A05CF2AD7B118 * __this, const RuntimeMethod* method);
// System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::NeedsEscaping(System.Char)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mBCD92F26CD88B2855FA734E26B0DBB18F0763482_inline (Il2CppChar ___value0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<System.Byte> System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::get_AllowList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5 (const RuntimeMethod* method);
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.Byte>::get_Item(System.Int32)
inline uint8_t* ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  uint8_t* (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, int32_t, const RuntimeMethod*))ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_gshared_inline)(__this, ___index0, method);
}
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.Byte>::GetPinnableReference()
inline uint8_t* ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method)
{
	return ((  uint8_t* (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, const RuntimeMethod*))ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_gshared)(__this, method);
}
// System.Int32 System.ReadOnlySpan`1<System.Byte>::get_Length()
inline int32_t ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, const RuntimeMethod*))ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_gshared_inline)(__this, method);
}
// System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_m128A8D90D65C5C6E0F1953E7EAE26D9636139172 (int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___length2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Byte>::.ctor(System.Void*,System.Int32)
inline void ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, void* ___pointer0, int32_t ___length1, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, void*, int32_t, const RuntimeMethod*))ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_gshared_inline)(__this, ___pointer0, ___length1, method);
}
// System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultJavaScriptEncoderBasicLatin__ctor_mA96626B6D6F2C7A9FAC381D4D93D908D8F249874 (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * __this, const RuntimeMethod* method);
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1 (Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71 * __this, const RuntimeMethod* method);
// System.Void System.Text.Encodings.Web.TextEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoder__ctor_m07027CA56CA490B41348EC294735935086AAE640 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, const RuntimeMethod* method);
// System.Void System.Text.Unicode.UnicodeHelpers::GetUtf16SurrogatePairFromAstralScalarValue(System.Int32,System.Char&,System.Char&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC7309772F2EDE8846DFBFFC499EFBBA3A40E4586 (int32_t ___scalar0, Il2CppChar* ___highSurrogate1, Il2CppChar* ___lowSurrogate2, const RuntimeMethod* method);
// System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedSingleCharacter(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA (int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___length2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method);
// System.Int32 System.Span`1<System.Byte>::get_Length()
inline int32_t Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *, const RuntimeMethod*))Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_gshared_inline)(__this, method);
}
// System.Boolean System.Text.UnicodeUtility::IsAsciiCodePoint(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D_inline (uint32_t ___value0, const RuntimeMethod* method);
// System.Byte[] System.Text.Encodings.Web.TextEncoder::GetAsciiEncoding(System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TextEncoder_GetAsciiEncoding_m6FF6E556B2432F8F8FB0E9568B6E9C398BBD0829_inline (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, uint8_t ___value0, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Byte>::Slice(System.Int32,System.Int32)
inline ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___start0, int32_t ___length1, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, int32_t, int32_t, const RuntimeMethod*))ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_gshared_inline)(__this, ___start0, ___length1, method);
}
// System.Void System.ReadOnlySpan`1<System.Byte>::CopyTo(System.Span`1<!0>)
inline void ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___destination0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 , const RuntimeMethod*))ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71_gshared)(__this, ___destination0, method);
}
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Byte>::Slice(System.Int32)
inline ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___start0, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, int32_t, const RuntimeMethod*))ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_gshared_inline)(__this, ___start0, method);
}
// System.Span`1<!0> System.Span`1<System.Byte>::Slice(System.Int32)
inline Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, int32_t ___start0, const RuntimeMethod* method)
{
	return ((  Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  (*) (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *, int32_t, const RuntimeMethod*))Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_gshared_inline)(__this, ___start0, method);
}
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Byte>::op_Implicit(!0[])
inline ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___array0, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  (*) (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, const RuntimeMethod*))ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14_gshared)(___array0, method);
}
// System.Boolean System.ReadOnlySpan`1<System.Byte>::TryCopyTo(System.Span`1<!0>)
inline bool ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___destination0, const RuntimeMethod* method)
{
	return ((  bool (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 , const RuntimeMethod*))ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B_gshared)(__this, ___destination0, method);
}
// System.Buffers.OperationStatus System.Text.Unicode.UnicodeHelpers::DecodeScalarValueFromUtf8(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnicodeHelpers_DecodeScalarValueFromUtf8_m12AC2FF4FA54E2207D7005136C5EF4ECAC281576 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, uint32_t* ___result1, int32_t* ___bytesConsumed2, const RuntimeMethod* method);
// System.Boolean System.ReadOnlySpan`1<System.Byte>::get_IsEmpty()
inline bool ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, const RuntimeMethod*))ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D_gshared)(__this, method);
}
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E (const RuntimeMethod* method);
// !0[] System.ReadOnlySpan`1<System.Byte>::ToArray()
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, const RuntimeMethod*))ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C_gshared)(__this, method);
}
// System.Boolean System.ReadOnlySpan`1<System.Char>::get_IsEmpty()
inline bool ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, const RuntimeMethod*))ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D_gshared)(__this, method);
}
// System.Int32 System.Span`1<System.Char>::get_Length()
inline int32_t Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *, const RuntimeMethod*))Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_gshared_inline)(__this, method);
}
// System.Int32 System.ReadOnlySpan`1<System.Char>::get_Length()
inline int32_t ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, const RuntimeMethod*))ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_gshared_inline)(__this, method);
}
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Char>::Slice(System.Int32,System.Int32)
inline ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___start0, int32_t ___length1, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, int32_t, int32_t, const RuntimeMethod*))ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_gshared_inline)(__this, ___start0, ___length1, method);
}
// System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.ReadOnlySpan`1<System.Char>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_FindFirstCharacterToEncode_m2423424CDE31B11CA2840777CD0C0422CD829F71 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___text0, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Char>::CopyTo(System.Span`1<!0>)
inline void ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___destination0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D , const RuntimeMethod*))ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F_gshared)(__this, ___destination0, method);
}
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.Char>::Slice(System.Int32)
inline ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___start0, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, int32_t, const RuntimeMethod*))ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_gshared_inline)(__this, ___start0, method);
}
// System.Span`1<!0> System.Span`1<System.Char>::Slice(System.Int32)
inline Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, int32_t ___start0, const RuntimeMethod* method)
{
	return ((  Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  (*) (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *, int32_t, const RuntimeMethod*))Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_gshared_inline)(__this, ___start0, method);
}
// System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::<Encode>g__EncodeCore|15_0(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_m97E09AEBAFA1C02707FF382DB82B6A2E30BF4931 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___source0, Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___destination1, int32_t* ___charsConsumed2, int32_t* ___charsWritten3, bool ___isFinalBlock4, const RuntimeMethod* method);
// !!0& System.Runtime.InteropServices.MemoryMarshal::GetReference<System.Char>(System.ReadOnlySpan`1<!!0>)
inline Il2CppChar* MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742 (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___span0, const RuntimeMethod* method)
{
	return ((  Il2CppChar* (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 , const RuntimeMethod*))MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742_gshared)(___span0, method);
}
// System.Void System.Text.Encodings.Web.TextEncoder::InitializeAsciiCache()
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void TextEncoder_InitializeAsciiCache_m1D9E9EDE89AA59C2A406239D1C722284E25B5398 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, const RuntimeMethod* method);
// System.Boolean System.Text.Encodings.Web.TextEncoder::DoesAsciiNeedEncoding(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TextEncoder_DoesAsciiNeedEncoding_m0525D9CBBC3FA4494ECD7D440EE01384F64DF27F_inline (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, uint32_t ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Byte>()
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_inline (const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (const RuntimeMethod*))Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_gshared_inline)(method);
}
// System.Boolean System.Span`1<System.Char>::get_IsEmpty()
inline bool Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6 (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *, const RuntimeMethod*))Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6_gshared)(__this, method);
}
// !!0[] System.Array::Empty<System.Char>()
inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_inline (const RuntimeMethod* method)
{
	return ((  CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* (*) (const RuntimeMethod*))Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_gshared_inline)(method);
}
// System.Span`1<!0> System.Span`1<System.Char>::op_Implicit(!0[])
inline Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2 (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___array0, const RuntimeMethod* method)
{
	return ((  Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  (*) (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*, const RuntimeMethod*))Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2_gshared)(___array0, method);
}
// !0& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1<System.Char>::get_Item(System.Int32)
inline Il2CppChar* ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Il2CppChar* (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, int32_t, const RuntimeMethod*))ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_gshared_inline)(__this, ___index0, method);
}
// System.Boolean System.Text.UnicodeUtility::IsSurrogateCodePoint(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsSurrogateCodePoint_m1C61245755AFFC5F88AEFDE2511BD42EC2E35471_inline (uint32_t ___value0, const RuntimeMethod* method);
// !0& System.Span`1<System.Char>::get_Item(System.Int32)
inline Il2CppChar* Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Il2CppChar* (*) (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *, int32_t, const RuntimeMethod*))Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_gshared_inline)(__this, ___index0, method);
}
// System.Boolean System.Text.UnicodeUtility::IsHighSurrogateCodePoint(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsHighSurrogateCodePoint_mC6FC7E1678F3638CDDE48C9DE27C7ECE51D16654_inline (uint32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.Text.UnicodeUtility::IsLowSurrogateCodePoint(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsLowSurrogateCodePoint_mFC9196588FB51D463621246A555CDF8EBD7C4F89_inline (uint32_t ___value0, const RuntimeMethod* method);
// System.UInt32 System.Text.UnicodeUtility::GetScalarFromUtf16SurrogatePair(System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6866E0E09B45BF1CDE2BB7865AF20F6460C752D0 (uint32_t ___highSurrogateCodePoint0, uint32_t ___lowSurrogateCodePoint1, const RuntimeMethod* method);
// !!0& System.Runtime.InteropServices.MemoryMarshal::GetReference<System.Char>(System.Span`1<!!0>)
inline Il2CppChar* MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361 (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___span0, const RuntimeMethod* method)
{
	return ((  Il2CppChar* (*) (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D , const RuntimeMethod*))MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361_gshared)(___span0, method);
}
// System.Int32 System.Text.UnicodeUtility::GetUtf16SequenceLength(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnicodeUtility_GetUtf16SequenceLength_mBAB104741499D1D4454BF674A1A4B380E3DA9A9E (uint32_t ___value0, const RuntimeMethod* method);
// System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::CreateNew()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  AllowedCharactersBitmap_CreateNew_m881B89C811D45EBD8281DA9FF3CA7957833A9252 (const RuntimeMethod* method);
// System.Int32 System.Text.Unicode.UnicodeRange::get_FirstCodePoint()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UnicodeRange_get_FirstCodePoint_mE2ECE0C7C54A3047DF4DBA68271FBF8067BCEB26_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, const RuntimeMethod* method);
// System.Int32 System.Text.Unicode.UnicodeRange::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UnicodeRange_get_Length_mD690963E9ABE4A2F5731E5CF27C00292C7FFC696_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, const RuntimeMethod* method);
// System.ReadOnlySpan`1<System.Byte> System.Text.Unicode.UnicodeHelpers::get_DefinedCharsBitmapSpan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B (const RuntimeMethod* method);
// System.UInt32 System.Buffers.Binary.BinaryPrimitives::ReadUInt32LittleEndian(System.ReadOnlySpan`1<System.Byte>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t BinaryPrimitives_ReadUInt32LittleEndian_mEE46641BC73CAACA64F2952CD791BE96F5DB44F4_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, const RuntimeMethod* method);
// System.Boolean System.Text.UnicodeUtility::IsInRangeInclusive(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline (uint32_t ___value0, uint32_t ___lowerBound1, uint32_t ___upperBound2, const RuntimeMethod* method);
// System.ReadOnlySpan`1<!!1> System.Runtime.InteropServices.MemoryMarshal::Cast<System.Byte,System.UInt32>(System.ReadOnlySpan`1<!!0>)
inline ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___span0, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 , const RuntimeMethod*))MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_gshared)(___span0, method);
}
// System.ReadOnlySpan`1<!0> System.ReadOnlySpan`1<System.UInt32>::op_Implicit(!0[])
inline ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387 (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___array0, const RuntimeMethod* method)
{
	return ((  ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  (*) (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*, const RuntimeMethod*))ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_gshared)(___array0, method);
}
// System.UInt32[] System.Text.Unicode.UnicodeHelpers::CreateDefinedCharacterBitmapMachineEndian()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m5276C6DA38121F0033E91B53041F9CBFAC7FD9CA (const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.Text.Unicode.UnicodeRange::set_FirstCodePoint(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnicodeRange_set_FirstCodePoint_m529C6CC5756D4A2AC163B3A6D274C3651CEBA345_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Text.Unicode.UnicodeRange::set_Length(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnicodeRange_set_Length_m8994FAD9DDDC18EE1F2757E5444584EA3DDF89E4_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Text.Unicode.UnicodeRange::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2 (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___firstCodePoint0, int32_t ___length1, const RuntimeMethod* method);
// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRange::Create(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B (Il2CppChar ___firstCharacter0, Il2CppChar ___lastCharacter1, const RuntimeMethod* method);
// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::CreateRange(System.Text.Unicode.UnicodeRange&,System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * UnicodeRanges_CreateRange_mDD642B176A42F1DE3EEE990E612E116293DE340C (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** ___range0, Il2CppChar ___first1, Il2CppChar ___last2, const RuntimeMethod* method);
// T System.Runtime.InteropServices.MemoryMarshal::Read<System.UInt32>(System.ReadOnlySpan`1<System.Byte>)
inline uint32_t MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, const RuntimeMethod* method)
{
	return ((  uint32_t (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 , const RuntimeMethod*))MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_gshared_inline)(___source0, method);
}
// System.UInt32 System.Buffers.Binary.BinaryPrimitives::ReverseEndianness(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20_inline (uint32_t ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowIndexOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC (const RuntimeMethod* method);
// System.Void* System.IntPtr::ToPointer()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void* IntPtr_ToPointer_m5C7CE32B14B6E30467B378052FEA25300833C61F_inline (intptr_t* __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentException_InvalidTypeWithPointersNotSupported(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4 (Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException(System.ExceptionArgument)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5 (int32_t ___argument0, const RuntimeMethod* method);
// System.Void System.IntPtr::.ctor(System.Void*)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void IntPtr__ctor_mBB7AF6DA6350129AD6422DE474FD52F715CC0C40_inline (intptr_t* __this, void* ___value0, const RuntimeMethod* method);
// System.Void System.ReadOnlySpan`1<System.Byte>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
inline void ReadOnlySpan_1__ctor_m19A753A110C19315A508935C46EA96BA81A69AC7_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *, intptr_t, int32_t, const RuntimeMethod*))ReadOnlySpan_1__ctor_m19A753A110C19315A508935C46EA96BA81A69AC7_gshared_inline)(__this, ___pinnable0, ___byteOffset1, ___length2, method);
}
// System.Void System.Span`1<System.Byte>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
inline void Span_1__ctor_m373EA84BF632F6408591B525142C56CAC893C040_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *, intptr_t, int32_t, const RuntimeMethod*))Span_1__ctor_m373EA84BF632F6408591B525142C56CAC893C040_gshared_inline)(__this, ___pinnable0, ___byteOffset1, ___length2, method);
}
// System.Void System.ReadOnlySpan`1<System.Char>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
inline void ReadOnlySpan_1__ctor_m0D025D7B51DA191FC6D80F997B27442A9F967A1D_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *, intptr_t, int32_t, const RuntimeMethod*))ReadOnlySpan_1__ctor_m0D025D7B51DA191FC6D80F997B27442A9F967A1D_gshared_inline)(__this, ___pinnable0, ___byteOffset1, ___length2, method);
}
// System.Void System.Span`1<System.Char>::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
inline void Span_1__ctor_m72C2E4C6E7A998608E6F19E69D922E7A70F65B86_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *, intptr_t, int32_t, const RuntimeMethod*))Span_1__ctor_m72C2E4C6E7A998608E6F19E69D922E7A70F65B86_gshared_inline)(__this, ___pinnable0, ___byteOffset1, ___length2, method);
}
// T& System.Runtime.InteropServices.MemoryMarshal::GetReference<System.Byte>(System.ReadOnlySpan`1<T>)
inline uint8_t* MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___span0, const RuntimeMethod* method)
{
	return ((  uint8_t* (*) (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 , const RuntimeMethod*))MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93_gshared)(___span0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Text.Internal.AllowedCharactersBitmap
IL2CPP_EXTERN_C void AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshal_pinvoke(const AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC& unmarshaled, AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_pinvoke& marshaled)
{
	marshaled.____allowedCharacters_0 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_UI4, unmarshaled.get__allowedCharacters_0());
}
IL2CPP_EXTERN_C void AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshal_pinvoke_back(const AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_pinvoke& marshaled, AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set__allowedCharacters_0((UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_UI4, UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, marshaled.____allowedCharacters_0));
}
// Conversion method for clean up from marshalling of: System.Text.Internal.AllowedCharactersBitmap
IL2CPP_EXTERN_C void AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshal_pinvoke_cleanup(AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.____allowedCharacters_0);
	marshaled.____allowedCharacters_0 = NULL;
}
// Conversion methods for marshalling of: System.Text.Internal.AllowedCharactersBitmap
IL2CPP_EXTERN_C void AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshal_com(const AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC& unmarshaled, AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_com& marshaled)
{
	marshaled.____allowedCharacters_0 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_UI4, unmarshaled.get__allowedCharacters_0());
}
IL2CPP_EXTERN_C void AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshal_com_back(const AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_com& marshaled, AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set__allowedCharacters_0((UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_UI4, UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, marshaled.____allowedCharacters_0));
}
// Conversion method for clean up from marshalling of: System.Text.Internal.AllowedCharactersBitmap
IL2CPP_EXTERN_C void AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshal_com_cleanup(AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC_marshaled_com& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.____allowedCharacters_0);
	marshaled.____allowedCharacters_0 = NULL;
}
// System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::CreateNew()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  AllowedCharactersBitmap_CreateNew_m881B89C811D45EBD8281DA9FF3CA7957833A9252 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_0 = (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)SZArrayNew(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var, (uint32_t)((int32_t)2048));
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_1;
		memset((&L_1), 0, sizeof(L_1));
		AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Text.Internal.AllowedCharactersBitmap::.ctor(System.UInt32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___allowedCharacters0, const RuntimeMethod* method)
{
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_0 = ___allowedCharacters0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCDB258E32AF5134A2B31FE4D1EE6C0E30C9B5E29)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8_RuntimeMethod_var)));
	}

IL_000e:
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_2 = ___allowedCharacters0;
		__this->set__allowedCharacters_0(L_2);
		return;
	}
}
IL2CPP_EXTERN_C  void AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8_AdjustorThunk (RuntimeObject * __this, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___allowedCharacters0, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8(_thisAdjusted, ___allowedCharacters0, method);
}
// System.Void System.Text.Internal.AllowedCharactersBitmap::AllowCharacter(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar ___character0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Il2CppChar L_0 = ___character0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = ((int32_t)((int32_t)L_1>>(int32_t)5));
		int32_t L_2 = V_0;
		V_2 = ((int32_t)((int32_t)L_2&(int32_t)((int32_t)31)));
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_3 = __this->get__allowedCharacters_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		uint32_t* L_5 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		int32_t L_6 = *((uint32_t*)L_5);
		int32_t L_7 = V_2;
		*((int32_t*)L_5) = (int32_t)((int32_t)((int32_t)L_6|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)31)))))));
		return;
	}
}
IL2CPP_EXTERN_C  void AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3_AdjustorThunk (RuntimeObject * __this, Il2CppChar ___character0, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3(_thisAdjusted, ___character0, method);
}
// System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidCharacter(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar ___character0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Il2CppChar L_0 = ___character0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = ((int32_t)((int32_t)L_1>>(int32_t)5));
		int32_t L_2 = V_0;
		V_2 = ((int32_t)((int32_t)L_2&(int32_t)((int32_t)31)));
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_3 = __this->get__allowedCharacters_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		uint32_t* L_5 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		int32_t L_6 = *((uint32_t*)L_5);
		int32_t L_7 = V_2;
		*((int32_t*)L_5) = (int32_t)((int32_t)((int32_t)L_6&(int32_t)((~((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)31)))))))));
		return;
	}
}
IL2CPP_EXTERN_C  void AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB_AdjustorThunk (RuntimeObject * __this, Il2CppChar ___character0, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB(_thisAdjusted, ___character0, method);
}
// System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidUndefinedCharacters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  L_0;
		L_0 = UnicodeHelpers_GetDefinedCharacterBitmap_mC09168E637ACEFF8DC6986A529B31C0352C8D60B_inline(/*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0027;
	}

IL_000a:
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_1 = __this->get__allowedCharacters_0();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		uint32_t* L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)));
		int32_t L_4 = *((uint32_t*)L_3);
		int32_t L_5 = V_1;
		uint32_t* L_6;
		L_6 = ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_inline((ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8 *)(&V_0), L_5, /*hidden argument*/ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_RuntimeMethod_var);
		int32_t L_7 = *((uint32_t*)L_6);
		*((int32_t*)L_3) = (int32_t)((int32_t)((int32_t)L_4&(int32_t)L_7));
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_1;
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_10 = __this->get__allowedCharacters_0();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
IL2CPP_EXTERN_C  void AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95(_thisAdjusted, method);
}
// System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_0 = __this->get__allowedCharacters_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_0);
		RuntimeObject * L_1;
		L_1 = Array_Clone_m3C566B3D3F4333212411BD7C3B61D798BADB3F3C((RuntimeArray *)(RuntimeArray *)L_0, /*hidden argument*/NULL);
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_2;
		memset((&L_2), 0, sizeof(L_2));
		AllowedCharactersBitmap__ctor_mAE91BB57E5D77AC0C6ECCE437483993A925173D8((&L_2), ((UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)Castclass((RuntimeObject*)L_1, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  _returnValue;
	_returnValue = AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsCharacterAllowed(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar ___character0, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = ___character0;
		bool L_1;
		L_1 = AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_inline((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393_AdjustorThunk (RuntimeObject * __this, Il2CppChar ___character0, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	bool _returnValue;
	_returnValue = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393(_thisAdjusted, ___character0, method);
	return _returnValue;
}
// System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsUnicodeScalarAllowed(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, int32_t ___unicodeScalar0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___unicodeScalar0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)5));
		int32_t L_1 = ___unicodeScalar0;
		V_1 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)));
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_2 = __this->get__allowedCharacters_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int32_t L_6 = V_1;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)31)))))))) <= ((uint32_t)0)))? 1 : 0);
	}
}
IL2CPP_EXTERN_C  bool AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_AdjustorThunk (RuntimeObject * __this, int32_t ___unicodeScalar0, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	bool _returnValue;
	_returnValue = AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_inline(_thisAdjusted, ___unicodeScalar0, method);
	return _returnValue;
}
// System.Int32 System.Text.Internal.AllowedCharactersBitmap::FindFirstCharacterToEncode(System.Char*,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6 (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, Il2CppChar* ___text0, int32_t ___textLength1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_00b4;
	}

IL_0007:
	{
		Il2CppChar* L_0 = ___text0;
		int32_t L_1 = V_0;
		int32_t L_2 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_0, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)2)))));
		bool L_3;
		L_3 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_4 = ___text0;
		int32_t L_5 = V_0;
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		V_0 = L_6;
		int32_t L_7 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_4, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_6), (int32_t)2)))));
		bool L_8;
		L_8 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_9 = ___text0;
		int32_t L_10 = V_0;
		int32_t L_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		V_0 = L_11;
		int32_t L_12 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_9, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_11), (int32_t)2)))));
		bool L_13;
		L_13 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_14 = ___text0;
		int32_t L_15 = V_0;
		int32_t L_16 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		V_0 = L_16;
		int32_t L_17 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_14, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_16), (int32_t)2)))));
		bool L_18;
		L_18 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_19 = ___text0;
		int32_t L_20 = V_0;
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
		V_0 = L_21;
		int32_t L_22 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_19, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_21), (int32_t)2)))));
		bool L_23;
		L_23 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_24 = ___text0;
		int32_t L_25 = V_0;
		int32_t L_26 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
		V_0 = L_26;
		int32_t L_27 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_24, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_26), (int32_t)2)))));
		bool L_28;
		L_28 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_29 = ___text0;
		int32_t L_30 = V_0;
		int32_t L_31 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		V_0 = L_31;
		int32_t L_32 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_29, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_31), (int32_t)2)))));
		bool L_33;
		L_33 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_34 = ___text0;
		int32_t L_35 = V_0;
		int32_t L_36 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
		V_0 = L_36;
		int32_t L_37 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_34, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_36), (int32_t)2)))));
		bool L_38;
		L_38 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
	}

IL_00b4:
	{
		int32_t L_40 = V_0;
		int32_t L_41 = ___textLength1;
		if ((((int32_t)L_40) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)8)))))
		{
			goto IL_0007;
		}
	}
	{
		goto IL_010b;
	}

IL_00bf:
	{
		Il2CppChar* L_42 = ___text0;
		int32_t L_43 = V_0;
		int32_t L_44 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_42, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_43), (int32_t)2)))));
		bool L_45;
		L_45 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_46 = ___text0;
		int32_t L_47 = V_0;
		int32_t L_48 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
		V_0 = L_48;
		int32_t L_49 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_46, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_48), (int32_t)2)))));
		bool L_50;
		L_50 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_51 = ___text0;
		int32_t L_52 = V_0;
		int32_t L_53 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
		V_0 = L_53;
		int32_t L_54 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_51, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_53), (int32_t)2)))));
		bool L_55;
		L_55 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_012c;
		}
	}
	{
		Il2CppChar* L_56 = ___text0;
		int32_t L_57 = V_0;
		int32_t L_58 = ((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
		V_0 = L_58;
		int32_t L_59 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_56, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_58), (int32_t)2)))));
		bool L_60;
		L_60 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_61 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1));
	}

IL_010b:
	{
		int32_t L_62 = V_0;
		int32_t L_63 = ___textLength1;
		if ((((int32_t)L_62) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_63, (int32_t)4)))))
		{
			goto IL_00bf;
		}
	}
	{
		goto IL_0126;
	}

IL_0113:
	{
		Il2CppChar* L_64 = ___text0;
		int32_t L_65 = V_0;
		int32_t L_66 = *((uint16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_64, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_65), (int32_t)2)))));
		bool L_67;
		L_67 = AllowedCharactersBitmap_IsCharacterAllowed_m8CAA5DD1B6E1EC5BF98C590C3C85AFACB2DF1393((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)__this, L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_68 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_68, (int32_t)1));
	}

IL_0126:
	{
		int32_t L_69 = V_0;
		int32_t L_70 = ___textLength1;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_0113;
		}
	}
	{
		V_0 = (-1);
	}

IL_012c:
	{
		int32_t L_71 = V_0;
		return L_71;
	}
}
IL2CPP_EXTERN_C  int32_t AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6_AdjustorThunk (RuntimeObject * __this, Il2CppChar* ___text0, int32_t ___textLength1, const RuntimeMethod* method)
{
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6(_thisAdjusted, ___text0, ___textLength1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.ctor(System.Text.Encodings.Web.TextEncoderSettings)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31 (DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * __this, TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * ___settings0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlEncoder__ctor_m7A58A67ABD105671C4D6B8CF1CB4A06C5BD61FE5(__this, /*hidden argument*/NULL);
		TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * L_0 = ___settings0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7DF882FBCC2A230A62D22FF65024431A34A858A2)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31_RuntimeMethod_var)));
	}

IL_0014:
	{
		TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * L_2 = ___settings0;
		NullCheck(L_2);
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_3;
		L_3 = TextEncoderSettings_GetAllowedCharacters_m2A3EB325C9034B4C0A61E78D85B8FDCAAC866727(L_2, /*hidden argument*/NULL);
		__this->set__allowedCharacters_4(L_3);
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * L_4 = __this->get_address_of__allowedCharacters_4();
		AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)L_4, /*hidden argument*/NULL);
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_5 = __this->get__allowedCharacters_4();
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		DefaultHtmlEncoder_ForbidHtmlCharacters_m79ED10B3D6F5D04E799E20AFAE7ABEE53537FFAB(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::ForbidHtmlCharacters(System.Text.Internal.AllowedCharactersBitmap)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultHtmlEncoder_ForbidHtmlCharacters_m79ED10B3D6F5D04E799E20AFAE7ABEE53537FFAB (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  ___allowedCharacters0, const RuntimeMethod* method)
{
	{
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&___allowedCharacters0), ((int32_t)60), /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&___allowedCharacters0), ((int32_t)62), /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&___allowedCharacters0), ((int32_t)38), /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&___allowedCharacters0), ((int32_t)39), /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&___allowedCharacters0), ((int32_t)34), /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&___allowedCharacters0), ((int32_t)43), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::WillEncode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultHtmlEncoder_WillEncode_m9DFF3D842B2D43F4ADA5003007490809FBEA36F5 (DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * __this, int32_t ___unicodeScalar0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___unicodeScalar0;
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)1;
	}

IL_000a:
	{
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * L_2 = __this->get_address_of__allowedCharacters_4();
		int32_t L_3 = ___unicodeScalar0;
		bool L_4;
		L_4 = AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_inline((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)L_2, L_3, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Text.Encodings.Web.DefaultHtmlEncoder::FindFirstCharacterToEncode(System.Char*,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DefaultHtmlEncoder_FindFirstCharacterToEncode_m1EB2DC500055222C3CE781D106E76674730CAF74 (DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * __this, Il2CppChar* ___text0, int32_t ___textLength1, const RuntimeMethod* method)
{
	{
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * L_0 = __this->get_address_of__allowedCharacters_4();
		Il2CppChar* L_1 = ___text0;
		int32_t L_2 = ___textLength1;
		int32_t L_3;
		L_3 = AllowedCharactersBitmap_FindFirstCharacterToEncode_m2EEA9A94D7B90DAC04D1C5B3AF70B845604293D6((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)L_0, (Il2CppChar*)(Il2CppChar*)L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultHtmlEncoder_TryEncodeUnicodeScalar_m68F407599404602732C186AD2A2BD84E058C2065 (DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * __this, int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___bufferLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar* L_0 = ___buffer1;
		if ((!(((uintptr_t)L_0) == ((uintptr_t)((uintptr_t)0)))))
		{
			goto IL_0010;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC263EA29ADF3548CFEBC57B532EED28451A56C10)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DefaultHtmlEncoder_TryEncodeUnicodeScalar_m68F407599404602732C186AD2A2BD84E058C2065_RuntimeMethod_var)));
	}

IL_0010:
	{
		int32_t L_2 = ___unicodeScalar0;
		bool L_3;
		L_3 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_2);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_4 = ___unicodeScalar0;
		Il2CppChar* L_5 = ___buffer1;
		int32_t L_6 = ___bufferLength2;
		int32_t* L_7 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5_inline(L_4, (Il2CppChar*)(Il2CppChar*)L_5, L_6, (int32_t*)L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0024:
	{
		int32_t L_9 = ___unicodeScalar0;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_10 = ((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->get_s_quote_6();
		Il2CppChar* L_11 = ___buffer1;
		int32_t L_12 = ___bufferLength2;
		int32_t* L_13 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3(L_10, (Il2CppChar*)(Il2CppChar*)L_11, L_12, (int32_t*)L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0038:
	{
		int32_t L_15 = ___unicodeScalar0;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)38)))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_16 = ((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->get_s_ampersand_7();
		Il2CppChar* L_17 = ___buffer1;
		int32_t L_18 = ___bufferLength2;
		int32_t* L_19 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_20;
		L_20 = TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3(L_16, (Il2CppChar*)(Il2CppChar*)L_17, L_18, (int32_t*)L_19, /*hidden argument*/NULL);
		return L_20;
	}

IL_004c:
	{
		int32_t L_21 = ___unicodeScalar0;
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_22 = ((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->get_s_lessthan_8();
		Il2CppChar* L_23 = ___buffer1;
		int32_t L_24 = ___bufferLength2;
		int32_t* L_25 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_26;
		L_26 = TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3(L_22, (Il2CppChar*)(Il2CppChar*)L_23, L_24, (int32_t*)L_25, /*hidden argument*/NULL);
		return L_26;
	}

IL_0060:
	{
		int32_t L_27 = ___unicodeScalar0;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0074;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_28 = ((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->get_s_greaterthan_9();
		Il2CppChar* L_29 = ___buffer1;
		int32_t L_30 = ___bufferLength2;
		int32_t* L_31 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_32;
		L_32 = TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3(L_28, (Il2CppChar*)(Il2CppChar*)L_29, L_30, (int32_t*)L_31, /*hidden argument*/NULL);
		return L_32;
	}

IL_0074:
	{
		int32_t L_33 = ___unicodeScalar0;
		Il2CppChar* L_34 = ___buffer1;
		int32_t L_35 = ___bufferLength2;
		int32_t* L_36 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		bool L_37;
		L_37 = DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m3390D134471CC26D9E0855BE7352D2056DB8C566(L_33, (Il2CppChar*)(Il2CppChar*)L_34, L_35, (int32_t*)L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m3390D134471CC26D9E0855BE7352D2056DB8C566 (int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___bufferLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___unicodeScalar0;
		V_1 = L_0;
	}

IL_0004:
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
		int32_t L_2 = V_1;
		V_1 = ((int32_t)((int32_t)L_2>>(int32_t)4));
		int32_t L_3 = V_1;
		if (L_3)
		{
			goto IL_0004;
		}
	}
	{
		int32_t* L_4 = ___numberOfCharactersWritten3;
		int32_t L_5 = V_0;
		*((int32_t*)L_4) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)4));
		int32_t L_6 = V_0;
		int32_t L_7 = ___bufferLength2;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)4))) <= ((int32_t)L_7)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t* L_8 = ___numberOfCharactersWritten3;
		*((int32_t*)L_8) = (int32_t)0;
		return (bool)0;
	}

IL_001f:
	{
		Il2CppChar* L_9 = ___buffer1;
		*((int16_t*)L_9) = (int16_t)((int32_t)38);
		Il2CppChar* L_10 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_10, (int32_t)2));
		Il2CppChar* L_11 = ___buffer1;
		*((int16_t*)L_11) = (int16_t)((int32_t)35);
		Il2CppChar* L_12 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_12, (int32_t)2));
		Il2CppChar* L_13 = ___buffer1;
		*((int16_t*)L_13) = (int16_t)((int32_t)120);
		Il2CppChar* L_14 = ___buffer1;
		int32_t L_15 = V_0;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_14, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_15), (int32_t)2))));
	}

IL_003d:
	{
		Il2CppChar* L_16 = ___buffer1;
		int32_t L_17 = ___unicodeScalar0;
		Il2CppChar L_18;
		L_18 = HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline(L_17, /*hidden argument*/NULL);
		*((int16_t*)L_16) = (int16_t)L_18;
		int32_t L_19 = ___unicodeScalar0;
		___unicodeScalar0 = ((int32_t)((int32_t)L_19>>(int32_t)4));
		Il2CppChar* L_20 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_subtract((intptr_t)L_20, (int32_t)2));
		int32_t L_21 = ___unicodeScalar0;
		if (L_21)
		{
			goto IL_003d;
		}
	}
	{
		Il2CppChar* L_22 = ___buffer1;
		int32_t L_23 = V_0;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_22, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1))), (int32_t)2))));
		Il2CppChar* L_24 = ___buffer1;
		*((int16_t*)L_24) = (int16_t)((int32_t)59);
		return (bool)1;
	}
}
// System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultHtmlEncoder__cctor_m78F8CF003B0AD87F7780B6201FC6E5C5222EA321 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral202D5AEF51C14B19CFE144337C7FDB3B9A7C7387);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AE05DE7E37F7C0C46151B22648E1D7156C0F837);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral81FECCD01231D97EE6D7C17B8F5531FE1A6D533E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE7270C80B176C288F0786D9BFAC99EED86E77F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_0 = (UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3*)(UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3*)SZArrayNew(UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3_il2cpp_TypeInfo_var, (uint32_t)1);
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_1 = L_0;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_2;
		L_2 = UnicodeRanges_get_BasicLatin_mA46E04A614BC261AFCE0DB4A27734D38BE369F7B(/*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 *)L_2);
		TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * L_3 = (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 *)il2cpp_codegen_object_new(TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366_il2cpp_TypeInfo_var);
		TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB(L_3, L_1, /*hidden argument*/NULL);
		DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 * L_4 = (DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351 *)il2cpp_codegen_object_new(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		DefaultHtmlEncoder__ctor_mB0068ED2BF1438488250875E4376131B1312AD31(L_4, L_3, /*hidden argument*/NULL);
		((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->set_Singleton_5(L_4);
		NullCheck(_stringLiteral81FECCD01231D97EE6D7C17B8F5531FE1A6D533E);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_5;
		L_5 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteral81FECCD01231D97EE6D7C17B8F5531FE1A6D533E, /*hidden argument*/NULL);
		((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->set_s_quote_6(L_5);
		NullCheck(_stringLiteral202D5AEF51C14B19CFE144337C7FDB3B9A7C7387);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_6;
		L_6 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteral202D5AEF51C14B19CFE144337C7FDB3B9A7C7387, /*hidden argument*/NULL);
		((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->set_s_ampersand_7(L_6);
		NullCheck(_stringLiteralDE7270C80B176C288F0786D9BFAC99EED86E77F8);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_7;
		L_7 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteralDE7270C80B176C288F0786D9BFAC99EED86E77F8, /*hidden argument*/NULL);
		((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->set_s_lessthan_8(L_7);
		NullCheck(_stringLiteral7AE05DE7E37F7C0C46151B22648E1D7156C0F837);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_8;
		L_8 = String_ToCharArray_m33E93AEB7086CBEBDFA5730EAAC49686F144089C(_stringLiteral7AE05DE7E37F7C0C46151B22648E1D7156C0F837, /*hidden argument*/NULL);
		((DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_StaticFields*)il2cpp_codegen_static_fields_for(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var))->set_s_greaterthan_9(L_8);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultJavaScriptEncoderBasicLatin__ctor_mA96626B6D6F2C7A9FAC381D4D93D908D8F249874 (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * V_0 = NULL;
	AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		JavaScriptEncoder__ctor_mEC23CDF138E68E96490421C2A6C9A5C8E34595E8(__this, /*hidden argument*/NULL);
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_0 = (UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3*)(UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3*)SZArrayNew(UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3_il2cpp_TypeInfo_var, (uint32_t)1);
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_1 = L_0;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_2;
		L_2 = UnicodeRanges_get_BasicLatin_mA46E04A614BC261AFCE0DB4A27734D38BE369F7B(/*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 *)L_2);
		TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * L_3 = (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 *)il2cpp_codegen_object_new(TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366_il2cpp_TypeInfo_var);
		TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB(L_3, L_1, /*hidden argument*/NULL);
		V_0 = L_3;
		TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * L_4 = V_0;
		NullCheck(L_4);
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_5;
		L_5 = TextEncoderSettings_GetAllowedCharacters_m2A3EB325C9034B4C0A61E78D85B8FDCAAC866727(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		AllowedCharactersBitmap_ForbidUndefinedCharacters_mAED7702178F51111FCCC60113786DE9C1228BC95((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&V_1), /*hidden argument*/NULL);
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DefaultHtmlEncoder_t5739F3FCFD82E28D6B920A5949EC91D67FBA4351_il2cpp_TypeInfo_var);
		DefaultHtmlEncoder_ForbidHtmlCharacters_m79ED10B3D6F5D04E799E20AFAE7ABEE53537FFAB(L_6, /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&V_1), ((int32_t)92), /*hidden argument*/NULL);
		AllowedCharactersBitmap_ForbidCharacter_m0FBA3CF2F06DB5D723EBF60E487E24F399C8F7DB((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)(&V_1), ((int32_t)96), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::WillEncode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultJavaScriptEncoderBasicLatin_WillEncode_m6C9AF873C95324CB96E3A0948E4D760BBA164DDD (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * __this, int32_t ___unicodeScalar0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___unicodeScalar0;
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)1;
	}

IL_000a:
	{
		int32_t L_2 = ___unicodeScalar0;
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mBCD92F26CD88B2855FA734E26B0DBB18F0763482_inline(((int32_t)((uint16_t)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::FindFirstCharacterToEncode(System.Char*,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_m42BB18A673C9EA96D3D1465626CB033FA8F586D7 (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * __this, Il2CppChar* ___text0, int32_t ___textLength1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int16_t* V_1 = NULL;
	int16_t* V_2 = NULL;
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Il2CppChar V_4 = 0x0;
	{
		Il2CppChar* L_0 = ___text0;
		if ((!(((uintptr_t)L_0) == ((uintptr_t)((uintptr_t)0)))))
		{
			goto IL_0010;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralBFCC6EE94F1B7AA05A04750903E25F93A7188AE0)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_m42BB18A673C9EA96D3D1465626CB033FA8F586D7_RuntimeMethod_var)));
	}

IL_0010:
	{
		int32_t L_2 = ___textLength1;
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		V_0 = 0;
		Il2CppChar* L_3 = ___text0;
		V_1 = (int16_t*)L_3;
		int16_t* L_4 = V_1;
		int32_t L_5 = ___textLength1;
		V_2 = (int16_t*)((int16_t*)il2cpp_codegen_add((intptr_t)L_4, (intptr_t)((intptr_t)((int64_t)il2cpp_codegen_multiply((int64_t)((int64_t)((uint64_t)((uint32_t)((uint32_t)L_5)))), (int64_t)((int64_t)((int64_t)2)))))));
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_6;
		L_6 = DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5(/*hidden argument*/NULL);
		V_3 = L_6;
	}

IL_0026:
	{
		int16_t* L_7 = V_1;
		int32_t L_8 = *((uint16_t*)L_7);
		V_4 = L_8;
		Il2CppChar L_9 = V_4;
		if ((((int32_t)L_9) > ((int32_t)((int32_t)127))))
		{
			goto IL_004a;
		}
	}
	{
		Il2CppChar L_10 = V_4;
		uint8_t* L_11;
		L_11 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_3), L_10, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_12 = *((uint8_t*)L_11);
		if (!L_12)
		{
			goto IL_004a;
		}
	}
	{
		int16_t* L_13 = V_1;
		V_1 = (int16_t*)((int16_t*)il2cpp_codegen_add((intptr_t)L_13, (int32_t)2));
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
		int16_t* L_15 = V_1;
		int16_t* L_16 = V_2;
		if ((!(((uintptr_t)L_15) >= ((uintptr_t)L_16))))
		{
			goto IL_0026;
		}
	}

IL_0048:
	{
		V_0 = (-1);
	}

IL_004a:
	{
		int32_t L_17 = V_0;
		return L_17;
	}
}
// System.Int32 System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::FindFirstCharacterToEncodeUtf8(System.ReadOnlySpan`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncodeUtf8_m2B4293BB09F1802C5D0746BEF198D5D4EFB94B8B (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * __this, ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___utf8Text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t* V_0 = NULL;
	uint8_t* V_1 = NULL;
	uint32_t V_2 = 0;
	int32_t V_3 = 0;
	uint8_t* V_4 = NULL;
	uint8_t* V_5 = NULL;
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		uint8_t* L_0;
		L_0 = ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Text0), /*hidden argument*/ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_RuntimeMethod_var);
		V_1 = (uint8_t*)L_0;
		uint8_t* L_1 = V_1;
		V_0 = (uint8_t*)((uintptr_t)L_1);
		int32_t L_2;
		L_2 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Text0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		V_2 = L_2;
		uint32_t L_3 = V_2;
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		V_3 = 0;
		uint8_t* L_4 = V_0;
		V_4 = (uint8_t*)L_4;
		uint8_t* L_5 = V_4;
		uint32_t L_6 = V_2;
		V_5 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_5, (intptr_t)((uintptr_t)L_6)));
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_7;
		L_7 = DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5(/*hidden argument*/NULL);
		V_6 = L_7;
	}

IL_0029:
	{
		uint8_t* L_8 = V_4;
		int32_t L_9 = *((uint8_t*)L_8);
		uint8_t* L_10;
		L_10 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_6), L_9, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_11 = *((uint8_t*)L_10);
		if (!L_11)
		{
			goto IL_0048;
		}
	}
	{
		uint8_t* L_12 = V_4;
		V_4 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_12, (int32_t)1));
		int32_t L_13 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		uint8_t* L_14 = V_4;
		uint8_t* L_15 = V_5;
		if ((!(((uintptr_t)L_14) >= ((uintptr_t)L_15))))
		{
			goto IL_0029;
		}
	}

IL_0046:
	{
		V_3 = (-1);
	}

IL_0048:
	{
		int32_t L_16 = V_3;
		return L_16;
	}
}
// System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m9FB3F0515CDD56A7B116A9582DA0277B58672770 (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * __this, int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___bufferLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* V_0 = NULL;
	{
		Il2CppChar* L_0 = ___buffer1;
		if ((!(((uintptr_t)L_0) == ((uintptr_t)((uintptr_t)0)))))
		{
			goto IL_0010;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC263EA29ADF3548CFEBC57B532EED28451A56C10)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m9FB3F0515CDD56A7B116A9582DA0277B58672770_RuntimeMethod_var)));
	}

IL_0010:
	{
		int32_t L_2 = ___unicodeScalar0;
		bool L_3;
		L_3 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_2);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_4 = ___unicodeScalar0;
		Il2CppChar* L_5 = ___buffer1;
		int32_t L_6 = ___bufferLength2;
		int32_t* L_7 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5_inline(L_4, (Il2CppChar*)(Il2CppChar*)L_5, L_6, (int32_t*)L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0024:
	{
		int32_t L_9 = ___unicodeScalar0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)8)))
		{
			case 0:
			{
				goto IL_004b;
			}
			case 1:
			{
				goto IL_0053;
			}
			case 2:
			{
				goto IL_005b;
			}
			case 3:
			{
				goto IL_007b;
			}
			case 4:
			{
				goto IL_0063;
			}
			case 5:
			{
				goto IL_006b;
			}
		}
	}
	{
		int32_t L_10 = ___unicodeScalar0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0073;
		}
	}
	{
		goto IL_007b;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_11 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_b_5();
		V_0 = L_11;
		goto IL_0086;
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_12 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_t_6();
		V_0 = L_12;
		goto IL_0086;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_13 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_n_7();
		V_0 = L_13;
		goto IL_0086;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_14 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_f_8();
		V_0 = L_14;
		goto IL_0086;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_15 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_r_9();
		V_0 = L_15;
		goto IL_0086;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_16 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_back_10();
		V_0 = L_16;
		goto IL_0086;
	}

IL_007b:
	{
		int32_t L_17 = ___unicodeScalar0;
		Il2CppChar* L_18 = ___buffer1;
		int32_t L_19 = ___bufferLength2;
		int32_t* L_20 = ___numberOfCharactersWritten3;
		bool L_21;
		L_21 = JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_m128A8D90D65C5C6E0F1953E7EAE26D9636139172(L_17, (Il2CppChar*)(Il2CppChar*)L_18, L_19, (int32_t*)L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0086:
	{
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_22 = V_0;
		Il2CppChar* L_23 = ___buffer1;
		int32_t L_24 = ___bufferLength2;
		int32_t* L_25 = ___numberOfCharactersWritten3;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		bool L_26;
		L_26 = TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3(L_22, (Il2CppChar*)(Il2CppChar*)L_23, L_24, (int32_t*)L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.ReadOnlySpan`1<System.Byte> System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::get_AllowList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30____EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_inline((&L_0), (void*)(void*)(il2cpp_codegen_get_field_data(U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30____EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1_FieldInfo_var)), ((int32_t)256), /*hidden argument*/ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var);
		return L_0;
	}
}
// System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::NeedsEscaping(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mBCD92F26CD88B2855FA734E26B0DBB18F0763482 (Il2CppChar ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Il2CppChar L_0 = ___value0;
		if ((((int32_t)L_0) > ((int32_t)((int32_t)127))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_1;
		L_1 = DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5(/*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppChar L_2 = ___value0;
		uint8_t* L_3;
		L_3 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_0), L_2, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_4 = *((uint8_t*)L_3);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0018:
	{
		return (bool)1;
	}
}
// System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultJavaScriptEncoderBasicLatin__cctor_m222E9373820AD2A1B7C409102319A2E108988A08 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * L_0 = (DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A *)il2cpp_codegen_object_new(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		DefaultJavaScriptEncoderBasicLatin__ctor_mA96626B6D6F2C7A9FAC381D4D93D908D8F249874(L_0, /*hidden argument*/NULL);
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_singleton_4(L_0);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_3 = L_2;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)98));
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_b_5(L_3);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_4 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_5 = L_4;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_6 = L_5;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)116));
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_t_6(L_6);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_7 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_8 = L_7;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_9 = L_8;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)110));
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_n_7(L_9);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_10 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_11 = L_10;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_12 = L_11;
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)102));
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_f_8(L_12);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_13 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_14 = L_13;
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_15 = L_14;
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)114));
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_r_9(L_15);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_16 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_17 = L_16;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_18 = L_17;
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)92));
		((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->set_s_back_10(L_18);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mCBFEB52E648A5B44C67BF45A9477100DE7C549F8 (EmbeddedAttribute_t69546BC39F6CF6333844CB63739D9E618500F639 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Char System.HexConverter::ToCharUpper(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0 (int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		___value0 = ((int32_t)((int32_t)L_0&(int32_t)((int32_t)15)));
		int32_t L_1 = ___value0;
		___value0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)48)));
		int32_t L_2 = ___value0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = ___value0;
		___value0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)7));
	}

IL_0016:
	{
		int32_t L_4 = ___value0;
		return ((int32_t)((uint16_t)L_4));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Text.Encodings.Web.HtmlEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HtmlEncoder__ctor_m7A58A67ABD105671C4D6B8CF1CB4A06C5BD61FE5 (HtmlEncoder_t09B67E830AECB24356C3BBFBD9AD44952F581217 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		TextEncoder__ctor_m07027CA56CA490B41348EC294735935086AAE640(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_mF3D5598C7168D0599B09FA11CCB6826470FFF86E (IsReadOnlyAttribute_tF5095FA09BDCB5CFD77CA99BB1889138DBCF597E * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Text.Encodings.Web.JavaScriptEncoder System.Text.Encodings.Web.JavaScriptEncoder::get_Default()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JavaScriptEncoder_tEE2A7276ABD8379AD6317965D32A05CF2AD7B118 * JavaScriptEncoder_get_Default_mB84D9CC3B4DD9BD204B79DFA01B45CC66DE263EE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A * L_0 = ((DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_StaticFields*)il2cpp_codegen_static_fields_for(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var))->get_s_singleton_4();
		return L_0;
	}
}
// System.Void System.Text.Encodings.Web.JavaScriptEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JavaScriptEncoder__ctor_mEC23CDF138E68E96490421C2A6C9A5C8E34595E8 (JavaScriptEncoder_tEE2A7276ABD8379AD6317965D32A05CF2AD7B118 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		TextEncoder__ctor_m07027CA56CA490B41348EC294735935086AAE640(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_m128A8D90D65C5C6E0F1953E7EAE26D9636139172 (int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___length2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	Il2CppChar V_1 = 0x0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___unicodeScalar0;
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_2 = ___unicodeScalar0;
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC7309772F2EDE8846DFBFFC499EFBBA3A40E4586(L_2, (Il2CppChar*)(&V_0), (Il2CppChar*)(&V_1), /*hidden argument*/NULL);
		Il2CppChar L_3 = V_0;
		Il2CppChar* L_4 = ___buffer1;
		int32_t L_5 = ___length2;
		bool L_6;
		L_6 = JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA(L_3, (Il2CppChar*)(Il2CppChar*)L_4, L_5, (int32_t*)(&V_2), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppChar L_7 = V_1;
		Il2CppChar* L_8 = ___buffer1;
		int32_t L_9 = V_2;
		int32_t L_10 = ___length2;
		int32_t L_11 = V_2;
		int32_t* L_12 = ___numberOfCharactersWritten3;
		bool L_13;
		L_13 = JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA(L_7, (Il2CppChar*)(Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_8, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_9), (int32_t)2)))), ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)L_11)), (int32_t*)L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0038;
		}
	}
	{
		int32_t* L_14 = ___numberOfCharactersWritten3;
		int32_t* L_15 = ___numberOfCharactersWritten3;
		int32_t L_16 = *((int32_t*)L_15);
		int32_t L_17 = V_2;
		*((int32_t*)L_14) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17));
		return (bool)1;
	}

IL_0038:
	{
		int32_t* L_18 = ___numberOfCharactersWritten3;
		*((int32_t*)L_18) = (int32_t)0;
		return (bool)0;
	}

IL_003d:
	{
		int32_t L_19 = ___unicodeScalar0;
		Il2CppChar* L_20 = ___buffer1;
		int32_t L_21 = ___length2;
		int32_t* L_22 = ___numberOfCharactersWritten3;
		bool L_23;
		L_23 = JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA(L_19, (Il2CppChar*)(Il2CppChar*)L_20, L_21, (int32_t*)L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedSingleCharacter(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mFA5BB945A6171D66742BFC079B78EF636103B4BA (int32_t ___unicodeScalar0, Il2CppChar* ___buffer1, int32_t ___length2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)6)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t* L_1 = ___numberOfCharactersWritten3;
		*((int32_t*)L_1) = (int32_t)0;
		return (bool)0;
	}

IL_0009:
	{
		Il2CppChar* L_2 = ___buffer1;
		*((int16_t*)L_2) = (int16_t)((int32_t)92);
		Il2CppChar* L_3 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_3, (int32_t)2));
		Il2CppChar* L_4 = ___buffer1;
		*((int16_t*)L_4) = (int16_t)((int32_t)117);
		Il2CppChar* L_5 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_5, (int32_t)2));
		Il2CppChar* L_6 = ___buffer1;
		int32_t L_7 = ___unicodeScalar0;
		Il2CppChar L_8;
		L_8 = HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline(((int32_t)((int32_t)L_7>>(int32_t)((int32_t)12))), /*hidden argument*/NULL);
		*((int16_t*)L_6) = (int16_t)L_8;
		Il2CppChar* L_9 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_9, (int32_t)2));
		Il2CppChar* L_10 = ___buffer1;
		int32_t L_11 = ___unicodeScalar0;
		Il2CppChar L_12;
		L_12 = HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline(((int32_t)((int32_t)L_11>>(int32_t)8)), /*hidden argument*/NULL);
		*((int16_t*)L_10) = (int16_t)L_12;
		Il2CppChar* L_13 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_13, (int32_t)2));
		Il2CppChar* L_14 = ___buffer1;
		int32_t L_15 = ___unicodeScalar0;
		Il2CppChar L_16;
		L_16 = HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline(((int32_t)((int32_t)L_15>>(int32_t)4)), /*hidden argument*/NULL);
		*((int16_t*)L_14) = (int16_t)L_16;
		Il2CppChar* L_17 = ___buffer1;
		___buffer1 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_17, (int32_t)2));
		Il2CppChar* L_18 = ___buffer1;
		int32_t L_19 = ___unicodeScalar0;
		Il2CppChar L_20;
		L_20 = HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline(L_19, /*hidden argument*/NULL);
		*((int16_t*)L_18) = (int16_t)L_20;
		int32_t* L_21 = ___numberOfCharactersWritten3;
		*((int32_t*)L_21) = (int32_t)6;
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Diagnostics.CodeAnalysis.NotNullAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotNullAttribute__ctor_mBC655B204641AE9C42B930C6D7E18B1C66EE3821 (NotNullAttribute_t7D38250DB89F63E61FBF1E7B00EA3A8DEF82B5C8 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullableAttribute__ctor_m4E6F5D0EDF01F454FA95F67ABA1E1572D08ED5DC (NullableAttribute_tDA80C6E82A02D3C2964FD7A383FC3D8479F169BD * __this, uint8_t p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)1);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = L_0;
		uint8_t L_2 = p0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_2);
		__this->set_NullableFlags_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullableContextAttribute__ctor_mD05457C07F76C71E92D933B236410CB953F4CF8C (NullableContextAttribute_tC39978867FAA8D5C736BC102E44BFEC48B5A93A3 * __this, uint8_t p0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		uint8_t L_0 = p0;
		__this->set_Flag_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Runtime.CompilerServices.NullablePublicOnlyAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullablePublicOnlyAttribute__ctor_mA40CF3164668BCCB7E13E2314543F0848C43A927 (NullablePublicOnlyAttribute_t96F87E83DDB1878DEC7B7862F687101AD0C0D7FB * __this, bool p0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		bool L_0 = p0;
		__this->set_IncludesInternals_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::EncodeUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_EncodeUtf8_m809B5C38F1121A085BCB1C18AEA3C6A28DFD0AED (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___utf8Source0, Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___utf8Destination1, int32_t* ___bytesConsumed2, int32_t* ___bytesWritten3, bool ___isFinalBlock4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar* V_2 = NULL;
	uint8_t* V_3 = NULL;
	uint32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_9 = NULL;
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_10;
	memset((&V_10), 0, sizeof(V_10));
	int32_t V_11 = 0;
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_12;
	memset((&V_12), 0, sizeof(V_12));
	{
		int32_t L_0;
		L_0 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		V_0 = L_0;
		int32_t L_1;
		L_1 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		V_1 = L_1;
		int8_t* L_2 = (int8_t*) alloca(((uintptr_t)((int32_t)48)));
		memset(L_2, 0, ((uintptr_t)((int32_t)48)));
		V_2 = (Il2CppChar*)(L_2);
		int8_t* L_3 = (int8_t*) alloca(((uintptr_t)((int32_t)72)));
		memset(L_3, 0, ((uintptr_t)((int32_t)72)));
		V_3 = (uint8_t*)(L_3);
		V_5 = 0;
		V_6 = 0;
		V_7 = 0;
		goto IL_0222;
	}

IL_002a:
	{
		int32_t L_4 = V_6;
		uint8_t* L_5;
		L_5 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), L_4, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_6 = *((uint8_t*)L_5);
		V_4 = L_6;
		uint32_t L_7 = V_4;
		bool L_8;
		L_8 = UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D_inline(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00e9;
		}
	}
	{
		uint32_t L_9 = V_4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10;
		L_10 = TextEncoder_GetAsciiEncoding_m6FF6E556B2432F8F8FB0E9568B6E9C398BBD0829_inline(__this, (uint8_t)((int32_t)((uint8_t)L_9)), /*hidden argument*/NULL);
		V_9 = L_10;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = ((TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_StaticFields*)il2cpp_codegen_static_fields_for(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var))->get_s_noEscape_3();
		if ((!(((RuntimeObject*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_11) == ((RuntimeObject*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_12))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_13 = V_6;
		int32_t L_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		V_6 = L_14;
		int32_t L_15;
		L_15 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		if ((((int32_t)L_14) <= ((int32_t)L_15)))
		{
			goto IL_0129;
		}
	}
	{
		int32_t L_16 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1));
		V_7 = 1;
		goto IL_0137;
	}

IL_0077:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = V_9;
		if (L_17)
		{
			goto IL_0086;
		}
	}
	{
		V_7 = 0;
		V_5 = 1;
		goto IL_0137;
	}

IL_0086:
	{
		int32_t L_18 = V_6;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_19 = V_6;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_20;
		L_20 = ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), 0, L_19, /*hidden argument*/ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_RuntimeMethod_var);
		V_10 = L_20;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_21 = ___utf8Destination1;
		ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_10), L_21, /*hidden argument*/ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71_RuntimeMethod_var);
		int32_t L_22 = V_6;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_23;
		L_23 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), L_22, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		___utf8Source0 = L_23;
		int32_t L_24 = V_6;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_25;
		L_25 = Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), L_24, /*hidden argument*/Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_RuntimeMethod_var);
		___utf8Destination1 = L_25;
		V_6 = 0;
	}

IL_00b8:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_26 = V_9;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_27;
		L_27 = ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14(L_26, /*hidden argument*/ReadOnlySpan_1_op_Implicit_m271601BEC05226DC7E9FA26C26B9267E4AFBCE14_RuntimeMethod_var);
		V_10 = L_27;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_28 = ___utf8Destination1;
		bool L_29;
		L_29 = ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_10), L_28, /*hidden argument*/ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B_RuntimeMethod_var);
		if (L_29)
		{
			goto IL_00d0;
		}
	}
	{
		V_7 = 1;
		goto IL_0137;
	}

IL_00d0:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_30 = V_9;
		NullCheck(L_30);
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_31;
		L_31 = Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), ((int32_t)((int32_t)(((RuntimeArray*)L_30)->max_length))), /*hidden argument*/Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_RuntimeMethod_var);
		___utf8Destination1 = L_31;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_32;
		L_32 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), 1, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		___utf8Source0 = L_32;
		goto IL_0129;
	}

IL_00e9:
	{
		int32_t L_33 = V_6;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_34;
		L_34 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), L_33, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		int32_t L_35;
		L_35 = UnicodeHelpers_DecodeScalarValueFromUtf8_m12AC2FF4FA54E2207D7005136C5EF4ECAC281576(L_34, (uint32_t*)(&V_4), (int32_t*)(&V_5), /*hidden argument*/NULL);
		V_7 = L_35;
		int32_t L_36 = V_7;
		if (L_36)
		{
			goto IL_0137;
		}
	}
	{
		uint32_t L_37 = V_4;
		bool L_38;
		L_38 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_37);
		if (L_38)
		{
			goto IL_0137;
		}
	}
	{
		int32_t L_39 = V_6;
		int32_t L_40 = V_5;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)L_40));
		int32_t L_41 = V_6;
		int32_t L_42;
		L_42 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		if ((((int32_t)L_41) <= ((int32_t)L_42)))
		{
			goto IL_0129;
		}
	}
	{
		int32_t L_43 = V_6;
		int32_t L_44 = V_5;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_43, (int32_t)L_44));
		V_7 = 1;
		goto IL_0137;
	}

IL_0129:
	{
		int32_t L_45 = V_6;
		int32_t L_46;
		L_46 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_002a;
		}
	}

IL_0137:
	{
		int32_t L_47 = V_6;
		if ((((int32_t)L_47) <= ((int32_t)0)))
		{
			goto IL_0169;
		}
	}
	{
		int32_t L_48 = V_6;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_49;
		L_49 = ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), 0, L_48, /*hidden argument*/ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_RuntimeMethod_var);
		V_10 = L_49;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_50 = ___utf8Destination1;
		ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_10), L_50, /*hidden argument*/ReadOnlySpan_1_CopyTo_mF2EAD6E8F0C7F903BA4143DE8CA48E4830249C71_RuntimeMethod_var);
		int32_t L_51 = V_6;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_52;
		L_52 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), L_51, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		___utf8Source0 = L_52;
		int32_t L_53 = V_6;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_54;
		L_54 = Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), L_53, /*hidden argument*/Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_RuntimeMethod_var);
		___utf8Destination1 = L_54;
		V_6 = 0;
	}

IL_0169:
	{
		bool L_55;
		L_55 = ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D_RuntimeMethod_var);
		if (L_55)
		{
			goto IL_022e;
		}
	}
	{
		int32_t L_56 = V_7;
		if (!L_56)
		{
			goto IL_01a3;
		}
	}
	{
		int32_t L_57 = V_7;
		if ((!(((uint32_t)L_57) == ((uint32_t)2))))
		{
			goto IL_019b;
		}
	}
	{
		bool L_58 = ___isFinalBlock4;
		if (L_58)
		{
			goto IL_01a3;
		}
	}
	{
		int32_t* L_59 = ___bytesConsumed2;
		int32_t L_60 = V_0;
		int32_t L_61;
		L_61 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		*((int32_t*)L_59) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_60, (int32_t)L_61));
		int32_t* L_62 = ___bytesWritten3;
		int32_t L_63 = V_1;
		int32_t L_64;
		L_64 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		*((int32_t*)L_62) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_63, (int32_t)L_64));
		return (int32_t)(2);
	}

IL_019b:
	{
		int32_t L_65 = V_7;
		if ((((int32_t)L_65) == ((int32_t)1)))
		{
			goto IL_023f;
		}
	}

IL_01a3:
	{
		uint32_t L_66 = V_4;
		Il2CppChar* L_67 = V_2;
		bool L_68;
		L_68 = VirtualFuncInvoker4< bool, int32_t, Il2CppChar*, int32_t, int32_t* >::Invoke(4 /* System.Boolean System.Text.Encodings.Web.TextEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&) */, __this, L_66, (Il2CppChar*)(Il2CppChar*)L_67, ((int32_t)24), (int32_t*)(&V_8));
		if (!L_68)
		{
			goto IL_01fe;
		}
	}
	{
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_69;
		L_69 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		Il2CppChar* L_70 = V_2;
		int32_t L_71 = V_8;
		uint8_t* L_72 = V_3;
		NullCheck(L_69);
		int32_t L_73;
		L_73 = VirtualFuncInvoker4< int32_t, Il2CppChar*, int32_t, uint8_t*, int32_t >::Invoke(19 /* System.Int32 System.Text.Encoding::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32) */, L_69, (Il2CppChar*)(Il2CppChar*)L_70, L_71, (uint8_t*)(uint8_t*)L_72, ((int32_t)72));
		V_11 = L_73;
		uint8_t* L_74 = V_3;
		int32_t L_75 = V_11;
		ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_12), (void*)(void*)L_74, L_75, /*hidden argument*/ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var);
		uint32_t L_76 = V_4;
		bool L_77;
		L_77 = UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D_inline(L_76, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_01e7;
		}
	}
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_78 = __this->get__asciiEscape_0();
		uint32_t L_79 = V_4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_80;
		L_80 = ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_12), /*hidden argument*/ReadOnlySpan_1_ToArray_m416E39B973AC3FE17DB7DA76A4E2A23DC76F2C2C_RuntimeMethod_var);
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_80);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(L_79), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_80);
	}

IL_01e7:
	{
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_81 = ___utf8Destination1;
		bool L_82;
		L_82 = ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_12), L_81, /*hidden argument*/ReadOnlySpan_1_TryCopyTo_m5C7C799A38B31B8F85DAAD69FA8D0DAE854BC03B_RuntimeMethod_var);
		if (!L_82)
		{
			goto IL_023f;
		}
	}
	{
		int32_t L_83 = V_11;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_84;
		L_84 = Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), L_83, /*hidden argument*/Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_RuntimeMethod_var);
		___utf8Destination1 = L_84;
		goto IL_0217;
	}

IL_01fe:
	{
		int32_t* L_85 = ___bytesConsumed2;
		int32_t L_86 = V_0;
		int32_t L_87;
		L_87 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		*((int32_t*)L_85) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_86, (int32_t)L_87));
		int32_t* L_88 = ___bytesWritten3;
		int32_t L_89 = V_1;
		int32_t L_90;
		L_90 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		*((int32_t*)L_88) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_89, (int32_t)L_90));
		return (int32_t)(3);
	}

IL_0217:
	{
		int32_t L_91 = V_5;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_92;
		L_92 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), L_91, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		___utf8Source0 = L_92;
	}

IL_0222:
	{
		bool L_93;
		L_93 = ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_IsEmpty_m42772AEFB3C5778AC760E95D3D24EDF45179469D_RuntimeMethod_var);
		if (!L_93)
		{
			goto IL_002a;
		}
	}

IL_022e:
	{
		int32_t* L_94 = ___bytesConsumed2;
		int32_t L_95 = V_0;
		*((int32_t*)L_94) = (int32_t)L_95;
		int32_t* L_96 = ___bytesWritten3;
		int32_t L_97 = V_1;
		int32_t L_98;
		L_98 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		*((int32_t*)L_96) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_97, (int32_t)L_98));
		return (int32_t)(0);
	}

IL_023f:
	{
		int32_t* L_99 = ___bytesConsumed2;
		int32_t L_100 = V_0;
		int32_t L_101;
		L_101 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		*((int32_t*)L_99) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_100, (int32_t)L_101));
		int32_t* L_102 = ___bytesWritten3;
		int32_t L_103 = V_1;
		int32_t L_104;
		L_104 = Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_inline((Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 *)(&___utf8Destination1), /*hidden argument*/Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_RuntimeMethod_var);
		*((int32_t*)L_102) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_103, (int32_t)L_104));
		return (int32_t)(1);
	}
}
// System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::Encode(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_Encode_m6AF81F7F7A8545803441E9F0D443D4EEB793AC83 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___source0, Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___destination1, int32_t* ___charsConsumed2, int32_t* ___charsWritten3, bool ___isFinalBlock4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		bool L_0;
		L_0 = ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_IsEmpty_m979A3AE3BF7796824619B1FF2DA0847A0C82433D_RuntimeMethod_var);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t* L_1 = ___charsConsumed2;
		*((int32_t*)L_1) = (int32_t)0;
		int32_t* L_2 = ___charsWritten3;
		*((int32_t*)L_2) = (int32_t)0;
		return (int32_t)(0);
	}

IL_0012:
	{
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_3 = ___source0;
		V_0 = L_3;
		int32_t L_4;
		L_4 = Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), /*hidden argument*/Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
		int32_t L_5;
		L_5 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_6;
		L_6 = Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), /*hidden argument*/Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_7;
		L_7 = ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), 0, L_6, /*hidden argument*/ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_RuntimeMethod_var);
		V_0 = L_7;
	}

IL_0034:
	{
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_8 = V_0;
		int32_t L_9;
		L_9 = TextEncoder_FindFirstCharacterToEncode_m2423424CDE31B11CA2840777CD0C0422CD829F71(__this, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_11;
		L_11 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&V_0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		V_1 = L_11;
	}

IL_0048:
	{
		int32_t L_12 = V_1;
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_13;
		L_13 = ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), 0, L_12, /*hidden argument*/ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_RuntimeMethod_var);
		V_5 = L_13;
		Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  L_14 = ___destination1;
		ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&V_5), L_14, /*hidden argument*/ReadOnlySpan_1_CopyTo_m4BBFA805EC16BEDF788F7EDE2C689AD48180DA0F_RuntimeMethod_var);
		int32_t L_15 = V_1;
		int32_t L_16;
		L_16 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t* L_17 = ___charsConsumed2;
		int32_t L_18;
		L_18 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		*((int32_t*)L_17) = (int32_t)L_18;
		int32_t* L_19 = ___charsWritten3;
		int32_t L_20;
		L_20 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		*((int32_t*)L_19) = (int32_t)L_20;
		return (int32_t)(0);
	}

IL_007a:
	{
		int32_t L_21 = V_1;
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_22;
		L_22 = ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), L_21, /*hidden argument*/ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_RuntimeMethod_var);
		int32_t L_23 = V_1;
		Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  L_24;
		L_24 = Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), L_23, /*hidden argument*/Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_RuntimeMethod_var);
		bool L_25 = ___isFinalBlock4;
		int32_t L_26;
		L_26 = TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_m97E09AEBAFA1C02707FF382DB82B6A2E30BF4931(__this, L_22, L_24, (int32_t*)(&V_3), (int32_t*)(&V_4), L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		int32_t* L_27 = ___charsConsumed2;
		int32_t L_28 = V_1;
		int32_t L_29 = V_3;
		*((int32_t*)L_27) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)L_29));
		int32_t* L_30 = ___charsWritten3;
		int32_t L_31 = V_1;
		int32_t L_32 = V_4;
		*((int32_t*)L_30) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)L_32));
		int32_t L_33 = V_2;
		return L_33;
	}
}
// System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.ReadOnlySpan`1<System.Char>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_FindFirstCharacterToEncode_m2423424CDE31B11CA2840777CD0C0422CD829F71 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar* V_0 = NULL;
	Il2CppChar* V_1 = NULL;
	{
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_0 = ___text0;
		Il2CppChar* L_1;
		L_1 = MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742(L_0, /*hidden argument*/MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCA0897C4C9A2C96612E0F546CD589D7392DD3742_RuntimeMethod_var);
		V_1 = (Il2CppChar*)L_1;
		Il2CppChar* L_2 = V_1;
		V_0 = (Il2CppChar*)((uintptr_t)L_2);
		Il2CppChar* L_3 = V_0;
		int32_t L_4;
		L_4 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___text0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		int32_t L_5;
		L_5 = VirtualFuncInvoker2< int32_t, Il2CppChar*, int32_t >::Invoke(5 /* System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.Char*,System.Int32) */, __this, (Il2CppChar*)(Il2CppChar*)L_3, L_4);
		return L_5;
	}
}
// System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncodeUtf8(System.ReadOnlySpan`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_FindFirstCharacterToEncodeUtf8_m7F5E427A71A6A13427292FEA421F58AAB2EB3A35 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___utf8Text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t* V_0 = NULL;
	uint8_t* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		bool L_0 = __this->get__isAsciiCacheInitialized_1();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		TextEncoder_InitializeAsciiCache_m1D9E9EDE89AA59C2A406239D1C722284E25B5398(__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		uint8_t* L_1;
		L_1 = ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Text0), /*hidden argument*/ReadOnlySpan_1_GetPinnableReference_m6298FF21807E545414FBE685B3DD33D63D8EBE04_RuntimeMethod_var);
		V_1 = (uint8_t*)L_1;
		uint8_t* L_2 = V_1;
		V_0 = (uint8_t*)((uintptr_t)L_2);
		V_2 = 0;
		goto IL_0060;
	}

IL_001f:
	{
		uint8_t* L_3 = V_0;
		int32_t L_4 = V_2;
		int32_t L_5 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_3, (int32_t)L_4)));
		bool L_6;
		L_6 = UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D_inline(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		uint8_t* L_7 = V_0;
		int32_t L_8 = V_2;
		int32_t L_9 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_7, (int32_t)L_8)));
		bool L_10;
		L_10 = TextEncoder_DoesAsciiNeedEncoding_m0525D9CBBC3FA4494ECD7D440EE01384F64DF27F_inline(__this, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		goto IL_0060;
	}

IL_003c:
	{
		int32_t L_12 = V_2;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_13;
		L_13 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Text0), L_12, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		int32_t L_14;
		L_14 = UnicodeHelpers_DecodeScalarValueFromUtf8_m12AC2FF4FA54E2207D7005136C5EF4ECAC281576(L_13, (uint32_t*)(&V_4), (int32_t*)(&V_5), /*hidden argument*/NULL);
		V_3 = L_14;
		int32_t L_15 = V_3;
		if (L_15)
		{
			goto IL_006c;
		}
	}
	{
		uint32_t L_16 = V_4;
		bool L_17;
		L_17 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_16);
		if (L_17)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_18 = V_2;
		int32_t L_19 = V_5;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)L_19));
	}

IL_0060:
	{
		int32_t L_20 = V_2;
		int32_t L_21;
		L_21 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___utf8Text0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_001f;
		}
	}
	{
		V_2 = (-1);
	}

IL_006c:
	{
		int32_t L_22 = V_2;
		return L_22;
	}
}
// System.Boolean System.Text.Encodings.Web.TextEncoder::TryCopyCharacters(System.Char[],System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextEncoder_TryCopyCharacters_m557ADF34204926DA672B3388A1B094078CD530E3 (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___source0, Il2CppChar* ___destination1, int32_t ___destinationLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___destinationLength2;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1 = ___source0;
		NullCheck(L_1);
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		int32_t* L_2 = ___numberOfCharactersWritten3;
		*((int32_t*)L_2) = (int32_t)0;
		return (bool)0;
	}

IL_000b:
	{
		V_0 = 0;
		goto IL_001d;
	}

IL_000f:
	{
		Il2CppChar* L_3 = ___destination1;
		int32_t L_4 = V_0;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_5 = ___source0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint16_t L_8 = (uint16_t)(L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		*((int16_t*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_3, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_4), (int32_t)2))))) = (int16_t)L_8;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_001d:
	{
		int32_t L_10 = V_0;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_11 = ___source0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))
		{
			goto IL_000f;
		}
	}
	{
		int32_t* L_12 = ___numberOfCharactersWritten3;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_13 = ___source0;
		NullCheck(L_13);
		*((int32_t*)L_12) = (int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length)));
		return (bool)1;
	}
}
// System.Boolean System.Text.Encodings.Web.TextEncoder::TryWriteScalarAsChar(System.Int32,System.Char*,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5 (int32_t ___unicodeScalar0, Il2CppChar* ___destination1, int32_t ___destinationLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___destinationLength2;
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t* L_1 = ___numberOfCharactersWritten3;
		*((int32_t*)L_1) = (int32_t)0;
		return (bool)0;
	}

IL_0009:
	{
		Il2CppChar* L_2 = ___destination1;
		int32_t L_3 = ___unicodeScalar0;
		*((int16_t*)L_2) = (int16_t)((int32_t)((uint16_t)L_3));
		int32_t* L_4 = ___numberOfCharactersWritten3;
		*((int32_t*)L_4) = (int32_t)1;
		return (bool)1;
	}
}
// System.Byte[] System.Text.Encodings.Web.TextEncoder::GetAsciiEncoding(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TextEncoder_GetAsciiEncoding_m6FF6E556B2432F8F8FB0E9568B6E9C398BBD0829 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_0 = __this->get__asciiEscape_0();
		uint8_t L_1 = ___value0;
		NullCheck(L_0);
		uint8_t L_2 = L_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = V_0;
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		uint8_t L_5 = ___value0;
		bool L_6;
		L_6 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_5);
		if (L_6)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = ((TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_StaticFields*)il2cpp_codegen_static_fields_for(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var))->get_s_noEscape_3();
		V_0 = L_7;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_8 = __this->get__asciiEscape_0();
		uint8_t L_9 = ___value0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_10);
	}

IL_0024:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Text.Encodings.Web.TextEncoder::InitializeAsciiCache()
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void TextEncoder_InitializeAsciiCache_m1D9E9EDE89AA59C2A406239D1C722284E25B5398 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0022;
	}

IL_0004:
	{
		AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964 * L_0 = __this->get_address_of__asciiNeedsEscaping_2();
		U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3 * L_1 = L_0->get_address_of_Data_0();
		bool* L_2 = L_1->get_address_of_FixedElementField_0();
		int32_t L_3 = V_0;
		int32_t L_4 = V_0;
		bool L_5;
		L_5 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_4);
		*((int8_t*)((bool*)il2cpp_codegen_add((intptr_t)L_2, (int32_t)L_3))) = (int8_t)L_5;
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0022:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)128))))
		{
			goto IL_0004;
		}
	}
	{
		il2cpp_codegen_memory_barrier();
		__this->set__isAsciiCacheInitialized_1(1);
		return;
	}
}
// System.Boolean System.Text.Encodings.Web.TextEncoder::DoesAsciiNeedEncoding(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextEncoder_DoesAsciiNeedEncoding_m0525D9CBBC3FA4494ECD7D440EE01384F64DF27F (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964 * L_0 = __this->get_address_of__asciiNeedsEscaping_2();
		U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3 * L_1 = L_0->get_address_of_Data_0();
		bool* L_2 = L_1->get_address_of_FixedElementField_0();
		uint32_t L_3 = ___value0;
		int32_t L_4 = *((uint8_t*)((bool*)il2cpp_codegen_add((intptr_t)L_2, (intptr_t)((uintptr_t)L_3))));
		return (bool)L_4;
	}
}
// System.Void System.Text.Encodings.Web.TextEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoder__ctor_m07027CA56CA490B41348EC294735935086AAE640 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_0 = (ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D*)SZArrayNew(ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D_il2cpp_TypeInfo_var, (uint32_t)((int32_t)128));
		__this->set__asciiEscape_0(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.Encodings.Web.TextEncoder::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoder__cctor_mA398909671E057BBEF55604F2AC2B31C545500D4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0;
		L_0 = Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_inline(/*hidden argument*/Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_RuntimeMethod_var);
		((TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_StaticFields*)il2cpp_codegen_static_fields_for(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var))->set_s_noEscape_3(L_0);
		return;
	}
}
// System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::<Encode>g__EncodeCore|15_0(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_m97E09AEBAFA1C02707FF382DB82B6A2E30BF4931 (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ___source0, Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  ___destination1, int32_t* ___charsConsumed2, int32_t* ___charsWritten3, bool ___isFinalBlock4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint32_t V_5 = 0;
	int32_t V_6 = 0;
	uint32_t V_7 = 0;
	Il2CppChar* V_8 = NULL;
	Il2CppChar* V_9 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		bool L_0;
		L_0 = Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), /*hidden argument*/Span_1_get_IsEmpty_mDB67D262DE62F89A2C557062CE73384C8E8921D6_RuntimeMethod_var);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1;
		L_1 = Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_inline(/*hidden argument*/Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_RuntimeMethod_var);
		Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  L_2;
		L_2 = Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2(L_1, /*hidden argument*/Span_1_op_Implicit_m9021BC9AFBA7FD03C196878985A382B044AE9EE2_RuntimeMethod_var);
		___destination1 = L_2;
	}

IL_0015:
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0127;
	}

IL_001e:
	{
		int32_t L_3 = V_1;
		Il2CppChar* L_4;
		L_4 = ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), L_3, /*hidden argument*/ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_RuntimeMethod_var);
		int32_t L_5 = *((uint16_t*)L_4);
		V_3 = L_5;
		int32_t L_6 = V_3;
		bool L_7;
		L_7 = UnicodeUtility_IsSurrogateCodePoint_m1C61245755AFFC5F88AEFDE2511BD42EC2E35471_inline(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_8 = V_3;
		bool L_9;
		L_9 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_8);
		if (L_9)
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_10 = V_0;
		int32_t L_11;
		L_11 = Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), /*hidden argument*/Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
		if ((!(((uint32_t)L_10) < ((uint32_t)L_11))))
		{
			goto IL_0143;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13 = L_12;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		Il2CppChar* L_14;
		L_14 = Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), L_13, /*hidden argument*/Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_RuntimeMethod_var);
		int32_t L_15 = V_3;
		*((int16_t*)L_14) = (int16_t)((int32_t)((uint16_t)L_15));
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
		goto IL_0127;
	}

IL_0061:
	{
		int32_t L_17 = V_3;
		V_5 = L_17;
		V_3 = ((int32_t)65533);
		uint32_t L_18 = V_5;
		bool L_19;
		L_19 = UnicodeUtility_IsHighSurrogateCodePoint_mC6FC7E1678F3638CDDE48C9DE27C7ECE51D16654_inline(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_20 = V_1;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
		int32_t L_21 = V_6;
		int32_t L_22;
		L_22 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		if ((!(((uint32_t)L_21) >= ((uint32_t)L_22))))
		{
			goto IL_008c;
		}
	}
	{
		bool L_23 = ___isFinalBlock4;
		if (L_23)
		{
			goto IL_00e7;
		}
	}
	{
		goto IL_013f;
	}

IL_008c:
	{
		int32_t L_24 = V_6;
		Il2CppChar* L_25;
		L_25 = ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), L_24, /*hidden argument*/ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_RuntimeMethod_var);
		int32_t L_26 = *((uint16_t*)L_25);
		V_7 = L_26;
		uint32_t L_27 = V_7;
		bool L_28;
		L_28 = UnicodeUtility_IsLowSurrogateCodePoint_mFC9196588FB51D463621246A555CDF8EBD7C4F89_inline(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e7;
		}
	}
	{
		uint32_t L_29 = V_5;
		uint32_t L_30 = V_7;
		uint32_t L_31;
		L_31 = UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6866E0E09B45BF1CDE2BB7865AF20F6460C752D0(L_29, L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		int32_t L_32 = V_3;
		bool L_33;
		L_33 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_32);
		if (L_33)
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_34 = V_0;
		int32_t L_35;
		L_35 = Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), /*hidden argument*/Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1))) < ((uint32_t)L_35))))
		{
			goto IL_0143;
		}
	}
	{
		int32_t L_36 = V_0;
		Il2CppChar* L_37;
		L_37 = Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), L_36, /*hidden argument*/Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_RuntimeMethod_var);
		uint32_t L_38 = V_5;
		*((int16_t*)L_37) = (int16_t)((int32_t)((uint16_t)L_38));
		int32_t L_39 = V_0;
		Il2CppChar* L_40;
		L_40 = Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1)), /*hidden argument*/Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_RuntimeMethod_var);
		uint32_t L_41 = V_7;
		*((int16_t*)L_40) = (int16_t)((int32_t)((uint16_t)L_41));
		int32_t L_42 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)2));
		int32_t L_43 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)2));
		goto IL_0127;
	}

IL_00e7:
	{
	}

IL_00e8:
	try
	{// begin try (depth: 1)
		{
			Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  L_44 = ___destination1;
			Il2CppChar* L_45;
			L_45 = MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361(L_44, /*hidden argument*/MemoryMarshal_GetReference_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m5757377E2BC403DA9CA8C33929F9BD50F7E23361_RuntimeMethod_var);
			V_9 = (Il2CppChar*)L_45;
			Il2CppChar* L_46 = V_9;
			V_8 = (Il2CppChar*)((uintptr_t)L_46);
			int32_t L_47 = V_3;
			Il2CppChar* L_48 = V_8;
			int32_t L_49 = V_0;
			int32_t L_50;
			L_50 = Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_inline((Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D *)(&___destination1), /*hidden argument*/Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_RuntimeMethod_var);
			int32_t L_51 = V_0;
			bool L_52;
			L_52 = VirtualFuncInvoker4< bool, int32_t, Il2CppChar*, int32_t, int32_t* >::Invoke(4 /* System.Boolean System.Text.Encodings.Web.TextEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&) */, __this, L_47, (Il2CppChar*)(Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_48, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_49), (int32_t)2)))), ((int32_t)il2cpp_codegen_subtract((int32_t)L_50, (int32_t)L_51)), (int32_t*)(&V_4));
			if (L_52)
			{
				goto IL_0112;
			}
		}

IL_0110:
		{
			IL2CPP_LEAVE(0x143, FINALLY_0114);
		}

IL_0112:
		{
			IL2CPP_LEAVE(0x119, FINALLY_0114);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0114;
	}

FINALLY_0114:
	{// begin finally (depth: 1)
		V_9 = (Il2CppChar*)((uintptr_t)0);
		IL2CPP_END_FINALLY(276)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(276)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x143, IL_0143)
		IL2CPP_JUMP_TBL(0x119, IL_0119)
	}

IL_0119:
	{
		int32_t L_53 = V_1;
		int32_t L_54 = V_3;
		int32_t L_55;
		L_55 = UnicodeUtility_GetUtf16SequenceLength_mBAB104741499D1D4454BF674A1A4B380E3DA9A9E(L_54, /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)L_55));
		int32_t L_56 = V_0;
		int32_t L_57 = V_4;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)L_57));
	}

IL_0127:
	{
		int32_t L_58 = V_1;
		int32_t L_59;
		L_59 = ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_inline((ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_RuntimeMethod_var);
		if ((!(((uint32_t)L_58) >= ((uint32_t)L_59))))
		{
			goto IL_001e;
		}
	}
	{
		V_2 = 0;
	}

IL_0136:
	{
		int32_t* L_60 = ___charsConsumed2;
		int32_t L_61 = V_1;
		*((int32_t*)L_60) = (int32_t)L_61;
		int32_t* L_62 = ___charsWritten3;
		int32_t L_63 = V_0;
		*((int32_t*)L_62) = (int32_t)L_63;
		int32_t L_64 = V_2;
		return L_64;
	}

IL_013f:
	{
		V_2 = 2;
		goto IL_0136;
	}

IL_0143:
	{
		V_2 = 1;
		goto IL_0136;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Text.Encodings.Web.TextEncoderSettings::.ctor(System.Text.Unicode.UnicodeRange[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * __this, UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* ___allowedRanges0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_0 = ___allowedRanges0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralE3D7E554C2FD3D52D9690E3D5BB7B7321C3FA52B)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TextEncoderSettings__ctor_mAA53311BDA540ACB84552A125588A58E6316C9EB_RuntimeMethod_var)));
	}

IL_0014:
	{
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_2;
		L_2 = AllowedCharactersBitmap_CreateNew_m881B89C811D45EBD8281DA9FF3CA7957833A9252(/*hidden argument*/NULL);
		__this->set__allowedCharactersBitmap_0(L_2);
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_3 = ___allowedRanges0;
		VirtualActionInvoker1< UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* >::Invoke(5 /* System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRanges(System.Text.Unicode.UnicodeRange[]) */, __this, L_3);
		return;
	}
}
// System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRange(System.Text.Unicode.UnicodeRange)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoderSettings_AllowRange_mA179A5E04975AF16B07145F78965DA63087B18EB (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * __this, UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * ___range0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_0 = ___range0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4845015737DC41475709911228278216EE4DC3AF)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TextEncoderSettings_AllowRange_mA179A5E04975AF16B07145F78965DA63087B18EB_RuntimeMethod_var)));
	}

IL_000e:
	{
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_2 = ___range0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = UnicodeRange_get_FirstCodePoint_mE2ECE0C7C54A3047DF4DBA68271FBF8067BCEB26_inline(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_4 = ___range0;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = UnicodeRange_get_Length_mD690963E9ABE4A2F5731E5CF27C00292C7FFC696_inline(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = 0;
		goto IL_0033;
	}

IL_0020:
	{
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * L_6 = __this->get_address_of__allowedCharactersBitmap_0();
		int32_t L_7 = V_0;
		int32_t L_8 = V_2;
		AllowedCharactersBitmap_AllowCharacter_mEB2C55159876F6C4B5508F5088C6CBF77FBA20C3((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)L_6, ((int32_t)((uint16_t)((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8)))), /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0033:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0020;
		}
	}
	{
		return;
	}
}
// System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRanges(System.Text.Unicode.UnicodeRange[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextEncoderSettings_AllowRanges_m60F42E23315096E4143FB35868A805ECC4660409 (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * __this, UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* ___ranges0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_0 = ___ranges0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4FC0613DB074A9C5DAB592FE3F86B3EDD439F7E5)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TextEncoderSettings_AllowRanges_m60F42E23315096E4143FB35868A805ECC4660409_RuntimeMethod_var)));
	}

IL_000e:
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0012:
	{
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_2 = ___ranges0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		VirtualActionInvoker1< UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * >::Invoke(4 /* System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRange(System.Text.Unicode.UnicodeRange) */, __this, L_5);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_001f:
	{
		int32_t L_7 = V_0;
		UnicodeRangeU5BU5D_t45AD8399AF7FC06E414848453BC2A2FB923AE0D3* L_8 = ___ranges0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length))))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.TextEncoderSettings::GetAllowedCharacters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  TextEncoderSettings_GetAllowedCharacters_m2A3EB325C9034B4C0A61E78D85B8FDCAAC866727 (TextEncoderSettings_t580906453C990BD276E16645DD720FD97C63A366 * __this, const RuntimeMethod* method)
{
	{
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * L_0 = __this->get_address_of__allowedCharactersBitmap_0();
		AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC  L_1;
		L_1 = AllowedCharactersBitmap_Clone_mD99189190F808C53BA5B0B3945397B0D9997421B((AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32[] System.Text.Unicode.UnicodeHelpers::CreateDefinedCharacterBitmapMachineEndian()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m5276C6DA38121F0033E91B53041F9CBFAC7FD9CA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_0;
	memset((&V_0), 0, sizeof(V_0));
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_0;
		L_0 = UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_2 = (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)SZArrayNew(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_1/(int32_t)4)));
		V_1 = L_2;
		V_2 = 0;
		goto IL_002f;
	}

IL_0019:
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_3 = V_1;
		int32_t L_4 = V_2;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_5 = V_0;
		uint32_t L_6;
		L_6 = BinaryPrimitives_ReadUInt32LittleEndian_mEE46641BC73CAACA64F2952CD791BE96F5DB44F4_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint32_t)L_6);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_7;
		L_7 = ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_0), 4, /*hidden argument*/ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_RuntimeMethod_var);
		V_0 = L_7;
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_9 = V_2;
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))))))
		{
			goto IL_0019;
		}
	}
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_11 = V_1;
		return L_11;
	}
}
// System.Buffers.OperationStatus System.Text.Unicode.UnicodeHelpers::DecodeScalarValueFromUtf8(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnicodeHelpers_DecodeScalarValueFromUtf8_m12AC2FF4FA54E2207D7005136C5EF4ECAC281576 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, uint32_t* ___result1, int32_t* ___bytesConsumed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		int32_t L_1;
		L_1 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_2 = V_0;
		uint8_t* L_3;
		L_3 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), L_2, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_4 = *((uint8_t*)L_3);
		V_1 = L_4;
		uint32_t L_5 = V_1;
		bool L_6;
		L_6 = UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D_inline(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002b;
		}
	}

IL_0021:
	{
		int32_t* L_7 = ___bytesConsumed2;
		int32_t L_8 = V_0;
		*((int32_t*)L_7) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		uint32_t* L_9 = ___result1;
		uint32_t L_10 = V_1;
		*((int32_t*)L_9) = (int32_t)L_10;
		return (int32_t)(0);
	}

IL_002b:
	{
		uint32_t L_11 = V_1;
		bool L_12;
		L_12 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_11, ((int32_t)194), ((int32_t)244), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0142;
		}
	}
	{
		uint32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)((int32_t)194)))<<(int32_t)6));
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
		int32_t L_15 = V_0;
		int32_t L_16;
		L_16 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((!(((uint32_t)L_15) < ((uint32_t)L_16))))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_17 = V_0;
		uint8_t* L_18;
		L_18 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), L_17, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_19 = *((uint8_t*)L_18);
		V_2 = ((int8_t)((int8_t)L_19));
		int32_t L_20 = V_2;
		if ((((int32_t)L_20) >= ((int32_t)((int32_t)-64))))
		{
			goto IL_0144;
		}
	}
	{
		uint32_t L_21 = V_1;
		int32_t L_22 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)L_22));
		uint32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)((int32_t)128)));
		uint32_t L_24 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)((int32_t)128)));
		uint32_t L_25 = V_1;
		if ((!(((uint32_t)L_25) >= ((uint32_t)((int32_t)2048)))))
		{
			goto IL_0021;
		}
	}
	{
		uint32_t L_26 = V_1;
		bool L_27;
		L_27 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_26, ((int32_t)2080), ((int32_t)3343), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0144;
		}
	}
	{
		uint32_t L_28 = V_1;
		bool L_29;
		L_29 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_28, ((int32_t)2912), ((int32_t)2943), /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_0144;
		}
	}
	{
		uint32_t L_30 = V_1;
		bool L_31;
		L_31 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_30, ((int32_t)3072), ((int32_t)3087), /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0144;
		}
	}
	{
		int32_t L_32 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
		int32_t L_33 = V_0;
		int32_t L_34;
		L_34 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((!(((uint32_t)L_33) < ((uint32_t)L_34))))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_35 = V_0;
		uint8_t* L_36;
		L_36 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), L_35, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_37 = *((uint8_t*)L_36);
		V_2 = ((int8_t)((int8_t)L_37));
		int32_t L_38 = V_2;
		if ((((int32_t)L_38) >= ((int32_t)((int32_t)-64))))
		{
			goto IL_0144;
		}
	}
	{
		uint32_t L_39 = V_1;
		V_1 = ((int32_t)((int32_t)L_39<<(int32_t)6));
		uint32_t L_40 = V_1;
		int32_t L_41 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)L_41));
		uint32_t L_42 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)((int32_t)128)));
		uint32_t L_43 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_43, (int32_t)((int32_t)131072)));
		uint32_t L_44 = V_1;
		if ((!(((uint32_t)L_44) > ((uint32_t)((int32_t)65535)))))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_45 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
		int32_t L_46 = V_0;
		int32_t L_47;
		L_47 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((!(((uint32_t)L_46) < ((uint32_t)L_47))))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_48 = V_0;
		uint8_t* L_49;
		L_49 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), L_48, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_50 = *((uint8_t*)L_49);
		V_2 = ((int8_t)((int8_t)L_50));
		int32_t L_51 = V_2;
		if ((((int32_t)L_51) >= ((int32_t)((int32_t)-64))))
		{
			goto IL_0144;
		}
	}
	{
		uint32_t L_52 = V_1;
		V_1 = ((int32_t)((int32_t)L_52<<(int32_t)6));
		uint32_t L_53 = V_1;
		int32_t L_54 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)L_54));
		uint32_t L_55 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)((int32_t)128)));
		uint32_t L_56 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_56, (int32_t)((int32_t)4194304)));
		goto IL_0021;
	}

IL_0142:
	{
		V_0 = 1;
	}

IL_0144:
	{
		int32_t* L_57 = ___bytesConsumed2;
		int32_t L_58 = V_0;
		*((int32_t*)L_57) = (int32_t)L_58;
		uint32_t* L_59 = ___result1;
		*((int32_t*)L_59) = (int32_t)((int32_t)65533);
		return (int32_t)(3);
	}

IL_0150:
	{
		int32_t* L_60 = ___bytesConsumed2;
		int32_t L_61 = V_0;
		*((int32_t*)L_60) = (int32_t)L_61;
		uint32_t* L_62 = ___result1;
		*((int32_t*)L_62) = (int32_t)((int32_t)65533);
		return (int32_t)(2);
	}
}
// System.ReadOnlySpan`1<System.UInt32> System.Text.Unicode.UnicodeHelpers::GetDefinedCharacterBitmap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  UnicodeHelpers_GetDefinedCharacterBitmap_mC09168E637ACEFF8DC6986A529B31C0352C8D60B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_1;
		L_1 = UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B(/*hidden argument*/NULL);
		ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  L_2;
		L_2 = MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597(L_1, /*hidden argument*/MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_RuntimeMethod_var);
		return L_2;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_3 = ((UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_StaticFields*)il2cpp_codegen_static_fields_for(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var))->get__definedCharacterBitmapBigEndian_0();
		ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  L_4;
		L_4 = ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387(L_3, /*hidden argument*/ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_RuntimeMethod_var);
		return L_4;
	}
}
// System.Void System.Text.Unicode.UnicodeHelpers::GetUtf16SurrogatePairFromAstralScalarValue(System.Int32,System.Char&,System.Char&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC7309772F2EDE8846DFBFFC499EFBBA3A40E4586 (int32_t ___scalar0, Il2CppChar* ___highSurrogate1, Il2CppChar* ___lowSurrogate2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___scalar0;
		V_0 = ((int32_t)((int32_t)L_0&(int32_t)((int32_t)65535)));
		int32_t L_1 = ___scalar0;
		V_1 = ((int32_t)((int32_t)L_1>>(int32_t)((int32_t)16)));
		int32_t L_2 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		Il2CppChar* L_3 = ___highSurrogate1;
		int32_t L_4 = V_2;
		int32_t L_5 = V_0;
		*((int16_t*)L_3) = (int16_t)((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)55296)|(int32_t)((int32_t)((int32_t)L_4<<(int32_t)6))))|(int32_t)((int32_t)((int32_t)L_5>>(int32_t)((int32_t)10)))))));
		Il2CppChar* L_6 = ___lowSurrogate2;
		int32_t L_7 = V_0;
		*((int16_t*)L_6) = (int16_t)((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)56320)|(int32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)1023)))))));
		return;
	}
}
// System.Boolean System.Text.Unicode.UnicodeHelpers::IsSupplementaryCodePoint(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456 (int32_t ___scalar0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___scalar0;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-65536)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.ReadOnlySpan`1<System.Byte> System.Text.Unicode.UnicodeHelpers::get_DefinedCharsBitmapSpan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30____8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_inline((&L_0), (void*)(void*)(il2cpp_codegen_get_field_data(U3CPrivateImplementationDetailsU3E_tECD56639E92428D6DACE7D7A2BA2ADEFC8698E30____8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0_FieldInfo_var)), ((int32_t)8192), /*hidden argument*/ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_RuntimeMethod_var);
		return L_0;
	}
}
// System.Void System.Text.Unicode.UnicodeHelpers::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeHelpers__cctor_mA13B36CB9011BAAB221BAFB70DD4F943E31BAF61 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_1;
		L_1 = UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m5276C6DA38121F0033E91B53041F9CBFAC7FD9CA(/*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = ((UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)(NULL));
	}

IL_000f:
	{
		((UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_StaticFields*)il2cpp_codegen_static_fields_for(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var))->set__definedCharacterBitmapBigEndian_0(G_B3_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Text.Unicode.UnicodeRange::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2 (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___firstCodePoint0, int32_t ___length1, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___firstCodePoint0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___firstCodePoint0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_001d;
		}
	}

IL_0012:
	{
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral09EAD6A50C87B14995000A914300979F01096C97)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2_RuntimeMethod_var)));
	}

IL_001d:
	{
		int32_t L_3 = ___length1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = ___firstCodePoint0;
		int32_t L_5 = ___length1;
		if ((((int64_t)((int64_t)il2cpp_codegen_add((int64_t)((int64_t)((int64_t)L_4)), (int64_t)((int64_t)((int64_t)L_5))))) <= ((int64_t)((int64_t)((int64_t)((int32_t)65536))))))
		{
			goto IL_0039;
		}
	}

IL_002e:
	{
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_6 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_6, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralE8744A8B8BD390EB66CA0CAE2376C973E6904FFB)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2_RuntimeMethod_var)));
	}

IL_0039:
	{
		int32_t L_7 = ___firstCodePoint0;
		UnicodeRange_set_FirstCodePoint_m529C6CC5756D4A2AC163B3A6D274C3651CEBA345_inline(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = ___length1;
		UnicodeRange_set_Length_m8994FAD9DDDC18EE1F2757E5444584EA3DDF89E4_inline(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Text.Unicode.UnicodeRange::get_FirstCodePoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnicodeRange_get_FirstCodePoint_mE2ECE0C7C54A3047DF4DBA68271FBF8067BCEB26 (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CFirstCodePointU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void System.Text.Unicode.UnicodeRange::set_FirstCodePoint(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeRange_set_FirstCodePoint_m529C6CC5756D4A2AC163B3A6D274C3651CEBA345 (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFirstCodePointU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 System.Text.Unicode.UnicodeRange::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnicodeRange_get_Length_mD690963E9ABE4A2F5731E5CF27C00292C7FFC696 (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CLengthU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void System.Text.Unicode.UnicodeRange::set_Length(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnicodeRange_set_Length_m8994FAD9DDDC18EE1F2757E5444584EA3DDF89E4 (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CLengthU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRange::Create(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B (Il2CppChar ___firstCharacter0, Il2CppChar ___lastCharacter1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___lastCharacter1;
		Il2CppChar L_1 = ___firstCharacter0;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralA7724F58887AE658863220F8D9138F5AC5532B2C)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B_RuntimeMethod_var)));
	}

IL_000f:
	{
		Il2CppChar L_3 = ___firstCharacter0;
		Il2CppChar L_4 = ___lastCharacter1;
		Il2CppChar L_5 = ___firstCharacter0;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_6 = (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 *)il2cpp_codegen_object_new(UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1_il2cpp_TypeInfo_var);
		UnicodeRange__ctor_mF024254B5BD9939F73CB3722DCD37E88D91FD0B2(L_6, L_3, ((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5)))), /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::CreateRange(System.Text.Unicode.UnicodeRange&,System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * UnicodeRanges_CreateRange_mDD642B176A42F1DE3EEE990E612E116293DE340C (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** ___range0, Il2CppChar ___first1, Il2CppChar ___last2, const RuntimeMethod* method)
{
	{
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** L_0 = ___range0;
		Il2CppChar L_1 = ___first1;
		Il2CppChar L_2 = ___last2;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_3;
		L_3 = UnicodeRange_Create_m3BF14FDDC800EBFD00402BB02B86B9881CF6D37B(L_1, L_2, /*hidden argument*/NULL);
		VolatileWrite((UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 **)L_0, L_3);
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 ** L_4 = ___range0;
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_5 = *((UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 **)L_4);
		return L_5;
	}
}
// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::get_BasicLatin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * UnicodeRanges_get_BasicLatin_mA46E04A614BC261AFCE0DB4A27734D38BE369F7B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * G_B2_0 = NULL;
	UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * G_B1_0 = NULL;
	{
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_0 = ((UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_StaticFields*)il2cpp_codegen_static_fields_for(UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_il2cpp_TypeInfo_var))->get__u0000_0();
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0016;
		}
	}
	{
		UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * L_2;
		L_2 = UnicodeRanges_CreateRange_mDD642B176A42F1DE3EEE990E612E116293DE340C((UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 **)(((UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_StaticFields*)il2cpp_codegen_static_fields_for(UnicodeRanges_t865E56FF7E48B26ACFC6B0B633E1BB27D0EA8BA2_il2cpp_TypeInfo_var))->get_address_of__u0000_0()), 0, ((int32_t)127), /*hidden argument*/NULL);
		G_B2_0 = L_2;
	}

IL_0016:
	{
		return G_B2_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 System.Text.UnicodeUtility::GetScalarFromUtf16SurrogatePair(System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6866E0E09B45BF1CDE2BB7865AF20F6460C752D0 (uint32_t ___highSurrogateCodePoint0, uint32_t ___lowSurrogateCodePoint1, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___highSurrogateCodePoint0;
		uint32_t L_1 = ___lowSurrogateCodePoint1;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)10))), (int32_t)L_1)), (int32_t)((int32_t)56613888)));
	}
}
// System.Int32 System.Text.UnicodeUtility::GetUtf16SequenceLength(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnicodeUtility_GetUtf16SequenceLength_mBAB104741499D1D4454BF674A1A4B380E3DA9A9E (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		___value0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)((int32_t)65536)));
		uint32_t L_1 = ___value0;
		___value0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)33554432)));
		uint32_t L_2 = ___value0;
		___value0 = ((int32_t)((uint32_t)L_2>>((int32_t)24)));
		uint32_t L_3 = ___value0;
		return L_3;
	}
}
// System.Boolean System.Text.UnicodeUtility::IsAsciiCodePoint(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		return (bool)((((int32_t)((!(((uint32_t)L_0) <= ((uint32_t)((int32_t)127))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Text.UnicodeUtility::IsHighSurrogateCodePoint(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnicodeUtility_IsHighSurrogateCodePoint_mC6FC7E1678F3638CDDE48C9DE27C7ECE51D16654 (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		bool L_1;
		L_1 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_0, ((int32_t)55296), ((int32_t)56319), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Text.UnicodeUtility::IsInRangeInclusive(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C (uint32_t ___value0, uint32_t ___lowerBound1, uint32_t ___upperBound2, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		uint32_t L_1 = ___lowerBound1;
		uint32_t L_2 = ___upperBound2;
		uint32_t L_3 = ___lowerBound1;
		return (bool)((((int32_t)((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1))) <= ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Text.UnicodeUtility::IsLowSurrogateCodePoint(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnicodeUtility_IsLowSurrogateCodePoint_mFC9196588FB51D463621246A555CDF8EBD7C4F89 (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		bool L_1;
		L_1 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_0, ((int32_t)56320), ((int32_t)57343), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Text.UnicodeUtility::IsSurrogateCodePoint(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnicodeUtility_IsSurrogateCodePoint_m1C61245755AFFC5F88AEFDE2511BD42EC2E35471 (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		bool L_1;
		L_1 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_0, ((int32_t)55296), ((int32_t)57343), /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


// Conversion methods for marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
IL2CPP_EXTERN_C void AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshal_pinvoke(const AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964& unmarshaled, AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_pinvoke& marshaled)
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke(unmarshaled.get_Data_0(), marshaled.___Data_0);
}
IL2CPP_EXTERN_C void AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshal_pinvoke_back(const AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_pinvoke& marshaled, AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964& unmarshaled)
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3  unmarshaled_Data_temp_0;
	memset((&unmarshaled_Data_temp_0), 0, sizeof(unmarshaled_Data_temp_0));
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke_back(marshaled.___Data_0, unmarshaled_Data_temp_0);
	unmarshaled.set_Data_0(unmarshaled_Data_temp_0);
}
// Conversion method for clean up from marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
IL2CPP_EXTERN_C void AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshal_pinvoke_cleanup(AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_pinvoke& marshaled)
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke_cleanup(marshaled.___Data_0);
}


// Conversion methods for marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
IL2CPP_EXTERN_C void AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshal_com(const AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964& unmarshaled, AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_com& marshaled)
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com(unmarshaled.get_Data_0(), marshaled.___Data_0);
}
IL2CPP_EXTERN_C void AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshal_com_back(const AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_com& marshaled, AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964& unmarshaled)
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3  unmarshaled_Data_temp_0;
	memset((&unmarshaled_Data_temp_0), 0, sizeof(unmarshaled_Data_temp_0));
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com_back(marshaled.___Data_0, unmarshaled_Data_temp_0);
	unmarshaled.set_Data_0(unmarshaled_Data_temp_0);
}
// Conversion method for clean up from marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
IL2CPP_EXTERN_C void AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshal_com_cleanup(AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964_marshaled_com& marshaled)
{
	U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com_cleanup(marshaled.___Data_0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke& marshaled)
{
	marshaled.___FixedElementField_0 = static_cast<int32_t>(unmarshaled.get_FixedElementField_0());
}
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke_back(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke& marshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled)
{
	bool unmarshaled_FixedElementField_temp_0 = false;
	unmarshaled_FixedElementField_temp_0 = static_cast<bool>(marshaled.___FixedElementField_0);
	unmarshaled.set_FixedElementField_0(unmarshaled_FixedElementField_temp_0);
}
// Conversion method for clean up from marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_pinvoke_cleanup(U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com& marshaled)
{
	marshaled.___FixedElementField_0 = static_cast<int32_t>(unmarshaled.get_FixedElementField_0());
}
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com_back(const U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com& marshaled, U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3& unmarshaled)
{
	bool unmarshaled_FixedElementField_temp_0 = false;
	unmarshaled_FixedElementField_temp_0 = static_cast<bool>(marshaled.___FixedElementField_0);
	unmarshaled.set_FixedElementField_0(unmarshaled_FixedElementField_temp_0);
}
// Conversion method for clean up from marshalling of: System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
IL2CPP_EXTERN_C void U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshal_com_cleanup(U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  UnicodeHelpers_GetDefinedCharacterBitmap_mC09168E637ACEFF8DC6986A529B31C0352C8D60B_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_1;
		L_1 = UnicodeHelpers_get_DefinedCharsBitmapSpan_mE8E1E773B4784A3743B8B407200DBC6368132F0B(/*hidden argument*/NULL);
		ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  L_2;
		L_2 = MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597(L_1, /*hidden argument*/MemoryMarshal_Cast_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mFDF63C7709748A6396A7E95CBA1879A46CF55597_RuntimeMethod_var);
		return L_2;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_3 = ((UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_StaticFields*)il2cpp_codegen_static_fields_for(UnicodeHelpers_t34D7B4DE51AE4026D172E562429F28B907FFA39C_il2cpp_TypeInfo_var))->get__definedCharacterBitmapBigEndian_0();
		ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8  L_4;
		L_4 = ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387(L_3, /*hidden argument*/ReadOnlySpan_1_op_Implicit_m635C328D15714DDFAF7F6A418B8907606656C387_RuntimeMethod_var);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool AllowedCharactersBitmap_IsUnicodeScalarAllowed_mACD46C15E049867196EBD270ED6CFDEABBA32373_inline (AllowedCharactersBitmap_t1D4B93C3295D1CD7A3A0EE997681EE3FFF6D81DC * __this, int32_t ___unicodeScalar0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___unicodeScalar0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)5));
		int32_t L_1 = ___unicodeScalar0;
		V_1 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)));
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_2 = __this->get__allowedCharacters_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int32_t L_6 = V_1;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)31)))))))) <= ((uint32_t)0)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeHelpers_IsSupplementaryCodePoint_m3F873CC59E3FB1E7C05004A5B0CDF4B1D1F38456_inline (int32_t ___scalar0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___scalar0;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-65536)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TextEncoder_TryWriteScalarAsChar_m292F186843687AB19956FF920F3B78B8B136ACF5_inline (int32_t ___unicodeScalar0, Il2CppChar* ___destination1, int32_t ___destinationLength2, int32_t* ___numberOfCharactersWritten3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___destinationLength2;
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t* L_1 = ___numberOfCharactersWritten3;
		*((int32_t*)L_1) = (int32_t)0;
		return (bool)0;
	}

IL_0009:
	{
		Il2CppChar* L_2 = ___destination1;
		int32_t L_3 = ___unicodeScalar0;
		*((int16_t*)L_2) = (int16_t)((int32_t)((uint16_t)L_3));
		int32_t* L_4 = ___numberOfCharactersWritten3;
		*((int32_t*)L_4) = (int32_t)1;
		return (bool)1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar HexConverter_ToCharUpper_mDB7A54AE6AC5CEFB9BA8F0BCDF1E1291A78508D0_inline (int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		___value0 = ((int32_t)((int32_t)L_0&(int32_t)((int32_t)15)));
		int32_t L_1 = ___value0;
		___value0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)48)));
		int32_t L_2 = ___value0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = ___value0;
		___value0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)7));
	}

IL_0016:
	{
		int32_t L_4 = ___value0;
		return ((int32_t)((uint16_t)L_4));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mBCD92F26CD88B2855FA734E26B0DBB18F0763482_inline (Il2CppChar ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Il2CppChar L_0 = ___value0;
		if ((((int32_t)L_0) > ((int32_t)((int32_t)127))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DefaultJavaScriptEncoderBasicLatin_t58DCF880FA21795658EC642C6557D4C51DCC208A_il2cpp_TypeInfo_var);
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_1;
		L_1 = DefaultJavaScriptEncoderBasicLatin_get_AllowList_m55A0EACA3E6FC044238783815FFC2EFBBACED2C5(/*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppChar L_2 = ___value0;
		uint8_t* L_3;
		L_3 = ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&V_0), L_2, /*hidden argument*/ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_RuntimeMethod_var);
		int32_t L_4 = *((uint8_t*)L_3);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0018:
	{
		return (bool)1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsAsciiCodePoint_mB0ACE861891DEB6E6676340F094B1DB6E477B47D_inline (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		return (bool)((((int32_t)((!(((uint32_t)L_0) <= ((uint32_t)((int32_t)127))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TextEncoder_GetAsciiEncoding_m6FF6E556B2432F8F8FB0E9568B6E9C398BBD0829_inline (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	{
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_0 = __this->get__asciiEscape_0();
		uint8_t L_1 = ___value0;
		NullCheck(L_0);
		uint8_t L_2 = L_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = V_0;
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		uint8_t L_5 = ___value0;
		bool L_6;
		L_6 = VirtualFuncInvoker1< bool, int32_t >::Invoke(6 /* System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32) */, __this, L_5);
		if (L_6)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = ((TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_StaticFields*)il2cpp_codegen_static_fields_for(TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04_il2cpp_TypeInfo_var))->get_s_noEscape_3();
		V_0 = L_7;
		ByteU5BU5DU5BU5D_t95107DE217CCFA8CD77945AC2CB9492D4D01FE8D* L_8 = __this->get__asciiEscape_0();
		uint8_t L_9 = ___value0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_10);
	}

IL_0024:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = V_0;
		return L_11;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TextEncoder_DoesAsciiNeedEncoding_m0525D9CBBC3FA4494ECD7D440EE01384F64DF27F_inline (TextEncoder_t24DE35AC27BA4933885C4161F808A699FD150D04 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		AsciiNeedsEscapingData_t7FD4060839F175F52DF30EE55CB9210030200964 * L_0 = __this->get_address_of__asciiNeedsEscaping_2();
		U3CDataU3Ee__FixedBuffer_t20F7210CF9F6481633AAC1847FC21F6CBDFFA6A3 * L_1 = L_0->get_address_of_Data_0();
		bool* L_2 = L_1->get_address_of_FixedElementField_0();
		uint32_t L_3 = ___value0;
		int32_t L_4 = *((uint8_t*)((bool*)il2cpp_codegen_add((intptr_t)L_2, (intptr_t)((uintptr_t)L_3))));
		return (bool)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsSurrogateCodePoint_m1C61245755AFFC5F88AEFDE2511BD42EC2E35471_inline (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		bool L_1;
		L_1 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_0, ((int32_t)55296), ((int32_t)57343), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsHighSurrogateCodePoint_mC6FC7E1678F3638CDDE48C9DE27C7ECE51D16654_inline (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		bool L_1;
		L_1 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_0, ((int32_t)55296), ((int32_t)56319), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsLowSurrogateCodePoint_mFC9196588FB51D463621246A555CDF8EBD7C4F89_inline (uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		bool L_1;
		L_1 = UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline(L_0, ((int32_t)56320), ((int32_t)57343), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UnicodeRange_get_FirstCodePoint_mE2ECE0C7C54A3047DF4DBA68271FBF8067BCEB26_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CFirstCodePointU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UnicodeRange_get_Length_mD690963E9ABE4A2F5731E5CF27C00292C7FFC696_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CLengthU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t BinaryPrimitives_ReadUInt32LittleEndian_mEE46641BC73CAACA64F2952CD791BE96F5DB44F4_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_0 = ___source0;
		uint32_t L_1;
		L_1 = MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_inline(L_0, /*hidden argument*/MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_RuntimeMethod_var);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		bool L_2 = ((BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		uint32_t L_3 = V_0;
		uint32_t L_4;
		L_4 = BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20_inline(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0015:
	{
		uint32_t L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UnicodeUtility_IsInRangeInclusive_mB5224DD9614F1E17155F420786DA6B01BA7AEA3C_inline (uint32_t ___value0, uint32_t ___lowerBound1, uint32_t ___upperBound2, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		uint32_t L_1 = ___lowerBound1;
		uint32_t L_2 = ___upperBound2;
		uint32_t L_3 = ___lowerBound1;
		return (bool)((((int32_t)((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1))) <= ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnicodeRange_set_FirstCodePoint_m529C6CC5756D4A2AC163B3A6D274C3651CEBA345_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFirstCodePointU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnicodeRange_set_Length_m8994FAD9DDDC18EE1F2757E5444584EA3DDF89E4_inline (UnicodeRange_tF6D9BE9976E0808A6F9FED783E291E84201C73B1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CLengthU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t* ReadOnlySpan_1_get_Item_m7506F2DD9B49E258D9FF7F604E3D208E23724BEC_gshared_inline (ReadOnlySpan_1_t9B1935E8B7D4B290267E8A535D027D55C09D42F8 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 * L_2 = (Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 *)__this->get__pinnable_0();
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		intptr_t L_3 = (intptr_t)__this->get__byteOffset_1();
		V_0 = (intptr_t)L_3;
		void* L_4;
		L_4 = IntPtr_ToPointer_m5C7CE32B14B6E30467B378052FEA25300833C61F_inline((intptr_t*)(intptr_t*)(&V_0), /*hidden argument*/NULL);
		uint32_t* L_5;
		L_5 = ((  uint32_t* (*) (void*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15)->methodPointer)((void*)(void*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15));
		int32_t L_6 = ___index0;
		uint32_t* L_7;
		L_7 = ((  uint32_t* (*) (uint32_t*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16)->methodPointer)((uint32_t*)(uint32_t*)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16));
		return (uint32_t*)L_7;
	}

IL_0030:
	{
		Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 * L_8 = (Pinnable_1_t4E40323FD6DE85A9C83E29C1A0CFB7EFDE2B1424 *)__this->get__pinnable_0();
		NullCheck(L_8);
		uint32_t* L_9 = (uint32_t*)L_8->get_address_of_Data_0();
		intptr_t L_10 = (intptr_t)__this->get__byteOffset_1();
		uint32_t* L_11;
		L_11 = ((  uint32_t* (*) (uint32_t*, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 17)->methodPointer)((uint32_t*)(uint32_t*)L_9, (intptr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 17));
		int32_t L_12 = ___index0;
		uint32_t* L_13;
		L_13 = ((  uint32_t* (*) (uint32_t*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16)->methodPointer)((uint32_t*)(uint32_t*)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16));
		return (uint32_t*)L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t* ReadOnlySpan_1_get_Item_mA0E86B3FF6945DBAEBB41C7887674F0D53248DDE_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_2 = (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)__this->get__pinnable_0();
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		intptr_t L_3 = (intptr_t)__this->get__byteOffset_1();
		V_0 = (intptr_t)L_3;
		void* L_4;
		L_4 = IntPtr_ToPointer_m5C7CE32B14B6E30467B378052FEA25300833C61F_inline((intptr_t*)(intptr_t*)(&V_0), /*hidden argument*/NULL);
		uint8_t* L_5;
		L_5 = ((  uint8_t* (*) (void*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15)->methodPointer)((void*)(void*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15));
		int32_t L_6 = ___index0;
		uint8_t* L_7;
		L_7 = ((  uint8_t* (*) (uint8_t*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16)->methodPointer)((uint8_t*)(uint8_t*)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16));
		return (uint8_t*)L_7;
	}

IL_0030:
	{
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_8 = (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)__this->get__pinnable_0();
		NullCheck(L_8);
		uint8_t* L_9 = (uint8_t*)L_8->get_address_of_Data_0();
		intptr_t L_10 = (intptr_t)__this->get__byteOffset_1();
		uint8_t* L_11;
		L_11 = ((  uint8_t* (*) (uint8_t*, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 17)->methodPointer)((uint8_t*)(uint8_t*)L_9, (intptr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 17));
		int32_t L_12 = ___index0;
		uint8_t* L_13;
		L_13 = ((  uint8_t* (*) (uint8_t*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16)->methodPointer)((uint8_t*)(uint8_t*)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16));
		return (uint8_t*)L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__length_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ReadOnlySpan_1__ctor_m52E53BB8862F26B1C23ED6BF8DE68F97F9BEFDDF_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, void* ___pointer0, int32_t ___length1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0;
		L_0 = ((  bool (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 13)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 13));
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->klass)->rgctx_data, 14)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4((Type_t *)L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		int32_t L_3 = ___length1;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_0020:
	{
		int32_t L_4 = ___length1;
		__this->set__length_2(L_4);
		__this->set__pinnable_0((Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)NULL);
		void* L_5 = ___pointer0;
		intptr_t L_6;
		memset((&L_6), 0, sizeof(L_6));
		IntPtr__ctor_mBB7AF6DA6350129AD6422DE474FD52F715CC0C40_inline((&L_6), (void*)(void*)L_5, /*hidden argument*/NULL);
		__this->set__byteOffset_1((intptr_t)L_6);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Span_1_get_Length_m4BFDA5E41279728ADF75E310F780E357ECB1923B_gshared_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__length_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_Slice_mF1BE8D57B1EBD1DAA878AFC7D38C9919B3226E9C_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___start0, int32_t ___length1, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) <= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___length1;
		int32_t L_3 = (int32_t)__this->get__length_2();
		int32_t L_4 = ___start0;
		if ((!(((uint32_t)L_2) > ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4))))))
		{
			goto IL_001a;
		}
	}

IL_0014:
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		intptr_t L_5 = (intptr_t)__this->get__byteOffset_1();
		int32_t L_6 = ___start0;
		intptr_t L_7;
		L_7 = ((  intptr_t (*) (intptr_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12)->methodPointer)((intptr_t)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12));
		V_0 = (intptr_t)L_7;
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_8 = (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)__this->get__pinnable_0();
		intptr_t L_9 = V_0;
		int32_t L_10 = ___length1;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_11;
		memset((&L_11), 0, sizeof(L_11));
		ReadOnlySpan_1__ctor_m19A753A110C19315A508935C46EA96BA81A69AC7_inline((&L_11), (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)L_8, (intptr_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 25));
		return (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 )L_11;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ReadOnlySpan_1_Slice_mE1BAD6A11C884E035D359E20DC4DC07FD20A4A4B_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, int32_t ___start0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) > ((uint32_t)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_000f:
	{
		intptr_t L_2 = (intptr_t)__this->get__byteOffset_1();
		int32_t L_3 = ___start0;
		intptr_t L_4;
		L_4 = ((  intptr_t (*) (intptr_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12)->methodPointer)((intptr_t)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12));
		V_0 = (intptr_t)L_4;
		int32_t L_5 = (int32_t)__this->get__length_2();
		int32_t L_6 = ___start0;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_6));
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_7 = (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)__this->get__pinnable_0();
		intptr_t L_8 = V_0;
		int32_t L_9 = V_1;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_10;
		memset((&L_10), 0, sizeof(L_10));
		ReadOnlySpan_1__ctor_m19A753A110C19315A508935C46EA96BA81A69AC7_inline((&L_10), (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)L_7, (intptr_t)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 25));
		return (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 )L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  Span_1_Slice_mB88D43B3503491E904A7607A97A3E689E4C5C0A0_gshared_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, int32_t ___start0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) > ((uint32_t)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_000f:
	{
		intptr_t L_2 = (intptr_t)__this->get__byteOffset_1();
		int32_t L_3 = ___start0;
		intptr_t L_4;
		L_4 = ((  intptr_t (*) (intptr_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 14)->methodPointer)((intptr_t)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 14));
		V_0 = (intptr_t)L_4;
		int32_t L_5 = (int32_t)__this->get__length_2();
		int32_t L_6 = ___start0;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_6));
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_7 = (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)__this->get__pinnable_0();
		intptr_t L_8 = V_0;
		int32_t L_9 = V_1;
		Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Span_1__ctor_m373EA84BF632F6408591B525142C56CAC893C040_inline((&L_10), (Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 *)L_7, (intptr_t)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15));
		return (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 )L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Span_1_get_Length_m102A7AD9B8F41CC1099A041EE9CA4EB824471429_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__length_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ReadOnlySpan_1_get_Length_m626D8806A4F947921582C93B00D24CA957C9FB13_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__length_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ReadOnlySpan_1_Slice_m263B11AC38B6DD520C1C0CCEC53D1AE555FB7B41_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___start0, int32_t ___length1, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) <= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___length1;
		int32_t L_3 = (int32_t)__this->get__length_2();
		int32_t L_4 = ___start0;
		if ((!(((uint32_t)L_2) > ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4))))))
		{
			goto IL_001a;
		}
	}

IL_0014:
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		intptr_t L_5 = (intptr_t)__this->get__byteOffset_1();
		int32_t L_6 = ___start0;
		intptr_t L_7;
		L_7 = ((  intptr_t (*) (intptr_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12)->methodPointer)((intptr_t)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12));
		V_0 = (intptr_t)L_7;
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_8 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		intptr_t L_9 = V_0;
		int32_t L_10 = ___length1;
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_11;
		memset((&L_11), 0, sizeof(L_11));
		ReadOnlySpan_1__ctor_m0D025D7B51DA191FC6D80F997B27442A9F967A1D_inline((&L_11), (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)L_8, (intptr_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 25));
		return (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 )L_11;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  ReadOnlySpan_1_Slice_mC055D9B8DCA2EB4562338BF36B89CC4B229D5C4D_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___start0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) > ((uint32_t)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_000f:
	{
		intptr_t L_2 = (intptr_t)__this->get__byteOffset_1();
		int32_t L_3 = ___start0;
		intptr_t L_4;
		L_4 = ((  intptr_t (*) (intptr_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12)->methodPointer)((intptr_t)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12));
		V_0 = (intptr_t)L_4;
		int32_t L_5 = (int32_t)__this->get__length_2();
		int32_t L_6 = ___start0;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_6));
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_7 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		intptr_t L_8 = V_0;
		int32_t L_9 = V_1;
		ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3  L_10;
		memset((&L_10), 0, sizeof(L_10));
		ReadOnlySpan_1__ctor_m0D025D7B51DA191FC6D80F997B27442A9F967A1D_inline((&L_10), (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)L_7, (intptr_t)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 25));
		return (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 )L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  Span_1_Slice_m3139EE8D9A681BF27501A6BDAC851A1FBBB1AF73_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, int32_t ___start0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) > ((uint32_t)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)1, /*hidden argument*/NULL);
	}

IL_000f:
	{
		intptr_t L_2 = (intptr_t)__this->get__byteOffset_1();
		int32_t L_3 = ___start0;
		intptr_t L_4;
		L_4 = ((  intptr_t (*) (intptr_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 14)->methodPointer)((intptr_t)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 14));
		V_0 = (intptr_t)L_4;
		int32_t L_5 = (int32_t)__this->get__length_2();
		int32_t L_6 = ___start0;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_6));
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_7 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		intptr_t L_8 = V_0;
		int32_t L_9 = V_1;
		Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Span_1__ctor_m72C2E4C6E7A998608E6F19E69D922E7A70F65B86_inline((&L_10), (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)L_7, (intptr_t)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15));
		return (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D )L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Array_Empty_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDF4F3B1C8C7DF22FFDB9CAA78E14C008B6F495C2_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ((EmptyArray_1_tB2402F7A8151EE5618C0BCC8815C169E00142333_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* Array_Empty_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m4B65C10F11A56C51206C1894D604C9CB51DF08CF_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_0 = ((EmptyArray_1_t8C9D46673F64ABE360DE6F02C2BA0A5566DC9FDC_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar* ReadOnlySpan_1_get_Item_m856578D58B9F85C63B15E582398B5EC2A49D1203_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_2 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		intptr_t L_3 = (intptr_t)__this->get__byteOffset_1();
		V_0 = (intptr_t)L_3;
		void* L_4;
		L_4 = IntPtr_ToPointer_m5C7CE32B14B6E30467B378052FEA25300833C61F_inline((intptr_t*)(intptr_t*)(&V_0), /*hidden argument*/NULL);
		Il2CppChar* L_5;
		L_5 = ((  Il2CppChar* (*) (void*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15)->methodPointer)((void*)(void*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 15));
		int32_t L_6 = ___index0;
		Il2CppChar* L_7;
		L_7 = ((  Il2CppChar* (*) (Il2CppChar*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16)->methodPointer)((Il2CppChar*)(Il2CppChar*)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16));
		return (Il2CppChar*)L_7;
	}

IL_0030:
	{
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_8 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		NullCheck(L_8);
		Il2CppChar* L_9 = (Il2CppChar*)L_8->get_address_of_Data_0();
		intptr_t L_10 = (intptr_t)__this->get__byteOffset_1();
		Il2CppChar* L_11;
		L_11 = ((  Il2CppChar* (*) (Il2CppChar*, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 17)->methodPointer)((Il2CppChar*)(Il2CppChar*)L_9, (intptr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 17));
		int32_t L_12 = ___index0;
		Il2CppChar* L_13;
		L_13 = ((  Il2CppChar* (*) (Il2CppChar*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16)->methodPointer)((Il2CppChar*)(Il2CppChar*)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 16));
		return (Il2CppChar*)L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar* Span_1_get_Item_m4D70FCC473E282316D5DFB7167F1E407A65E11FB_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__length_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_2 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		intptr_t L_3 = (intptr_t)__this->get__byteOffset_1();
		V_0 = (intptr_t)L_3;
		void* L_4;
		L_4 = IntPtr_ToPointer_m5C7CE32B14B6E30467B378052FEA25300833C61F_inline((intptr_t*)(intptr_t*)(&V_0), /*hidden argument*/NULL);
		Il2CppChar* L_5;
		L_5 = ((  Il2CppChar* (*) (void*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 18)->methodPointer)((void*)(void*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 18));
		int32_t L_6 = ___index0;
		Il2CppChar* L_7;
		L_7 = ((  Il2CppChar* (*) (Il2CppChar*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 19)->methodPointer)((Il2CppChar*)(Il2CppChar*)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 19));
		return (Il2CppChar*)(L_7);
	}

IL_0030:
	{
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_8 = (Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C *)__this->get__pinnable_0();
		NullCheck(L_8);
		Il2CppChar* L_9 = (Il2CppChar*)L_8->get_address_of_Data_0();
		intptr_t L_10 = (intptr_t)__this->get__byteOffset_1();
		Il2CppChar* L_11;
		L_11 = ((  Il2CppChar* (*) (Il2CppChar*, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 20)->methodPointer)((Il2CppChar*)(Il2CppChar*)L_9, (intptr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 20));
		int32_t L_12 = ___index0;
		Il2CppChar* L_13;
		L_13 = ((  Il2CppChar* (*) (Il2CppChar*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 19)->methodPointer)((Il2CppChar*)(Il2CppChar*)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 19));
		return (Il2CppChar*)(L_13);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20_inline (uint32_t ___value0, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	{
		uint32_t L_0 = ___value0;
		V_0 = ((int32_t)((int32_t)L_0&(int32_t)((int32_t)16711935)));
		uint32_t L_1 = ___value0;
		V_1 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-16711936)));
		uint32_t L_2 = V_0;
		uint32_t L_3 = V_0;
		uint32_t L_4 = V_1;
		uint32_t L_5 = V_1;
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_2>>8))|(int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)24))))), (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)((int32_t)((uint32_t)L_5>>((int32_t)24)))))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void* IntPtr_ToPointer_m5C7CE32B14B6E30467B378052FEA25300833C61F_inline (intptr_t* __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = *__this;
		return (void*)(L_0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void IntPtr__ctor_mBB7AF6DA6350129AD6422DE474FD52F715CC0C40_inline (intptr_t* __this, void* ___value0, const RuntimeMethod* method)
{
	{
		void* L_0 = ___value0;
		*__this = ((intptr_t)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t MemoryMarshal_Read_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_m6979E20D9DB999F6CC91AA46E0643802AA436AF9_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0;
		L_0 = ((  bool (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4((Type_t *)L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		int32_t L_3;
		L_3 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		int32_t L_4;
		L_4 = ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)(&___source0), /*hidden argument*/ReadOnlySpan_1_get_Length_m0D02A059B63020F14BCD1DDD5F72D4EBA34B3955_RuntimeMethod_var);
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_002a;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5((int32_t)0, /*hidden argument*/NULL);
	}

IL_002a:
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  L_5 = ___source0;
		uint8_t* L_6;
		L_6 = MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 )L_5, /*hidden argument*/MemoryMarshal_GetReference_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m72C9B7E3B84540539945F1E80ED3E1AAE90E5D93_RuntimeMethod_var);
		uint32_t L_7;
		L_7 = ((  uint32_t (*) (uint8_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((uint8_t*)(uint8_t*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return (uint32_t)L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ReadOnlySpan_1__ctor_m19A753A110C19315A508935C46EA96BA81A69AC7_gshared_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * __this, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___length2;
		__this->set__length_2(L_0);
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_1 = ___pinnable0;
		__this->set__pinnable_0(L_1);
		intptr_t L_2 = ___byteOffset1;
		__this->set__byteOffset_1((intptr_t)L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Span_1__ctor_m373EA84BF632F6408591B525142C56CAC893C040_gshared_inline (Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * __this, Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___length2;
		__this->set__length_2(L_0);
		Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * L_1 = ___pinnable0;
		__this->set__pinnable_0(L_1);
		intptr_t L_2 = ___byteOffset1;
		__this->set__byteOffset_1((intptr_t)L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ReadOnlySpan_1__ctor_m0D025D7B51DA191FC6D80F997B27442A9F967A1D_gshared_inline (ReadOnlySpan_1_t89EC7F5A5B7253DA599EEFE0E19D147F376E9DC3 * __this, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___length2;
		__this->set__length_2(L_0);
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_1 = ___pinnable0;
		__this->set__pinnable_0(L_1);
		intptr_t L_2 = ___byteOffset1;
		__this->set__byteOffset_1((intptr_t)L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Span_1__ctor_m72C2E4C6E7A998608E6F19E69D922E7A70F65B86_gshared_inline (Span_1_tA75C255D430A726B7E037BEBB5B06E037194902D * __this, Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * ___pinnable0, intptr_t ___byteOffset1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___length2;
		__this->set__length_2(L_0);
		Pinnable_1_t95E8BF0F62016718CAF24D71446F85A32986715C * L_1 = ___pinnable0;
		__this->set__pinnable_0(L_1);
		intptr_t L_2 = ___byteOffset1;
		__this->set__byteOffset_1((intptr_t)L_2);
		return;
	}
}
