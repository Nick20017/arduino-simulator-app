﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDefaultAliasAttribute
struct AssemblyDefaultAliasAttribute_tBED24B7B2D875CB2BD712ABC4099024C2505B7AA;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyMetadataAttribute
struct AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Diagnostics.CodeAnalysis.DisallowNullAttribute
struct DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D;
// System.Diagnostics.CodeAnalysis.DoesNotReturnAttribute
struct DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140;
// System.Diagnostics.CodeAnalysis.DynamicallyAccessedMembersAttribute
struct DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B;
// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_tE201891FE727EB3FB75B488A2BF6D4DF3CB80614;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.CompilerServices.IsByRefLikeAttribute
struct IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F;
// System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute
struct MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813;
// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2;
// System.Diagnostics.CodeAnalysis.NotNullWhenAttribute
struct NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA;
// System.Runtime.CompilerServices.NullableAttribute
struct NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417;
// System.Runtime.CompilerServices.NullableContextAttribute
struct NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDefaultAliasAttribute
struct AssemblyDefaultAliasAttribute_tBED24B7B2D875CB2BD712ABC4099024C2505B7AA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDefaultAliasAttribute::m_defaultAlias
	String_t* ___m_defaultAlias_0;

public:
	inline static int32_t get_offset_of_m_defaultAlias_0() { return static_cast<int32_t>(offsetof(AssemblyDefaultAliasAttribute_tBED24B7B2D875CB2BD712ABC4099024C2505B7AA, ___m_defaultAlias_0)); }
	inline String_t* get_m_defaultAlias_0() const { return ___m_defaultAlias_0; }
	inline String_t** get_address_of_m_defaultAlias_0() { return &___m_defaultAlias_0; }
	inline void set_m_defaultAlias_0(String_t* value)
	{
		___m_defaultAlias_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultAlias_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyMetadataAttribute
struct AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyMetadataAttribute::m_key
	String_t* ___m_key_0;
	// System.String System.Reflection.AssemblyMetadataAttribute::m_value
	String_t* ___m_value_1;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_key_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F, ___m_value_1)); }
	inline String_t* get_m_value_1() const { return ___m_value_1; }
	inline String_t** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(String_t* value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_value_1), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Diagnostics.DebuggerDisplayAttribute::name
	String_t* ___name_0;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::value
	String_t* ___value_1;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::type
	String_t* ___type_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_2), (void*)value);
	}
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Diagnostics.CodeAnalysis.DisallowNullAttribute
struct DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.CodeAnalysis.DoesNotReturnAttribute
struct DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.IsByRefLikeAttribute
struct IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute
struct MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute::<ReturnValue>k__BackingField
	bool ___U3CReturnValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CReturnValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813, ___U3CReturnValueU3Ek__BackingField_0)); }
	inline bool get_U3CReturnValueU3Ek__BackingField_0() const { return ___U3CReturnValueU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CReturnValueU3Ek__BackingField_0() { return &___U3CReturnValueU3Ek__BackingField_0; }
	inline void set_U3CReturnValueU3Ek__BackingField_0(bool value)
	{
		___U3CReturnValueU3Ek__BackingField_0 = value;
	}
};


// System.Diagnostics.CodeAnalysis.NotNullWhenAttribute
struct NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Diagnostics.CodeAnalysis.NotNullWhenAttribute::<ReturnValue>k__BackingField
	bool ___U3CReturnValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CReturnValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA, ___U3CReturnValueU3Ek__BackingField_0)); }
	inline bool get_U3CReturnValueU3Ek__BackingField_0() const { return ___U3CReturnValueU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CReturnValueU3Ek__BackingField_0() { return &___U3CReturnValueU3Ek__BackingField_0; }
	inline void set_U3CReturnValueU3Ek__BackingField_0(bool value)
	{
		___U3CReturnValueU3Ek__BackingField_0 = value;
	}
};


// System.Runtime.CompilerServices.NullableAttribute
struct NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Byte[] System.Runtime.CompilerServices.NullableAttribute::NullableFlags
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___NullableFlags_0;

public:
	inline static int32_t get_offset_of_NullableFlags_0() { return static_cast<int32_t>(offsetof(NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417, ___NullableFlags_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_NullableFlags_0() const { return ___NullableFlags_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_NullableFlags_0() { return &___NullableFlags_0; }
	inline void set_NullableFlags_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___NullableFlags_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NullableFlags_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.NullableContextAttribute
struct NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Byte System.Runtime.CompilerServices.NullableContextAttribute::Flag
	uint8_t ___Flag_0;

public:
	inline static int32_t get_offset_of_Flag_0() { return static_cast<int32_t>(offsetof(NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA, ___Flag_0)); }
	inline uint8_t get_Flag_0() const { return ___Flag_0; }
	inline uint8_t* get_address_of_Flag_0() { return &___Flag_0; }
	inline void set_Flag_0(uint8_t value)
	{
		___Flag_0 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.CodeAnalysis.DynamicallyAccessedMemberTypes
struct DynamicallyAccessedMemberTypes_t791D73C5D0A020B4FD7EA75B611765B8F25DE656 
{
public:
	// System.Int32 System.Diagnostics.CodeAnalysis.DynamicallyAccessedMemberTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DynamicallyAccessedMemberTypes_t791D73C5D0A020B4FD7EA75B611765B8F25DE656, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.ComponentModel.EditorBrowsableState
struct EditorBrowsableState_t5212E3E4B6F8B3190040444A9D6FBCA975F02BA1 
{
public:
	// System.Int32 System.ComponentModel.EditorBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorBrowsableState_t5212E3E4B6F8B3190040444A9D6FBCA975F02BA1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Resources.UltimateResourceFallbackLocation
struct UltimateResourceFallbackLocation_tA4EBEA627CD0C386314EBB60D7A4225C435D0F0B 
{
public:
	// System.Int32 System.Resources.UltimateResourceFallbackLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UltimateResourceFallbackLocation_tA4EBEA627CD0C386314EBB60D7A4225C435D0F0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// System.Diagnostics.CodeAnalysis.DynamicallyAccessedMembersAttribute
struct DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.CodeAnalysis.DynamicallyAccessedMemberTypes System.Diagnostics.CodeAnalysis.DynamicallyAccessedMembersAttribute::<MemberTypes>k__BackingField
	int32_t ___U3CMemberTypesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMemberTypesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B, ___U3CMemberTypesU3Ek__BackingField_0)); }
	inline int32_t get_U3CMemberTypesU3Ek__BackingField_0() const { return ___U3CMemberTypesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMemberTypesU3Ek__BackingField_0() { return &___U3CMemberTypesU3Ek__BackingField_0; }
	inline void set_U3CMemberTypesU3Ek__BackingField_0(int32_t value)
	{
		___U3CMemberTypesU3Ek__BackingField_0 = value;
	}
};


// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_tE201891FE727EB3FB75B488A2BF6D4DF3CB80614  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.ComponentModel.EditorBrowsableState System.ComponentModel.EditorBrowsableAttribute::browsableState
	int32_t ___browsableState_0;

public:
	inline static int32_t get_offset_of_browsableState_0() { return static_cast<int32_t>(offsetof(EditorBrowsableAttribute_tE201891FE727EB3FB75B488A2BF6D4DF3CB80614, ___browsableState_0)); }
	inline int32_t get_browsableState_0() const { return ___browsableState_0; }
	inline int32_t* get_address_of_browsableState_0() { return &___browsableState_0; }
	inline void set_browsableState_0(int32_t value)
	{
		___browsableState_0 = value;
	}
};


// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Resources.NeutralResourcesLanguageAttribute::_culture
	String_t* ____culture_0;
	// System.Resources.UltimateResourceFallbackLocation System.Resources.NeutralResourcesLanguageAttribute::_fallbackLoc
	int32_t ____fallbackLoc_1;

public:
	inline static int32_t get_offset_of__culture_0() { return static_cast<int32_t>(offsetof(NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2, ____culture_0)); }
	inline String_t* get__culture_0() const { return ____culture_0; }
	inline String_t** get_address_of__culture_0() { return &____culture_0; }
	inline void set__culture_0(String_t* value)
	{
		____culture_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____culture_0), (void*)value);
	}

	inline static int32_t get_offset_of__fallbackLoc_1() { return static_cast<int32_t>(offsetof(NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2, ____fallbackLoc_1)); }
	inline int32_t get__fallbackLoc_1() const { return ____fallbackLoc_1; }
	inline int32_t* get_address_of__fallbackLoc_1() { return &____fallbackLoc_1; }
	inline void set__fallbackLoc_1(int32_t value)
	{
		____fallbackLoc_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDefaultAliasAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDefaultAliasAttribute__ctor_m0C9991C32ED63B598FA509F3AF74554A5C874EB0 (AssemblyDefaultAliasAttribute_tBED24B7B2D875CB2BD712ABC4099024C2505B7AA * __this, String_t* ___defaultAlias0, const RuntimeMethod* method);
// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NeutralResourcesLanguageAttribute__ctor_mF2BB52FC7FE116CCA2AEDD08A2DA1DF7055B56AF (NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2 * __this, String_t* ___cultureName0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyMetadataAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529 (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * __this, String_t* ___key0, String_t* ___value1, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7 (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.CodeAnalysis.DoesNotReturnAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7 (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84 (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * __this, uint8_t p0, const RuntimeMethod* method);
// System.Void System.Diagnostics.CodeAnalysis.NotNullWhenAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13 (NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA * __this, bool ___returnValue0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988 (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19 (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * __this, uint8_t p0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsByRefLikeAttribute__ctor_mBAF4018209D90000A5E262F2451AE1C4A82314D9 (IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void System.ComponentModel.EditorBrowsableAttribute::.ctor(System.ComponentModel.EditorBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorBrowsableAttribute__ctor_mC77290C5157BDA154F1D03BD1551223B07A851D4 (EditorBrowsableAttribute_tE201891FE727EB3FB75B488A2BF6D4DF3CB80614 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void System.Diagnostics.CodeAnalysis.DynamicallyAccessedMembersAttribute::.ctor(System.Diagnostics.CodeAnalysis.DynamicallyAccessedMemberTypes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicallyAccessedMembersAttribute__ctor_mDBF0F1C7DE04C8B522241B8C64AE1EF300581AB2 (DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B * __this, int32_t ___memberTypes0, const RuntimeMethod* method);
// System.Void System.Diagnostics.CodeAnalysis.DisallowNullAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowNullAttribute__ctor_m828EAECBBC572B96840FDD59AB52545D6B5A604B (DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66 (MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 * __this, bool ___returnValue0, const RuntimeMethod* method);
static void System_Text_Json_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[3];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[4];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, true, NULL);
	}
	{
		AssemblyDefaultAliasAttribute_tBED24B7B2D875CB2BD712ABC4099024C2505B7AA * tmp = (AssemblyDefaultAliasAttribute_tBED24B7B2D875CB2BD712ABC4099024C2505B7AA *)cache->attributes[5];
		AssemblyDefaultAliasAttribute__ctor_m0C9991C32ED63B598FA509F3AF74554A5C874EB0(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x2E\x54\x65\x78\x74\x2E\x4A\x73\x6F\x6E"), NULL);
	}
	{
		NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2 * tmp = (NeutralResourcesLanguageAttribute_t14C9436446C8E9EB3C2244D386AF1C84ADC80FB2 *)cache->attributes[6];
		NeutralResourcesLanguageAttribute__ctor_mF2BB52FC7FE116CCA2AEDD08A2DA1DF7055B56AF(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x2D\x55\x53"), NULL);
	}
	{
		AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * tmp = (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F *)cache->attributes[7];
		AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x46\x72\x61\x6D\x65\x77\x6F\x72\x6B\x41\x73\x73\x65\x6D\x62\x6C\x79"), il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * tmp = (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F *)cache->attributes[8];
		AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x72\x76\x69\x63\x65\x61\x62\x6C\x65"), il2cpp_codegen_string_new_wrapper("\x54\x72\x75\x65"), NULL);
	}
	{
		AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * tmp = (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F *)cache->attributes[9];
		AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x65\x72\x49\x6E\x62\x6F\x78"), il2cpp_codegen_string_new_wrapper("\x54\x72\x75\x65"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[10];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[11];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\xC2\xA9\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E\x2E\x20\x41\x6C\x6C\x20\x72\x69\x67\x68\x74\x73\x20\x72\x65\x73\x65\x72\x76\x65\x64\x2E"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[12];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x2E\x54\x65\x78\x74\x2E\x4A\x73\x6F\x6E"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[13];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x35\x2E\x30\x2E\x35\x32\x31\x2E\x31\x36\x36\x30\x39"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[14];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x35\x2E\x30\x2E\x35\x2B\x32\x66\x37\x34\x30\x61\x64\x63\x31\x34\x35\x37\x65\x38\x61\x32\x38\x63\x31\x63\x30\x37\x32\x39\x39\x33\x62\x36\x36\x66\x35\x31\x35\x39\x37\x37\x65\x62\x35\x31"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[15];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\xC2\xAE\x20\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[16];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x2E\x54\x65\x78\x74\x2E\x4A\x73\x6F\x6E"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[17];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * tmp = (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F *)cache->attributes[18];
		AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x70\x6F\x73\x69\x74\x6F\x72\x79\x55\x72\x6C"), il2cpp_codegen_string_new_wrapper("\x67\x69\x74\x3A\x2F\x2F\x67\x69\x74\x68\x75\x62\x2E\x63\x6F\x6D\x2F\x64\x6F\x74\x6E\x65\x74\x2F\x72\x75\x6E\x74\x69\x6D\x65"), NULL);
	}
}
static void EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * tmp = (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7(tmp, NULL);
	}
}
static void IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * tmp = (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7(tmp, NULL);
	}
}
static void IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * tmp = (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7(tmp, NULL);
	}
}
static void NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * tmp = (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 27524LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 5196LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
	{
		EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * tmp = (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 *)cache->attributes[2];
		EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7(tmp, NULL);
	}
}
static void NullablePublicOnlyAttribute_t73D8C76AD0B97ABE84FBCB949817AB930665B3C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 * tmp = (EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 2LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void SR_t9B1F15CEEAB1CD24A1889ABB4CDB6DA7D327E9EF_CustomAttributesCacheGenerator_SR_Format_m98E7A215B14ABB55764D42C2B1B82AA655F42297____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ReferenceEqualityComparer_t724BFFD314267A57B52C0A3AFFF04465F58A30CF_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReferenceEqualityComparer_t724BFFD314267A57B52C0A3AFFF04465F58A30CF_CustomAttributesCacheGenerator_ReferenceEqualityComparer_get_Instance_m43B68CACB4FE50ABE65B13C224229C45CB7FDC9C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 27072LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B_CustomAttributesCacheGenerator_U3CMemberTypesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DynamicallyAccessedMemberTypes_t791D73C5D0A020B4FD7EA75B611765B8F25DE656_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 2432LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 2048LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813_CustomAttributesCacheGenerator_U3CReturnValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 2048LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA_CustomAttributesCacheGenerator_U3CReturnValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowOutOfMemoryException_BufferMaximumSizeExceeded_m6AACBBF084A0F2C5949C350FEC33B41F950E2A61(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_PropertyNameTooLarge_m77F825E6FA2ABACC4885601C5C87D9309FF8F0AD(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_ValueTooLarge_mB6D13E7E631571FE910659E29715926B40210CE2(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_ValueNotSupported_m28B4A5E1B7773753D54D7D3D00CA9EF64B8D9BD4(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_NeedLargerSpan_m42791059B9B5492C16E61273D3F33692EAEF9B1B(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_mA7A541529B4A42EF018C2E86CB4C896BD8900615(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_mAEE102BDC978858CF1E7B35197714BC9126D0B34(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonReaderException_mF1FBE8AEF1017A0B089C3B41D3C381DD4C1428BA(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_m388FA9CAE1E6661558861A241C547A9E0DF887BC(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_InvalidUTF8_m5D596F01AA0D55AF6DA151FD0B5C1511090E1B47(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_InvalidUTF16_mFDF55F6C19EC28ED0408D32C4ADC08ECAC22245B(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m95A110D95E052A207FF2F081DF1880159CAD85D0(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_mDF46BDC21FFF5BA6D77F4B3BD33C73B5948D6665(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_SerializationNotSupported_mF73201276822CF6CC001E95CBA48CD83D6109062(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_ConstructorMaxOf64Parameters_mCDB2E3AC4FFD00386CBAA2DEFDD8AE1CD5BA4F1D(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_DictionaryKeyTypeNotSupported_m27903431590452F80C7758007DFD55D8B7A7E611(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_DeserializeUnableToConvertValue_m96FE8F452BB614BAB85A7E20BA40E22690C93F7B(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidCastException_DeserializeUnableToAssignValue_m4C68F1D7ED3EDC35AFC765960D3A679E34C51630(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_DeserializeUnableToAssignNull_mCBADC1DEFC6B20B8679C4EE5FE83BC5098203B9A(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_SerializationConverterRead_m4A28E5B4CC04FD16BA7CAA84D1B021F92EE220D7(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_SerializationConverterWrite_mD0997F6FA916D1E612AB3C737A23612AD9297164(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_SerializerCycleDetected_mAA15526A580E83EC7EDA47D50598EBD0D0D8E438(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_m973F42F766807F7E7BBC203E354BA06CA8EA055B(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_CannotSerializeInvalidType_mD2FD606E0497CF4BA9029A955923E121356C04D3(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationConverterNotCompatible_mC298E0922D7F2B7B68B13B4D3E5743C766D8F84D(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid_mA2E9DC408E16194EFCECD1A1371C32DE747E1339(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible_m6065CAB5382ABA2091B10F92E69FF90F37C59BFC(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerOptionsImmutable_mBEFF1850BBA433ADD5B20540B3C7594BEB6ABE0F(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameConflict_mF30BA34FBED9BA4C3F4F4CA6E834080C5AFE1CB3(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameNull_m9023D53865B4D6A638CC630D5D77584FE074E8CC(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_NamingPolicyReturnNull_mB9EB953C12D35F4E93345C912DBF23DB636451D5(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull_m5F731D2B8C69BD2CA0359516616C34D5ECDB80D8(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters_m05E79160F6B9CD59D30D8906BB2987EED6954601(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ConstructorParameterIncompleteBinding_mB139EAC290271F39E1736F7F1E9E2FF9B14C297F(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam_m4872A1B0FF96357B0C6B638C4EBA9E1761880D2E(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid_m603A1316F663B20F69E80D9E3811B452E919570E(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid_mB9A9F19F8AF43DC125EA07D32794D98B98F19E20(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid_mFC629CF3501D8B07CADE0DB5D3A96AFC3A8D517B(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ConverterCanConvertNullableRedundant_mD154A895AB1495392DCCBA759457612D5A7C6E66(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored_m5EE618649BD8D2CBB732C6F0292DC3C4E0DFF91F(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m8C8F65479D5823F1F0A53F62D329626CB6590681(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m8C8F65479D5823F1F0A53F62D329626CB6590681____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A____reader1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_AddJsonExceptionInformation_m27AA11CA530AE35B24E4AD7E1BF42051046CDD75____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_AddJsonExceptionInformation_m27AA11CA530AE35B24E4AD7E1BF42051046CDD75____reader1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m60D724FB73CD33130D2CF820A08AE5332F7AFBE8(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m60D724FB73CD33130D2CF820A08AE5332F7AFBE8____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_AddJsonExceptionInformation_m19A08342AAA7D86A83B9A636BEA522C2F2232918____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateAttribute_m6431717BAB38FE1C8D3A06DF615F73301FA5125D(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_m5D557E2F44F1A96A25337111B7BC2F1F6D0F5AF7(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_mB015CCC694FA963580C7403419A0D32B7941789A(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid_mDECC955E535EBF5416567323A734BA671818A50A(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3____reader1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mFBBEF762422B3346268279581FA1372DE9B433FB(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mFBBEF762422B3346268279581FA1372DE9B433FB____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_DeserializeNoConstructor_m0316A2404C45BB2BBD6532E76FE9F384DB113010(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_CannotPopulateCollection_m18F801EC086A905E56F9380CFA8E011746E95DD1(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataValuesInvalidToken_mDC0FA9D497DFC575CCFD6422C843790C64656AEB(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataReferenceNotFound_m07F016A3E7895EC7753C09442854C4950A77F142(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataValueWasNotString_m9867E87D56EF64AABEE3844B4929587C5CB49A22(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataValueWasNotString_mC0D5A8277188661CECD17D5BAF5E7934D691DB34(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mCF9676414315C513337B5E8644F0A4927FD99F75(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mB36E6C2A52F6DBEDC8F30D9FCAF626F29A8B1399(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataIdIsNotFirstProperty_m845C5039FF51C4DF91792496C8A5A660AC75BC07(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataMissingIdBeforeValues_mE063C70456CE239D60E25F7381022F36523B764E(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_m338531C4877DDB86A6298075523EE839FD80D4E6(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_m338531C4877DDB86A6298075523EE839FD80D4E6____reader2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataDuplicateIdFound_m168ECF07F07194EAC525711DE4894FBF0BF44C59(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataInvalidReferenceToValueType_m6AACE011A6BBB6D2092908770A52F33F538FE0F3(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m5DDDF08403852837CA5A82CD77FA084374975BF8(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m5DDDF08403852837CA5A82CD77FA084374975BF8____reader2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataPreservedArrayValuesNotFound_mC760A54801B7D5833BB0BBB73CD9D331137C2DB0(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable_m2541C3387E3799EA00D221A1E9D96E5748B4C2D5(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType_m59D2BC0FFD79A685E8D72FA1ED73341DCB4828D2(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowUnexpectedMetadataException_m66CB72849A9036C64AC1B6C05F19E46C0277F47C(CustomAttributesCache* cache)
{
	{
		DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 * tmp = (DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140 *)cache->attributes[0];
		DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D(tmp, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_U3CIsDisposableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_get_IsDisposable_m32674A8E6A4F43BAAB12D25D7AB4B1F8202D6337(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_WriteTo_m4F0091C2294C11E823F07D253C25BA2DD720FDB1(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_UnescapeString_m2DF634F6A71C280B6D0B94CDFFA6B1889640FBA5____row0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_WritePropertyName_mD6770EEC4513C8606F5A55BFD29B59FE9B8B486F____row0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_WriteString_m97C83763877F4E016B011EBD62E8B7D975D2E6C1____row0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_Parse_m0C451D2F8DE35FD2D93E4F76D8DEB3FED0784504(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_ParseValue_mAFD1AF7093FB6B8974C2C4ECC111F8438E0DA731(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_TryParseValue_m376E476DA7C90522D73A2BCA568CC00ECD2EBBCC____document1(CustomAttributesCache* cache)
{
	{
		NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA * tmp = (NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA *)cache->attributes[0];
		NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13(tmp, true, NULL);
	}
}
static void DbRow_tAF1E82E152D2383E60EEAD13C1907E86A169560F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void MetadataDb_t5CA7F8F87B7B358F3B574C7E142142511EB67E26_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetadataDb_t5CA7F8F87B7B358F3B574C7E142142511EB67E26_CustomAttributesCacheGenerator_MetadataDb_get_Length_m358C82F2ACD118F1A31750A408DE4A80D9706AC1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void MetadataDb_t5CA7F8F87B7B358F3B574C7E142142511EB67E26_CustomAttributesCacheGenerator_MetadataDb_set_Length_mDEED679131C22B2B98BD28E71CABB539B01E79C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_U3CAllowTrailingCommasU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_JsonDocumentOptions_get_CommentHandling_m250724FEAA7D45E779F026A8E2AE1792E8CC0223(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_JsonDocumentOptions_get_MaxDepth_m910F496990C5F7315734B42E49D6B46D7D7FB146(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_JsonDocumentOptions_get_AllowTrailingCommas_mB695E7244BD9C08298D2A980CE3C8D6C69D0F2ED(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[2];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x7B\x44\x65\x62\x75\x67\x67\x65\x72\x44\x69\x73\x70\x6C\x61\x79\x2C\x6E\x71\x7D"), NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_GetProperty_mF3F895EADD2EBC27D13528083FBA7A8E754C02B9(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_TryGetProperty_m1F48BA59A2FF4DD5DEC6B04E2DFEF7785CAB89DE(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_GetString_mC81CE313F3CF6ECFCC6374545F7B598A244FA53E(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_GetRawText_m57611974A95EAA4466054192D51EAC5A1487249D(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_WriteTo_m1AC8B59D427C19E13F2134BA8964477C9B3E4F20(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_ToString_mA0336A268F12CE4735C0307F12F4717EA5A24B29(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49____TokenType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49____DebuggerDisplay_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ArrayEnumerator_t20A1E2484160186A78E0A58CF4F9E9B7075A71D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x7B\x43\x75\x72\x72\x65\x6E\x74\x2C\x6E\x71\x7D"), NULL);
	}
}
static void ArrayEnumerator_t20A1E2484160186A78E0A58CF4F9E9B7075A71D0_CustomAttributesCacheGenerator_ArrayEnumerator_t20A1E2484160186A78E0A58CF4F9E9B7075A71D0____System_Collections_IEnumerator_Current_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void ObjectEnumerator_t66BDC39C224ABC31BECA2237860C5FCB04EF7812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x7B\x43\x75\x72\x72\x65\x6E\x74\x2C\x6E\x71\x7D"), NULL);
	}
}
static void ObjectEnumerator_t66BDC39C224ABC31BECA2237860C5FCB04EF7812_CustomAttributesCacheGenerator_ObjectEnumerator_t66BDC39C224ABC31BECA2237860C5FCB04EF7812____System_Collections_IEnumerator_Current_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[1];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[2];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x7B\x44\x65\x62\x75\x67\x67\x65\x72\x44\x69\x73\x70\x6C\x61\x79\x2C\x6E\x71\x7D"), NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[3];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator_U3C_nameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator_JsonProperty_get_Value_mF766C4EFFB858EEAD2A009AF55BB8B51095AC96A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Encode_m48F8D41B2ADAECA21D3B959F62C2D08A73A1F317(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Encode_m48F8D41B2ADAECA21D3B959F62C2D08A73A1F317____encoder1(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Encode_m670B6211FE6F2601D2CA63B0065AE2C71DD11A49____encoder1(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Equals_mCE77AC6B7E663C8B7D8412B394E91F715A5BE682(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_ToString_mEC405B95F27BDBC3814C264035F3B0151621800F(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[1];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CAppendPathInformationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CLineNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CBytePositionInLineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException__ctor_m4A92F38705B569451DA8C63F2E1AD5375DF6E8CA(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_AppendPathInformation_mE4D721DFC493525073DDB220BA2A1499C1C08362(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_AppendPathInformation_m30D14D80A9F43FB975791CEEBA7B728E6A18EA6A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_GetObjectData_m82F05FF628ABF99E30D9C8581FB85D37EE9134B7(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_LineNumber_m7D21C9A5A7431696FB3B195690A1532E46DC4601(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_LineNumber_m60FC0BA8C1BA4D775C58DC3657DCEEAA3B9D6631(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_BytePositionInLine_m74C4E152BDFC2439E479ED098CEF728C74EE713B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_BytePositionInLine_m6CAD5D3CB0CC4F9638C91C71B8E0A44DFEBF9569(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_Path_m1B60B637D1DCB55E79E0F91381230144DC3DACA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_Path_m2ADEFDBC15D75EBD73C8DBCB04C21DA8717B571A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_Message_m7FCB5F5C5C8587DE96DD591EE60CF610AE367AD7(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99____Message_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_GetSpan_m9ED27DE6D38D4D2F3CFBAB1F52898CF2B2BA407F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_ReadWithVerify_mCC9DD465FF6CCB7DAF613B7D203956794D5D9939(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_TryAdd_mABD6B4AD92EA7F473B450B4D21C8A51634C2F5B5____key1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_TryAdd_mABD6B4AD92EA7F473B450B4D21C8A51634C2F5B5____value2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_U3CTryParseDateTimeOffsetU3Eg__ParseOffsetU7C21_0_m088E2588D6A3243BACA89CEF31560FE6818CC879(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_ToValueKind_mC5065501F4DC87FF00ED3C35317E53E604B4DC5D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_IndexOfQuoteOrAnyControlOrBackSlash_m1465C1D49863A0D9DF2D893FCEF23416D6736AA4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_TryGetUnescapedBase64Bytes_m6B842CBF6259E1588D3161286FA663515A9512A4____bytes2(CustomAttributesCache* cache)
{
	{
		NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA * tmp = (NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA *)cache->attributes[0];
		NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13(tmp, true, NULL);
	}
}
static void JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_TryDecodeBase64InPlace_mB9FB71368E0C656AD14ED4EC3AAF8C15A8557610____bytes1(CustomAttributesCache* cache)
{
	{
		NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA * tmp = (NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA *)cache->attributes[0];
		NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13(tmp, true, NULL);
	}
}
static void JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_TryDecodeBase64_mCB66A113B2B0E3D25218985C4F68DEE3BEF4D91B____bytes1(CustomAttributesCache* cache)
{
	{
		NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA * tmp = (NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA *)cache->attributes[0];
		NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13(tmp, true, NULL);
	}
}
static void JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_U3CAllowTrailingCommasU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_get_CommentHandling_mC784945C4A7D8BF00627E0D71CAF144E059CA45B(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_get_MaxDepth_mE9E196FD22578986617CBC0B6104934965E7DE18(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_get_AllowTrailingCommas_m6BE15A1BB0597C203011627DCF4E9CDDB42049BA(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_set_AllowTrailingCommas_mB8C99444AB5B8A9D50FDDD8E2CDC8F473B6D0641(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635 * tmp = (IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635 *)cache->attributes[0];
		IsByRefLikeAttribute__ctor_mBAF4018209D90000A5E262F2451AE1C4A82314D9(tmp, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x79\x70\x65\x73\x20\x77\x69\x74\x68\x20\x65\x6D\x62\x65\x64\x64\x65\x64\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x20\x61\x72\x65\x20\x6E\x6F\x74\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64\x20\x69\x6E\x20\x74\x68\x69\x73\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x79\x6F\x75\x72\x20\x63\x6F\x6D\x70\x69\x6C\x65\x72\x2E"), true, NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[2];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x7B\x44\x65\x62\x75\x67\x67\x65\x72\x44\x69\x73\x70\x6C\x61\x79\x2C\x6E\x71\x7D"), NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CValueSpanU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CTokenStartIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CHasValueSequenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CValueSequenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_ValueSpan_mBABD1CCB678681EF0BAB792BD92B1A667B5280EA(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_ValueSpan_mCB290F34DBE06C916AC61FCD97325363E6DF8840(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_TokenStartIndex_m065C1E55350901EE451793D91D45C6BFD6727825(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_TokenStartIndex_mD8DE6AC3843B60206928FF65843B1063725E11D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_HasValueSequence_m0E4BC52E674A56D3F8FD1EF9F56BCA86F7851658(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_HasValueSequence_mA04C744A5B061CF11B292F4FA30685D8FF959819(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_ValueSequence_m8BFD79D1CB26B6B8C081CC01F855EFD3A48E74F3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_ValueSequence_m14F871806DC91F89B691C08FDFDBA71DB941C6E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_RollBackState_mE1AB8FF05072C2D1C921F4D97E56122C15E905AD____state0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeNegativeSignMultiSegment_m135AE9A283C44CD51F7BEFF2A2E0FD9890FCED3C____rollBackState2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeZeroMultiSegment_m281FC6658603949FC90711BE6C81B8C3108B9E2D____rollBackState2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_mE037A2CF96C775C86599C5A61F9E7EEB2C411140____rollBackState2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeSignMultiSegment_m05A9CA258362283F551D2CC8A7B43DAAA1F99D7F____rollBackState2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetString_mF6ACA3547007BAAD951423AD47FC7C8FB1EE1C44(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetBytesFromBase64_m0B0E3C7FA55C6B1BF7B6513D80CB4159543F5F67(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetSByte_mF0C15DF1DFAAFED79A5FF6527BC1EC6DF8D6D541(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetUInt16_m91B334F666BA8952300156AC744D340C01A0FA68(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetUInt32_mCAAA27CB167078F0E49C896E0D38F71DA98A1DA1(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetUInt64_m04FC93AD8353898C2E5383CBF9616A0D03DB05AF(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35____value0(CustomAttributesCache* cache)
{
	{
		NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA * tmp = (NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA *)cache->attributes[0];
		NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13(tmp, true, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetSByte_mF7778E9C90F9D7C8CBE321D9C4B3859CE02B9106(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetUInt16_m018B28F35D595DDDBB3A104F69E2BAB463CDC274(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetUInt32_m3568E2AB280AF64148B26E5A570DE313B5F6A557(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetUInt64_mA1BFC2EDB1096EEA85D3F3992A6C33B599769A63(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65____DebuggerDisplay_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65____DebugTokenType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void PartialStateForRollback_t2E18041EA3BC1766B260CDD441330401D6F4D7B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x2E\x7B\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x7D\x2C\x20\x7B\x54\x79\x70\x65\x2E\x4E\x61\x6D\x65\x7D"), NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x2E\x7B\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x7D\x2C\x20\x7B\x54\x79\x70\x65\x2E\x4E\x61\x6D\x65\x7D"), NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CCreateObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CCreateObjectWithArgsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CAddMethodDelegateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CClassTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CDataExtensionPropertyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CElementTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CPropertyInfoForClassInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CParameterCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_CreateObject_m3924F94319E8CC8037E397EC71A758534AE7E3EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_CreateObject_mCB383ACB75DFF40E78CD099DECE8A89CC5C95C9A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_CreateObjectWithArgs_m5F1565F5FBA9FF4DA02786C6654771AC177B1E39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_CreateObjectWithArgs_m737C399B0900D4807B7E6A881979765184CA70BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_AddMethodDelegate_m488E73B03A89040319408B5DF87C9C4DA514335E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_AddMethodDelegate_m99DA3023113AB512DAB80E80B226BD003AF03135(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_ClassType_mB004262B1B41B552E9345A10FFF3F8E48F2D408B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_ClassType_m1FF3CC9DB07D8AB41E249DA6AF8AA0DBA83616B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_DataExtensionProperty_mEE9451B64DEAF5818915EA439213B5A8FEC028FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_DataExtensionProperty_m869AA0DB667AFCA30A5BA4DAD22FC963343E9DEB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_ElementType_m770A51860EC4421D6741DCFF06916FB33AF53858(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_ElementType_mC498B4A763F7C2A01DBA9D553145F8D0CA6C7874(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_Options_mA487DF18E8069C47D2FA573FB9DF584F488F93CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_Options_m2F599919B350219F457359B4E7FA7A1B1E83D8C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_Type_mF4833C6030319D73C7C0E8115D34603648574D70(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_Type_m35C5D4D035C6196861487CAE2B4E7D8516848672(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_PropertyInfoForClassInfo_m43DD25A15635FAD68648F55AD79A8B6FB15433FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_PropertyInfoForClassInfo_m50F366281A4E6F986341608C934DEDA64EF2BFD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_ParameterCount_m9A7A9DA3DC8B1796546F01CAE46D2F6AB5F71A84(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_ParameterCount_m703F8184C00284D40E133AD69FB81EC896B5020F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_IsPropertyRefEqual_mB02C7B4FC60D6274B5A70866495240515CC8DF34____propertyRef0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_IsParameterRefEqual_m88A98AAE34615FB5DEB1F8309A5FF81DE7D1C639____parameterRef0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_U3CInitializeConstructorParametersU3Eg__GetMemberTypeU7C46_0_mA004A63FFB79C1F8C5E65EC01C505FC2E75CDEBC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_ParameterLookupKey_get_Name_m048FB355797E2E00063B62A5FDB6A30CE1C543CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_ParameterLookupKey_get_Type_m66F89A37473AEEDE2674D17D6FD97C773D54B23A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_U3CDuplicateNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_U3CJsonPropertyInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_ParameterLookupValue_get_DuplicateName_m2846FDEB4E05B3167D754D5F2774A3F8FFDD3B50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_ParameterLookupValue_set_DuplicateName_mE89B81F211FE3342D05118E95ACF64341027BB43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_ParameterLookupValue_get_JsonPropertyInfo_m8570A2FA9EDD6BFA2A1C5DE2B8AF26287C402CFA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonNamingPolicy_tC50C562D30D5F9EDB5D4F5D68614850A38843BBC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonNamingPolicy_tC50C562D30D5F9EDB5D4F5D68614850A38843BBC_CustomAttributesCacheGenerator_U3CCamelCaseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonNamingPolicy_tC50C562D30D5F9EDB5D4F5D68614850A38843BBC_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CConverterBaseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CDefaultValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CIgnoreDefaultValuesOnReadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CNameAsUtf8BytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CNumberHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CRuntimePropertyTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CShouldDeserializeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_ConverterBase_m280CF92A015816989461A48B357E7C2D1BA6D398(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_ConverterBase_m63A442D88DCC1E79C3E1FA314ADE4351B0278E4D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_DefaultValue_m9C4947604651E2557BBF7107864DCAB0DE907877(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_DefaultValue_mD2FA38670AC6C3527521CDB54EAD7F359E8F8E1B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_IgnoreDefaultValuesOnRead_mFB2E81AD69B208C02EDA92059CC9F0706B162D4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_IgnoreDefaultValuesOnRead_m9A32A1D7B960DB0BCA25BF1D7F657C36E8AD43FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_Options_mD740985271C61D63EE42CB9B9C49DF14B84639B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_Options_m8D916748A8A1CFFAF7BBFECBE0B46461BD2A05B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_NameAsUtf8Bytes_mC74D28CA16D8E6CF8F4341D3A10001917FA4EEC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_NameAsUtf8Bytes_m9F52C8A73862C84E1AB8A136F85A8925778AF9C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_NumberHandling_mA793C420F52157BFAC40C89B747D48159D519FB4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_NumberHandling_m69F1AFEAA58436C0EB39B05FB8FBE29870288CF5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_Position_m4ECD7B7B8E7B34713BFC26010F66169AB271D601(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_Position_m50E95F79ED24C836FC139AF7C7D3190BC0B2DC76(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_RuntimePropertyType_m12B753E79AA5EE3DDDE208C90283F9D7D7A1B9F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_RuntimePropertyType_m936B468A974E2FC8D5A5771111A948E3FBBF1E0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_ShouldDeserialize_m0BFEBB5B82CFD53BA18436FDF40579DD363D8C4B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_ShouldDeserialize_m2B4028AA15136C622319698384D9F848B6575B6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_1_t51FCEAA906D6924F2738F588AF886649E190DBCD_CustomAttributesCacheGenerator_U3CTypedDefaultValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_1_t51FCEAA906D6924F2738F588AF886649E190DBCD_CustomAttributesCacheGenerator_JsonParameterInfo_1_get_TypedDefaultValue_m681FEAF53F1C086271B606C7118006F463CB978D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonParameterInfo_1_t51FCEAA906D6924F2738F588AF886649E190DBCD_CustomAttributesCacheGenerator_JsonParameterInfo_1_set_TypedDefaultValue_m2028FB1A3946FF4D24CE3D0FB52EE54A7347A325(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6D\x62\x65\x72\x49\x6E\x66\x6F\x3D\x7B\x4D\x65\x6D\x62\x65\x72\x49\x6E\x66\x6F\x7D"), NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CDeclaredPropertyTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CHasGetterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CHasSetterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIgnoreDefaultValuesOnReadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIgnoreDefaultValuesOnWriteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIsForClassInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CNameAsStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CParentClassTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CMemberInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CRuntimePropertyTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CShouldSerializeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CShouldDeserializeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIsIgnoredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CNumberHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CPropertyTypeCanBeNullU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_DeclaredPropertyType_mACBDE6A28FC500481702320EF6E636DCE7C73DAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_DeclaredPropertyType_m58E832BD84C9BB9961F9818EF8E1A9B541E552DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_HasGetter_mA8ABECDB9D68BD370F0ECF0B51A530AF6CBE49C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_HasGetter_m3E3B9FE50F71BF6965E4179F44C5118394F4EB76(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_HasSetter_mB7EBA2506BCCB52852390BA7C0EF680114BFCC8A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_HasSetter_mC66078A3C7DAAEF93BD18FEC9CCFCB684901BEBD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IgnoreDefaultValuesOnRead_m485BF3036EFE5652D3354AB52BFE4AD9C462EBD4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IgnoreDefaultValuesOnRead_m9ACA6E5CBB4477A57064464E509A4A031287A294(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IgnoreDefaultValuesOnWrite_mE576FFF14D1B98A34C69F485B99757933138AE42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IgnoreDefaultValuesOnWrite_m591097336FB6F6C0AE4BFF25BBED8FC9515381D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IsForClassInfo_m5E9EB12AD97755E0026D726A4DC13F9D3A570A9C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IsForClassInfo_mF91A27CA43772F77B01DF9DBE19276384CBD3A67(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_NameAsString_m907A831A0369E765CDED710035B36F0FA5AC8B9B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_NameAsString_mD58CEEB19C9F3D7C5AD6406FCBD1355D53C48D36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_Options_m13E3BF459F8675C65E6C608482C9C0789F75B97A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_Options_m53F38E18046D1268125293BDDF9ACF1B0F34B668(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_ParentClassType_mBFF8F3DC065713FD77F12788DFD0E353C0FA877F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_ParentClassType_m14715F89089DCC5D3B64C72199F01FE44409CD11(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_MemberInfo_mDB50F867A36435920C12ED71CA72580FF7EB3A8E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_MemberInfo_m6BBABD3EA7E95FACE7C093144DB3648FBD62E4B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_RuntimePropertyType_mE2C5C15069C2A00DE2C1AD067091F95D79149019(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_RuntimePropertyType_m5CFF3F102D982C87100EEFFF55192D91D75EB45B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_ShouldSerialize_m17CDCACBC8CBDE574091BDCDD33CEB8D017839D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_ShouldSerialize_m8BA04314A995978B0066992640E2296C146C3839(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_ShouldDeserialize_mA6E24C13D03410019DE42F107146B0361AB3D5F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_ShouldDeserialize_mD9BB3AA10AC77793B2D49D65460CAF4EC7908709(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IsIgnored_m21287FDA8B36179BDE92703E355B5CDCCD6B2C1D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IsIgnored_m81FFCABC248845376883684AC190028BADAC7A84(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_NumberHandling_m3F57EC55C9CC7CE9C1017CBF1B737759673F3947(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_NumberHandling_m7171FCC5706B63D324F911FA3D868549B17183B0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_PropertyTypeCanBeNull_m00E89F86EC69375E087FE13D14C58FF621132391(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_PropertyTypeCanBeNull_m1B4AEF7B3C4E167742433F15E6DBBA625C7FAFA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_U3CGetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_U3CSetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_U3CConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_get_Get_m97B30AC8E17860A25194510E56579A3772661EDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_set_Get_mE67BAD47F380088072E730BB8E499DA4A92AA8A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_get_Set_m8CB54AE95DEE35CE41A0A7A759D62769D7E7C7A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_set_Set_mEFF83D7A04998146CE9F9F9ACF10D2485F2BFE69(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_get_Converter_mF71EC785285275F2987FA1FDB4251C4E79884EA2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_set_Converter_m7366925B8226F17F31A03FAC040A1B9DCAAA9395(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Deserialize_m48873845B16152EC9392A85834DF263182111620(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Deserialize_m48873845B16152EC9392A85834DF263182111620____json0(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_WriteCore_m8E6E18D5998A61A65F9FB6892C30C2EFC706F5BD____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_WriteCore_mDE73B6E3C4C75D4F374316DAEF0D726C54911E21____value2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Serialize_m7513647C440FBC2D65E6F2A32326A90F60A47A6C____options1(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Serialize_m3BEE629EB4FB56379EC8BC3299D6CF05420C4208____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_U3CConvertersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_U3C_lastClassU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_U3CEffectiveMaxDepthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_get_Converters_m387F196CD3A362700DF74ECFEC1DE4873D84180E(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_GetConverter_m3F90FE1FF74CAFEDE6BA42D78D9AFB3CF317D6E0(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_get__lastClass_m625BC41A0607D88F18B0796584D3AA94640639EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_set__lastClass_mC4E971444165788CBAF11FE1B3BA0DFACCCF0CC8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_get_EffectiveMaxDepth_m5B3CE03573116B7D5920A24F386226C7497CA6B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_U3CGetDefaultSimpleConvertersU3Eg__AddU7C3_0_mE1E7914A06F0CF6977D174FD4DDDCD2BA2D7CC86(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_U3CGetDictionaryKeyConverterU3Eg__GetEnumConverterU7C4_0_m422925E719E621089038D602E3BBD474F7C96DB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_U3CGetDictionaryKeyConvertersU3Eg__AddU7C6_0_mDA972273A76E4AF978A8EF39C01033AAA1D83A96(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904____Converters_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904____IgnoreNullValues_PropertyInfo(CustomAttributesCache* cache)
{
	{
		EditorBrowsableAttribute_tE201891FE727EB3FB75B488A2BF6D4DF3CB80614 * tmp = (EditorBrowsableAttribute_tE201891FE727EB3FB75B488A2BF6D4DF3CB80614 *)cache->attributes[0];
		EditorBrowsableAttribute__ctor_mC77290C5157BDA154F1D03BD1551223B07A851D4(tmp, 1LL, NULL);
	}
}
static void JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904____MemberAccessorStrategy_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t6D5AA24C4E207C767D92A44E3D5445CDE5E147A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t593C74B93352CD492DB9EB3E51E6C6101A0062FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t98714B109933020910C32D029B08AD882351011B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParameterRef_t63724617E2606E66C97FE015F6219FAB92A19A28_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void PropertyRef_tEEABE244AAB73F6A49CCC651F546B85017B8BFBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x74\x68\x3A\x7B\x4A\x73\x6F\x6E\x50\x61\x74\x68\x28\x29\x7D\x20\x43\x75\x72\x72\x65\x6E\x74\x3A\x20\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x2E\x7B\x43\x75\x72\x72\x65\x6E\x74\x2E\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x7D\x2C\x20\x7B\x43\x75\x72\x72\x65\x6E\x74\x2E\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x54\x79\x70\x65\x2E\x4E\x61\x6D\x65\x7D"), NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m8ED401FF34693D816348D4CE72C93E7394006130(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m8ED401FF34693D816348D4CE72C93E7394006130____frame1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__GetCountU7C19_1_m75E574E6B1DBFD7C4822567526DC011D8AD9FC40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__AppendPropertyNameU7C19_2_mA80E427F474EAE78F6CC63C1028369018338D28E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m104FC083CCE5BB72491DD4BA0B26BCCE2BAFB176(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m104FC083CCE5BB72491DD4BA0B26BCCE2BAFB176____frame0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ReadStackFrame_t35BBF02D5EACDF26EE823E40A647ED5DBE8B1756_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x2E\x7B\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x7D\x2C\x20\x7B\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x54\x79\x70\x65\x2E\x4E\x61\x6D\x65\x7D"), NULL);
	}
}
static void WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x74\x68\x3A\x7B\x50\x72\x6F\x70\x65\x72\x74\x79\x50\x61\x74\x68\x28\x29\x7D\x20\x43\x75\x72\x72\x65\x6E\x74\x3A\x20\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x2E\x7B\x43\x75\x72\x72\x65\x6E\x74\x2E\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x7D\x2C\x20\x7B\x43\x75\x72\x72\x65\x6E\x74\x2E\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x54\x79\x70\x65\x2E\x4E\x61\x6D\x65\x7D"), NULL);
	}
}
static void WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator_WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_mFA8217E89FE0DFE851708748202127A07F74AEBA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator_WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_mFA8217E89FE0DFE851708748202127A07F74AEBA____frame1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator_WriteStack_U3CPropertyPathU3Eg__AppendPropertyNameU7C13_1_m5A499E425FD4D40245FC2BDDF6CB558870F6A287(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WriteStackFrame_t417CDBE27C42D3F13F553AF5E88DFE6EDA7C63C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x2E\x7B\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x43\x6C\x61\x73\x73\x54\x79\x70\x65\x7D\x2C\x20\x7B\x4A\x73\x6F\x6E\x43\x6C\x61\x73\x73\x49\x6E\x66\x6F\x2E\x54\x79\x70\x65\x2E\x4E\x61\x6D\x65\x7D"), NULL);
	}
}
static void TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator_TypeExtensions_IsNullableValueType_m4898086A046C3E4BAE28D49F62382EABE2D51079(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator_TypeExtensions_IsNullableType_m12352B86A8DCA9AB2EB22C019B3A19C15144CFFF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator_TypeExtensions_IsAssignableFromInternal_m87241C5103114B0C94797AC15693EF1AC0C62803(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[1];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator_U3CEncoderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator_JsonWriterOptions_get_Encoder_m232389EA65BBFEC33F6133822B6014A12A3DD4FE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator_JsonWriterOptions_set_Encoder_mC29EDD6B2528E7E44A0E76471CC62E46FA114F00(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x7B\x44\x65\x62\x75\x67\x67\x65\x72\x44\x69\x73\x70\x6C\x61\x79\x2C\x6E\x71\x7D"), NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_U3CBytesPendingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_U3CBytesCommittedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_get_BytesPending_m3972ECDA5A4E7DBC6A17F7AA0610CB20AB58EF2C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_set_BytesPending_m21E09C8027465D1AD0C46E31DD91C8C83524DB10(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_get_BytesCommitted_m4647460AF1A97E6805CF12393C978FE240CCB076(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_set_BytesCommitted_m05CFA019A22B21A6E59B688CCD22B1C76C42E9B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter__ctor_m878AA70AA00DE7300C1749D15BF67D09B30B335C(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WritePropertyName_mC9ED158A09A22E81D95630C164199CE7DF9A849E(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteString_mC4C4697F80008CC325DA268D181126F27A29F2C0(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteStringValue_m1271B865848F4BF39035ACB38CD54B914A3BF385(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteNumberValue_mF8CCB5E8AF7CB32821E48257DAEB9488540C95B5(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteNumberValue_m14B1AC5C45AD6F1FB2A9F42FF28CEA2CF6D8CBCD(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C____DebuggerDisplay_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 1, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void JsonConstructorAttribute_tEE4F726B023906AEA9CDE3CB7FD8BD72E8F558E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[1];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 412LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator_U3CConverterTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator_JsonConverterAttribute_get_ConverterType_mBD3D8984B125937D81BEE770510CBD59DE992D6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[1];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 2, NULL);
	}
}
static void JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator_JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3____ConverterType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
	{
		DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B * tmp = (DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B *)cache->attributes[1];
		DynamicallyAccessedMembersAttribute__ctor_mDBF0F1C7DE04C8B522241B8C64AE1EF300581AB2(tmp, 1LL, NULL);
	}
}
static void JsonExtensionDataAttribute_t9CB1515ADBEA9C5B127BC4821287B0619D435667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonIgnoreAttribute_tAFD2E63A69829808553EA5CC6DAB6C19B6FD2C8A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonIgnoreAttribute_tAFD2E63A69829808553EA5CC6DAB6C19B6FD2C8A_CustomAttributesCacheGenerator_U3CConditionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonIgnoreAttribute_tAFD2E63A69829808553EA5CC6DAB6C19B6FD2C8A_CustomAttributesCacheGenerator_JsonIgnoreAttribute_get_Condition_m5218388DF4B31B960A88F80CDB92BAD3A1575391(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonIncludeAttribute_t42FA83469AFC2D07F39E164E43AA09B20EB83E45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonNumberHandlingAttribute_t0CBA24EDCE63CCEC4F7DDC42A56A6155DAA54816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 396LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonNumberHandlingAttribute_t0CBA24EDCE63CCEC4F7DDC42A56A6155DAA54816_CustomAttributesCacheGenerator_U3CHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonNumberHandlingAttribute_t0CBA24EDCE63CCEC4F7DDC42A56A6155DAA54816_CustomAttributesCacheGenerator_JsonNumberHandlingAttribute_get_Handling_m47EA30AE14CB014E642C09FC271D744207304236(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyNameAttribute_tA9BAC1CBD1A30B982B34B6FBDD9A74016C54A2C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[2];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
}
static void JsonPropertyNameAttribute_tA9BAC1CBD1A30B982B34B6FBDD9A74016C54A2C0_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyNameAttribute_tA9BAC1CBD1A30B982B34B6FBDD9A74016C54A2C0_CustomAttributesCacheGenerator_JsonPropertyNameAttribute_get_Name_m43379618C5E4C36982CD87DCC7DCD46C9AF7636A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConverterList_t8EE54566818565F34182EC18568C4864F26B2C0A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetCompatibleGenericBaseClass_mFE8785F58EDD007ECCB3D76077B0565BEE8DAA2B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetCompatibleGenericInterface_mB640DD8282CAC982B0360A4B1F5DB36603201157(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_IsImmutableDictionaryType_m35307B7347E0B7A698DA5E80E439A484D063DA42(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_IsImmutableEnumerableType_m7718A49E6C83EB1AE4F16348E3FC3CDBB61A1EF9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetImmutableEnumerableCreateRangeMethod_m750E671FCD270FA0B1B2E58C2AFDF41843605706(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetImmutableDictionaryCreateRangeMethod_mCAF50FB9DCCB97B1BEE3EAE5E50A55427D6EC02F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_IsNonGenericStackOrQueue_m4E32775D53BA32FD8CBA158643133A7F924BCDE9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CCanUseDirectReadOrWriteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CCanBePolymorphicU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CIsValueTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CIsInternalConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CConstructorIsParameterizedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CConstructorInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_CanUseDirectReadOrWrite_mE2CF0ECA1CCB2379ADDB5CA9927657FAE9420C97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_CanUseDirectReadOrWrite_m13DC84941788179AF7B397950B55551825EE1A46(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_CanBePolymorphic_mBA6DE039DABAEDEDDBBA10D5EF2D6535744EE450(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_CanBePolymorphic_m8CD8BA9054E3885A732BEFDF45DEC79A80145A7F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_IsValueType_mB362FB9B424A6911D4F4F54CE6E0FCB142D4895E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_IsValueType_mA4651543A67E50CC092948CEA8F29BE5B23DC911(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_IsInternalConverter_m0FC7605392703BA872B4DD89C327766EFF2C79A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_IsInternalConverter_mF87A3D79885CFC9C8D8BD08BAF230EF7DEE9AF40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_ConstructorIsParameterized_m120E1195BC89B5743A895922978D983F3A4E92AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_ConstructorInfo_mE3B569C452D11FED23B56BF8DFF557811B830959(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_ConstructorInfo_mA1D9AA73D0C0C45A774EF06EA013731BC58B698C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3____ElementType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3____ConstructorInfo_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonConverterFactory_tA7C5BBF890986F0A26A1259C87976F681069EE73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonConverterFactory_tA7C5BBF890986F0A26A1259C87976F681069EE73_CustomAttributesCacheGenerator_JsonConverterFactory_tA7C5BBF890986F0A26A1259C87976F681069EE73____ElementType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_U3CHandleNullOnReadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_U3CHandleNullOnWriteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_U3CCanBeNullU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_WriteCore_mF5723030398F3B3C33E33B080C0645FABF89A92E____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_get_HandleNullOnRead_m8DF85C0D9C9EDF126337A5665F1A90223354B69F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_set_HandleNullOnRead_m4F3CD209183C39B31467B086C77988484432A5F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_get_HandleNullOnWrite_m2150D830E0E7CE43B516932BF3A92D71DC43E110(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_set_HandleNullOnWrite_m336A1EE8A52E0A09D42C5A1DEB1C4275370F9F1E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_get_CanBeNull_m5CB7B8E5BC1D8B71C4A158E989643EA26CFE8C5E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_TryWrite_mC4CA66FBF06D08970AC91E9C64FF25B4BDB46497____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_WriteWithQuotes_m52D04EBF6D2C9745CE9DB5A89ECE2944515F2902____value1(CustomAttributesCache* cache)
{
	{
		DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D * tmp = (DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D *)cache->attributes[0];
		DisallowNullAttribute__ctor_m828EAECBBC572B96840FDD59AB52545D6B5A604B(tmp, NULL);
	}
}
static void JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34____ElementType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[0];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 2, NULL);
	}
}
static void JsonNumberHandling_tC60ADBC09774A886DBBAE55E7C4F5E6908477EFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void ReferenceHandler_t8489FE77ED8ABB51919798A3F1AEEBB59D7FCA6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void ReferenceHandler_t8489FE77ED8ABB51919798A3F1AEEBB59D7FCA6F_CustomAttributesCacheGenerator_U3CPreserveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReferenceResolver_t087C19EF63D0709C29E27E16D0BF0DA510BEBE87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA * tmp = (NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA *)cache->attributes[0];
		NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84(tmp, 1, NULL);
	}
	{
		NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 * tmp = (NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417 *)cache->attributes[1];
		NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19(tmp, 0, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t22913487C669704BBF5B5C7D3EF3A55C52F8F60F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_1_t78352DF79D99A95F591FA5A31A5440C1CA004689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_5_t711C123B007209ACEBF204A28D8B584B24964B50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_1_tDA50CBC9B7F7AF40C54E9CF315910343F63F0ED7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_1_t27CDB3D6E327EA969E0B214A97F75207BBDD5EEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_1_tE3167BCCA75784B3CE8C37E098DCA4B27909EE61_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_1_t51729BD4D4897C8D52D1CC75AE814620183247BA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_1_t3F3A1DE0C71DF3E00FEB7F6AEF6661C978B93FFF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ArrayConverter_2_t738A531D7A3C6E0AFCB25E30E956ADAB7D5FD46C_CustomAttributesCacheGenerator_ArrayConverter_2_Add_mA8C55F4C35457E96A2EA232544F182EA48E1CC57____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ConcurrentQueueOfTConverter_2_tAFDBFCB4D455EC207336F12779941C463F7A14D6_CustomAttributesCacheGenerator_ConcurrentQueueOfTConverter_2_Add_m2E02E7372C151304E32E629E6F91588829617D16____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ConcurrentStackOfTConverter_2_tE0CD646473117484AB404C8133BF468FF87C004D_CustomAttributesCacheGenerator_ConcurrentStackOfTConverter_2_Add_m2003F3DC3E4F1F3EA31AE1294A13D5DFDC1EEFC5____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void DictionaryDefaultConverter_3_tA62DAE9BC2F192BA50468530511AFF7941A43014_CustomAttributesCacheGenerator_DictionaryDefaultConverter_3_Add_m15D1A9A9EF84F82BD9D106C11555B11B4AEC34EA____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void DictionaryDefaultConverter_3_tA62DAE9BC2F192BA50468530511AFF7941A43014_CustomAttributesCacheGenerator_DictionaryDefaultConverter_3_OnTryRead_mC6BF3E6BB0611A624A55B33D907514211522F033____value4(CustomAttributesCache* cache)
{
	{
		MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 * tmp = (MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 *)cache->attributes[0];
		MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66(tmp, false, NULL);
	}
}
static void DictionaryDefaultConverter_3_tA62DAE9BC2F192BA50468530511AFF7941A43014_CustomAttributesCacheGenerator_DictionaryDefaultConverter_3_U3COnTryReadU3Eg__ReadDictionaryKeyU7C12_0_m3D849C4A2202DAB2E7B46EC2BD55BC829549AEA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_tFF88964461080A164C511F6FD720CE40FE59E02F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DictionaryOfTKeyTValueConverter_3_tA274F0E2A3EB2660B331194B8856AE3ECE544200_CustomAttributesCacheGenerator_DictionaryOfTKeyTValueConverter_3_Add_m74764BB7563516CF6744324708F99152A75B23A3____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ICollectionOfTConverter_2_t94EE78D560E29B7B1ABFDB4C7FC2EBE0FFD1118D_CustomAttributesCacheGenerator_ICollectionOfTConverter_2_Add_m7093DFB5C5AF29EA562B55660682E21A418C5FA0____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IDictionaryConverter_1_t2258D0512F1BF4E366D60612FC562EF47650C81D_CustomAttributesCacheGenerator_IDictionaryConverter_1_Add_m16C3A757A17DE63302C60E66FD34615194E37391____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IDictionaryOfTKeyTValueConverter_3_t952E923CFE2BD55DFCBF1B4132C7DF1BD68D8E03_CustomAttributesCacheGenerator_IDictionaryOfTKeyTValueConverter_3_Add_m67266029526CFFC925A3008CC9A74C80848C3EF3____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IEnumerableConverter_1_t4EE4E93D2103E1A39CC092C486E03763A64E00A9_CustomAttributesCacheGenerator_IEnumerableConverter_1_Add_m3121D8B90D9933B293A1F300BDA46F004A4AD639____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IEnumerableDefaultConverter_2_t5F900274EEA245B8BE1771664576677B2D55F20F_CustomAttributesCacheGenerator_IEnumerableDefaultConverter_2_Add_mFD08199F7BAAE66896A62ECEE16A3602EEC31B95____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IEnumerableDefaultConverter_2_t5F900274EEA245B8BE1771664576677B2D55F20F_CustomAttributesCacheGenerator_IEnumerableDefaultConverter_2_OnTryRead_m7DC07F0B6CEEC04127C930E373401C9F3D61A18C____value4(CustomAttributesCache* cache)
{
	{
		MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 * tmp = (MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 *)cache->attributes[0];
		MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66(tmp, false, NULL);
	}
}
static void IEnumerableOfTConverter_2_tB6887BCD6F0D5899AC78295C829672681ACB77F2_CustomAttributesCacheGenerator_IEnumerableOfTConverter_2_Add_m774BBB9687370A2B6BB9E1E32142F383291E961D____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IEnumerableWithAddMethodConverter_1_t989F922F36BE095F9AA186B58026492ADA7EC03C_CustomAttributesCacheGenerator_IEnumerableWithAddMethodConverter_1_Add_m126906C51B8B823DD32BEC6D8D75D0CBE1DAF5CC____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IListConverter_1_t45F01AC4B0F94D192395D7CE1466C2D8C28EC9A2_CustomAttributesCacheGenerator_IListConverter_1_Add_mA7193ACCB58EA1636E58E8BC3AAEFECE4D7D393F____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IListOfTConverter_2_tBEB208D96A82B8BAFECB962ED44C38A3A9003457_CustomAttributesCacheGenerator_IListOfTConverter_2_Add_m4ED07A81323514D01D3BEAB40FD9F89C4FE5F294____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ImmutableDictionaryOfTKeyTValueConverter_3_tFE1D0F780D64C770E0144410CE24C0DD3885D6E5_CustomAttributesCacheGenerator_ImmutableDictionaryOfTKeyTValueConverter_3_Add_m67B68023309D5EAF4F22F5F30DA24AD1E5DB4834____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ImmutableEnumerableOfTConverter_2_tA3247BA20882FC44C0ED22A6E94632316ED73AD3_CustomAttributesCacheGenerator_ImmutableEnumerableOfTConverter_2_Add_m7F41C9FB5BB92FFBBA6D9E68A89BE88AA6117289____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void IReadOnlyDictionaryOfTKeyTValueConverter_3_t2BFCFEC76862FEBEE06CF57739F149D63410C0AE_CustomAttributesCacheGenerator_IReadOnlyDictionaryOfTKeyTValueConverter_3_Add_m55F405716C54EEBEE5DB48D5CB9114258AB01182____value1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ISetOfTConverter_2_t3640D79EE209D8EB3467BD71F7509529418F817F_CustomAttributesCacheGenerator_ISetOfTConverter_2_Add_mD33D35BE2CF00C158F11405429ED8DA494BBC6D5____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ListOfTConverter_2_tE8A491A335CA5CA8B704CEC2B1D35A8A1C1D8D3F_CustomAttributesCacheGenerator_ListOfTConverter_2_Add_m1E0CA0DEADA14C50979B46DAB4641317C6DAF0E2____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void QueueOfTConverter_2_tF2816F12578BAD6961900AB08D11B44C0E98CBB0_CustomAttributesCacheGenerator_QueueOfTConverter_2_Add_mC0683A2C6C74DFA12FDA5B3B99F0C9CDAD8E44BE____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void StackOfTConverter_2_tA56FD2B04CBBA4749F4471DB60151561228BC7F7_CustomAttributesCacheGenerator_StackOfTConverter_2_Add_mAE48E54DC4B7F78FFF3BA918398D7B72889AD87C____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F * tmp = (IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7(tmp, NULL);
	}
}
static void ObjectDefaultConverter_1_t28F9BFA559442122CC9C0434B5D7EE6F99A8D11D_CustomAttributesCacheGenerator_ObjectDefaultConverter_1_OnTryRead_m4AF3B46BABEBF42B4998811EE1B180AE238D8350____value4(CustomAttributesCache* cache)
{
	{
		MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 * tmp = (MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 *)cache->attributes[0];
		MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66(tmp, false, NULL);
	}
}
static void ObjectWithParameterizedConstructorConverter_1_tEBAFA9FEF841EFD0ADE2F321935A745DBF0867DD_CustomAttributesCacheGenerator_ObjectWithParameterizedConstructorConverter_1_OnTryRead_m947A07DE4DD15BA037A5136737171E22BA92EC08____value4(CustomAttributesCache* cache)
{
	{
		MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 * tmp = (MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813 *)cache->attributes[0];
		MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66(tmp, false, NULL);
	}
}
static void EnumConverterOptions_tD44D098FC756833359A906ED8BE832FD43563C0A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t9560635BCB76F96CB2F672D0A52A2D6663E1E3B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_System_Text_Json_AttributeGenerators[];
const CustomAttributesCacheGenerator g_System_Text_Json_AttributeGenerators[502] = 
{
	EmbeddedAttribute_tED93CAFDB384C4CBC5ABC45CBC1FAAE08490C5F6_CustomAttributesCacheGenerator,
	IsReadOnlyAttribute_tCFD464CF521FA0C4B9968DCEFD86D4356B79852F_CustomAttributesCacheGenerator,
	IsByRefLikeAttribute_t1F19BC1C63ED038A942F7DF9A3DB2C73139D8635_CustomAttributesCacheGenerator,
	NullableAttribute_tB9FE29681056EC05AA8FDBE631A6FA9CBD395417_CustomAttributesCacheGenerator,
	NullableContextAttribute_tE8DC00A574F65F272B40B20A828BF334CD3A4EBA_CustomAttributesCacheGenerator,
	NullablePublicOnlyAttribute_t73D8C76AD0B97ABE84FBCB949817AB930665B3C8_CustomAttributesCacheGenerator,
	DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B_CustomAttributesCacheGenerator,
	DynamicallyAccessedMemberTypes_t791D73C5D0A020B4FD7EA75B611765B8F25DE656_CustomAttributesCacheGenerator,
	DisallowNullAttribute_t149E819A0D025B94E70ADBF28E3F228778086E1D_CustomAttributesCacheGenerator,
	MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813_CustomAttributesCacheGenerator,
	NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA_CustomAttributesCacheGenerator,
	DoesNotReturnAttribute_t775E73316FFBA0453D60293B9C8BD925B7C0F140_CustomAttributesCacheGenerator,
	DbRow_tAF1E82E152D2383E60EEAD13C1907E86A169560F_CustomAttributesCacheGenerator,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator,
	ArrayEnumerator_t20A1E2484160186A78E0A58CF4F9E9B7075A71D0_CustomAttributesCacheGenerator,
	ObjectEnumerator_t66BDC39C224ABC31BECA2237860C5FCB04EF7812_CustomAttributesCacheGenerator,
	JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator,
	JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator,
	JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator,
	JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator,
	PartialStateForRollback_t2E18041EA3BC1766B260CDD441330401D6F4D7B1_CustomAttributesCacheGenerator,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator,
	JsonNamingPolicy_tC50C562D30D5F9EDB5D4F5D68614850A38843BBC_CustomAttributesCacheGenerator,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t6D5AA24C4E207C767D92A44E3D5445CDE5E147A7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t593C74B93352CD492DB9EB3E51E6C6101A0062FD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t98714B109933020910C32D029B08AD882351011B_CustomAttributesCacheGenerator,
	ParameterRef_t63724617E2606E66C97FE015F6219FAB92A19A28_CustomAttributesCacheGenerator,
	PropertyRef_tEEABE244AAB73F6A49CCC651F546B85017B8BFBD_CustomAttributesCacheGenerator,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator,
	ReadStackFrame_t35BBF02D5EACDF26EE823E40A647ED5DBE8B1756_CustomAttributesCacheGenerator,
	WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator,
	WriteStackFrame_t417CDBE27C42D3F13F553AF5E88DFE6EDA7C63C9_CustomAttributesCacheGenerator,
	TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator,
	JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator,
	JsonConstructorAttribute_tEE4F726B023906AEA9CDE3CB7FD8BD72E8F558E5_CustomAttributesCacheGenerator,
	JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator,
	JsonExtensionDataAttribute_t9CB1515ADBEA9C5B127BC4821287B0619D435667_CustomAttributesCacheGenerator,
	JsonIgnoreAttribute_tAFD2E63A69829808553EA5CC6DAB6C19B6FD2C8A_CustomAttributesCacheGenerator,
	JsonIncludeAttribute_t42FA83469AFC2D07F39E164E43AA09B20EB83E45_CustomAttributesCacheGenerator,
	JsonNumberHandlingAttribute_t0CBA24EDCE63CCEC4F7DDC42A56A6155DAA54816_CustomAttributesCacheGenerator,
	JsonPropertyNameAttribute_tA9BAC1CBD1A30B982B34B6FBDD9A74016C54A2C0_CustomAttributesCacheGenerator,
	ConverterList_t8EE54566818565F34182EC18568C4864F26B2C0A_CustomAttributesCacheGenerator,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator,
	JsonConverterFactory_tA7C5BBF890986F0A26A1259C87976F681069EE73_CustomAttributesCacheGenerator,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator,
	JsonNumberHandling_tC60ADBC09774A886DBBAE55E7C4F5E6908477EFA_CustomAttributesCacheGenerator,
	ReferenceHandler_t8489FE77ED8ABB51919798A3F1AEEBB59D7FCA6F_CustomAttributesCacheGenerator,
	ReferenceResolver_t087C19EF63D0709C29E27E16D0BF0DA510BEBE87_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t22913487C669704BBF5B5C7D3EF3A55C52F8F60F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_1_t78352DF79D99A95F591FA5A31A5440C1CA004689_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_5_t711C123B007209ACEBF204A28D8B584B24964B50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_1_tDA50CBC9B7F7AF40C54E9CF315910343F63F0ED7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_1_t27CDB3D6E327EA969E0B214A97F75207BBDD5EEF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_1_tE3167BCCA75784B3CE8C37E098DCA4B27909EE61_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_1_t51729BD4D4897C8D52D1CC75AE814620183247BA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_1_t3F3A1DE0C71DF3E00FEB7F6AEF6661C978B93FFF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_tFF88964461080A164C511F6FD720CE40FE59E02F_CustomAttributesCacheGenerator,
	EnumConverterOptions_tD44D098FC756833359A906ED8BE832FD43563C0A_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t9560635BCB76F96CB2F672D0A52A2D6663E1E3B1_CustomAttributesCacheGenerator,
	ReferenceEqualityComparer_t724BFFD314267A57B52C0A3AFFF04465F58A30CF_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	DynamicallyAccessedMembersAttribute_t2B62F06C453878913C22154B5531EC48F24B682B_CustomAttributesCacheGenerator_U3CMemberTypesU3Ek__BackingField,
	MaybeNullWhenAttribute_t0C8A4E6635C073802063427A92CF4E0DD78C0813_CustomAttributesCacheGenerator_U3CReturnValueU3Ek__BackingField,
	NotNullWhenAttribute_t116D2704148A5687760FC8FAEEEE9969C1CB72EA_CustomAttributesCacheGenerator_U3CReturnValueU3Ek__BackingField,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_U3CIsDisposableU3Ek__BackingField,
	MetadataDb_t5CA7F8F87B7B358F3B574C7E142142511EB67E26_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField,
	JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_U3CAllowTrailingCommasU3Ek__BackingField,
	JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator_U3C_nameU3Ek__BackingField,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CAppendPathInformationU3Ek__BackingField,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CLineNumberU3Ek__BackingField,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CBytePositionInLineU3Ek__BackingField,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField,
	JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_U3CAllowTrailingCommasU3Ek__BackingField,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CValueSpanU3Ek__BackingField,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CTokenStartIndexU3Ek__BackingField,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CHasValueSequenceU3Ek__BackingField,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_U3CValueSequenceU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CCreateObjectU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CCreateObjectWithArgsU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CAddMethodDelegateU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CClassTypeU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CDataExtensionPropertyU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CElementTypeU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CPropertyInfoForClassInfoU3Ek__BackingField,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_U3CParameterCountU3Ek__BackingField,
	ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_U3CDuplicateNameU3Ek__BackingField,
	ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_U3CJsonPropertyInfoU3Ek__BackingField,
	JsonNamingPolicy_tC50C562D30D5F9EDB5D4F5D68614850A38843BBC_CustomAttributesCacheGenerator_U3CCamelCaseU3Ek__BackingField,
	JsonNamingPolicy_tC50C562D30D5F9EDB5D4F5D68614850A38843BBC_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CConverterBaseU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CDefaultValueU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CIgnoreDefaultValuesOnReadU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CNameAsUtf8BytesU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CNumberHandlingU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CRuntimePropertyTypeU3Ek__BackingField,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_U3CShouldDeserializeU3Ek__BackingField,
	JsonParameterInfo_1_t51FCEAA906D6924F2738F588AF886649E190DBCD_CustomAttributesCacheGenerator_U3CTypedDefaultValueU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CDeclaredPropertyTypeU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CHasGetterU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CHasSetterU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIgnoreDefaultValuesOnReadU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIgnoreDefaultValuesOnWriteU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIsForClassInfoU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CNameAsStringU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CParentClassTypeU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CMemberInfoU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CRuntimePropertyTypeU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CShouldSerializeU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CShouldDeserializeU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CIsIgnoredU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CNumberHandlingU3Ek__BackingField,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_U3CPropertyTypeCanBeNullU3Ek__BackingField,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_U3CGetU3Ek__BackingField,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_U3CSetU3Ek__BackingField,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_U3CConverterU3Ek__BackingField,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_U3CConvertersU3Ek__BackingField,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_U3C_lastClassU3Ek__BackingField,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_U3CEffectiveMaxDepthU3Ek__BackingField,
	JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator_U3CEncoderU3Ek__BackingField,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_U3CBytesPendingU3Ek__BackingField,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_U3CBytesCommittedU3Ek__BackingField,
	JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator_U3CConverterTypeU3Ek__BackingField,
	JsonIgnoreAttribute_tAFD2E63A69829808553EA5CC6DAB6C19B6FD2C8A_CustomAttributesCacheGenerator_U3CConditionU3Ek__BackingField,
	JsonNumberHandlingAttribute_t0CBA24EDCE63CCEC4F7DDC42A56A6155DAA54816_CustomAttributesCacheGenerator_U3CHandlingU3Ek__BackingField,
	JsonPropertyNameAttribute_tA9BAC1CBD1A30B982B34B6FBDD9A74016C54A2C0_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CCanUseDirectReadOrWriteU3Ek__BackingField,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CCanBePolymorphicU3Ek__BackingField,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CIsValueTypeU3Ek__BackingField,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CIsInternalConverterU3Ek__BackingField,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CConstructorIsParameterizedU3Ek__BackingField,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_U3CConstructorInfoU3Ek__BackingField,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_U3CHandleNullOnReadU3Ek__BackingField,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_U3CHandleNullOnWriteU3Ek__BackingField,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_U3CCanBeNullU3Ek__BackingField,
	ReferenceHandler_t8489FE77ED8ABB51919798A3F1AEEBB59D7FCA6F_CustomAttributesCacheGenerator_U3CPreserveU3Ek__BackingField,
	ReferenceEqualityComparer_t724BFFD314267A57B52C0A3AFFF04465F58A30CF_CustomAttributesCacheGenerator_ReferenceEqualityComparer_get_Instance_m43B68CACB4FE50ABE65B13C224229C45CB7FDC9C,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowOutOfMemoryException_BufferMaximumSizeExceeded_m6AACBBF084A0F2C5949C350FEC33B41F950E2A61,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_PropertyNameTooLarge_m77F825E6FA2ABACC4885601C5C87D9309FF8F0AD,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_ValueTooLarge_mB6D13E7E631571FE910659E29715926B40210CE2,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_ValueNotSupported_m28B4A5E1B7773753D54D7D3D00CA9EF64B8D9BD4,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_NeedLargerSpan_m42791059B9B5492C16E61273D3F33692EAEF9B1B,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_mA7A541529B4A42EF018C2E86CB4C896BD8900615,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_mAEE102BDC978858CF1E7B35197714BC9126D0B34,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonReaderException_mF1FBE8AEF1017A0B089C3B41D3C381DD4C1428BA,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_m388FA9CAE1E6661558861A241C547A9E0DF887BC,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_InvalidUTF8_m5D596F01AA0D55AF6DA151FD0B5C1511090E1B47,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowArgumentException_InvalidUTF16_mFDF55F6C19EC28ED0408D32C4ADC08ECAC22245B,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m95A110D95E052A207FF2F081DF1880159CAD85D0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_mDF46BDC21FFF5BA6D77F4B3BD33C73B5948D6665,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_SerializationNotSupported_mF73201276822CF6CC001E95CBA48CD83D6109062,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_ConstructorMaxOf64Parameters_mCDB2E3AC4FFD00386CBAA2DEFDD8AE1CD5BA4F1D,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_DictionaryKeyTypeNotSupported_m27903431590452F80C7758007DFD55D8B7A7E611,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_DeserializeUnableToConvertValue_m96FE8F452BB614BAB85A7E20BA40E22690C93F7B,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidCastException_DeserializeUnableToAssignValue_m4C68F1D7ED3EDC35AFC765960D3A679E34C51630,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_DeserializeUnableToAssignNull_mCBADC1DEFC6B20B8679C4EE5FE83BC5098203B9A,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_SerializationConverterRead_m4A28E5B4CC04FD16BA7CAA84D1B021F92EE220D7,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_SerializationConverterWrite_mD0997F6FA916D1E612AB3C737A23612AD9297164,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_SerializerCycleDetected_mAA15526A580E83EC7EDA47D50598EBD0D0D8E438,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_m973F42F766807F7E7BBC203E354BA06CA8EA055B,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_CannotSerializeInvalidType_mD2FD606E0497CF4BA9029A955923E121356C04D3,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationConverterNotCompatible_mC298E0922D7F2B7B68B13B4D3E5743C766D8F84D,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid_mA2E9DC408E16194EFCECD1A1371C32DE747E1339,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible_m6065CAB5382ABA2091B10F92E69FF90F37C59BFC,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerOptionsImmutable_mBEFF1850BBA433ADD5B20540B3C7594BEB6ABE0F,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameConflict_mF30BA34FBED9BA4C3F4F4CA6E834080C5AFE1CB3,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameNull_m9023D53865B4D6A638CC630D5D77584FE074E8CC,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_NamingPolicyReturnNull_mB9EB953C12D35F4E93345C912DBF23DB636451D5,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull_m5F731D2B8C69BD2CA0359516616C34D5ECDB80D8,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters_m05E79160F6B9CD59D30D8906BB2987EED6954601,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ConstructorParameterIncompleteBinding_mB139EAC290271F39E1736F7F1E9E2FF9B14C297F,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam_m4872A1B0FF96357B0C6B638C4EBA9E1761880D2E,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid_m603A1316F663B20F69E80D9E3811B452E919570E,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid_mB9A9F19F8AF43DC125EA07D32794D98B98F19E20,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid_mFC629CF3501D8B07CADE0DB5D3A96AFC3A8D517B,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_ConverterCanConvertNullableRedundant_mD154A895AB1495392DCCBA759457612D5A7C6E66,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored_m5EE618649BD8D2CBB732C6F0292DC3C4E0DFF91F,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m8C8F65479D5823F1F0A53F62D329626CB6590681,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m60D724FB73CD33130D2CF820A08AE5332F7AFBE8,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateAttribute_m6431717BAB38FE1C8D3A06DF615F73301FA5125D,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_m5D557E2F44F1A96A25337111B7BC2F1F6D0F5AF7,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_mB015CCC694FA963580C7403419A0D32B7941789A,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid_mDECC955E535EBF5416567323A734BA671818A50A,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mFBBEF762422B3346268279581FA1372DE9B433FB,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_DeserializeNoConstructor_m0316A2404C45BB2BBD6532E76FE9F384DB113010,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_CannotPopulateCollection_m18F801EC086A905E56F9380CFA8E011746E95DD1,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataValuesInvalidToken_mDC0FA9D497DFC575CCFD6422C843790C64656AEB,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataReferenceNotFound_m07F016A3E7895EC7753C09442854C4950A77F142,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataValueWasNotString_m9867E87D56EF64AABEE3844B4929587C5CB49A22,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataValueWasNotString_mC0D5A8277188661CECD17D5BAF5E7934D691DB34,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mCF9676414315C513337B5E8644F0A4927FD99F75,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mB36E6C2A52F6DBEDC8F30D9FCAF626F29A8B1399,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataIdIsNotFirstProperty_m845C5039FF51C4DF91792496C8A5A660AC75BC07,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataMissingIdBeforeValues_mE063C70456CE239D60E25F7381022F36523B764E,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_m338531C4877DDB86A6298075523EE839FD80D4E6,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataDuplicateIdFound_m168ECF07F07194EAC525711DE4894FBF0BF44C59,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataInvalidReferenceToValueType_m6AACE011A6BBB6D2092908770A52F33F538FE0F3,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m5DDDF08403852837CA5A82CD77FA084374975BF8,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataPreservedArrayValuesNotFound_mC760A54801B7D5833BB0BBB73CD9D331137C2DB0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable_m2541C3387E3799EA00D221A1E9D96E5748B4C2D5,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType_m59D2BC0FFD79A685E8D72FA1ED73341DCB4828D2,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowUnexpectedMetadataException_m66CB72849A9036C64AC1B6C05F19E46C0277F47C,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_get_IsDisposable_m32674A8E6A4F43BAAB12D25D7AB4B1F8202D6337,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_WriteTo_m4F0091C2294C11E823F07D253C25BA2DD720FDB1,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_Parse_m0C451D2F8DE35FD2D93E4F76D8DEB3FED0784504,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_ParseValue_mAFD1AF7093FB6B8974C2C4ECC111F8438E0DA731,
	MetadataDb_t5CA7F8F87B7B358F3B574C7E142142511EB67E26_CustomAttributesCacheGenerator_MetadataDb_get_Length_m358C82F2ACD118F1A31750A408DE4A80D9706AC1,
	MetadataDb_t5CA7F8F87B7B358F3B574C7E142142511EB67E26_CustomAttributesCacheGenerator_MetadataDb_set_Length_mDEED679131C22B2B98BD28E71CABB539B01E79C7,
	JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_JsonDocumentOptions_get_CommentHandling_m250724FEAA7D45E779F026A8E2AE1792E8CC0223,
	JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_JsonDocumentOptions_get_MaxDepth_m910F496990C5F7315734B42E49D6B46D7D7FB146,
	JsonDocumentOptions_t418891DA23BEAC275F96171708DAA6C84110F675_CustomAttributesCacheGenerator_JsonDocumentOptions_get_AllowTrailingCommas_mB695E7244BD9C08298D2A980CE3C8D6C69D0F2ED,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_GetProperty_mF3F895EADD2EBC27D13528083FBA7A8E754C02B9,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_TryGetProperty_m1F48BA59A2FF4DD5DEC6B04E2DFEF7785CAB89DE,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_GetString_mC81CE313F3CF6ECFCC6374545F7B598A244FA53E,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_GetRawText_m57611974A95EAA4466054192D51EAC5A1487249D,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_WriteTo_m1AC8B59D427C19E13F2134BA8964477C9B3E4F20,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_ToString_mA0336A268F12CE4735C0307F12F4717EA5A24B29,
	JsonProperty_tCB1F3938C98F37487EAEE5CE83568B4E7E397043_CustomAttributesCacheGenerator_JsonProperty_get_Value_mF766C4EFFB858EEAD2A009AF55BB8B51095AC96A,
	JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Encode_m48F8D41B2ADAECA21D3B959F62C2D08A73A1F317,
	JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Equals_mCE77AC6B7E663C8B7D8412B394E91F715A5BE682,
	JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_ToString_mEC405B95F27BDBC3814C264035F3B0151621800F,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException__ctor_m4A92F38705B569451DA8C63F2E1AD5375DF6E8CA,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_AppendPathInformation_mE4D721DFC493525073DDB220BA2A1499C1C08362,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_AppendPathInformation_m30D14D80A9F43FB975791CEEBA7B728E6A18EA6A,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_GetObjectData_m82F05FF628ABF99E30D9C8581FB85D37EE9134B7,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_LineNumber_m7D21C9A5A7431696FB3B195690A1532E46DC4601,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_LineNumber_m60FC0BA8C1BA4D775C58DC3657DCEEAA3B9D6631,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_BytePositionInLine_m74C4E152BDFC2439E479ED098CEF728C74EE713B,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_BytePositionInLine_m6CAD5D3CB0CC4F9638C91C71B8E0A44DFEBF9569,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_Path_m1B60B637D1DCB55E79E0F91381230144DC3DACA0,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_set_Path_m2ADEFDBC15D75EBD73C8DBCB04C21DA8717B571A,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_get_Message_m7FCB5F5C5C8587DE96DD591EE60CF610AE367AD7,
	JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_GetSpan_m9ED27DE6D38D4D2F3CFBAB1F52898CF2B2BA407F,
	JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_ReadWithVerify_mCC9DD465FF6CCB7DAF613B7D203956794D5D9939,
	JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_U3CTryParseDateTimeOffsetU3Eg__ParseOffsetU7C21_0_m088E2588D6A3243BACA89CEF31560FE6818CC879,
	JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_ToValueKind_mC5065501F4DC87FF00ED3C35317E53E604B4DC5D,
	JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_IndexOfQuoteOrAnyControlOrBackSlash_m1465C1D49863A0D9DF2D893FCEF23416D6736AA4,
	JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_get_CommentHandling_mC784945C4A7D8BF00627E0D71CAF144E059CA45B,
	JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_get_MaxDepth_mE9E196FD22578986617CBC0B6104934965E7DE18,
	JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_get_AllowTrailingCommas_m6BE15A1BB0597C203011627DCF4E9CDDB42049BA,
	JsonReaderOptions_tD7C9A60EFCE572E4EA0AC3B1B0731900248253D3_CustomAttributesCacheGenerator_JsonReaderOptions_set_AllowTrailingCommas_mB8C99444AB5B8A9D50FDDD8E2CDC8F473B6D0641,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_ValueSpan_mBABD1CCB678681EF0BAB792BD92B1A667B5280EA,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_ValueSpan_mCB290F34DBE06C916AC61FCD97325363E6DF8840,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_TokenStartIndex_m065C1E55350901EE451793D91D45C6BFD6727825,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_TokenStartIndex_mD8DE6AC3843B60206928FF65843B1063725E11D1,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_HasValueSequence_m0E4BC52E674A56D3F8FD1EF9F56BCA86F7851658,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_HasValueSequence_mA04C744A5B061CF11B292F4FA30685D8FF959819,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_get_ValueSequence_m8BFD79D1CB26B6B8C081CC01F855EFD3A48E74F3,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_set_ValueSequence_m14F871806DC91F89B691C08FDFDBA71DB941C6E4,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetString_mF6ACA3547007BAAD951423AD47FC7C8FB1EE1C44,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetBytesFromBase64_m0B0E3C7FA55C6B1BF7B6513D80CB4159543F5F67,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetSByte_mF0C15DF1DFAAFED79A5FF6527BC1EC6DF8D6D541,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetUInt16_m91B334F666BA8952300156AC744D340C01A0FA68,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetUInt32_mCAAA27CB167078F0E49C896E0D38F71DA98A1DA1,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_GetUInt64_m04FC93AD8353898C2E5383CBF9616A0D03DB05AF,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetSByte_mF7778E9C90F9D7C8CBE321D9C4B3859CE02B9106,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetUInt16_m018B28F35D595DDDBB3A104F69E2BAB463CDC274,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetUInt32_m3568E2AB280AF64148B26E5A570DE313B5F6A557,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetUInt64_mA1BFC2EDB1096EEA85D3F3992A6C33B599769A63,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_CreateObject_m3924F94319E8CC8037E397EC71A758534AE7E3EC,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_CreateObject_mCB383ACB75DFF40E78CD099DECE8A89CC5C95C9A,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_CreateObjectWithArgs_m5F1565F5FBA9FF4DA02786C6654771AC177B1E39,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_CreateObjectWithArgs_m737C399B0900D4807B7E6A881979765184CA70BE,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_AddMethodDelegate_m488E73B03A89040319408B5DF87C9C4DA514335E,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_AddMethodDelegate_m99DA3023113AB512DAB80E80B226BD003AF03135,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_ClassType_mB004262B1B41B552E9345A10FFF3F8E48F2D408B,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_ClassType_m1FF3CC9DB07D8AB41E249DA6AF8AA0DBA83616B5,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_DataExtensionProperty_mEE9451B64DEAF5818915EA439213B5A8FEC028FD,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_DataExtensionProperty_m869AA0DB667AFCA30A5BA4DAD22FC963343E9DEB,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_ElementType_m770A51860EC4421D6741DCFF06916FB33AF53858,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_ElementType_mC498B4A763F7C2A01DBA9D553145F8D0CA6C7874,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_Options_mA487DF18E8069C47D2FA573FB9DF584F488F93CE,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_Options_m2F599919B350219F457359B4E7FA7A1B1E83D8C6,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_Type_mF4833C6030319D73C7C0E8115D34603648574D70,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_Type_m35C5D4D035C6196861487CAE2B4E7D8516848672,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_PropertyInfoForClassInfo_m43DD25A15635FAD68648F55AD79A8B6FB15433FC,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_PropertyInfoForClassInfo_m50F366281A4E6F986341608C934DEDA64EF2BFD2,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_get_ParameterCount_m9A7A9DA3DC8B1796546F01CAE46D2F6AB5F71A84,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_set_ParameterCount_m703F8184C00284D40E133AD69FB81EC896B5020F,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_U3CInitializeConstructorParametersU3Eg__GetMemberTypeU7C46_0_mA004A63FFB79C1F8C5E65EC01C505FC2E75CDEBC,
	ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_ParameterLookupKey_get_Name_m048FB355797E2E00063B62A5FDB6A30CE1C543CC,
	ParameterLookupKey_t256E425DFF067FB4EADE144D16CE9F71AB8FF6F9_CustomAttributesCacheGenerator_ParameterLookupKey_get_Type_m66F89A37473AEEDE2674D17D6FD97C773D54B23A,
	ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_ParameterLookupValue_get_DuplicateName_m2846FDEB4E05B3167D754D5F2774A3F8FFDD3B50,
	ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_ParameterLookupValue_set_DuplicateName_mE89B81F211FE3342D05118E95ACF64341027BB43,
	ParameterLookupValue_t4CD624C3EDE8825E6865586D118C4B839F1532A9_CustomAttributesCacheGenerator_ParameterLookupValue_get_JsonPropertyInfo_m8570A2FA9EDD6BFA2A1C5DE2B8AF26287C402CFA,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_ConverterBase_m280CF92A015816989461A48B357E7C2D1BA6D398,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_ConverterBase_m63A442D88DCC1E79C3E1FA314ADE4351B0278E4D,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_DefaultValue_m9C4947604651E2557BBF7107864DCAB0DE907877,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_DefaultValue_mD2FA38670AC6C3527521CDB54EAD7F359E8F8E1B,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_IgnoreDefaultValuesOnRead_mFB2E81AD69B208C02EDA92059CC9F0706B162D4E,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_IgnoreDefaultValuesOnRead_m9A32A1D7B960DB0BCA25BF1D7F657C36E8AD43FA,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_Options_mD740985271C61D63EE42CB9B9C49DF14B84639B7,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_Options_m8D916748A8A1CFFAF7BBFECBE0B46461BD2A05B9,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_NameAsUtf8Bytes_mC74D28CA16D8E6CF8F4341D3A10001917FA4EEC2,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_NameAsUtf8Bytes_m9F52C8A73862C84E1AB8A136F85A8925778AF9C0,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_NumberHandling_mA793C420F52157BFAC40C89B747D48159D519FB4,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_NumberHandling_m69F1AFEAA58436C0EB39B05FB8FBE29870288CF5,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_Position_m4ECD7B7B8E7B34713BFC26010F66169AB271D601,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_Position_m50E95F79ED24C836FC139AF7C7D3190BC0B2DC76,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_RuntimePropertyType_m12B753E79AA5EE3DDDE208C90283F9D7D7A1B9F6,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_RuntimePropertyType_m936B468A974E2FC8D5A5771111A948E3FBBF1E0E,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_get_ShouldDeserialize_m0BFEBB5B82CFD53BA18436FDF40579DD363D8C4B,
	JsonParameterInfo_t5A63F545864DCF3B4626F55CA5A68D301F48B1F1_CustomAttributesCacheGenerator_JsonParameterInfo_set_ShouldDeserialize_m2B4028AA15136C622319698384D9F848B6575B6E,
	JsonParameterInfo_1_t51FCEAA906D6924F2738F588AF886649E190DBCD_CustomAttributesCacheGenerator_JsonParameterInfo_1_get_TypedDefaultValue_m681FEAF53F1C086271B606C7118006F463CB978D,
	JsonParameterInfo_1_t51FCEAA906D6924F2738F588AF886649E190DBCD_CustomAttributesCacheGenerator_JsonParameterInfo_1_set_TypedDefaultValue_m2028FB1A3946FF4D24CE3D0FB52EE54A7347A325,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_DeclaredPropertyType_mACBDE6A28FC500481702320EF6E636DCE7C73DAA,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_DeclaredPropertyType_m58E832BD84C9BB9961F9818EF8E1A9B541E552DB,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_HasGetter_mA8ABECDB9D68BD370F0ECF0B51A530AF6CBE49C0,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_HasGetter_m3E3B9FE50F71BF6965E4179F44C5118394F4EB76,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_HasSetter_mB7EBA2506BCCB52852390BA7C0EF680114BFCC8A,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_HasSetter_mC66078A3C7DAAEF93BD18FEC9CCFCB684901BEBD,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IgnoreDefaultValuesOnRead_m485BF3036EFE5652D3354AB52BFE4AD9C462EBD4,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IgnoreDefaultValuesOnRead_m9ACA6E5CBB4477A57064464E509A4A031287A294,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IgnoreDefaultValuesOnWrite_mE576FFF14D1B98A34C69F485B99757933138AE42,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IgnoreDefaultValuesOnWrite_m591097336FB6F6C0AE4BFF25BBED8FC9515381D9,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IsForClassInfo_m5E9EB12AD97755E0026D726A4DC13F9D3A570A9C,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IsForClassInfo_mF91A27CA43772F77B01DF9DBE19276384CBD3A67,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_NameAsString_m907A831A0369E765CDED710035B36F0FA5AC8B9B,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_NameAsString_mD58CEEB19C9F3D7C5AD6406FCBD1355D53C48D36,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_Options_m13E3BF459F8675C65E6C608482C9C0789F75B97A,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_Options_m53F38E18046D1268125293BDDF9ACF1B0F34B668,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_ParentClassType_mBFF8F3DC065713FD77F12788DFD0E353C0FA877F,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_ParentClassType_m14715F89089DCC5D3B64C72199F01FE44409CD11,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_MemberInfo_mDB50F867A36435920C12ED71CA72580FF7EB3A8E,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_MemberInfo_m6BBABD3EA7E95FACE7C093144DB3648FBD62E4B6,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_RuntimePropertyType_mE2C5C15069C2A00DE2C1AD067091F95D79149019,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_RuntimePropertyType_m5CFF3F102D982C87100EEFFF55192D91D75EB45B,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_ShouldSerialize_m17CDCACBC8CBDE574091BDCDD33CEB8D017839D4,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_ShouldSerialize_m8BA04314A995978B0066992640E2296C146C3839,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_ShouldDeserialize_mA6E24C13D03410019DE42F107146B0361AB3D5F0,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_ShouldDeserialize_mD9BB3AA10AC77793B2D49D65460CAF4EC7908709,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_IsIgnored_m21287FDA8B36179BDE92703E355B5CDCCD6B2C1D,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_IsIgnored_m81FFCABC248845376883684AC190028BADAC7A84,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_NumberHandling_m3F57EC55C9CC7CE9C1017CBF1B737759673F3947,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_NumberHandling_m7171FCC5706B63D324F911FA3D868549B17183B0,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_get_PropertyTypeCanBeNull_m00E89F86EC69375E087FE13D14C58FF621132391,
	JsonPropertyInfo_t4ECFDE458350897F1300CC958DE8A6777F13920F_CustomAttributesCacheGenerator_JsonPropertyInfo_set_PropertyTypeCanBeNull_m1B4AEF7B3C4E167742433F15E6DBBA625C7FAFA0,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_get_Get_m97B30AC8E17860A25194510E56579A3772661EDB,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_set_Get_mE67BAD47F380088072E730BB8E499DA4A92AA8A4,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_get_Set_m8CB54AE95DEE35CE41A0A7A759D62769D7E7C7A4,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_set_Set_mEFF83D7A04998146CE9F9F9ACF10D2485F2BFE69,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_get_Converter_mF71EC785285275F2987FA1FDB4251C4E79884EA2,
	JsonPropertyInfo_1_t30D8C664EF38EA399B10853E62B5F061AA9657BA_CustomAttributesCacheGenerator_JsonPropertyInfo_1_set_Converter_m7366925B8226F17F31A03FAC040A1B9DCAAA9395,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Deserialize_m48873845B16152EC9392A85834DF263182111620,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_get_Converters_m387F196CD3A362700DF74ECFEC1DE4873D84180E,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_GetConverter_m3F90FE1FF74CAFEDE6BA42D78D9AFB3CF317D6E0,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_get__lastClass_m625BC41A0607D88F18B0796584D3AA94640639EE,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_set__lastClass_mC4E971444165788CBAF11FE1B3BA0DFACCCF0CC8,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_get_EffectiveMaxDepth_m5B3CE03573116B7D5920A24F386226C7497CA6B9,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_U3CGetDefaultSimpleConvertersU3Eg__AddU7C3_0_mE1E7914A06F0CF6977D174FD4DDDCD2BA2D7CC86,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_U3CGetDictionaryKeyConverterU3Eg__GetEnumConverterU7C4_0_m422925E719E621089038D602E3BBD474F7C96DB5,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_U3CGetDictionaryKeyConvertersU3Eg__AddU7C6_0_mDA972273A76E4AF978A8EF39C01033AAA1D83A96,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m8ED401FF34693D816348D4CE72C93E7394006130,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__GetCountU7C19_1_m75E574E6B1DBFD7C4822567526DC011D8AD9FC40,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__AppendPropertyNameU7C19_2_mA80E427F474EAE78F6CC63C1028369018338D28E,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m104FC083CCE5BB72491DD4BA0B26BCCE2BAFB176,
	WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator_WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_mFA8217E89FE0DFE851708748202127A07F74AEBA,
	WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator_WriteStack_U3CPropertyPathU3Eg__AppendPropertyNameU7C13_1_m5A499E425FD4D40245FC2BDDF6CB558870F6A287,
	TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator_TypeExtensions_IsNullableValueType_m4898086A046C3E4BAE28D49F62382EABE2D51079,
	TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator_TypeExtensions_IsNullableType_m12352B86A8DCA9AB2EB22C019B3A19C15144CFFF,
	TypeExtensions_t9CF6327BAF4A9FE06EB2C9E75BD148BB58A62746_CustomAttributesCacheGenerator_TypeExtensions_IsAssignableFromInternal_m87241C5103114B0C94797AC15693EF1AC0C62803,
	JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator_JsonWriterOptions_get_Encoder_m232389EA65BBFEC33F6133822B6014A12A3DD4FE,
	JsonWriterOptions_tDF037E30F6E08170C5D33C09632D79BC06A4A67C_CustomAttributesCacheGenerator_JsonWriterOptions_set_Encoder_mC29EDD6B2528E7E44A0E76471CC62E46FA114F00,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_get_BytesPending_m3972ECDA5A4E7DBC6A17F7AA0610CB20AB58EF2C,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_set_BytesPending_m21E09C8027465D1AD0C46E31DD91C8C83524DB10,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_get_BytesCommitted_m4647460AF1A97E6805CF12393C978FE240CCB076,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_set_BytesCommitted_m05CFA019A22B21A6E59B688CCD22B1C76C42E9B3,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter__ctor_m878AA70AA00DE7300C1749D15BF67D09B30B335C,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WritePropertyName_mC9ED158A09A22E81D95630C164199CE7DF9A849E,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteString_mC4C4697F80008CC325DA268D181126F27A29F2C0,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteStringValue_m1271B865848F4BF39035ACB38CD54B914A3BF385,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteNumberValue_mF8CCB5E8AF7CB32821E48257DAEB9488540C95B5,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_WriteNumberValue_m14B1AC5C45AD6F1FB2A9F42FF28CEA2CF6D8CBCD,
	JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator_JsonConverterAttribute_get_ConverterType_mBD3D8984B125937D81BEE770510CBD59DE992D6F,
	JsonIgnoreAttribute_tAFD2E63A69829808553EA5CC6DAB6C19B6FD2C8A_CustomAttributesCacheGenerator_JsonIgnoreAttribute_get_Condition_m5218388DF4B31B960A88F80CDB92BAD3A1575391,
	JsonNumberHandlingAttribute_t0CBA24EDCE63CCEC4F7DDC42A56A6155DAA54816_CustomAttributesCacheGenerator_JsonNumberHandlingAttribute_get_Handling_m47EA30AE14CB014E642C09FC271D744207304236,
	JsonPropertyNameAttribute_tA9BAC1CBD1A30B982B34B6FBDD9A74016C54A2C0_CustomAttributesCacheGenerator_JsonPropertyNameAttribute_get_Name_m43379618C5E4C36982CD87DCC7DCD46C9AF7636A,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetCompatibleGenericBaseClass_mFE8785F58EDD007ECCB3D76077B0565BEE8DAA2B,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetCompatibleGenericInterface_mB640DD8282CAC982B0360A4B1F5DB36603201157,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_IsImmutableDictionaryType_m35307B7347E0B7A698DA5E80E439A484D063DA42,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_IsImmutableEnumerableType_m7718A49E6C83EB1AE4F16348E3FC3CDBB61A1EF9,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetImmutableEnumerableCreateRangeMethod_m750E671FCD270FA0B1B2E58C2AFDF41843605706,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_GetImmutableDictionaryCreateRangeMethod_mCAF50FB9DCCB97B1BEE3EAE5E50A55427D6EC02F,
	IEnumerableConverterFactoryHelpers_t5662AAD4BC454B694F30CF76B78CC1D16AB37379_CustomAttributesCacheGenerator_IEnumerableConverterFactoryHelpers_IsNonGenericStackOrQueue_m4E32775D53BA32FD8CBA158643133A7F924BCDE9,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_CanUseDirectReadOrWrite_mE2CF0ECA1CCB2379ADDB5CA9927657FAE9420C97,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_CanUseDirectReadOrWrite_m13DC84941788179AF7B397950B55551825EE1A46,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_CanBePolymorphic_mBA6DE039DABAEDEDDBBA10D5EF2D6535744EE450,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_CanBePolymorphic_m8CD8BA9054E3885A732BEFDF45DEC79A80145A7F,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_IsValueType_mB362FB9B424A6911D4F4F54CE6E0FCB142D4895E,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_IsValueType_mA4651543A67E50CC092948CEA8F29BE5B23DC911,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_IsInternalConverter_m0FC7605392703BA872B4DD89C327766EFF2C79A6,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_IsInternalConverter_mF87A3D79885CFC9C8D8BD08BAF230EF7DEE9AF40,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_ConstructorIsParameterized_m120E1195BC89B5743A895922978D983F3A4E92AF,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_get_ConstructorInfo_mE3B569C452D11FED23B56BF8DFF557811B830959,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_set_ConstructorInfo_mA1D9AA73D0C0C45A774EF06EA013731BC58B698C,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_get_HandleNullOnRead_m8DF85C0D9C9EDF126337A5665F1A90223354B69F,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_set_HandleNullOnRead_m4F3CD209183C39B31467B086C77988484432A5F6,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_get_HandleNullOnWrite_m2150D830E0E7CE43B516932BF3A92D71DC43E110,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_set_HandleNullOnWrite_m336A1EE8A52E0A09D42C5A1DEB1C4275370F9F1E,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_get_CanBeNull_m5CB7B8E5BC1D8B71C4A158E989643EA26CFE8C5E,
	DictionaryDefaultConverter_3_tA62DAE9BC2F192BA50468530511AFF7941A43014_CustomAttributesCacheGenerator_DictionaryDefaultConverter_3_U3COnTryReadU3Eg__ReadDictionaryKeyU7C12_0_m3D849C4A2202DAB2E7B46EC2BD55BC829549AEA4,
	SR_t9B1F15CEEAB1CD24A1889ABB4CDB6DA7D327E9EF_CustomAttributesCacheGenerator_SR_Format_m98E7A215B14ABB55764D42C2B1B82AA655F42297____args1,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m8C8F65479D5823F1F0A53F62D329626CB6590681____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A____reader1,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_AddJsonExceptionInformation_m27AA11CA530AE35B24E4AD7E1BF42051046CDD75____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_AddJsonExceptionInformation_m27AA11CA530AE35B24E4AD7E1BF42051046CDD75____reader1,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ReThrowWithPath_m60D724FB73CD33130D2CF820A08AE5332F7AFBE8____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_AddJsonExceptionInformation_m19A08342AAA7D86A83B9A636BEA522C2F2232918____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3____reader1,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowNotSupportedException_mFBBEF762422B3346268279581FA1372DE9B433FB____state0,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_m338531C4877DDB86A6298075523EE839FD80D4E6____reader2,
	ThrowHelper_tB003E820DA4CFC3E2DA9EC535D887A3CA77ED6C9_CustomAttributesCacheGenerator_ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m5DDDF08403852837CA5A82CD77FA084374975BF8____reader2,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_UnescapeString_m2DF634F6A71C280B6D0B94CDFFA6B1889640FBA5____row0,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_WritePropertyName_mD6770EEC4513C8606F5A55BFD29B59FE9B8B486F____row0,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_WriteString_m97C83763877F4E016B011EBD62E8B7D975D2E6C1____row0,
	JsonDocument_t9E34DA6C1FAC60E7537974DBA444E4240FDD3DF1_CustomAttributesCacheGenerator_JsonDocument_TryParseValue_m376E476DA7C90522D73A2BCA568CC00ECD2EBBCC____document1,
	JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Encode_m48F8D41B2ADAECA21D3B959F62C2D08A73A1F317____encoder1,
	JsonEncodedText_t2D09D08EE32F0946009E8C0BE33509AE1C33D346_CustomAttributesCacheGenerator_JsonEncodedText_Encode_m670B6211FE6F2601D2CA63B0065AE2C71DD11A49____encoder1,
	JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_TryAdd_mABD6B4AD92EA7F473B450B4D21C8A51634C2F5B5____key1,
	JsonHelpers_tB783D88DE9073B462494BF1884F39DE9B4DFF0A3_CustomAttributesCacheGenerator_JsonHelpers_TryAdd_mABD6B4AD92EA7F473B450B4D21C8A51634C2F5B5____value2,
	JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_TryGetUnescapedBase64Bytes_m6B842CBF6259E1588D3161286FA663515A9512A4____bytes2,
	JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_TryDecodeBase64InPlace_mB9FB71368E0C656AD14ED4EC3AAF8C15A8557610____bytes1,
	JsonReaderHelper_t163DD5EA6CD8509EA622AD4FA0626499757BF69F_CustomAttributesCacheGenerator_JsonReaderHelper_TryDecodeBase64_mCB66A113B2B0E3D25218985C4F68DEE3BEF4D91B____bytes1,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_RollBackState_mE1AB8FF05072C2D1C921F4D97E56122C15E905AD____state0,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeNegativeSignMultiSegment_m135AE9A283C44CD51F7BEFF2A2E0FD9890FCED3C____rollBackState2,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeZeroMultiSegment_m281FC6658603949FC90711BE6C81B8C3108B9E2D____rollBackState2,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_mE037A2CF96C775C86599C5A61F9E7EEB2C411140____rollBackState2,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_ConsumeSignMultiSegment_m05A9CA258362283F551D2CC8A7B43DAAA1F99D7F____rollBackState2,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35____value0,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_IsPropertyRefEqual_mB02C7B4FC60D6274B5A70866495240515CC8DF34____propertyRef0,
	JsonClassInfo_t1F7C9730029369449C401FA477693844B283CB18_CustomAttributesCacheGenerator_JsonClassInfo_IsParameterRefEqual_m88A98AAE34615FB5DEB1F8309A5FF81DE7D1C639____parameterRef0,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Deserialize_m48873845B16152EC9392A85834DF263182111620____json0,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_WriteCore_m8E6E18D5998A61A65F9FB6892C30C2EFC706F5BD____value1,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_WriteCore_mDE73B6E3C4C75D4F374316DAEF0D726C54911E21____value2,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Serialize_m7513647C440FBC2D65E6F2A32326A90F60A47A6C____options1,
	JsonSerializer_tCC819A8FC8F239499861C3BDEED1EA183A9EAA66_CustomAttributesCacheGenerator_JsonSerializer_Serialize_m3BEE629EB4FB56379EC8BC3299D6CF05420C4208____value0,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m8ED401FF34693D816348D4CE72C93E7394006130____frame1,
	ReadStack_t14CBAF200D5EA9722FC42A7DBFA991438573447E_CustomAttributesCacheGenerator_ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m104FC083CCE5BB72491DD4BA0B26BCCE2BAFB176____frame0,
	WriteStack_t624F9C4FD4312A83B1BB9AFDC475267AC88CAE30_CustomAttributesCacheGenerator_WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_mFA8217E89FE0DFE851708748202127A07F74AEBA____frame1,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_WriteCore_mF5723030398F3B3C33E33B080C0645FABF89A92E____value1,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_TryWrite_mC4CA66FBF06D08970AC91E9C64FF25B4BDB46497____value1,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_WriteWithQuotes_m52D04EBF6D2C9745CE9DB5A89ECE2944515F2902____value1,
	ArrayConverter_2_t738A531D7A3C6E0AFCB25E30E956ADAB7D5FD46C_CustomAttributesCacheGenerator_ArrayConverter_2_Add_mA8C55F4C35457E96A2EA232544F182EA48E1CC57____value0,
	ConcurrentQueueOfTConverter_2_tAFDBFCB4D455EC207336F12779941C463F7A14D6_CustomAttributesCacheGenerator_ConcurrentQueueOfTConverter_2_Add_m2E02E7372C151304E32E629E6F91588829617D16____value0,
	ConcurrentStackOfTConverter_2_tE0CD646473117484AB404C8133BF468FF87C004D_CustomAttributesCacheGenerator_ConcurrentStackOfTConverter_2_Add_m2003F3DC3E4F1F3EA31AE1294A13D5DFDC1EEFC5____value0,
	DictionaryDefaultConverter_3_tA62DAE9BC2F192BA50468530511AFF7941A43014_CustomAttributesCacheGenerator_DictionaryDefaultConverter_3_Add_m15D1A9A9EF84F82BD9D106C11555B11B4AEC34EA____value1,
	DictionaryDefaultConverter_3_tA62DAE9BC2F192BA50468530511AFF7941A43014_CustomAttributesCacheGenerator_DictionaryDefaultConverter_3_OnTryRead_mC6BF3E6BB0611A624A55B33D907514211522F033____value4,
	DictionaryOfTKeyTValueConverter_3_tA274F0E2A3EB2660B331194B8856AE3ECE544200_CustomAttributesCacheGenerator_DictionaryOfTKeyTValueConverter_3_Add_m74764BB7563516CF6744324708F99152A75B23A3____value1,
	ICollectionOfTConverter_2_t94EE78D560E29B7B1ABFDB4C7FC2EBE0FFD1118D_CustomAttributesCacheGenerator_ICollectionOfTConverter_2_Add_m7093DFB5C5AF29EA562B55660682E21A418C5FA0____value0,
	IDictionaryConverter_1_t2258D0512F1BF4E366D60612FC562EF47650C81D_CustomAttributesCacheGenerator_IDictionaryConverter_1_Add_m16C3A757A17DE63302C60E66FD34615194E37391____value1,
	IDictionaryOfTKeyTValueConverter_3_t952E923CFE2BD55DFCBF1B4132C7DF1BD68D8E03_CustomAttributesCacheGenerator_IDictionaryOfTKeyTValueConverter_3_Add_m67266029526CFFC925A3008CC9A74C80848C3EF3____value1,
	IEnumerableConverter_1_t4EE4E93D2103E1A39CC092C486E03763A64E00A9_CustomAttributesCacheGenerator_IEnumerableConverter_1_Add_m3121D8B90D9933B293A1F300BDA46F004A4AD639____value0,
	IEnumerableDefaultConverter_2_t5F900274EEA245B8BE1771664576677B2D55F20F_CustomAttributesCacheGenerator_IEnumerableDefaultConverter_2_Add_mFD08199F7BAAE66896A62ECEE16A3602EEC31B95____value0,
	IEnumerableDefaultConverter_2_t5F900274EEA245B8BE1771664576677B2D55F20F_CustomAttributesCacheGenerator_IEnumerableDefaultConverter_2_OnTryRead_m7DC07F0B6CEEC04127C930E373401C9F3D61A18C____value4,
	IEnumerableOfTConverter_2_tB6887BCD6F0D5899AC78295C829672681ACB77F2_CustomAttributesCacheGenerator_IEnumerableOfTConverter_2_Add_m774BBB9687370A2B6BB9E1E32142F383291E961D____value0,
	IEnumerableWithAddMethodConverter_1_t989F922F36BE095F9AA186B58026492ADA7EC03C_CustomAttributesCacheGenerator_IEnumerableWithAddMethodConverter_1_Add_m126906C51B8B823DD32BEC6D8D75D0CBE1DAF5CC____value0,
	IListConverter_1_t45F01AC4B0F94D192395D7CE1466C2D8C28EC9A2_CustomAttributesCacheGenerator_IListConverter_1_Add_mA7193ACCB58EA1636E58E8BC3AAEFECE4D7D393F____value0,
	IListOfTConverter_2_tBEB208D96A82B8BAFECB962ED44C38A3A9003457_CustomAttributesCacheGenerator_IListOfTConverter_2_Add_m4ED07A81323514D01D3BEAB40FD9F89C4FE5F294____value0,
	ImmutableDictionaryOfTKeyTValueConverter_3_tFE1D0F780D64C770E0144410CE24C0DD3885D6E5_CustomAttributesCacheGenerator_ImmutableDictionaryOfTKeyTValueConverter_3_Add_m67B68023309D5EAF4F22F5F30DA24AD1E5DB4834____value1,
	ImmutableEnumerableOfTConverter_2_tA3247BA20882FC44C0ED22A6E94632316ED73AD3_CustomAttributesCacheGenerator_ImmutableEnumerableOfTConverter_2_Add_m7F41C9FB5BB92FFBBA6D9E68A89BE88AA6117289____value0,
	IReadOnlyDictionaryOfTKeyTValueConverter_3_t2BFCFEC76862FEBEE06CF57739F149D63410C0AE_CustomAttributesCacheGenerator_IReadOnlyDictionaryOfTKeyTValueConverter_3_Add_m55F405716C54EEBEE5DB48D5CB9114258AB01182____value1,
	ISetOfTConverter_2_t3640D79EE209D8EB3467BD71F7509529418F817F_CustomAttributesCacheGenerator_ISetOfTConverter_2_Add_mD33D35BE2CF00C158F11405429ED8DA494BBC6D5____value0,
	ListOfTConverter_2_tE8A491A335CA5CA8B704CEC2B1D35A8A1C1D8D3F_CustomAttributesCacheGenerator_ListOfTConverter_2_Add_m1E0CA0DEADA14C50979B46DAB4641317C6DAF0E2____value0,
	QueueOfTConverter_2_tF2816F12578BAD6961900AB08D11B44C0E98CBB0_CustomAttributesCacheGenerator_QueueOfTConverter_2_Add_mC0683A2C6C74DFA12FDA5B3B99F0C9CDAD8E44BE____value0,
	StackOfTConverter_2_tA56FD2B04CBBA4749F4471DB60151561228BC7F7_CustomAttributesCacheGenerator_StackOfTConverter_2_Add_mAE48E54DC4B7F78FFF3BA918398D7B72889AD87C____value0,
	ObjectDefaultConverter_1_t28F9BFA559442122CC9C0434B5D7EE6F99A8D11D_CustomAttributesCacheGenerator_ObjectDefaultConverter_1_OnTryRead_m4AF3B46BABEBF42B4998811EE1B180AE238D8350____value4,
	ObjectWithParameterizedConstructorConverter_1_tEBAFA9FEF841EFD0ADE2F321935A745DBF0867DD_CustomAttributesCacheGenerator_ObjectWithParameterizedConstructorConverter_1_OnTryRead_m947A07DE4DD15BA037A5136737171E22BA92EC08____value4,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49____TokenType_PropertyInfo,
	JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49_CustomAttributesCacheGenerator_JsonElement_tE672EEF882F0BE64187481F9A01ED59DC71A3A49____DebuggerDisplay_PropertyInfo,
	ArrayEnumerator_t20A1E2484160186A78E0A58CF4F9E9B7075A71D0_CustomAttributesCacheGenerator_ArrayEnumerator_t20A1E2484160186A78E0A58CF4F9E9B7075A71D0____System_Collections_IEnumerator_Current_PropertyInfo,
	ObjectEnumerator_t66BDC39C224ABC31BECA2237860C5FCB04EF7812_CustomAttributesCacheGenerator_ObjectEnumerator_t66BDC39C224ABC31BECA2237860C5FCB04EF7812____System_Collections_IEnumerator_Current_PropertyInfo,
	JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99_CustomAttributesCacheGenerator_JsonException_tE22D94920BE340B46FF09AEC78FCDBE5C9068F99____Message_PropertyInfo,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65____DebuggerDisplay_PropertyInfo,
	Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65_CustomAttributesCacheGenerator_Utf8JsonReader_tFFE5533971B0C511EB8AA1DA06BB31D3D8C21C65____DebugTokenType_PropertyInfo,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904____Converters_PropertyInfo,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904____IgnoreNullValues_PropertyInfo,
	JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904_CustomAttributesCacheGenerator_JsonSerializerOptions_t77B1FC04F2E8F2FB6884E3A0DCBAC3B5B0067904____MemberAccessorStrategy_PropertyInfo,
	Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C_CustomAttributesCacheGenerator_Utf8JsonWriter_t93EB29BFC60D7BEB75A4B67A20EB67FDABAF4D0C____DebuggerDisplay_PropertyInfo,
	JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3_CustomAttributesCacheGenerator_JsonConverterAttribute_tB2F55EC9F1967409A1DCDF474311EEA52E1DBCC3____ConverterType_PropertyInfo,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3____ElementType_PropertyInfo,
	JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3_CustomAttributesCacheGenerator_JsonConverter_t251202A0A1DDA5AC5C3F7316E5532F3EA725CDB3____ConstructorInfo_PropertyInfo,
	JsonConverterFactory_tA7C5BBF890986F0A26A1259C87976F681069EE73_CustomAttributesCacheGenerator_JsonConverterFactory_tA7C5BBF890986F0A26A1259C87976F681069EE73____ElementType_PropertyInfo,
	JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34_CustomAttributesCacheGenerator_JsonConverter_1_tA778586617E32F79F25ACE74C5E368D94B46CD34____ElementType_PropertyInfo,
	System_Text_Json_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_inherited_2(L_0);
		return;
	}
}
