﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String SR::Format(System.String,System.Object)
extern void SR_Format_mB69240C1ED63A6C8F368CF678CF2EFC426495DEF (void);
// 0x00000002 System.Byte System.Numerics.ConstantHelper::GetByteWithAllBitsSet()
extern void ConstantHelper_GetByteWithAllBitsSet_m0576CC61FD3FF54167A6F677EAC509D474F24DA0 (void);
// 0x00000003 System.SByte System.Numerics.ConstantHelper::GetSByteWithAllBitsSet()
extern void ConstantHelper_GetSByteWithAllBitsSet_m9425F62C7CF3152B7F9D51264EB8522F129BE35C (void);
// 0x00000004 System.UInt16 System.Numerics.ConstantHelper::GetUInt16WithAllBitsSet()
extern void ConstantHelper_GetUInt16WithAllBitsSet_m706C3B69AAF97CC1CCB925B77CA5F366EADCCB9B (void);
// 0x00000005 System.Int16 System.Numerics.ConstantHelper::GetInt16WithAllBitsSet()
extern void ConstantHelper_GetInt16WithAllBitsSet_mFC4EE5413A19B7EEA9614870E1E9358F170E5283 (void);
// 0x00000006 System.UInt32 System.Numerics.ConstantHelper::GetUInt32WithAllBitsSet()
extern void ConstantHelper_GetUInt32WithAllBitsSet_m1291A24D83AE857F805D6131FD0869629BC1AD25 (void);
// 0x00000007 System.Int32 System.Numerics.ConstantHelper::GetInt32WithAllBitsSet()
extern void ConstantHelper_GetInt32WithAllBitsSet_m0D3B1E7B0FE29654A91FBD9D78A9246246205A89 (void);
// 0x00000008 System.UInt64 System.Numerics.ConstantHelper::GetUInt64WithAllBitsSet()
extern void ConstantHelper_GetUInt64WithAllBitsSet_m1720FAAB3D22565C69C7B243B202400924DDB233 (void);
// 0x00000009 System.Int64 System.Numerics.ConstantHelper::GetInt64WithAllBitsSet()
extern void ConstantHelper_GetInt64WithAllBitsSet_mA4A86C3A738C461AB41B08160E3525F999CB83C1 (void);
// 0x0000000A System.Single System.Numerics.ConstantHelper::GetSingleWithAllBitsSet()
extern void ConstantHelper_GetSingleWithAllBitsSet_mEEBFE9935E690DB26E7A5573D89919520C0361DC (void);
// 0x0000000B System.Double System.Numerics.ConstantHelper::GetDoubleWithAllBitsSet()
extern void ConstantHelper_GetDoubleWithAllBitsSet_mEADB78CE8EC99480C395268E657369666AB37A85 (void);
// 0x0000000C System.Void System.Numerics.JitIntrinsicAttribute::.ctor()
extern void JitIntrinsicAttribute__ctor_mE7224A25DE472FA4606849F3677A1CE586D2375B (void);
// 0x0000000D System.Int32 System.Numerics.Vector`1::get_Count()
// 0x0000000E System.Numerics.Vector`1<T> System.Numerics.Vector`1::get_Zero()
// 0x0000000F System.Int32 System.Numerics.Vector`1::InitializeCount()
// 0x00000010 System.Void System.Numerics.Vector`1::.ctor(T)
// 0x00000011 System.Void System.Numerics.Vector`1::.ctor(System.Void*)
// 0x00000012 System.Void System.Numerics.Vector`1::.ctor(System.Void*,System.Int32)
// 0x00000013 System.Void System.Numerics.Vector`1::.ctor(System.Numerics.Register&)
// 0x00000014 T System.Numerics.Vector`1::get_Item(System.Int32)
// 0x00000015 System.Boolean System.Numerics.Vector`1::Equals(System.Object)
// 0x00000016 System.Boolean System.Numerics.Vector`1::Equals(System.Numerics.Vector`1<T>)
// 0x00000017 System.Int32 System.Numerics.Vector`1::GetHashCode()
// 0x00000018 System.String System.Numerics.Vector`1::ToString()
// 0x00000019 System.String System.Numerics.Vector`1::ToString(System.String,System.IFormatProvider)
// 0x0000001A System.Numerics.Vector`1<T> System.Numerics.Vector`1::op_BitwiseOr(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x0000001B System.Boolean System.Numerics.Vector`1::op_Equality(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x0000001C System.Boolean System.Numerics.Vector`1::op_Inequality(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x0000001D System.Numerics.Vector`1<System.Byte> System.Numerics.Vector`1::op_Explicit(System.Numerics.Vector`1<T>)
// 0x0000001E System.Numerics.Vector`1<System.UInt64> System.Numerics.Vector`1::op_Explicit(System.Numerics.Vector`1<T>)
// 0x0000001F System.Numerics.Vector`1<T> System.Numerics.Vector`1::Equals(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x00000020 System.Numerics.Vector`1<T> System.Numerics.Vector`1::LessThan(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x00000021 System.Boolean System.Numerics.Vector`1::ScalarEquals(T,T)
// 0x00000022 System.Boolean System.Numerics.Vector`1::ScalarLessThan(T,T)
// 0x00000023 T System.Numerics.Vector`1::GetZeroValue()
// 0x00000024 T System.Numerics.Vector`1::GetOneValue()
// 0x00000025 T System.Numerics.Vector`1::GetAllBitsSetValue()
// 0x00000026 System.Void System.Numerics.Vector`1::.cctor()
// 0x00000027 System.Numerics.Vector`1<T> System.Numerics.Vector::Equals(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x00000028 System.Numerics.Vector`1<T> System.Numerics.Vector::LessThan(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x00000029 System.Boolean System.Numerics.Vector::get_IsHardwareAccelerated()
extern void Vector_get_IsHardwareAccelerated_m282E2F7F13A15EAEFC4059305F18867139398D74 (void);
// 0x0000002A System.Numerics.Vector`1<T> System.Numerics.Vector::BitwiseOr(System.Numerics.Vector`1<T>,System.Numerics.Vector`1<T>)
// 0x0000002B System.Numerics.Vector`1<System.Byte> System.Numerics.Vector::AsVectorByte(System.Numerics.Vector`1<T>)
// 0x0000002C System.Numerics.Vector`1<System.UInt64> System.Numerics.Vector::AsVectorUInt64(System.Numerics.Vector`1<T>)
// 0x0000002D System.Int32 System.Numerics.Hashing.HashHelpers::Combine(System.Int32,System.Int32)
extern void HashHelpers_Combine_mD58E949D140CF373068694215342D6D499325526 (void);
// 0x0000002E System.Void System.Numerics.Hashing.HashHelpers::.cctor()
extern void HashHelpers__cctor_m707776FE348C3F3E39AB59A3B10EA9FA1834DC89 (void);
static Il2CppMethodPointer s_methodPointers[46] = 
{
	SR_Format_mB69240C1ED63A6C8F368CF678CF2EFC426495DEF,
	ConstantHelper_GetByteWithAllBitsSet_m0576CC61FD3FF54167A6F677EAC509D474F24DA0,
	ConstantHelper_GetSByteWithAllBitsSet_m9425F62C7CF3152B7F9D51264EB8522F129BE35C,
	ConstantHelper_GetUInt16WithAllBitsSet_m706C3B69AAF97CC1CCB925B77CA5F366EADCCB9B,
	ConstantHelper_GetInt16WithAllBitsSet_mFC4EE5413A19B7EEA9614870E1E9358F170E5283,
	ConstantHelper_GetUInt32WithAllBitsSet_m1291A24D83AE857F805D6131FD0869629BC1AD25,
	ConstantHelper_GetInt32WithAllBitsSet_m0D3B1E7B0FE29654A91FBD9D78A9246246205A89,
	ConstantHelper_GetUInt64WithAllBitsSet_m1720FAAB3D22565C69C7B243B202400924DDB233,
	ConstantHelper_GetInt64WithAllBitsSet_mA4A86C3A738C461AB41B08160E3525F999CB83C1,
	ConstantHelper_GetSingleWithAllBitsSet_mEEBFE9935E690DB26E7A5573D89919520C0361DC,
	ConstantHelper_GetDoubleWithAllBitsSet_mEADB78CE8EC99480C395268E657369666AB37A85,
	JitIntrinsicAttribute__ctor_mE7224A25DE472FA4606849F3677A1CE586D2375B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Vector_get_IsHardwareAccelerated_m282E2F7F13A15EAEFC4059305F18867139398D74,
	NULL,
	NULL,
	NULL,
	HashHelpers_Combine_mD58E949D140CF373068694215342D6D499325526,
	HashHelpers__cctor_m707776FE348C3F3E39AB59A3B10EA9FA1834DC89,
};
static const int32_t s_InvokerIndices[46] = 
{
	3305,
	3869,
	3869,
	3855,
	3855,
	3856,
	3856,
	3857,
	3857,
	3871,
	3853,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3869,
	-1,
	-1,
	-1,
	3246,
	3876,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x02000006, { 0, 19 } },
	{ 0x06000027, { 19, 2 } },
	{ 0x06000028, { 21, 2 } },
	{ 0x0600002A, { 23, 2 } },
	{ 0x0600002B, { 25, 2 } },
	{ 0x0600002C, { 27, 2 } },
};
extern const uint32_t g_rgctx_Vector_1_tF7A45F0A2EF8BE6E926454C4CA5E0F4804A21DC3;
extern const uint32_t g_rgctx_T_t06D9E50A6E6D6D9F351D7C0EFCD541CE4B5F8472;
extern const uint32_t g_rgctx_T_t06D9E50A6E6D6D9F351D7C0EFCD541CE4B5F8472;
extern const uint32_t g_rgctx_Vector_1_get_Count_mE0946996CD7E479DA6FE7E4325FA2F0E4B01A4CB;
extern const uint32_t g_rgctx_Vector_1__ctor_m0ECFCCBEBDE0011A081BF1D562D4733F069236E0;
extern const uint32_t g_rgctx_Vector_1_tF7A45F0A2EF8BE6E926454C4CA5E0F4804A21DC3;
extern const uint32_t g_rgctx_Vector_1_Equals_mED01BE3CA4754EAF0573B3B754B541CE251939D3;
extern const uint32_t g_rgctx_Vector_1_get_Item_mCBB1340A98AA95C0A09D3C28BDD76BB3DBCDC124;
extern const uint32_t g_rgctx_Vector_1_ScalarEquals_m569D2314AE4C130D532667A4F8CCF1977AB3EEE7;
extern const uint32_t g_rgctx_Vector_1_ToString_m00C44F0CD051B516CB9D767081E8DFADF69AEDA7;
extern const uint32_t g_rgctx_Vector_1_op_Equality_mDA6DC44B09284D3485A5AF10FE041494933B99CE;
extern const uint32_t g_rgctx_Vector_1__ctor_m5429F988FA161649E3B779263858ADAB4B0D45FA;
extern const uint32_t g_rgctx_Vector_1__ctor_m01B42284B5AF2632000B1CE313A8B2FE93340AC2;
extern const uint32_t g_rgctx_Vector_1_ScalarLessThan_m3E556D08AA2C178EE8933B7239B4C064ACBA1627;
extern const uint32_t g_rgctx_Vector_1_InitializeCount_mAB664BBA0F49594FEE9ECEADD4FD79BDBDD143D5;
extern const uint32_t g_rgctx_Vector_1_GetZeroValue_mFA9C0E2E74164E65E45ECF4883ED52628722334B;
extern const uint32_t g_rgctx_Vector_1__ctor_m1EFBEEC5C409FAA32891E6033796F272299130F4;
extern const uint32_t g_rgctx_Vector_1_GetOneValue_m24AF0349A41FB1CECA3E22C21A2CA8B654740CBD;
extern const uint32_t g_rgctx_Vector_1_GetAllBitsSetValue_m0FF33B3AF15F7C7793DE33CE5EBAEF2F8E2379E2;
extern const uint32_t g_rgctx_Vector_1_Equals_m44997B8A9D19F2BC841C2107B598B07F37958CF5;
extern const uint32_t g_rgctx_Vector_1_tADEE21373E4B6864A660139E8A3982FB79E5EB98;
extern const uint32_t g_rgctx_Vector_1_LessThan_m52261962F4F3DFDA26E4B131FCADB3E4FB4C5B0A;
extern const uint32_t g_rgctx_Vector_1_t97856DCBC0CB91A6A692D98E7694731126063CD7;
extern const uint32_t g_rgctx_Vector_1_op_BitwiseOr_m7CE52122D03433EF1140C5B9525F1CA4B41025DF;
extern const uint32_t g_rgctx_Vector_1_t1B75B542EB06238A360C7189BD826645ABD6C964;
extern const uint32_t g_rgctx_Vector_1_op_Explicit_m42CD582FD6AED22545625AC49244A16EB459C536;
extern const uint32_t g_rgctx_Vector_1_tE59BBB5BC415ADCE892044DE3AFAD6D9E16513A9;
extern const uint32_t g_rgctx_Vector_1_op_Explicit_mBF6C258F6F140C99C363D33351DA3DB450288545;
extern const uint32_t g_rgctx_Vector_1_tB42EBB6972D7363F9EFCC46816871DD54FE91E55;
static const Il2CppRGCTXDefinition s_rgctxValues[29] = 
{
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_tF7A45F0A2EF8BE6E926454C4CA5E0F4804A21DC3 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t06D9E50A6E6D6D9F351D7C0EFCD541CE4B5F8472 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t06D9E50A6E6D6D9F351D7C0EFCD541CE4B5F8472 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_get_Count_mE0946996CD7E479DA6FE7E4325FA2F0E4B01A4CB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1__ctor_m0ECFCCBEBDE0011A081BF1D562D4733F069236E0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_tF7A45F0A2EF8BE6E926454C4CA5E0F4804A21DC3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_Equals_mED01BE3CA4754EAF0573B3B754B541CE251939D3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_get_Item_mCBB1340A98AA95C0A09D3C28BDD76BB3DBCDC124 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_ScalarEquals_m569D2314AE4C130D532667A4F8CCF1977AB3EEE7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_ToString_m00C44F0CD051B516CB9D767081E8DFADF69AEDA7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_op_Equality_mDA6DC44B09284D3485A5AF10FE041494933B99CE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1__ctor_m5429F988FA161649E3B779263858ADAB4B0D45FA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1__ctor_m01B42284B5AF2632000B1CE313A8B2FE93340AC2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_ScalarLessThan_m3E556D08AA2C178EE8933B7239B4C064ACBA1627 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_InitializeCount_mAB664BBA0F49594FEE9ECEADD4FD79BDBDD143D5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_GetZeroValue_mFA9C0E2E74164E65E45ECF4883ED52628722334B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1__ctor_m1EFBEEC5C409FAA32891E6033796F272299130F4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_GetOneValue_m24AF0349A41FB1CECA3E22C21A2CA8B654740CBD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_GetAllBitsSetValue_m0FF33B3AF15F7C7793DE33CE5EBAEF2F8E2379E2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_Equals_m44997B8A9D19F2BC841C2107B598B07F37958CF5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_tADEE21373E4B6864A660139E8A3982FB79E5EB98 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_LessThan_m52261962F4F3DFDA26E4B131FCADB3E4FB4C5B0A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_t97856DCBC0CB91A6A692D98E7694731126063CD7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_op_BitwiseOr_m7CE52122D03433EF1140C5B9525F1CA4B41025DF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_t1B75B542EB06238A360C7189BD826645ABD6C964 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_op_Explicit_m42CD582FD6AED22545625AC49244A16EB459C536 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_tE59BBB5BC415ADCE892044DE3AFAD6D9E16513A9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_op_Explicit_mBF6C258F6F140C99C363D33351DA3DB450288545 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Vector_1_tB42EBB6972D7363F9EFCC46816871DD54FE91E55 },
};
extern const CustomAttributesCacheGenerator g_System_Numerics_Vectors_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Numerics_Vectors_CodeGenModule;
const Il2CppCodeGenModule g_System_Numerics_Vectors_CodeGenModule = 
{
	"System.Numerics.Vectors.dll",
	46,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	29,
	s_rgctxValues,
	NULL,
	g_System_Numerics_Vectors_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
