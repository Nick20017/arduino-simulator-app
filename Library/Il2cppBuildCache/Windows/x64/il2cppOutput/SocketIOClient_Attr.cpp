﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyMetadataAttribute
struct AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CStartPingU3Eb__0U3Ed_t3655EEBE87BF783338EFD1D7513C2CA49967F18F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CStartPollingU3Eb__0U3Ed_tFD93EAF0E50A0A00BF9B2322A276C6E24654A267_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyMetadataAttribute
struct AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyMetadataAttribute::m_key
	String_t* ___m_key_0;
	// System.String System.Reflection.AssemblyMetadataAttribute::m_value
	String_t* ___m_value_1;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_key_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F, ___m_value_1)); }
	inline String_t* get_m_value_1() const { return ___m_value_1; }
	inline String_t** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(String_t* value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_value_1), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyMetadataAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529 (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * __this, String_t* ___key0, String_t* ___value1, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
static void SocketIOClient_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F * tmp = (AssemblyMetadataAttribute_tC11B256960EB0C004D873A8822B5F84C5590455F *)cache->attributes[0];
		AssemblyMetadataAttribute__ctor_m5A4B63DCD1CCB566C877C0CFCD9FE0BBDD249529(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x70\x6F\x73\x69\x74\x6F\x72\x79\x55\x72\x6C"), il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x67\x69\x74\x68\x75\x62\x2E\x63\x6F\x6D\x2F\x64\x6F\x67\x68\x61\x70\x70\x79\x2F\x73\x6F\x63\x6B\x65\x74\x2E\x69\x6F\x2D\x63\x6C\x69\x65\x6E\x74\x2D\x63\x73\x68\x61\x72\x70"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[1];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x63\x6B\x65\x74\x49\x4F\x43\x6C\x69\x65\x6E\x74"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[2];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x30\x2E\x33"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[3];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x30\x2E\x33\x2E\x30"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[4];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x73\x6F\x63\x6B\x65\x74\x2E\x69\x6F\x2D\x63\x6C\x69\x65\x6E\x74\x20\x69\x6D\x70\x6C\x65\x6D\x65\x6E\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[5];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x65\x61\x73\x65"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x6F\x57\x6F\x6E\x67"), NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[7];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[8];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[9];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[10];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[11];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x63\x6B\x65\x74\x49\x4F\x43\x6C\x69\x65\x6E\x74"), NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CRouterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CConnectedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CAttemptsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CJsonSerializerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CHttpClientU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CClientWebSocketProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnConnected(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnDisconnected(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnected(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnectAttempt(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnectError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnectFailed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnPing(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnPong(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Router_mDD45FFAA3E2375AB1453CF1FEF1EBAFF04EC2C36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Router_m0FAF923817200C24A0C11D52916D305FC4E83598(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Id_m4A76C984F9DEDCFA1581EA13521476502407A191(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Namespace_mFF9357291747A0DA8E839E9A1881FA6B88AF508D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Namespace_m6B8326534BD2D5A7D8DDC39C9BEE34C1150A6AC0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Connected_m7C107DF1177FAC56963CD4763F19ABAA1B996822(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Connected_m0AFE2343FD42A779FCBC37DCE77182F584A844DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Attempts_m1A54D69B54AFB5EB2FDF8767B89C0EA67D18354A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Attempts_mD2C1453050CA459794420DC171F3387A9C81DAD0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Options_m86CB03192102E0C6879AD39377BC57B6A0221A8B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_JsonSerializer_m3C41A32E2B80151E68566937F30408EAA2E1CEEA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_JsonSerializer_mD1D7C846218A09DA9DC4B74DAEC5B7BC340EA255(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_HttpClient_m4C50BBCEFF741BDC01B3A2466285A22FE428E759(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_HttpClient_mB42A5639BCEC711DF82157F1D8B88D9E239A8490(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_ClientWebSocketProvider_mA66C593DEA05F6004930AF3DBB2617FE487D87F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_ClientWebSocketProvider_m3F2082D8E14113B6EB79A3EAE98885F2DA4F9053(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_add_OnConnected_m8C3D5DFB18C2DCC42BFC0E31D50D27C1B942BA39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_remove_OnConnected_m60DBEBC0A3CB61B6D7E0435E29BD2A7F59925F2F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_add_OnError_mE4F5A0D01D36956BF4F84F71EA89E83B5B0FE76C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_remove_OnError_mE622D17481DE041B0A2CB152F83FEC8BB1EE9A08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_add_OnDisconnected_mB2910F22013D2B8805CD34D14EDA1DD741BDB7A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_remove_OnDisconnected_mED2BB005D80B81241C1F5348C8309908AE13E338(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_ConnectAsync_mF024D2306601A545290846FAD9CF9A6787FCE7FD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_0_0_0_var), NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_m671FB6EADF9481835AD0C81F90F47D49955305C8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_0_0_0_var), NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_m671FB6EADF9481835AD0C81F90F47D49955305C8____data1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_mB0F21924EB56E3822698F52CFFDEF79A97D355E1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_0_0_0_var), NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_mB0F21924EB56E3822698F52CFFDEF79A97D355E1____data2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_InvokeDisconnect_m52F6E016A6DEB7F701A1E6D162EF4D6F38A2F43B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tD4DDE161A30EF615D1C97EEAFBA52DC512CD0D07_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__81_SetStateMachine_m1ECCE93BD645592AA838CC0DAA79961A910639F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_CustomAttributesCacheGenerator_U3CEmitAsyncU3Ed__101_SetStateMachine_m82FDB3356A487BDFBB32BE08AC8B04DA1A1C1C67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_CustomAttributesCacheGenerator_U3CEmitAsyncU3Ed__102_SetStateMachine_mFE7510E343DBC01A0B53EBC8EED3EF255919C940(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_CustomAttributesCacheGenerator_U3CInvokeDisconnectU3Ed__105_SetStateMachine_m04BF0AC0D285F30E692F1EB9064013F6F8D34ECA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CConnectionTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CQueryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionDelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionDelayMaxU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionAttemptsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CExtraHeadersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CTransportU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CEIOU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Path_m1FBC6F3D54F28D6AEFAF8BC34CE8AB83521BFD22(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_Path_mF10633E51D73A179E9C13DE98858877C01880F5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ConnectionTimeout_m42D3C8D3D6D02020137D14416430ED2A72A751F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ConnectionTimeout_mCBB08504FAD849BC46274DE73825CB22D5B8CD00(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Query_mFA11FB20F1BF0F85E248DBE76C1E32D3FB98FAA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Reconnection_mD04D1F2BF70F0AD7530BB503EE5107B63B8C7E05(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_Reconnection_mB136EA9D898F774248A5A98CB955F984FE6D7F83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ReconnectionDelay_m37AECC4F9CB91B1C1F54502B7D418E8D1947DF11(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ReconnectionDelay_m82D0995BD1EEC7BE8B4638E922DAA79AD7814A39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ReconnectionDelayMax_m72FFED7A33E94DAA89D19BA954A5731580C4B47F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ReconnectionDelayMax_mE4BEC23B941B5F7E4489B81DEBF91BD320D46387(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ReconnectionAttempts_m475F99729FA0F7C0CE98D92E1586AF21F5C02A3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ReconnectionAttempts_m57223BAF3247069041AC79EBD52170484CE29F7C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ExtraHeaders_mB57D5270503BC51BFF3B6B5FC32A433CAE43ACA7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Transport_m9FA7D6F68B7B38E11B0354A29E6521C654F83B02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_Transport_m04CEA24437BE96E7A5452F26A8A6015A66CB0884(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_EIO_mD417CA97149DFF2BE4A2AB3ABE8E0CAC43C79AA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_EIO_mE6719F4103690097238584098D0E24CDCF0699A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_U3CInComingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_U3CSocketIOU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_U3CPacketIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_SocketIOResponse_get_InComingBytes_mD5D41E0AC1EA34453CE9DF5A373CE1BB4C31971E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_SocketIOResponse_get_SocketIO_m5B71B6FA3F2A4D38B42813E966DFAF116E7FB08F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_SocketIOResponse_set_PacketId_mA5D00FA08CA5BE3CFC7AFBD2F974D84476AF9495(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_U3CConfigOptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_get_ConfigOptions_m0FFE3A15E07BC02B600221B1436F198C3C469AD1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_CloseAsync_m9E183F55F9C327EB8835C7C5EF11775EFD2EDF81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_0_0_0_var), NULL);
	}
}
static void DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_ConnectAsync_m64C51C977FF8A51221EA2562864A63D000BBCD13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_0_0_0_var), NULL);
	}
}
static void DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_ReceiveAsync_m648B2AACE8C9D93EF572D52706821146B5955069(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_0_0_0_var), NULL);
	}
}
static void DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_SendAsync_m6A1809AC61900AB4A24437BD226FEEEFAEFD89B8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_0_0_0_var), NULL);
	}
}
static void U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_CustomAttributesCacheGenerator_U3CCloseAsyncU3Ed__9_SetStateMachine_mD148CE0C89031C23EA867D2FF34BB81F6FBB8984(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__10_SetStateMachine_m437EFC3CEDEB097994C3197EFE243E90FB0F0E53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_CustomAttributesCacheGenerator_U3CReceiveAsyncU3Ed__11_SetStateMachine_m66A5AF498F96EFF912632912CD93F2D36043A027(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__12_SetStateMachine_m64CD752CE62EA1350A8057CFA24ADAAF7C3C8FD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_U3COnTextReceivedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_U3COnBinaryReceivedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_get_OnTextReceived_m15CF89073B7DA2790586FAE9400128815DCAC7C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_set_OnTextReceived_mAEA01E64C138127591AAAD75A2DBF1AEBE3314FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_get_OnBinaryReceived_mB99F51EE3345D6202EF7337B52FB40A2D7FF5D41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_set_OnBinaryReceived_m965D579FDA010E4F69943F9C9EB3335AE4B2A6C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_GetAsync_m0EBB5C5F7C47931030A70A6DB24EF1B269EF68F1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_0_0_0_var), NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_SendAsync_m2C7673A37302B7AEE79CD307BF0CFF3B1EA95508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_0_0_0_var), NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_PostAsync_mBACF830DEAC2474E755F9B9ACC46FC5D8D7C526A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_0_0_0_var), NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_PostAsync_m3D2C65490C2C391161ED0E7D320E3CC8EBF33AA7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_0_0_0_var), NULL);
	}
}
static void HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_ProduceMessageAsync_m97A6135E853A5F2A3BE2A3EB439C2087A7DDD42E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_0_0_0_var), NULL);
	}
}
static void U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_CustomAttributesCacheGenerator_U3CGetAsyncU3Ed__12_SetStateMachine_mCF888DD1BB02D2EB5E362AC40CEE07D81E519E7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__13_SetStateMachine_m024A5A18084436D890DAB736AE15C8572355A482(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_CustomAttributesCacheGenerator_U3CPostAsyncU3Ed__14_SetStateMachine_m3BE227D571DCB3C1149853D5363EDFB3012D06D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t0B9AFEE0D6310AB713FEDF5943F446D0B51F2807_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_CustomAttributesCacheGenerator_U3CPostAsyncU3Ed__15_SetStateMachine_mC519448B2884FD4C0A877B254582828AC538BCB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_CustomAttributesCacheGenerator_U3CProduceMessageAsyncU3Ed__17_SetStateMachine_m85ADDF619E98B828A3AD77EFE309E8EE0E6DCC5A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CServerUriU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CEIOU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CUriConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3COnMessageReceivedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3COnTransportClosedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_ServerUri_m76DD5F6D1F3CE15C77E08B5F3E14184DC5889764(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_ServerUri_mEE683013DBB0F35BCFEC5DBE625DDF099F8A878D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_Namespace_m1EFE88CA37B3F58535288CA4E2F8464310056B30(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_Namespace_mD055A954C5FF6298D49A8143EFB7EA93AB90E4F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_Protocol_m8138CD8D64707ABDAB1EA4527E10622E496A3A04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_Protocol_mD60882D5BF23AAF4C1E38A15BCD14174824517E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_EIO_mC86D14482EB4E63F3802B103188DC42F1A96B677(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_EIO_mE9713AC8626966E10E044F0646C307B1613CC500(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_UriConverter_mBB3506ECF9A1916623F14F8E3793E0A195012B5D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_UriConverter_m2941F574F55685D9365E939FF0B4FFD687E973D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_OnMessageReceived_mD238146DE5B5BA3A69FBEF3D45901A4CDFAFF44E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_OnMessageReceived_m0CAFAEC2DE52000DEE5CA7A96F73246E3CFA69F1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_OnTransportClosed_mB5E39DCA53D6C88CD8D4BA0E7A241B3256AB5D7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_OnTransportClosed_mB4D7DE54D0F47B81D7C6E6B783A317C004EEB876(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_ConnectAsync_m2427AB13C13D5667E53D2BC80D9BF53B0FF98034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_ConnectByWebsocketAsync_m5E61A3B5E272E5F83206A8D021E002D31E7C1D82(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_ConnectByPollingAsync_mE44BDC34D0F4CBFCCF74F16237B66A1292A9D650(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_OnOpened_mF81F97FDD32B87BA06CAB553E4592F5A17361B67(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_OnTextReceived_mCA059D8F291380A10E94F37F81FE437A8C26FF28(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_SendAsync_m6CCD8FE5CD3547DD657D25B0B9E90CA78B76640F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_DisconnectAsync_m5F372E1D46A2C1283A015AC6130E80D78E783DB4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_SendAsync_m5370779C7885AB5A5842E53DB896F3F2D26241B6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_0_0_0_var), NULL);
	}
}
static void TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_SendAsync_mD1B98253A50DF7675ABE1A5D29419B081294B4C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_0_0_0_var), NULL);
	}
}
static void U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__41_SetStateMachine_mABAF1145E76B3C806CD1DC24224EE718C0FE4526(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_CustomAttributesCacheGenerator_U3CConnectByWebsocketAsyncU3Ed__42_SetStateMachine_m3294DBB8241985C904860D8C910296D0AE1EF4BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_CustomAttributesCacheGenerator_U3CConnectByPollingAsyncU3Ed__43_SetStateMachine_m37FECED5E2C8F12E3FC6308915571A7789E48339(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass44_0_t91DAD191A5043EBA3AEF013DCCD3BF13F7FAFA10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass44_0_t91DAD191A5043EBA3AEF013DCCD3BF13F7FAFA10_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass44_0_U3CStartPollingU3Eb__0_mF8DE4F95BFD4770AA2743B6041AB854968ECFBC8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CStartPollingU3Eb__0U3Ed_tFD93EAF0E50A0A00BF9B2322A276C6E24654A267_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CStartPollingU3Eb__0U3Ed_tFD93EAF0E50A0A00BF9B2322A276C6E24654A267_0_0_0_var), NULL);
	}
}
static void U3CU3CStartPollingU3Eb__0U3Ed_tFD93EAF0E50A0A00BF9B2322A276C6E24654A267_CustomAttributesCacheGenerator_U3CU3CStartPollingU3Eb__0U3Ed_SetStateMachine_mE5328B25B4712C524080DCAE323D9FD3C4263794(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass45_0_t592C4689D5D4768EF662780E4A6AA5E849A4A1B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass45_0_t592C4689D5D4768EF662780E4A6AA5E849A4A1B5_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass45_0_U3CStartPingU3Eb__0_mC8876E143A7683FADD043024F9F92910936F1D6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CStartPingU3Eb__0U3Ed_t3655EEBE87BF783338EFD1D7513C2CA49967F18F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CStartPingU3Eb__0U3Ed_t3655EEBE87BF783338EFD1D7513C2CA49967F18F_0_0_0_var), NULL);
	}
}
static void U3CU3CStartPingU3Eb__0U3Ed_t3655EEBE87BF783338EFD1D7513C2CA49967F18F_CustomAttributesCacheGenerator_U3CU3CStartPingU3Eb__0U3Ed_SetStateMachine_mE910C8499A852C5A9AE83B20C285F1B5226A63A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_CustomAttributesCacheGenerator_U3COnOpenedU3Ed__46_SetStateMachine_mAED54523AB8B9D9861736CCEA7A47D1EE7022413(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_CustomAttributesCacheGenerator_U3COnTextReceivedU3Ed__47_SetStateMachine_m75171F6AEA5BEBA0568157A3D74819D9786CEE73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__50_SetStateMachine_mECC493E4D4C5433A4B2620F29E2BF856DC2FAFFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_CustomAttributesCacheGenerator_U3CDisconnectAsyncU3Ed__51_SetStateMachine_mE795A2BF18086096E7B38E6CADA5BDBFBFC96D30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__52_SetStateMachine_mC80CFA8D89B2E8846F2F18BBC81A9ED45370A481(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__53_SetStateMachine_mC93F378EE76C870A8B0DDD34A803ADE568CBAB48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CReceiveChunkSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CSendChunkSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CConnectionTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CReceiveWaitU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3COnTextReceivedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3COnBinaryReceivedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3COnAbortedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_ReceiveChunkSize_m7423C6ABF999816047CBBE5091421C083583BB53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_ReceiveChunkSize_m4039BD765C6687FA791AA26D54BAA52BD8D4B39E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_SendChunkSize_mF03112851A9A4B9EC8B53FA9DCB4C01177AC138E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_SendChunkSize_mDF22C5AA6380D76D2C03F8F5B1BE613F56657F7C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_ConnectionTimeout_m805685CAF6A21DAA5B4581E9115BF5DF7A638A6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_ConnectionTimeout_m9B7C2A72B06C3EB53EFF499EF49D757505461D99(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_ReceiveWait_mD2A1F60C8F17900ACF6B9DC77B2398894772876A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_OnTextReceived_m80A4849545A1B10B03397447C624C2B69B603859(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_OnTextReceived_mDA981BF42B62CB4AE52C3011F996031E36EFA51A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_OnBinaryReceived_m12078E3CE2369D226DB519AB8EC7858D912A7308(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_OnBinaryReceived_mD8C6635BB91FD7D6158BA90281356FF2BEA0B260(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_OnAborted_mF078CCDAAF48AE1F1E12275CF8E213756FF093FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_OnAborted_mBB91B48B2DF92743FCCA2255932BE9D67E55AD81(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_ConnectAsync_mDD02270F7B94ED91A84ADAE0775BA32698BE7EDF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_0_0_0_var), NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_SendAsync_mCBCA4E1F380CA76309F844B9D7BD356B37E6B636(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_0_0_0_var), NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_SendAsync_mE057E9C97004675A5303352DB450032FDDBA19D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_0_0_0_var), NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_SendAsync_mECEA08E3E7437DAD3170C2BCA9C51AD5F07EF349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_0_0_0_var), NULL);
	}
}
static void WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_ListenAsync_mB0C9B98540C9A632DEA0267D818FD1EEA03F6E7B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_0_0_0_var), NULL);
	}
}
static void U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__32_SetStateMachine_mC9095D635C19D612D42B3374031B330C1DAE74A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__34_SetStateMachine_mFB8DA5CD9392EE2E24757CB72F3F922B3843E502(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__35_SetStateMachine_m03876D9EA87154470B66213C6C917DA9288593B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__36_SetStateMachine_m4A84217F6A869CA3E24C7909B818F4B37FA67C7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_CustomAttributesCacheGenerator_U3CListenAsyncU3Ed__37_SetStateMachine_mCB6D898F64A30EBAFEE9F5D18346C8C403B85460(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Namespace_mAA8F0EDECFAF3E10B0DBECF08F653C1BFDBE8C39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Namespace_m646059378AD2E597BEF2CD530131C6A3FB7C5367(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Event_mA5C2557061343D628095E49FC95B01F89B76A3A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Event_m8029DA632FCD8CE8FA190756ED4B6C0EEA6DEF0A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Id_m97035E0F4A2949EA52F3F073D456BF9488D29E33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Id_m22A1C8842F872CAC13EC07D4D7BE5D5F03A11555(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_JsonElements_m22A4CAD588EB7F51469D0EC607EC1517FB9242B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_JsonElements_mE1BEFAD769DD0E5A18F7D4A792A8BBE6A308B944(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Json_m7BDB54A72D4B384A282EEFEE3FC20ADD8E386006(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Json_mD9768CB381F44A08DF584D94BABC0F2AE54845E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_BinaryCount_mCAD5E1F353E75D1CD65F7E56171D7308456098C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_BinaryCount_mFB6806B814207CD244A17A422839451BA3EEE00D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Eio_mA5F0D55BB0AA5B36FC12DC93555DBB37E61BC798(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Protocol_m7D775B3886C401B5485E9E00C0B1588FD37D53DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_OutgoingBytes_m9AF39913F8652D5805D6DF76A6BB5DDA94865B49(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_OutgoingBytes_m0AEF3EF6974784DB66FF5328C4810A0C4FBDBEAB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_IncomingBytes_m6176E430798B6B981ACF0EDE3E8B46186906F8CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_IncomingBytes_m1092FBA649646F7815529C8BAC29791429906284(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Namespace_m63EC8D1F5F4A3FD033A21A9A7736E1BBCB8E6BB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Namespace_m0E308A032CFAE0C1E9A7AECEFEA8F3122B1EB3E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Event_m4E1029DC9E74D4791827C070DF1BE7229E74564C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_JsonElements_m4EBE99C0156E918C12946B8801BBF4532E13F2BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_JsonElements_m3A410CA3471CD989D75DFFAB35A79858A5953A88(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Json_m1D2D03D72410C66DA620FA77B772E0661D4EADFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Id_m7B65941B3858ACCB71050DB455C09B83710CD4B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Id_m552EB2A26BC0AE3A67177B0D3DBE174F690AF824(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_OutgoingBytes_m17FAB47D528E3B2C40E407B974F77F121E5902B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_IncomingBytes_mD5D10FF092216467AC85772D7DF2C89158499DAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_IncomingBytes_m61174B457A26958E141D260B3BF1508A146BD976(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_BinaryCount_mCB5AE12314E9677CA289F008B341891041778DAE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Eio_mB675DDC19D4BC87F0983349E6C1DBF3E84FBC843(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Protocol_mE2EC5B7E221B3493A7063810BBBD538FFF724069(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Namespace_mF3061BBC4D59B4A4647697ACBC0CC30000E5A559(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Namespace_mC27A209BE8B51508A386C97A6CAA58EE2F695D53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Event_m51C6D5CD0A4A594CD8485498498B23F344D71EC9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_JsonElements_m89379F5E3191C45DD20BEC5B41486663A8247FE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_JsonElements_mAF9ADD78439515C24E55DF9D654A69BA97F51BB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Json_mC12CDDE2EC5E404E8869AA0C4B7A2B5705B0A468(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Id_m5FDB4ECB225E66B9A372CE532ED6A0573DF2B626(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Id_mBAD409A433D3E03028AB31C3B7D6946D1B853F4D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_BinaryCount_m7FAC63EADBBE1A1119FA3D02AA8413EA9D38B7A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_BinaryCount_m8DE802695E94A4DC67C1EDED8A58E68443432575(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Eio_mC80168F7A70621DA9819ABD2A3BD062812BA3FA6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Protocol_mDCC45C47884C9084000C140F6543A0187DAF426E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_OutgoingBytes_mB1E8E08B93D7E4D6255E215C0DB3B72D6B692A06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_IncomingBytes_mA30B44355FB52052C70AC0987A2BA0C7AA2B96BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_IncomingBytes_mE598B123B5E9B1A32DB1E75DB2EDA6647AB3159C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CSidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CQueryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Namespace_mBB9748CE4C8C74F0C4A5D149A7BC761709665E95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Namespace_mA7FDE6E95A56A0C4DDCDC02127D102B482D3D6FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Sid_m4D121694E0C88D04E5A99433C953CE1585B2BF29(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Sid_m80379E0AC6A06BB24F3FEC26BB9563C8CD59E93C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_OutgoingBytes_mFAC5CCE676B708DFE7EFCA8B6845E0B55CA34704(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_IncomingBytes_mCE4223CDA74C273D269F1E6D04C6B3E5BCDB108F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_IncomingBytes_mB3582A4D805FFB5E600AAE3E113534B5C2BA28E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_BinaryCount_m1C3D0F041C7BCE37A2007B2CAFD89FE4D5726946(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Eio_mE24F9D9F65069643E7A512BB0611448FE892F0C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Eio_mCA4D23CABE3F03CA5F2D48A4929155903607621E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Protocol_m05896E4D86A5D4F7D4DBFFC6CFF73482322B0654(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Query_m45D8A90C909B5954183B9AACBBFD2908F17C1692(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Query_mDDE08088D7D65E1896B3DDF467CF66D4CBF9C1B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_Namespace_mBC654DF14D562AACD4678E23EFF3A5AC69640D59(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_Namespace_m137124B9A6AD089CD1819267FC45348711C220C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_OutgoingBytes_m4B4FE93340DDDF8301A0A6CB5ED17C8C5470ED17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_IncomingBytes_mC6CF82CBBD5D15443DE29F95E76844F09CE831AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_IncomingBytes_m7BBCB930B5C7FD1A9EA060E32B6FF5F54E1CC1C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_BinaryCount_m87D1D38C6DBAC669C1714C9E08CC3E3D020F7C04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_Eio_m7D809F9D4C4F6B3DA317012FD4915F95113F3F90(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_Protocol_mAC0B93C5BC99FDCCA8917275B3FAD679CB40C0EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_Message_m2FA4EC941DBB4D885911E9B6A08206CCC6EAE05C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Message_m1C676E1D517B0B82E65977F394E66A0C23BDA2FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Namespace_m1621D5622A176D55983D56ACBF3BDF9225391D43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_OutgoingBytes_mB7064FB185F60EF9688241881E7A548EA4FEE8B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_IncomingBytes_mD9E5D721243093414A3BC2A673642D1D3F3930FB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_IncomingBytes_mF36F79779BC64A1B7AE435DFFA050428A35F689A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_BinaryCount_mD3A9C112BB333B628E3F160DCD82B651971A9DC3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_Eio_mEA9E313693DB15A377C002395ECFA1AF273BC29E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Eio_m9A26978230A37C6746E2EFA8BD7CBD28E45B68E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Protocol_mD3210142031FD98E9A9080037014A9DC9AD2D325(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Namespace_mA2473D71D774C869AB286DFA44A5BFAD6AAE3D97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Namespace_m262295490C730CAC637DF2A0ED3E5222171253D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Event_m1B6110DACCD1175CCA546189CA5BC6D31B852E99(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Event_mFD9DA13A75C0D0C1F4926558FA4D78F808F76C39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Id_m67179AADC6C01AE12374E1940C861770727F9ED5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Id_m806A84CA037CCBD53F96C5C31632AA70F152C1E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_JsonElements_m5F76EDD056F8AFD32C049EF08816ACAE644F5017(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_JsonElements_mF40A41BCE4C89E3EA46B74519DE5EE213355D03D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Json_mCDB73487936C56C04AE0AAA0855B57A25F68BF41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Json_m4386F4B466346D2538B8AF0696D729521C9D00D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_OutgoingBytes_mEE2B105F54ECACF5F024B33B6DE55FBB8B4A2C05(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_IncomingBytes_mA4330EBE622AF968FD4C3863787567891BADA0FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_IncomingBytes_m3A6B20C6F5C764CE1974A6DB4C6E8C2EF5D5AE34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_BinaryCount_m77F298305163FA7315EEFEF117D8A9FBEF798B06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Eio_m695EDB9FFB1E5724ED628F6827E2E9D60DA40943(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Protocol_m714D79DD63FECC93AC5CF26366C0FCD3CC61845D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CSidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CUpgradesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CPingIntervalU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CPingTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_Sid_m1C2DFD9ABF2841ECC72816888C39F53B90A123BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Sid_m91D157ACA4F17CE6F04F912C5E6576EFA7ED1E3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_Upgrades_mF713B2623F775FA9E340303DB033C263E826C9C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Upgrades_m57F2B66A6F46EFAEFE0DB04833C058834D058076(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_PingInterval_m6BAFB5A01580F69D4BDFABBBB2453F38DC6B0774(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_PingInterval_m6F90759D62F4B9F106213211CBEB729BD77516FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_PingTimeout_mA06D177C1911170D70B6C871074BB8751A257F1C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_OutgoingBytes_m84828C6286305F9254089FE27036FD61DED71E07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_IncomingBytes_mEBB3DA727A3E6E1B5102B2BF2C79839E5452A6E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_IncomingBytes_m55D0D0FBA7764E86BFDDD74A6B5DB3642258EC4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_BinaryCount_m3B7FEAE705FFBE0FB050E7DD20FF4173D0BDD811(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Eio_m27947BFE1007703F0AAF867D632D461E99252612(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Protocol_m896FE4E58F36070B60C381736E34B8385D79BD56(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_get_OutgoingBytes_mA33B556D75B63546FE1680B93906C4DDACC16F54(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_get_IncomingBytes_mAB4CB776A307E0C1627ECC173704ACB7DEB005CB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_set_IncomingBytes_m02799D45BFCC060407A5DADA8EE815B3BDB7E597(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_get_BinaryCount_m5A1C66215FB119E85F93E4F59D34C1DAD2F6CD33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_set_Eio_mB6C5A7B749780F1CB30A671BCE98CC7BF033A26A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_set_Protocol_mE38DF0C7211E1389CE1D252151F33FD54965404F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_OutgoingBytes_m92CBE117D20EFD12A4A8674566CE185D76B16690(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_IncomingBytes_mC2E61D002A47797E8379978F60B8FA39EB89E072(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_IncomingBytes_m9B6B09A1AA7AB28699B3C0AA5E1EFC4A5973711D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_BinaryCount_m8568D4D84A7BC394DDE4CDA945DCB1A1BB9B1695(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_Eio_m5ADF0E510ECCBBFBF803155E9392544C5D8F9CEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_Protocol_m19FE10462749E7A401EAA6FFAD950ACB492E6C7F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_Duration_m678F36A9284497BF1D8B738AE321D6C0EEEBA9F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_Duration_m3F8BA41944161F752771D65A9AD307460068B6F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ByteArrayConverter_t7BDBFFE466A0967463D8CF39235FF35793951D07_CustomAttributesCacheGenerator_U3CBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ByteArrayConverter_t7BDBFFE466A0967463D8CF39235FF35793951D07_CustomAttributesCacheGenerator_ByteArrayConverter_get_Bytes_mCF080CE5AFFFCB4649F40F3E469A297420BAFC28(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_U3CBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_get_Json_mB0DB437258A308B7C3631B24D12457F813CE5000(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_set_Json_m00057D5060370A218B3659E161AB15FF0E0AFAC4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_get_Bytes_mB4255CBA3832D483011FE9A4462BDCE1FE65D052(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_set_Bytes_mB09A845EA53D94853444F111F8A114E041FA0602(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SystemTextJsonSerializer_tE4204226053CF0444C730ADB6BE6E5F8D8EF3853_CustomAttributesCacheGenerator_U3COptionsProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SystemTextJsonSerializer_tE4204226053CF0444C730ADB6BE6E5F8D8EF3853_CustomAttributesCacheGenerator_SystemTextJsonSerializer_CreateOptions_m284D599E8AA27E39CBDB33800DCF85488E6DFC61(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x4F\x70\x74\x69\x6F\x6E\x73\x50\x72\x6F\x76\x69\x64\x65\x72\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void SystemTextJsonSerializer_tE4204226053CF0444C730ADB6BE6E5F8D8EF3853_CustomAttributesCacheGenerator_SystemTextJsonSerializer_get_OptionsProvider_m78ECE00826C3253812E11D9BBBE1CB0374896AD5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_SocketIOClient_AttributeGenerators[];
const CustomAttributesCacheGenerator g_SocketIOClient_AttributeGenerators[429] = 
{
	U3CU3Ec_tD4DDE161A30EF615D1C97EEAFBA52DC512CD0D07_CustomAttributesCacheGenerator,
	U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_CustomAttributesCacheGenerator,
	U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_CustomAttributesCacheGenerator,
	U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_CustomAttributesCacheGenerator,
	U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_CustomAttributesCacheGenerator,
	U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_CustomAttributesCacheGenerator,
	U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_CustomAttributesCacheGenerator,
	U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_CustomAttributesCacheGenerator,
	U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_CustomAttributesCacheGenerator,
	U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_CustomAttributesCacheGenerator,
	U3CU3Ec_t0B9AFEE0D6310AB713FEDF5943F446D0B51F2807_CustomAttributesCacheGenerator,
	U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_CustomAttributesCacheGenerator,
	U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_CustomAttributesCacheGenerator,
	U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_CustomAttributesCacheGenerator,
	U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_CustomAttributesCacheGenerator,
	U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass44_0_t91DAD191A5043EBA3AEF013DCCD3BF13F7FAFA10_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass45_0_t592C4689D5D4768EF662780E4A6AA5E849A4A1B5_CustomAttributesCacheGenerator,
	U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_CustomAttributesCacheGenerator,
	U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_CustomAttributesCacheGenerator,
	U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_CustomAttributesCacheGenerator,
	U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_CustomAttributesCacheGenerator,
	U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_CustomAttributesCacheGenerator,
	U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_CustomAttributesCacheGenerator,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CRouterU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CConnectedU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CAttemptsU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CJsonSerializerU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CHttpClientU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_U3CClientWebSocketProviderU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnConnected,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnError,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnDisconnected,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnected,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnectAttempt,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnectError,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnReconnectFailed,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnPing,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_OnPong,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CConnectionTimeoutU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CQueryU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionDelayU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionDelayMaxU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CReconnectionAttemptsU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CExtraHeadersU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CTransportU3Ek__BackingField,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_U3CEIOU3Ek__BackingField,
	SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_U3CInComingBytesU3Ek__BackingField,
	SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_U3CSocketIOU3Ek__BackingField,
	SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_U3CPacketIdU3Ek__BackingField,
	DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_U3CConfigOptionsU3Ek__BackingField,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_U3COnTextReceivedU3Ek__BackingField,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_U3COnBinaryReceivedU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CServerUriU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CEIOU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3CUriConverterU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3COnMessageReceivedU3Ek__BackingField,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_U3COnTransportClosedU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CReceiveChunkSizeU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CSendChunkSizeU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CConnectionTimeoutU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3CReceiveWaitU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3COnTextReceivedU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3COnBinaryReceivedU3Ek__BackingField,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_U3COnAbortedU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CSidU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_U3CQueryU3Ek__BackingField,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CMessageU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CEventU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CJsonElementsU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CSidU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CNamespaceU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CUpgradesU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CPingIntervalU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CPingTimeoutU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3COutgoingBytesU3Ek__BackingField,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CIncomingBytesU3Ek__BackingField,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CBinaryCountU3Ek__BackingField,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CEioU3Ek__BackingField,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CProtocolU3Ek__BackingField,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_U3CDurationU3Ek__BackingField,
	ByteArrayConverter_t7BDBFFE466A0967463D8CF39235FF35793951D07_CustomAttributesCacheGenerator_U3CBytesU3Ek__BackingField,
	JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_U3CJsonU3Ek__BackingField,
	JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_U3CBytesU3Ek__BackingField,
	SystemTextJsonSerializer_tE4204226053CF0444C730ADB6BE6E5F8D8EF3853_CustomAttributesCacheGenerator_U3COptionsProviderU3Ek__BackingField,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Router_mDD45FFAA3E2375AB1453CF1FEF1EBAFF04EC2C36,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Router_m0FAF923817200C24A0C11D52916D305FC4E83598,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Id_m4A76C984F9DEDCFA1581EA13521476502407A191,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Namespace_mFF9357291747A0DA8E839E9A1881FA6B88AF508D,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Namespace_m6B8326534BD2D5A7D8DDC39C9BEE34C1150A6AC0,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Connected_m7C107DF1177FAC56963CD4763F19ABAA1B996822,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Connected_m0AFE2343FD42A779FCBC37DCE77182F584A844DC,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Attempts_m1A54D69B54AFB5EB2FDF8767B89C0EA67D18354A,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_Attempts_mD2C1453050CA459794420DC171F3387A9C81DAD0,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_Options_m86CB03192102E0C6879AD39377BC57B6A0221A8B,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_JsonSerializer_m3C41A32E2B80151E68566937F30408EAA2E1CEEA,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_JsonSerializer_mD1D7C846218A09DA9DC4B74DAEC5B7BC340EA255,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_HttpClient_m4C50BBCEFF741BDC01B3A2466285A22FE428E759,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_HttpClient_mB42A5639BCEC711DF82157F1D8B88D9E239A8490,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_get_ClientWebSocketProvider_mA66C593DEA05F6004930AF3DBB2617FE487D87F8,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_set_ClientWebSocketProvider_m3F2082D8E14113B6EB79A3EAE98885F2DA4F9053,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_add_OnConnected_m8C3D5DFB18C2DCC42BFC0E31D50D27C1B942BA39,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_remove_OnConnected_m60DBEBC0A3CB61B6D7E0435E29BD2A7F59925F2F,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_add_OnError_mE4F5A0D01D36956BF4F84F71EA89E83B5B0FE76C,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_remove_OnError_mE622D17481DE041B0A2CB152F83FEC8BB1EE9A08,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_add_OnDisconnected_mB2910F22013D2B8805CD34D14EDA1DD741BDB7A1,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_remove_OnDisconnected_mED2BB005D80B81241C1F5348C8309908AE13E338,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_ConnectAsync_mF024D2306601A545290846FAD9CF9A6787FCE7FD,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_m671FB6EADF9481835AD0C81F90F47D49955305C8,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_mB0F21924EB56E3822698F52CFFDEF79A97D355E1,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_InvokeDisconnect_m52F6E016A6DEB7F701A1E6D162EF4D6F38A2F43B,
	U3CConnectAsyncU3Ed__81_tEC0C85D67FAE73578E76205200920388621860BC_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__81_SetStateMachine_m1ECCE93BD645592AA838CC0DAA79961A910639F5,
	U3CEmitAsyncU3Ed__101_t352A5FDE536E2B8206AD4E4A636B869BD30E04C1_CustomAttributesCacheGenerator_U3CEmitAsyncU3Ed__101_SetStateMachine_m82FDB3356A487BDFBB32BE08AC8B04DA1A1C1C67,
	U3CEmitAsyncU3Ed__102_t451F5D6B80244A918B1E5BB5F478831E3E3F1701_CustomAttributesCacheGenerator_U3CEmitAsyncU3Ed__102_SetStateMachine_mFE7510E343DBC01A0B53EBC8EED3EF255919C940,
	U3CInvokeDisconnectU3Ed__105_t2EDEBF660EDF2FFE4C4E2CACAF0F2DC0C421C931_CustomAttributesCacheGenerator_U3CInvokeDisconnectU3Ed__105_SetStateMachine_m04BF0AC0D285F30E692F1EB9064013F6F8D34ECA,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Path_m1FBC6F3D54F28D6AEFAF8BC34CE8AB83521BFD22,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_Path_mF10633E51D73A179E9C13DE98858877C01880F5A,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ConnectionTimeout_m42D3C8D3D6D02020137D14416430ED2A72A751F0,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ConnectionTimeout_mCBB08504FAD849BC46274DE73825CB22D5B8CD00,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Query_mFA11FB20F1BF0F85E248DBE76C1E32D3FB98FAA4,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Reconnection_mD04D1F2BF70F0AD7530BB503EE5107B63B8C7E05,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_Reconnection_mB136EA9D898F774248A5A98CB955F984FE6D7F83,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ReconnectionDelay_m37AECC4F9CB91B1C1F54502B7D418E8D1947DF11,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ReconnectionDelay_m82D0995BD1EEC7BE8B4638E922DAA79AD7814A39,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ReconnectionDelayMax_m72FFED7A33E94DAA89D19BA954A5731580C4B47F,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ReconnectionDelayMax_mE4BEC23B941B5F7E4489B81DEBF91BD320D46387,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ReconnectionAttempts_m475F99729FA0F7C0CE98D92E1586AF21F5C02A3E,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_ReconnectionAttempts_m57223BAF3247069041AC79EBD52170484CE29F7C,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_ExtraHeaders_mB57D5270503BC51BFF3B6B5FC32A433CAE43ACA7,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_Transport_m9FA7D6F68B7B38E11B0354A29E6521C654F83B02,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_Transport_m04CEA24437BE96E7A5452F26A8A6015A66CB0884,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_get_EIO_mD417CA97149DFF2BE4A2AB3ABE8E0CAC43C79AA9,
	SocketIOOptions_tC7827F52E7E5F2E850A68A4F0245EC316271FDD9_CustomAttributesCacheGenerator_SocketIOOptions_set_EIO_mE6719F4103690097238584098D0E24CDCF0699A1,
	SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_SocketIOResponse_get_InComingBytes_mD5D41E0AC1EA34453CE9DF5A373CE1BB4C31971E,
	SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_SocketIOResponse_get_SocketIO_m5B71B6FA3F2A4D38B42813E966DFAF116E7FB08F,
	SocketIOResponse_t144CA415D0501189BB4958CE5246F5229557160C_CustomAttributesCacheGenerator_SocketIOResponse_set_PacketId_mA5D00FA08CA5BE3CFC7AFBD2F974D84476AF9495,
	DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_get_ConfigOptions_m0FFE3A15E07BC02B600221B1436F198C3C469AD1,
	DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_CloseAsync_m9E183F55F9C327EB8835C7C5EF11775EFD2EDF81,
	DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_ConnectAsync_m64C51C977FF8A51221EA2562864A63D000BBCD13,
	DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_ReceiveAsync_m648B2AACE8C9D93EF572D52706821146B5955069,
	DefaultClientWebSocket_t6E221A6712938200C4ACE91E4ED148FCD87FD974_CustomAttributesCacheGenerator_DefaultClientWebSocket_SendAsync_m6A1809AC61900AB4A24437BD226FEEEFAEFD89B8,
	U3CCloseAsyncU3Ed__9_tAC450241F2738C7E88A463AE205E53DD0F472200_CustomAttributesCacheGenerator_U3CCloseAsyncU3Ed__9_SetStateMachine_mD148CE0C89031C23EA867D2FF34BB81F6FBB8984,
	U3CConnectAsyncU3Ed__10_t71EAD2AC6AF9800F0EFF936DC03513F46828E869_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__10_SetStateMachine_m437EFC3CEDEB097994C3197EFE243E90FB0F0E53,
	U3CReceiveAsyncU3Ed__11_tA94E1CDD625F17268ACEAA6FBCFAFD4322F6A2AA_CustomAttributesCacheGenerator_U3CReceiveAsyncU3Ed__11_SetStateMachine_m66A5AF498F96EFF912632912CD93F2D36043A027,
	U3CSendAsyncU3Ed__12_t03B48FB814C8582E8C3FCA0A93DE32A1A202B660_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__12_SetStateMachine_m64CD752CE62EA1350A8057CFA24ADAAF7C3C8FD5,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_get_OnTextReceived_m15CF89073B7DA2790586FAE9400128815DCAC7C5,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_set_OnTextReceived_mAEA01E64C138127591AAAD75A2DBF1AEBE3314FD,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_get_OnBinaryReceived_mB99F51EE3345D6202EF7337B52FB40A2D7FF5D41,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_set_OnBinaryReceived_m965D579FDA010E4F69943F9C9EB3335AE4B2A6C4,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_GetAsync_m0EBB5C5F7C47931030A70A6DB24EF1B269EF68F1,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_SendAsync_m2C7673A37302B7AEE79CD307BF0CFF3B1EA95508,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_PostAsync_mBACF830DEAC2474E755F9B9ACC46FC5D8D7C526A,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_PostAsync_m3D2C65490C2C391161ED0E7D320E3CC8EBF33AA7,
	HttpTransport_t2C8EE173BC6D135E1EAF3AFC71AE199D4096456D_CustomAttributesCacheGenerator_HttpTransport_ProduceMessageAsync_m97A6135E853A5F2A3BE2A3EB439C2087A7DDD42E,
	U3CGetAsyncU3Ed__12_t0D443F3F9245139C0A60A621D3D984F9CF499F50_CustomAttributesCacheGenerator_U3CGetAsyncU3Ed__12_SetStateMachine_mCF888DD1BB02D2EB5E362AC40CEE07D81E519E7A,
	U3CSendAsyncU3Ed__13_t8CC349965FCFFCB8BAC5C472A7C91E2A86661647_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__13_SetStateMachine_m024A5A18084436D890DAB736AE15C8572355A482,
	U3CPostAsyncU3Ed__14_t00E8622061AB3CEA43EADB68B0CE11823C512517_CustomAttributesCacheGenerator_U3CPostAsyncU3Ed__14_SetStateMachine_m3BE227D571DCB3C1149853D5363EDFB3012D06D1,
	U3CPostAsyncU3Ed__15_t92AB44394A24854EDE05E48D76C50B3BFE2A951F_CustomAttributesCacheGenerator_U3CPostAsyncU3Ed__15_SetStateMachine_mC519448B2884FD4C0A877B254582828AC538BCB9,
	U3CProduceMessageAsyncU3Ed__17_t473D8699750363085FDA028EB1D6E691F0667C99_CustomAttributesCacheGenerator_U3CProduceMessageAsyncU3Ed__17_SetStateMachine_m85ADDF619E98B828A3AD77EFE309E8EE0E6DCC5A,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_ServerUri_m76DD5F6D1F3CE15C77E08B5F3E14184DC5889764,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_ServerUri_mEE683013DBB0F35BCFEC5DBE625DDF099F8A878D,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_Namespace_m1EFE88CA37B3F58535288CA4E2F8464310056B30,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_Namespace_mD055A954C5FF6298D49A8143EFB7EA93AB90E4F3,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_Protocol_m8138CD8D64707ABDAB1EA4527E10622E496A3A04,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_Protocol_mD60882D5BF23AAF4C1E38A15BCD14174824517E1,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_EIO_mC86D14482EB4E63F3802B103188DC42F1A96B677,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_EIO_mE9713AC8626966E10E044F0646C307B1613CC500,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_UriConverter_mBB3506ECF9A1916623F14F8E3793E0A195012B5D,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_UriConverter_m2941F574F55685D9365E939FF0B4FFD687E973D8,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_OnMessageReceived_mD238146DE5B5BA3A69FBEF3D45901A4CDFAFF44E,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_OnMessageReceived_m0CAFAEC2DE52000DEE5CA7A96F73246E3CFA69F1,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_get_OnTransportClosed_mB5E39DCA53D6C88CD8D4BA0E7A241B3256AB5D7D,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_set_OnTransportClosed_mB4D7DE54D0F47B81D7C6E6B783A317C004EEB876,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_ConnectAsync_m2427AB13C13D5667E53D2BC80D9BF53B0FF98034,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_ConnectByWebsocketAsync_m5E61A3B5E272E5F83206A8D021E002D31E7C1D82,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_ConnectByPollingAsync_mE44BDC34D0F4CBFCCF74F16237B66A1292A9D650,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_OnOpened_mF81F97FDD32B87BA06CAB553E4592F5A17361B67,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_OnTextReceived_mCA059D8F291380A10E94F37F81FE437A8C26FF28,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_SendAsync_m6CCD8FE5CD3547DD657D25B0B9E90CA78B76640F,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_DisconnectAsync_m5F372E1D46A2C1283A015AC6130E80D78E783DB4,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_SendAsync_m5370779C7885AB5A5842E53DB896F3F2D26241B6,
	TransportRouter_t77A864363EB83A98E6C43E43BEA25B6A115EC4E3_CustomAttributesCacheGenerator_TransportRouter_SendAsync_mD1B98253A50DF7675ABE1A5D29419B081294B4C9,
	U3CConnectAsyncU3Ed__41_t6A1C2615AC447F3DE72561AC36B3ACD7075F6BEA_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__41_SetStateMachine_mABAF1145E76B3C806CD1DC24224EE718C0FE4526,
	U3CConnectByWebsocketAsyncU3Ed__42_tCB5191332F2CA1D429C7927ACFF084D7391AB4B3_CustomAttributesCacheGenerator_U3CConnectByWebsocketAsyncU3Ed__42_SetStateMachine_m3294DBB8241985C904860D8C910296D0AE1EF4BA,
	U3CConnectByPollingAsyncU3Ed__43_t3DDC694165D52D65EE28CF8823D39D7B8C54D403_CustomAttributesCacheGenerator_U3CConnectByPollingAsyncU3Ed__43_SetStateMachine_m37FECED5E2C8F12E3FC6308915571A7789E48339,
	U3CU3Ec__DisplayClass44_0_t91DAD191A5043EBA3AEF013DCCD3BF13F7FAFA10_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass44_0_U3CStartPollingU3Eb__0_mF8DE4F95BFD4770AA2743B6041AB854968ECFBC8,
	U3CU3CStartPollingU3Eb__0U3Ed_tFD93EAF0E50A0A00BF9B2322A276C6E24654A267_CustomAttributesCacheGenerator_U3CU3CStartPollingU3Eb__0U3Ed_SetStateMachine_mE5328B25B4712C524080DCAE323D9FD3C4263794,
	U3CU3Ec__DisplayClass45_0_t592C4689D5D4768EF662780E4A6AA5E849A4A1B5_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass45_0_U3CStartPingU3Eb__0_mC8876E143A7683FADD043024F9F92910936F1D6D,
	U3CU3CStartPingU3Eb__0U3Ed_t3655EEBE87BF783338EFD1D7513C2CA49967F18F_CustomAttributesCacheGenerator_U3CU3CStartPingU3Eb__0U3Ed_SetStateMachine_mE910C8499A852C5A9AE83B20C285F1B5226A63A2,
	U3COnOpenedU3Ed__46_tC126164B8F92990949F47354CF35D0D6243FC30D_CustomAttributesCacheGenerator_U3COnOpenedU3Ed__46_SetStateMachine_mAED54523AB8B9D9861736CCEA7A47D1EE7022413,
	U3COnTextReceivedU3Ed__47_tBF3EC55538FD20A2C2F295FA2F7AB9A71CB0ACFE_CustomAttributesCacheGenerator_U3COnTextReceivedU3Ed__47_SetStateMachine_m75171F6AEA5BEBA0568157A3D74819D9786CEE73,
	U3CSendAsyncU3Ed__50_tD4214C11A3FE21618E2CFD580BA76BD12D8AC791_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__50_SetStateMachine_mECC493E4D4C5433A4B2620F29E2BF856DC2FAFFB,
	U3CDisconnectAsyncU3Ed__51_tF945902F5B39D5605489C837F3C2C815CBC5D110_CustomAttributesCacheGenerator_U3CDisconnectAsyncU3Ed__51_SetStateMachine_mE795A2BF18086096E7B38E6CADA5BDBFBFC96D30,
	U3CSendAsyncU3Ed__52_t17F9FE76F8AE93621EEC10DD5F9969EDB742A2E0_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__52_SetStateMachine_mC80CFA8D89B2E8846F2F18BBC81A9ED45370A481,
	U3CSendAsyncU3Ed__53_tF5E922CA75779B32F77E70E0CB99949E53AA4641_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__53_SetStateMachine_mC93F378EE76C870A8B0DDD34A803ADE568CBAB48,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_ReceiveChunkSize_m7423C6ABF999816047CBBE5091421C083583BB53,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_ReceiveChunkSize_m4039BD765C6687FA791AA26D54BAA52BD8D4B39E,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_SendChunkSize_mF03112851A9A4B9EC8B53FA9DCB4C01177AC138E,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_SendChunkSize_mDF22C5AA6380D76D2C03F8F5B1BE613F56657F7C,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_ConnectionTimeout_m805685CAF6A21DAA5B4581E9115BF5DF7A638A6F,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_ConnectionTimeout_m9B7C2A72B06C3EB53EFF499EF49D757505461D99,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_ReceiveWait_mD2A1F60C8F17900ACF6B9DC77B2398894772876A,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_OnTextReceived_m80A4849545A1B10B03397447C624C2B69B603859,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_OnTextReceived_mDA981BF42B62CB4AE52C3011F996031E36EFA51A,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_OnBinaryReceived_m12078E3CE2369D226DB519AB8EC7858D912A7308,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_OnBinaryReceived_mD8C6635BB91FD7D6158BA90281356FF2BEA0B260,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_get_OnAborted_mF078CCDAAF48AE1F1E12275CF8E213756FF093FF,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_set_OnAborted_mBB91B48B2DF92743FCCA2255932BE9D67E55AD81,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_ConnectAsync_mDD02270F7B94ED91A84ADAE0775BA32698BE7EDF,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_SendAsync_mCBCA4E1F380CA76309F844B9D7BD356B37E6B636,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_SendAsync_mE057E9C97004675A5303352DB450032FDDBA19D6,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_SendAsync_mECEA08E3E7437DAD3170C2BCA9C51AD5F07EF349,
	WebSocketTransport_t3E0A4E8E59D7477C26FB2C2D44DC7C0A69DECFC7_CustomAttributesCacheGenerator_WebSocketTransport_ListenAsync_mB0C9B98540C9A632DEA0267D818FD1EEA03F6E7B,
	U3CConnectAsyncU3Ed__32_tE9BA4AE30873EA605A9A262FEE1C96D9EB3901DD_CustomAttributesCacheGenerator_U3CConnectAsyncU3Ed__32_SetStateMachine_mC9095D635C19D612D42B3374031B330C1DAE74A9,
	U3CSendAsyncU3Ed__34_t62E2B188681D6FD30D8EEA9755F13D1DF544B37E_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__34_SetStateMachine_mFB8DA5CD9392EE2E24757CB72F3F922B3843E502,
	U3CSendAsyncU3Ed__35_tAE567C590E515A593B862177053A3BDAA7600897_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__35_SetStateMachine_m03876D9EA87154470B66213C6C917DA9288593B3,
	U3CSendAsyncU3Ed__36_t6F1FF8A99D4056DFAC694AC0DB7F997483C92CD7_CustomAttributesCacheGenerator_U3CSendAsyncU3Ed__36_SetStateMachine_m4A84217F6A869CA3E24C7909B818F4B37FA67C7E,
	U3CListenAsyncU3Ed__37_tD4C3DDB89F6F9FC2C4ADE0AFAEA8FC6A1B00B611_CustomAttributesCacheGenerator_U3CListenAsyncU3Ed__37_SetStateMachine_mCB6D898F64A30EBAFEE9F5D18346C8C403B85460,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Namespace_mAA8F0EDECFAF3E10B0DBECF08F653C1BFDBE8C39,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Namespace_m646059378AD2E597BEF2CD530131C6A3FB7C5367,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Event_mA5C2557061343D628095E49FC95B01F89B76A3A5,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Event_m8029DA632FCD8CE8FA190756ED4B6C0EEA6DEF0A,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Id_m97035E0F4A2949EA52F3F073D456BF9488D29E33,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Id_m22A1C8842F872CAC13EC07D4D7BE5D5F03A11555,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_JsonElements_m22A4CAD588EB7F51469D0EC607EC1517FB9242B9,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_JsonElements_mE1BEFAD769DD0E5A18F7D4A792A8BBE6A308B944,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_Json_m7BDB54A72D4B384A282EEFEE3FC20ADD8E386006,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Json_mD9768CB381F44A08DF584D94BABC0F2AE54845E5,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_BinaryCount_mCAD5E1F353E75D1CD65F7E56171D7308456098C5,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_BinaryCount_mFB6806B814207CD244A17A422839451BA3EEE00D,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Eio_mA5F0D55BB0AA5B36FC12DC93555DBB37E61BC798,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_Protocol_m7D775B3886C401B5485E9E00C0B1588FD37D53DD,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_OutgoingBytes_m9AF39913F8652D5805D6DF76A6BB5DDA94865B49,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_OutgoingBytes_m0AEF3EF6974784DB66FF5328C4810A0C4FBDBEAB,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_get_IncomingBytes_m6176E430798B6B981ACF0EDE3E8B46186906F8CE,
	BinaryMessage_t28194EB4729F8DFAB0CED52264095224521C2E3E_CustomAttributesCacheGenerator_BinaryMessage_set_IncomingBytes_m1092FBA649646F7815529C8BAC29791429906284,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Namespace_m63EC8D1F5F4A3FD033A21A9A7736E1BBCB8E6BB6,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Namespace_m0E308A032CFAE0C1E9A7AECEFEA8F3122B1EB3E7,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Event_m4E1029DC9E74D4791827C070DF1BE7229E74564C,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_JsonElements_m4EBE99C0156E918C12946B8801BBF4532E13F2BF,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_JsonElements_m3A410CA3471CD989D75DFFAB35A79858A5953A88,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Json_m1D2D03D72410C66DA620FA77B772E0661D4EADFD,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_Id_m7B65941B3858ACCB71050DB455C09B83710CD4B2,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Id_m552EB2A26BC0AE3A67177B0D3DBE174F690AF824,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_OutgoingBytes_m17FAB47D528E3B2C40E407B974F77F121E5902B7,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_IncomingBytes_mD5D10FF092216467AC85772D7DF2C89158499DAD,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_IncomingBytes_m61174B457A26958E141D260B3BF1508A146BD976,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_get_BinaryCount_mCB5AE12314E9677CA289F008B341891041778DAE,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Eio_mB675DDC19D4BC87F0983349E6C1DBF3E84FBC843,
	ClientAckMessage_t9DD747D50972AA642B7407DEA42E90AC780984B3_CustomAttributesCacheGenerator_ClientAckMessage_set_Protocol_mE2EC5B7E221B3493A7063810BBBD538FFF724069,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Namespace_mF3061BBC4D59B4A4647697ACBC0CC30000E5A559,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Namespace_mC27A209BE8B51508A386C97A6CAA58EE2F695D53,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Event_m51C6D5CD0A4A594CD8485498498B23F344D71EC9,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_JsonElements_m89379F5E3191C45DD20BEC5B41486663A8247FE9,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_JsonElements_mAF9ADD78439515C24E55DF9D654A69BA97F51BB8,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Json_mC12CDDE2EC5E404E8869AA0C4B7A2B5705B0A468,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_Id_m5FDB4ECB225E66B9A372CE532ED6A0573DF2B626,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Id_mBAD409A433D3E03028AB31C3B7D6946D1B853F4D,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_BinaryCount_m7FAC63EADBBE1A1119FA3D02AA8413EA9D38B7A0,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_BinaryCount_m8DE802695E94A4DC67C1EDED8A58E68443432575,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Eio_mC80168F7A70621DA9819ABD2A3BD062812BA3FA6,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_Protocol_mDCC45C47884C9084000C140F6543A0187DAF426E,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_OutgoingBytes_mB1E8E08B93D7E4D6255E215C0DB3B72D6B692A06,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_get_IncomingBytes_mA30B44355FB52052C70AC0987A2BA0C7AA2B96BF,
	ClientBinaryAckMessage_t7BB6C564C9E83643436EAABD17D5EA6422BDEF18_CustomAttributesCacheGenerator_ClientBinaryAckMessage_set_IncomingBytes_mE598B123B5E9B1A32DB1E75DB2EDA6647AB3159C,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Namespace_mBB9748CE4C8C74F0C4A5D149A7BC761709665E95,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Namespace_mA7FDE6E95A56A0C4DDCDC02127D102B482D3D6FA,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Sid_m4D121694E0C88D04E5A99433C953CE1585B2BF29,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Sid_m80379E0AC6A06BB24F3FEC26BB9563C8CD59E93C,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_OutgoingBytes_mFAC5CCE676B708DFE7EFCA8B6845E0B55CA34704,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_IncomingBytes_mCE4223CDA74C273D269F1E6D04C6B3E5BCDB108F,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_IncomingBytes_mB3582A4D805FFB5E600AAE3E113534B5C2BA28E8,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_BinaryCount_m1C3D0F041C7BCE37A2007B2CAFD89FE4D5726946,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Eio_mE24F9D9F65069643E7A512BB0611448FE892F0C2,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Eio_mCA4D23CABE3F03CA5F2D48A4929155903607621E,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Protocol_m05896E4D86A5D4F7D4DBFFC6CFF73482322B0654,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_get_Query_m45D8A90C909B5954183B9AACBBFD2908F17C1692,
	ConnectedMessage_t5A3FA1AAB910FFD97C1356668DACF4E78E999B4E_CustomAttributesCacheGenerator_ConnectedMessage_set_Query_mDDE08088D7D65E1896B3DDF467CF66D4CBF9C1B2,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_Namespace_mBC654DF14D562AACD4678E23EFF3A5AC69640D59,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_Namespace_m137124B9A6AD089CD1819267FC45348711C220C4,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_OutgoingBytes_m4B4FE93340DDDF8301A0A6CB5ED17C8C5470ED17,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_IncomingBytes_mC6CF82CBBD5D15443DE29F95E76844F09CE831AC,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_IncomingBytes_m7BBCB930B5C7FD1A9EA060E32B6FF5F54E1CC1C7,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_get_BinaryCount_m87D1D38C6DBAC669C1714C9E08CC3E3D020F7C04,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_Eio_m7D809F9D4C4F6B3DA317012FD4915F95113F3F90,
	DisconnectedMessage_tDEE79CAAFB6FCA5ED1899661D5204CEB9279FBB5_CustomAttributesCacheGenerator_DisconnectedMessage_set_Protocol_mAC0B93C5BC99FDCCA8917275B3FAD679CB40C0EE,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_Message_m2FA4EC941DBB4D885911E9B6A08206CCC6EAE05C,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Message_m1C676E1D517B0B82E65977F394E66A0C23BDA2FD,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Namespace_m1621D5622A176D55983D56ACBF3BDF9225391D43,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_OutgoingBytes_mB7064FB185F60EF9688241881E7A548EA4FEE8B8,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_IncomingBytes_mD9E5D721243093414A3BC2A673642D1D3F3930FB,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_IncomingBytes_mF36F79779BC64A1B7AE435DFFA050428A35F689A,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_BinaryCount_mD3A9C112BB333B628E3F160DCD82B651971A9DC3,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_get_Eio_mEA9E313693DB15A377C002395ECFA1AF273BC29E,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Eio_m9A26978230A37C6746E2EFA8BD7CBD28E45B68E7,
	ErrorMessage_tA347C2C78D2730163797C903DC1B61163A2AAF9A_CustomAttributesCacheGenerator_ErrorMessage_set_Protocol_mD3210142031FD98E9A9080037014A9DC9AD2D325,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Namespace_mA2473D71D774C869AB286DFA44A5BFAD6AAE3D97,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Namespace_m262295490C730CAC637DF2A0ED3E5222171253D4,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Event_m1B6110DACCD1175CCA546189CA5BC6D31B852E99,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Event_mFD9DA13A75C0D0C1F4926558FA4D78F808F76C39,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Id_m67179AADC6C01AE12374E1940C861770727F9ED5,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Id_m806A84CA037CCBD53F96C5C31632AA70F152C1E1,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_JsonElements_m5F76EDD056F8AFD32C049EF08816ACAE644F5017,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_JsonElements_mF40A41BCE4C89E3EA46B74519DE5EE213355D03D,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_Json_mCDB73487936C56C04AE0AAA0855B57A25F68BF41,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Json_m4386F4B466346D2538B8AF0696D729521C9D00D6,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_OutgoingBytes_mEE2B105F54ECACF5F024B33B6DE55FBB8B4A2C05,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_IncomingBytes_mA4330EBE622AF968FD4C3863787567891BADA0FD,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_IncomingBytes_m3A6B20C6F5C764CE1974A6DB4C6E8C2EF5D5AE34,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_get_BinaryCount_m77F298305163FA7315EEFEF117D8A9FBEF798B06,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Eio_m695EDB9FFB1E5724ED628F6827E2E9D60DA40943,
	EventMessage_t977472551DDB7D632C9CAC59FC6B63D80993EA15_CustomAttributesCacheGenerator_EventMessage_set_Protocol_m714D79DD63FECC93AC5CF26366C0FCD3CC61845D,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_Sid_m1C2DFD9ABF2841ECC72816888C39F53B90A123BB,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Sid_m91D157ACA4F17CE6F04F912C5E6576EFA7ED1E3E,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_Upgrades_mF713B2623F775FA9E340303DB033C263E826C9C1,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Upgrades_m57F2B66A6F46EFAEFE0DB04833C058834D058076,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_PingInterval_m6BAFB5A01580F69D4BDFABBBB2453F38DC6B0774,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_PingInterval_m6F90759D62F4B9F106213211CBEB729BD77516FD,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_PingTimeout_mA06D177C1911170D70B6C871074BB8751A257F1C,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_OutgoingBytes_m84828C6286305F9254089FE27036FD61DED71E07,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_IncomingBytes_mEBB3DA727A3E6E1B5102B2BF2C79839E5452A6E4,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_IncomingBytes_m55D0D0FBA7764E86BFDDD74A6B5DB3642258EC4F,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_get_BinaryCount_m3B7FEAE705FFBE0FB050E7DD20FF4173D0BDD811,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Eio_m27947BFE1007703F0AAF867D632D461E99252612,
	OpenedMessage_t7143302AD3C12A1BF987ACC91A94642A65B341A1_CustomAttributesCacheGenerator_OpenedMessage_set_Protocol_m896FE4E58F36070B60C381736E34B8385D79BD56,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_get_OutgoingBytes_mA33B556D75B63546FE1680B93906C4DDACC16F54,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_get_IncomingBytes_mAB4CB776A307E0C1627ECC173704ACB7DEB005CB,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_set_IncomingBytes_m02799D45BFCC060407A5DADA8EE815B3BDB7E597,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_get_BinaryCount_m5A1C66215FB119E85F93E4F59D34C1DAD2F6CD33,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_set_Eio_mB6C5A7B749780F1CB30A671BCE98CC7BF033A26A,
	PingMessage_tD9CBE66CE792D6FF1B3092D3D7DDE704745DC077_CustomAttributesCacheGenerator_PingMessage_set_Protocol_mE38DF0C7211E1389CE1D252151F33FD54965404F,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_OutgoingBytes_m92CBE117D20EFD12A4A8674566CE185D76B16690,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_IncomingBytes_mC2E61D002A47797E8379978F60B8FA39EB89E072,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_IncomingBytes_m9B6B09A1AA7AB28699B3C0AA5E1EFC4A5973711D,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_BinaryCount_m8568D4D84A7BC394DDE4CDA945DCB1A1BB9B1695,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_Eio_m5ADF0E510ECCBBFBF803155E9392544C5D8F9CEF,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_Protocol_m19FE10462749E7A401EAA6FFAD950ACB492E6C7F,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_get_Duration_m678F36A9284497BF1D8B738AE321D6C0EEEBA9F8,
	PongMessage_t424593F9786A71B9493A8257B22AB16C73A3CD37_CustomAttributesCacheGenerator_PongMessage_set_Duration_m3F8BA41944161F752771D65A9AD307460068B6F0,
	ByteArrayConverter_t7BDBFFE466A0967463D8CF39235FF35793951D07_CustomAttributesCacheGenerator_ByteArrayConverter_get_Bytes_mCF080CE5AFFFCB4649F40F3E469A297420BAFC28,
	JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_get_Json_mB0DB437258A308B7C3631B24D12457F813CE5000,
	JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_set_Json_m00057D5060370A218B3659E161AB15FF0E0AFAC4,
	JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_get_Bytes_mB4255CBA3832D483011FE9A4462BDCE1FE65D052,
	JsonSerializeResult_t747545D85D5AFC0C96726C41807150BFAA3615F1_CustomAttributesCacheGenerator_JsonSerializeResult_set_Bytes_mB09A845EA53D94853444F111F8A114E041FA0602,
	SystemTextJsonSerializer_tE4204226053CF0444C730ADB6BE6E5F8D8EF3853_CustomAttributesCacheGenerator_SystemTextJsonSerializer_CreateOptions_m284D599E8AA27E39CBDB33800DCF85488E6DFC61,
	SystemTextJsonSerializer_tE4204226053CF0444C730ADB6BE6E5F8D8EF3853_CustomAttributesCacheGenerator_SystemTextJsonSerializer_get_OptionsProvider_m78ECE00826C3253812E11D9BBBE1CB0374896AD5,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_m671FB6EADF9481835AD0C81F90F47D49955305C8____data1,
	SocketIO_tABD26D388876DD3074BE42A3C66E874C04A85025_CustomAttributesCacheGenerator_SocketIO_EmitAsync_mB0F21924EB56E3822698F52CFFDEF79A97D355E1____data2,
	SocketIOClient_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
