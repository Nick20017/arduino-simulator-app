﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
extern void IsByRefLikeAttribute__ctor_mBAF4018209D90000A5E262F2451AE1C4A82314D9 (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19 (void);
// 0x00000005 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84 (void);
// 0x00000006 System.Void System.Runtime.CompilerServices.NullablePublicOnlyAttribute::.ctor(System.Boolean)
extern void NullablePublicOnlyAttribute__ctor_m0C249ACE94B8A56FD98C494E164745681C07BFFC (void);
// 0x00000007 System.Char System.HexConverter::ToCharUpper(System.Int32)
extern void HexConverter_ToCharUpper_mF6A33228506E78664B6DD4DC6DD64E116160B565 (void);
// 0x00000008 System.Int32 System.HexConverter::FromChar(System.Int32)
extern void HexConverter_FromChar_mEFBD63D37E6298E8067997ECD9525D9055BA47A2 (void);
// 0x00000009 System.Boolean System.HexConverter::IsHexChar(System.Int32)
extern void HexConverter_IsHexChar_mC65F541844A5C576B87D5876293C5253394BDB5C (void);
// 0x0000000A System.ReadOnlySpan`1<System.Byte> System.HexConverter::get_CharToHexLookup()
extern void HexConverter_get_CharToHexLookup_m662F5D081F59BD939B773E221C246025A3E1106D (void);
// 0x0000000B System.Boolean System.SR::UsingResourceKeys()
extern void SR_UsingResourceKeys_mA97725F3E19BB2DCFEFDFFA236D77067A2A1A1D8 (void);
// 0x0000000C System.String System.SR::GetResourceString(System.String,System.String)
extern void SR_GetResourceString_m2D921EAADF4574169AC960323C3347BB2AB0B3D5 (void);
// 0x0000000D System.String System.SR::Format(System.String,System.Object)
extern void SR_Format_m36EF1227D7C63135EC15BDFD28B13BE407AFF8BD (void);
// 0x0000000E System.String System.SR::Format(System.String,System.Object,System.Object)
extern void SR_Format_m8AA8EF600D0F3253D2543A88CE469D44DA95E7EA (void);
// 0x0000000F System.String System.SR::Format(System.String,System.Object,System.Object,System.Object)
extern void SR_Format_m5965DF79038A06C4529BA0C615B3D95CDBE8FBC0 (void);
// 0x00000010 System.String System.SR::Format(System.String,System.Object[])
extern void SR_Format_m98E7A215B14ABB55764D42C2B1B82AA655F42297 (void);
// 0x00000011 System.Resources.ResourceManager System.SR::get_ResourceManager()
extern void SR_get_ResourceManager_m15FC5066BDC3FA360108455FF39B0482B2825559 (void);
// 0x00000012 System.String System.SR::get_ArrayDepthTooLarge()
extern void SR_get_ArrayDepthTooLarge_m8E5246CC370DE2F9A4B34825F399CBCB14806876 (void);
// 0x00000013 System.String System.SR::get_CannotReadIncompleteUTF16()
extern void SR_get_CannotReadIncompleteUTF16_mFB2C4F4FF896E7A55C3DB5643191326D9B8165B5 (void);
// 0x00000014 System.String System.SR::get_CannotReadInvalidUTF16()
extern void SR_get_CannotReadInvalidUTF16_mAE828DD100654ED53A8AB575FD83DA87108A92CF (void);
// 0x00000015 System.String System.SR::get_CannotStartObjectArrayAfterPrimitiveOrClose()
extern void SR_get_CannotStartObjectArrayAfterPrimitiveOrClose_mFF35AF8C05CF6369E3C4A86E548FF9E401ECDD3F (void);
// 0x00000016 System.String System.SR::get_CannotStartObjectArrayWithoutProperty()
extern void SR_get_CannotStartObjectArrayWithoutProperty_m796897E58F30C853597753A827F3A6BE5E0E8D2D (void);
// 0x00000017 System.String System.SR::get_CannotTranscodeInvalidUtf8()
extern void SR_get_CannotTranscodeInvalidUtf8_mAEEF8B9D2AA4B647D5773014AD372CA67D40EB86 (void);
// 0x00000018 System.String System.SR::get_CannotDecodeInvalidBase64()
extern void SR_get_CannotDecodeInvalidBase64_m425D21F8E36FDC72014BB3CD5AA74B17A7BF14AD (void);
// 0x00000019 System.String System.SR::get_CannotTranscodeInvalidUtf16()
extern void SR_get_CannotTranscodeInvalidUtf16_mDF84CC7A7ABFEAA1851B8D919F26C9541BC1239F (void);
// 0x0000001A System.String System.SR::get_CannotEncodeInvalidUTF16()
extern void SR_get_CannotEncodeInvalidUTF16_mAFCBF8ED915D612C54B32ED4722C9FD7685259ED (void);
// 0x0000001B System.String System.SR::get_CannotEncodeInvalidUTF8()
extern void SR_get_CannotEncodeInvalidUTF8_mD044622D0CADDDA2D1888F6FAED934F6D1587494 (void);
// 0x0000001C System.String System.SR::get_CannotWritePropertyWithinArray()
extern void SR_get_CannotWritePropertyWithinArray_m421E81B8569A8E2FF35358575C1C7E940F450C5A (void);
// 0x0000001D System.String System.SR::get_CannotWritePropertyAfterProperty()
extern void SR_get_CannotWritePropertyAfterProperty_m9DB88F0E19DDB2E956DA3D147314473F6F7F3B6A (void);
// 0x0000001E System.String System.SR::get_CannotWriteValueAfterPrimitiveOrClose()
extern void SR_get_CannotWriteValueAfterPrimitiveOrClose_m5EF4BA121A746B12D44E1B2841E7DD326FA8DF0F (void);
// 0x0000001F System.String System.SR::get_CannotWriteValueWithinObject()
extern void SR_get_CannotWriteValueWithinObject_mE2E8F0AA4138D19BD3B5D87D4582E6BA1EDD063F (void);
// 0x00000020 System.String System.SR::get_DepthTooLarge()
extern void SR_get_DepthTooLarge_mCCABDA0571BF22B5950D9544B8C3A43E8AD963ED (void);
// 0x00000021 System.String System.SR::get_EndOfCommentNotFound()
extern void SR_get_EndOfCommentNotFound_m7EC7813B9DBBFE106F451AF7BF154CE7D47091A1 (void);
// 0x00000022 System.String System.SR::get_EndOfStringNotFound()
extern void SR_get_EndOfStringNotFound_mE297F8B18F982A97EFF63F9AF985A782FC0B976E (void);
// 0x00000023 System.String System.SR::get_ExpectedEndAfterSingleJson()
extern void SR_get_ExpectedEndAfterSingleJson_m2CBB628DCD24D55C5E65A50EE6BD232E69432ACC (void);
// 0x00000024 System.String System.SR::get_ExpectedEndOfDigitNotFound()
extern void SR_get_ExpectedEndOfDigitNotFound_mF5FC07B1E08E3C27D425934D25DA55EFA5039BC3 (void);
// 0x00000025 System.String System.SR::get_ExpectedFalse()
extern void SR_get_ExpectedFalse_m686BF2B905B951FD7A0B7B113B07F4CAD46771E9 (void);
// 0x00000026 System.String System.SR::get_ExpectedJsonTokens()
extern void SR_get_ExpectedJsonTokens_mBD05CE8ABE9D099915995FF7C614E43D6857B48B (void);
// 0x00000027 System.String System.SR::get_ExpectedOneCompleteToken()
extern void SR_get_ExpectedOneCompleteToken_m3B85490B936859F69328A8C7E08BC1B0AAD75A88 (void);
// 0x00000028 System.String System.SR::get_ExpectedNextDigitEValueNotFound()
extern void SR_get_ExpectedNextDigitEValueNotFound_m44344C55496E8230A0C9149446C02063C3782DCB (void);
// 0x00000029 System.String System.SR::get_ExpectedNull()
extern void SR_get_ExpectedNull_m3EF080DE6DCA1BAE1607E59338060FF6F3423BBB (void);
// 0x0000002A System.String System.SR::get_ExpectedSeparatorAfterPropertyNameNotFound()
extern void SR_get_ExpectedSeparatorAfterPropertyNameNotFound_mFF5042B71FE071A749F3476A14EF91EDBDEFBABF (void);
// 0x0000002B System.String System.SR::get_ExpectedStartOfPropertyNotFound()
extern void SR_get_ExpectedStartOfPropertyNotFound_mB9531B5B0EDCB65FF524E2BC7074D372670AE5F0 (void);
// 0x0000002C System.String System.SR::get_ExpectedStartOfPropertyOrValueNotFound()
extern void SR_get_ExpectedStartOfPropertyOrValueNotFound_mF76027CF08CB01E66E54EA8E60CEBDF2E87BC043 (void);
// 0x0000002D System.String System.SR::get_ExpectedStartOfValueNotFound()
extern void SR_get_ExpectedStartOfValueNotFound_m072A531F7E8A19B716CD16F93C0852070AB8BB67 (void);
// 0x0000002E System.String System.SR::get_ExpectedTrue()
extern void SR_get_ExpectedTrue_mC7DFA8983568EABB3EA8B6DF826B2B5739E1D940 (void);
// 0x0000002F System.String System.SR::get_ExpectedValueAfterPropertyNameNotFound()
extern void SR_get_ExpectedValueAfterPropertyNameNotFound_m420FF497C1ED3DDD5FFC9B58BAF5161F6EE1372A (void);
// 0x00000030 System.String System.SR::get_FailedToGetLargerSpan()
extern void SR_get_FailedToGetLargerSpan_m3BB2F4C63451BBAF226386631F78536835E42328 (void);
// 0x00000031 System.String System.SR::get_FoundInvalidCharacter()
extern void SR_get_FoundInvalidCharacter_m8F98496B841628A9B11FDDFCB4F88BDA59F0B548 (void);
// 0x00000032 System.String System.SR::get_InvalidCast()
extern void SR_get_InvalidCast_m1F4FC3EA35C0AC8A2B5D41514AD1B3D450D836A2 (void);
// 0x00000033 System.String System.SR::get_InvalidCharacterAfterEscapeWithinString()
extern void SR_get_InvalidCharacterAfterEscapeWithinString_m33041C4D461F0DFE6AD9D181D52E51694D51DA78 (void);
// 0x00000034 System.String System.SR::get_InvalidCharacterWithinString()
extern void SR_get_InvalidCharacterWithinString_mE4B9F0D32B28B929E8AB8F5CA13974C26EA53F07 (void);
// 0x00000035 System.String System.SR::get_InvalidEndOfJsonNonPrimitive()
extern void SR_get_InvalidEndOfJsonNonPrimitive_m9F28AFFDECA3FD3AEB740ECA6ACD48D7BE26635C (void);
// 0x00000036 System.String System.SR::get_InvalidHexCharacterWithinString()
extern void SR_get_InvalidHexCharacterWithinString_m61BE5F7ED9A6EA51595D41A87515B3777DB5419F (void);
// 0x00000037 System.String System.SR::get_JsonDocumentDoesNotSupportComments()
extern void SR_get_JsonDocumentDoesNotSupportComments_mD0E352AFD97325D9743DD47A2CDC0012D04BE1EF (void);
// 0x00000038 System.String System.SR::get_JsonElementHasWrongType()
extern void SR_get_JsonElementHasWrongType_m06721C1E1B060B438059DDAAB0527C2222F0CE59 (void);
// 0x00000039 System.String System.SR::get_MaxDepthMustBePositive()
extern void SR_get_MaxDepthMustBePositive_m7631F46DAE0D387C659ABE111CC9EC097028AF8D (void);
// 0x0000003A System.String System.SR::get_CommentHandlingMustBeValid()
extern void SR_get_CommentHandlingMustBeValid_mFF8EFAEBE0A74CD87C2B2B1F7AE9060C7E7DEC31 (void);
// 0x0000003B System.String System.SR::get_MismatchedObjectArray()
extern void SR_get_MismatchedObjectArray_mEE3EAD16343FEAC84B9DD1343FAA26CFD9C3342C (void);
// 0x0000003C System.String System.SR::get_CannotWriteEndAfterProperty()
extern void SR_get_CannotWriteEndAfterProperty_m1A13AF63A2C1B92898C0DF01AEAA333E6F4604B4 (void);
// 0x0000003D System.String System.SR::get_ObjectDepthTooLarge()
extern void SR_get_ObjectDepthTooLarge_mAF0DA4C99B69E61DCE4EA3B561C1B7F23DFA45D0 (void);
// 0x0000003E System.String System.SR::get_PropertyNameTooLarge()
extern void SR_get_PropertyNameTooLarge_m415529365C3B8C57EDB66411E8AEC8DB85DA49C5 (void);
// 0x0000003F System.String System.SR::get_FormatDecimal()
extern void SR_get_FormatDecimal_m009838A3534AB6E504C0D5A6742A2ACF2C267908 (void);
// 0x00000040 System.String System.SR::get_FormatDouble()
extern void SR_get_FormatDouble_m90FBC078819AFDD80C2F100DC32588BAA6552B03 (void);
// 0x00000041 System.String System.SR::get_FormatInt32()
extern void SR_get_FormatInt32_m36CEC22CDE95FDCD25FB18260B457855504A477E (void);
// 0x00000042 System.String System.SR::get_FormatInt64()
extern void SR_get_FormatInt64_mE3050647A21317A3F3AF1070683FA7C358F4EAC2 (void);
// 0x00000043 System.String System.SR::get_FormatSingle()
extern void SR_get_FormatSingle_m7946C5D11B0370E6EDC9FA19E628382BD3031A50 (void);
// 0x00000044 System.String System.SR::get_FormatUInt32()
extern void SR_get_FormatUInt32_mB176E75C88FB8AEA8E16D6F2A8A6ED49965327A5 (void);
// 0x00000045 System.String System.SR::get_FormatUInt64()
extern void SR_get_FormatUInt64_m6564B4C41078A600E0A9009B174C67CAB20A0B29 (void);
// 0x00000046 System.String System.SR::get_RequiredDigitNotFoundAfterDecimal()
extern void SR_get_RequiredDigitNotFoundAfterDecimal_mD94795A8DD7543F1D60AE8E6379DE02C2060256D (void);
// 0x00000047 System.String System.SR::get_RequiredDigitNotFoundAfterSign()
extern void SR_get_RequiredDigitNotFoundAfterSign_m238EB00FE33260F01E265A1B01C61E9ED8D1EBA8 (void);
// 0x00000048 System.String System.SR::get_RequiredDigitNotFoundEndOfData()
extern void SR_get_RequiredDigitNotFoundEndOfData_m45227DC6C780B1A8E47D62CF0AAB6790F74A3665 (void);
// 0x00000049 System.String System.SR::get_SpecialNumberValuesNotSupported()
extern void SR_get_SpecialNumberValuesNotSupported_mA8D5568457FB092FFAE1913F9529EC01B10480F4 (void);
// 0x0000004A System.String System.SR::get_ValueTooLarge()
extern void SR_get_ValueTooLarge_m1AF9044E740CDC4CD66FA6D04E20B9691BE5039A (void);
// 0x0000004B System.String System.SR::get_ZeroDepthAtEnd()
extern void SR_get_ZeroDepthAtEnd_mA1A2333C1E20A816C198F27FFA159EDFA602026A (void);
// 0x0000004C System.String System.SR::get_DeserializeUnableToConvertValue()
extern void SR_get_DeserializeUnableToConvertValue_m16ECD3B34DF3EEB8D2D59F63E655CA3FA88BD8F0 (void);
// 0x0000004D System.String System.SR::get_BufferWriterAdvancedTooFar()
extern void SR_get_BufferWriterAdvancedTooFar_m1A35B258CD64B5F3E0ED5D95DE441105A0E78144 (void);
// 0x0000004E System.String System.SR::get_FormatDateTime()
extern void SR_get_FormatDateTime_m3715CD18BF4333F9F31C7D94BE7DF92626B3956F (void);
// 0x0000004F System.String System.SR::get_FormatDateTimeOffset()
extern void SR_get_FormatDateTimeOffset_m66682500E6071DE7590691A2CADE3143467C71CD (void);
// 0x00000050 System.String System.SR::get_FormatGuid()
extern void SR_get_FormatGuid_mB4DF841F16EC3053865680D5588F017D08C0A6B2 (void);
// 0x00000051 System.String System.SR::get_ExpectedStartOfPropertyOrValueAfterComment()
extern void SR_get_ExpectedStartOfPropertyOrValueAfterComment_m9477F68FF6E5E93D723D28DDF35D3A87D1AD5368 (void);
// 0x00000052 System.String System.SR::get_TrailingCommaNotAllowedBeforeArrayEnd()
extern void SR_get_TrailingCommaNotAllowedBeforeArrayEnd_m2921E6AA28B2E0ACC59DF93D052C80694FC7ED2F (void);
// 0x00000053 System.String System.SR::get_TrailingCommaNotAllowedBeforeObjectEnd()
extern void SR_get_TrailingCommaNotAllowedBeforeObjectEnd_mA68FA644F011194FC455FF534F12D90F37594BE9 (void);
// 0x00000054 System.String System.SR::get_SerializerOptionsImmutable()
extern void SR_get_SerializerOptionsImmutable_m554851415FEFBE8DCC7377CF33F1B45568E01B56 (void);
// 0x00000055 System.String System.SR::get_SerializerPropertyNameConflict()
extern void SR_get_SerializerPropertyNameConflict_mCF1896F158E6898327D9E6510BC4906246D7EDA6 (void);
// 0x00000056 System.String System.SR::get_SerializerPropertyNameNull()
extern void SR_get_SerializerPropertyNameNull_m36BAF80399FC3141977B43223BFE83C8DDFCC749 (void);
// 0x00000057 System.String System.SR::get_SerializationDataExtensionPropertyInvalid()
extern void SR_get_SerializationDataExtensionPropertyInvalid_mD272087BD1E647A7E07F307C879D59DB3E20E57C (void);
// 0x00000058 System.String System.SR::get_SerializationDuplicateTypeAttribute()
extern void SR_get_SerializationDuplicateTypeAttribute_mC58F3BC6F19A5EA97D67C4EDC0D1AE7FC1188A0B (void);
// 0x00000059 System.String System.SR::get_SerializationNotSupportedType()
extern void SR_get_SerializationNotSupportedType_mEFEFCD3214DE4BA4FE7E64AF900D0AFAF28E8F10 (void);
// 0x0000005A System.String System.SR::get_InvalidCharacterAtStartOfComment()
extern void SR_get_InvalidCharacterAtStartOfComment_m14525B64E5302158767DDE18089E6B2C6A8427CC (void);
// 0x0000005B System.String System.SR::get_UnexpectedEndOfDataWhileReadingComment()
extern void SR_get_UnexpectedEndOfDataWhileReadingComment_m1B98B6762B9A23E2700C05D3C5D35E91A8B530FF (void);
// 0x0000005C System.String System.SR::get_CannotSkip()
extern void SR_get_CannotSkip_mD3706C1D328229102B14C32A2556EE0F5004500F (void);
// 0x0000005D System.String System.SR::get_NotEnoughData()
extern void SR_get_NotEnoughData_m1AEF73034C6E00E2B851E094BCC8DCE731BA1708 (void);
// 0x0000005E System.String System.SR::get_UnexpectedEndOfLineSeparator()
extern void SR_get_UnexpectedEndOfLineSeparator_m074B44BA6623162BCF899B4ACD971A62A42FD8A8 (void);
// 0x0000005F System.String System.SR::get_DeserializeNoConstructor()
extern void SR_get_DeserializeNoConstructor_mC378B8122523F38D4D286DF45DF798DCFEB2B160 (void);
// 0x00000060 System.String System.SR::get_DeserializePolymorphicInterface()
extern void SR_get_DeserializePolymorphicInterface_m59C024C53C4D920B4F6EEEE5C3252F5CDB26C1E2 (void);
// 0x00000061 System.String System.SR::get_SerializationConverterOnAttributeNotCompatible()
extern void SR_get_SerializationConverterOnAttributeNotCompatible_mBD321216A99ED8DA4BA782DDA6EDEE10A0809AE5 (void);
// 0x00000062 System.String System.SR::get_SerializationConverterOnAttributeInvalid()
extern void SR_get_SerializationConverterOnAttributeInvalid_m17DAC92FF850AEE2EB0A6D24DABAB9ABFBA64765 (void);
// 0x00000063 System.String System.SR::get_SerializationConverterRead()
extern void SR_get_SerializationConverterRead_m76B57CA2043F03E2022F7B4370B9AC410744607E (void);
// 0x00000064 System.String System.SR::get_SerializationConverterNotCompatible()
extern void SR_get_SerializationConverterNotCompatible_m97CE4CE914CBE701C016C61FC6B3D4200E9C38AD (void);
// 0x00000065 System.String System.SR::get_SerializationConverterWrite()
extern void SR_get_SerializationConverterWrite_mD85FC4886DA512F98F8F515DF2C830CB3E677874 (void);
// 0x00000066 System.String System.SR::get_NamingPolicyReturnNull()
extern void SR_get_NamingPolicyReturnNull_m03339F7AF61FA34FD5B622585C8070706F54B3CA (void);
// 0x00000067 System.String System.SR::get_SerializationDuplicateAttribute()
extern void SR_get_SerializationDuplicateAttribute_m5FAF927C5FAA30DCD6E899E69D8526A519A62AEE (void);
// 0x00000068 System.String System.SR::get_SerializeUnableToSerialize()
extern void SR_get_SerializeUnableToSerialize_m8EC60230E9F30396C357BEE9E0D37EE1BF1CF3D8 (void);
// 0x00000069 System.String System.SR::get_FormatByte()
extern void SR_get_FormatByte_mF1FC572F1B92D55407AD1D2C9903878869DD7E30 (void);
// 0x0000006A System.String System.SR::get_FormatInt16()
extern void SR_get_FormatInt16_m6C826C304D1F6607CA2FF209280B3E0CA177B042 (void);
// 0x0000006B System.String System.SR::get_FormatSByte()
extern void SR_get_FormatSByte_mA1D76EB6BAF90958F3D6676DEBA1F079EB037716 (void);
// 0x0000006C System.String System.SR::get_FormatUInt16()
extern void SR_get_FormatUInt16_mFEAB5443D9747CFBDFCFB0C8BE59F676BFAAAD4C (void);
// 0x0000006D System.String System.SR::get_SerializerCycleDetected()
extern void SR_get_SerializerCycleDetected_mFFE7721687271185764649CAAB7742FF3EBE21FD (void);
// 0x0000006E System.String System.SR::get_InvalidLeadingZeroInNumber()
extern void SR_get_InvalidLeadingZeroInNumber_m6BF7631527A1196B57B4E8A963A03B109EDA9A87 (void);
// 0x0000006F System.String System.SR::get_MetadataCannotParsePreservedObjectToImmutable()
extern void SR_get_MetadataCannotParsePreservedObjectToImmutable_m7EC3A2101BB813A5EE910A203FA9546C7E85CD9D (void);
// 0x00000070 System.String System.SR::get_MetadataDuplicateIdFound()
extern void SR_get_MetadataDuplicateIdFound_m48729069C940EBF6327DD830AD23EFF4A45B50F5 (void);
// 0x00000071 System.String System.SR::get_MetadataIdIsNotFirstProperty()
extern void SR_get_MetadataIdIsNotFirstProperty_m7A4D0D201F50ED06A3E7896FDCD21A10A6644B86 (void);
// 0x00000072 System.String System.SR::get_MetadataInvalidReferenceToValueType()
extern void SR_get_MetadataInvalidReferenceToValueType_m2EEC9DAD55468AEB45E9223F8F8A6BC2206271BF (void);
// 0x00000073 System.String System.SR::get_MetadataInvalidTokenAfterValues()
extern void SR_get_MetadataInvalidTokenAfterValues_m0EEB8F20C189BE3ED7AA9F09476FA77A9C830EC4 (void);
// 0x00000074 System.String System.SR::get_MetadataPreservedArrayFailed()
extern void SR_get_MetadataPreservedArrayFailed_mAA65C7CCABAE5ED4926E388DD43238124B4A38E9 (void);
// 0x00000075 System.String System.SR::get_MetadataPreservedArrayInvalidProperty()
extern void SR_get_MetadataPreservedArrayInvalidProperty_mB89B42C953D2CF70F3BB5BDB8BF8C67279591E9D (void);
// 0x00000076 System.String System.SR::get_MetadataPreservedArrayPropertyNotFound()
extern void SR_get_MetadataPreservedArrayPropertyNotFound_mE12E492A731C2BAE3910426B6AEB2EF09C554706 (void);
// 0x00000077 System.String System.SR::get_MetadataReferenceCannotContainOtherProperties()
extern void SR_get_MetadataReferenceCannotContainOtherProperties_m1DFC9A024F9BD3931AA5FD0BB743FC4D462DDCE2 (void);
// 0x00000078 System.String System.SR::get_MetadataReferenceNotFound()
extern void SR_get_MetadataReferenceNotFound_m8D45C95CA5000DF1FB5CE89D261D1B2DE5CBE290 (void);
// 0x00000079 System.String System.SR::get_MetadataValueWasNotString()
extern void SR_get_MetadataValueWasNotString_m475A3FA20675470B82F401EF804E4C48558040DF (void);
// 0x0000007A System.String System.SR::get_MetadataInvalidPropertyWithLeadingDollarSign()
extern void SR_get_MetadataInvalidPropertyWithLeadingDollarSign_m359A67E1B5F8433B09015726C6EF235AD046819F (void);
// 0x0000007B System.String System.SR::get_MultipleMembersBindWithConstructorParameter()
extern void SR_get_MultipleMembersBindWithConstructorParameter_mCAD92CB106A39E220CB6299AB523A4424B612D08 (void);
// 0x0000007C System.String System.SR::get_ConstructorParamIncompleteBinding()
extern void SR_get_ConstructorParamIncompleteBinding_m2CCDAC5A3D90147955966AA020D69D7FD9EF44A8 (void);
// 0x0000007D System.String System.SR::get_ConstructorMaxOf64Parameters()
extern void SR_get_ConstructorMaxOf64Parameters_mF375B56BADBD0B8ABEFC373D305E2725F40B6AE6 (void);
// 0x0000007E System.String System.SR::get_ObjectWithParameterizedCtorRefMetadataNotHonored()
extern void SR_get_ObjectWithParameterizedCtorRefMetadataNotHonored_m2D6CEEDF3A5B0CEA3E6838A7CACA524C924B4EE0 (void);
// 0x0000007F System.String System.SR::get_SerializerConverterFactoryReturnsNull()
extern void SR_get_SerializerConverterFactoryReturnsNull_m7DBC25C7DC67B14F11C41790FBFFCA05CF3DDBB2 (void);
// 0x00000080 System.String System.SR::get_SerializationNotSupportedParentType()
extern void SR_get_SerializationNotSupportedParentType_mDDE8D71E618315BF53158A7B57B5F2F4D852E03E (void);
// 0x00000081 System.String System.SR::get_ExtensionDataCannotBindToCtorParam()
extern void SR_get_ExtensionDataCannotBindToCtorParam_m58B18F22CED7F2C91949BE94587D11416A0099AD (void);
// 0x00000082 System.String System.SR::get_BufferMaximumSizeExceeded()
extern void SR_get_BufferMaximumSizeExceeded_m5E9134FD11FF952E124CA4F7B791222061F93E84 (void);
// 0x00000083 System.String System.SR::get_CannotSerializeInvalidType()
extern void SR_get_CannotSerializeInvalidType_m4D018B06F6C60234741517E11F15CE9CDBA6944B (void);
// 0x00000084 System.String System.SR::get_SerializeTypeInstanceNotSupported()
extern void SR_get_SerializeTypeInstanceNotSupported_m2D26AC49F1AEDE3712C8E81C64ACD78425C72E07 (void);
// 0x00000085 System.String System.SR::get_JsonIncludeOnNonPublicInvalid()
extern void SR_get_JsonIncludeOnNonPublicInvalid_m5AB7E007F3CF437C400FEEBFE8000B76A442521A (void);
// 0x00000086 System.String System.SR::get_CannotSerializeInvalidMember()
extern void SR_get_CannotSerializeInvalidMember_m16810AFDB270CA695FAD551B200CC818CD7A98DE (void);
// 0x00000087 System.String System.SR::get_CannotPopulateCollection()
extern void SR_get_CannotPopulateCollection_m45B711730FA7FD187B50EB486D99760786166AD6 (void);
// 0x00000088 System.String System.SR::get_FormatBoolean()
extern void SR_get_FormatBoolean_mC7C304F9CF8C80F0A9C94BB2AD0DC7FBEF29FAB0 (void);
// 0x00000089 System.String System.SR::get_DictionaryKeyTypeNotSupported()
extern void SR_get_DictionaryKeyTypeNotSupported_mEF919ABBACD824A7444A721193836258EFD50BC5 (void);
// 0x0000008A System.String System.SR::get_IgnoreConditionOnValueTypeInvalid()
extern void SR_get_IgnoreConditionOnValueTypeInvalid_m2881F0F05D89ED29DD2FD236424EECA72343618F (void);
// 0x0000008B System.String System.SR::get_NumberHandlingConverterMustBeBuiltIn()
extern void SR_get_NumberHandlingConverterMustBeBuiltIn_m088C7B0AB327313123566300C5BF4BF3CC95AC15 (void);
// 0x0000008C System.String System.SR::get_NumberHandlingOnPropertyTypeMustBeNumberOrCollection()
extern void SR_get_NumberHandlingOnPropertyTypeMustBeNumberOrCollection_m7F0517EDB2D65A409E4332D53029358E70E9881B (void);
// 0x0000008D System.String System.SR::get_ConverterCanConvertNullableRedundant()
extern void SR_get_ConverterCanConvertNullableRedundant_m9A7BC8BF50863F47042BC8040C0E482E9CB184F3 (void);
// 0x0000008E System.String System.SR::get_MetadataReferenceOfTypeCannotBeAssignedToType()
extern void SR_get_MetadataReferenceOfTypeCannotBeAssignedToType_mBF707435EF589D0735ED83754A20848E2FACED22 (void);
// 0x0000008F System.String System.SR::get_DeserializeUnableToAssignValue()
extern void SR_get_DeserializeUnableToAssignValue_m42D498C3505DF4B12CAB20B788F803258D4C4120 (void);
// 0x00000090 System.String System.SR::get_DeserializeUnableToAssignNull()
extern void SR_get_DeserializeUnableToAssignNull_m637A485DFDF37C776B4C366E21CCB18DD30E626D (void);
// 0x00000091 System.Void System.SR::.cctor()
extern void SR__cctor_m8D90FACF117AD47C9B5B644B8867CB82DF5047A8 (void);
// 0x00000092 System.Void System.Collections.Generic.ReferenceEqualityComparer::.ctor()
extern void ReferenceEqualityComparer__ctor_mD3DE60465F5D40F02215F150B29DBFD2E44BBDB8 (void);
// 0x00000093 System.Collections.Generic.ReferenceEqualityComparer System.Collections.Generic.ReferenceEqualityComparer::get_Instance()
extern void ReferenceEqualityComparer_get_Instance_m43B68CACB4FE50ABE65B13C224229C45CB7FDC9C (void);
// 0x00000094 System.Boolean System.Collections.Generic.ReferenceEqualityComparer::Equals(System.Object,System.Object)
extern void ReferenceEqualityComparer_Equals_m318279318BB973F0E7DAD80D891B4BB043E05B36 (void);
// 0x00000095 System.Int32 System.Collections.Generic.ReferenceEqualityComparer::GetHashCode(System.Object)
extern void ReferenceEqualityComparer_GetHashCode_m1566E710AA14E24C7EAF3C98116E4268A5024000 (void);
// 0x00000096 System.Void System.Collections.Generic.ReferenceEqualityComparer::.cctor()
extern void ReferenceEqualityComparer__cctor_m20BA2C2111D1D47CA7B643AA5200686DCEDBA196 (void);
// 0x00000097 System.Void System.Diagnostics.CodeAnalysis.DynamicallyAccessedMembersAttribute::.ctor(System.Diagnostics.CodeAnalysis.DynamicallyAccessedMemberTypes)
extern void DynamicallyAccessedMembersAttribute__ctor_mDBF0F1C7DE04C8B522241B8C64AE1EF300581AB2 (void);
// 0x00000098 System.Void System.Diagnostics.CodeAnalysis.DisallowNullAttribute::.ctor()
extern void DisallowNullAttribute__ctor_m828EAECBBC572B96840FDD59AB52545D6B5A604B (void);
// 0x00000099 System.Void System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute::.ctor(System.Boolean)
extern void MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66 (void);
// 0x0000009A System.Void System.Diagnostics.CodeAnalysis.NotNullWhenAttribute::.ctor(System.Boolean)
extern void NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13 (void);
// 0x0000009B System.Void System.Diagnostics.CodeAnalysis.DoesNotReturnAttribute::.ctor()
extern void DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D (void);
// 0x0000009C System.ReadOnlyMemory`1<T> System.Buffers.ArrayBufferWriter`1::get_WrittenMemory()
// 0x0000009D System.Int32 System.Buffers.ArrayBufferWriter`1::get_WrittenCount()
// 0x0000009E System.Int32 System.Buffers.ArrayBufferWriter`1::get_FreeCapacity()
// 0x0000009F System.Void System.Buffers.ArrayBufferWriter`1::Clear()
// 0x000000A0 System.Void System.Buffers.ArrayBufferWriter`1::Advance(System.Int32)
// 0x000000A1 System.Memory`1<T> System.Buffers.ArrayBufferWriter`1::GetMemory(System.Int32)
// 0x000000A2 System.Void System.Buffers.ArrayBufferWriter`1::CheckAndResizeBuffer(System.Int32)
// 0x000000A3 System.Void System.Buffers.ArrayBufferWriter`1::ThrowInvalidOperationException_AdvancedTooFar(System.Int32)
// 0x000000A4 System.Void System.Buffers.ArrayBufferWriter`1::ThrowOutOfMemoryException(System.UInt32)
// 0x000000A5 System.Void System.Text.Json.PooledByteBufferWriter::.ctor(System.Int32)
extern void PooledByteBufferWriter__ctor_m2BFCEA5F4BE8129352CD1FA340253322AB96C179 (void);
// 0x000000A6 System.ReadOnlyMemory`1<System.Byte> System.Text.Json.PooledByteBufferWriter::get_WrittenMemory()
extern void PooledByteBufferWriter_get_WrittenMemory_mEAC90C8BAB9357BCAAD2693956860E526BA7FAD1 (void);
// 0x000000A7 System.Void System.Text.Json.PooledByteBufferWriter::ClearHelper()
extern void PooledByteBufferWriter_ClearHelper_mE3AC6CCE9E333A079A9FA78F727A843C8442EE36 (void);
// 0x000000A8 System.Void System.Text.Json.PooledByteBufferWriter::Dispose()
extern void PooledByteBufferWriter_Dispose_m136536F82685598415188C0749D28096A3BAC83E (void);
// 0x000000A9 System.Void System.Text.Json.PooledByteBufferWriter::Advance(System.Int32)
extern void PooledByteBufferWriter_Advance_m17066E970C30D1281CBDCD42A262373B5C8FA7AD (void);
// 0x000000AA System.Memory`1<System.Byte> System.Text.Json.PooledByteBufferWriter::GetMemory(System.Int32)
extern void PooledByteBufferWriter_GetMemory_mEE7A1F1C92427018DE967C6DFB000382D8D9BBD9 (void);
// 0x000000AB System.Void System.Text.Json.PooledByteBufferWriter::CheckAndResizeBuffer(System.Int32)
extern void PooledByteBufferWriter_CheckAndResizeBuffer_m722C42DADC2B79C901D6D052A78EE70612742C25 (void);
// 0x000000AC System.Void System.Text.Json.ThrowHelper::ThrowOutOfMemoryException_BufferMaximumSizeExceeded(System.UInt32)
extern void ThrowHelper_ThrowOutOfMemoryException_BufferMaximumSizeExceeded_m6AACBBF084A0F2C5949C350FEC33B41F950E2A61 (void);
// 0x000000AD System.ArgumentOutOfRangeException System.Text.Json.ThrowHelper::GetArgumentOutOfRangeException_MaxDepthMustBePositive(System.String)
extern void ThrowHelper_GetArgumentOutOfRangeException_MaxDepthMustBePositive_m61E323FF2E23F64ED4780B8B7D43E0982F68D5B2 (void);
// 0x000000AE System.ArgumentOutOfRangeException System.Text.Json.ThrowHelper::GetArgumentOutOfRangeException(System.String,System.String)
extern void ThrowHelper_GetArgumentOutOfRangeException_m12F1DB29BB3A4C3EF7387FDEA9BC653DAF915967 (void);
// 0x000000AF System.ArgumentOutOfRangeException System.Text.Json.ThrowHelper::GetArgumentOutOfRangeException_CommentEnumMustBeInRange(System.String)
extern void ThrowHelper_GetArgumentOutOfRangeException_CommentEnumMustBeInRange_m314794343C743984A26372AABA3C20CCC7C8007F (void);
// 0x000000B0 System.ArgumentException System.Text.Json.ThrowHelper::GetArgumentException(System.String)
extern void ThrowHelper_GetArgumentException_mF411D1E286E8443C3DF3A14535B9F7B16B045C3E (void);
// 0x000000B1 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_PropertyNameTooLarge(System.Int32)
extern void ThrowHelper_ThrowArgumentException_PropertyNameTooLarge_m77F825E6FA2ABACC4885601C5C87D9309FF8F0AD (void);
// 0x000000B2 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_ValueTooLarge(System.Int32)
extern void ThrowHelper_ThrowArgumentException_ValueTooLarge_mB6D13E7E631571FE910659E29715926B40210CE2 (void);
// 0x000000B3 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_ValueNotSupported()
extern void ThrowHelper_ThrowArgumentException_ValueNotSupported_m28B4A5E1B7773753D54D7D3D00CA9EF64B8D9BD4 (void);
// 0x000000B4 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_NeedLargerSpan()
extern void ThrowHelper_ThrowInvalidOperationException_NeedLargerSpan_m42791059B9B5492C16E61273D3F33692EAEF9B1B (void);
// 0x000000B5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException(System.Int32)
extern void ThrowHelper_ThrowInvalidOperationException_mA7A541529B4A42EF018C2E86CB4C896BD8900615 (void);
// 0x000000B6 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException(System.String)
extern void ThrowHelper_ThrowInvalidOperationException_mAEE102BDC978858CF1E7B35197714BC9126D0B34 (void);
// 0x000000B7 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.String)
extern void ThrowHelper_GetInvalidOperationException_m582DC8908C0D711A28CE7AE688532F184EF123B6 (void);
// 0x000000B8 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedNumber(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedNumber_m5CC60EAE038846B66BC7505CEFD0B1BE0D1E1458 (void);
// 0x000000B9 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedBoolean(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedBoolean_m234A5FDADB32EB98D7275302ADCEC155B867BDD5 (void);
// 0x000000BA System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedString(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedString_m7A2BC67F8A810C0EAC070DF8908836988652E6C4 (void);
// 0x000000BB System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_CannotSkipOnPartial()
extern void ThrowHelper_GetInvalidOperationException_CannotSkipOnPartial_m697CEBF1A69D299EA92AACEF8059AF57D6344826 (void);
// 0x000000BC System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.String,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_m064819D6786C949F9E6E08EE8E7F403AB762FEFE (void);
// 0x000000BD System.InvalidOperationException System.Text.Json.ThrowHelper::GetJsonElementWrongTypeException(System.Text.Json.JsonTokenType,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetJsonElementWrongTypeException_m2888DF6174A2B338E3B9DEE4066AC35BAB80B879 (void);
// 0x000000BE System.InvalidOperationException System.Text.Json.ThrowHelper::GetJsonElementWrongTypeException(System.Text.Json.JsonValueKind,System.Text.Json.JsonValueKind)
extern void ThrowHelper_GetJsonElementWrongTypeException_mE1A4B1DD7EA07B76C467EDE5EDB770C23AC5D0D3 (void);
// 0x000000BF System.Void System.Text.Json.ThrowHelper::ThrowJsonReaderException(System.Text.Json.Utf8JsonReader&,System.Text.Json.ExceptionResource,System.Byte,System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_ThrowJsonReaderException_mF1FBE8AEF1017A0B089C3B41D3C381DD4C1428BA (void);
// 0x000000C0 System.Text.Json.JsonException System.Text.Json.ThrowHelper::GetJsonReaderException(System.Text.Json.Utf8JsonReader&,System.Text.Json.ExceptionResource,System.Byte,System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_GetJsonReaderException_m249FFD6F2F5BFAB6CB3E1BFE03B57AC158BF8B66 (void);
// 0x000000C1 System.Boolean System.Text.Json.ThrowHelper::IsPrintable(System.Byte)
extern void ThrowHelper_IsPrintable_mDB633E61D3835B0FEA8FD61F63104BA9DFB55323 (void);
// 0x000000C2 System.String System.Text.Json.ThrowHelper::GetPrintableString(System.Byte)
extern void ThrowHelper_GetPrintableString_m76688EFAA55DD7C6CC1732540772879F29818544 (void);
// 0x000000C3 System.String System.Text.Json.ThrowHelper::GetResourceString(System.Text.Json.Utf8JsonReader&,System.Text.Json.ExceptionResource,System.Byte,System.String)
extern void ThrowHelper_GetResourceString_mEAF650CD41629EDBEF44935376339BBF6F864F05 (void);
// 0x000000C4 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException(System.Text.Json.ExceptionResource,System.Int32,System.Byte,System.Text.Json.JsonTokenType)
extern void ThrowHelper_ThrowInvalidOperationException_m388FA9CAE1E6661558861A241C547A9E0DF887BC (void);
// 0x000000C5 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_InvalidUTF8(System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_ThrowArgumentException_InvalidUTF8_m5D596F01AA0D55AF6DA151FD0B5C1511090E1B47 (void);
// 0x000000C6 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_InvalidUTF16(System.Int32)
extern void ThrowHelper_ThrowArgumentException_InvalidUTF16_mFDF55F6C19EC28ED0408D32C4ADC08ECAC22245B (void);
// 0x000000C7 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ReadInvalidUTF16(System.Int32)
extern void ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m95A110D95E052A207FF2F081DF1880159CAD85D0 (void);
// 0x000000C8 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ReadInvalidUTF16()
extern void ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_mDF46BDC21FFF5BA6D77F4B3BD33C73B5948D6665 (void);
// 0x000000C9 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ReadInvalidUTF8(System.Text.DecoderFallbackException)
extern void ThrowHelper_GetInvalidOperationException_ReadInvalidUTF8_mCD654D469B7C7F7C15E7077698F3B40A4C98F5CE (void);
// 0x000000CA System.ArgumentException System.Text.Json.ThrowHelper::GetArgumentException_ReadInvalidUTF16(System.Text.EncoderFallbackException)
extern void ThrowHelper_GetArgumentException_ReadInvalidUTF16_mAC507137EE56EF11514CAFCD1E047CDEBCE88F64 (void);
// 0x000000CB System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.String,System.Exception)
extern void ThrowHelper_GetInvalidOperationException_m6C35ECC9021533FEA1446E3F96CA367FC7C02CD6 (void);
// 0x000000CC System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.Text.Json.ExceptionResource,System.Int32,System.Byte,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_m16E3F279B21CF83224DBCDDCD9CCA52242A3DA5B (void);
// 0x000000CD System.String System.Text.Json.ThrowHelper::GetResourceString(System.Text.Json.ExceptionResource,System.Int32,System.Byte,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetResourceString_m790996CE2B876233A019A3880DBEBF498F2AD843 (void);
// 0x000000CE System.FormatException System.Text.Json.ThrowHelper::GetFormatException()
extern void ThrowHelper_GetFormatException_m7843E7FE6ECC25CA77D86E9A38B13D6526E31CB5 (void);
// 0x000000CF System.FormatException System.Text.Json.ThrowHelper::GetFormatException(System.Text.Json.NumericType)
extern void ThrowHelper_GetFormatException_m0C1014E4627199E87C62C7C95383E92136C2B716 (void);
// 0x000000D0 System.FormatException System.Text.Json.ThrowHelper::GetFormatException(System.Text.Json.DataType)
extern void ThrowHelper_GetFormatException_m8F999978C10BB03985DD61295AF41DF352F95179 (void);
// 0x000000D1 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedChar(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedChar_mFF8F998633112C890EE11CD32DE6632557B487CA (void);
// 0x000000D2 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_SerializationNotSupported(System.Type)
extern void ThrowHelper_ThrowNotSupportedException_SerializationNotSupported_mF73201276822CF6CC001E95CBA48CD83D6109062 (void);
// 0x000000D3 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_ConstructorMaxOf64Parameters(System.Reflection.ConstructorInfo,System.Type)
extern void ThrowHelper_ThrowNotSupportedException_ConstructorMaxOf64Parameters_mCDB2E3AC4FFD00386CBAA2DEFDD8AE1CD5BA4F1D (void);
// 0x000000D4 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_DictionaryKeyTypeNotSupported(System.Type)
extern void ThrowHelper_ThrowNotSupportedException_DictionaryKeyTypeNotSupported_m27903431590452F80C7758007DFD55D8B7A7E611 (void);
// 0x000000D5 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_DeserializeUnableToConvertValue(System.Type)
extern void ThrowHelper_ThrowJsonException_DeserializeUnableToConvertValue_m96FE8F452BB614BAB85A7E20BA40E22690C93F7B (void);
// 0x000000D6 System.Void System.Text.Json.ThrowHelper::ThrowInvalidCastException_DeserializeUnableToAssignValue(System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidCastException_DeserializeUnableToAssignValue_m4C68F1D7ED3EDC35AFC765960D3A679E34C51630 (void);
// 0x000000D7 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_DeserializeUnableToAssignNull(System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_DeserializeUnableToAssignNull_mCBADC1DEFC6B20B8679C4EE5FE83BC5098203B9A (void);
// 0x000000D8 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_SerializationConverterRead(System.Text.Json.Serialization.JsonConverter)
extern void ThrowHelper_ThrowJsonException_SerializationConverterRead_m4A28E5B4CC04FD16BA7CAA84D1B021F92EE220D7 (void);
// 0x000000D9 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_SerializationConverterWrite(System.Text.Json.Serialization.JsonConverter)
extern void ThrowHelper_ThrowJsonException_SerializationConverterWrite_mD0997F6FA916D1E612AB3C737A23612AD9297164 (void);
// 0x000000DA System.Void System.Text.Json.ThrowHelper::ThrowJsonException_SerializerCycleDetected(System.Int32)
extern void ThrowHelper_ThrowJsonException_SerializerCycleDetected_mAA15526A580E83EC7EDA47D50598EBD0D0D8E438 (void);
// 0x000000DB System.Void System.Text.Json.ThrowHelper::ThrowJsonException(System.String)
extern void ThrowHelper_ThrowJsonException_m973F42F766807F7E7BBC203E354BA06CA8EA055B (void);
// 0x000000DC System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_CannotSerializeInvalidType(System.Type,System.Type,System.Reflection.MemberInfo)
extern void ThrowHelper_ThrowInvalidOperationException_CannotSerializeInvalidType_mD2FD606E0497CF4BA9029A955923E121356C04D3 (void);
// 0x000000DD System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationConverterNotCompatible(System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationConverterNotCompatible_mC298E0922D7F2B7B68B13B4D3E5743C766D8F84D (void);
// 0x000000DE System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid(System.Type,System.Reflection.MemberInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid_mA2E9DC408E16194EFCECD1A1371C32DE747E1339 (void);
// 0x000000DF System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible(System.Type,System.Reflection.MemberInfo,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible_m6065CAB5382ABA2091B10F92E69FF90F37C59BFC (void);
// 0x000000E0 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerOptionsImmutable()
extern void ThrowHelper_ThrowInvalidOperationException_SerializerOptionsImmutable_mBEFF1850BBA433ADD5B20540B3C7594BEB6ABE0F (void);
// 0x000000E1 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerPropertyNameConflict(System.Type,System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameConflict_mF30BA34FBED9BA4C3F4F4CA6E834080C5AFE1CB3 (void);
// 0x000000E2 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerPropertyNameNull(System.Type,System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameNull_m9023D53865B4D6A638CC630D5D77584FE074E8CC (void);
// 0x000000E3 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_NamingPolicyReturnNull(System.Text.Json.JsonNamingPolicy)
extern void ThrowHelper_ThrowInvalidOperationException_NamingPolicyReturnNull_mB9EB953C12D35F4E93345C912DBF23DB636451D5 (void);
// 0x000000E4 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull(System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull_m5F731D2B8C69BD2CA0359516616C34D5ECDB80D8 (void);
// 0x000000E5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters(System.Type,System.String,System.String,System.String,System.Reflection.ConstructorInfo)
extern void ThrowHelper_ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters_m05E79160F6B9CD59D30D8906BB2987EED6954601 (void);
// 0x000000E6 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ConstructorParameterIncompleteBinding(System.Reflection.ConstructorInfo,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_ConstructorParameterIncompleteBinding_mB139EAC290271F39E1736F7F1E9E2FF9B14C297F (void);
// 0x000000E7 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam(System.Reflection.MemberInfo,System.Type,System.Reflection.ConstructorInfo)
extern void ThrowHelper_ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam_m4872A1B0FF96357B0C6B638C4EBA9E1761880D2E (void);
// 0x000000E8 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid(System.Reflection.MemberInfo,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid_m603A1316F663B20F69E80D9E3811B452E919570E (void);
// 0x000000E9 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid(System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid_mB9A9F19F8AF43DC125EA07D32794D98B98F19E20 (void);
// 0x000000EA System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid(System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid_mFC629CF3501D8B07CADE0DB5D3A96AFC3A8D517B (void);
// 0x000000EB System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ConverterCanConvertNullableRedundant(System.Type,System.Text.Json.Serialization.JsonConverter)
extern void ThrowHelper_ThrowInvalidOperationException_ConverterCanConvertNullableRedundant_mD154A895AB1495392DCCBA759457612D5A7C6E66 (void);
// 0x000000EC System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored_m5EE618649BD8D2CBB732C6F0292DC3C4E0DFF91F (void);
// 0x000000ED System.Void System.Text.Json.ThrowHelper::ReThrowWithPath(System.Text.Json.ReadStack&,System.Text.Json.JsonReaderException)
extern void ThrowHelper_ReThrowWithPath_m8C8F65479D5823F1F0A53F62D329626CB6590681 (void);
// 0x000000EE System.Void System.Text.Json.ThrowHelper::ReThrowWithPath(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Exception)
extern void ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A (void);
// 0x000000EF System.Void System.Text.Json.ThrowHelper::AddJsonExceptionInformation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonException)
extern void ThrowHelper_AddJsonExceptionInformation_m27AA11CA530AE35B24E4AD7E1BF42051046CDD75 (void);
// 0x000000F0 System.Void System.Text.Json.ThrowHelper::ReThrowWithPath(System.Text.Json.WriteStack&,System.Exception)
extern void ThrowHelper_ReThrowWithPath_m60D724FB73CD33130D2CF820A08AE5332F7AFBE8 (void);
// 0x000000F1 System.Void System.Text.Json.ThrowHelper::AddJsonExceptionInformation(System.Text.Json.WriteStack&,System.Text.Json.JsonException)
extern void ThrowHelper_AddJsonExceptionInformation_m19A08342AAA7D86A83B9A636BEA522C2F2232918 (void);
// 0x000000F2 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDuplicateAttribute(System.Type,System.Type,System.Reflection.MemberInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateAttribute_m6431717BAB38FE1C8D3A06DF615F73301FA5125D (void);
// 0x000000F3 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDuplicateTypeAttribute(System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_m5D557E2F44F1A96A25337111B7BC2F1F6D0F5AF7 (void);
// 0x000000F4 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDuplicateTypeAttribute(System.Type)
// 0x000000F5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid(System.Type,System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid_mDECC955E535EBF5416567323A734BA671818A50A (void);
// 0x000000F6 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.NotSupportedException)
extern void ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3 (void);
// 0x000000F7 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException(System.Text.Json.WriteStack&,System.NotSupportedException)
extern void ThrowHelper_ThrowNotSupportedException_mFBBEF762422B3346268279581FA1372DE9B433FB (void);
// 0x000000F8 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_DeserializeNoConstructor(System.Type,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowNotSupportedException_DeserializeNoConstructor_m0316A2404C45BB2BBD6532E76FE9F384DB113010 (void);
// 0x000000F9 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_CannotPopulateCollection(System.Type,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowNotSupportedException_CannotPopulateCollection_m18F801EC086A905E56F9380CFA8E011746E95DD1 (void);
// 0x000000FA System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataValuesInvalidToken(System.Text.Json.JsonTokenType)
extern void ThrowHelper_ThrowJsonException_MetadataValuesInvalidToken_mDC0FA9D497DFC575CCFD6422C843790C64656AEB (void);
// 0x000000FB System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataReferenceNotFound(System.String)
extern void ThrowHelper_ThrowJsonException_MetadataReferenceNotFound_m07F016A3E7895EC7753C09442854C4950A77F142 (void);
// 0x000000FC System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataValueWasNotString(System.Text.Json.JsonTokenType)
extern void ThrowHelper_ThrowJsonException_MetadataValueWasNotString_m9867E87D56EF64AABEE3844B4929587C5CB49A22 (void);
// 0x000000FD System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataValueWasNotString(System.Text.Json.JsonValueKind)
extern void ThrowHelper_ThrowJsonException_MetadataValueWasNotString_mC0D5A8277188661CECD17D5BAF5E7934D691DB34 (void);
// 0x000000FE System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mCF9676414315C513337B5E8644F0A4927FD99F75 (void);
// 0x000000FF System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties()
extern void ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mB36E6C2A52F6DBEDC8F30D9FCAF626F29A8B1399 (void);
// 0x00000100 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataIdIsNotFirstProperty(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowJsonException_MetadataIdIsNotFirstProperty_m845C5039FF51C4DF91792496C8A5A660AC75BC07 (void);
// 0x00000101 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataMissingIdBeforeValues(System.Text.Json.ReadStack&,System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_ThrowJsonException_MetadataMissingIdBeforeValues_mE063C70456CE239D60E25F7381022F36523B764E (void);
// 0x00000102 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
extern void ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_m338531C4877DDB86A6298075523EE839FD80D4E6 (void);
// 0x00000103 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataDuplicateIdFound(System.String)
extern void ThrowHelper_ThrowJsonException_MetadataDuplicateIdFound_m168ECF07F07194EAC525711DE4894FBF0BF44C59 (void);
// 0x00000104 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataInvalidReferenceToValueType(System.Type)
extern void ThrowHelper_ThrowJsonException_MetadataInvalidReferenceToValueType_m6AACE011A6BBB6D2092908770A52F33F538FE0F3 (void);
// 0x00000105 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataPreservedArrayInvalidProperty(System.Text.Json.ReadStack&,System.Type,System.Text.Json.Utf8JsonReader&)
extern void ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m5DDDF08403852837CA5A82CD77FA084374975BF8 (void);
// 0x00000106 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataPreservedArrayValuesNotFound(System.Text.Json.ReadStack&,System.Type)
extern void ThrowHelper_ThrowJsonException_MetadataPreservedArrayValuesNotFound_mC760A54801B7D5833BB0BBB73CD9D331137C2DB0 (void);
// 0x00000107 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable(System.Type)
extern void ThrowHelper_ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable_m2541C3387E3799EA00D221A1E9D96E5748B4C2D5 (void);
// 0x00000108 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType(System.String,System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType_m59D2BC0FFD79A685E8D72FA1ED73341DCB4828D2 (void);
// 0x00000109 System.Void System.Text.Json.ThrowHelper::ThrowUnexpectedMetadataException(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowUnexpectedMetadataException_m66CB72849A9036C64AC1B6C05F19E46C0277F47C (void);
// 0x0000010A System.Int32 System.Text.Json.BitStack::get_CurrentDepth()
extern void BitStack_get_CurrentDepth_m89D98204DBF12922D8C94B3447F28558A607561E (void);
// 0x0000010B System.Void System.Text.Json.BitStack::PushTrue()
extern void BitStack_PushTrue_m3E753BB8ECFB2193C4F9097FFBA0FB9C502DCCC7 (void);
// 0x0000010C System.Void System.Text.Json.BitStack::PushFalse()
extern void BitStack_PushFalse_mDD8A80022173EAF25EC74EFF7CFA03A6054F8D25 (void);
// 0x0000010D System.Void System.Text.Json.BitStack::PushToArray(System.Boolean)
extern void BitStack_PushToArray_m20AF975C1EFDCFAA0D410D815A8B062A9CCECA23 (void);
// 0x0000010E System.Boolean System.Text.Json.BitStack::Pop()
extern void BitStack_Pop_m65E1670A3A560768AC7DD19C52A9D5B8C4CE2482 (void);
// 0x0000010F System.Boolean System.Text.Json.BitStack::PopFromArray()
extern void BitStack_PopFromArray_m205D4F9EEB5B51B6074124CEE2F8974AA2A950DF (void);
// 0x00000110 System.Void System.Text.Json.BitStack::DoubleArray(System.Int32)
extern void BitStack_DoubleArray_m589E01AA096E64AE9187C458D962BE8CA0E92051 (void);
// 0x00000111 System.Void System.Text.Json.BitStack::SetFirstBit()
extern void BitStack_SetFirstBit_m9F284C76F69FBAE879387EE0B8B190B6967840BE (void);
// 0x00000112 System.Void System.Text.Json.BitStack::ResetFirstBit()
extern void BitStack_ResetFirstBit_m6A4CD054DF57522D30866089E313E8353A6889A3 (void);
// 0x00000113 System.Int32 System.Text.Json.BitStack::Div32Rem(System.Int32,System.Int32&)
extern void BitStack_Div32Rem_mBCFE7A3E6E7485C5BCBD8CA7E0FCC4E55AE5FA5C (void);
// 0x00000114 System.Boolean System.Text.Json.JsonDocument::get_IsDisposable()
extern void JsonDocument_get_IsDisposable_m32674A8E6A4F43BAAB12D25D7AB4B1F8202D6337 (void);
// 0x00000115 System.Text.Json.JsonElement System.Text.Json.JsonDocument::get_RootElement()
extern void JsonDocument_get_RootElement_mC37C0BB4BB812EDF790A637056A706B018B47F7C (void);
// 0x00000116 System.Void System.Text.Json.JsonDocument::.ctor(System.ReadOnlyMemory`1<System.Byte>,System.Text.Json.JsonDocument/MetadataDb,System.Byte[],System.Boolean)
extern void JsonDocument__ctor_m7E52FD9EBA1C3226C56E20AA0DA4B202D1242B03 (void);
// 0x00000117 System.Void System.Text.Json.JsonDocument::Dispose()
extern void JsonDocument_Dispose_mFDC6DAE4A8A489669A9E94D725E44011386516C3 (void);
// 0x00000118 System.Void System.Text.Json.JsonDocument::WriteTo(System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteTo_m4F0091C2294C11E823F07D253C25BA2DD720FDB1 (void);
// 0x00000119 System.Text.Json.JsonTokenType System.Text.Json.JsonDocument::GetJsonTokenType(System.Int32)
extern void JsonDocument_GetJsonTokenType_mA77779B2ACF3378B18E237FCBE42EA20CE1F9E54 (void);
// 0x0000011A System.Int32 System.Text.Json.JsonDocument::GetEndIndex(System.Int32,System.Boolean)
extern void JsonDocument_GetEndIndex_mCB49D234EB50A854CC608B35B030D317C6478A47 (void);
// 0x0000011B System.ReadOnlyMemory`1<System.Byte> System.Text.Json.JsonDocument::GetRawValue(System.Int32,System.Boolean)
extern void JsonDocument_GetRawValue_mE28C290DD5A1C03AC1F21BAD7D18F84C9CB60664 (void);
// 0x0000011C System.ReadOnlyMemory`1<System.Byte> System.Text.Json.JsonDocument::GetPropertyRawValue(System.Int32)
extern void JsonDocument_GetPropertyRawValue_m63CA89B4668E2CA7CE2871C0FEE4059D84CA7487 (void);
// 0x0000011D System.String System.Text.Json.JsonDocument::GetString(System.Int32,System.Text.Json.JsonTokenType)
extern void JsonDocument_GetString_mBDDE86EC31F3DFC12B55F90D8DEBBBAC90216990 (void);
// 0x0000011E System.Boolean System.Text.Json.JsonDocument::TextEquals(System.Int32,System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Boolean)
extern void JsonDocument_TextEquals_m010068C6214B0F779FEE248BFE48C776F613794D (void);
// 0x0000011F System.Boolean System.Text.Json.JsonDocument::TryGetValue(System.Int32,System.Int32&)
extern void JsonDocument_TryGetValue_mAFAC3A2D30BB0F9A8257AD2E613700C854D858B9 (void);
// 0x00000120 System.String System.Text.Json.JsonDocument::GetRawValueAsString(System.Int32)
extern void JsonDocument_GetRawValueAsString_m2ED5DAD23249C1D3CA77F67F89B9D0ABAEBC3EA3 (void);
// 0x00000121 System.String System.Text.Json.JsonDocument::GetPropertyRawValueAsString(System.Int32)
extern void JsonDocument_GetPropertyRawValueAsString_mBD94E7344807BB331156514E29A7E62153B3A5FE (void);
// 0x00000122 System.Text.Json.JsonElement System.Text.Json.JsonDocument::CloneElement(System.Int32)
extern void JsonDocument_CloneElement_m7ED18578F6EE37AD3BFC073B91EFB926D3D53266 (void);
// 0x00000123 System.Void System.Text.Json.JsonDocument::WriteElementTo(System.Int32,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteElementTo_m84378E2AA9142CD0A2CC706AFB40E7EA11AD2EDE (void);
// 0x00000124 System.Void System.Text.Json.JsonDocument::WriteComplexElement(System.Int32,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteComplexElement_mB3A1E8016B754D9BC465328E18AA16CE12B208D9 (void);
// 0x00000125 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonDocument::UnescapeString(System.Text.Json.JsonDocument/DbRow&,System.ArraySegment`1<System.Byte>&)
extern void JsonDocument_UnescapeString_m2DF634F6A71C280B6D0B94CDFFA6B1889640FBA5 (void);
// 0x00000126 System.Void System.Text.Json.JsonDocument::ClearAndReturn(System.ArraySegment`1<System.Byte>)
extern void JsonDocument_ClearAndReturn_m3EDBAFCD9DD7E564BB515F4605E70E1E2F64BC95 (void);
// 0x00000127 System.Void System.Text.Json.JsonDocument::WritePropertyName(System.Text.Json.JsonDocument/DbRow&,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WritePropertyName_mD6770EEC4513C8606F5A55BFD29B59FE9B8B486F (void);
// 0x00000128 System.Void System.Text.Json.JsonDocument::WriteString(System.Text.Json.JsonDocument/DbRow&,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteString_m97C83763877F4E016B011EBD62E8B7D975D2E6C1 (void);
// 0x00000129 System.Void System.Text.Json.JsonDocument::Parse(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonReaderOptions,System.Text.Json.JsonDocument/MetadataDb&,System.Text.Json.JsonDocument/StackRowStack&)
extern void JsonDocument_Parse_mFC0B173CCBC2F2ADD9040336FCB3B0D8AA8F743E (void);
// 0x0000012A System.Void System.Text.Json.JsonDocument::CheckNotDisposed()
extern void JsonDocument_CheckNotDisposed_m68165D4ED110E314C9A451131C85C5FB543BEEA1 (void);
// 0x0000012B System.Void System.Text.Json.JsonDocument::CheckExpectedType(System.Text.Json.JsonTokenType,System.Text.Json.JsonTokenType)
extern void JsonDocument_CheckExpectedType_m93425EFAD920FE044D8B62494000B3DD1258FBE6 (void);
// 0x0000012C System.Void System.Text.Json.JsonDocument::CheckSupportedOptions(System.Text.Json.JsonReaderOptions,System.String)
extern void JsonDocument_CheckSupportedOptions_m296FE175CC445F992C47DAD6ACC0C6B31C5D490D (void);
// 0x0000012D System.Text.Json.JsonDocument System.Text.Json.JsonDocument::Parse(System.ReadOnlyMemory`1<System.Char>,System.Text.Json.JsonDocumentOptions)
extern void JsonDocument_Parse_mF51C731F472CB28C445CE4FCD0F9D6D7ACBAA2DB (void);
// 0x0000012E System.Text.Json.JsonDocument System.Text.Json.JsonDocument::Parse(System.String,System.Text.Json.JsonDocumentOptions)
extern void JsonDocument_Parse_m0C451D2F8DE35FD2D93E4F76D8DEB3FED0784504 (void);
// 0x0000012F System.Text.Json.JsonDocument System.Text.Json.JsonDocument::ParseValue(System.Text.Json.Utf8JsonReader&)
extern void JsonDocument_ParseValue_mAFD1AF7093FB6B8974C2C4ECC111F8438E0DA731 (void);
// 0x00000130 System.Boolean System.Text.Json.JsonDocument::TryParseValue(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonDocument&,System.Boolean)
extern void JsonDocument_TryParseValue_m376E476DA7C90522D73A2BCA568CC00ECD2EBBCC (void);
// 0x00000131 System.Text.Json.JsonDocument System.Text.Json.JsonDocument::Parse(System.ReadOnlyMemory`1<System.Byte>,System.Text.Json.JsonReaderOptions,System.Byte[])
extern void JsonDocument_Parse_mDCD23B7F0D42C0481828DAA5ACCDEF6DFC858BE1 (void);
// 0x00000132 System.Boolean System.Text.Json.JsonDocument::TryGetNamedPropertyValue(System.Int32,System.ReadOnlySpan`1<System.Char>,System.Text.Json.JsonElement&)
extern void JsonDocument_TryGetNamedPropertyValue_mF6E1B17BCA05976B537A781DFB887D83B2C1686A (void);
// 0x00000133 System.Boolean System.Text.Json.JsonDocument::TryGetNamedPropertyValue(System.Int32,System.Int32,System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonElement&)
extern void JsonDocument_TryGetNamedPropertyValue_m17E846EC263CBC7083EFF948D0A71BF4D5B767EA (void);
// 0x00000134 System.Int32 System.Text.Json.JsonDocument/DbRow::get_Location()
extern void DbRow_get_Location_m64A1FE8677C03A2ED3384727229059262AD3F149 (void);
// 0x00000135 System.Int32 System.Text.Json.JsonDocument/DbRow::get_SizeOrLength()
extern void DbRow_get_SizeOrLength_mD035DFC369AC701D3930C33A159A067B00913772 (void);
// 0x00000136 System.Boolean System.Text.Json.JsonDocument/DbRow::get_IsUnknownSize()
extern void DbRow_get_IsUnknownSize_mAB1898B5C44A75E8F3F5737B39E22A9829052B9E (void);
// 0x00000137 System.Boolean System.Text.Json.JsonDocument/DbRow::get_HasComplexChildren()
extern void DbRow_get_HasComplexChildren_m7FB54FB106AA84F5168A030E00E21B2687C58D09 (void);
// 0x00000138 System.Int32 System.Text.Json.JsonDocument/DbRow::get_NumberOfRows()
extern void DbRow_get_NumberOfRows_mB356515E008E677E4E79834F315165BD49A825DF (void);
// 0x00000139 System.Text.Json.JsonTokenType System.Text.Json.JsonDocument/DbRow::get_TokenType()
extern void DbRow_get_TokenType_m25D3999BC52C9991FC22A97FEC677997068D1E7B (void);
// 0x0000013A System.Void System.Text.Json.JsonDocument/DbRow::.ctor(System.Text.Json.JsonTokenType,System.Int32,System.Int32)
extern void DbRow__ctor_m83855B245AEDA3542D69D4181E26D625780729FF (void);
// 0x0000013B System.Boolean System.Text.Json.JsonDocument/DbRow::get_IsSimpleValue()
extern void DbRow_get_IsSimpleValue_mF9C4D4A9AA674E10C15D23BFDCEE3B7A9CE78638 (void);
// 0x0000013C System.Int32 System.Text.Json.JsonDocument/MetadataDb::get_Length()
extern void MetadataDb_get_Length_m358C82F2ACD118F1A31750A408DE4A80D9706AC1 (void);
// 0x0000013D System.Void System.Text.Json.JsonDocument/MetadataDb::set_Length(System.Int32)
extern void MetadataDb_set_Length_mDEED679131C22B2B98BD28E71CABB539B01E79C7 (void);
// 0x0000013E System.Void System.Text.Json.JsonDocument/MetadataDb::.ctor(System.Byte[])
extern void MetadataDb__ctor_mF5690ADF4EA9661465EF315D0505E445C6E222FB (void);
// 0x0000013F System.Void System.Text.Json.JsonDocument/MetadataDb::.ctor(System.Int32)
extern void MetadataDb__ctor_mADDFB275C40C9946CEFB568AE37356AEB5FBE14D (void);
// 0x00000140 System.Void System.Text.Json.JsonDocument/MetadataDb::Dispose()
extern void MetadataDb_Dispose_m65983FCE3C671C0E078BA5B7192D66EA6224D218 (void);
// 0x00000141 System.Void System.Text.Json.JsonDocument/MetadataDb::TrimExcess()
extern void MetadataDb_TrimExcess_m4BAD9F80840A78E601E56F2EAE6822A02267A1CD (void);
// 0x00000142 System.Void System.Text.Json.JsonDocument/MetadataDb::Append(System.Text.Json.JsonTokenType,System.Int32,System.Int32)
extern void MetadataDb_Append_m70BEF3B5C094C35F03E20E23CABEF3905616B61A (void);
// 0x00000143 System.Void System.Text.Json.JsonDocument/MetadataDb::Enlarge()
extern void MetadataDb_Enlarge_m1D564D4B6A73CB588BBACAD06E20CAC2CC87BE91 (void);
// 0x00000144 System.Void System.Text.Json.JsonDocument/MetadataDb::SetLength(System.Int32,System.Int32)
extern void MetadataDb_SetLength_mF874FB9D92FF15E65645B752D2F16693FD85EA53 (void);
// 0x00000145 System.Void System.Text.Json.JsonDocument/MetadataDb::SetNumberOfRows(System.Int32,System.Int32)
extern void MetadataDb_SetNumberOfRows_mE9A9288F6CFE360E85CC8E0A50AE964D43C470E2 (void);
// 0x00000146 System.Void System.Text.Json.JsonDocument/MetadataDb::SetHasComplexChildren(System.Int32)
extern void MetadataDb_SetHasComplexChildren_m23B89D6C169828F97B1138999C88AD2C334E46D9 (void);
// 0x00000147 System.Int32 System.Text.Json.JsonDocument/MetadataDb::FindIndexOfFirstUnsetSizeOrLength(System.Text.Json.JsonTokenType)
extern void MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m93260D165FFDE321BC10AEB51ADA16EA571BDA92 (void);
// 0x00000148 System.Int32 System.Text.Json.JsonDocument/MetadataDb::FindOpenElement(System.Text.Json.JsonTokenType)
extern void MetadataDb_FindOpenElement_m764A0CA02C9B76F9FB4501BEC77D157A87ADCC8C (void);
// 0x00000149 System.Text.Json.JsonDocument/DbRow System.Text.Json.JsonDocument/MetadataDb::Get(System.Int32)
extern void MetadataDb_Get_m54EC69D0C0A5AFA94790823E043C9C5AF611E879 (void);
// 0x0000014A System.Text.Json.JsonTokenType System.Text.Json.JsonDocument/MetadataDb::GetJsonTokenType(System.Int32)
extern void MetadataDb_GetJsonTokenType_mFE77CF70D471447224C7088BF78347AD9922BDD8 (void);
// 0x0000014B System.Text.Json.JsonDocument/MetadataDb System.Text.Json.JsonDocument/MetadataDb::CopySegment(System.Int32,System.Int32)
extern void MetadataDb_CopySegment_mE2A805BC0A4A35436649F6DFF69A286C416A056C (void);
// 0x0000014C System.Void System.Text.Json.JsonDocument/StackRow::.ctor(System.Int32,System.Int32)
extern void StackRow__ctor_mE83A64F557423B2C1417CF2CAA47FCB6DAE79597 (void);
// 0x0000014D System.Void System.Text.Json.JsonDocument/StackRowStack::.ctor(System.Int32)
extern void StackRowStack__ctor_mC2D0A937968645C04762AE37C6F9196811FC60AD (void);
// 0x0000014E System.Void System.Text.Json.JsonDocument/StackRowStack::Dispose()
extern void StackRowStack_Dispose_mE69DCE4BA09A561767066C061517880FC9FD240E (void);
// 0x0000014F System.Void System.Text.Json.JsonDocument/StackRowStack::Push(System.Text.Json.JsonDocument/StackRow)
extern void StackRowStack_Push_m2EB6DF0A8051783F40F7911AC981072DB6DF3AA0 (void);
// 0x00000150 System.Text.Json.JsonDocument/StackRow System.Text.Json.JsonDocument/StackRowStack::Pop()
extern void StackRowStack_Pop_mB7727C6BB4EF6384ADB6A055F63AE463231B3731 (void);
// 0x00000151 System.Void System.Text.Json.JsonDocument/StackRowStack::Enlarge()
extern void StackRowStack_Enlarge_m9CAB452BCBA0F7795608ED801F3741F645037CF0 (void);
// 0x00000152 System.Text.Json.JsonCommentHandling System.Text.Json.JsonDocumentOptions::get_CommentHandling()
extern void JsonDocumentOptions_get_CommentHandling_m250724FEAA7D45E779F026A8E2AE1792E8CC0223 (void);
// 0x00000153 System.Int32 System.Text.Json.JsonDocumentOptions::get_MaxDepth()
extern void JsonDocumentOptions_get_MaxDepth_m910F496990C5F7315734B42E49D6B46D7D7FB146 (void);
// 0x00000154 System.Boolean System.Text.Json.JsonDocumentOptions::get_AllowTrailingCommas()
extern void JsonDocumentOptions_get_AllowTrailingCommas_mB695E7244BD9C08298D2A980CE3C8D6C69D0F2ED (void);
// 0x00000155 System.Text.Json.JsonReaderOptions System.Text.Json.JsonDocumentOptions::GetReaderOptions()
extern void JsonDocumentOptions_GetReaderOptions_mB45CDABD01DCD171F5576B841FD948C183A3F4BC (void);
// 0x00000156 System.Void System.Text.Json.JsonElement::.ctor(System.Text.Json.JsonDocument,System.Int32)
extern void JsonElement__ctor_m08BBC84BCD6BF2156EEA51E7E367E772C44E8C9C (void);
// 0x00000157 System.Text.Json.JsonTokenType System.Text.Json.JsonElement::get_TokenType()
extern void JsonElement_get_TokenType_m2DE21D16B1E01312F72BCD8C9E6EABB3C4CDD7B8 (void);
// 0x00000158 System.Text.Json.JsonValueKind System.Text.Json.JsonElement::get_ValueKind()
extern void JsonElement_get_ValueKind_m34757B769FC468837C793AAB42507082105A1AE7 (void);
// 0x00000159 System.Text.Json.JsonElement System.Text.Json.JsonElement::GetProperty(System.String)
extern void JsonElement_GetProperty_mF3F895EADD2EBC27D13528083FBA7A8E754C02B9 (void);
// 0x0000015A System.Boolean System.Text.Json.JsonElement::TryGetProperty(System.String,System.Text.Json.JsonElement&)
extern void JsonElement_TryGetProperty_m1F48BA59A2FF4DD5DEC6B04E2DFEF7785CAB89DE (void);
// 0x0000015B System.Boolean System.Text.Json.JsonElement::TryGetProperty(System.ReadOnlySpan`1<System.Char>,System.Text.Json.JsonElement&)
extern void JsonElement_TryGetProperty_m915535CFE0D507A447594BD147BE125D20EE0B88 (void);
// 0x0000015C System.String System.Text.Json.JsonElement::GetString()
extern void JsonElement_GetString_mC81CE313F3CF6ECFCC6374545F7B598A244FA53E (void);
// 0x0000015D System.Boolean System.Text.Json.JsonElement::TryGetInt32(System.Int32&)
extern void JsonElement_TryGetInt32_mA8E8A07BDB3F2CD9315116A391DA2F31023EA506 (void);
// 0x0000015E System.Int32 System.Text.Json.JsonElement::GetInt32()
extern void JsonElement_GetInt32_mE298927691DCC4168662962C7B7FF3749003A5D6 (void);
// 0x0000015F System.String System.Text.Json.JsonElement::GetRawText()
extern void JsonElement_GetRawText_m57611974A95EAA4466054192D51EAC5A1487249D (void);
// 0x00000160 System.String System.Text.Json.JsonElement::GetPropertyRawText()
extern void JsonElement_GetPropertyRawText_mE33C03151821E238E589A59D52856D5E796CBEC5 (void);
// 0x00000161 System.Boolean System.Text.Json.JsonElement::TextEqualsHelper(System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Boolean)
extern void JsonElement_TextEqualsHelper_m388909D510BCC8E2EE370A17626A111A77E362D5 (void);
// 0x00000162 System.Void System.Text.Json.JsonElement::WriteTo(System.Text.Json.Utf8JsonWriter)
extern void JsonElement_WriteTo_m1AC8B59D427C19E13F2134BA8964477C9B3E4F20 (void);
// 0x00000163 System.Text.Json.JsonElement/ArrayEnumerator System.Text.Json.JsonElement::EnumerateArray()
extern void JsonElement_EnumerateArray_mADD6A7BD2664B80DC5FEF55DE964046AADA2AC28 (void);
// 0x00000164 System.Text.Json.JsonElement/ObjectEnumerator System.Text.Json.JsonElement::EnumerateObject()
extern void JsonElement_EnumerateObject_m25AB68731BDC8CA13253A206EABEA1F219AEA40C (void);
// 0x00000165 System.String System.Text.Json.JsonElement::ToString()
extern void JsonElement_ToString_mA0336A268F12CE4735C0307F12F4717EA5A24B29 (void);
// 0x00000166 System.Text.Json.JsonElement System.Text.Json.JsonElement::Clone()
extern void JsonElement_Clone_m2D1217819F1C1A069A7291948BD2CDE7D58B090B (void);
// 0x00000167 System.Void System.Text.Json.JsonElement::CheckValidInstance()
extern void JsonElement_CheckValidInstance_m37FCD235BD0B2BF84200713D1CFB2D040A7A8439 (void);
// 0x00000168 System.String System.Text.Json.JsonElement::get_DebuggerDisplay()
extern void JsonElement_get_DebuggerDisplay_mD7A4D8389DA4D9A8790C4FDDFEB0A20101CD8883 (void);
// 0x00000169 System.Void System.Text.Json.JsonElement/ArrayEnumerator::.ctor(System.Text.Json.JsonElement)
extern void ArrayEnumerator__ctor_mE63EDB3BC6E93B2C7E2A40C824553B06F853E68C (void);
// 0x0000016A System.Text.Json.JsonElement System.Text.Json.JsonElement/ArrayEnumerator::get_Current()
extern void ArrayEnumerator_get_Current_mF52CB9638D47BF1F300A8D90DE72F16FE7760A50 (void);
// 0x0000016B System.Text.Json.JsonElement/ArrayEnumerator System.Text.Json.JsonElement/ArrayEnumerator::GetEnumerator()
extern void ArrayEnumerator_GetEnumerator_m84826411DC72414B035DD2383B98995030DC1D64 (void);
// 0x0000016C System.Collections.IEnumerator System.Text.Json.JsonElement/ArrayEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_mFA06CBD3D1326936DCA477D78774A7D5244051B2 (void);
// 0x0000016D System.Collections.Generic.IEnumerator`1<System.Text.Json.JsonElement> System.Text.Json.JsonElement/ArrayEnumerator::System.Collections.Generic.IEnumerable<System.Text.Json.JsonElement>.GetEnumerator()
extern void ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m7E27A28429F4B25E812197A40277394C3898FBF4 (void);
// 0x0000016E System.Void System.Text.Json.JsonElement/ArrayEnumerator::Dispose()
extern void ArrayEnumerator_Dispose_m013242AA10DC5F3B889EF39004D9E62C88484F36 (void);
// 0x0000016F System.Void System.Text.Json.JsonElement/ArrayEnumerator::Reset()
extern void ArrayEnumerator_Reset_m4E108783AA0C9538A0723F914B99F999D0907825 (void);
// 0x00000170 System.Object System.Text.Json.JsonElement/ArrayEnumerator::System.Collections.IEnumerator.get_Current()
extern void ArrayEnumerator_System_Collections_IEnumerator_get_Current_m0625474B01CE733D09EC696C2D3943FEBDA9D8B9 (void);
// 0x00000171 System.Boolean System.Text.Json.JsonElement/ArrayEnumerator::MoveNext()
extern void ArrayEnumerator_MoveNext_m4A4255B7F6ADA0D569E1644355F8542007BC99E8 (void);
// 0x00000172 System.Void System.Text.Json.JsonElement/ObjectEnumerator::.ctor(System.Text.Json.JsonElement)
extern void ObjectEnumerator__ctor_mE6CFBEE462A2DAD097E3CF38C5E1F93B3A72575A (void);
// 0x00000173 System.Text.Json.JsonProperty System.Text.Json.JsonElement/ObjectEnumerator::get_Current()
extern void ObjectEnumerator_get_Current_mEBA991658CFB79D8EC48C940587E8F56E5FCF6A6 (void);
// 0x00000174 System.Text.Json.JsonElement/ObjectEnumerator System.Text.Json.JsonElement/ObjectEnumerator::GetEnumerator()
extern void ObjectEnumerator_GetEnumerator_m6431E7E6BAF73BDCC9B211871E2046D8C5557C9F (void);
// 0x00000175 System.Collections.IEnumerator System.Text.Json.JsonElement/ObjectEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mFABC61E9837E42D4B98EDD2EB829169691151DA4 (void);
// 0x00000176 System.Collections.Generic.IEnumerator`1<System.Text.Json.JsonProperty> System.Text.Json.JsonElement/ObjectEnumerator::System.Collections.Generic.IEnumerable<System.Text.Json.JsonProperty>.GetEnumerator()
extern void ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m152A46470709CCB7745C15E69C8B5E3CC379F292 (void);
// 0x00000177 System.Void System.Text.Json.JsonElement/ObjectEnumerator::Dispose()
extern void ObjectEnumerator_Dispose_m05CE64A63964F3D868775EC395EF5AFCBFD0C772 (void);
// 0x00000178 System.Void System.Text.Json.JsonElement/ObjectEnumerator::Reset()
extern void ObjectEnumerator_Reset_m2375949AFCB32D333217A2F8674E48BF3A948DC7 (void);
// 0x00000179 System.Object System.Text.Json.JsonElement/ObjectEnumerator::System.Collections.IEnumerator.get_Current()
extern void ObjectEnumerator_System_Collections_IEnumerator_get_Current_m819E29B32A75D77D74B6CA9F65F28BDFF7C7E6BC (void);
// 0x0000017A System.Boolean System.Text.Json.JsonElement/ObjectEnumerator::MoveNext()
extern void ObjectEnumerator_MoveNext_m9EF4007222C1BFD3D5BEF8CC03A76CE222F4D859 (void);
// 0x0000017B System.Text.Json.JsonElement System.Text.Json.JsonProperty::get_Value()
extern void JsonProperty_get_Value_mF766C4EFFB858EEAD2A009AF55BB8B51095AC96A (void);
// 0x0000017C System.Void System.Text.Json.JsonProperty::.ctor(System.Text.Json.JsonElement,System.String)
extern void JsonProperty__ctor_m0C3653F7D2BC6F745C523466CA7294E355FABB13 (void);
// 0x0000017D System.Boolean System.Text.Json.JsonProperty::EscapedNameEquals(System.ReadOnlySpan`1<System.Byte>)
extern void JsonProperty_EscapedNameEquals_m9070FF7B8371466D91AF6A4E38D5699FD5107D86 (void);
// 0x0000017E System.String System.Text.Json.JsonProperty::ToString()
extern void JsonProperty_ToString_m1FCFE27631E5AC6F3238874289B06303471487B0 (void);
// 0x0000017F System.String System.Text.Json.JsonProperty::get_DebuggerDisplay()
extern void JsonProperty_get_DebuggerDisplay_mC4D1DE967EC567BBE7888EB699087916594E59E2 (void);
// 0x00000180 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_TrueValue()
extern void JsonConstants_get_TrueValue_mB15CD35C5DCF01FC1B11FA40C7F5C34150DC3777 (void);
// 0x00000181 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_FalseValue()
extern void JsonConstants_get_FalseValue_mEB3BF36A6AF283105DBCFC295BD3C88BC2073F9A (void);
// 0x00000182 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_NullValue()
extern void JsonConstants_get_NullValue_mC4C9020714D4581E9C5D4FA4C3C9949E5D3CA24C (void);
// 0x00000183 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_NaNValue()
extern void JsonConstants_get_NaNValue_m06A191FDB7B355617B3441931550E5935CCAF23C (void);
// 0x00000184 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_PositiveInfinityValue()
extern void JsonConstants_get_PositiveInfinityValue_mD891D70B91D72B2B26DA087FCED01D224AF4BCC9 (void);
// 0x00000185 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_NegativeInfinityValue()
extern void JsonConstants_get_NegativeInfinityValue_m74A03D18E26D8E27B5135614B99F320601B52DAF (void);
// 0x00000186 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_Delimiters()
extern void JsonConstants_get_Delimiters_m219BB10D79A42BD53D0CEDCECCBD7027DD7D67F5 (void);
// 0x00000187 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_EscapableChars()
extern void JsonConstants_get_EscapableChars_m670D8AA1BEAFE60E810633D7E39DEA0F3307FB65 (void);
// 0x00000188 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonEncodedText::get_EncodedUtf8Bytes()
extern void JsonEncodedText_get_EncodedUtf8Bytes_m90590ADF42E78917035B3FA5FE2B23661DBF945B (void);
// 0x00000189 System.Void System.Text.Json.JsonEncodedText::.ctor(System.Byte[])
extern void JsonEncodedText__ctor_m0EF268C655A1728E129E94D60BAA71894F1BD77A (void);
// 0x0000018A System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::Encode(System.String,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_Encode_m48F8D41B2ADAECA21D3B959F62C2D08A73A1F317 (void);
// 0x0000018B System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::Encode(System.ReadOnlySpan`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_Encode_m670B6211FE6F2601D2CA63B0065AE2C71DD11A49 (void);
// 0x0000018C System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::TranscodeAndEncode(System.ReadOnlySpan`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_TranscodeAndEncode_mE4216E009FE93DF4778A8C7CA21140158BC9D454 (void);
// 0x0000018D System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::EncodeHelper(System.ReadOnlySpan`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_EncodeHelper_m9F749FCD42E83877F9FB197707C3D41464925821 (void);
// 0x0000018E System.Boolean System.Text.Json.JsonEncodedText::Equals(System.Text.Json.JsonEncodedText)
extern void JsonEncodedText_Equals_m122DC2FCB6321FDA3F63E1973CF385FBB3C9FB69 (void);
// 0x0000018F System.Boolean System.Text.Json.JsonEncodedText::Equals(System.Object)
extern void JsonEncodedText_Equals_mCE77AC6B7E663C8B7D8412B394E91F715A5BE682 (void);
// 0x00000190 System.String System.Text.Json.JsonEncodedText::ToString()
extern void JsonEncodedText_ToString_mEC405B95F27BDBC3814C264035F3B0151621800F (void);
// 0x00000191 System.Int32 System.Text.Json.JsonEncodedText::GetHashCode()
extern void JsonEncodedText_GetHashCode_m2B447D2F286EC689D45FA77B4ECCD9DD072D2A70 (void);
// 0x00000192 System.Void System.Text.Json.JsonException::.ctor(System.String,System.String,System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>,System.Exception)
extern void JsonException__ctor_mFABDAC7A4D8A6D06955153DAF9055EAEA99ADEA3 (void);
// 0x00000193 System.Void System.Text.Json.JsonException::.ctor(System.String,System.String,System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>)
extern void JsonException__ctor_m332A4F36C16574E65E0E110226C3475D0A19C78E (void);
// 0x00000194 System.Void System.Text.Json.JsonException::.ctor(System.String,System.Exception)
extern void JsonException__ctor_m65DC9A06A2D3B15D7DA72DFFBA5430648A864BA6 (void);
// 0x00000195 System.Void System.Text.Json.JsonException::.ctor(System.String)
extern void JsonException__ctor_m141AAB105C01DB804B3BDA2C65D2A9EE9B6BBDBB (void);
// 0x00000196 System.Void System.Text.Json.JsonException::.ctor()
extern void JsonException__ctor_mAD45F3B7D522764CEECCAEF24E9BE96A3E930065 (void);
// 0x00000197 System.Void System.Text.Json.JsonException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException__ctor_m4A92F38705B569451DA8C63F2E1AD5375DF6E8CA (void);
// 0x00000198 System.Boolean System.Text.Json.JsonException::get_AppendPathInformation()
extern void JsonException_get_AppendPathInformation_mE4D721DFC493525073DDB220BA2A1499C1C08362 (void);
// 0x00000199 System.Void System.Text.Json.JsonException::set_AppendPathInformation(System.Boolean)
extern void JsonException_set_AppendPathInformation_m30D14D80A9F43FB975791CEEBA7B728E6A18EA6A (void);
// 0x0000019A System.Void System.Text.Json.JsonException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException_GetObjectData_m82F05FF628ABF99E30D9C8581FB85D37EE9134B7 (void);
// 0x0000019B System.Nullable`1<System.Int64> System.Text.Json.JsonException::get_LineNumber()
extern void JsonException_get_LineNumber_m7D21C9A5A7431696FB3B195690A1532E46DC4601 (void);
// 0x0000019C System.Void System.Text.Json.JsonException::set_LineNumber(System.Nullable`1<System.Int64>)
extern void JsonException_set_LineNumber_m60FC0BA8C1BA4D775C58DC3657DCEEAA3B9D6631 (void);
// 0x0000019D System.Nullable`1<System.Int64> System.Text.Json.JsonException::get_BytePositionInLine()
extern void JsonException_get_BytePositionInLine_m74C4E152BDFC2439E479ED098CEF728C74EE713B (void);
// 0x0000019E System.Void System.Text.Json.JsonException::set_BytePositionInLine(System.Nullable`1<System.Int64>)
extern void JsonException_set_BytePositionInLine_m6CAD5D3CB0CC4F9638C91C71B8E0A44DFEBF9569 (void);
// 0x0000019F System.String System.Text.Json.JsonException::get_Path()
extern void JsonException_get_Path_m1B60B637D1DCB55E79E0F91381230144DC3DACA0 (void);
// 0x000001A0 System.Void System.Text.Json.JsonException::set_Path(System.String)
extern void JsonException_set_Path_m2ADEFDBC15D75EBD73C8DBCB04C21DA8717B571A (void);
// 0x000001A1 System.String System.Text.Json.JsonException::get_Message()
extern void JsonException_get_Message_m7FCB5F5C5C8587DE96DD591EE60CF610AE367AD7 (void);
// 0x000001A2 System.Void System.Text.Json.JsonException::SetMessage(System.String)
extern void JsonException_SetMessage_mC33D6F67599831B007961369D4EF48C84030527E (void);
// 0x000001A3 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonHelpers::GetSpan(System.Text.Json.Utf8JsonReader&)
extern void JsonHelpers_GetSpan_m9ED27DE6D38D4D2F3CFBAB1F52898CF2B2BA407F (void);
// 0x000001A4 System.Boolean System.Text.Json.JsonHelpers::IsInRangeInclusive(System.UInt32,System.UInt32,System.UInt32)
extern void JsonHelpers_IsInRangeInclusive_mB8422E5BCAC689DE6E7C2EDD3EAE62FC2BF4DFCA (void);
// 0x000001A5 System.Boolean System.Text.Json.JsonHelpers::IsInRangeInclusive(System.Int32,System.Int32,System.Int32)
extern void JsonHelpers_IsInRangeInclusive_m6426AE714324BA64C7D679371A81961A23E320B9 (void);
// 0x000001A6 System.Boolean System.Text.Json.JsonHelpers::IsInRangeInclusive(System.Int64,System.Int64,System.Int64)
extern void JsonHelpers_IsInRangeInclusive_m7901E58F8EB54BCA5EED278E203A2312F0903B83 (void);
// 0x000001A7 System.Boolean System.Text.Json.JsonHelpers::IsDigit(System.Byte)
extern void JsonHelpers_IsDigit_m84481DC035BF769A6F43A43C39850A671308651A (void);
// 0x000001A8 System.Void System.Text.Json.JsonHelpers::ReadWithVerify(System.Text.Json.Utf8JsonReader&)
extern void JsonHelpers_ReadWithVerify_mCC9DD465FF6CCB7DAF613B7D203956794D5D9939 (void);
// 0x000001A9 System.String System.Text.Json.JsonHelpers::Utf8GetString(System.ReadOnlySpan`1<System.Byte>)
extern void JsonHelpers_Utf8GetString_mD0CD323CF639EF33135BAF90D8440790CC9A84E6 (void);
// 0x000001AA System.Boolean System.Text.Json.JsonHelpers::TryAdd(System.Collections.Generic.Dictionary`2<TKey,TValue>,TKey&,TValue&)
// 0x000001AB System.Boolean System.Text.Json.JsonHelpers::IsFinite(System.Double)
extern void JsonHelpers_IsFinite_mCECEA683C0C204194A519BDFF08286B66301D46D (void);
// 0x000001AC System.Boolean System.Text.Json.JsonHelpers::IsFinite(System.Single)
extern void JsonHelpers_IsFinite_m4BE9A83B2285048612AB306BFDD96BF78437DF5C (void);
// 0x000001AD System.Boolean System.Text.Json.JsonHelpers::IsValidDateTimeOffsetParseLength(System.Int32)
extern void JsonHelpers_IsValidDateTimeOffsetParseLength_mE27A9B53F25CF9EAA7591CEDAAD22C2C82AF4321 (void);
// 0x000001AE System.Boolean System.Text.Json.JsonHelpers::IsValidDateTimeOffsetParseLength(System.Int64)
extern void JsonHelpers_IsValidDateTimeOffsetParseLength_m630C990EC673B6E444096CEDCCF0A9C6A480A8F5 (void);
// 0x000001AF System.Boolean System.Text.Json.JsonHelpers::TryParseAsISO(System.ReadOnlySpan`1<System.Byte>,System.DateTime&)
extern void JsonHelpers_TryParseAsISO_m4765D0C0BE547CF469AD6D899020B2A8707101BA (void);
// 0x000001B0 System.Boolean System.Text.Json.JsonHelpers::TryParseAsISO(System.ReadOnlySpan`1<System.Byte>,System.DateTimeOffset&)
extern void JsonHelpers_TryParseAsISO_m7192F4EA6AC1052851402A3EC32D42FD2AB3511B (void);
// 0x000001B1 System.Boolean System.Text.Json.JsonHelpers::TryParseDateTimeOffset(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonHelpers/DateTimeParseData&)
extern void JsonHelpers_TryParseDateTimeOffset_m1A4C13DB22C35EF04F81401B4D8AA23395665B01 (void);
// 0x000001B2 System.Boolean System.Text.Json.JsonHelpers::TryGetNextTwoDigits(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void JsonHelpers_TryGetNextTwoDigits_mDE72FD732E4B06BAE1CECB89545C5F30AE06AB06 (void);
// 0x000001B3 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTimeOffset(System.DateTime,System.Text.Json.JsonHelpers/DateTimeParseData&,System.DateTimeOffset&)
extern void JsonHelpers_TryCreateDateTimeOffset_m72E9B922F178306D098E0118B105CBF236053198 (void);
// 0x000001B4 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTimeOffset(System.Text.Json.JsonHelpers/DateTimeParseData&,System.DateTimeOffset&)
extern void JsonHelpers_TryCreateDateTimeOffset_m5AFB3B83A75ADDDCBA3F7C3465AF477270FF06E2 (void);
// 0x000001B5 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTimeOffsetInterpretingDataAsLocalTime(System.Text.Json.JsonHelpers/DateTimeParseData,System.DateTimeOffset&)
extern void JsonHelpers_TryCreateDateTimeOffsetInterpretingDataAsLocalTime_m09E17B89B063C875CAF7107F03D24750655BAB06 (void);
// 0x000001B6 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTime(System.Text.Json.JsonHelpers/DateTimeParseData,System.DateTimeKind,System.DateTime&)
extern void JsonHelpers_TryCreateDateTime_mDB77646E3C77A97C22CDEBAA2D7740C8B4173058 (void);
// 0x000001B7 System.Byte[] System.Text.Json.JsonHelpers::GetEscapedPropertyNameSection(System.ReadOnlySpan`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonHelpers_GetEscapedPropertyNameSection_mE92CBBE2D3CAFFB05DC77BF57868783D479FDED5 (void);
// 0x000001B8 System.Byte[] System.Text.Json.JsonHelpers::EscapeValue(System.ReadOnlySpan`1<System.Byte>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonHelpers_EscapeValue_mD3344723EFD2DBCE9934B2539023282D8D64E9FB (void);
// 0x000001B9 System.Byte[] System.Text.Json.JsonHelpers::GetEscapedPropertyNameSection(System.ReadOnlySpan`1<System.Byte>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonHelpers_GetEscapedPropertyNameSection_m399DA9D2CCE67FAA15755BA1E0276A1D32DDF6A8 (void);
// 0x000001BA System.Byte[] System.Text.Json.JsonHelpers::GetPropertyNameSection(System.ReadOnlySpan`1<System.Byte>)
extern void JsonHelpers_GetPropertyNameSection_m7A5442B54707D2DA49847EC771C381A404EB7147 (void);
// 0x000001BB System.Void System.Text.Json.JsonHelpers::.cctor()
extern void JsonHelpers__cctor_m7BB93DD74A8C950F6C02B4F9F3E1710BB6A92B2A (void);
// 0x000001BC System.Boolean System.Text.Json.JsonHelpers::<TryParseDateTimeOffset>g__ParseOffset|21_0(System.Text.Json.JsonHelpers/DateTimeParseData&,System.ReadOnlySpan`1<System.Byte>)
extern void JsonHelpers_U3CTryParseDateTimeOffsetU3Eg__ParseOffsetU7C21_0_m088E2588D6A3243BACA89CEF31560FE6818CC879 (void);
// 0x000001BD System.Boolean System.Text.Json.JsonHelpers/DateTimeParseData::get_OffsetNegative()
extern void DateTimeParseData_get_OffsetNegative_m0D054B07A34B50D970C5751B503F342C14F5F3E9 (void);
// 0x000001BE System.Void System.Text.Json.JsonReaderException::.ctor(System.String,System.Int64,System.Int64)
extern void JsonReaderException__ctor_mA6CFB4B33BBD4A66482E0949B37FC965356A347B (void);
// 0x000001BF System.Void System.Text.Json.JsonReaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonReaderException__ctor_m9F7695A2E888443A119FBCF35B88AB7F48DB38A8 (void);
// 0x000001C0 System.ValueTuple`2<System.Int32,System.Int32> System.Text.Json.JsonReaderHelper::CountNewLines(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_CountNewLines_m1678FE5DF2E5BF871EDDA733812F1F2278EDC14C (void);
// 0x000001C1 System.Text.Json.JsonValueKind System.Text.Json.JsonReaderHelper::ToValueKind(System.Text.Json.JsonTokenType)
extern void JsonReaderHelper_ToValueKind_mC5065501F4DC87FF00ED3C35317E53E604B4DC5D (void);
// 0x000001C2 System.Boolean System.Text.Json.JsonReaderHelper::IsTokenTypePrimitive(System.Text.Json.JsonTokenType)
extern void JsonReaderHelper_IsTokenTypePrimitive_m2241739D3A5713388C3DB156A7B8075092C45DF9 (void);
// 0x000001C3 System.Boolean System.Text.Json.JsonReaderHelper::IsHexDigit(System.Byte)
extern void JsonReaderHelper_IsHexDigit_m8A8A0702FD1A088E1CB8D44EA2CA0F1E70255F75 (void);
// 0x000001C4 System.Int32 System.Text.Json.JsonReaderHelper::IndexOfQuoteOrAnyControlOrBackSlash(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_IndexOfQuoteOrAnyControlOrBackSlash_m1465C1D49863A0D9DF2D893FCEF23416D6736AA4 (void);
// 0x000001C5 System.Int32 System.Text.Json.JsonReaderHelper::IndexOfOrLessThan(System.Byte&,System.Byte,System.Byte,System.Byte,System.Int32)
extern void JsonReaderHelper_IndexOfOrLessThan_mF446B6FF9A622A7E5C26B2D1C51FDCC31E45B4B2 (void);
// 0x000001C6 System.Int32 System.Text.Json.JsonReaderHelper::LocateFirstFoundByte(System.Numerics.Vector`1<System.Byte>)
extern void JsonReaderHelper_LocateFirstFoundByte_m5325ED5EC0B3BBAC5EBCD5C368019DE870CE3C5E (void);
// 0x000001C7 System.Int32 System.Text.Json.JsonReaderHelper::LocateFirstFoundByte(System.UInt64)
extern void JsonReaderHelper_LocateFirstFoundByte_mA8C087A129D89A6562F715DC98C63E1618F56812 (void);
// 0x000001C8 System.Boolean System.Text.Json.JsonReaderHelper::TryGetEscapedDateTime(System.ReadOnlySpan`1<System.Byte>,System.DateTime&)
extern void JsonReaderHelper_TryGetEscapedDateTime_m12A7F2DCE9D1E7A263885D163C07C83B088D7794 (void);
// 0x000001C9 System.Boolean System.Text.Json.JsonReaderHelper::TryGetEscapedDateTimeOffset(System.ReadOnlySpan`1<System.Byte>,System.DateTimeOffset&)
extern void JsonReaderHelper_TryGetEscapedDateTimeOffset_m8EE8EE332C8B35178CBFE539A4AB3759B2780DB0 (void);
// 0x000001CA System.Boolean System.Text.Json.JsonReaderHelper::TryGetEscapedGuid(System.ReadOnlySpan`1<System.Byte>,System.Guid&)
extern void JsonReaderHelper_TryGetEscapedGuid_mC0F11E2356D2E0CA3F78CD92275C0EA47C6C66DE (void);
// 0x000001CB System.Char System.Text.Json.JsonReaderHelper::GetFloatingPointStandardParseFormat(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_GetFloatingPointStandardParseFormat_mB0392AB52B46FF8C8CA58BD748E6AB175F251CA0 (void);
// 0x000001CC System.Boolean System.Text.Json.JsonReaderHelper::TryGetFloatingPointConstant(System.ReadOnlySpan`1<System.Byte>,System.Single&)
extern void JsonReaderHelper_TryGetFloatingPointConstant_m600E0F6BA37CD6BA8A4AF81C0BF3B99430EC6897 (void);
// 0x000001CD System.Boolean System.Text.Json.JsonReaderHelper::TryGetFloatingPointConstant(System.ReadOnlySpan`1<System.Byte>,System.Double&)
extern void JsonReaderHelper_TryGetFloatingPointConstant_m82834D401884E26CA0A51A376C0C813BDD6D9A47 (void);
// 0x000001CE System.Boolean System.Text.Json.JsonReaderHelper::TryGetUnescapedBase64Bytes(System.ReadOnlySpan`1<System.Byte>,System.Int32,System.Byte[]&)
extern void JsonReaderHelper_TryGetUnescapedBase64Bytes_m6B842CBF6259E1588D3161286FA663515A9512A4 (void);
// 0x000001CF System.String System.Text.Json.JsonReaderHelper::GetUnescapedString(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void JsonReaderHelper_GetUnescapedString_mFFCD9FDBC8F93BDE37608AED6C79A58265B65DDB (void);
// 0x000001D0 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonReaderHelper::GetUnescapedSpan(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void JsonReaderHelper_GetUnescapedSpan_m4B1158FDA6985C88988F6B3727E0C8908CDC5B5C (void);
// 0x000001D1 System.Boolean System.Text.Json.JsonReaderHelper::UnescapeAndCompare(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_UnescapeAndCompare_mFF06C4450588353306D1EEA42EC36ADA7CB253B3 (void);
// 0x000001D2 System.Boolean System.Text.Json.JsonReaderHelper::TryDecodeBase64InPlace(System.Span`1<System.Byte>,System.Byte[]&)
extern void JsonReaderHelper_TryDecodeBase64InPlace_mB9FB71368E0C656AD14ED4EC3AAF8C15A8557610 (void);
// 0x000001D3 System.Boolean System.Text.Json.JsonReaderHelper::TryDecodeBase64(System.ReadOnlySpan`1<System.Byte>,System.Byte[]&)
extern void JsonReaderHelper_TryDecodeBase64_mCB66A113B2B0E3D25218985C4F68DEE3BEF4D91B (void);
// 0x000001D4 System.String System.Text.Json.JsonReaderHelper::TranscodeHelper(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_TranscodeHelper_m52E43E75CA9B5AA0A22814B86CD1F5B1D8FFAA9F (void);
// 0x000001D5 System.Int32 System.Text.Json.JsonReaderHelper::GetUtf8ByteCount(System.ReadOnlySpan`1<System.Char>)
extern void JsonReaderHelper_GetUtf8ByteCount_mA812505B2257B2693CC85B6AED55950D496B0AC8 (void);
// 0x000001D6 System.Int32 System.Text.Json.JsonReaderHelper::GetUtf8FromText(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Byte>)
extern void JsonReaderHelper_GetUtf8FromText_m5B178FE06DF9C8F8E5438AC4F16A0FE7BA01476A (void);
// 0x000001D7 System.String System.Text.Json.JsonReaderHelper::GetTextFromUtf8(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_GetTextFromUtf8_mD404489C2197250EA489A1EC2363A946B5B4CC39 (void);
// 0x000001D8 System.Void System.Text.Json.JsonReaderHelper::Unescape(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32,System.Int32&)
extern void JsonReaderHelper_Unescape_mF575929C5DB64F96EFCDDECBC76271E167B8CD91 (void);
// 0x000001D9 System.Void System.Text.Json.JsonReaderHelper::EncodeToUtf8Bytes(System.UInt32,System.Span`1<System.Byte>,System.Int32&)
extern void JsonReaderHelper_EncodeToUtf8Bytes_m89BD0F513CF493D9F5FB4E22565C1D2FAD986F1B (void);
// 0x000001DA System.Void System.Text.Json.JsonReaderHelper::.cctor()
extern void JsonReaderHelper__cctor_m9D2E150F807D4483B2C86FBD3DCC6EA57C1A1A7F (void);
// 0x000001DB System.Text.Json.JsonCommentHandling System.Text.Json.JsonReaderOptions::get_CommentHandling()
extern void JsonReaderOptions_get_CommentHandling_mC784945C4A7D8BF00627E0D71CAF144E059CA45B (void);
// 0x000001DC System.Void System.Text.Json.JsonReaderOptions::set_CommentHandling(System.Text.Json.JsonCommentHandling)
extern void JsonReaderOptions_set_CommentHandling_m840CCCE2E2296122C6AC1E8B8E75D1EEB54557E7 (void);
// 0x000001DD System.Int32 System.Text.Json.JsonReaderOptions::get_MaxDepth()
extern void JsonReaderOptions_get_MaxDepth_mE9E196FD22578986617CBC0B6104934965E7DE18 (void);
// 0x000001DE System.Void System.Text.Json.JsonReaderOptions::set_MaxDepth(System.Int32)
extern void JsonReaderOptions_set_MaxDepth_m7CA249F02B7EFB090EBE8DD1A81134BF15890127 (void);
// 0x000001DF System.Boolean System.Text.Json.JsonReaderOptions::get_AllowTrailingCommas()
extern void JsonReaderOptions_get_AllowTrailingCommas_m6BE15A1BB0597C203011627DCF4E9CDDB42049BA (void);
// 0x000001E0 System.Void System.Text.Json.JsonReaderOptions::set_AllowTrailingCommas(System.Boolean)
extern void JsonReaderOptions_set_AllowTrailingCommas_mB8C99444AB5B8A9D50FDDD8E2CDC8F473B6D0641 (void);
// 0x000001E1 System.Void System.Text.Json.JsonReaderState::.ctor(System.Text.Json.JsonReaderOptions)
extern void JsonReaderState__ctor_mBF6D934854F0D3489AED812C79FD31BD7DAEDB21 (void);
// 0x000001E2 System.Text.Json.JsonReaderOptions System.Text.Json.JsonReaderState::get_Options()
extern void JsonReaderState_get_Options_m66769D6D489C3E79567B39C83DD8464073804370 (void);
// 0x000001E3 System.Boolean System.Text.Json.Utf8JsonReader::get_IsLastSpan()
extern void Utf8JsonReader_get_IsLastSpan_m7B992A026074DDEFE7B18BBAF1D35F7FCC67307D (void);
// 0x000001E4 System.Buffers.ReadOnlySequence`1<System.Byte> System.Text.Json.Utf8JsonReader::get_OriginalSequence()
extern void Utf8JsonReader_get_OriginalSequence_m695F93AC97FC88673CF1C5B42CB2781FCC421DF3 (void);
// 0x000001E5 System.ReadOnlySpan`1<System.Byte> System.Text.Json.Utf8JsonReader::get_OriginalSpan()
extern void Utf8JsonReader_get_OriginalSpan_m5CE4EC409D428FFA49D657B1EB8095A5A1BF75FD (void);
// 0x000001E6 System.ReadOnlySpan`1<System.Byte> System.Text.Json.Utf8JsonReader::get_ValueSpan()
extern void Utf8JsonReader_get_ValueSpan_mBABD1CCB678681EF0BAB792BD92B1A667B5280EA (void);
// 0x000001E7 System.Void System.Text.Json.Utf8JsonReader::set_ValueSpan(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_set_ValueSpan_mCB290F34DBE06C916AC61FCD97325363E6DF8840 (void);
// 0x000001E8 System.Int64 System.Text.Json.Utf8JsonReader::get_BytesConsumed()
extern void Utf8JsonReader_get_BytesConsumed_mED511A2A33941953398E81C2630928B1BE296C60 (void);
// 0x000001E9 System.Int64 System.Text.Json.Utf8JsonReader::get_TokenStartIndex()
extern void Utf8JsonReader_get_TokenStartIndex_m065C1E55350901EE451793D91D45C6BFD6727825 (void);
// 0x000001EA System.Void System.Text.Json.Utf8JsonReader::set_TokenStartIndex(System.Int64)
extern void Utf8JsonReader_set_TokenStartIndex_mD8DE6AC3843B60206928FF65843B1063725E11D1 (void);
// 0x000001EB System.Int32 System.Text.Json.Utf8JsonReader::get_CurrentDepth()
extern void Utf8JsonReader_get_CurrentDepth_m27479A89058293B9D65E5245D4288745BC187941 (void);
// 0x000001EC System.Boolean System.Text.Json.Utf8JsonReader::get_IsInArray()
extern void Utf8JsonReader_get_IsInArray_mA7603C29F57D874052CDED9AC19EE23B18786A16 (void);
// 0x000001ED System.Text.Json.JsonTokenType System.Text.Json.Utf8JsonReader::get_TokenType()
extern void Utf8JsonReader_get_TokenType_m589FFFF94F1DF061F5BAA87791CE6EDC77B153FC (void);
// 0x000001EE System.Boolean System.Text.Json.Utf8JsonReader::get_HasValueSequence()
extern void Utf8JsonReader_get_HasValueSequence_m0E4BC52E674A56D3F8FD1EF9F56BCA86F7851658 (void);
// 0x000001EF System.Void System.Text.Json.Utf8JsonReader::set_HasValueSequence(System.Boolean)
extern void Utf8JsonReader_set_HasValueSequence_mA04C744A5B061CF11B292F4FA30685D8FF959819 (void);
// 0x000001F0 System.Boolean System.Text.Json.Utf8JsonReader::get_IsFinalBlock()
extern void Utf8JsonReader_get_IsFinalBlock_m044741A01CC206E766FFC12FC40D20F5F3D20C16 (void);
// 0x000001F1 System.Buffers.ReadOnlySequence`1<System.Byte> System.Text.Json.Utf8JsonReader::get_ValueSequence()
extern void Utf8JsonReader_get_ValueSequence_m8BFD79D1CB26B6B8C081CC01F855EFD3A48E74F3 (void);
// 0x000001F2 System.Void System.Text.Json.Utf8JsonReader::set_ValueSequence(System.Buffers.ReadOnlySequence`1<System.Byte>)
extern void Utf8JsonReader_set_ValueSequence_m14F871806DC91F89B691C08FDFDBA71DB941C6E4 (void);
// 0x000001F3 System.Text.Json.JsonReaderState System.Text.Json.Utf8JsonReader::get_CurrentState()
extern void Utf8JsonReader_get_CurrentState_mB19357F20C4CBCFDD92278B4D2AEF65D82ECDA5A (void);
// 0x000001F4 System.Void System.Text.Json.Utf8JsonReader::.ctor(System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Text.Json.JsonReaderState)
extern void Utf8JsonReader__ctor_mCA5FF66B2E66D867A4F30167AADF35FA7B66D00F (void);
// 0x000001F5 System.Boolean System.Text.Json.Utf8JsonReader::Read()
extern void Utf8JsonReader_Read_m48C98153390FA527AB0DD5449555BB69A2881816 (void);
// 0x000001F6 System.Void System.Text.Json.Utf8JsonReader::Skip()
extern void Utf8JsonReader_Skip_m5E206D71CC77C21D2329A9F809AC53367CC74091 (void);
// 0x000001F7 System.Void System.Text.Json.Utf8JsonReader::SkipHelper()
extern void Utf8JsonReader_SkipHelper_mBD2F0EAE5EFE06BA861442584F998D344B9D6316 (void);
// 0x000001F8 System.Boolean System.Text.Json.Utf8JsonReader::TrySkip()
extern void Utf8JsonReader_TrySkip_m30F2CB8BDCDD32BCE7C345A84398E3ED5AE38AC5 (void);
// 0x000001F9 System.Boolean System.Text.Json.Utf8JsonReader::TrySkipHelper()
extern void Utf8JsonReader_TrySkipHelper_m403637E1D8D991978EC5C7D0D45B7AD39203E865 (void);
// 0x000001FA System.Void System.Text.Json.Utf8JsonReader::StartObject()
extern void Utf8JsonReader_StartObject_m80A9CBB11BC789B32FD92F9D951807CDA73F88EE (void);
// 0x000001FB System.Void System.Text.Json.Utf8JsonReader::EndObject()
extern void Utf8JsonReader_EndObject_mF882CCEFFE2075AE1BA7A8B82F787F3472ACD900 (void);
// 0x000001FC System.Void System.Text.Json.Utf8JsonReader::StartArray()
extern void Utf8JsonReader_StartArray_m9DC35176A3D6D63A0F0CE6E12EF81057795E5B2D (void);
// 0x000001FD System.Void System.Text.Json.Utf8JsonReader::EndArray()
extern void Utf8JsonReader_EndArray_m595FA16248A8A07CA4B68CF12789592F941E1149 (void);
// 0x000001FE System.Void System.Text.Json.Utf8JsonReader::UpdateBitStackOnEndToken()
extern void Utf8JsonReader_UpdateBitStackOnEndToken_m5681C904065EB11EBAB2CE8E3FD2E0323DEBDBC7 (void);
// 0x000001FF System.Boolean System.Text.Json.Utf8JsonReader::ReadSingleSegment()
extern void Utf8JsonReader_ReadSingleSegment_m4D6BE7EA668855AF1A3DC170E54996088B0C08A2 (void);
// 0x00000200 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreData()
extern void Utf8JsonReader_HasMoreData_m0849ADA8E4F15E0C12811A4A1144D964F7AED2DD (void);
// 0x00000201 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreData(System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_HasMoreData_m1E2DEEDB648DF43DB37BC3044E8D633D16FB076E (void);
// 0x00000202 System.Boolean System.Text.Json.Utf8JsonReader::ReadFirstToken(System.Byte)
extern void Utf8JsonReader_ReadFirstToken_mF7DA287E1C8E5B7C1C885339DC4C1DC3E48C38F6 (void);
// 0x00000203 System.Void System.Text.Json.Utf8JsonReader::SkipWhiteSpace()
extern void Utf8JsonReader_SkipWhiteSpace_m7E57D7BF978E4641391C9516F38820A2CE0556AA (void);
// 0x00000204 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeValue(System.Byte)
extern void Utf8JsonReader_ConsumeValue_mA7A960E791A5055383C64E7DD9AA669E71115799 (void);
// 0x00000205 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeLiteral(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonTokenType)
extern void Utf8JsonReader_ConsumeLiteral_m2A3B4794861C4EF32D8370B2D73C6412000660FC (void);
// 0x00000206 System.Boolean System.Text.Json.Utf8JsonReader::CheckLiteral(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_CheckLiteral_m0906217EC5F76E1683DC47AFE73B4B56126791EC (void);
// 0x00000207 System.Void System.Text.Json.Utf8JsonReader::ThrowInvalidLiteral(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_ThrowInvalidLiteral_mFE5C045DA40FA4649AFE040AA0C332C753D8AFC5 (void);
// 0x00000208 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNumber()
extern void Utf8JsonReader_ConsumeNumber_m4B68EF9DC535940BA5B09F601BE7A469C25482BE (void);
// 0x00000209 System.Boolean System.Text.Json.Utf8JsonReader::ConsumePropertyName()
extern void Utf8JsonReader_ConsumePropertyName_m4D54B25BFE01BFFF5976F17D7A9D5D7C166EACD0 (void);
// 0x0000020A System.Boolean System.Text.Json.Utf8JsonReader::ConsumeString()
extern void Utf8JsonReader_ConsumeString_m0A2F49639EADB6BE4495CA600CED86A892F3EC82 (void);
// 0x0000020B System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringAndValidate(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeStringAndValidate_mD3DCAF6E986C245047B6B08FFBB149269E1941E5 (void);
// 0x0000020C System.Boolean System.Text.Json.Utf8JsonReader::ValidateHexDigits(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ValidateHexDigits_mEB6781B13A74BC4EE94ECEE64A8362067945893E (void);
// 0x0000020D System.Boolean System.Text.Json.Utf8JsonReader::TryGetNumber(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_TryGetNumber_mA537AE8E9D9E0C1AA6EB929872959D9E0D1C1AAA (void);
// 0x0000020E System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeNegativeSign(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeNegativeSign_mEAB4DACF3FA5003223B06958EB7BA236E6D3CD57 (void);
// 0x0000020F System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeZero(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeZero_m237EEC547BCEE5ACC2FA5C371DFC4C2F8D7C1184 (void);
// 0x00000210 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeIntegerDigits(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeIntegerDigits_mD3C9A55484E0188A9DF616C6C43CDC7332FD166E (void);
// 0x00000211 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeDecimalDigits(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeDecimalDigits_m32313C386676761CA3EDDB3F80EAD756C8B0E76D (void);
// 0x00000212 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeSign(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeSign_m9D31DED0190794A96D6BD43EFD2D01D4FB326F12 (void);
// 0x00000213 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNextTokenOrRollback(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenOrRollback_m734D5BDD909FE3FC93D2D62FC823048553C28BF3 (void);
// 0x00000214 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextToken(System.Byte)
extern void Utf8JsonReader_ConsumeNextToken_m3CA8B80E08B12A2E034EE6913DA5F8185A32361D (void);
// 0x00000215 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenFromLastNonCommentToken()
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_m6B24CB081250AF44DCEA9938616E206978FBE8BB (void);
// 0x00000216 System.Boolean System.Text.Json.Utf8JsonReader::SkipAllComments(System.Byte&)
extern void Utf8JsonReader_SkipAllComments_m4B68EAA87BA8197DFB88BEA53B67B7C5D5E5BF1F (void);
// 0x00000217 System.Boolean System.Text.Json.Utf8JsonReader::SkipAllComments(System.Byte&,System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_SkipAllComments_m0626448D38FC1F68C36361A5CE32F753E3C3A9B4 (void);
// 0x00000218 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenUntilAfterAllCommentsAreSkipped(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m9EBCD5E4D2F5B96282EA227FC60DECFD12DC322D (void);
// 0x00000219 System.Boolean System.Text.Json.Utf8JsonReader::SkipComment()
extern void Utf8JsonReader_SkipComment_m91E3271B505861F135B1F7CBEA89FF9D03DBFEC4 (void);
// 0x0000021A System.Boolean System.Text.Json.Utf8JsonReader::SkipSingleLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_SkipSingleLineComment_m080A7403DB593F785293CEB04C935F97608A47A5 (void);
// 0x0000021B System.Int32 System.Text.Json.Utf8JsonReader::FindLineSeparator(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_FindLineSeparator_m1A63CFD96D293C6851A62C87C0829B08268CC50D (void);
// 0x0000021C System.Void System.Text.Json.Utf8JsonReader::ThrowOnDangerousLineSeparator(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_ThrowOnDangerousLineSeparator_m9DB458C5580C40BD100AC7993DFC3445EA6D1DCC (void);
// 0x0000021D System.Boolean System.Text.Json.Utf8JsonReader::SkipMultiLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_SkipMultiLineComment_mDDE267D4B9BCE906A04E91290B75E2E69C943839 (void);
// 0x0000021E System.Boolean System.Text.Json.Utf8JsonReader::ConsumeComment()
extern void Utf8JsonReader_ConsumeComment_mDDA4B89EB77DF4767A97289E5AE1A45C29875605 (void);
// 0x0000021F System.Boolean System.Text.Json.Utf8JsonReader::ConsumeSingleLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeSingleLineComment_m6820206B83C4C8FB1F2D85DA65DA783F0C4B0848 (void);
// 0x00000220 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeMultiLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeMultiLineComment_mDF378938E725595AE26E07EBC9448EBC122BF2A8 (void);
// 0x00000221 System.String System.Text.Json.Utf8JsonReader::get_DebuggerDisplay()
extern void Utf8JsonReader_get_DebuggerDisplay_m90465723303F2563AD6293AD79277A5101A811C8 (void);
// 0x00000222 System.String System.Text.Json.Utf8JsonReader::get_DebugTokenType()
extern void Utf8JsonReader_get_DebugTokenType_m5C7DEDDF27CDA9E961186CA3261276037A38961F (void);
// 0x00000223 System.ReadOnlySpan`1<System.Byte> System.Text.Json.Utf8JsonReader::GetUnescapedSpan()
extern void Utf8JsonReader_GetUnescapedSpan_m2180AE402D0F1D887B57335433E1B98A8F6A0030 (void);
// 0x00000224 System.Boolean System.Text.Json.Utf8JsonReader::ReadMultiSegment()
extern void Utf8JsonReader_ReadMultiSegment_m7A789B154E6126292F523C039F86A28936652DA7 (void);
// 0x00000225 System.Boolean System.Text.Json.Utf8JsonReader::ValidateStateAtEndOfData()
extern void Utf8JsonReader_ValidateStateAtEndOfData_mE44BD918232309740E30D9A285F6F63A2EF5E76C (void);
// 0x00000226 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreDataMultiSegment()
extern void Utf8JsonReader_HasMoreDataMultiSegment_mB2C1C0751141A6B4B150D6A1F310560936C69C18 (void);
// 0x00000227 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreDataMultiSegment(System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_HasMoreDataMultiSegment_m71B085F039270BE038C5F171206D7F198AB21FF5 (void);
// 0x00000228 System.Boolean System.Text.Json.Utf8JsonReader::GetNextSpan()
extern void Utf8JsonReader_GetNextSpan_m2B9D198EE611302CE973A7E6268D835C5164553B (void);
// 0x00000229 System.Boolean System.Text.Json.Utf8JsonReader::ReadFirstTokenMultiSegment(System.Byte)
extern void Utf8JsonReader_ReadFirstTokenMultiSegment_m63D211F1A922E4DD4BC8A801BCECAC0CCEA6267D (void);
// 0x0000022A System.Void System.Text.Json.Utf8JsonReader::SkipWhiteSpaceMultiSegment()
extern void Utf8JsonReader_SkipWhiteSpaceMultiSegment_m11BAAD269031D687444204D40BB0EA7C6E2D9427 (void);
// 0x0000022B System.Boolean System.Text.Json.Utf8JsonReader::ConsumeValueMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeValueMultiSegment_m913D0F4B616A77E94761359A3617D2EE8102A95D (void);
// 0x0000022C System.Boolean System.Text.Json.Utf8JsonReader::ConsumeLiteralMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonTokenType)
extern void Utf8JsonReader_ConsumeLiteralMultiSegment_mBA1B229E2B0682719CDC963E68E9E8EF203D9610 (void);
// 0x0000022D System.Boolean System.Text.Json.Utf8JsonReader::CheckLiteralMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_CheckLiteralMultiSegment_mBD77DA207039A439C116B70313DA1C1EAE1E79CB (void);
// 0x0000022E System.Int32 System.Text.Json.Utf8JsonReader::FindMismatch(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_FindMismatch_mFC922E786DE159AC6D606BE3AB07F15BA6743BD0 (void);
// 0x0000022F System.Text.Json.JsonException System.Text.Json.Utf8JsonReader::GetInvalidLiteralMultiSegment(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_GetInvalidLiteralMultiSegment_m60F4782DDD6F88D7898813D5C0468DD1A1BEAF55 (void);
// 0x00000230 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNumberMultiSegment()
extern void Utf8JsonReader_ConsumeNumberMultiSegment_mE723026EE74A4624867675A37F7C8E267998EE3C (void);
// 0x00000231 System.Boolean System.Text.Json.Utf8JsonReader::ConsumePropertyNameMultiSegment()
extern void Utf8JsonReader_ConsumePropertyNameMultiSegment_m3F71002973F225C8F6FD075BC3C76A506D7BB72E (void);
// 0x00000232 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringMultiSegment()
extern void Utf8JsonReader_ConsumeStringMultiSegment_mB24E40054CA1A60B8210FC43CF23EF6F8BE3F151 (void);
// 0x00000233 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringNextSegment()
extern void Utf8JsonReader_ConsumeStringNextSegment_m3CCF0CCD09B5856298887E1096CCE6F02A60D0A9 (void);
// 0x00000234 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringAndValidateMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mA8AD93CE8405F63672B369F6848EC276631D5B33 (void);
// 0x00000235 System.Void System.Text.Json.Utf8JsonReader::RollBackState(System.Text.Json.Utf8JsonReader/PartialStateForRollback&,System.Boolean)
extern void Utf8JsonReader_RollBackState_mE1AB8FF05072C2D1C921F4D97E56122C15E905AD (void);
// 0x00000236 System.Boolean System.Text.Json.Utf8JsonReader::TryGetNumberMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_TryGetNumberMultiSegment_mAB46FB985C042DAF2B0F290A19B221AB58FDBAB1 (void);
// 0x00000237 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeNegativeSignMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeNegativeSignMultiSegment_m135AE9A283C44CD51F7BEFF2A2E0FD9890FCED3C (void);
// 0x00000238 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeZeroMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeZeroMultiSegment_m281FC6658603949FC90711BE6C81B8C3108B9E2D (void);
// 0x00000239 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeIntegerDigitsMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_mF5A5A69C0744D08C722001E1E049D5D1703A9308 (void);
// 0x0000023A System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeDecimalDigitsMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_mE037A2CF96C775C86599C5A61F9E7EEB2C411140 (void);
// 0x0000023B System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeSignMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeSignMultiSegment_m05A9CA258362283F551D2CC8A7B43DAAA1F99D7F (void);
// 0x0000023C System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNextTokenOrRollbackMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m8A4377711B0F17F01DE5CBC05DC2A26109647E43 (void);
// 0x0000023D System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenMultiSegment_m4CA61D5BA79665005FBE0CE4E9694F501351086A (void);
// 0x0000023E System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenFromLastNonCommentTokenMultiSegment()
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mA24E85BCB4C5A5FF4C5BCE2BCB0AF267FFF92E42 (void);
// 0x0000023F System.Boolean System.Text.Json.Utf8JsonReader::SkipAllCommentsMultiSegment(System.Byte&)
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_mAC8F7760E50129CC670532D1BABFCE46303011AF (void);
// 0x00000240 System.Boolean System.Text.Json.Utf8JsonReader::SkipAllCommentsMultiSegment(System.Byte&,System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_m14037635702E57A5503363E26FD8907DABF504B4 (void);
// 0x00000241 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_mE0992B12538E8587B695ED2D6EA28CBACF2EFE2C (void);
// 0x00000242 System.Boolean System.Text.Json.Utf8JsonReader::SkipOrConsumeCommentMultiSegmentWithRollback()
extern void Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m050F36798BF507E32B4C696A1BA9DBF566543055 (void);
// 0x00000243 System.Boolean System.Text.Json.Utf8JsonReader::SkipCommentMultiSegment(System.Int32&)
extern void Utf8JsonReader_SkipCommentMultiSegment_mF89DEC8E6F0EBCD5B6472A4C37949F9C14C69B97 (void);
// 0x00000244 System.Boolean System.Text.Json.Utf8JsonReader::SkipSingleLineCommentMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_SkipSingleLineCommentMultiSegment_m010391D904BA6ACBF5FCB5034CB8EE91C9C3E091 (void);
// 0x00000245 System.Int32 System.Text.Json.Utf8JsonReader::FindLineSeparatorMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_FindLineSeparatorMultiSegment_m1517AF51E8F05AD9F0B0E3C8247A737D4756D9F2 (void);
// 0x00000246 System.Void System.Text.Json.Utf8JsonReader::ThrowOnDangerousLineSeparatorMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m0CB9C5BCC5C677E851EE114F5AFAA5DC1EE42CDB (void);
// 0x00000247 System.Boolean System.Text.Json.Utf8JsonReader::SkipMultiLineCommentMultiSegment(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_SkipMultiLineCommentMultiSegment_mCD3026993722AB7389E158550B086BD01A090C33 (void);
// 0x00000248 System.Text.Json.Utf8JsonReader/PartialStateForRollback System.Text.Json.Utf8JsonReader::CaptureState()
extern void Utf8JsonReader_CaptureState_m8BBAE4358A88D8BAD2FB811C910F1192BCA21325 (void);
// 0x00000249 System.String System.Text.Json.Utf8JsonReader::GetString()
extern void Utf8JsonReader_GetString_mF6ACA3547007BAAD951423AD47FC7C8FB1EE1C44 (void);
// 0x0000024A System.Boolean System.Text.Json.Utf8JsonReader::GetBoolean()
extern void Utf8JsonReader_GetBoolean_m908285BF5B202C675CBEE893F2B41E9BC5F2806B (void);
// 0x0000024B System.Byte[] System.Text.Json.Utf8JsonReader::GetBytesFromBase64()
extern void Utf8JsonReader_GetBytesFromBase64_m0B0E3C7FA55C6B1BF7B6513D80CB4159543F5F67 (void);
// 0x0000024C System.Byte System.Text.Json.Utf8JsonReader::GetByte()
extern void Utf8JsonReader_GetByte_mA2DAAA2A21699AB68FD020A0BC41AC8D3935C439 (void);
// 0x0000024D System.Byte System.Text.Json.Utf8JsonReader::GetByteWithQuotes()
extern void Utf8JsonReader_GetByteWithQuotes_m3BFD4FEF30EBA5DC312DC07D7C6EC9D132B591B9 (void);
// 0x0000024E System.SByte System.Text.Json.Utf8JsonReader::GetSByte()
extern void Utf8JsonReader_GetSByte_mF0C15DF1DFAAFED79A5FF6527BC1EC6DF8D6D541 (void);
// 0x0000024F System.SByte System.Text.Json.Utf8JsonReader::GetSByteWithQuotes()
extern void Utf8JsonReader_GetSByteWithQuotes_mBA0D6B8A92034B6683EF86A600395EB38129922C (void);
// 0x00000250 System.Int16 System.Text.Json.Utf8JsonReader::GetInt16()
extern void Utf8JsonReader_GetInt16_mD10C668BD0F694A704B5A7299ABFDE45B9DFD4EE (void);
// 0x00000251 System.Int16 System.Text.Json.Utf8JsonReader::GetInt16WithQuotes()
extern void Utf8JsonReader_GetInt16WithQuotes_m3BA45015843A11D2A9C67FA2B83E83DB0AF69EC1 (void);
// 0x00000252 System.Int32 System.Text.Json.Utf8JsonReader::GetInt32()
extern void Utf8JsonReader_GetInt32_mB50F3838DE3DC950FC7FFDFC24721A136DA76BC8 (void);
// 0x00000253 System.Int32 System.Text.Json.Utf8JsonReader::GetInt32WithQuotes()
extern void Utf8JsonReader_GetInt32WithQuotes_mF05F615F6638E5308D86A63939D7639221402D52 (void);
// 0x00000254 System.Int64 System.Text.Json.Utf8JsonReader::GetInt64()
extern void Utf8JsonReader_GetInt64_m68E1F5DB10DBB2E490FE90D1CDBC1C5E81EFDB8D (void);
// 0x00000255 System.Int64 System.Text.Json.Utf8JsonReader::GetInt64WithQuotes()
extern void Utf8JsonReader_GetInt64WithQuotes_mE8901124409E515BBAB819575C5851BD854BE8ED (void);
// 0x00000256 System.UInt16 System.Text.Json.Utf8JsonReader::GetUInt16()
extern void Utf8JsonReader_GetUInt16_m91B334F666BA8952300156AC744D340C01A0FA68 (void);
// 0x00000257 System.UInt16 System.Text.Json.Utf8JsonReader::GetUInt16WithQuotes()
extern void Utf8JsonReader_GetUInt16WithQuotes_m2BA11333E17BC23C70A1CA4E90AE7960BA808CB1 (void);
// 0x00000258 System.UInt32 System.Text.Json.Utf8JsonReader::GetUInt32()
extern void Utf8JsonReader_GetUInt32_mCAAA27CB167078F0E49C896E0D38F71DA98A1DA1 (void);
// 0x00000259 System.UInt32 System.Text.Json.Utf8JsonReader::GetUInt32WithQuotes()
extern void Utf8JsonReader_GetUInt32WithQuotes_m2221571EDFBA7F56DC5E51B2AC8F0318548E6DD8 (void);
// 0x0000025A System.UInt64 System.Text.Json.Utf8JsonReader::GetUInt64()
extern void Utf8JsonReader_GetUInt64_m04FC93AD8353898C2E5383CBF9616A0D03DB05AF (void);
// 0x0000025B System.UInt64 System.Text.Json.Utf8JsonReader::GetUInt64WithQuotes()
extern void Utf8JsonReader_GetUInt64WithQuotes_m80FDBEC80D9C007947133F9D259BC28EC239CC53 (void);
// 0x0000025C System.Single System.Text.Json.Utf8JsonReader::GetSingle()
extern void Utf8JsonReader_GetSingle_m65F5DCB28FD9BE75EDBC6C94B20F26E32C3305DB (void);
// 0x0000025D System.Single System.Text.Json.Utf8JsonReader::GetSingleWithQuotes()
extern void Utf8JsonReader_GetSingleWithQuotes_mA66BC371F801F4DA57D5701543059442BD108597 (void);
// 0x0000025E System.Single System.Text.Json.Utf8JsonReader::GetSingleFloatingPointConstant()
extern void Utf8JsonReader_GetSingleFloatingPointConstant_m37AAA7DD71B9E5379C449141C075EED6C4BDD4AD (void);
// 0x0000025F System.Double System.Text.Json.Utf8JsonReader::GetDouble()
extern void Utf8JsonReader_GetDouble_m1D71F0D17190B5ADBF720FB13597184F00B83159 (void);
// 0x00000260 System.Double System.Text.Json.Utf8JsonReader::GetDoubleWithQuotes()
extern void Utf8JsonReader_GetDoubleWithQuotes_m62F2D7D5B21BCCCA007091EFC60D69F1B6B1A1B5 (void);
// 0x00000261 System.Double System.Text.Json.Utf8JsonReader::GetDoubleFloatingPointConstant()
extern void Utf8JsonReader_GetDoubleFloatingPointConstant_m623ADC6DD4A7D49297B7B7306DB045A9B295B91A (void);
// 0x00000262 System.Decimal System.Text.Json.Utf8JsonReader::GetDecimal()
extern void Utf8JsonReader_GetDecimal_mFEA9D944822478D418E3275FEF14E78CAECE0BA7 (void);
// 0x00000263 System.Decimal System.Text.Json.Utf8JsonReader::GetDecimalWithQuotes()
extern void Utf8JsonReader_GetDecimalWithQuotes_m7421A53B8EDCB1669FF8591DE10A9407E9224141 (void);
// 0x00000264 System.DateTime System.Text.Json.Utf8JsonReader::GetDateTime()
extern void Utf8JsonReader_GetDateTime_mA26D2BA988CF28A40FC962FED1145B29BFE57067 (void);
// 0x00000265 System.DateTime System.Text.Json.Utf8JsonReader::GetDateTimeNoValidation()
extern void Utf8JsonReader_GetDateTimeNoValidation_m4516B03D0EF902C15DFA9BAB9BF057ADC647B6DF (void);
// 0x00000266 System.DateTimeOffset System.Text.Json.Utf8JsonReader::GetDateTimeOffset()
extern void Utf8JsonReader_GetDateTimeOffset_mE45024C43B2BE11C35AC5DFA1B247F0C5C888A3C (void);
// 0x00000267 System.DateTimeOffset System.Text.Json.Utf8JsonReader::GetDateTimeOffsetNoValidation()
extern void Utf8JsonReader_GetDateTimeOffsetNoValidation_m62B6E29DF9475F352B974BDAAFC391B6133F7A7B (void);
// 0x00000268 System.Guid System.Text.Json.Utf8JsonReader::GetGuid()
extern void Utf8JsonReader_GetGuid_m7A0ECB26F40A0A64EE37C7393F109B3E63193CB5 (void);
// 0x00000269 System.Guid System.Text.Json.Utf8JsonReader::GetGuidNoValidation()
extern void Utf8JsonReader_GetGuidNoValidation_m5DF916F1E98E9F975BF7D48BD9760CAD9771CEC9 (void);
// 0x0000026A System.Boolean System.Text.Json.Utf8JsonReader::TryGetBytesFromBase64(System.Byte[]&)
extern void Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35 (void);
// 0x0000026B System.Boolean System.Text.Json.Utf8JsonReader::TryGetByte(System.Byte&)
extern void Utf8JsonReader_TryGetByte_m57093639B039FA045AE217E89A38023A51F25877 (void);
// 0x0000026C System.Boolean System.Text.Json.Utf8JsonReader::TryGetByteCore(System.Byte&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetByteCore_m5975171C440BA1DB0F5946CEE3A95AD25E90AA83 (void);
// 0x0000026D System.Boolean System.Text.Json.Utf8JsonReader::TryGetSByte(System.SByte&)
extern void Utf8JsonReader_TryGetSByte_mF7778E9C90F9D7C8CBE321D9C4B3859CE02B9106 (void);
// 0x0000026E System.Boolean System.Text.Json.Utf8JsonReader::TryGetSByteCore(System.SByte&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetSByteCore_m8789EC6277347F2746BACBE65069C7E63B3ED6EE (void);
// 0x0000026F System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt16(System.Int16&)
extern void Utf8JsonReader_TryGetInt16_m5E65C5335E39AAA608EC42137A1F9909B61C6085 (void);
// 0x00000270 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt16Core(System.Int16&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetInt16Core_m489F57E73BB2F417F88E7A98DA2ABB2705B3AD8B (void);
// 0x00000271 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt32(System.Int32&)
extern void Utf8JsonReader_TryGetInt32_m93479A8A49CE3407F4F48F9F3311D8CCECDFC7BA (void);
// 0x00000272 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt32Core(System.Int32&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetInt32Core_mF9C2BAB3D4887F47FD89DE68149E0E4FD95FD7F6 (void);
// 0x00000273 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt64(System.Int64&)
extern void Utf8JsonReader_TryGetInt64_m04985C755DD8030B3AC7D8045291994F453D42A3 (void);
// 0x00000274 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt64Core(System.Int64&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetInt64Core_m9984ADE08AAFE1243D84F6B9F3F6FF3D59B6D5CF (void);
// 0x00000275 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt16(System.UInt16&)
extern void Utf8JsonReader_TryGetUInt16_m018B28F35D595DDDBB3A104F69E2BAB463CDC274 (void);
// 0x00000276 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt16Core(System.UInt16&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetUInt16Core_m413B4DF3E32855458F822546C8F91B0138F71816 (void);
// 0x00000277 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt32(System.UInt32&)
extern void Utf8JsonReader_TryGetUInt32_m3568E2AB280AF64148B26E5A570DE313B5F6A557 (void);
// 0x00000278 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt32Core(System.UInt32&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetUInt32Core_m52DBDE64A8BE6BBFB4AC92E782FA38637E88B79D (void);
// 0x00000279 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt64(System.UInt64&)
extern void Utf8JsonReader_TryGetUInt64_mA1BFC2EDB1096EEA85D3F3992A6C33B599769A63 (void);
// 0x0000027A System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt64Core(System.UInt64&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetUInt64Core_m3DBEB9BA3D05D681BF354807C9D16B725CC7256D (void);
// 0x0000027B System.Boolean System.Text.Json.Utf8JsonReader::TryGetSingle(System.Single&)
extern void Utf8JsonReader_TryGetSingle_mF21A462DD40F497AE887B4F93EB3F329632E6D34 (void);
// 0x0000027C System.Boolean System.Text.Json.Utf8JsonReader::TryGetDouble(System.Double&)
extern void Utf8JsonReader_TryGetDouble_mA5FF188644DBB6DFF353DA7887B0A28B7322EDD6 (void);
// 0x0000027D System.Boolean System.Text.Json.Utf8JsonReader::TryGetDecimal(System.Decimal&)
extern void Utf8JsonReader_TryGetDecimal_m9129A3234F32FBF6B0C85C3F4D2AB8C5F008F297 (void);
// 0x0000027E System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTime(System.DateTime&)
extern void Utf8JsonReader_TryGetDateTime_m5D9B9E2E3B345769B3D1505271DBE6F7EF852895 (void);
// 0x0000027F System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTimeCore(System.DateTime&)
extern void Utf8JsonReader_TryGetDateTimeCore_m08A0D16A29B1B5071737A3E3C2A88FA68EC68A14 (void);
// 0x00000280 System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTimeOffset(System.DateTimeOffset&)
extern void Utf8JsonReader_TryGetDateTimeOffset_m33ADB6C9F6844507D161D845D20E471322D84587 (void);
// 0x00000281 System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTimeOffsetCore(System.DateTimeOffset&)
extern void Utf8JsonReader_TryGetDateTimeOffsetCore_m97770A3308970C50091E12FA977F276276D776DA (void);
// 0x00000282 System.Boolean System.Text.Json.Utf8JsonReader::TryGetGuid(System.Guid&)
extern void Utf8JsonReader_TryGetGuid_m5D97B625335AB390E15C8AA7A4D4794B8BA868A5 (void);
// 0x00000283 System.Boolean System.Text.Json.Utf8JsonReader::TryGetGuidCore(System.Guid&)
extern void Utf8JsonReader_TryGetGuidCore_mB8171BAB36B60E93D015CA3CF34B48BC53C7C171 (void);
// 0x00000284 System.Void System.Text.Json.Utf8JsonReader/PartialStateForRollback::.ctor(System.Int64,System.Int64,System.Int32,System.SequencePosition)
extern void PartialStateForRollback__ctor_m0A2CA55E0DA1307DCE07752039D69E1E171C6178 (void);
// 0x00000285 System.SequencePosition System.Text.Json.Utf8JsonReader/PartialStateForRollback::GetStartPosition(System.Int32)
extern void PartialStateForRollback_GetStartPosition_mD8F495352B433FEE2C9D27A58FDBAC331C418D63 (void);
// 0x00000286 System.Void System.Text.Json.Arguments`4::.ctor()
// 0x00000287 System.Void System.Text.Json.ArgumentState::.ctor()
extern void ArgumentState__ctor_m3033135B5B010F2D2835EF99DDB6C0E05FB75490 (void);
// 0x00000288 System.String System.Text.Json.JsonCamelCaseNamingPolicy::ConvertName(System.String)
extern void JsonCamelCaseNamingPolicy_ConvertName_mFB023872AB7D502C0CC509E886E3EA3B607C6DA2 (void);
// 0x00000289 System.Void System.Text.Json.JsonCamelCaseNamingPolicy::FixCasing(System.Span`1<System.Char>)
extern void JsonCamelCaseNamingPolicy_FixCasing_m2C442C406C789CE960E6D5F67C8F2E82B25F15E8 (void);
// 0x0000028A System.Void System.Text.Json.JsonCamelCaseNamingPolicy::.ctor()
extern void JsonCamelCaseNamingPolicy__ctor_mBBEC8A003FA852B335157E6EF9476B7EEA06B101 (void);
// 0x0000028B System.Text.Json.JsonClassInfo/ConstructorDelegate System.Text.Json.JsonClassInfo::get_CreateObject()
extern void JsonClassInfo_get_CreateObject_m3924F94319E8CC8037E397EC71A758534AE7E3EC (void);
// 0x0000028C System.Void System.Text.Json.JsonClassInfo::set_CreateObject(System.Text.Json.JsonClassInfo/ConstructorDelegate)
extern void JsonClassInfo_set_CreateObject_mCB383ACB75DFF40E78CD099DECE8A89CC5C95C9A (void);
// 0x0000028D System.Object System.Text.Json.JsonClassInfo::get_CreateObjectWithArgs()
extern void JsonClassInfo_get_CreateObjectWithArgs_m5F1565F5FBA9FF4DA02786C6654771AC177B1E39 (void);
// 0x0000028E System.Void System.Text.Json.JsonClassInfo::set_CreateObjectWithArgs(System.Object)
extern void JsonClassInfo_set_CreateObjectWithArgs_m737C399B0900D4807B7E6A881979765184CA70BE (void);
// 0x0000028F System.Object System.Text.Json.JsonClassInfo::get_AddMethodDelegate()
extern void JsonClassInfo_get_AddMethodDelegate_m488E73B03A89040319408B5DF87C9C4DA514335E (void);
// 0x00000290 System.Void System.Text.Json.JsonClassInfo::set_AddMethodDelegate(System.Object)
extern void JsonClassInfo_set_AddMethodDelegate_m99DA3023113AB512DAB80E80B226BD003AF03135 (void);
// 0x00000291 System.Text.Json.ClassType System.Text.Json.JsonClassInfo::get_ClassType()
extern void JsonClassInfo_get_ClassType_mB004262B1B41B552E9345A10FFF3F8E48F2D408B (void);
// 0x00000292 System.Void System.Text.Json.JsonClassInfo::set_ClassType(System.Text.Json.ClassType)
extern void JsonClassInfo_set_ClassType_m1FF3CC9DB07D8AB41E249DA6AF8AA0DBA83616B5 (void);
// 0x00000293 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::get_DataExtensionProperty()
extern void JsonClassInfo_get_DataExtensionProperty_mEE9451B64DEAF5818915EA439213B5A8FEC028FD (void);
// 0x00000294 System.Void System.Text.Json.JsonClassInfo::set_DataExtensionProperty(System.Text.Json.JsonPropertyInfo)
extern void JsonClassInfo_set_DataExtensionProperty_m869AA0DB667AFCA30A5BA4DAD22FC963343E9DEB (void);
// 0x00000295 System.Text.Json.JsonClassInfo System.Text.Json.JsonClassInfo::get_ElementClassInfo()
extern void JsonClassInfo_get_ElementClassInfo_m30E9BA9D8ADFA4C050194FC0EC1765CEF33A2BBF (void);
// 0x00000296 System.Type System.Text.Json.JsonClassInfo::get_ElementType()
extern void JsonClassInfo_get_ElementType_m770A51860EC4421D6741DCFF06916FB33AF53858 (void);
// 0x00000297 System.Void System.Text.Json.JsonClassInfo::set_ElementType(System.Type)
extern void JsonClassInfo_set_ElementType_mC498B4A763F7C2A01DBA9D553145F8D0CA6C7874 (void);
// 0x00000298 System.Text.Json.JsonSerializerOptions System.Text.Json.JsonClassInfo::get_Options()
extern void JsonClassInfo_get_Options_mA487DF18E8069C47D2FA573FB9DF584F488F93CE (void);
// 0x00000299 System.Void System.Text.Json.JsonClassInfo::set_Options(System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_set_Options_m2F599919B350219F457359B4E7FA7A1B1E83D8C6 (void);
// 0x0000029A System.Type System.Text.Json.JsonClassInfo::get_Type()
extern void JsonClassInfo_get_Type_mF4833C6030319D73C7C0E8115D34603648574D70 (void);
// 0x0000029B System.Void System.Text.Json.JsonClassInfo::set_Type(System.Type)
extern void JsonClassInfo_set_Type_m35C5D4D035C6196861487CAE2B4E7D8516848672 (void);
// 0x0000029C System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::get_PropertyInfoForClassInfo()
extern void JsonClassInfo_get_PropertyInfoForClassInfo_m43DD25A15635FAD68648F55AD79A8B6FB15433FC (void);
// 0x0000029D System.Void System.Text.Json.JsonClassInfo::set_PropertyInfoForClassInfo(System.Text.Json.JsonPropertyInfo)
extern void JsonClassInfo_set_PropertyInfoForClassInfo_m50F366281A4E6F986341608C934DEDA64EF2BFD2 (void);
// 0x0000029E System.Void System.Text.Json.JsonClassInfo::.ctor(System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo__ctor_mE1D0B01962472E469C523D57A75D32E64D88BE77 (void);
// 0x0000029F System.Void System.Text.Json.JsonClassInfo::CacheMember(System.Type,System.Type,System.Reflection.MemberInfo,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Collections.Generic.Dictionary`2<System.String,System.Text.Json.JsonPropertyInfo>,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void JsonClassInfo_CacheMember_mB47AA9FB5D7DFE32D3436A7739B067BD737D0605 (void);
// 0x000002A0 System.Void System.Text.Json.JsonClassInfo::InitializeConstructorParameters(System.Reflection.ConstructorInfo)
extern void JsonClassInfo_InitializeConstructorParameters_mA4841FC4DC12A035D08974A78209A4131C1F8952 (void);
// 0x000002A1 System.Boolean System.Text.Json.JsonClassInfo::PropertyIsOverridenAndIgnored(System.Reflection.MemberInfo,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>)
extern void JsonClassInfo_PropertyIsOverridenAndIgnored_m9F0F3A2F966803144ED9AECEB339FCDFE6854D1E (void);
// 0x000002A2 System.Boolean System.Text.Json.JsonClassInfo::PropertyIsVirtual(System.Reflection.PropertyInfo)
extern void JsonClassInfo_PropertyIsVirtual_m8610B207119E3153F68A000CEA57A0158CEEBC34 (void);
// 0x000002A3 System.Boolean System.Text.Json.JsonClassInfo::DetermineExtensionDataProperty(System.Collections.Generic.Dictionary`2<System.String,System.Text.Json.JsonPropertyInfo>)
extern void JsonClassInfo_DetermineExtensionDataProperty_m8E49914601E84681B4C73C9C2EE06DEA37AEE242 (void);
// 0x000002A4 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::GetPropertyWithUniqueAttribute(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Text.Json.JsonPropertyInfo>)
extern void JsonClassInfo_GetPropertyWithUniqueAttribute_m72402EC2871CD6CF02CB461628106D494F239B04 (void);
// 0x000002A5 System.Text.Json.JsonParameterInfo System.Text.Json.JsonClassInfo::AddConstructorParameter(System.Reflection.ParameterInfo,System.Text.Json.JsonPropertyInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_AddConstructorParameter_m48ADD8B3A3E613EB33C754E9AA41578D2840D51D (void);
// 0x000002A6 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonClassInfo::GetConverter(System.Type,System.Type,System.Reflection.MemberInfo,System.Type&,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_GetConverter_mDF285EF8F1E6020192A8D4D28862F1A72F1312C9 (void);
// 0x000002A7 System.Void System.Text.Json.JsonClassInfo::ValidateType(System.Type,System.Type,System.Reflection.MemberInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_ValidateType_m8A3CCC0F1BB681B9C694590B1657AB2AEF88A28E (void);
// 0x000002A8 System.Boolean System.Text.Json.JsonClassInfo::IsInvalidForSerialization(System.Type)
extern void JsonClassInfo_IsInvalidForSerialization_m2C0C98A692A0ECF16387ED03F935E8B1E649FCB4 (void);
// 0x000002A9 System.Boolean System.Text.Json.JsonClassInfo::IsByRefLike(System.Type)
extern void JsonClassInfo_IsByRefLike_mE5104DEA04DD67289BFF771B1D2BBD7891772E62 (void);
// 0x000002AA System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling> System.Text.Json.JsonClassInfo::GetNumberHandlingForType(System.Type)
extern void JsonClassInfo_GetNumberHandlingForType_mDE10C1748C995329323778BA99FEAFC956659114 (void);
// 0x000002AB System.Int32 System.Text.Json.JsonClassInfo::get_ParameterCount()
extern void JsonClassInfo_get_ParameterCount_m9A7A9DA3DC8B1796546F01CAE46D2F6AB5F71A84 (void);
// 0x000002AC System.Void System.Text.Json.JsonClassInfo::set_ParameterCount(System.Int32)
extern void JsonClassInfo_set_ParameterCount_m703F8184C00284D40E133AD69FB81EC896B5020F (void);
// 0x000002AD System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::AddProperty(System.Reflection.MemberInfo,System.Type,System.Type,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_AddProperty_m66CD279D4E591EA62DD5805E9377331B526D44FC (void);
// 0x000002AE System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::CreateProperty(System.Type,System.Type,System.Reflection.MemberInfo,System.Type,System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>)
extern void JsonClassInfo_CreateProperty_m100B36BF9AEDA5054F7105C2B583691B84162996 (void);
// 0x000002AF System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::CreatePropertyInfoForClassInfo(System.Type,System.Type,System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_CreatePropertyInfoForClassInfo_mF76E7B49D61833668E211B63C003F3DD4575EEDE (void);
// 0x000002B0 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::GetProperty(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStackFrame&,System.Byte[]&)
extern void JsonClassInfo_GetProperty_m3AD05B450229D1255C1064BE1D5B6F60F8DD4806 (void);
// 0x000002B1 System.Text.Json.JsonParameterInfo System.Text.Json.JsonClassInfo::GetParameter(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStackFrame&,System.Byte[]&)
extern void JsonClassInfo_GetParameter_m07D28EB05506CA359731F2B9C9DCA2D2020FC8BA (void);
// 0x000002B2 System.Boolean System.Text.Json.JsonClassInfo::IsPropertyRefEqual(System.Text.Json.PropertyRef&,System.ReadOnlySpan`1<System.Byte>,System.UInt64)
extern void JsonClassInfo_IsPropertyRefEqual_mB02C7B4FC60D6274B5A70866495240515CC8DF34 (void);
// 0x000002B3 System.Boolean System.Text.Json.JsonClassInfo::IsParameterRefEqual(System.Text.Json.ParameterRef&,System.ReadOnlySpan`1<System.Byte>,System.UInt64)
extern void JsonClassInfo_IsParameterRefEqual_m88A98AAE34615FB5DEB1F8309A5FF81DE7D1C639 (void);
// 0x000002B4 System.UInt64 System.Text.Json.JsonClassInfo::GetKey(System.ReadOnlySpan`1<System.Byte>)
extern void JsonClassInfo_GetKey_mAD8308BF1511DB603B12BEB82E22EF8E93A7C20F (void);
// 0x000002B5 System.Void System.Text.Json.JsonClassInfo::UpdateSortedPropertyCache(System.Text.Json.ReadStackFrame&)
extern void JsonClassInfo_UpdateSortedPropertyCache_m4659FB1EA75A9A250BF3ECA5DA92EE1736E83867 (void);
// 0x000002B6 System.Void System.Text.Json.JsonClassInfo::UpdateSortedParameterCache(System.Text.Json.ReadStackFrame&)
extern void JsonClassInfo_UpdateSortedParameterCache_mD7203B5F9AAA8035D6A3A06B7B376FA54460FAE6 (void);
// 0x000002B7 System.Void System.Text.Json.JsonClassInfo::.cctor()
extern void JsonClassInfo__cctor_mF826838C045B496FD4FB3EA1C87C593A20419021 (void);
// 0x000002B8 System.Type System.Text.Json.JsonClassInfo::<InitializeConstructorParameters>g__GetMemberType|46_0(System.Reflection.MemberInfo)
extern void JsonClassInfo_U3CInitializeConstructorParametersU3Eg__GetMemberTypeU7C46_0_mA004A63FFB79C1F8C5E65EC01C505FC2E75CDEBC (void);
// 0x000002B9 System.Void System.Text.Json.JsonClassInfo/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern void ConstructorDelegate__ctor_m8BF4F0FB25B31B565C9970B41F618B3E5FB0CFFA (void);
// 0x000002BA System.Object System.Text.Json.JsonClassInfo/ConstructorDelegate::Invoke()
extern void ConstructorDelegate_Invoke_m9B0B0DE22F6407E28E7A905F4B2FD373B2176511 (void);
// 0x000002BB System.Void System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x000002BC T System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1::Invoke(System.Object[])
// 0x000002BD System.Void System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5::.ctor(System.Object,System.IntPtr)
// 0x000002BE T System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5::Invoke(TArg0,TArg1,TArg2,TArg3)
// 0x000002BF System.Void System.Text.Json.JsonClassInfo/ParameterLookupKey::.ctor(System.String,System.Type)
extern void ParameterLookupKey__ctor_mE556DF61D1FA243C6204BA09DB581C64A01469D8 (void);
// 0x000002C0 System.String System.Text.Json.JsonClassInfo/ParameterLookupKey::get_Name()
extern void ParameterLookupKey_get_Name_m048FB355797E2E00063B62A5FDB6A30CE1C543CC (void);
// 0x000002C1 System.Type System.Text.Json.JsonClassInfo/ParameterLookupKey::get_Type()
extern void ParameterLookupKey_get_Type_m66F89A37473AEEDE2674D17D6FD97C773D54B23A (void);
// 0x000002C2 System.Int32 System.Text.Json.JsonClassInfo/ParameterLookupKey::GetHashCode()
extern void ParameterLookupKey_GetHashCode_m270F39E11121C94B24B3471AEA06E1396A8A962C (void);
// 0x000002C3 System.Boolean System.Text.Json.JsonClassInfo/ParameterLookupKey::Equals(System.Object)
extern void ParameterLookupKey_Equals_m763548E25FABB3FFC76054F10098EFB1D95E4F57 (void);
// 0x000002C4 System.Void System.Text.Json.JsonClassInfo/ParameterLookupValue::.ctor(System.Text.Json.JsonPropertyInfo)
extern void ParameterLookupValue__ctor_mD1613835BCF09DB67A265CB62B529F079E261816 (void);
// 0x000002C5 System.String System.Text.Json.JsonClassInfo/ParameterLookupValue::get_DuplicateName()
extern void ParameterLookupValue_get_DuplicateName_m2846FDEB4E05B3167D754D5F2774A3F8FFDD3B50 (void);
// 0x000002C6 System.Void System.Text.Json.JsonClassInfo/ParameterLookupValue::set_DuplicateName(System.String)
extern void ParameterLookupValue_set_DuplicateName_mE89B81F211FE3342D05118E95ACF64341027BB43 (void);
// 0x000002C7 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo/ParameterLookupValue::get_JsonPropertyInfo()
extern void ParameterLookupValue_get_JsonPropertyInfo_m8570A2FA9EDD6BFA2A1C5DE2B8AF26287C402CFA (void);
// 0x000002C8 System.String System.Text.Json.JsonDefaultNamingPolicy::ConvertName(System.String)
extern void JsonDefaultNamingPolicy_ConvertName_m7D36EE2599AE2EFFBCEE52F459253A5792DF27B1 (void);
// 0x000002C9 System.Void System.Text.Json.JsonDefaultNamingPolicy::.ctor()
extern void JsonDefaultNamingPolicy__ctor_m63421AA6B297DEABA6F0EE0AEB7EB42F55113CFA (void);
// 0x000002CA System.Void System.Text.Json.JsonNamingPolicy::.ctor()
extern void JsonNamingPolicy__ctor_m047EFD87C8E2BF12B060264523C949A0E353A080 (void);
// 0x000002CB System.String System.Text.Json.JsonNamingPolicy::ConvertName(System.String)
// 0x000002CC System.Void System.Text.Json.JsonNamingPolicy::.cctor()
extern void JsonNamingPolicy__cctor_m4CA469CACC21C0ABF740797B7F54A0536F8D46EC (void);
// 0x000002CD System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonParameterInfo::get_ConverterBase()
extern void JsonParameterInfo_get_ConverterBase_m280CF92A015816989461A48B357E7C2D1BA6D398 (void);
// 0x000002CE System.Void System.Text.Json.JsonParameterInfo::set_ConverterBase(System.Text.Json.Serialization.JsonConverter)
extern void JsonParameterInfo_set_ConverterBase_m63A442D88DCC1E79C3E1FA314ADE4351B0278E4D (void);
// 0x000002CF System.Object System.Text.Json.JsonParameterInfo::get_DefaultValue()
extern void JsonParameterInfo_get_DefaultValue_m9C4947604651E2557BBF7107864DCAB0DE907877 (void);
// 0x000002D0 System.Void System.Text.Json.JsonParameterInfo::set_DefaultValue(System.Object)
extern void JsonParameterInfo_set_DefaultValue_mD2FA38670AC6C3527521CDB54EAD7F359E8F8E1B (void);
// 0x000002D1 System.Boolean System.Text.Json.JsonParameterInfo::get_IgnoreDefaultValuesOnRead()
extern void JsonParameterInfo_get_IgnoreDefaultValuesOnRead_mFB2E81AD69B208C02EDA92059CC9F0706B162D4E (void);
// 0x000002D2 System.Void System.Text.Json.JsonParameterInfo::set_IgnoreDefaultValuesOnRead(System.Boolean)
extern void JsonParameterInfo_set_IgnoreDefaultValuesOnRead_m9A32A1D7B960DB0BCA25BF1D7F657C36E8AD43FA (void);
// 0x000002D3 System.Text.Json.JsonSerializerOptions System.Text.Json.JsonParameterInfo::get_Options()
extern void JsonParameterInfo_get_Options_mD740985271C61D63EE42CB9B9C49DF14B84639B7 (void);
// 0x000002D4 System.Void System.Text.Json.JsonParameterInfo::set_Options(System.Text.Json.JsonSerializerOptions)
extern void JsonParameterInfo_set_Options_m8D916748A8A1CFFAF7BBFECBE0B46461BD2A05B9 (void);
// 0x000002D5 System.Byte[] System.Text.Json.JsonParameterInfo::get_NameAsUtf8Bytes()
extern void JsonParameterInfo_get_NameAsUtf8Bytes_mC74D28CA16D8E6CF8F4341D3A10001917FA4EEC2 (void);
// 0x000002D6 System.Void System.Text.Json.JsonParameterInfo::set_NameAsUtf8Bytes(System.Byte[])
extern void JsonParameterInfo_set_NameAsUtf8Bytes_m9F52C8A73862C84E1AB8A136F85A8925778AF9C0 (void);
// 0x000002D7 System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling> System.Text.Json.JsonParameterInfo::get_NumberHandling()
extern void JsonParameterInfo_get_NumberHandling_mA793C420F52157BFAC40C89B747D48159D519FB4 (void);
// 0x000002D8 System.Void System.Text.Json.JsonParameterInfo::set_NumberHandling(System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>)
extern void JsonParameterInfo_set_NumberHandling_m69F1AFEAA58436C0EB39B05FB8FBE29870288CF5 (void);
// 0x000002D9 System.Int32 System.Text.Json.JsonParameterInfo::get_Position()
extern void JsonParameterInfo_get_Position_m4ECD7B7B8E7B34713BFC26010F66169AB271D601 (void);
// 0x000002DA System.Void System.Text.Json.JsonParameterInfo::set_Position(System.Int32)
extern void JsonParameterInfo_set_Position_m50E95F79ED24C836FC139AF7C7D3190BC0B2DC76 (void);
// 0x000002DB System.Text.Json.JsonClassInfo System.Text.Json.JsonParameterInfo::get_RuntimeClassInfo()
extern void JsonParameterInfo_get_RuntimeClassInfo_mAC478726629495B15346BB844B7197D268C52399 (void);
// 0x000002DC System.Type System.Text.Json.JsonParameterInfo::get_RuntimePropertyType()
extern void JsonParameterInfo_get_RuntimePropertyType_m12B753E79AA5EE3DDDE208C90283F9D7D7A1B9F6 (void);
// 0x000002DD System.Void System.Text.Json.JsonParameterInfo::set_RuntimePropertyType(System.Type)
extern void JsonParameterInfo_set_RuntimePropertyType_m936B468A974E2FC8D5A5771111A948E3FBBF1E0E (void);
// 0x000002DE System.Boolean System.Text.Json.JsonParameterInfo::get_ShouldDeserialize()
extern void JsonParameterInfo_get_ShouldDeserialize_m0BFEBB5B82CFD53BA18436FDF40579DD363D8C4B (void);
// 0x000002DF System.Void System.Text.Json.JsonParameterInfo::set_ShouldDeserialize(System.Boolean)
extern void JsonParameterInfo_set_ShouldDeserialize_m2B4028AA15136C622319698384D9F848B6575B6E (void);
// 0x000002E0 System.Void System.Text.Json.JsonParameterInfo::Initialize(System.Type,System.Reflection.ParameterInfo,System.Text.Json.JsonPropertyInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonParameterInfo_Initialize_m99FC496E75F369E9A069F32103D7CB38256E50AA (void);
// 0x000002E1 System.Text.Json.JsonParameterInfo System.Text.Json.JsonParameterInfo::CreateIgnoredParameterPlaceholder(System.Text.Json.JsonPropertyInfo)
extern void JsonParameterInfo_CreateIgnoredParameterPlaceholder_m51708F7954493FD7D828E145CE3F6FA8BA924877 (void);
// 0x000002E2 System.Void System.Text.Json.JsonParameterInfo::.ctor()
extern void JsonParameterInfo__ctor_m87C8C406AC90E89FBAA5D2A963DCB69E0F06B322 (void);
// 0x000002E3 T System.Text.Json.JsonParameterInfo`1::get_TypedDefaultValue()
// 0x000002E4 System.Void System.Text.Json.JsonParameterInfo`1::set_TypedDefaultValue(T)
// 0x000002E5 System.Void System.Text.Json.JsonParameterInfo`1::Initialize(System.Type,System.Reflection.ParameterInfo,System.Text.Json.JsonPropertyInfo,System.Text.Json.JsonSerializerOptions)
// 0x000002E6 System.Void System.Text.Json.JsonParameterInfo`1::.ctor()
// 0x000002E7 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonPropertyInfo::get_ConverterBase()
// 0x000002E8 System.Void System.Text.Json.JsonPropertyInfo::set_ConverterBase(System.Text.Json.Serialization.JsonConverter)
// 0x000002E9 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonPropertyInfo::GetPropertyPlaceholder()
extern void JsonPropertyInfo_GetPropertyPlaceholder_m0002854C8F4336E9CDB8E656BA31AC417679C15A (void);
// 0x000002EA System.Text.Json.JsonPropertyInfo System.Text.Json.JsonPropertyInfo::CreateIgnoredPropertyPlaceholder(System.Reflection.MemberInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonPropertyInfo_CreateIgnoredPropertyPlaceholder_mFC68A9FB329938FE039B4AC8DA44FB740B0C7409 (void);
// 0x000002EB System.Type System.Text.Json.JsonPropertyInfo::get_DeclaredPropertyType()
extern void JsonPropertyInfo_get_DeclaredPropertyType_mACBDE6A28FC500481702320EF6E636DCE7C73DAA (void);
// 0x000002EC System.Void System.Text.Json.JsonPropertyInfo::set_DeclaredPropertyType(System.Type)
extern void JsonPropertyInfo_set_DeclaredPropertyType_m58E832BD84C9BB9961F9818EF8E1A9B541E552DB (void);
// 0x000002ED System.Void System.Text.Json.JsonPropertyInfo::GetPolicies(System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Boolean)
extern void JsonPropertyInfo_GetPolicies_mA6712A0AE3BE7EC4D25DBA0974D37F38F0B73C56 (void);
// 0x000002EE System.Void System.Text.Json.JsonPropertyInfo::DeterminePropertyName()
extern void JsonPropertyInfo_DeterminePropertyName_m25078709433A5F0C62AF288414D8501FAF3A9FCE (void);
// 0x000002EF System.Void System.Text.Json.JsonPropertyInfo::DetermineSerializationCapabilities(System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>)
extern void JsonPropertyInfo_DetermineSerializationCapabilities_m1A2CC2F58501D7B30A1BB823C716EC1EE83631CA (void);
// 0x000002F0 System.Void System.Text.Json.JsonPropertyInfo::DetermineIgnoreCondition(System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Boolean)
extern void JsonPropertyInfo_DetermineIgnoreCondition_m0AFA34AD28A7DE506410D6782D297165A1AAC6E1 (void);
// 0x000002F1 System.Void System.Text.Json.JsonPropertyInfo::DetermineNumberHandling(System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>)
extern void JsonPropertyInfo_DetermineNumberHandling_m662836017EE113BCC3426911282DEC55FAC6818A (void);
// 0x000002F2 TAttribute System.Text.Json.JsonPropertyInfo::GetAttribute(System.Reflection.MemberInfo)
// 0x000002F3 System.Boolean System.Text.Json.JsonPropertyInfo::GetMemberAndWriteJson(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x000002F4 System.Boolean System.Text.Json.JsonPropertyInfo::GetMemberAndWriteJsonExtensionData(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x000002F5 System.Object System.Text.Json.JsonPropertyInfo::GetValueAsObject(System.Object)
// 0x000002F6 System.Boolean System.Text.Json.JsonPropertyInfo::get_HasGetter()
extern void JsonPropertyInfo_get_HasGetter_mA8ABECDB9D68BD370F0ECF0B51A530AF6CBE49C0 (void);
// 0x000002F7 System.Void System.Text.Json.JsonPropertyInfo::set_HasGetter(System.Boolean)
extern void JsonPropertyInfo_set_HasGetter_m3E3B9FE50F71BF6965E4179F44C5118394F4EB76 (void);
// 0x000002F8 System.Boolean System.Text.Json.JsonPropertyInfo::get_HasSetter()
extern void JsonPropertyInfo_get_HasSetter_mB7EBA2506BCCB52852390BA7C0EF680114BFCC8A (void);
// 0x000002F9 System.Void System.Text.Json.JsonPropertyInfo::set_HasSetter(System.Boolean)
extern void JsonPropertyInfo_set_HasSetter_mC66078A3C7DAAEF93BD18FEC9CCFCB684901BEBD (void);
// 0x000002FA System.Void System.Text.Json.JsonPropertyInfo::Initialize(System.Type,System.Type,System.Type,System.Text.Json.ClassType,System.Reflection.MemberInfo,System.Text.Json.Serialization.JsonConverter,System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Text.Json.JsonSerializerOptions)
extern void JsonPropertyInfo_Initialize_m87CEBD25FEBE62431919EE154555571AC147341A (void);
// 0x000002FB System.Boolean System.Text.Json.JsonPropertyInfo::get_IgnoreDefaultValuesOnRead()
extern void JsonPropertyInfo_get_IgnoreDefaultValuesOnRead_m485BF3036EFE5652D3354AB52BFE4AD9C462EBD4 (void);
// 0x000002FC System.Void System.Text.Json.JsonPropertyInfo::set_IgnoreDefaultValuesOnRead(System.Boolean)
extern void JsonPropertyInfo_set_IgnoreDefaultValuesOnRead_m9ACA6E5CBB4477A57064464E509A4A031287A294 (void);
// 0x000002FD System.Boolean System.Text.Json.JsonPropertyInfo::get_IgnoreDefaultValuesOnWrite()
extern void JsonPropertyInfo_get_IgnoreDefaultValuesOnWrite_mE576FFF14D1B98A34C69F485B99757933138AE42 (void);
// 0x000002FE System.Void System.Text.Json.JsonPropertyInfo::set_IgnoreDefaultValuesOnWrite(System.Boolean)
extern void JsonPropertyInfo_set_IgnoreDefaultValuesOnWrite_m591097336FB6F6C0AE4BFF25BBED8FC9515381D9 (void);
// 0x000002FF System.Boolean System.Text.Json.JsonPropertyInfo::get_IsForClassInfo()
extern void JsonPropertyInfo_get_IsForClassInfo_m5E9EB12AD97755E0026D726A4DC13F9D3A570A9C (void);
// 0x00000300 System.Void System.Text.Json.JsonPropertyInfo::set_IsForClassInfo(System.Boolean)
extern void JsonPropertyInfo_set_IsForClassInfo_mF91A27CA43772F77B01DF9DBE19276384CBD3A67 (void);
// 0x00000301 System.String System.Text.Json.JsonPropertyInfo::get_NameAsString()
extern void JsonPropertyInfo_get_NameAsString_m907A831A0369E765CDED710035B36F0FA5AC8B9B (void);
// 0x00000302 System.Void System.Text.Json.JsonPropertyInfo::set_NameAsString(System.String)
extern void JsonPropertyInfo_set_NameAsString_mD58CEEB19C9F3D7C5AD6406FCBD1355D53C48D36 (void);
// 0x00000303 System.Text.Json.JsonSerializerOptions System.Text.Json.JsonPropertyInfo::get_Options()
extern void JsonPropertyInfo_get_Options_m13E3BF459F8675C65E6C608482C9C0789F75B97A (void);
// 0x00000304 System.Void System.Text.Json.JsonPropertyInfo::set_Options(System.Text.Json.JsonSerializerOptions)
extern void JsonPropertyInfo_set_Options_m53F38E18046D1268125293BDDF9ACF1B0F34B668 (void);
// 0x00000305 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonAndAddExtensionProperty(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
extern void JsonPropertyInfo_ReadJsonAndAddExtensionProperty_mC188228BBDE083F556DF4A11AC496F3A19256D02 (void);
// 0x00000306 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonAndSetMember(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
// 0x00000307 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonAsObject(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Object&)
// 0x00000308 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonExtensionDataValue(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Object&)
extern void JsonPropertyInfo_ReadJsonExtensionDataValue_mAD9A1C2ED99013A0777C13386BE11F408668B44E (void);
// 0x00000309 System.Type System.Text.Json.JsonPropertyInfo::get_ParentClassType()
extern void JsonPropertyInfo_get_ParentClassType_mBFF8F3DC065713FD77F12788DFD0E353C0FA877F (void);
// 0x0000030A System.Void System.Text.Json.JsonPropertyInfo::set_ParentClassType(System.Type)
extern void JsonPropertyInfo_set_ParentClassType_m14715F89089DCC5D3B64C72199F01FE44409CD11 (void);
// 0x0000030B System.Reflection.MemberInfo System.Text.Json.JsonPropertyInfo::get_MemberInfo()
extern void JsonPropertyInfo_get_MemberInfo_mDB50F867A36435920C12ED71CA72580FF7EB3A8E (void);
// 0x0000030C System.Void System.Text.Json.JsonPropertyInfo::set_MemberInfo(System.Reflection.MemberInfo)
extern void JsonPropertyInfo_set_MemberInfo_m6BBABD3EA7E95FACE7C093144DB3648FBD62E4B6 (void);
// 0x0000030D System.Text.Json.JsonClassInfo System.Text.Json.JsonPropertyInfo::get_RuntimeClassInfo()
extern void JsonPropertyInfo_get_RuntimeClassInfo_m8CF6F07A07ED366B98034C37161FF6A37C5FEC96 (void);
// 0x0000030E System.Type System.Text.Json.JsonPropertyInfo::get_RuntimePropertyType()
extern void JsonPropertyInfo_get_RuntimePropertyType_mE2C5C15069C2A00DE2C1AD067091F95D79149019 (void);
// 0x0000030F System.Void System.Text.Json.JsonPropertyInfo::set_RuntimePropertyType(System.Type)
extern void JsonPropertyInfo_set_RuntimePropertyType_m5CFF3F102D982C87100EEFFF55192D91D75EB45B (void);
// 0x00000310 System.Void System.Text.Json.JsonPropertyInfo::SetExtensionDictionaryAsObject(System.Object,System.Object)
// 0x00000311 System.Boolean System.Text.Json.JsonPropertyInfo::get_ShouldSerialize()
extern void JsonPropertyInfo_get_ShouldSerialize_m17CDCACBC8CBDE574091BDCDD33CEB8D017839D4 (void);
// 0x00000312 System.Void System.Text.Json.JsonPropertyInfo::set_ShouldSerialize(System.Boolean)
extern void JsonPropertyInfo_set_ShouldSerialize_m8BA04314A995978B0066992640E2296C146C3839 (void);
// 0x00000313 System.Boolean System.Text.Json.JsonPropertyInfo::get_ShouldDeserialize()
extern void JsonPropertyInfo_get_ShouldDeserialize_mA6E24C13D03410019DE42F107146B0361AB3D5F0 (void);
// 0x00000314 System.Void System.Text.Json.JsonPropertyInfo::set_ShouldDeserialize(System.Boolean)
extern void JsonPropertyInfo_set_ShouldDeserialize_mD9BB3AA10AC77793B2D49D65460CAF4EC7908709 (void);
// 0x00000315 System.Boolean System.Text.Json.JsonPropertyInfo::get_IsIgnored()
extern void JsonPropertyInfo_get_IsIgnored_m21287FDA8B36179BDE92703E355B5CDCCD6B2C1D (void);
// 0x00000316 System.Void System.Text.Json.JsonPropertyInfo::set_IsIgnored(System.Boolean)
extern void JsonPropertyInfo_set_IsIgnored_m81FFCABC248845376883684AC190028BADAC7A84 (void);
// 0x00000317 System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling> System.Text.Json.JsonPropertyInfo::get_NumberHandling()
extern void JsonPropertyInfo_get_NumberHandling_m3F57EC55C9CC7CE9C1017CBF1B737759673F3947 (void);
// 0x00000318 System.Void System.Text.Json.JsonPropertyInfo::set_NumberHandling(System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>)
extern void JsonPropertyInfo_set_NumberHandling_m7171FCC5706B63D324F911FA3D868549B17183B0 (void);
// 0x00000319 System.Boolean System.Text.Json.JsonPropertyInfo::get_PropertyTypeCanBeNull()
extern void JsonPropertyInfo_get_PropertyTypeCanBeNull_m00E89F86EC69375E087FE13D14C58FF621132391 (void);
// 0x0000031A System.Void System.Text.Json.JsonPropertyInfo::set_PropertyTypeCanBeNull(System.Boolean)
extern void JsonPropertyInfo_set_PropertyTypeCanBeNull_m1B4AEF7B3C4E167742433F15E6DBBA625C7FAFA0 (void);
// 0x0000031B System.Void System.Text.Json.JsonPropertyInfo::.ctor()
extern void JsonPropertyInfo__ctor_mA12D185F265F12C6EFE9E29C1225A3490338B1F3 (void);
// 0x0000031C System.Void System.Text.Json.JsonPropertyInfo::.cctor()
extern void JsonPropertyInfo__cctor_m283BE798DF2D39D8379E8E49E98F9109F6296BD2 (void);
// 0x0000031D System.Func`2<System.Object,T> System.Text.Json.JsonPropertyInfo`1::get_Get()
// 0x0000031E System.Void System.Text.Json.JsonPropertyInfo`1::set_Get(System.Func`2<System.Object,T>)
// 0x0000031F System.Action`2<System.Object,T> System.Text.Json.JsonPropertyInfo`1::get_Set()
// 0x00000320 System.Void System.Text.Json.JsonPropertyInfo`1::set_Set(System.Action`2<System.Object,T>)
// 0x00000321 System.Text.Json.Serialization.JsonConverter`1<T> System.Text.Json.JsonPropertyInfo`1::get_Converter()
// 0x00000322 System.Void System.Text.Json.JsonPropertyInfo`1::set_Converter(System.Text.Json.Serialization.JsonConverter`1<T>)
// 0x00000323 System.Void System.Text.Json.JsonPropertyInfo`1::Initialize(System.Type,System.Type,System.Type,System.Text.Json.ClassType,System.Reflection.MemberInfo,System.Text.Json.Serialization.JsonConverter,System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Text.Json.JsonSerializerOptions)
// 0x00000324 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonPropertyInfo`1::get_ConverterBase()
// 0x00000325 System.Void System.Text.Json.JsonPropertyInfo`1::set_ConverterBase(System.Text.Json.Serialization.JsonConverter)
// 0x00000326 System.Object System.Text.Json.JsonPropertyInfo`1::GetValueAsObject(System.Object)
// 0x00000327 System.Boolean System.Text.Json.JsonPropertyInfo`1::GetMemberAndWriteJson(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x00000328 System.Boolean System.Text.Json.JsonPropertyInfo`1::GetMemberAndWriteJsonExtensionData(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x00000329 System.Boolean System.Text.Json.JsonPropertyInfo`1::ReadJsonAndSetMember(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
// 0x0000032A System.Boolean System.Text.Json.JsonPropertyInfo`1::ReadJsonAsObject(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Object&)
// 0x0000032B System.Void System.Text.Json.JsonPropertyInfo`1::SetExtensionDictionaryAsObject(System.Object,System.Object)
// 0x0000032C System.Void System.Text.Json.JsonPropertyInfo`1::.ctor()
// 0x0000032D System.Boolean System.Text.Json.JsonSerializer::ResolveMetadataForJsonObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000032E System.Boolean System.Text.Json.JsonSerializer::ResolveMetadataForJsonArray(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000032F System.Boolean System.Text.Json.JsonSerializer::TryReadAheadMetadataAndSetState(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.StackFrameObjectState)
extern void JsonSerializer_TryReadAheadMetadataAndSetState_m18C3E5513D3BD9B27109262DCD4DC5A0FDC6402A (void);
// 0x00000330 System.Text.Json.MetadataPropertyName System.Text.Json.JsonSerializer::GetMetadataPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void JsonSerializer_GetMetadataPropertyName_mF1443E52F7898BD6C9BCC6B6C5787A1BC84E6BC7 (void);
// 0x00000331 System.Boolean System.Text.Json.JsonSerializer::TryGetReferenceFromJsonElement(System.Text.Json.ReadStack&,System.Text.Json.JsonElement,System.Object&)
extern void JsonSerializer_TryGetReferenceFromJsonElement_m17DB1EDA1C943CA0F9F145EE601A69DBAB9B6387 (void);
// 0x00000332 System.Void System.Text.Json.JsonSerializer::ValidateValueIsCorrectType(System.Object,System.String)
// 0x00000333 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonSerializer::LookupProperty(System.Object,System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&,System.Boolean&,System.Boolean)
extern void JsonSerializer_LookupProperty_m6AA7BC6D36648D0E270EDB172BBF757DBBC98C09 (void);
// 0x00000334 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonSerializer::GetPropertyName(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
extern void JsonSerializer_GetPropertyName_mB251E25F8AF927A1FB1795EBD3EFA0C50E0D7237 (void);
// 0x00000335 System.Void System.Text.Json.JsonSerializer::CreateDataExtensionProperty(System.Object,System.Text.Json.JsonPropertyInfo)
extern void JsonSerializer_CreateDataExtensionProperty_mA9DA737787344F0BE4E3029D717E31BCC4AF2D49 (void);
// 0x00000336 TValue System.Text.Json.JsonSerializer::ReadCore(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000337 TValue System.Text.Json.JsonSerializer::ReadCore(System.Text.Json.Serialization.JsonConverter,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000338 TValue System.Text.Json.JsonSerializer::Deserialize(System.String,System.Text.Json.JsonSerializerOptions)
// 0x00000339 TValue System.Text.Json.JsonSerializer::Deserialize(System.String,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x0000033A System.Text.Json.MetadataPropertyName System.Text.Json.JsonSerializer::WriteReferenceForObject(System.Text.Json.Serialization.JsonConverter,System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
extern void JsonSerializer_WriteReferenceForObject_m160349753BEA999B4A3A1247586C186A07369272 (void);
// 0x0000033B System.Text.Json.MetadataPropertyName System.Text.Json.JsonSerializer::WriteReferenceForCollection(System.Text.Json.Serialization.JsonConverter,System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
extern void JsonSerializer_WriteReferenceForCollection_mB0094640168D976CA3C8D605724B441E1744D15C (void);
// 0x0000033C System.Void System.Text.Json.JsonSerializer::WriteCore(System.Text.Json.Utf8JsonWriter,TValue&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x0000033D System.Boolean System.Text.Json.JsonSerializer::WriteCore(System.Text.Json.Serialization.JsonConverter,System.Text.Json.Utf8JsonWriter,TValue&,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000033E System.String System.Text.Json.JsonSerializer::Serialize(TValue,System.Text.Json.JsonSerializerOptions)
// 0x0000033F System.String System.Text.Json.JsonSerializer::Serialize(TValue&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000340 System.Void System.Text.Json.JsonSerializer::.cctor()
extern void JsonSerializer__cctor_mCB5617E55AB44E467BEDEF30A3D1EE3CF7F184F1 (void);
// 0x00000341 System.Collections.Generic.Dictionary`2<System.Type,System.Text.Json.Serialization.JsonConverter> System.Text.Json.JsonSerializerOptions::GetDefaultSimpleConverters()
extern void JsonSerializerOptions_GetDefaultSimpleConverters_mB2C7D4015A718DC111FCF029A4B011CF576262ED (void);
// 0x00000342 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::GetDictionaryKeyConverter(System.Type)
extern void JsonSerializerOptions_GetDictionaryKeyConverter_mBF44EBD27BAFE9B9A177824C3A72E42767B6DAB9 (void);
// 0x00000343 System.Collections.Concurrent.ConcurrentDictionary`2<System.Type,System.Text.Json.Serialization.JsonConverter> System.Text.Json.JsonSerializerOptions::GetDictionaryKeyConverters()
extern void JsonSerializerOptions_GetDictionaryKeyConverters_m47585D77EE9AC1A259A1F1C23BC8BF3E9E7B84EB (void);
// 0x00000344 System.Collections.Generic.IList`1<System.Text.Json.Serialization.JsonConverter> System.Text.Json.JsonSerializerOptions::get_Converters()
extern void JsonSerializerOptions_get_Converters_m387F196CD3A362700DF74ECFEC1DE4873D84180E (void);
// 0x00000345 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::DetermineConverter(System.Type,System.Type,System.Reflection.MemberInfo)
extern void JsonSerializerOptions_DetermineConverter_m1DB19C7DF6014BB9051B050BC6886B48B13EFC4E (void);
// 0x00000346 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::GetConverter(System.Type)
extern void JsonSerializerOptions_GetConverter_m3F90FE1FF74CAFEDE6BA42D78D9AFB3CF317D6E0 (void);
// 0x00000347 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::GetConverterFromAttribute(System.Text.Json.Serialization.JsonConverterAttribute,System.Type,System.Type,System.Reflection.MemberInfo)
extern void JsonSerializerOptions_GetConverterFromAttribute_m2AA115134BF1C2F46386A9F21F61106BF6EDBDFC (void);
// 0x00000348 System.Attribute System.Text.Json.JsonSerializerOptions::GetAttributeThatCanHaveMultiple(System.Type,System.Type,System.Reflection.MemberInfo)
extern void JsonSerializerOptions_GetAttributeThatCanHaveMultiple_mC2B1111C6D9FBF3E67BCEEA476EB66A5BDA9FEDC (void);
// 0x00000349 System.Attribute System.Text.Json.JsonSerializerOptions::GetAttributeThatCanHaveMultiple(System.Type,System.Type)
extern void JsonSerializerOptions_GetAttributeThatCanHaveMultiple_mA27AFAD71EBA68DF05A1E77BF2DC90E24695AF9F (void);
// 0x0000034A System.Attribute System.Text.Json.JsonSerializerOptions::GetAttributeThatCanHaveMultiple(System.Type,System.Type,System.Reflection.MemberInfo,System.Object[])
extern void JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m89673A39073669E7EF22CF83B03EB008C318CB92 (void);
// 0x0000034B System.Text.Json.JsonClassInfo System.Text.Json.JsonSerializerOptions::get__lastClass()
extern void JsonSerializerOptions_get__lastClass_m625BC41A0607D88F18B0796584D3AA94640639EE (void);
// 0x0000034C System.Void System.Text.Json.JsonSerializerOptions::set__lastClass(System.Text.Json.JsonClassInfo)
extern void JsonSerializerOptions_set__lastClass_mC4E971444165788CBAF11FE1B3BA0DFACCCF0CC8 (void);
// 0x0000034D System.Void System.Text.Json.JsonSerializerOptions::.ctor()
extern void JsonSerializerOptions__ctor_m685DCDC94C86F90CDBD5704BDCEA4F983E71FF32 (void);
// 0x0000034E System.Boolean System.Text.Json.JsonSerializerOptions::get_AllowTrailingCommas()
extern void JsonSerializerOptions_get_AllowTrailingCommas_m03E3A6D2DFD2C0BC49693FA42A9926E6FF51F701 (void);
// 0x0000034F System.Int32 System.Text.Json.JsonSerializerOptions::get_DefaultBufferSize()
extern void JsonSerializerOptions_get_DefaultBufferSize_mD25998F6684286580A73189AD652EEB026F854AA (void);
// 0x00000350 System.Text.Encodings.Web.JavaScriptEncoder System.Text.Json.JsonSerializerOptions::get_Encoder()
extern void JsonSerializerOptions_get_Encoder_mB75B706FD84160A43BF875AB8B45C29944A7E996 (void);
// 0x00000351 System.Text.Json.JsonNamingPolicy System.Text.Json.JsonSerializerOptions::get_DictionaryKeyPolicy()
extern void JsonSerializerOptions_get_DictionaryKeyPolicy_mA6A655B77780FD0D1C0D6C1B4F5F5E18ADF21370 (void);
// 0x00000352 System.Boolean System.Text.Json.JsonSerializerOptions::get_IgnoreNullValues()
extern void JsonSerializerOptions_get_IgnoreNullValues_mA7D834AD9E4988A07DA6632B315D170D07219F4E (void);
// 0x00000353 System.Text.Json.Serialization.JsonIgnoreCondition System.Text.Json.JsonSerializerOptions::get_DefaultIgnoreCondition()
extern void JsonSerializerOptions_get_DefaultIgnoreCondition_m042572302083490D8932064ADCB08DA3BBD8E943 (void);
// 0x00000354 System.Text.Json.Serialization.JsonNumberHandling System.Text.Json.JsonSerializerOptions::get_NumberHandling()
extern void JsonSerializerOptions_get_NumberHandling_mBCFB177F8810B8E86E17A3EE8CD887B60CDD3B06 (void);
// 0x00000355 System.Boolean System.Text.Json.JsonSerializerOptions::get_IgnoreReadOnlyProperties()
extern void JsonSerializerOptions_get_IgnoreReadOnlyProperties_m02A855070C522C0E3235E755F025CB19AF5E60C9 (void);
// 0x00000356 System.Boolean System.Text.Json.JsonSerializerOptions::get_IgnoreReadOnlyFields()
extern void JsonSerializerOptions_get_IgnoreReadOnlyFields_m6B05586263255677FD679759BC56EAD61C7670A5 (void);
// 0x00000357 System.Boolean System.Text.Json.JsonSerializerOptions::get_IncludeFields()
extern void JsonSerializerOptions_get_IncludeFields_mCB66F8236EDEE738FF5C027C3BE758CF6144F86D (void);
// 0x00000358 System.Int32 System.Text.Json.JsonSerializerOptions::get_MaxDepth()
extern void JsonSerializerOptions_get_MaxDepth_m76A401099406884829012AFD5D3974841DCD2414 (void);
// 0x00000359 System.Int32 System.Text.Json.JsonSerializerOptions::get_EffectiveMaxDepth()
extern void JsonSerializerOptions_get_EffectiveMaxDepth_m5B3CE03573116B7D5920A24F386226C7497CA6B9 (void);
// 0x0000035A System.Text.Json.JsonNamingPolicy System.Text.Json.JsonSerializerOptions::get_PropertyNamingPolicy()
extern void JsonSerializerOptions_get_PropertyNamingPolicy_mE46B28D19C9052B9D6155852651866DD63ED44A7 (void);
// 0x0000035B System.Boolean System.Text.Json.JsonSerializerOptions::get_PropertyNameCaseInsensitive()
extern void JsonSerializerOptions_get_PropertyNameCaseInsensitive_mD32CF777E463003A0B778207E3EBD452E6A613F1 (void);
// 0x0000035C System.Text.Json.JsonCommentHandling System.Text.Json.JsonSerializerOptions::get_ReadCommentHandling()
extern void JsonSerializerOptions_get_ReadCommentHandling_m71B386675D5DC6B9B0369F7DF3091F2677D80F0B (void);
// 0x0000035D System.Boolean System.Text.Json.JsonSerializerOptions::get_WriteIndented()
extern void JsonSerializerOptions_get_WriteIndented_m684D835945BC60ED198825303361C90444824CD5 (void);
// 0x0000035E System.Text.Json.Serialization.ReferenceHandler System.Text.Json.JsonSerializerOptions::get_ReferenceHandler()
extern void JsonSerializerOptions_get_ReferenceHandler_m0617CB46B783371338540A3A470133DE6CB5CD9A (void);
// 0x0000035F System.Text.Json.MemberAccessor System.Text.Json.JsonSerializerOptions::get_MemberAccessorStrategy()
extern void JsonSerializerOptions_get_MemberAccessorStrategy_mA1BC386CE6006A4E6AE72350C652E32EF9D29B0F (void);
// 0x00000360 System.Text.Json.JsonClassInfo System.Text.Json.JsonSerializerOptions::GetOrAddClass(System.Type)
extern void JsonSerializerOptions_GetOrAddClass_mC4C69C74911631072200AD6AB5A4343176D95D07 (void);
// 0x00000361 System.Text.Json.JsonClassInfo System.Text.Json.JsonSerializerOptions::GetOrAddClassForRootType(System.Type)
extern void JsonSerializerOptions_GetOrAddClassForRootType_mD9ECD25EC6AA5ECFD8AB35CA89BA487C6AE2EC8A (void);
// 0x00000362 System.Boolean System.Text.Json.JsonSerializerOptions::TypeIsCached(System.Type)
extern void JsonSerializerOptions_TypeIsCached_mF038D4D0C7E6D190C5D60B3D5CC2D68B5B8CC961 (void);
// 0x00000363 System.Text.Json.JsonReaderOptions System.Text.Json.JsonSerializerOptions::GetReaderOptions()
extern void JsonSerializerOptions_GetReaderOptions_mD8A4C5D8650831EA9415502C95563ADAE6E14336 (void);
// 0x00000364 System.Text.Json.JsonWriterOptions System.Text.Json.JsonSerializerOptions::GetWriterOptions()
extern void JsonSerializerOptions_GetWriterOptions_mD1361A9A74AF5C0CAA7FAB06718C9516989CED47 (void);
// 0x00000365 System.Void System.Text.Json.JsonSerializerOptions::VerifyMutable()
extern void JsonSerializerOptions_VerifyMutable_m0C305EB691332076592359F232D8639FF95467F6 (void);
// 0x00000366 System.Void System.Text.Json.JsonSerializerOptions::.cctor()
extern void JsonSerializerOptions__cctor_mE6F9A7547EBF12548CB7B40D6DB6851CA14232AC (void);
// 0x00000367 System.Void System.Text.Json.JsonSerializerOptions::<GetDefaultSimpleConverters>g__Add|3_0(System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions/<>c__DisplayClass3_0&)
extern void JsonSerializerOptions_U3CGetDefaultSimpleConvertersU3Eg__AddU7C3_0_mE1E7914A06F0CF6977D174FD4DDDCD2BA2D7CC86 (void);
// 0x00000368 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::<GetDictionaryKeyConverter>g__GetEnumConverter|4_0(System.Text.Json.JsonSerializerOptions/<>c__DisplayClass4_0&)
extern void JsonSerializerOptions_U3CGetDictionaryKeyConverterU3Eg__GetEnumConverterU7C4_0_m422925E719E621089038D602E3BBD474F7C96DB5 (void);
// 0x00000369 System.Void System.Text.Json.JsonSerializerOptions::<GetDictionaryKeyConverters>g__Add|6_0(System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions/<>c__DisplayClass6_0&)
extern void JsonSerializerOptions_U3CGetDictionaryKeyConvertersU3Eg__AddU7C6_0_mDA972273A76E4AF978A8EF39C01033AAA1D83A96 (void);
// 0x0000036A System.Text.Json.JsonClassInfo/ConstructorDelegate System.Text.Json.MemberAccessor::CreateConstructor(System.Type)
// 0x0000036B System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1<T> System.Text.Json.MemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x0000036C System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5<T,TArg0,TArg1,TArg2,TArg3> System.Text.Json.MemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x0000036D System.Action`2<TCollection,System.Object> System.Text.Json.MemberAccessor::CreateAddMethodDelegate()
// 0x0000036E System.Func`2<System.Collections.Generic.IEnumerable`1<TElement>,TCollection> System.Text.Json.MemberAccessor::CreateImmutableEnumerableCreateRangeDelegate()
// 0x0000036F System.Func`2<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>>,TCollection> System.Text.Json.MemberAccessor::CreateImmutableDictionaryCreateRangeDelegate()
// 0x00000370 System.Func`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreatePropertyGetter(System.Reflection.PropertyInfo)
// 0x00000371 System.Action`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreatePropertySetter(System.Reflection.PropertyInfo)
// 0x00000372 System.Func`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreateFieldGetter(System.Reflection.FieldInfo)
// 0x00000373 System.Action`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreateFieldSetter(System.Reflection.FieldInfo)
// 0x00000374 System.Void System.Text.Json.MemberAccessor::.ctor()
extern void MemberAccessor__ctor_mAF5FE11042E3052B767A8602683A634754B107CC (void);
// 0x00000375 System.Void System.Text.Json.ParameterRef::.ctor(System.UInt64,System.Text.Json.JsonParameterInfo,System.Byte[])
extern void ParameterRef__ctor_mAE5D0234AF0B54F1B4F1C0F808F635329A9928A2 (void);
// 0x00000376 System.Void System.Text.Json.PropertyRef::.ctor(System.UInt64,System.Text.Json.JsonPropertyInfo,System.Byte[])
extern void PropertyRef__ctor_mA2ED2603D61FFE0F81222405419BB00EE8455EF4 (void);
// 0x00000377 System.Boolean System.Text.Json.ReadStack::get_IsContinuation()
extern void ReadStack_get_IsContinuation_mD01FD9A2DAC4FD9C3E805929E0104AAED9EFF95A (void);
// 0x00000378 System.Boolean System.Text.Json.ReadStack::get_IsLastContinuation()
extern void ReadStack_get_IsLastContinuation_mA8012A8628BE1311AE139DD8DC7D98E902EACF7F (void);
// 0x00000379 System.Void System.Text.Json.ReadStack::AddCurrent()
extern void ReadStack_AddCurrent_mF400E1F933A12C71C19C3A4DFCBE009008B823D5 (void);
// 0x0000037A System.Void System.Text.Json.ReadStack::Initialize(System.Type,System.Text.Json.JsonSerializerOptions,System.Boolean)
extern void ReadStack_Initialize_m7E7F3ED59DEEFF3DC132C8773C83EE001A238EAC (void);
// 0x0000037B System.Void System.Text.Json.ReadStack::Push()
extern void ReadStack_Push_mA7CB7FCFBF6F542FD626BDE571D54F1D136B3C0E (void);
// 0x0000037C System.Void System.Text.Json.ReadStack::Pop(System.Boolean)
extern void ReadStack_Pop_mD314744BCA164729B64A75EAAEB43FC240346DAB (void);
// 0x0000037D System.String System.Text.Json.ReadStack::JsonPath()
extern void ReadStack_JsonPath_m86FCB866216FA16E285600FB62136552D1CA7DED (void);
// 0x0000037E System.Void System.Text.Json.ReadStack::SetConstructorArgumentState()
extern void ReadStack_SetConstructorArgumentState_mB792C92FEA853A82C6564EB5F41E3A2ED909B992 (void);
// 0x0000037F System.Void System.Text.Json.ReadStack::.cctor()
extern void ReadStack__cctor_m9F755C7761204A7F190514C335221AE561537546 (void);
// 0x00000380 System.Void System.Text.Json.ReadStack::<JsonPath>g__AppendStackFrame|19_0(System.Text.StringBuilder,System.Text.Json.ReadStackFrame&)
extern void ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m8ED401FF34693D816348D4CE72C93E7394006130 (void);
// 0x00000381 System.Int32 System.Text.Json.ReadStack::<JsonPath>g__GetCount|19_1(System.Collections.IEnumerable)
extern void ReadStack_U3CJsonPathU3Eg__GetCountU7C19_1_m75E574E6B1DBFD7C4822567526DC011D8AD9FC40 (void);
// 0x00000382 System.Void System.Text.Json.ReadStack::<JsonPath>g__AppendPropertyName|19_2(System.Text.StringBuilder,System.String)
extern void ReadStack_U3CJsonPathU3Eg__AppendPropertyNameU7C19_2_mA80E427F474EAE78F6CC63C1028369018338D28E (void);
// 0x00000383 System.String System.Text.Json.ReadStack::<JsonPath>g__GetPropertyName|19_3(System.Text.Json.ReadStackFrame&)
extern void ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m104FC083CCE5BB72491DD4BA0B26BCCE2BAFB176 (void);
// 0x00000384 System.Void System.Text.Json.ReadStackFrame::EndConstructorParameter()
extern void ReadStackFrame_EndConstructorParameter_mE5D6B2F709289B69DD06F58040B1AA47DF4C4EC7 (void);
// 0x00000385 System.Void System.Text.Json.ReadStackFrame::EndProperty()
extern void ReadStackFrame_EndProperty_m6BBD5C956CCF9412B4B122B5E2501681DE66EEBA (void);
// 0x00000386 System.Void System.Text.Json.ReadStackFrame::EndElement()
extern void ReadStackFrame_EndElement_m4F6CA0BDACF8EEAB43A942243D21DF32C5C26C8D (void);
// 0x00000387 System.Boolean System.Text.Json.ReadStackFrame::IsProcessingDictionary()
extern void ReadStackFrame_IsProcessingDictionary_mBC439FBEB5E92414A12DBE4F25CFC1B8B541ED0A (void);
// 0x00000388 System.Boolean System.Text.Json.ReadStackFrame::IsProcessingEnumerable()
extern void ReadStackFrame_IsProcessingEnumerable_m8BA2785FB705C7BC0FFFAE84E35516CCAA028D8A (void);
// 0x00000389 System.Void System.Text.Json.ReadStackFrame::Reset()
extern void ReadStackFrame_Reset_mA12D43AF86177A2B7822F04C0E20B9556D8867F1 (void);
// 0x0000038A System.Boolean System.Text.Json.WriteStack::get_IsContinuation()
extern void WriteStack_get_IsContinuation_m88FEB1F19992286B54F88DE66079DAA6CB1BF319 (void);
// 0x0000038B System.Void System.Text.Json.WriteStack::AddCurrent()
extern void WriteStack_AddCurrent_m47F360C33799C68FED06B57AA0321371A5A4CF68 (void);
// 0x0000038C System.Text.Json.Serialization.JsonConverter System.Text.Json.WriteStack::Initialize(System.Type,System.Text.Json.JsonSerializerOptions,System.Boolean)
extern void WriteStack_Initialize_m1BA93CE9F8C4B1A365B61E8AF2A3B0D1FBCF7F3F (void);
// 0x0000038D System.Void System.Text.Json.WriteStack::Push()
extern void WriteStack_Push_mEFB808A906431C95FBFE4A654F9FEFB8C962F048 (void);
// 0x0000038E System.Void System.Text.Json.WriteStack::Pop(System.Boolean)
extern void WriteStack_Pop_mA03ABCC412B4F0C5720FB1774D6A61F716450B48 (void);
// 0x0000038F System.String System.Text.Json.WriteStack::PropertyPath()
extern void WriteStack_PropertyPath_m2B7C7609CF52ABC0DCCB60162CB58BFEA3EE4969 (void);
// 0x00000390 System.Void System.Text.Json.WriteStack::<PropertyPath>g__AppendStackFrame|13_0(System.Text.StringBuilder,System.Text.Json.WriteStackFrame&)
extern void WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_mFA8217E89FE0DFE851708748202127A07F74AEBA (void);
// 0x00000391 System.Void System.Text.Json.WriteStack::<PropertyPath>g__AppendPropertyName|13_1(System.Text.StringBuilder,System.String)
extern void WriteStack_U3CPropertyPathU3Eg__AppendPropertyNameU7C13_1_m5A499E425FD4D40245FC2BDDF6CB558870F6A287 (void);
// 0x00000392 System.Void System.Text.Json.WriteStackFrame::EndDictionaryElement()
extern void WriteStackFrame_EndDictionaryElement_mF8B3A60F6AAB6D4EF329FC9EC0026B1BF43A184B (void);
// 0x00000393 System.Void System.Text.Json.WriteStackFrame::EndProperty()
extern void WriteStackFrame_EndProperty_mFD22836ABA865D92442FBBC0E612A51A74F0F503 (void);
// 0x00000394 System.Text.Json.JsonPropertyInfo System.Text.Json.WriteStackFrame::GetPolymorphicJsonPropertyInfo()
extern void WriteStackFrame_GetPolymorphicJsonPropertyInfo_mC6241293FD5975BCF17AE77AAD2A1735932D473C (void);
// 0x00000395 System.Text.Json.Serialization.JsonConverter System.Text.Json.WriteStackFrame::InitializeReEntry(System.Type,System.Text.Json.JsonSerializerOptions,System.String)
extern void WriteStackFrame_InitializeReEntry_m6F30B6BDCE7931B9867F8DE7CE69299CA0C5330B (void);
// 0x00000396 System.Void System.Text.Json.WriteStackFrame::Reset()
extern void WriteStackFrame_Reset_m919F66E6B37A5AABD075DF89DC00723E2FE74485 (void);
// 0x00000397 System.Boolean System.Text.Json.TypeExtensions::IsNullableValueType(System.Type)
extern void TypeExtensions_IsNullableValueType_m4898086A046C3E4BAE28D49F62382EABE2D51079 (void);
// 0x00000398 System.Boolean System.Text.Json.TypeExtensions::IsNullableType(System.Type)
extern void TypeExtensions_IsNullableType_m12352B86A8DCA9AB2EB22C019B3A19C15144CFFF (void);
// 0x00000399 System.Boolean System.Text.Json.TypeExtensions::IsAssignableFromInternal(System.Type,System.Type)
extern void TypeExtensions_IsAssignableFromInternal_m87241C5103114B0C94797AC15693EF1AC0C62803 (void);
// 0x0000039A System.Void System.Text.Json.JsonWriterHelper::WriteIndentation(System.Span`1<System.Byte>,System.Int32)
extern void JsonWriterHelper_WriteIndentation_m8712AB577179603DF11C02333F9C0B4FD5388AF4 (void);
// 0x0000039B System.Void System.Text.Json.JsonWriterHelper::ValidateProperty(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateProperty_mA9619E69A4DEE0A2EF600C8881552A4A02DE6051 (void);
// 0x0000039C System.Void System.Text.Json.JsonWriterHelper::ValidateValue(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateValue_m7896B25EB4023B71DBC98FDA97242CB0E3C458E1 (void);
// 0x0000039D System.Void System.Text.Json.JsonWriterHelper::ValidateBytes(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateBytes_mC728D6FE42DBCD544F12F98ABDD55B8E387B9CA7 (void);
// 0x0000039E System.Void System.Text.Json.JsonWriterHelper::ValidateDouble(System.Double)
extern void JsonWriterHelper_ValidateDouble_mDFD0BE277B2D44D79C8CEF25FD1FBB4CB9F10A15 (void);
// 0x0000039F System.Void System.Text.Json.JsonWriterHelper::ValidateSingle(System.Single)
extern void JsonWriterHelper_ValidateSingle_m0ADF667516A892AFEE48061137CF087ED5078616 (void);
// 0x000003A0 System.Void System.Text.Json.JsonWriterHelper::ValidateProperty(System.ReadOnlySpan`1<System.Char>)
extern void JsonWriterHelper_ValidateProperty_mFF2FA821AE187E6CA3F5DBB08A09CD4E9FAE473C (void);
// 0x000003A1 System.Void System.Text.Json.JsonWriterHelper::ValidateValue(System.ReadOnlySpan`1<System.Char>)
extern void JsonWriterHelper_ValidateValue_m362488F874F8A95B4B24112E27588CE76C71F696 (void);
// 0x000003A2 System.Void System.Text.Json.JsonWriterHelper::ValidateNumber(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateNumber_mBF35E6030A120C621778812860E76F76C5129963 (void);
// 0x000003A3 System.Void System.Text.Json.JsonWriterHelper::WriteDateTimeTrimmed(System.Span`1<System.Byte>,System.DateTime,System.Int32&)
extern void JsonWriterHelper_WriteDateTimeTrimmed_mEE9DD7C3360953F43562045A20C5B0F7A0EA1B17 (void);
// 0x000003A4 System.Void System.Text.Json.JsonWriterHelper::WriteDateTimeOffsetTrimmed(System.Span`1<System.Byte>,System.DateTimeOffset,System.Int32&)
extern void JsonWriterHelper_WriteDateTimeOffsetTrimmed_m51D0409624F026B505AD812107EB7554F06A2A1C (void);
// 0x000003A5 System.Void System.Text.Json.JsonWriterHelper::TrimDateTimeOffset(System.Span`1<System.Byte>,System.Int32&)
extern void JsonWriterHelper_TrimDateTimeOffset_m88440DBC7ED3C898144132A001DB53F6B5814AD4 (void);
// 0x000003A6 System.UInt32 System.Text.Json.JsonWriterHelper::DivMod(System.UInt32,System.UInt32,System.UInt32&)
extern void JsonWriterHelper_DivMod_m24077803A35C71313756020418637482134766AF (void);
// 0x000003A7 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonWriterHelper::get_AllowList()
extern void JsonWriterHelper_get_AllowList_m805D9C7A9D48EFCD489AB80BD12D54E54E2069D1 (void);
// 0x000003A8 System.Boolean System.Text.Json.JsonWriterHelper::NeedsEscaping(System.Byte)
extern void JsonWriterHelper_NeedsEscaping_mBC38FB4BB1F4A6E25F2290769848B4FC5AFA6443 (void);
// 0x000003A9 System.Boolean System.Text.Json.JsonWriterHelper::NeedsEscapingNoBoundsCheck(System.Char)
extern void JsonWriterHelper_NeedsEscapingNoBoundsCheck_m1726AD27336636DBA4D1ED35C6A7A35576DCF7C3 (void);
// 0x000003AA System.Int32 System.Text.Json.JsonWriterHelper::NeedsEscaping(System.ReadOnlySpan`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonWriterHelper_NeedsEscaping_m8D513359DF89D5CEAEE6B28549F7BE974BB69D40 (void);
// 0x000003AB System.Int32 System.Text.Json.JsonWriterHelper::NeedsEscaping(System.ReadOnlySpan`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonWriterHelper_NeedsEscaping_mD8D4E8A8B346E6F1E91580CA83FB13CD6AD30BEA (void);
// 0x000003AC System.Int32 System.Text.Json.JsonWriterHelper::GetMaxEscapedLength(System.Int32,System.Int32)
extern void JsonWriterHelper_GetMaxEscapedLength_m600D967D2D4A80595C90B496097F912720DBC0BD (void);
// 0x000003AD System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_m071B896FC9E70EE8CA0284772E1E56201D42539C (void);
// 0x000003AE System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_mEF28F3394651A92DC0635CBBA0BFC2E362CFEA30 (void);
// 0x000003AF System.Void System.Text.Json.JsonWriterHelper::EscapeNextBytes(System.Byte,System.Span`1<System.Byte>,System.Int32&)
extern void JsonWriterHelper_EscapeNextBytes_mDEB4D5C8088F6DBB31FCA5641B6B8F3B2C4B0FDE (void);
// 0x000003B0 System.Boolean System.Text.Json.JsonWriterHelper::IsAsciiValue(System.Byte)
extern void JsonWriterHelper_IsAsciiValue_m40ADB4FD3BB1E29EF8259F5B67BC2D38AC849870 (void);
// 0x000003B1 System.Boolean System.Text.Json.JsonWriterHelper::IsAsciiValue(System.Char)
extern void JsonWriterHelper_IsAsciiValue_m511C15A62875EC4BC5C65766F71AFAEAFFAF0494 (void);
// 0x000003B2 System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_mED258B0AC21DA8C964CE1FFC708B221995EF8280 (void);
// 0x000003B3 System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_m219882343D18E7485EE5DC95C846BE4E70517761 (void);
// 0x000003B4 System.Void System.Text.Json.JsonWriterHelper::EscapeNextChars(System.Char,System.Span`1<System.Char>,System.Int32&)
extern void JsonWriterHelper_EscapeNextChars_mD0D8A8A11108E9D13EC6F4C59D0525BD0330D95A (void);
// 0x000003B5 System.Int32 System.Text.Json.JsonWriterHelper::WriteHex(System.Int32,System.Span`1<System.Char>,System.Int32)
extern void JsonWriterHelper_WriteHex_m78946CB72326295D44767A496D976A25DA64FABE (void);
// 0x000003B6 System.Buffers.OperationStatus System.Text.Json.JsonWriterHelper::ToUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&)
extern void JsonWriterHelper_ToUtf8_m8C88E880A53E299AC0B145E7D9D373562357C62A (void);
// 0x000003B7 System.Int32 System.Text.Json.JsonWriterHelper::PtrDiff(System.Char*,System.Char*)
extern void JsonWriterHelper_PtrDiff_m27CDC21C09A8C4F0C531C275B2BE14632DDC94AE (void);
// 0x000003B8 System.Int32 System.Text.Json.JsonWriterHelper::PtrDiff(System.Byte*,System.Byte*)
extern void JsonWriterHelper_PtrDiff_mCA9266FECE40CB5EFF86CD3CBD17F301A86D2B4E (void);
// 0x000003B9 System.Void System.Text.Json.JsonWriterHelper::.cctor()
extern void JsonWriterHelper__cctor_mF433480DDBAFA69A6F1412D0CC547C4FB55FBC62 (void);
// 0x000003BA System.Text.Encodings.Web.JavaScriptEncoder System.Text.Json.JsonWriterOptions::get_Encoder()
extern void JsonWriterOptions_get_Encoder_m232389EA65BBFEC33F6133822B6014A12A3DD4FE (void);
// 0x000003BB System.Void System.Text.Json.JsonWriterOptions::set_Encoder(System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonWriterOptions_set_Encoder_mC29EDD6B2528E7E44A0E76471CC62E46FA114F00 (void);
// 0x000003BC System.Boolean System.Text.Json.JsonWriterOptions::get_Indented()
extern void JsonWriterOptions_get_Indented_m1A267394B53602FEC4043C51430CA74205BE2316 (void);
// 0x000003BD System.Void System.Text.Json.JsonWriterOptions::set_Indented(System.Boolean)
extern void JsonWriterOptions_set_Indented_mC3F43DEDCB5B57EEFDF253E8397F49F73DB43C91 (void);
// 0x000003BE System.Boolean System.Text.Json.JsonWriterOptions::get_SkipValidation()
extern void JsonWriterOptions_get_SkipValidation_mEEE3015DF8EADA12C2E39BC1684FB3A5D2850683 (void);
// 0x000003BF System.Void System.Text.Json.JsonWriterOptions::set_SkipValidation(System.Boolean)
extern void JsonWriterOptions_set_SkipValidation_m46DB044F25318B23C13DCFC763A9967C06CBA832 (void);
// 0x000003C0 System.Boolean System.Text.Json.JsonWriterOptions::get_IndentedOrNotSkipValidation()
extern void JsonWriterOptions_get_IndentedOrNotSkipValidation_m7F6599D6058B3C9C18EC99304B0B5E386CD84DD7 (void);
// 0x000003C1 System.Int32 System.Text.Json.Utf8JsonWriter::get_BytesPending()
extern void Utf8JsonWriter_get_BytesPending_m3972ECDA5A4E7DBC6A17F7AA0610CB20AB58EF2C (void);
// 0x000003C2 System.Void System.Text.Json.Utf8JsonWriter::set_BytesPending(System.Int32)
extern void Utf8JsonWriter_set_BytesPending_m21E09C8027465D1AD0C46E31DD91C8C83524DB10 (void);
// 0x000003C3 System.Int64 System.Text.Json.Utf8JsonWriter::get_BytesCommitted()
extern void Utf8JsonWriter_get_BytesCommitted_m4647460AF1A97E6805CF12393C978FE240CCB076 (void);
// 0x000003C4 System.Void System.Text.Json.Utf8JsonWriter::set_BytesCommitted(System.Int64)
extern void Utf8JsonWriter_set_BytesCommitted_m05CFA019A22B21A6E59B688CCD22B1C76C42E9B3 (void);
// 0x000003C5 System.Int32 System.Text.Json.Utf8JsonWriter::get_Indentation()
extern void Utf8JsonWriter_get_Indentation_m162E25CDEE92EEC5C82650530A018BA5C049A874 (void);
// 0x000003C6 System.Int32 System.Text.Json.Utf8JsonWriter::get_CurrentDepth()
extern void Utf8JsonWriter_get_CurrentDepth_m2770E27E9396C97DD4E4B20BFECAE60EF23E8417 (void);
// 0x000003C7 System.Void System.Text.Json.Utf8JsonWriter::.ctor(System.Buffers.IBufferWriter`1<System.Byte>,System.Text.Json.JsonWriterOptions)
extern void Utf8JsonWriter__ctor_m878AA70AA00DE7300C1749D15BF67D09B30B335C (void);
// 0x000003C8 System.Void System.Text.Json.Utf8JsonWriter::ResetHelper()
extern void Utf8JsonWriter_ResetHelper_m106A696B3EBF0175EF391F2DAEC5FA2F3DD5129A (void);
// 0x000003C9 System.Void System.Text.Json.Utf8JsonWriter::CheckNotDisposed()
extern void Utf8JsonWriter_CheckNotDisposed_m83F234E5126E68BAD92D6145BF87189C78E8FF33 (void);
// 0x000003CA System.Void System.Text.Json.Utf8JsonWriter::Flush()
extern void Utf8JsonWriter_Flush_mDF86E72B309640B3B8CD33D86089E313C78E60D8 (void);
// 0x000003CB System.Void System.Text.Json.Utf8JsonWriter::Dispose()
extern void Utf8JsonWriter_Dispose_mA686B611C38448D0567ADB7A2CDE41CA0D7273C4 (void);
// 0x000003CC System.Void System.Text.Json.Utf8JsonWriter::WriteStartArray()
extern void Utf8JsonWriter_WriteStartArray_m5AF69C1DC77BE068C92A9198EC8275D1FD13A9D3 (void);
// 0x000003CD System.Void System.Text.Json.Utf8JsonWriter::WriteStartObject()
extern void Utf8JsonWriter_WriteStartObject_m405F3F6425E7ECBDC32F3663421787FD9593A115 (void);
// 0x000003CE System.Void System.Text.Json.Utf8JsonWriter::WriteStart(System.Byte)
extern void Utf8JsonWriter_WriteStart_mB5CA5EA053FF7063C403EFFB317095D4B3D6FCE1 (void);
// 0x000003CF System.Void System.Text.Json.Utf8JsonWriter::WriteStartMinimized(System.Byte)
extern void Utf8JsonWriter_WriteStartMinimized_mF8D7F79CA85776D5041D59E908B9BC630AD26C71 (void);
// 0x000003D0 System.Void System.Text.Json.Utf8JsonWriter::WriteStartSlow(System.Byte)
extern void Utf8JsonWriter_WriteStartSlow_m9FF27EF7C53F3AD8EBBAD48E6DD6DDD16EFCE6B3 (void);
// 0x000003D1 System.Void System.Text.Json.Utf8JsonWriter::ValidateStart()
extern void Utf8JsonWriter_ValidateStart_mD2AF3E65F07B403D5E8D76A13DEB7D2132477966 (void);
// 0x000003D2 System.Void System.Text.Json.Utf8JsonWriter::WriteStartIndented(System.Byte)
extern void Utf8JsonWriter_WriteStartIndented_mB34D235939A0AB34C559DA572268048F7DE14DF5 (void);
// 0x000003D3 System.Void System.Text.Json.Utf8JsonWriter::WriteStartArray(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WriteStartArray_m818E99BFA0330F7A9850F26B1496F808720FDBB6 (void);
// 0x000003D4 System.Void System.Text.Json.Utf8JsonWriter::WriteStartHelper(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WriteStartHelper_mECFAAEF012DF4CAED3CF2DA51A6C8E7B770A93B0 (void);
// 0x000003D5 System.Void System.Text.Json.Utf8JsonWriter::WriteStartByOptions(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WriteStartByOptions_m43C300E0D8774384B11C92D4B0BA92F8109445F2 (void);
// 0x000003D6 System.Void System.Text.Json.Utf8JsonWriter::WriteEndArray()
extern void Utf8JsonWriter_WriteEndArray_mFA4F851879CFEA614A25B18A3A6EF9EFDA2E4454 (void);
// 0x000003D7 System.Void System.Text.Json.Utf8JsonWriter::WriteEndObject()
extern void Utf8JsonWriter_WriteEndObject_m370A04892C849299DB8B27866FC1F33359249E09 (void);
// 0x000003D8 System.Void System.Text.Json.Utf8JsonWriter::WriteEnd(System.Byte)
extern void Utf8JsonWriter_WriteEnd_mA795ED14708D59E3DC44D5E4E97E6663009F2D9F (void);
// 0x000003D9 System.Void System.Text.Json.Utf8JsonWriter::WriteEndMinimized(System.Byte)
extern void Utf8JsonWriter_WriteEndMinimized_m34C7375A11890A5FDAEF72A1647EE6BAAE8C561B (void);
// 0x000003DA System.Void System.Text.Json.Utf8JsonWriter::WriteEndSlow(System.Byte)
extern void Utf8JsonWriter_WriteEndSlow_m034429F9528A98972D804D7D7296C8C71885B8E2 (void);
// 0x000003DB System.Void System.Text.Json.Utf8JsonWriter::ValidateEnd(System.Byte)
extern void Utf8JsonWriter_ValidateEnd_mEF5170B7F687F80F287B2E444C89061A0479814B (void);
// 0x000003DC System.Void System.Text.Json.Utf8JsonWriter::WriteEndIndented(System.Byte)
extern void Utf8JsonWriter_WriteEndIndented_mEE5818F054A644AC0AC6702FAF93C66B9842567B (void);
// 0x000003DD System.Void System.Text.Json.Utf8JsonWriter::WriteNewLine(System.Span`1<System.Byte>)
extern void Utf8JsonWriter_WriteNewLine_m0EE49D5BBCC209D1E4B172C69FECB357DA48825D (void);
// 0x000003DE System.Void System.Text.Json.Utf8JsonWriter::UpdateBitStackOnStart(System.Byte)
extern void Utf8JsonWriter_UpdateBitStackOnStart_m4EBC052F260BA75D58908C90A1447C7693278703 (void);
// 0x000003DF System.Void System.Text.Json.Utf8JsonWriter::Grow(System.Int32)
extern void Utf8JsonWriter_Grow_m27BD8F9402D326DBEE3280EDDF6EAC9D79136872 (void);
// 0x000003E0 System.Void System.Text.Json.Utf8JsonWriter::FirstCallToGetMemory(System.Int32)
extern void Utf8JsonWriter_FirstCallToGetMemory_m4BC1A5C7F3D0E3DE6575E6F5FB189844488B7C75 (void);
// 0x000003E1 System.Void System.Text.Json.Utf8JsonWriter::SetFlagToAddListSeparatorBeforeNextItem()
extern void Utf8JsonWriter_SetFlagToAddListSeparatorBeforeNextItem_m81A0884A33153ACE7D9C64CA1EB8798A7AF98F26 (void);
// 0x000003E2 System.String System.Text.Json.Utf8JsonWriter::get_DebuggerDisplay()
extern void Utf8JsonWriter_get_DebuggerDisplay_m97377549BD621291AE6CDB2B4FBC2D7CB4EBDFC8 (void);
// 0x000003E3 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.DateTime)
extern void Utf8JsonWriter_WritePropertyName_m4FD0A8F47F96CF03A45092DF00F9857ADAF46A67 (void);
// 0x000003E4 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.DateTimeOffset)
extern void Utf8JsonWriter_WritePropertyName_m64DC1F81413E96F2DE3347ABF72D1B0A8D96921F (void);
// 0x000003E5 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Decimal)
extern void Utf8JsonWriter_WritePropertyName_m2956BB08421B6FDB88ACFBF0603E317CD66ED04E (void);
// 0x000003E6 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Double)
extern void Utf8JsonWriter_WritePropertyName_mB47FEE2DFC9D07ACD1F4EA4EC031E624A264D279 (void);
// 0x000003E7 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Single)
extern void Utf8JsonWriter_WritePropertyName_mD5A7873A26BE9F37ACBA5C81933D38EEF9907D08 (void);
// 0x000003E8 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Guid)
extern void Utf8JsonWriter_WritePropertyName_m7EBAE815A669E7BDF1B7ACB71264F3B9F8FA1EBB (void);
// 0x000003E9 System.Void System.Text.Json.Utf8JsonWriter::ValidateDepth()
extern void Utf8JsonWriter_ValidateDepth_m506A98E5E009906D3FEBA660D9E9601EF2234B98 (void);
// 0x000003EA System.Void System.Text.Json.Utf8JsonWriter::ValidateWritingProperty()
extern void Utf8JsonWriter_ValidateWritingProperty_mA019E18A4D74422B9E9CEB4272FCFE3D64BA3973 (void);
// 0x000003EB System.Void System.Text.Json.Utf8JsonWriter::ValidateWritingProperty(System.Byte)
extern void Utf8JsonWriter_ValidateWritingProperty_mF318177CA6DAF442AA1249CE17FE83904D998A4A (void);
// 0x000003EC System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameMinimized(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WritePropertyNameMinimized_mED43D25B962FD6C8711B7DB7CA72DED650DD275E (void);
// 0x000003ED System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameIndented(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WritePropertyNameIndented_m34B7AA96A926924464764C5C64B1C8718F43C0A5 (void);
// 0x000003EE System.Void System.Text.Json.Utf8JsonWriter::TranscodeAndWrite(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Byte>)
extern void Utf8JsonWriter_TranscodeAndWrite_m1514F44946C17B820849C1191F8E518A270D942C (void);
// 0x000003EF System.Void System.Text.Json.Utf8JsonWriter::WriteNull(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WriteNull_m855DBD5672B217200384C6359E8E6E5E292025EA (void);
// 0x000003F0 System.Void System.Text.Json.Utf8JsonWriter::WriteNullSection(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNullSection_m85198ECB18623D7AA9DD9BF3FE2100FDFAA87B41 (void);
// 0x000003F1 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralHelper(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralHelper_m6E48796A22D21CEBD4A6FE115C3AB6B5DDADAC3F (void);
// 0x000003F2 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralByOptions(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralByOptions_m4C6B106278263022E6117AF53CAD369259588E66 (void);
// 0x000003F3 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralMinimized(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralMinimized_m752FA36599815EBF0536EC40CAC9D4C94E4CCD5C (void);
// 0x000003F4 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralSection(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralSection_m00FD131C181B991BF090BF8D70628EBC490FA295 (void);
// 0x000003F5 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralIndented(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralIndented_mEBC56FDD2CBD9DB35A08A4D624622AE406BC05FF (void);
// 0x000003F6 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Boolean)
extern void Utf8JsonWriter_WritePropertyName_mB5FF7C5AAB68DFDA99500F7E603E476149531658 (void);
// 0x000003F7 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Int32)
extern void Utf8JsonWriter_WritePropertyName_mF7BDE7B7C942A7E4913489702A2718C97647E869 (void);
// 0x000003F8 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Int64)
extern void Utf8JsonWriter_WritePropertyName_mF405F3CF850CFFB3FAD1AF64C26E414883DE3075 (void);
// 0x000003F9 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WritePropertyName_mBB7BDDE39B79F06BA46A38B238500E634103EE40 (void);
// 0x000003FA System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameSection(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyNameSection_m32C1F6143B269D86A3918095097CD090A8127B17 (void);
// 0x000003FB System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameHelper(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyNameHelper_m66F7E72310AB89EC92B272FBFADD5CDD42C5CCAD (void);
// 0x000003FC System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.String)
extern void Utf8JsonWriter_WritePropertyName_mC9ED158A09A22E81D95630C164199CE7DF9A849E (void);
// 0x000003FD System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WritePropertyName_m0765F554DF826D559BC0CC78E484D924DD5105AB (void);
// 0x000003FE System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeProperty(System.ReadOnlySpan`1<System.Char>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeProperty_m080C5EA0547428273A28745A50EA5E237ACBBCF4 (void);
// 0x000003FF System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptionsPropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringByOptionsPropertyName_mA39463F14C036B2D86A0FEB6F2D608884A5AA9E7 (void);
// 0x00000400 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimizedPropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringMinimizedPropertyName_m39B609112B1A71879877B162EE9E6EF8734ED170 (void);
// 0x00000401 System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndentedPropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringIndentedPropertyName_mE7D313F37D232CF5625D9C409CDD66AEBBA6A355 (void);
// 0x00000402 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyName_m330CA7F278BFD74BA7314CCD560D263D09D91286 (void);
// 0x00000403 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameUnescaped(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyNameUnescaped_mAF05F3BBA02E80510DD8DEEB7852071502EE96AF (void);
// 0x00000404 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeProperty(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeProperty_m537908F234FF38878C7B1B039BE4AFC24DF0A7B6 (void);
// 0x00000405 System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptionsPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringByOptionsPropertyName_m666E6AEF3456971737062B4818459241ED338CC1 (void);
// 0x00000406 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimizedPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringMinimizedPropertyName_m0E87A70328BEE0E6695A1BCCC461E1F06692BA2F (void);
// 0x00000407 System.Void System.Text.Json.Utf8JsonWriter::WriteStringPropertyNameSection(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringPropertyNameSection_m3F659A854A68E9AC08E217672F35297FDF711ECC (void);
// 0x00000408 System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndentedPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringIndentedPropertyName_mEFF104DAB0E76363414FF5D34DA75D2F68F393B1 (void);
// 0x00000409 System.Void System.Text.Json.Utf8JsonWriter::WriteString(System.Text.Json.JsonEncodedText,System.String)
extern void Utf8JsonWriter_WriteString_mC4C4697F80008CC325DA268D181126F27A29F2C0 (void);
// 0x0000040A System.Void System.Text.Json.Utf8JsonWriter::WriteString(System.Text.Json.JsonEncodedText,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteString_mAD798D5F92A07D36BEE5DEDCA5369ABE2326DFD3 (void);
// 0x0000040B System.Void System.Text.Json.Utf8JsonWriter::WriteStringHelperEscapeValue(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringHelperEscapeValue_m3E958CDC4C2E3F9561C5F48367EBBF80FA113BCB (void);
// 0x0000040C System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeValueOnly(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeValueOnly_m11445B997CDDFB6185550719E632F3C244C3521C (void);
// 0x0000040D System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptions(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringByOptions_m45DC97F766159BFF9280DA9615A9638913C776B0 (void);
// 0x0000040E System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimized(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringMinimized_mD2A20FCD732FFE50292C2A5CA960317FF5DCC2CC (void);
// 0x0000040F System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndented(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringIndented_mBB1396532375449C755810749186021969AD99A7 (void);
// 0x00000410 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.UInt32)
extern void Utf8JsonWriter_WritePropertyName_m78F53A27211272470E5933ED12D0D5F16BFBBE74 (void);
// 0x00000411 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.UInt64)
extern void Utf8JsonWriter_WritePropertyName_mE07372EBF45AE7D561122888BEFB6A80CAE5A2C8 (void);
// 0x00000412 System.Void System.Text.Json.Utf8JsonWriter::WriteBase64StringValue(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64StringValue_m275CDD8B7A07EBEF48C50C9E104252F162EA1B37 (void);
// 0x00000413 System.Void System.Text.Json.Utf8JsonWriter::WriteBase64ByOptions(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64ByOptions_m29D7DA30FBA68D6442C0EF39E98DEC30243CB7A8 (void);
// 0x00000414 System.Void System.Text.Json.Utf8JsonWriter::WriteBase64Minimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64Minimized_m386B30BE0A80EA1E7ABA12C1D15B684C280E77F3 (void);
// 0x00000415 System.Void System.Text.Json.Utf8JsonWriter::WriteBase64Indented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64Indented_m9E83F9BD27AC2DC5FAC4AFDF637DECDBFE4082F1 (void);
// 0x00000416 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.DateTime)
extern void Utf8JsonWriter_WriteStringValue_m644B995EDDE02E8FFD4E20E5169BB4864C325FE3 (void);
// 0x00000417 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueMinimized(System.DateTime)
extern void Utf8JsonWriter_WriteStringValueMinimized_mAB05CF9C89552B4CFB794981F31D06724F67AC89 (void);
// 0x00000418 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueIndented(System.DateTime)
extern void Utf8JsonWriter_WriteStringValueIndented_mF8616ED47AC4EA002D35A45420DAF14225948F4F (void);
// 0x00000419 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.DateTimeOffset)
extern void Utf8JsonWriter_WriteStringValue_m10C15B14827394FC4E310F0A4483D2F469A3B092 (void);
// 0x0000041A System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueMinimized(System.DateTimeOffset)
extern void Utf8JsonWriter_WriteStringValueMinimized_m245EF5ABE4AD9955DC4690384D762AE71996FF18 (void);
// 0x0000041B System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueIndented(System.DateTimeOffset)
extern void Utf8JsonWriter_WriteStringValueIndented_m017F23DFC3BD7CF91E189D5BF59C322EB1C02EF8 (void);
// 0x0000041C System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValue_m4CCC908500FA7EA5FBE08752AC5407EEF872F1F4 (void);
// 0x0000041D System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValueMinimized_mBAB8C7A8ECC05F152A4A3265A7E39C22AC994C12 (void);
// 0x0000041E System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValueIndented_mEF81968ABF0B57912BA5D89D1D2B8CFC81230480 (void);
// 0x0000041F System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValueAsString_m16F29B30D227FABD286B348796BFAA23F3475490 (void);
// 0x00000420 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Double)
extern void Utf8JsonWriter_WriteNumberValue_m6038EC46F7253F8C59A795B46405F23227843B8D (void);
// 0x00000421 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Double)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m0090A828C8E20294AC6B937672BC6DB01B06EE1D (void);
// 0x00000422 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Double)
extern void Utf8JsonWriter_WriteNumberValueIndented_m51FCE2A59BF0B66AA06837D79674428F47C7B811 (void);
// 0x00000423 System.Boolean System.Text.Json.Utf8JsonWriter::TryFormatDouble(System.Double,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8JsonWriter_TryFormatDouble_m4ACD8F4466168BA50ED4CFB3277E3498FBC9AD75 (void);
// 0x00000424 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Double)
extern void Utf8JsonWriter_WriteNumberValueAsString_m248D41A780B766CA06A09D38C0E1C8D7800ED355 (void);
// 0x00000425 System.Void System.Text.Json.Utf8JsonWriter::WriteFloatingPointConstant(System.Double)
extern void Utf8JsonWriter_WriteFloatingPointConstant_m704AABCD749D18C16864B3916C9B19E8B6D1F72A (void);
// 0x00000426 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Single)
extern void Utf8JsonWriter_WriteNumberValue_m6D68ED28982B8659EDD4B0CE95910D06E32F4474 (void);
// 0x00000427 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Single)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m5A5486EA90A1CFDD96119204B63F9F535E6BDB1F (void);
// 0x00000428 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Single)
extern void Utf8JsonWriter_WriteNumberValueIndented_m3699BD007A78E03253A29E8ED7A081E50C569913 (void);
// 0x00000429 System.Boolean System.Text.Json.Utf8JsonWriter::TryFormatSingle(System.Single,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8JsonWriter_TryFormatSingle_m12FC465E743236B535EDF2B328AA1C7B872E9FAE (void);
// 0x0000042A System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Single)
extern void Utf8JsonWriter_WriteNumberValueAsString_m80AC2D04E5FA2ECC742BAB9989E560069B892821 (void);
// 0x0000042B System.Void System.Text.Json.Utf8JsonWriter::WriteFloatingPointConstant(System.Single)
extern void Utf8JsonWriter_WriteFloatingPointConstant_m6823815B694A404B1AF3A0D2D0A94E7B6F81EF81 (void);
// 0x0000042C System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValue_m7495DB5B019117D0D320F2679624B8AAE7D61E25 (void);
// 0x0000042D System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m5F2D7B1B8265E9622874C58CF7B89DD9EE9DE0A6 (void);
// 0x0000042E System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValueIndented_mB5E713CC39B1A739B1C9C9C6ACC7BF64929BD984 (void);
// 0x0000042F System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.Guid)
extern void Utf8JsonWriter_WriteStringValue_m0BCC1677B57A2864278016B527944F0CD93738DA (void);
// 0x00000430 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueMinimized(System.Guid)
extern void Utf8JsonWriter_WriteStringValueMinimized_m3E20D3F91904027475F1BE4B78DEC24BE9487C4D (void);
// 0x00000431 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueIndented(System.Guid)
extern void Utf8JsonWriter_WriteStringValueIndented_m90D3EA22AAA12C10A0ED689B9CBB9259515FE1EB (void);
// 0x00000432 System.Void System.Text.Json.Utf8JsonWriter::ValidateWritingValue()
extern void Utf8JsonWriter_ValidateWritingValue_mE1B9843F8C411945405F163550146191AE0FC800 (void);
// 0x00000433 System.Void System.Text.Json.Utf8JsonWriter::Base64EncodeAndWrite(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32)
extern void Utf8JsonWriter_Base64EncodeAndWrite_mFB3D8240F7A4DBCF593719FB3390BD867445D8EE (void);
// 0x00000434 System.Void System.Text.Json.Utf8JsonWriter::WriteNullValue()
extern void Utf8JsonWriter_WriteNullValue_mA3AF6DE23639AF39722BBD33D3BF20FF232A5BE1 (void);
// 0x00000435 System.Void System.Text.Json.Utf8JsonWriter::WriteBooleanValue(System.Boolean)
extern void Utf8JsonWriter_WriteBooleanValue_mA5C6F9B1E59F82EDDDA7D8F1470D582D01305596 (void);
// 0x00000436 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralByOptions(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralByOptions_mC4810114A406D30782EBF028DF9A7B8E3B424F90 (void);
// 0x00000437 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralMinimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralMinimized_m9248E07917AF98DD60C06C97FA9F7BC99F51E3C6 (void);
// 0x00000438 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralIndented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralIndented_m1CF383922DE0AC0D9F5C729C3DAE12F405101AB9 (void);
// 0x00000439 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Int32)
extern void Utf8JsonWriter_WriteNumberValue_mD81F32E5440076B04200EC09C81A6EED33BE9531 (void);
// 0x0000043A System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Int64)
extern void Utf8JsonWriter_WriteNumberValue_mD2BFA46B5B105F948CD9A96CD53943ADBAA2CBC5 (void);
// 0x0000043B System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Int64)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m98E4B5524CC79FEAA25A931403BAA9433479E7A8 (void);
// 0x0000043C System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Int64)
extern void Utf8JsonWriter_WriteNumberValueIndented_m065A2AD2237FCEAA69FA5AC355DD94F58AB29F35 (void);
// 0x0000043D System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Int64)
extern void Utf8JsonWriter_WriteNumberValueAsString_mCCA1F14F8E1D8DA7E02903B3825C1E866C5D00FC (void);
// 0x0000043E System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WriteStringValue_m31C559DBF8E79BC0ABED29C30728BBD421FBC60E (void);
// 0x0000043F System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.String)
extern void Utf8JsonWriter_WriteStringValue_m1271B865848F4BF39035ACB38CD54B914A3BF385 (void);
// 0x00000440 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringValue_m2686D29E6B439FA843932F3A349006BDE80A64BC (void);
// 0x00000441 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscape(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringEscape_mF785130598B4D5F033E9B34E903D06D6B0202B4E (void);
// 0x00000442 System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptions(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringByOptions_m64A45FC421092557F36B99B93BD7BC18F027EFEA (void);
// 0x00000443 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimized(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringMinimized_m864BE8CB2FCEA8976887BD5C3A15E1C7735ABE59 (void);
// 0x00000444 System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndented(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringIndented_m9942DCC20D3304DFF4B9BBA4B0C0B45BB25C88AD (void);
// 0x00000445 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeValue(System.ReadOnlySpan`1<System.Char>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeValue_mB819A602D08A40D4FF68EB03D88FB218E878CA23 (void);
// 0x00000446 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringValue_mA0AF3BD9AFD4DEB068CB076562F3D760C506DCDF (void);
// 0x00000447 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscape(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringEscape_mDCC0DD9B5548C5A6701D995EC4FDB6085102B2DA (void);
// 0x00000448 System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptions(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringByOptions_m75719263F0B2039C18C4C4811C7EDA377AFC89E3 (void);
// 0x00000449 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringMinimized_m8F06D2BC1C9839AD9CABD46F5EBE49249A854C52 (void);
// 0x0000044A System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringIndented_m54C699BC345AFDE60E17088AA62BA2B4CD7F0257 (void);
// 0x0000044B System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeValue(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeValue_m27FEFF0BFE40B2484EF0547BA26F25F2B11DCA96 (void);
// 0x0000044C System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsStringUnescaped(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValueAsStringUnescaped_m3F59FCCAAE8DA3B4605A0358A589AAB7DC6550A6 (void);
// 0x0000044D System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.UInt32)
extern void Utf8JsonWriter_WriteNumberValue_mF8CCB5E8AF7CB32821E48257DAEB9488540C95B5 (void);
// 0x0000044E System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValue_m14B1AC5C45AD6F1FB2A9F42FF28CEA2CF6D8CBCD (void);
// 0x0000044F System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m1F41036EE317C0A8CEE2DA9D6C072042BB219928 (void);
// 0x00000450 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValueIndented_mE474F2518F3ADA4C7F787FDEDBD2C153E5AC0650 (void);
// 0x00000451 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValueAsString_mE670846DCCD878B872DB6FD051E8EA6EB8CFED42 (void);
// 0x00000452 System.Void System.Text.Json.Utf8JsonWriter::.cctor()
extern void Utf8JsonWriter__cctor_mD41DD5176CEC82423AC711C304F37EDA3B8CAC1B (void);
// 0x00000453 System.Type System.Text.Json.Serialization.JsonConverterAttribute::get_ConverterType()
extern void JsonConverterAttribute_get_ConverterType_mBD3D8984B125937D81BEE770510CBD59DE992D6F (void);
// 0x00000454 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.JsonConverterAttribute::CreateConverter(System.Type)
extern void JsonConverterAttribute_CreateConverter_m0EF4444BF485369A2A5F9023E03EC8C124E0B34C (void);
// 0x00000455 System.Text.Json.Serialization.JsonIgnoreCondition System.Text.Json.Serialization.JsonIgnoreAttribute::get_Condition()
extern void JsonIgnoreAttribute_get_Condition_m5218388DF4B31B960A88F80CDB92BAD3A1575391 (void);
// 0x00000456 System.Text.Json.Serialization.JsonNumberHandling System.Text.Json.Serialization.JsonNumberHandlingAttribute::get_Handling()
extern void JsonNumberHandlingAttribute_get_Handling_m47EA30AE14CB014E642C09FC271D744207304236 (void);
// 0x00000457 System.String System.Text.Json.Serialization.JsonPropertyNameAttribute::get_Name()
extern void JsonPropertyNameAttribute_get_Name_m43379618C5E4C36982CD87DCC7DCD46C9AF7636A (void);
// 0x00000458 System.Void System.Text.Json.Serialization.ConverterList::.ctor(System.Text.Json.JsonSerializerOptions)
extern void ConverterList__ctor_m7956CA822F885BE13239190A0AAF958E03950869 (void);
// 0x00000459 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.ConverterList::get_Item(System.Int32)
extern void ConverterList_get_Item_mDFFAC649D8706D825E9B3412F06EA95E068000C0 (void);
// 0x0000045A System.Void System.Text.Json.Serialization.ConverterList::set_Item(System.Int32,System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_set_Item_m397EBDB7ACE75DADAA7194BB9B034EA046606128 (void);
// 0x0000045B System.Int32 System.Text.Json.Serialization.ConverterList::get_Count()
extern void ConverterList_get_Count_m6E0DE98A3850DB3A4A5EC9024DB63F0A739558AD (void);
// 0x0000045C System.Boolean System.Text.Json.Serialization.ConverterList::get_IsReadOnly()
extern void ConverterList_get_IsReadOnly_m87EE2CF9E124FF5D19488059BE6F215D894EDC05 (void);
// 0x0000045D System.Void System.Text.Json.Serialization.ConverterList::Add(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Add_m1F0C1BCBB85EBA89D586F4EEA8F87AB8ED30D7AA (void);
// 0x0000045E System.Void System.Text.Json.Serialization.ConverterList::Clear()
extern void ConverterList_Clear_m404C78E5D51713B9E923361FBD51ABA02E455C27 (void);
// 0x0000045F System.Boolean System.Text.Json.Serialization.ConverterList::Contains(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Contains_m6DAD9A6249FC6715E3E5FC97BCD8494266029865 (void);
// 0x00000460 System.Void System.Text.Json.Serialization.ConverterList::CopyTo(System.Text.Json.Serialization.JsonConverter[],System.Int32)
extern void ConverterList_CopyTo_m1B02A2AF35F337BD4BAC62D0C5135F25ACEA67C6 (void);
// 0x00000461 System.Collections.Generic.IEnumerator`1<System.Text.Json.Serialization.JsonConverter> System.Text.Json.Serialization.ConverterList::GetEnumerator()
extern void ConverterList_GetEnumerator_mA2395D76A6CD16AF83C873361220DE759E715DC3 (void);
// 0x00000462 System.Int32 System.Text.Json.Serialization.ConverterList::IndexOf(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_IndexOf_m2A289B4A827C0B2E0FB3DDD111048D149D22EDEF (void);
// 0x00000463 System.Void System.Text.Json.Serialization.ConverterList::Insert(System.Int32,System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Insert_mE4AAC6CD09995A35BC0A5676B5278B3E7D3A23A0 (void);
// 0x00000464 System.Boolean System.Text.Json.Serialization.ConverterList::Remove(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Remove_m96AFCD48E6D8CA3531441A4C867C482BABD3B5E6 (void);
// 0x00000465 System.Void System.Text.Json.Serialization.ConverterList::RemoveAt(System.Int32)
extern void ConverterList_RemoveAt_m59B14B97F0484125A311BE119694C8264AC11943 (void);
// 0x00000466 System.Collections.IEnumerator System.Text.Json.Serialization.ConverterList::System.Collections.IEnumerable.GetEnumerator()
extern void ConverterList_System_Collections_IEnumerable_GetEnumerator_mC89C567B32F6ED8242857A0F3AA329BD7112F134 (void);
// 0x00000467 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetCompatibleGenericBaseClass(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetCompatibleGenericBaseClass_mFE8785F58EDD007ECCB3D76077B0565BEE8DAA2B (void);
// 0x00000468 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetCompatibleGenericInterface(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetCompatibleGenericInterface_mB640DD8282CAC982B0360A4B1F5DB36603201157 (void);
// 0x00000469 System.Boolean System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::IsImmutableDictionaryType(System.Type)
extern void IEnumerableConverterFactoryHelpers_IsImmutableDictionaryType_m35307B7347E0B7A698DA5E80E439A484D063DA42 (void);
// 0x0000046A System.Boolean System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::IsImmutableEnumerableType(System.Type)
extern void IEnumerableConverterFactoryHelpers_IsImmutableEnumerableType_m7718A49E6C83EB1AE4F16348E3FC3CDBB61A1EF9 (void);
// 0x0000046B System.Reflection.MethodInfo System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableEnumerableCreateRangeMethod(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableEnumerableCreateRangeMethod_m750E671FCD270FA0B1B2E58C2AFDF41843605706 (void);
// 0x0000046C System.Reflection.MethodInfo System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableDictionaryCreateRangeMethod(System.Type,System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableDictionaryCreateRangeMethod_mCAF50FB9DCCB97B1BEE3EAE5E50A55427D6EC02F (void);
// 0x0000046D System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableEnumerableConstructingType(System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableEnumerableConstructingType_mE18B88FF0D674788A765CCE271E806C39124E35E (void);
// 0x0000046E System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableDictionaryConstructingType(System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableDictionaryConstructingType_m3A729A5F1A962AD48029801F0BF302807939C6CD (void);
// 0x0000046F System.Boolean System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::IsNonGenericStackOrQueue(System.Type)
extern void IEnumerableConverterFactoryHelpers_IsNonGenericStackOrQueue_m4E32775D53BA32FD8CBA158643133A7F924BCDE9 (void);
// 0x00000470 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetTypeIfExists(System.String)
extern void IEnumerableConverterFactoryHelpers_GetTypeIfExists_mFF636FA2D1E096D31C8BEF8AB7892437A824CDBF (void);
// 0x00000471 System.Text.Json.ClassType System.Text.Json.Serialization.JsonCollectionConverter`2::get_ClassType()
// 0x00000472 System.Type System.Text.Json.Serialization.JsonCollectionConverter`2::get_ElementType()
// 0x00000473 System.Void System.Text.Json.Serialization.JsonCollectionConverter`2::.ctor()
// 0x00000474 System.Text.Json.ClassType System.Text.Json.Serialization.JsonDictionaryConverter`1::get_ClassType()
// 0x00000475 System.Boolean System.Text.Json.Serialization.JsonDictionaryConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000476 System.Void System.Text.Json.Serialization.JsonDictionaryConverter`1::.ctor()
// 0x00000477 System.Text.Json.ClassType System.Text.Json.Serialization.JsonObjectConverter`1::get_ClassType()
// 0x00000478 System.Type System.Text.Json.Serialization.JsonObjectConverter`1::get_ElementType()
// 0x00000479 System.Void System.Text.Json.Serialization.JsonObjectConverter`1::.ctor()
// 0x0000047A System.Void System.Text.Json.Serialization.JsonConverter::.ctor()
extern void JsonConverter__ctor_mD9817852B6266F19DD517CB9CD6054042B59BDD7 (void);
// 0x0000047B System.Boolean System.Text.Json.Serialization.JsonConverter::CanConvert(System.Type)
// 0x0000047C System.Text.Json.ClassType System.Text.Json.Serialization.JsonConverter::get_ClassType()
// 0x0000047D System.Boolean System.Text.Json.Serialization.JsonConverter::get_CanUseDirectReadOrWrite()
extern void JsonConverter_get_CanUseDirectReadOrWrite_mE2CF0ECA1CCB2379ADDB5CA9927657FAE9420C97 (void);
// 0x0000047E System.Void System.Text.Json.Serialization.JsonConverter::set_CanUseDirectReadOrWrite(System.Boolean)
extern void JsonConverter_set_CanUseDirectReadOrWrite_m13DC84941788179AF7B397950B55551825EE1A46 (void);
// 0x0000047F System.Boolean System.Text.Json.Serialization.JsonConverter::get_CanHaveIdMetadata()
extern void JsonConverter_get_CanHaveIdMetadata_mD91E3E32D02A833D96462A328F712EE6EF2FA33C (void);
// 0x00000480 System.Boolean System.Text.Json.Serialization.JsonConverter::get_CanBePolymorphic()
extern void JsonConverter_get_CanBePolymorphic_mBA6DE039DABAEDEDDBBA10D5EF2D6535744EE450 (void);
// 0x00000481 System.Void System.Text.Json.Serialization.JsonConverter::set_CanBePolymorphic(System.Boolean)
extern void JsonConverter_set_CanBePolymorphic_m8CD8BA9054E3885A732BEFDF45DEC79A80145A7F (void);
// 0x00000482 System.Text.Json.JsonPropertyInfo System.Text.Json.Serialization.JsonConverter::CreateJsonPropertyInfo()
// 0x00000483 System.Text.Json.JsonParameterInfo System.Text.Json.Serialization.JsonConverter::CreateJsonParameterInfo()
// 0x00000484 System.Type System.Text.Json.Serialization.JsonConverter::get_ElementType()
// 0x00000485 System.Boolean System.Text.Json.Serialization.JsonConverter::get_IsValueType()
extern void JsonConverter_get_IsValueType_mB362FB9B424A6911D4F4F54CE6E0FCB142D4895E (void);
// 0x00000486 System.Void System.Text.Json.Serialization.JsonConverter::set_IsValueType(System.Boolean)
extern void JsonConverter_set_IsValueType_mA4651543A67E50CC092948CEA8F29BE5B23DC911 (void);
// 0x00000487 System.Boolean System.Text.Json.Serialization.JsonConverter::get_IsInternalConverter()
extern void JsonConverter_get_IsInternalConverter_m0FC7605392703BA872B4DD89C327766EFF2C79A6 (void);
// 0x00000488 System.Void System.Text.Json.Serialization.JsonConverter::set_IsInternalConverter(System.Boolean)
extern void JsonConverter_set_IsInternalConverter_mF87A3D79885CFC9C8D8BD08BAF230EF7DEE9AF40 (void);
// 0x00000489 System.Object System.Text.Json.Serialization.JsonConverter::ReadCoreAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000048A System.Type System.Text.Json.Serialization.JsonConverter::get_RuntimeType()
extern void JsonConverter_get_RuntimeType_m9B6CC6CFEA51A1B69F633305552039289B1BA9A7 (void);
// 0x0000048B System.Boolean System.Text.Json.Serialization.JsonConverter::ShouldFlush(System.Text.Json.Utf8JsonWriter,System.Text.Json.WriteStack&)
extern void JsonConverter_ShouldFlush_mAA4522431653C4C34E9301B1F8417189EFA0476C (void);
// 0x0000048C System.Type System.Text.Json.Serialization.JsonConverter::get_TypeToConvert()
// 0x0000048D System.Boolean System.Text.Json.Serialization.JsonConverter::TryReadAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,System.Object&)
// 0x0000048E System.Boolean System.Text.Json.Serialization.JsonConverter::TryWriteAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000048F System.Boolean System.Text.Json.Serialization.JsonConverter::WriteCoreAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000490 System.Void System.Text.Json.Serialization.JsonConverter::WriteWithQuotesAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000491 System.Boolean System.Text.Json.Serialization.JsonConverter::get_ConstructorIsParameterized()
extern void JsonConverter_get_ConstructorIsParameterized_m120E1195BC89B5743A895922978D983F3A4E92AF (void);
// 0x00000492 System.Reflection.ConstructorInfo System.Text.Json.Serialization.JsonConverter::get_ConstructorInfo()
extern void JsonConverter_get_ConstructorInfo_mE3B569C452D11FED23B56BF8DFF557811B830959 (void);
// 0x00000493 System.Void System.Text.Json.Serialization.JsonConverter::set_ConstructorInfo(System.Reflection.ConstructorInfo)
extern void JsonConverter_set_ConstructorInfo_mA1D9AA73D0C0C45A774EF06EA013731BC58B698C (void);
// 0x00000494 System.Void System.Text.Json.Serialization.JsonConverter::Initialize(System.Text.Json.JsonSerializerOptions)
extern void JsonConverter_Initialize_mC07941DBDF2999E50EB5B6BB36DE9DFCC393EBF2 (void);
// 0x00000495 System.Void System.Text.Json.Serialization.JsonConverter::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
extern void JsonConverter_CreateInstanceForReferenceResolver_m0ED09AE2DC04B1B4A1C551E37C82FD8928FB8C03 (void);
// 0x00000496 System.Boolean System.Text.Json.Serialization.JsonConverter::SingleValueReadWithReadAhead(System.Text.Json.ClassType,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void JsonConverter_SingleValueReadWithReadAhead_m5D43CECB31D8AF4F26804EB08A50133B8EEEE37B (void);
// 0x00000497 System.Boolean System.Text.Json.Serialization.JsonConverter::DoSingleValueReadWithReadAhead(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void JsonConverter_DoSingleValueReadWithReadAhead_m6F9F19FE6055B6F43F791EB59FD7442698088206 (void);
// 0x00000498 System.Void System.Text.Json.Serialization.JsonConverterFactory::.ctor()
extern void JsonConverterFactory__ctor_m388B7D545BCCB9998C7AAB01597321C355B6F340 (void);
// 0x00000499 System.Text.Json.ClassType System.Text.Json.Serialization.JsonConverterFactory::get_ClassType()
extern void JsonConverterFactory_get_ClassType_m82994907E2B8A2D15B8EA517384E02024344E20A (void);
// 0x0000049A System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.JsonConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
// 0x0000049B System.Text.Json.JsonPropertyInfo System.Text.Json.Serialization.JsonConverterFactory::CreateJsonPropertyInfo()
extern void JsonConverterFactory_CreateJsonPropertyInfo_m31164552EFB0E7ABFEADAD4A540061153D3D7E8B (void);
// 0x0000049C System.Text.Json.JsonParameterInfo System.Text.Json.Serialization.JsonConverterFactory::CreateJsonParameterInfo()
extern void JsonConverterFactory_CreateJsonParameterInfo_m63FA03387B0E2A0B53A7198EC281CB7716FF3BFA (void);
// 0x0000049D System.Type System.Text.Json.Serialization.JsonConverterFactory::get_ElementType()
extern void JsonConverterFactory_get_ElementType_m40B893CA28F4E2A2ECF370CE7384C314AB3C6202 (void);
// 0x0000049E System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.JsonConverterFactory::GetConverterInternal(System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonConverterFactory_GetConverterInternal_m8DC679FAC00808EE572E83710DE7FAD0890FEAD7 (void);
// 0x0000049F System.Object System.Text.Json.Serialization.JsonConverterFactory::ReadCoreAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
extern void JsonConverterFactory_ReadCoreAsObject_mF5A35DF3E689CF3BA748E0160C93B63D99CF2F5C (void);
// 0x000004A0 System.Boolean System.Text.Json.Serialization.JsonConverterFactory::TryReadAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,System.Object&)
extern void JsonConverterFactory_TryReadAsObject_m9579A04077C8AD890E4422A5B9D5E68421A55801 (void);
// 0x000004A1 System.Boolean System.Text.Json.Serialization.JsonConverterFactory::TryWriteAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void JsonConverterFactory_TryWriteAsObject_mF4545EEF2AC4AE4F0890ACF63BEFD60F9552E5D4 (void);
// 0x000004A2 System.Type System.Text.Json.Serialization.JsonConverterFactory::get_TypeToConvert()
extern void JsonConverterFactory_get_TypeToConvert_mB079B427354ACD22DE952F8A96BA9869521940F6 (void);
// 0x000004A3 System.Boolean System.Text.Json.Serialization.JsonConverterFactory::WriteCoreAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void JsonConverterFactory_WriteCoreAsObject_mCEBEFDEB14DD8FB62DECC01CBDF6CD13CA24D3E8 (void);
// 0x000004A4 System.Void System.Text.Json.Serialization.JsonConverterFactory::WriteWithQuotesAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void JsonConverterFactory_WriteWithQuotesAsObject_mE29CE18197B67C412372802CE64CFD719C48A064 (void);
// 0x000004A5 System.Object System.Text.Json.Serialization.JsonConverter`1::ReadCoreAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x000004A6 T System.Text.Json.Serialization.JsonConverter`1::ReadCore(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x000004A7 System.Boolean System.Text.Json.Serialization.JsonConverter`1::WriteCoreAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004A8 System.Boolean System.Text.Json.Serialization.JsonConverter`1::WriteCore(System.Text.Json.Utf8JsonWriter,T&,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004A9 System.Void System.Text.Json.Serialization.JsonConverter`1::.ctor()
// 0x000004AA System.Boolean System.Text.Json.Serialization.JsonConverter`1::CanConvert(System.Type)
// 0x000004AB System.Text.Json.ClassType System.Text.Json.Serialization.JsonConverter`1::get_ClassType()
// 0x000004AC System.Text.Json.JsonPropertyInfo System.Text.Json.Serialization.JsonConverter`1::CreateJsonPropertyInfo()
// 0x000004AD System.Text.Json.JsonParameterInfo System.Text.Json.Serialization.JsonConverter`1::CreateJsonParameterInfo()
// 0x000004AE System.Type System.Text.Json.Serialization.JsonConverter`1::get_ElementType()
// 0x000004AF System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_HandleNull()
// 0x000004B0 System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_HandleNullOnRead()
// 0x000004B1 System.Void System.Text.Json.Serialization.JsonConverter`1::set_HandleNullOnRead(System.Boolean)
// 0x000004B2 System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_HandleNullOnWrite()
// 0x000004B3 System.Void System.Text.Json.Serialization.JsonConverter`1::set_HandleNullOnWrite(System.Boolean)
// 0x000004B4 System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_CanBeNull()
// 0x000004B5 System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryWriteAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004B6 System.Boolean System.Text.Json.Serialization.JsonConverter`1::OnTryWrite(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004B7 System.Boolean System.Text.Json.Serialization.JsonConverter`1::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x000004B8 T System.Text.Json.Serialization.JsonConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000004B9 System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x000004BA System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryReadAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,System.Object&)
// 0x000004BB System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryWrite(System.Text.Json.Utf8JsonWriter,T&,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004BC System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryWriteDataExtensionProperty(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004BD System.Type System.Text.Json.Serialization.JsonConverter`1::get_TypeToConvert()
// 0x000004BE System.Void System.Text.Json.Serialization.JsonConverter`1::VerifyRead(System.Text.Json.JsonTokenType,System.Int32,System.Int64,System.Boolean,System.Text.Json.Utf8JsonReader&)
// 0x000004BF System.Void System.Text.Json.Serialization.JsonConverter`1::VerifyWrite(System.Int32,System.Text.Json.Utf8JsonWriter)
// 0x000004C0 System.Void System.Text.Json.Serialization.JsonConverter`1::Write(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions)
// 0x000004C1 T System.Text.Json.Serialization.JsonConverter`1::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
// 0x000004C2 System.Void System.Text.Json.Serialization.JsonConverter`1::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004C3 System.Void System.Text.Json.Serialization.JsonConverter`1::WriteWithQuotesAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004C4 T System.Text.Json.Serialization.JsonConverter`1::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000004C5 System.Void System.Text.Json.Serialization.JsonConverter`1::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000004C6 T System.Text.Json.Serialization.JsonResumableConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000004C7 System.Void System.Text.Json.Serialization.JsonResumableConverter`1::Write(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions)
// 0x000004C8 System.Boolean System.Text.Json.Serialization.JsonResumableConverter`1::get_HandleNull()
// 0x000004C9 System.Void System.Text.Json.Serialization.JsonResumableConverter`1::.ctor()
// 0x000004CA System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.PreserveReferenceHandler::CreateResolver()
extern void PreserveReferenceHandler_CreateResolver_mB7A12856B97F61A222CD08C3570550E76785C8FE (void);
// 0x000004CB System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.PreserveReferenceHandler::CreateResolver(System.Boolean)
extern void PreserveReferenceHandler_CreateResolver_m573822DCBEBAFC158B150EC9ADE8697C607799EC (void);
// 0x000004CC System.Void System.Text.Json.Serialization.PreserveReferenceHandler::.ctor()
extern void PreserveReferenceHandler__ctor_mEDCBCAB598908AC79A7DD0E5EB07087B968B51D1 (void);
// 0x000004CD System.Void System.Text.Json.Serialization.PreserveReferenceResolver::.ctor(System.Boolean)
extern void PreserveReferenceResolver__ctor_mAEB2BBB86124DD7F3A1B4555049D6A0EDDE5CDD7 (void);
// 0x000004CE System.Void System.Text.Json.Serialization.PreserveReferenceResolver::AddReference(System.String,System.Object)
extern void PreserveReferenceResolver_AddReference_m779013105D39EBA6265F758B239099C5A1E17BF9 (void);
// 0x000004CF System.String System.Text.Json.Serialization.PreserveReferenceResolver::GetReference(System.Object,System.Boolean&)
extern void PreserveReferenceResolver_GetReference_m9B3567D39A1CAD134864F4485AE3946DD8BC52B0 (void);
// 0x000004D0 System.Object System.Text.Json.Serialization.PreserveReferenceResolver::ResolveReference(System.String)
extern void PreserveReferenceResolver_ResolveReference_mD115CAE3824CED5C2472442BC1ECD80C818F0621 (void);
// 0x000004D1 System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.ReferenceHandler::CreateResolver()
// 0x000004D2 System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.ReferenceHandler::CreateResolver(System.Boolean)
extern void ReferenceHandler_CreateResolver_mCD769ED59A046E2C51A0788EC628284BE37F7EA9 (void);
// 0x000004D3 System.Void System.Text.Json.Serialization.ReferenceHandler::.ctor()
extern void ReferenceHandler__ctor_mAEE21A2298791B2C655BF05084A4A742FD6256E4 (void);
// 0x000004D4 System.Void System.Text.Json.Serialization.ReferenceHandler::.cctor()
extern void ReferenceHandler__cctor_m48CB714B1431FB031A19E150013244FF8A7B09B1 (void);
// 0x000004D5 System.Void System.Text.Json.Serialization.ReferenceResolver::AddReference(System.String,System.Object)
// 0x000004D6 System.String System.Text.Json.Serialization.ReferenceResolver::GetReference(System.Object,System.Boolean&)
// 0x000004D7 System.Object System.Text.Json.Serialization.ReferenceResolver::ResolveReference(System.String)
// 0x000004D8 System.Void System.Text.Json.Serialization.ReferenceResolver::.ctor()
extern void ReferenceResolver__ctor_m656741FB196E1C669DB1138F1B094190D8AD95A1 (void);
// 0x000004D9 System.Text.Json.JsonClassInfo/ConstructorDelegate System.Text.Json.Serialization.ReflectionMemberAccessor::CreateConstructor(System.Type)
extern void ReflectionMemberAccessor_CreateConstructor_m8533912016C1643578B568F78EDFDE578FB852BD (void);
// 0x000004DA System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1<T> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x000004DB System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5<T,TArg0,TArg1,TArg2,TArg3> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x000004DC System.Action`2<TCollection,System.Object> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateAddMethodDelegate()
// 0x000004DD System.Func`2<System.Collections.Generic.IEnumerable`1<TElement>,TCollection> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateImmutableEnumerableCreateRangeDelegate()
// 0x000004DE System.Func`2<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>>,TCollection> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateImmutableDictionaryCreateRangeDelegate()
// 0x000004DF System.Func`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionMemberAccessor::CreatePropertyGetter(System.Reflection.PropertyInfo)
// 0x000004E0 System.Action`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionMemberAccessor::CreatePropertySetter(System.Reflection.PropertyInfo)
// 0x000004E1 System.Func`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateFieldGetter(System.Reflection.FieldInfo)
// 0x000004E2 System.Action`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionMemberAccessor::CreateFieldSetter(System.Reflection.FieldInfo)
// 0x000004E3 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor::.ctor()
extern void ReflectionMemberAccessor__ctor_m95203B14A7542A66CDBCA345EAB98B69D2A291FC (void);
// 0x000004E4 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m1949513CDEB192A289C6C414E182FEC9DBB766EC (void);
// 0x000004E5 System.Object System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass0_0::<CreateConstructor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CCreateConstructorU3Eb__0_mB23814DC9A2E875C3CDE33E6C86AA756C30B3B6D (void);
// 0x000004E6 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass1_0`1::.ctor()
// 0x000004E7 T System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass1_0`1::<CreateParameterizedConstructor>b__0(System.Object[])
// 0x000004E8 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass2_0`5::.ctor()
// 0x000004E9 T System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass2_0`5::<CreateParameterizedConstructor>b__0(TArg0,TArg1,TArg2,TArg3)
// 0x000004EA System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass3_0`1::.ctor()
// 0x000004EB System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass3_0`1::<CreateAddMethodDelegate>b__0(TCollection,System.Object)
// 0x000004EC System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass6_0`1::.ctor()
// 0x000004ED TProperty System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass6_0`1::<CreatePropertyGetter>b__0(System.Object)
// 0x000004EE System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass7_0`1::.ctor()
// 0x000004EF System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass7_0`1::<CreatePropertySetter>b__0(System.Object,TProperty)
// 0x000004F0 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass8_0`1::.ctor()
// 0x000004F1 TProperty System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass8_0`1::<CreateFieldGetter>b__0(System.Object)
// 0x000004F2 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass9_0`1::.ctor()
// 0x000004F3 System.Void System.Text.Json.Serialization.ReflectionMemberAccessor/<>c__DisplayClass9_0`1::<CreateFieldSetter>b__0(System.Object,TProperty)
// 0x000004F4 System.Boolean System.Text.Json.Serialization.Converters.ArrayConverter`2::get_CanHaveIdMetadata()
// 0x000004F5 System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x000004F6 System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004F7 System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004F8 System.Boolean System.Text.Json.Serialization.Converters.ArrayConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004F9 System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::.ctor()
// 0x000004FA System.Void System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x000004FB System.Void System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004FC System.Boolean System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004FD System.Void System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::.ctor()
// 0x000004FE System.Void System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x000004FF System.Void System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000500 System.Boolean System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000501 System.Void System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::.ctor()
// 0x00000502 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000503 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000504 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x00000505 System.Type System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::get_ElementType()
// 0x00000506 System.Text.Json.Serialization.JsonConverter`1<TValue> System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::GetValueConverter(System.Text.Json.JsonClassInfo)
// 0x00000507 System.Text.Json.Serialization.JsonConverter`1<TKey> System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::GetKeyConverter(System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000508 System.Boolean System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,TCollection&)
// 0x00000509 System.Boolean System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::OnTryWrite(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000050A System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000050B System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::.ctor()
// 0x0000050C System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::.cctor()
// 0x0000050D TKey System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::<OnTryRead>g__ReadDictionaryKey|12_0(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3/<>c__DisplayClass12_0<TCollection,TKey,TValue>&)
// 0x0000050E System.Void System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000050F System.Void System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x00000510 System.Boolean System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000511 System.Void System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::.ctor()
// 0x00000512 System.Void System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000513 System.Void System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000514 System.Boolean System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000515 System.Type System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::get_RuntimeType()
// 0x00000516 System.Void System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::.ctor()
// 0x00000517 System.Void System.Text.Json.Serialization.Converters.IDictionaryConverter`1::Add(System.String,System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000518 System.Text.Json.Serialization.JsonConverter`1<System.Object> System.Text.Json.Serialization.Converters.IDictionaryConverter`1::GetObjectKeyConverter(System.Text.Json.JsonSerializerOptions)
// 0x00000519 System.Void System.Text.Json.Serialization.Converters.IDictionaryConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x0000051A System.Boolean System.Text.Json.Serialization.Converters.IDictionaryConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000051B System.Type System.Text.Json.Serialization.Converters.IDictionaryConverter`1::get_RuntimeType()
// 0x0000051C System.Void System.Text.Json.Serialization.Converters.IDictionaryConverter`1::.ctor()
// 0x0000051D System.Void System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000051E System.Void System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x0000051F System.Boolean System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000520 System.Type System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::get_RuntimeType()
// 0x00000521 System.Void System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::.ctor()
// 0x00000522 System.Void System.Text.Json.Serialization.Converters.IEnumerableConverter`1::Add(System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000523 System.Void System.Text.Json.Serialization.Converters.IEnumerableConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000524 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000525 System.Type System.Text.Json.Serialization.Converters.IEnumerableConverter`1::get_RuntimeType()
// 0x00000526 System.Void System.Text.Json.Serialization.Converters.IEnumerableConverter`1::.ctor()
// 0x00000527 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::CanConvert(System.Type)
extern void IEnumerableConverterFactory_CanConvert_m1A21C7F5751C29F1F8CB3DA6DA2A9E7091508D28 (void);
// 0x00000528 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void IEnumerableConverterFactory_CreateConverter_m7D6677A63ACEEF0B2F4D1C1E5A4BA57CD9681C3B (void);
// 0x00000529 System.Void System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::.ctor()
extern void IEnumerableConverterFactory__ctor_m1F85BD2E8D11D236DD476600E062A22BF071A831 (void);
// 0x0000052A System.Void System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::.cctor()
extern void IEnumerableConverterFactory__cctor_m3E2A6931F9CD31444F66237248382D8FC4BB5EB3 (void);
// 0x0000052B System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000052C System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000052D System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000052E System.Text.Json.Serialization.JsonConverter`1<TElement> System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::GetElementConverter(System.Text.Json.JsonClassInfo)
// 0x0000052F System.Text.Json.Serialization.JsonConverter`1<TElement> System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::GetElementConverter(System.Text.Json.WriteStack&)
// 0x00000530 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,TCollection&)
// 0x00000531 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::OnTryWrite(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000532 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000533 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000534 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::.ctor()
// 0x00000535 System.Void System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000536 System.Void System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000537 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000538 System.Type System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::get_RuntimeType()
// 0x00000539 System.Void System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::.ctor()
// 0x0000053A System.Void System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::Add(System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000053B System.Void System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000053C System.Boolean System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000053D System.Void System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::.ctor()
// 0x0000053E System.Void System.Text.Json.Serialization.Converters.IListConverter`1::Add(System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000053F System.Void System.Text.Json.Serialization.Converters.IListConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000540 System.Boolean System.Text.Json.Serialization.Converters.IListConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000541 System.Type System.Text.Json.Serialization.Converters.IListConverter`1::get_RuntimeType()
// 0x00000542 System.Void System.Text.Json.Serialization.Converters.IListConverter`1::.ctor()
// 0x00000543 System.Void System.Text.Json.Serialization.Converters.IListOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000544 System.Void System.Text.Json.Serialization.Converters.IListOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000545 System.Boolean System.Text.Json.Serialization.Converters.IListOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000546 System.Type System.Text.Json.Serialization.Converters.IListOfTConverter`2::get_RuntimeType()
// 0x00000547 System.Void System.Text.Json.Serialization.Converters.IListOfTConverter`2::.ctor()
// 0x00000548 System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000549 System.Boolean System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::get_CanHaveIdMetadata()
// 0x0000054A System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x0000054B System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000054C System.Boolean System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000054D System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::.ctor()
// 0x0000054E System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000054F System.Boolean System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::get_CanHaveIdMetadata()
// 0x00000550 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000551 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000552 System.Boolean System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000553 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::.ctor()
// 0x00000554 System.Void System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000555 System.Void System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x00000556 System.Boolean System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000557 System.Type System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::get_RuntimeType()
// 0x00000558 System.Void System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::.ctor()
// 0x00000559 System.Void System.Text.Json.Serialization.Converters.ISetOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000055A System.Void System.Text.Json.Serialization.Converters.ISetOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000055B System.Boolean System.Text.Json.Serialization.Converters.ISetOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000055C System.Type System.Text.Json.Serialization.Converters.ISetOfTConverter`2::get_RuntimeType()
// 0x0000055D System.Void System.Text.Json.Serialization.Converters.ISetOfTConverter`2::.ctor()
// 0x0000055E System.Void System.Text.Json.Serialization.Converters.ListOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000055F System.Void System.Text.Json.Serialization.Converters.ListOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000560 System.Boolean System.Text.Json.Serialization.Converters.ListOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000561 System.Void System.Text.Json.Serialization.Converters.ListOfTConverter`2::.ctor()
// 0x00000562 System.Void System.Text.Json.Serialization.Converters.QueueOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000563 System.Void System.Text.Json.Serialization.Converters.QueueOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000564 System.Boolean System.Text.Json.Serialization.Converters.QueueOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000565 System.Void System.Text.Json.Serialization.Converters.QueueOfTConverter`2::.ctor()
// 0x00000566 System.Void System.Text.Json.Serialization.Converters.StackOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000567 System.Void System.Text.Json.Serialization.Converters.StackOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000568 System.Boolean System.Text.Json.Serialization.Converters.StackOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000569 System.Void System.Text.Json.Serialization.Converters.StackOfTConverter`2::.ctor()
// 0x0000056A System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::Initialize(System.Text.Json.JsonSerializerOptions)
// 0x0000056B System.Boolean System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::TryLookupConstructorParameter(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.JsonParameterInfo&)
// 0x0000056C System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::EndRead(System.Text.Json.ReadStack&)
// 0x0000056D System.Boolean System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::FoundKeyProperty(System.String,System.Boolean)
// 0x0000056E System.Boolean System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::FoundValueProperty(System.String,System.Boolean)
// 0x0000056F System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::.ctor()
// 0x00000570 System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::.cctor()
// 0x00000571 System.Boolean System.Text.Json.Serialization.Converters.ObjectConverterFactory::CanConvert(System.Type)
extern void ObjectConverterFactory_CanConvert_m6543F946815E3D80FBB55C426E9D42087DDBA0B5 (void);
// 0x00000572 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.ObjectConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverterFactory_CreateConverter_mF5C40E6C87E1E4C695C254AF2824A8E1E6638409 (void);
// 0x00000573 System.Boolean System.Text.Json.Serialization.Converters.ObjectConverterFactory::IsKeyValuePair(System.Type)
extern void ObjectConverterFactory_IsKeyValuePair_mDBD26F0A8A4A394BBC1ADF24D27DDD87B7D676E5 (void);
// 0x00000574 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.ObjectConverterFactory::CreateKeyValuePairConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverterFactory_CreateKeyValuePairConverter_m1880F891BCEF327ACBE44EC5BA04AB4C822F6829 (void);
// 0x00000575 System.Reflection.ConstructorInfo System.Text.Json.Serialization.Converters.ObjectConverterFactory::GetDeserializationConstructor(System.Type)
extern void ObjectConverterFactory_GetDeserializationConstructor_m237DD562CA25D869CCDB2CD198F33073CC9C6984 (void);
// 0x00000576 System.Void System.Text.Json.Serialization.Converters.ObjectConverterFactory::.ctor()
extern void ObjectConverterFactory__ctor_mC9095404A2B5BC9D0ADAF1D5F31C8C8668868BAB (void);
// 0x00000577 System.Boolean System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x00000578 System.Boolean System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::OnTryWrite(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000579 System.Void System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::ReadPropertyValue(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonPropertyInfo,System.Boolean)
// 0x0000057A System.Boolean System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::ReadAheadPropertyValue(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonPropertyInfo)
// 0x0000057B System.Void System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000057C System.Void System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::.ctor()
// 0x0000057D System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x0000057E System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::InitializeConstructorArgumentCaches(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000057F System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::ReadAndCacheConstructorArgument(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000580 System.Object System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::CreateObject(System.Text.Json.ReadStackFrame&)
// 0x00000581 System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::ReadConstructorArguments(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
// 0x00000582 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::ReadConstructorArgumentsWithContinuation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
// 0x00000583 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::HandleConstructorArgumentWithContinuation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000584 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::HandlePropertyWithContinuation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonPropertyInfo)
// 0x00000585 System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::BeginRead(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
// 0x00000586 System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::EndRead(System.Text.Json.ReadStack&)
// 0x00000587 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::TryLookupConstructorParameter(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.JsonParameterInfo&)
// 0x00000588 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::get_ConstructorIsParameterized()
// 0x00000589 System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::.ctor()
// 0x0000058A System.Boolean System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::ReadAndCacheConstructorArgument(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x0000058B System.Object System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::CreateObject(System.Text.Json.ReadStackFrame&)
// 0x0000058C System.Void System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::InitializeConstructorArgumentCaches(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000058D System.Void System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::.ctor()
// 0x0000058E System.Object System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::CreateObject(System.Text.Json.ReadStackFrame&)
// 0x0000058F System.Boolean System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::ReadAndCacheConstructorArgument(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000590 System.Boolean System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::TryRead(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo,TArg&)
// 0x00000591 System.Void System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::InitializeConstructorArgumentCaches(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000592 System.Void System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::.ctor()
// 0x00000593 System.Boolean System.Text.Json.Serialization.Converters.BooleanConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void BooleanConverter_Read_mFB2A09BB84222639135B3B46AF8267A7FFAC7D93 (void);
// 0x00000594 System.Void System.Text.Json.Serialization.Converters.BooleanConverter::Write(System.Text.Json.Utf8JsonWriter,System.Boolean,System.Text.Json.JsonSerializerOptions)
extern void BooleanConverter_Write_m41E803744353A7A3BA9713B348C68AF0EB4DB222 (void);
// 0x00000595 System.Boolean System.Text.Json.Serialization.Converters.BooleanConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void BooleanConverter_ReadWithQuotes_m3F5A61BADCCD0A402462CB52A39E60984BD5B61C (void);
// 0x00000596 System.Void System.Text.Json.Serialization.Converters.BooleanConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Boolean,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void BooleanConverter_WriteWithQuotes_m41C8F8BFC73EC7DE758C72BD540936E126691D26 (void);
// 0x00000597 System.Void System.Text.Json.Serialization.Converters.BooleanConverter::.ctor()
extern void BooleanConverter__ctor_mE388F5832A9681929911F9B7353AB103279ABA13 (void);
// 0x00000598 System.Byte[] System.Text.Json.Serialization.Converters.ByteArrayConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Read_mD3EC879942CAA662EA0816256731313209464465 (void);
// 0x00000599 System.Void System.Text.Json.Serialization.Converters.ByteArrayConverter::Write(System.Text.Json.Utf8JsonWriter,System.Byte[],System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Write_mAE292A366097ADA6F4AC019859969D9388BA1B7F (void);
// 0x0000059A System.Void System.Text.Json.Serialization.Converters.ByteArrayConverter::.ctor()
extern void ByteArrayConverter__ctor_m3F6F244FD1C500A2E219E2CA82C92BD219A07601 (void);
// 0x0000059B System.Void System.Text.Json.Serialization.Converters.ByteConverter::.ctor()
extern void ByteConverter__ctor_mDC1BE36B8806904F274C7BC105C3428B66FCF897 (void);
// 0x0000059C System.Byte System.Text.Json.Serialization.Converters.ByteConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ByteConverter_Read_m36BFD618C371042FEDA433A3B3E5698B82231263 (void);
// 0x0000059D System.Void System.Text.Json.Serialization.Converters.ByteConverter::Write(System.Text.Json.Utf8JsonWriter,System.Byte,System.Text.Json.JsonSerializerOptions)
extern void ByteConverter_Write_m898451B6F179CE908AFE20194A739766C29C9789 (void);
// 0x0000059E System.Byte System.Text.Json.Serialization.Converters.ByteConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void ByteConverter_ReadWithQuotes_m4A233A2BA86FB11BEAA2B811DED4685F94A6E64C (void);
// 0x0000059F System.Void System.Text.Json.Serialization.Converters.ByteConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Byte,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void ByteConverter_WriteWithQuotes_m730BDDA853C1A1D2223DC1FE07529F3537361050 (void);
// 0x000005A0 System.Byte System.Text.Json.Serialization.Converters.ByteConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void ByteConverter_ReadNumberWithCustomHandling_mFEBB66F13E29236D097AAD3C762EF8F130C3A420 (void);
// 0x000005A1 System.Void System.Text.Json.Serialization.Converters.ByteConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Byte,System.Text.Json.Serialization.JsonNumberHandling)
extern void ByteConverter_WriteNumberWithCustomHandling_mDF8AB8B886F0F792B6751D9A8586A155A83E6911 (void);
// 0x000005A2 System.Char System.Text.Json.Serialization.Converters.CharConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void CharConverter_Read_mD8D347B9B0F8B7E206F4148B9A124F803B0FBDEE (void);
// 0x000005A3 System.Void System.Text.Json.Serialization.Converters.CharConverter::Write(System.Text.Json.Utf8JsonWriter,System.Char,System.Text.Json.JsonSerializerOptions)
extern void CharConverter_Write_m4690C764BE7D54FC9103D5C49BD771A713A2C9CA (void);
// 0x000005A4 System.Char System.Text.Json.Serialization.Converters.CharConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void CharConverter_ReadWithQuotes_m1CEA2C76923B56EAEAD30C9C1A328B099770E49B (void);
// 0x000005A5 System.Void System.Text.Json.Serialization.Converters.CharConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Char,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void CharConverter_WriteWithQuotes_m4499E42F5C44FE3B17C8C8F95984B05117908825 (void);
// 0x000005A6 System.Void System.Text.Json.Serialization.Converters.CharConverter::.ctor()
extern void CharConverter__ctor_mD056ED844564EB48D3803EDF9E29162DF9F08647 (void);
// 0x000005A7 System.DateTime System.Text.Json.Serialization.Converters.DateTimeConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DateTimeConverter_Read_m71862B549C6FFF428D4C547040B6C881FFBA4DB8 (void);
// 0x000005A8 System.Void System.Text.Json.Serialization.Converters.DateTimeConverter::Write(System.Text.Json.Utf8JsonWriter,System.DateTime,System.Text.Json.JsonSerializerOptions)
extern void DateTimeConverter_Write_mAA4174777ED8224977D11C536C395C93C0B871D6 (void);
// 0x000005A9 System.DateTime System.Text.Json.Serialization.Converters.DateTimeConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DateTimeConverter_ReadWithQuotes_mE0A9B086B0855CF6979E5EE047E3CE06A6594BE4 (void);
// 0x000005AA System.Void System.Text.Json.Serialization.Converters.DateTimeConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.DateTime,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DateTimeConverter_WriteWithQuotes_mCD99AB163FBBE90DBAC390ED00A015C545FBCC5E (void);
// 0x000005AB System.Void System.Text.Json.Serialization.Converters.DateTimeConverter::.ctor()
extern void DateTimeConverter__ctor_m69497F4C605C588FEAB797F50762A0D7B396CBEC (void);
// 0x000005AC System.DateTimeOffset System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DateTimeOffsetConverter_Read_mC344B8C52B92A8BAB047CF5C32C4CEFDC57DDAB3 (void);
// 0x000005AD System.Void System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::Write(System.Text.Json.Utf8JsonWriter,System.DateTimeOffset,System.Text.Json.JsonSerializerOptions)
extern void DateTimeOffsetConverter_Write_m82D4B6513B205F9BD02BEAB210BD045C68C05D9A (void);
// 0x000005AE System.DateTimeOffset System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DateTimeOffsetConverter_ReadWithQuotes_m35836A466E40BB87C9822CF800E67ACD1EE557D7 (void);
// 0x000005AF System.Void System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.DateTimeOffset,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DateTimeOffsetConverter_WriteWithQuotes_m1A16C1DEFFE193417900F336557154B516D31FBF (void);
// 0x000005B0 System.Void System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::.ctor()
extern void DateTimeOffsetConverter__ctor_m06CE7465B3E5F11F6D16C814D9C9D7D3DDA5E2DC (void);
// 0x000005B1 System.Void System.Text.Json.Serialization.Converters.DecimalConverter::.ctor()
extern void DecimalConverter__ctor_mEFCA590307B9F7A366B88EBF1C8367291F5EBCEC (void);
// 0x000005B2 System.Decimal System.Text.Json.Serialization.Converters.DecimalConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DecimalConverter_Read_m7ADF3C4B1F4C7BF848182930287BDC8413973C7E (void);
// 0x000005B3 System.Void System.Text.Json.Serialization.Converters.DecimalConverter::Write(System.Text.Json.Utf8JsonWriter,System.Decimal,System.Text.Json.JsonSerializerOptions)
extern void DecimalConverter_Write_m4E9821CBBA9A0A00531B73AF83B2C423269F970A (void);
// 0x000005B4 System.Decimal System.Text.Json.Serialization.Converters.DecimalConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DecimalConverter_ReadWithQuotes_mBEF033E77CACAD377716659AEB8F65E43D99A1F5 (void);
// 0x000005B5 System.Void System.Text.Json.Serialization.Converters.DecimalConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Decimal,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DecimalConverter_WriteWithQuotes_m01155DC815E9662D1878A6EF1727A0ECE2A4FE39 (void);
// 0x000005B6 System.Decimal System.Text.Json.Serialization.Converters.DecimalConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void DecimalConverter_ReadNumberWithCustomHandling_mF8C5C71390C989CB258349167D3346659DE23B0C (void);
// 0x000005B7 System.Void System.Text.Json.Serialization.Converters.DecimalConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Decimal,System.Text.Json.Serialization.JsonNumberHandling)
extern void DecimalConverter_WriteNumberWithCustomHandling_m4853FB324B7D90EFA779252D47FBFAA1B9422267 (void);
// 0x000005B8 System.Void System.Text.Json.Serialization.Converters.DoubleConverter::.ctor()
extern void DoubleConverter__ctor_m702EB5B29EE3EE96395C7B8E079FDA8D04F8B7CD (void);
// 0x000005B9 System.Double System.Text.Json.Serialization.Converters.DoubleConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DoubleConverter_Read_m21651F7F263680E93072C2E7E3BE650A902ED877 (void);
// 0x000005BA System.Void System.Text.Json.Serialization.Converters.DoubleConverter::Write(System.Text.Json.Utf8JsonWriter,System.Double,System.Text.Json.JsonSerializerOptions)
extern void DoubleConverter_Write_mF42CEC2F8C8BBF70FBC32631208E0DCB77C05009 (void);
// 0x000005BB System.Double System.Text.Json.Serialization.Converters.DoubleConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DoubleConverter_ReadWithQuotes_mBE3F5B124A6865F00F6C9FA32700CDFF734934E4 (void);
// 0x000005BC System.Void System.Text.Json.Serialization.Converters.DoubleConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Double,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DoubleConverter_WriteWithQuotes_m8E9CC8AA72F955F3EA2F6EF14D9488059695F234 (void);
// 0x000005BD System.Double System.Text.Json.Serialization.Converters.DoubleConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void DoubleConverter_ReadNumberWithCustomHandling_mA748D66176F5C229A8259642811DEBCB560209D5 (void);
// 0x000005BE System.Void System.Text.Json.Serialization.Converters.DoubleConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Double,System.Text.Json.Serialization.JsonNumberHandling)
extern void DoubleConverter_WriteNumberWithCustomHandling_mBEB8266FCDB55DA191EF2365202ED3B965EFE7B7 (void);
// 0x000005BF System.Boolean System.Text.Json.Serialization.Converters.EnumConverter`1::CanConvert(System.Type)
// 0x000005C0 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::.ctor(System.Text.Json.Serialization.Converters.EnumConverterOptions,System.Text.Json.JsonSerializerOptions)
// 0x000005C1 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::.ctor(System.Text.Json.Serialization.Converters.EnumConverterOptions,System.Text.Json.JsonNamingPolicy,System.Text.Json.JsonSerializerOptions)
// 0x000005C2 T System.Text.Json.Serialization.Converters.EnumConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000005C3 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::Write(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions)
// 0x000005C4 System.UInt64 System.Text.Json.Serialization.Converters.EnumConverter`1::ConvertToUInt64(System.Object)
// 0x000005C5 System.Boolean System.Text.Json.Serialization.Converters.EnumConverter`1::IsValidIdentifier(System.String)
// 0x000005C6 System.Text.Json.JsonEncodedText System.Text.Json.Serialization.Converters.EnumConverter`1::FormatEnumValue(System.String,System.Text.Encodings.Web.JavaScriptEncoder)
// 0x000005C7 System.String System.Text.Json.Serialization.Converters.EnumConverter`1::FormatEnumValueToString(System.String,System.Text.Encodings.Web.JavaScriptEncoder)
// 0x000005C8 T System.Text.Json.Serialization.Converters.EnumConverter`1::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
// 0x000005C9 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000005CA System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::.cctor()
// 0x000005CB System.Void System.Text.Json.Serialization.Converters.EnumConverterFactory::.ctor()
extern void EnumConverterFactory__ctor_mEEDAE7130310FDECC8E6F062133C6F6F7DA09DDA (void);
// 0x000005CC System.Boolean System.Text.Json.Serialization.Converters.EnumConverterFactory::CanConvert(System.Type)
extern void EnumConverterFactory_CanConvert_m0D83650EBCD31A14AC620C285057BA4BAE2018F5 (void);
// 0x000005CD System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.EnumConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void EnumConverterFactory_CreateConverter_m20B8EF5F66696851433404F45C06DD841CAD3328 (void);
// 0x000005CE System.Guid System.Text.Json.Serialization.Converters.GuidConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void GuidConverter_Read_mEF4830B9006FDBAA156B92C0AB97C842EDF7629C (void);
// 0x000005CF System.Void System.Text.Json.Serialization.Converters.GuidConverter::Write(System.Text.Json.Utf8JsonWriter,System.Guid,System.Text.Json.JsonSerializerOptions)
extern void GuidConverter_Write_m0AACDF686CBDE7FF89B770CAEFD925675BD718F9 (void);
// 0x000005D0 System.Guid System.Text.Json.Serialization.Converters.GuidConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void GuidConverter_ReadWithQuotes_m84235BDFEEDAFB834A5BFC726AC8F963DCBA1A94 (void);
// 0x000005D1 System.Void System.Text.Json.Serialization.Converters.GuidConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Guid,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void GuidConverter_WriteWithQuotes_mA6384193EE0C2385C13EB73B11EA60D12585C230 (void);
// 0x000005D2 System.Void System.Text.Json.Serialization.Converters.GuidConverter::.ctor()
extern void GuidConverter__ctor_m5630C5C8D370ED5766E306996B18A6010F84B0CA (void);
// 0x000005D3 System.Void System.Text.Json.Serialization.Converters.Int16Converter::.ctor()
extern void Int16Converter__ctor_mBD70BF43C269316264BF5CE138887075824CC748 (void);
// 0x000005D4 System.Int16 System.Text.Json.Serialization.Converters.Int16Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void Int16Converter_Read_mE31ADD80DAE4D3D19FEBB76E6F3DC6265079DFDF (void);
// 0x000005D5 System.Void System.Text.Json.Serialization.Converters.Int16Converter::Write(System.Text.Json.Utf8JsonWriter,System.Int16,System.Text.Json.JsonSerializerOptions)
extern void Int16Converter_Write_m68EA146B5E5FAB2613D49987F56E4167B7A1CE0B (void);
// 0x000005D6 System.Int16 System.Text.Json.Serialization.Converters.Int16Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void Int16Converter_ReadWithQuotes_m8D4E3D1CE5ACA53EA0BD898691E57AE226ED1048 (void);
// 0x000005D7 System.Void System.Text.Json.Serialization.Converters.Int16Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Int16,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void Int16Converter_WriteWithQuotes_m91D7C8064F59EA295300649FB1A9C6360CD453EB (void);
// 0x000005D8 System.Int16 System.Text.Json.Serialization.Converters.Int16Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int16Converter_ReadNumberWithCustomHandling_m7A7386D05259D1B5E6E1F48BD1DA4906B2024F76 (void);
// 0x000005D9 System.Void System.Text.Json.Serialization.Converters.Int16Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Int16,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int16Converter_WriteNumberWithCustomHandling_m429A09F34743A751F01CE58FF4DBB5F186408123 (void);
// 0x000005DA System.Void System.Text.Json.Serialization.Converters.Int32Converter::.ctor()
extern void Int32Converter__ctor_m86BB105F3C950053BA4573D91081C4F512469649 (void);
// 0x000005DB System.Int32 System.Text.Json.Serialization.Converters.Int32Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void Int32Converter_Read_mB293C305008AB7739BCC636615FBD10994B4B6D7 (void);
// 0x000005DC System.Void System.Text.Json.Serialization.Converters.Int32Converter::Write(System.Text.Json.Utf8JsonWriter,System.Int32,System.Text.Json.JsonSerializerOptions)
extern void Int32Converter_Write_mC427AE572FB79274C73C1BC8429327935AAD4C8F (void);
// 0x000005DD System.Int32 System.Text.Json.Serialization.Converters.Int32Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void Int32Converter_ReadWithQuotes_m11B1466B71AD1FC1905E8FBD183F52B275128E3B (void);
// 0x000005DE System.Void System.Text.Json.Serialization.Converters.Int32Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Int32,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void Int32Converter_WriteWithQuotes_m16E4C4192BA8C1393CEA69DAD8661859D19BF42B (void);
// 0x000005DF System.Int32 System.Text.Json.Serialization.Converters.Int32Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int32Converter_ReadNumberWithCustomHandling_mAE9D0F72AEC059596152C275E9A1696AEA06C5FA (void);
// 0x000005E0 System.Void System.Text.Json.Serialization.Converters.Int32Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Int32,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int32Converter_WriteNumberWithCustomHandling_m3E83221E6D55EF225BC8F9069C18BF27CF395EDC (void);
// 0x000005E1 System.Void System.Text.Json.Serialization.Converters.Int64Converter::.ctor()
extern void Int64Converter__ctor_m75702B4968715592FEFF2ABEBBF794D23AE80768 (void);
// 0x000005E2 System.Int64 System.Text.Json.Serialization.Converters.Int64Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void Int64Converter_Read_mBA8C19892337C0D03B4C3A180CD6F72804952C21 (void);
// 0x000005E3 System.Void System.Text.Json.Serialization.Converters.Int64Converter::Write(System.Text.Json.Utf8JsonWriter,System.Int64,System.Text.Json.JsonSerializerOptions)
extern void Int64Converter_Write_m3C145DDE5EBFDAEB8931DA12216F39B64ACE0CF5 (void);
// 0x000005E4 System.Int64 System.Text.Json.Serialization.Converters.Int64Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void Int64Converter_ReadWithQuotes_mF8ABD27019D7E41A79CC841890361693399866DC (void);
// 0x000005E5 System.Void System.Text.Json.Serialization.Converters.Int64Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Int64,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void Int64Converter_WriteWithQuotes_m8EBAECFBF7E253E4CACF1B97130956CB1B710B61 (void);
// 0x000005E6 System.Int64 System.Text.Json.Serialization.Converters.Int64Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int64Converter_ReadNumberWithCustomHandling_m91CE222A62902E5B5444C923B6C563148F67FF41 (void);
// 0x000005E7 System.Void System.Text.Json.Serialization.Converters.Int64Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Int64,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int64Converter_WriteNumberWithCustomHandling_m239802F34EDC0CAAA1C3BE3796B90B67C1072A03 (void);
// 0x000005E8 System.Text.Json.JsonDocument System.Text.Json.Serialization.Converters.JsonDocumentConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonDocumentConverter_Read_m69B4B2572ED0150780A126562600339E51EB1020 (void);
// 0x000005E9 System.Void System.Text.Json.Serialization.Converters.JsonDocumentConverter::Write(System.Text.Json.Utf8JsonWriter,System.Text.Json.JsonDocument,System.Text.Json.JsonSerializerOptions)
extern void JsonDocumentConverter_Write_m96D0D6A047E48022F4C0F3C4DC04FAD6B34E67D9 (void);
// 0x000005EA System.Void System.Text.Json.Serialization.Converters.JsonDocumentConverter::.ctor()
extern void JsonDocumentConverter__ctor_mFDD94AA0515271294B40E5A9B6206031FAF8A462 (void);
// 0x000005EB System.Text.Json.JsonElement System.Text.Json.Serialization.Converters.JsonElementConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonElementConverter_Read_m62556747E36B4E965742F1B2A62839C3F6D30F01 (void);
// 0x000005EC System.Void System.Text.Json.Serialization.Converters.JsonElementConverter::Write(System.Text.Json.Utf8JsonWriter,System.Text.Json.JsonElement,System.Text.Json.JsonSerializerOptions)
extern void JsonElementConverter_Write_m875FEA16A976C30F89E2566036BC71B31941E14B (void);
// 0x000005ED System.Void System.Text.Json.Serialization.Converters.JsonElementConverter::.ctor()
extern void JsonElementConverter__ctor_m59F52B2210D31ED7532E9F9AE5D26241EF21E72D (void);
// 0x000005EE System.Void System.Text.Json.Serialization.Converters.NullableConverter`1::.ctor(System.Text.Json.Serialization.JsonConverter`1<T>)
// 0x000005EF System.Nullable`1<T> System.Text.Json.Serialization.Converters.NullableConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000005F0 System.Void System.Text.Json.Serialization.Converters.NullableConverter`1::Write(System.Text.Json.Utf8JsonWriter,System.Nullable`1<T>,System.Text.Json.JsonSerializerOptions)
// 0x000005F1 System.Nullable`1<T> System.Text.Json.Serialization.Converters.NullableConverter`1::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000005F2 System.Void System.Text.Json.Serialization.Converters.NullableConverter`1::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Nullable`1<T>,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000005F3 System.Boolean System.Text.Json.Serialization.Converters.NullableConverterFactory::CanConvert(System.Type)
extern void NullableConverterFactory_CanConvert_mE4D9E8D6B7D755557333B4AA02162CAA0CE50A44 (void);
// 0x000005F4 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.NullableConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void NullableConverterFactory_CreateConverter_m54C590DDA872BFE55C59D43A6BF7782ED090BF90 (void);
// 0x000005F5 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.NullableConverterFactory::CreateValueConverter(System.Type,System.Text.Json.Serialization.JsonConverter)
extern void NullableConverterFactory_CreateValueConverter_m451CC34971C525BF2FFC89EE6697F936C1100492 (void);
// 0x000005F6 System.Void System.Text.Json.Serialization.Converters.NullableConverterFactory::.ctor()
extern void NullableConverterFactory__ctor_mA68D5C5C18A8D880B91392F77C1543DA2D5927DA (void);
// 0x000005F7 System.Object System.Text.Json.Serialization.Converters.ObjectConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverter_Read_m255A07C57AFC85B5DAF7C030DA1A79BB3FB5D9F3 (void);
// 0x000005F8 System.Void System.Text.Json.Serialization.Converters.ObjectConverter::Write(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverter_Write_m390A6C2A02AE9F6EED29A7D8612E4F02A7E13B0D (void);
// 0x000005F9 System.Object System.Text.Json.Serialization.Converters.ObjectConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void ObjectConverter_ReadWithQuotes_m773C97CC7A7D13547810511EE56585EDE01D2F86 (void);
// 0x000005FA System.Void System.Text.Json.Serialization.Converters.ObjectConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void ObjectConverter_WriteWithQuotes_mA18D561E027585854C2EAA45575FA2B9CF2DCB68 (void);
// 0x000005FB System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.ObjectConverter::GetRuntimeConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverter_GetRuntimeConverter_mFC806C4DF191410398CA9E1CEF5C065180A95AEA (void);
// 0x000005FC System.Void System.Text.Json.Serialization.Converters.ObjectConverter::.ctor()
extern void ObjectConverter__ctor_mE6AA92F654A4CBA2FF3F33BF7C4B2FF1EE6ADD64 (void);
// 0x000005FD System.Void System.Text.Json.Serialization.Converters.SByteConverter::.ctor()
extern void SByteConverter__ctor_m0226F352C1E59905C1FC4534AFC979DBDEE59CD4 (void);
// 0x000005FE System.SByte System.Text.Json.Serialization.Converters.SByteConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void SByteConverter_Read_m59BF56D2CE77C7DF3C3B0B034623E93E3AF2AAB0 (void);
// 0x000005FF System.Void System.Text.Json.Serialization.Converters.SByteConverter::Write(System.Text.Json.Utf8JsonWriter,System.SByte,System.Text.Json.JsonSerializerOptions)
extern void SByteConverter_Write_m03988D60FBAC4F1C6FD9148BC1F4BA22D1D4EEEE (void);
// 0x00000600 System.SByte System.Text.Json.Serialization.Converters.SByteConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void SByteConverter_ReadWithQuotes_mDB3B3B13BEE5C5335F5DD39DDFDDD49CBB2457A6 (void);
// 0x00000601 System.Void System.Text.Json.Serialization.Converters.SByteConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.SByte,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void SByteConverter_WriteWithQuotes_m168EC0103C1DE49348A48AE46C7AABBBB047EF98 (void);
// 0x00000602 System.SByte System.Text.Json.Serialization.Converters.SByteConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void SByteConverter_ReadNumberWithCustomHandling_mA25DD660D994657E4BC2189F9561AD61D92231EB (void);
// 0x00000603 System.Void System.Text.Json.Serialization.Converters.SByteConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.SByte,System.Text.Json.Serialization.JsonNumberHandling)
extern void SByteConverter_WriteNumberWithCustomHandling_m68A3B9D795436708BA0305B5EEC02471A7115EFA (void);
// 0x00000604 System.Void System.Text.Json.Serialization.Converters.SingleConverter::.ctor()
extern void SingleConverter__ctor_m1D3CCBA0D91B726AF4A6357FE9F89EF212A75B96 (void);
// 0x00000605 System.Single System.Text.Json.Serialization.Converters.SingleConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void SingleConverter_Read_mA58721E52C79C54351B95AF2026B3D688A8CE248 (void);
// 0x00000606 System.Void System.Text.Json.Serialization.Converters.SingleConverter::Write(System.Text.Json.Utf8JsonWriter,System.Single,System.Text.Json.JsonSerializerOptions)
extern void SingleConverter_Write_m1A327CCFAD5C3C82822713A6087182FDF2E286EB (void);
// 0x00000607 System.Single System.Text.Json.Serialization.Converters.SingleConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void SingleConverter_ReadWithQuotes_m084B0E1107CAE5AF77F6E35B1BC32181D2986FB8 (void);
// 0x00000608 System.Void System.Text.Json.Serialization.Converters.SingleConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Single,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void SingleConverter_WriteWithQuotes_m2543083B1F856039184CA6C66FC26B212D695F0E (void);
// 0x00000609 System.Single System.Text.Json.Serialization.Converters.SingleConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void SingleConverter_ReadNumberWithCustomHandling_m46445002FE93669507284AF5D13C348A80A69311 (void);
// 0x0000060A System.Void System.Text.Json.Serialization.Converters.SingleConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Single,System.Text.Json.Serialization.JsonNumberHandling)
extern void SingleConverter_WriteNumberWithCustomHandling_m572662F9B95E308AE0CE3EC6C23652E664C88DA9 (void);
// 0x0000060B System.String System.Text.Json.Serialization.Converters.StringConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void StringConverter_Read_mAD9A97FF3AE09596D8B066A5CEBBF2FFE4C77DD8 (void);
// 0x0000060C System.Void System.Text.Json.Serialization.Converters.StringConverter::Write(System.Text.Json.Utf8JsonWriter,System.String,System.Text.Json.JsonSerializerOptions)
extern void StringConverter_Write_mAC569F931091E5D12AE5CE67B4744812FF1B684B (void);
// 0x0000060D System.String System.Text.Json.Serialization.Converters.StringConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void StringConverter_ReadWithQuotes_m9701E6D776302D7F442A172B843CC7C99C15BC84 (void);
// 0x0000060E System.Void System.Text.Json.Serialization.Converters.StringConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.String,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void StringConverter_WriteWithQuotes_mEFA505055E63279D3D56A3ED4609A83E4031CD0B (void);
// 0x0000060F System.Void System.Text.Json.Serialization.Converters.StringConverter::.ctor()
extern void StringConverter__ctor_m08EA132640C1FD185A138324ADC672C95253C53E (void);
// 0x00000610 System.Type System.Text.Json.Serialization.Converters.TypeConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void TypeConverter_Read_m504541EA26F6622097C748075A76FB5B9B3CDEFF (void);
// 0x00000611 System.Void System.Text.Json.Serialization.Converters.TypeConverter::Write(System.Text.Json.Utf8JsonWriter,System.Type,System.Text.Json.JsonSerializerOptions)
extern void TypeConverter_Write_mDB6A094A2A298A57DB9BBAE51DED652F967FBF5B (void);
// 0x00000612 System.Void System.Text.Json.Serialization.Converters.TypeConverter::.ctor()
extern void TypeConverter__ctor_mBC50A16A2BAE8EB6797FE215E167909B962222AD (void);
// 0x00000613 System.Void System.Text.Json.Serialization.Converters.UInt16Converter::.ctor()
extern void UInt16Converter__ctor_mB9FAB69B5C4A1AE5969F36F87EA01CB6B08C6AA1 (void);
// 0x00000614 System.UInt16 System.Text.Json.Serialization.Converters.UInt16Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UInt16Converter_Read_m0FE2A140946AAA98C66D4E55D72BAACDE7C9FF7A (void);
// 0x00000615 System.Void System.Text.Json.Serialization.Converters.UInt16Converter::Write(System.Text.Json.Utf8JsonWriter,System.UInt16,System.Text.Json.JsonSerializerOptions)
extern void UInt16Converter_Write_mBB50A24D83B482540E79F8CCE6C65DFB1429E878 (void);
// 0x00000616 System.UInt16 System.Text.Json.Serialization.Converters.UInt16Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void UInt16Converter_ReadWithQuotes_m399BC8FCE941BE79E3962BAE4E9474BA1272696E (void);
// 0x00000617 System.Void System.Text.Json.Serialization.Converters.UInt16Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.UInt16,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void UInt16Converter_WriteWithQuotes_mAB636299084028B276F94889DC5B95B5DD38375F (void);
// 0x00000618 System.UInt16 System.Text.Json.Serialization.Converters.UInt16Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt16Converter_ReadNumberWithCustomHandling_mD13135F3762A0B8CAD963023CF2985B85059C903 (void);
// 0x00000619 System.Void System.Text.Json.Serialization.Converters.UInt16Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.UInt16,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt16Converter_WriteNumberWithCustomHandling_mCA10802C9674C2C5FD1E936F2D79D6651243B078 (void);
// 0x0000061A System.Void System.Text.Json.Serialization.Converters.UInt32Converter::.ctor()
extern void UInt32Converter__ctor_mF2FDEC998CB93C31EC382FD294B78058F82B4781 (void);
// 0x0000061B System.UInt32 System.Text.Json.Serialization.Converters.UInt32Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UInt32Converter_Read_m65C9D0C61A51645F7B3126B4BF2E2A6FFFC94978 (void);
// 0x0000061C System.Void System.Text.Json.Serialization.Converters.UInt32Converter::Write(System.Text.Json.Utf8JsonWriter,System.UInt32,System.Text.Json.JsonSerializerOptions)
extern void UInt32Converter_Write_mB2A61E88793C2756E786C4BE6B7A1618C5B30926 (void);
// 0x0000061D System.UInt32 System.Text.Json.Serialization.Converters.UInt32Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void UInt32Converter_ReadWithQuotes_mB3192ED36F55E17EBA521A5C63461C65E84F8C36 (void);
// 0x0000061E System.Void System.Text.Json.Serialization.Converters.UInt32Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.UInt32,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void UInt32Converter_WriteWithQuotes_m770F506AEF837ECFAA145834E4CBF672877C7843 (void);
// 0x0000061F System.UInt32 System.Text.Json.Serialization.Converters.UInt32Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt32Converter_ReadNumberWithCustomHandling_m1CB5FAAE836611E01CD3A92ACA9C09DD5AA0061C (void);
// 0x00000620 System.Void System.Text.Json.Serialization.Converters.UInt32Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.UInt32,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt32Converter_WriteNumberWithCustomHandling_m9518AF0B62A0E9E224888CAA2C6CDA4A5F7AA374 (void);
// 0x00000621 System.Void System.Text.Json.Serialization.Converters.UInt64Converter::.ctor()
extern void UInt64Converter__ctor_mD350ADC6E5C9FF3209C8576B823405605E977143 (void);
// 0x00000622 System.UInt64 System.Text.Json.Serialization.Converters.UInt64Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UInt64Converter_Read_m17AD682D5DF5BF044D364FDD5295A5CD7C9D8AD5 (void);
// 0x00000623 System.Void System.Text.Json.Serialization.Converters.UInt64Converter::Write(System.Text.Json.Utf8JsonWriter,System.UInt64,System.Text.Json.JsonSerializerOptions)
extern void UInt64Converter_Write_m3F4F83DA9482F95D671343FBF31D3E03E8EB372D (void);
// 0x00000624 System.UInt64 System.Text.Json.Serialization.Converters.UInt64Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void UInt64Converter_ReadWithQuotes_mE4FA5A8A469D62A99B0139B2C0C7F1F065CDA09C (void);
// 0x00000625 System.Void System.Text.Json.Serialization.Converters.UInt64Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.UInt64,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void UInt64Converter_WriteWithQuotes_m581D380D3BC23B7FB980DA2623540984F1E30282 (void);
// 0x00000626 System.UInt64 System.Text.Json.Serialization.Converters.UInt64Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt64Converter_ReadNumberWithCustomHandling_mB697FCA3A9C7C17B7200DF5F740A3D27A75CD593 (void);
// 0x00000627 System.Void System.Text.Json.Serialization.Converters.UInt64Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.UInt64,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt64Converter_WriteNumberWithCustomHandling_m42ADE1C96A5060435CDAC28E2E68F37A419DC63B (void);
// 0x00000628 System.Uri System.Text.Json.Serialization.Converters.UriConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UriConverter_Read_mFB7C5E401604C8E774BD25D31C3BC110C796402B (void);
// 0x00000629 System.Void System.Text.Json.Serialization.Converters.UriConverter::Write(System.Text.Json.Utf8JsonWriter,System.Uri,System.Text.Json.JsonSerializerOptions)
extern void UriConverter_Write_m33C6F8B59440D93ED80906717A73F2C0B958FA64 (void);
// 0x0000062A System.Void System.Text.Json.Serialization.Converters.UriConverter::.ctor()
extern void UriConverter__ctor_mA7F8DFF219CE29B1F0FFC072A23833E44F033340 (void);
// 0x0000062B System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD8060E810F4B51B2479D48F4E38A17EFDA20CAF8 (void);
static Il2CppMethodPointer s_methodPointers[1579] = 
{
	EmbeddedAttribute__ctor_m5268D451D72411EEDF844759DFB5260571B786E7,
	IsReadOnlyAttribute__ctor_m40DD59B1F218F1A9FA9352E6C35EFB651C9C7AB7,
	IsByRefLikeAttribute__ctor_mBAF4018209D90000A5E262F2451AE1C4A82314D9,
	NullableAttribute__ctor_m8BF007522EE357738E8C785DB4366F2D97E78C19,
	NullableContextAttribute__ctor_m68D48412AD963BAB4B133B8A2D216D93CDD50B84,
	NullablePublicOnlyAttribute__ctor_m0C249ACE94B8A56FD98C494E164745681C07BFFC,
	HexConverter_ToCharUpper_mF6A33228506E78664B6DD4DC6DD64E116160B565,
	HexConverter_FromChar_mEFBD63D37E6298E8067997ECD9525D9055BA47A2,
	HexConverter_IsHexChar_mC65F541844A5C576B87D5876293C5253394BDB5C,
	HexConverter_get_CharToHexLookup_m662F5D081F59BD939B773E221C246025A3E1106D,
	SR_UsingResourceKeys_mA97725F3E19BB2DCFEFDFFA236D77067A2A1A1D8,
	SR_GetResourceString_m2D921EAADF4574169AC960323C3347BB2AB0B3D5,
	SR_Format_m36EF1227D7C63135EC15BDFD28B13BE407AFF8BD,
	SR_Format_m8AA8EF600D0F3253D2543A88CE469D44DA95E7EA,
	SR_Format_m5965DF79038A06C4529BA0C615B3D95CDBE8FBC0,
	SR_Format_m98E7A215B14ABB55764D42C2B1B82AA655F42297,
	SR_get_ResourceManager_m15FC5066BDC3FA360108455FF39B0482B2825559,
	SR_get_ArrayDepthTooLarge_m8E5246CC370DE2F9A4B34825F399CBCB14806876,
	SR_get_CannotReadIncompleteUTF16_mFB2C4F4FF896E7A55C3DB5643191326D9B8165B5,
	SR_get_CannotReadInvalidUTF16_mAE828DD100654ED53A8AB575FD83DA87108A92CF,
	SR_get_CannotStartObjectArrayAfterPrimitiveOrClose_mFF35AF8C05CF6369E3C4A86E548FF9E401ECDD3F,
	SR_get_CannotStartObjectArrayWithoutProperty_m796897E58F30C853597753A827F3A6BE5E0E8D2D,
	SR_get_CannotTranscodeInvalidUtf8_mAEEF8B9D2AA4B647D5773014AD372CA67D40EB86,
	SR_get_CannotDecodeInvalidBase64_m425D21F8E36FDC72014BB3CD5AA74B17A7BF14AD,
	SR_get_CannotTranscodeInvalidUtf16_mDF84CC7A7ABFEAA1851B8D919F26C9541BC1239F,
	SR_get_CannotEncodeInvalidUTF16_mAFCBF8ED915D612C54B32ED4722C9FD7685259ED,
	SR_get_CannotEncodeInvalidUTF8_mD044622D0CADDDA2D1888F6FAED934F6D1587494,
	SR_get_CannotWritePropertyWithinArray_m421E81B8569A8E2FF35358575C1C7E940F450C5A,
	SR_get_CannotWritePropertyAfterProperty_m9DB88F0E19DDB2E956DA3D147314473F6F7F3B6A,
	SR_get_CannotWriteValueAfterPrimitiveOrClose_m5EF4BA121A746B12D44E1B2841E7DD326FA8DF0F,
	SR_get_CannotWriteValueWithinObject_mE2E8F0AA4138D19BD3B5D87D4582E6BA1EDD063F,
	SR_get_DepthTooLarge_mCCABDA0571BF22B5950D9544B8C3A43E8AD963ED,
	SR_get_EndOfCommentNotFound_m7EC7813B9DBBFE106F451AF7BF154CE7D47091A1,
	SR_get_EndOfStringNotFound_mE297F8B18F982A97EFF63F9AF985A782FC0B976E,
	SR_get_ExpectedEndAfterSingleJson_m2CBB628DCD24D55C5E65A50EE6BD232E69432ACC,
	SR_get_ExpectedEndOfDigitNotFound_mF5FC07B1E08E3C27D425934D25DA55EFA5039BC3,
	SR_get_ExpectedFalse_m686BF2B905B951FD7A0B7B113B07F4CAD46771E9,
	SR_get_ExpectedJsonTokens_mBD05CE8ABE9D099915995FF7C614E43D6857B48B,
	SR_get_ExpectedOneCompleteToken_m3B85490B936859F69328A8C7E08BC1B0AAD75A88,
	SR_get_ExpectedNextDigitEValueNotFound_m44344C55496E8230A0C9149446C02063C3782DCB,
	SR_get_ExpectedNull_m3EF080DE6DCA1BAE1607E59338060FF6F3423BBB,
	SR_get_ExpectedSeparatorAfterPropertyNameNotFound_mFF5042B71FE071A749F3476A14EF91EDBDEFBABF,
	SR_get_ExpectedStartOfPropertyNotFound_mB9531B5B0EDCB65FF524E2BC7074D372670AE5F0,
	SR_get_ExpectedStartOfPropertyOrValueNotFound_mF76027CF08CB01E66E54EA8E60CEBDF2E87BC043,
	SR_get_ExpectedStartOfValueNotFound_m072A531F7E8A19B716CD16F93C0852070AB8BB67,
	SR_get_ExpectedTrue_mC7DFA8983568EABB3EA8B6DF826B2B5739E1D940,
	SR_get_ExpectedValueAfterPropertyNameNotFound_m420FF497C1ED3DDD5FFC9B58BAF5161F6EE1372A,
	SR_get_FailedToGetLargerSpan_m3BB2F4C63451BBAF226386631F78536835E42328,
	SR_get_FoundInvalidCharacter_m8F98496B841628A9B11FDDFCB4F88BDA59F0B548,
	SR_get_InvalidCast_m1F4FC3EA35C0AC8A2B5D41514AD1B3D450D836A2,
	SR_get_InvalidCharacterAfterEscapeWithinString_m33041C4D461F0DFE6AD9D181D52E51694D51DA78,
	SR_get_InvalidCharacterWithinString_mE4B9F0D32B28B929E8AB8F5CA13974C26EA53F07,
	SR_get_InvalidEndOfJsonNonPrimitive_m9F28AFFDECA3FD3AEB740ECA6ACD48D7BE26635C,
	SR_get_InvalidHexCharacterWithinString_m61BE5F7ED9A6EA51595D41A87515B3777DB5419F,
	SR_get_JsonDocumentDoesNotSupportComments_mD0E352AFD97325D9743DD47A2CDC0012D04BE1EF,
	SR_get_JsonElementHasWrongType_m06721C1E1B060B438059DDAAB0527C2222F0CE59,
	SR_get_MaxDepthMustBePositive_m7631F46DAE0D387C659ABE111CC9EC097028AF8D,
	SR_get_CommentHandlingMustBeValid_mFF8EFAEBE0A74CD87C2B2B1F7AE9060C7E7DEC31,
	SR_get_MismatchedObjectArray_mEE3EAD16343FEAC84B9DD1343FAA26CFD9C3342C,
	SR_get_CannotWriteEndAfterProperty_m1A13AF63A2C1B92898C0DF01AEAA333E6F4604B4,
	SR_get_ObjectDepthTooLarge_mAF0DA4C99B69E61DCE4EA3B561C1B7F23DFA45D0,
	SR_get_PropertyNameTooLarge_m415529365C3B8C57EDB66411E8AEC8DB85DA49C5,
	SR_get_FormatDecimal_m009838A3534AB6E504C0D5A6742A2ACF2C267908,
	SR_get_FormatDouble_m90FBC078819AFDD80C2F100DC32588BAA6552B03,
	SR_get_FormatInt32_m36CEC22CDE95FDCD25FB18260B457855504A477E,
	SR_get_FormatInt64_mE3050647A21317A3F3AF1070683FA7C358F4EAC2,
	SR_get_FormatSingle_m7946C5D11B0370E6EDC9FA19E628382BD3031A50,
	SR_get_FormatUInt32_mB176E75C88FB8AEA8E16D6F2A8A6ED49965327A5,
	SR_get_FormatUInt64_m6564B4C41078A600E0A9009B174C67CAB20A0B29,
	SR_get_RequiredDigitNotFoundAfterDecimal_mD94795A8DD7543F1D60AE8E6379DE02C2060256D,
	SR_get_RequiredDigitNotFoundAfterSign_m238EB00FE33260F01E265A1B01C61E9ED8D1EBA8,
	SR_get_RequiredDigitNotFoundEndOfData_m45227DC6C780B1A8E47D62CF0AAB6790F74A3665,
	SR_get_SpecialNumberValuesNotSupported_mA8D5568457FB092FFAE1913F9529EC01B10480F4,
	SR_get_ValueTooLarge_m1AF9044E740CDC4CD66FA6D04E20B9691BE5039A,
	SR_get_ZeroDepthAtEnd_mA1A2333C1E20A816C198F27FFA159EDFA602026A,
	SR_get_DeserializeUnableToConvertValue_m16ECD3B34DF3EEB8D2D59F63E655CA3FA88BD8F0,
	SR_get_BufferWriterAdvancedTooFar_m1A35B258CD64B5F3E0ED5D95DE441105A0E78144,
	SR_get_FormatDateTime_m3715CD18BF4333F9F31C7D94BE7DF92626B3956F,
	SR_get_FormatDateTimeOffset_m66682500E6071DE7590691A2CADE3143467C71CD,
	SR_get_FormatGuid_mB4DF841F16EC3053865680D5588F017D08C0A6B2,
	SR_get_ExpectedStartOfPropertyOrValueAfterComment_m9477F68FF6E5E93D723D28DDF35D3A87D1AD5368,
	SR_get_TrailingCommaNotAllowedBeforeArrayEnd_m2921E6AA28B2E0ACC59DF93D052C80694FC7ED2F,
	SR_get_TrailingCommaNotAllowedBeforeObjectEnd_mA68FA644F011194FC455FF534F12D90F37594BE9,
	SR_get_SerializerOptionsImmutable_m554851415FEFBE8DCC7377CF33F1B45568E01B56,
	SR_get_SerializerPropertyNameConflict_mCF1896F158E6898327D9E6510BC4906246D7EDA6,
	SR_get_SerializerPropertyNameNull_m36BAF80399FC3141977B43223BFE83C8DDFCC749,
	SR_get_SerializationDataExtensionPropertyInvalid_mD272087BD1E647A7E07F307C879D59DB3E20E57C,
	SR_get_SerializationDuplicateTypeAttribute_mC58F3BC6F19A5EA97D67C4EDC0D1AE7FC1188A0B,
	SR_get_SerializationNotSupportedType_mEFEFCD3214DE4BA4FE7E64AF900D0AFAF28E8F10,
	SR_get_InvalidCharacterAtStartOfComment_m14525B64E5302158767DDE18089E6B2C6A8427CC,
	SR_get_UnexpectedEndOfDataWhileReadingComment_m1B98B6762B9A23E2700C05D3C5D35E91A8B530FF,
	SR_get_CannotSkip_mD3706C1D328229102B14C32A2556EE0F5004500F,
	SR_get_NotEnoughData_m1AEF73034C6E00E2B851E094BCC8DCE731BA1708,
	SR_get_UnexpectedEndOfLineSeparator_m074B44BA6623162BCF899B4ACD971A62A42FD8A8,
	SR_get_DeserializeNoConstructor_mC378B8122523F38D4D286DF45DF798DCFEB2B160,
	SR_get_DeserializePolymorphicInterface_m59C024C53C4D920B4F6EEEE5C3252F5CDB26C1E2,
	SR_get_SerializationConverterOnAttributeNotCompatible_mBD321216A99ED8DA4BA782DDA6EDEE10A0809AE5,
	SR_get_SerializationConverterOnAttributeInvalid_m17DAC92FF850AEE2EB0A6D24DABAB9ABFBA64765,
	SR_get_SerializationConverterRead_m76B57CA2043F03E2022F7B4370B9AC410744607E,
	SR_get_SerializationConverterNotCompatible_m97CE4CE914CBE701C016C61FC6B3D4200E9C38AD,
	SR_get_SerializationConverterWrite_mD85FC4886DA512F98F8F515DF2C830CB3E677874,
	SR_get_NamingPolicyReturnNull_m03339F7AF61FA34FD5B622585C8070706F54B3CA,
	SR_get_SerializationDuplicateAttribute_m5FAF927C5FAA30DCD6E899E69D8526A519A62AEE,
	SR_get_SerializeUnableToSerialize_m8EC60230E9F30396C357BEE9E0D37EE1BF1CF3D8,
	SR_get_FormatByte_mF1FC572F1B92D55407AD1D2C9903878869DD7E30,
	SR_get_FormatInt16_m6C826C304D1F6607CA2FF209280B3E0CA177B042,
	SR_get_FormatSByte_mA1D76EB6BAF90958F3D6676DEBA1F079EB037716,
	SR_get_FormatUInt16_mFEAB5443D9747CFBDFCFB0C8BE59F676BFAAAD4C,
	SR_get_SerializerCycleDetected_mFFE7721687271185764649CAAB7742FF3EBE21FD,
	SR_get_InvalidLeadingZeroInNumber_m6BF7631527A1196B57B4E8A963A03B109EDA9A87,
	SR_get_MetadataCannotParsePreservedObjectToImmutable_m7EC3A2101BB813A5EE910A203FA9546C7E85CD9D,
	SR_get_MetadataDuplicateIdFound_m48729069C940EBF6327DD830AD23EFF4A45B50F5,
	SR_get_MetadataIdIsNotFirstProperty_m7A4D0D201F50ED06A3E7896FDCD21A10A6644B86,
	SR_get_MetadataInvalidReferenceToValueType_m2EEC9DAD55468AEB45E9223F8F8A6BC2206271BF,
	SR_get_MetadataInvalidTokenAfterValues_m0EEB8F20C189BE3ED7AA9F09476FA77A9C830EC4,
	SR_get_MetadataPreservedArrayFailed_mAA65C7CCABAE5ED4926E388DD43238124B4A38E9,
	SR_get_MetadataPreservedArrayInvalidProperty_mB89B42C953D2CF70F3BB5BDB8BF8C67279591E9D,
	SR_get_MetadataPreservedArrayPropertyNotFound_mE12E492A731C2BAE3910426B6AEB2EF09C554706,
	SR_get_MetadataReferenceCannotContainOtherProperties_m1DFC9A024F9BD3931AA5FD0BB743FC4D462DDCE2,
	SR_get_MetadataReferenceNotFound_m8D45C95CA5000DF1FB5CE89D261D1B2DE5CBE290,
	SR_get_MetadataValueWasNotString_m475A3FA20675470B82F401EF804E4C48558040DF,
	SR_get_MetadataInvalidPropertyWithLeadingDollarSign_m359A67E1B5F8433B09015726C6EF235AD046819F,
	SR_get_MultipleMembersBindWithConstructorParameter_mCAD92CB106A39E220CB6299AB523A4424B612D08,
	SR_get_ConstructorParamIncompleteBinding_m2CCDAC5A3D90147955966AA020D69D7FD9EF44A8,
	SR_get_ConstructorMaxOf64Parameters_mF375B56BADBD0B8ABEFC373D305E2725F40B6AE6,
	SR_get_ObjectWithParameterizedCtorRefMetadataNotHonored_m2D6CEEDF3A5B0CEA3E6838A7CACA524C924B4EE0,
	SR_get_SerializerConverterFactoryReturnsNull_m7DBC25C7DC67B14F11C41790FBFFCA05CF3DDBB2,
	SR_get_SerializationNotSupportedParentType_mDDE8D71E618315BF53158A7B57B5F2F4D852E03E,
	SR_get_ExtensionDataCannotBindToCtorParam_m58B18F22CED7F2C91949BE94587D11416A0099AD,
	SR_get_BufferMaximumSizeExceeded_m5E9134FD11FF952E124CA4F7B791222061F93E84,
	SR_get_CannotSerializeInvalidType_m4D018B06F6C60234741517E11F15CE9CDBA6944B,
	SR_get_SerializeTypeInstanceNotSupported_m2D26AC49F1AEDE3712C8E81C64ACD78425C72E07,
	SR_get_JsonIncludeOnNonPublicInvalid_m5AB7E007F3CF437C400FEEBFE8000B76A442521A,
	SR_get_CannotSerializeInvalidMember_m16810AFDB270CA695FAD551B200CC818CD7A98DE,
	SR_get_CannotPopulateCollection_m45B711730FA7FD187B50EB486D99760786166AD6,
	SR_get_FormatBoolean_mC7C304F9CF8C80F0A9C94BB2AD0DC7FBEF29FAB0,
	SR_get_DictionaryKeyTypeNotSupported_mEF919ABBACD824A7444A721193836258EFD50BC5,
	SR_get_IgnoreConditionOnValueTypeInvalid_m2881F0F05D89ED29DD2FD236424EECA72343618F,
	SR_get_NumberHandlingConverterMustBeBuiltIn_m088C7B0AB327313123566300C5BF4BF3CC95AC15,
	SR_get_NumberHandlingOnPropertyTypeMustBeNumberOrCollection_m7F0517EDB2D65A409E4332D53029358E70E9881B,
	SR_get_ConverterCanConvertNullableRedundant_m9A7BC8BF50863F47042BC8040C0E482E9CB184F3,
	SR_get_MetadataReferenceOfTypeCannotBeAssignedToType_mBF707435EF589D0735ED83754A20848E2FACED22,
	SR_get_DeserializeUnableToAssignValue_m42D498C3505DF4B12CAB20B788F803258D4C4120,
	SR_get_DeserializeUnableToAssignNull_m637A485DFDF37C776B4C366E21CCB18DD30E626D,
	SR__cctor_m8D90FACF117AD47C9B5B644B8867CB82DF5047A8,
	ReferenceEqualityComparer__ctor_mD3DE60465F5D40F02215F150B29DBFD2E44BBDB8,
	ReferenceEqualityComparer_get_Instance_m43B68CACB4FE50ABE65B13C224229C45CB7FDC9C,
	ReferenceEqualityComparer_Equals_m318279318BB973F0E7DAD80D891B4BB043E05B36,
	ReferenceEqualityComparer_GetHashCode_m1566E710AA14E24C7EAF3C98116E4268A5024000,
	ReferenceEqualityComparer__cctor_m20BA2C2111D1D47CA7B643AA5200686DCEDBA196,
	DynamicallyAccessedMembersAttribute__ctor_mDBF0F1C7DE04C8B522241B8C64AE1EF300581AB2,
	DisallowNullAttribute__ctor_m828EAECBBC572B96840FDD59AB52545D6B5A604B,
	MaybeNullWhenAttribute__ctor_m151C74610CA51F84259123A4B5996459F06C2F66,
	NotNullWhenAttribute__ctor_m9F2F1AEF7023128756FB1CC3A80713C80BDE8B13,
	DoesNotReturnAttribute__ctor_m365998521609AD313F2672B0198AB466FF2AFF8D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PooledByteBufferWriter__ctor_m2BFCEA5F4BE8129352CD1FA340253322AB96C179,
	PooledByteBufferWriter_get_WrittenMemory_mEAC90C8BAB9357BCAAD2693956860E526BA7FAD1,
	PooledByteBufferWriter_ClearHelper_mE3AC6CCE9E333A079A9FA78F727A843C8442EE36,
	PooledByteBufferWriter_Dispose_m136536F82685598415188C0749D28096A3BAC83E,
	PooledByteBufferWriter_Advance_m17066E970C30D1281CBDCD42A262373B5C8FA7AD,
	PooledByteBufferWriter_GetMemory_mEE7A1F1C92427018DE967C6DFB000382D8D9BBD9,
	PooledByteBufferWriter_CheckAndResizeBuffer_m722C42DADC2B79C901D6D052A78EE70612742C25,
	ThrowHelper_ThrowOutOfMemoryException_BufferMaximumSizeExceeded_m6AACBBF084A0F2C5949C350FEC33B41F950E2A61,
	ThrowHelper_GetArgumentOutOfRangeException_MaxDepthMustBePositive_m61E323FF2E23F64ED4780B8B7D43E0982F68D5B2,
	ThrowHelper_GetArgumentOutOfRangeException_m12F1DB29BB3A4C3EF7387FDEA9BC653DAF915967,
	ThrowHelper_GetArgumentOutOfRangeException_CommentEnumMustBeInRange_m314794343C743984A26372AABA3C20CCC7C8007F,
	ThrowHelper_GetArgumentException_mF411D1E286E8443C3DF3A14535B9F7B16B045C3E,
	ThrowHelper_ThrowArgumentException_PropertyNameTooLarge_m77F825E6FA2ABACC4885601C5C87D9309FF8F0AD,
	ThrowHelper_ThrowArgumentException_ValueTooLarge_mB6D13E7E631571FE910659E29715926B40210CE2,
	ThrowHelper_ThrowArgumentException_ValueNotSupported_m28B4A5E1B7773753D54D7D3D00CA9EF64B8D9BD4,
	ThrowHelper_ThrowInvalidOperationException_NeedLargerSpan_m42791059B9B5492C16E61273D3F33692EAEF9B1B,
	ThrowHelper_ThrowInvalidOperationException_mA7A541529B4A42EF018C2E86CB4C896BD8900615,
	ThrowHelper_ThrowInvalidOperationException_mAEE102BDC978858CF1E7B35197714BC9126D0B34,
	ThrowHelper_GetInvalidOperationException_m582DC8908C0D711A28CE7AE688532F184EF123B6,
	ThrowHelper_GetInvalidOperationException_ExpectedNumber_m5CC60EAE038846B66BC7505CEFD0B1BE0D1E1458,
	ThrowHelper_GetInvalidOperationException_ExpectedBoolean_m234A5FDADB32EB98D7275302ADCEC155B867BDD5,
	ThrowHelper_GetInvalidOperationException_ExpectedString_m7A2BC67F8A810C0EAC070DF8908836988652E6C4,
	ThrowHelper_GetInvalidOperationException_CannotSkipOnPartial_m697CEBF1A69D299EA92AACEF8059AF57D6344826,
	ThrowHelper_GetInvalidOperationException_m064819D6786C949F9E6E08EE8E7F403AB762FEFE,
	ThrowHelper_GetJsonElementWrongTypeException_m2888DF6174A2B338E3B9DEE4066AC35BAB80B879,
	ThrowHelper_GetJsonElementWrongTypeException_mE1A4B1DD7EA07B76C467EDE5EDB770C23AC5D0D3,
	ThrowHelper_ThrowJsonReaderException_mF1FBE8AEF1017A0B089C3B41D3C381DD4C1428BA,
	ThrowHelper_GetJsonReaderException_m249FFD6F2F5BFAB6CB3E1BFE03B57AC158BF8B66,
	ThrowHelper_IsPrintable_mDB633E61D3835B0FEA8FD61F63104BA9DFB55323,
	ThrowHelper_GetPrintableString_m76688EFAA55DD7C6CC1732540772879F29818544,
	ThrowHelper_GetResourceString_mEAF650CD41629EDBEF44935376339BBF6F864F05,
	ThrowHelper_ThrowInvalidOperationException_m388FA9CAE1E6661558861A241C547A9E0DF887BC,
	ThrowHelper_ThrowArgumentException_InvalidUTF8_m5D596F01AA0D55AF6DA151FD0B5C1511090E1B47,
	ThrowHelper_ThrowArgumentException_InvalidUTF16_mFDF55F6C19EC28ED0408D32C4ADC08ECAC22245B,
	ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m95A110D95E052A207FF2F081DF1880159CAD85D0,
	ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_mDF46BDC21FFF5BA6D77F4B3BD33C73B5948D6665,
	ThrowHelper_GetInvalidOperationException_ReadInvalidUTF8_mCD654D469B7C7F7C15E7077698F3B40A4C98F5CE,
	ThrowHelper_GetArgumentException_ReadInvalidUTF16_mAC507137EE56EF11514CAFCD1E047CDEBCE88F64,
	ThrowHelper_GetInvalidOperationException_m6C35ECC9021533FEA1446E3F96CA367FC7C02CD6,
	ThrowHelper_GetInvalidOperationException_m16E3F279B21CF83224DBCDDCD9CCA52242A3DA5B,
	ThrowHelper_GetResourceString_m790996CE2B876233A019A3880DBEBF498F2AD843,
	ThrowHelper_GetFormatException_m7843E7FE6ECC25CA77D86E9A38B13D6526E31CB5,
	ThrowHelper_GetFormatException_m0C1014E4627199E87C62C7C95383E92136C2B716,
	ThrowHelper_GetFormatException_m8F999978C10BB03985DD61295AF41DF352F95179,
	ThrowHelper_GetInvalidOperationException_ExpectedChar_mFF8F998633112C890EE11CD32DE6632557B487CA,
	ThrowHelper_ThrowNotSupportedException_SerializationNotSupported_mF73201276822CF6CC001E95CBA48CD83D6109062,
	ThrowHelper_ThrowNotSupportedException_ConstructorMaxOf64Parameters_mCDB2E3AC4FFD00386CBAA2DEFDD8AE1CD5BA4F1D,
	ThrowHelper_ThrowNotSupportedException_DictionaryKeyTypeNotSupported_m27903431590452F80C7758007DFD55D8B7A7E611,
	ThrowHelper_ThrowJsonException_DeserializeUnableToConvertValue_m96FE8F452BB614BAB85A7E20BA40E22690C93F7B,
	ThrowHelper_ThrowInvalidCastException_DeserializeUnableToAssignValue_m4C68F1D7ED3EDC35AFC765960D3A679E34C51630,
	ThrowHelper_ThrowInvalidOperationException_DeserializeUnableToAssignNull_mCBADC1DEFC6B20B8679C4EE5FE83BC5098203B9A,
	ThrowHelper_ThrowJsonException_SerializationConverterRead_m4A28E5B4CC04FD16BA7CAA84D1B021F92EE220D7,
	ThrowHelper_ThrowJsonException_SerializationConverterWrite_mD0997F6FA916D1E612AB3C737A23612AD9297164,
	ThrowHelper_ThrowJsonException_SerializerCycleDetected_mAA15526A580E83EC7EDA47D50598EBD0D0D8E438,
	ThrowHelper_ThrowJsonException_m973F42F766807F7E7BBC203E354BA06CA8EA055B,
	ThrowHelper_ThrowInvalidOperationException_CannotSerializeInvalidType_mD2FD606E0497CF4BA9029A955923E121356C04D3,
	ThrowHelper_ThrowInvalidOperationException_SerializationConverterNotCompatible_mC298E0922D7F2B7B68B13B4D3E5743C766D8F84D,
	ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid_mA2E9DC408E16194EFCECD1A1371C32DE747E1339,
	ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible_m6065CAB5382ABA2091B10F92E69FF90F37C59BFC,
	ThrowHelper_ThrowInvalidOperationException_SerializerOptionsImmutable_mBEFF1850BBA433ADD5B20540B3C7594BEB6ABE0F,
	ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameConflict_mF30BA34FBED9BA4C3F4F4CA6E834080C5AFE1CB3,
	ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameNull_m9023D53865B4D6A638CC630D5D77584FE074E8CC,
	ThrowHelper_ThrowInvalidOperationException_NamingPolicyReturnNull_mB9EB953C12D35F4E93345C912DBF23DB636451D5,
	ThrowHelper_ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull_m5F731D2B8C69BD2CA0359516616C34D5ECDB80D8,
	ThrowHelper_ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters_m05E79160F6B9CD59D30D8906BB2987EED6954601,
	ThrowHelper_ThrowInvalidOperationException_ConstructorParameterIncompleteBinding_mB139EAC290271F39E1736F7F1E9E2FF9B14C297F,
	ThrowHelper_ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam_m4872A1B0FF96357B0C6B638C4EBA9E1761880D2E,
	ThrowHelper_ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid_m603A1316F663B20F69E80D9E3811B452E919570E,
	ThrowHelper_ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid_mB9A9F19F8AF43DC125EA07D32794D98B98F19E20,
	ThrowHelper_ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid_mFC629CF3501D8B07CADE0DB5D3A96AFC3A8D517B,
	ThrowHelper_ThrowInvalidOperationException_ConverterCanConvertNullableRedundant_mD154A895AB1495392DCCBA759457612D5A7C6E66,
	ThrowHelper_ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored_m5EE618649BD8D2CBB732C6F0292DC3C4E0DFF91F,
	ThrowHelper_ReThrowWithPath_m8C8F65479D5823F1F0A53F62D329626CB6590681,
	ThrowHelper_ReThrowWithPath_mF03F4EF489A65B6DBC1494B4DC06317EE6C4A17A,
	ThrowHelper_AddJsonExceptionInformation_m27AA11CA530AE35B24E4AD7E1BF42051046CDD75,
	ThrowHelper_ReThrowWithPath_m60D724FB73CD33130D2CF820A08AE5332F7AFBE8,
	ThrowHelper_AddJsonExceptionInformation_m19A08342AAA7D86A83B9A636BEA522C2F2232918,
	ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateAttribute_m6431717BAB38FE1C8D3A06DF615F73301FA5125D,
	ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_m5D557E2F44F1A96A25337111B7BC2F1F6D0F5AF7,
	NULL,
	ThrowHelper_ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid_mDECC955E535EBF5416567323A734BA671818A50A,
	ThrowHelper_ThrowNotSupportedException_mE9B6B73900691A34CF493C9F28D268C7E908B8D3,
	ThrowHelper_ThrowNotSupportedException_mFBBEF762422B3346268279581FA1372DE9B433FB,
	ThrowHelper_ThrowNotSupportedException_DeserializeNoConstructor_m0316A2404C45BB2BBD6532E76FE9F384DB113010,
	ThrowHelper_ThrowNotSupportedException_CannotPopulateCollection_m18F801EC086A905E56F9380CFA8E011746E95DD1,
	ThrowHelper_ThrowJsonException_MetadataValuesInvalidToken_mDC0FA9D497DFC575CCFD6422C843790C64656AEB,
	ThrowHelper_ThrowJsonException_MetadataReferenceNotFound_m07F016A3E7895EC7753C09442854C4950A77F142,
	ThrowHelper_ThrowJsonException_MetadataValueWasNotString_m9867E87D56EF64AABEE3844B4929587C5CB49A22,
	ThrowHelper_ThrowJsonException_MetadataValueWasNotString_mC0D5A8277188661CECD17D5BAF5E7934D691DB34,
	ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mCF9676414315C513337B5E8644F0A4927FD99F75,
	ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mB36E6C2A52F6DBEDC8F30D9FCAF626F29A8B1399,
	ThrowHelper_ThrowJsonException_MetadataIdIsNotFirstProperty_m845C5039FF51C4DF91792496C8A5A660AC75BC07,
	ThrowHelper_ThrowJsonException_MetadataMissingIdBeforeValues_mE063C70456CE239D60E25F7381022F36523B764E,
	ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_m338531C4877DDB86A6298075523EE839FD80D4E6,
	ThrowHelper_ThrowJsonException_MetadataDuplicateIdFound_m168ECF07F07194EAC525711DE4894FBF0BF44C59,
	ThrowHelper_ThrowJsonException_MetadataInvalidReferenceToValueType_m6AACE011A6BBB6D2092908770A52F33F538FE0F3,
	ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m5DDDF08403852837CA5A82CD77FA084374975BF8,
	ThrowHelper_ThrowJsonException_MetadataPreservedArrayValuesNotFound_mC760A54801B7D5833BB0BBB73CD9D331137C2DB0,
	ThrowHelper_ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable_m2541C3387E3799EA00D221A1E9D96E5748B4C2D5,
	ThrowHelper_ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType_m59D2BC0FFD79A685E8D72FA1ED73341DCB4828D2,
	ThrowHelper_ThrowUnexpectedMetadataException_m66CB72849A9036C64AC1B6C05F19E46C0277F47C,
	BitStack_get_CurrentDepth_m89D98204DBF12922D8C94B3447F28558A607561E,
	BitStack_PushTrue_m3E753BB8ECFB2193C4F9097FFBA0FB9C502DCCC7,
	BitStack_PushFalse_mDD8A80022173EAF25EC74EFF7CFA03A6054F8D25,
	BitStack_PushToArray_m20AF975C1EFDCFAA0D410D815A8B062A9CCECA23,
	BitStack_Pop_m65E1670A3A560768AC7DD19C52A9D5B8C4CE2482,
	BitStack_PopFromArray_m205D4F9EEB5B51B6074124CEE2F8974AA2A950DF,
	BitStack_DoubleArray_m589E01AA096E64AE9187C458D962BE8CA0E92051,
	BitStack_SetFirstBit_m9F284C76F69FBAE879387EE0B8B190B6967840BE,
	BitStack_ResetFirstBit_m6A4CD054DF57522D30866089E313E8353A6889A3,
	BitStack_Div32Rem_mBCFE7A3E6E7485C5BCBD8CA7E0FCC4E55AE5FA5C,
	JsonDocument_get_IsDisposable_m32674A8E6A4F43BAAB12D25D7AB4B1F8202D6337,
	JsonDocument_get_RootElement_mC37C0BB4BB812EDF790A637056A706B018B47F7C,
	JsonDocument__ctor_m7E52FD9EBA1C3226C56E20AA0DA4B202D1242B03,
	JsonDocument_Dispose_mFDC6DAE4A8A489669A9E94D725E44011386516C3,
	JsonDocument_WriteTo_m4F0091C2294C11E823F07D253C25BA2DD720FDB1,
	JsonDocument_GetJsonTokenType_mA77779B2ACF3378B18E237FCBE42EA20CE1F9E54,
	JsonDocument_GetEndIndex_mCB49D234EB50A854CC608B35B030D317C6478A47,
	JsonDocument_GetRawValue_mE28C290DD5A1C03AC1F21BAD7D18F84C9CB60664,
	JsonDocument_GetPropertyRawValue_m63CA89B4668E2CA7CE2871C0FEE4059D84CA7487,
	JsonDocument_GetString_mBDDE86EC31F3DFC12B55F90D8DEBBBAC90216990,
	JsonDocument_TextEquals_m010068C6214B0F779FEE248BFE48C776F613794D,
	JsonDocument_TryGetValue_mAFAC3A2D30BB0F9A8257AD2E613700C854D858B9,
	JsonDocument_GetRawValueAsString_m2ED5DAD23249C1D3CA77F67F89B9D0ABAEBC3EA3,
	JsonDocument_GetPropertyRawValueAsString_mBD94E7344807BB331156514E29A7E62153B3A5FE,
	JsonDocument_CloneElement_m7ED18578F6EE37AD3BFC073B91EFB926D3D53266,
	JsonDocument_WriteElementTo_m84378E2AA9142CD0A2CC706AFB40E7EA11AD2EDE,
	JsonDocument_WriteComplexElement_mB3A1E8016B754D9BC465328E18AA16CE12B208D9,
	JsonDocument_UnescapeString_m2DF634F6A71C280B6D0B94CDFFA6B1889640FBA5,
	JsonDocument_ClearAndReturn_m3EDBAFCD9DD7E564BB515F4605E70E1E2F64BC95,
	JsonDocument_WritePropertyName_mD6770EEC4513C8606F5A55BFD29B59FE9B8B486F,
	JsonDocument_WriteString_m97C83763877F4E016B011EBD62E8B7D975D2E6C1,
	JsonDocument_Parse_mFC0B173CCBC2F2ADD9040336FCB3B0D8AA8F743E,
	JsonDocument_CheckNotDisposed_m68165D4ED110E314C9A451131C85C5FB543BEEA1,
	JsonDocument_CheckExpectedType_m93425EFAD920FE044D8B62494000B3DD1258FBE6,
	JsonDocument_CheckSupportedOptions_m296FE175CC445F992C47DAD6ACC0C6B31C5D490D,
	JsonDocument_Parse_mF51C731F472CB28C445CE4FCD0F9D6D7ACBAA2DB,
	JsonDocument_Parse_m0C451D2F8DE35FD2D93E4F76D8DEB3FED0784504,
	JsonDocument_ParseValue_mAFD1AF7093FB6B8974C2C4ECC111F8438E0DA731,
	JsonDocument_TryParseValue_m376E476DA7C90522D73A2BCA568CC00ECD2EBBCC,
	JsonDocument_Parse_mDCD23B7F0D42C0481828DAA5ACCDEF6DFC858BE1,
	JsonDocument_TryGetNamedPropertyValue_mF6E1B17BCA05976B537A781DFB887D83B2C1686A,
	JsonDocument_TryGetNamedPropertyValue_m17E846EC263CBC7083EFF948D0A71BF4D5B767EA,
	DbRow_get_Location_m64A1FE8677C03A2ED3384727229059262AD3F149,
	DbRow_get_SizeOrLength_mD035DFC369AC701D3930C33A159A067B00913772,
	DbRow_get_IsUnknownSize_mAB1898B5C44A75E8F3F5737B39E22A9829052B9E,
	DbRow_get_HasComplexChildren_m7FB54FB106AA84F5168A030E00E21B2687C58D09,
	DbRow_get_NumberOfRows_mB356515E008E677E4E79834F315165BD49A825DF,
	DbRow_get_TokenType_m25D3999BC52C9991FC22A97FEC677997068D1E7B,
	DbRow__ctor_m83855B245AEDA3542D69D4181E26D625780729FF,
	DbRow_get_IsSimpleValue_mF9C4D4A9AA674E10C15D23BFDCEE3B7A9CE78638,
	MetadataDb_get_Length_m358C82F2ACD118F1A31750A408DE4A80D9706AC1,
	MetadataDb_set_Length_mDEED679131C22B2B98BD28E71CABB539B01E79C7,
	MetadataDb__ctor_mF5690ADF4EA9661465EF315D0505E445C6E222FB,
	MetadataDb__ctor_mADDFB275C40C9946CEFB568AE37356AEB5FBE14D,
	MetadataDb_Dispose_m65983FCE3C671C0E078BA5B7192D66EA6224D218,
	MetadataDb_TrimExcess_m4BAD9F80840A78E601E56F2EAE6822A02267A1CD,
	MetadataDb_Append_m70BEF3B5C094C35F03E20E23CABEF3905616B61A,
	MetadataDb_Enlarge_m1D564D4B6A73CB588BBACAD06E20CAC2CC87BE91,
	MetadataDb_SetLength_mF874FB9D92FF15E65645B752D2F16693FD85EA53,
	MetadataDb_SetNumberOfRows_mE9A9288F6CFE360E85CC8E0A50AE964D43C470E2,
	MetadataDb_SetHasComplexChildren_m23B89D6C169828F97B1138999C88AD2C334E46D9,
	MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m93260D165FFDE321BC10AEB51ADA16EA571BDA92,
	MetadataDb_FindOpenElement_m764A0CA02C9B76F9FB4501BEC77D157A87ADCC8C,
	MetadataDb_Get_m54EC69D0C0A5AFA94790823E043C9C5AF611E879,
	MetadataDb_GetJsonTokenType_mFE77CF70D471447224C7088BF78347AD9922BDD8,
	MetadataDb_CopySegment_mE2A805BC0A4A35436649F6DFF69A286C416A056C,
	StackRow__ctor_mE83A64F557423B2C1417CF2CAA47FCB6DAE79597,
	StackRowStack__ctor_mC2D0A937968645C04762AE37C6F9196811FC60AD,
	StackRowStack_Dispose_mE69DCE4BA09A561767066C061517880FC9FD240E,
	StackRowStack_Push_m2EB6DF0A8051783F40F7911AC981072DB6DF3AA0,
	StackRowStack_Pop_mB7727C6BB4EF6384ADB6A055F63AE463231B3731,
	StackRowStack_Enlarge_m9CAB452BCBA0F7795608ED801F3741F645037CF0,
	JsonDocumentOptions_get_CommentHandling_m250724FEAA7D45E779F026A8E2AE1792E8CC0223,
	JsonDocumentOptions_get_MaxDepth_m910F496990C5F7315734B42E49D6B46D7D7FB146,
	JsonDocumentOptions_get_AllowTrailingCommas_mB695E7244BD9C08298D2A980CE3C8D6C69D0F2ED,
	JsonDocumentOptions_GetReaderOptions_mB45CDABD01DCD171F5576B841FD948C183A3F4BC,
	JsonElement__ctor_m08BBC84BCD6BF2156EEA51E7E367E772C44E8C9C,
	JsonElement_get_TokenType_m2DE21D16B1E01312F72BCD8C9E6EABB3C4CDD7B8,
	JsonElement_get_ValueKind_m34757B769FC468837C793AAB42507082105A1AE7,
	JsonElement_GetProperty_mF3F895EADD2EBC27D13528083FBA7A8E754C02B9,
	JsonElement_TryGetProperty_m1F48BA59A2FF4DD5DEC6B04E2DFEF7785CAB89DE,
	JsonElement_TryGetProperty_m915535CFE0D507A447594BD147BE125D20EE0B88,
	JsonElement_GetString_mC81CE313F3CF6ECFCC6374545F7B598A244FA53E,
	JsonElement_TryGetInt32_mA8E8A07BDB3F2CD9315116A391DA2F31023EA506,
	JsonElement_GetInt32_mE298927691DCC4168662962C7B7FF3749003A5D6,
	JsonElement_GetRawText_m57611974A95EAA4466054192D51EAC5A1487249D,
	JsonElement_GetPropertyRawText_mE33C03151821E238E589A59D52856D5E796CBEC5,
	JsonElement_TextEqualsHelper_m388909D510BCC8E2EE370A17626A111A77E362D5,
	JsonElement_WriteTo_m1AC8B59D427C19E13F2134BA8964477C9B3E4F20,
	JsonElement_EnumerateArray_mADD6A7BD2664B80DC5FEF55DE964046AADA2AC28,
	JsonElement_EnumerateObject_m25AB68731BDC8CA13253A206EABEA1F219AEA40C,
	JsonElement_ToString_mA0336A268F12CE4735C0307F12F4717EA5A24B29,
	JsonElement_Clone_m2D1217819F1C1A069A7291948BD2CDE7D58B090B,
	JsonElement_CheckValidInstance_m37FCD235BD0B2BF84200713D1CFB2D040A7A8439,
	JsonElement_get_DebuggerDisplay_mD7A4D8389DA4D9A8790C4FDDFEB0A20101CD8883,
	ArrayEnumerator__ctor_mE63EDB3BC6E93B2C7E2A40C824553B06F853E68C,
	ArrayEnumerator_get_Current_mF52CB9638D47BF1F300A8D90DE72F16FE7760A50,
	ArrayEnumerator_GetEnumerator_m84826411DC72414B035DD2383B98995030DC1D64,
	ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_mFA06CBD3D1326936DCA477D78774A7D5244051B2,
	ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m7E27A28429F4B25E812197A40277394C3898FBF4,
	ArrayEnumerator_Dispose_m013242AA10DC5F3B889EF39004D9E62C88484F36,
	ArrayEnumerator_Reset_m4E108783AA0C9538A0723F914B99F999D0907825,
	ArrayEnumerator_System_Collections_IEnumerator_get_Current_m0625474B01CE733D09EC696C2D3943FEBDA9D8B9,
	ArrayEnumerator_MoveNext_m4A4255B7F6ADA0D569E1644355F8542007BC99E8,
	ObjectEnumerator__ctor_mE6CFBEE462A2DAD097E3CF38C5E1F93B3A72575A,
	ObjectEnumerator_get_Current_mEBA991658CFB79D8EC48C940587E8F56E5FCF6A6,
	ObjectEnumerator_GetEnumerator_m6431E7E6BAF73BDCC9B211871E2046D8C5557C9F,
	ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mFABC61E9837E42D4B98EDD2EB829169691151DA4,
	ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m152A46470709CCB7745C15E69C8B5E3CC379F292,
	ObjectEnumerator_Dispose_m05CE64A63964F3D868775EC395EF5AFCBFD0C772,
	ObjectEnumerator_Reset_m2375949AFCB32D333217A2F8674E48BF3A948DC7,
	ObjectEnumerator_System_Collections_IEnumerator_get_Current_m819E29B32A75D77D74B6CA9F65F28BDFF7C7E6BC,
	ObjectEnumerator_MoveNext_m9EF4007222C1BFD3D5BEF8CC03A76CE222F4D859,
	JsonProperty_get_Value_mF766C4EFFB858EEAD2A009AF55BB8B51095AC96A,
	JsonProperty__ctor_m0C3653F7D2BC6F745C523466CA7294E355FABB13,
	JsonProperty_EscapedNameEquals_m9070FF7B8371466D91AF6A4E38D5699FD5107D86,
	JsonProperty_ToString_m1FCFE27631E5AC6F3238874289B06303471487B0,
	JsonProperty_get_DebuggerDisplay_mC4D1DE967EC567BBE7888EB699087916594E59E2,
	JsonConstants_get_TrueValue_mB15CD35C5DCF01FC1B11FA40C7F5C34150DC3777,
	JsonConstants_get_FalseValue_mEB3BF36A6AF283105DBCFC295BD3C88BC2073F9A,
	JsonConstants_get_NullValue_mC4C9020714D4581E9C5D4FA4C3C9949E5D3CA24C,
	JsonConstants_get_NaNValue_m06A191FDB7B355617B3441931550E5935CCAF23C,
	JsonConstants_get_PositiveInfinityValue_mD891D70B91D72B2B26DA087FCED01D224AF4BCC9,
	JsonConstants_get_NegativeInfinityValue_m74A03D18E26D8E27B5135614B99F320601B52DAF,
	JsonConstants_get_Delimiters_m219BB10D79A42BD53D0CEDCECCBD7027DD7D67F5,
	JsonConstants_get_EscapableChars_m670D8AA1BEAFE60E810633D7E39DEA0F3307FB65,
	JsonEncodedText_get_EncodedUtf8Bytes_m90590ADF42E78917035B3FA5FE2B23661DBF945B,
	JsonEncodedText__ctor_m0EF268C655A1728E129E94D60BAA71894F1BD77A,
	JsonEncodedText_Encode_m48F8D41B2ADAECA21D3B959F62C2D08A73A1F317,
	JsonEncodedText_Encode_m670B6211FE6F2601D2CA63B0065AE2C71DD11A49,
	JsonEncodedText_TranscodeAndEncode_mE4216E009FE93DF4778A8C7CA21140158BC9D454,
	JsonEncodedText_EncodeHelper_m9F749FCD42E83877F9FB197707C3D41464925821,
	JsonEncodedText_Equals_m122DC2FCB6321FDA3F63E1973CF385FBB3C9FB69,
	JsonEncodedText_Equals_mCE77AC6B7E663C8B7D8412B394E91F715A5BE682,
	JsonEncodedText_ToString_mEC405B95F27BDBC3814C264035F3B0151621800F,
	JsonEncodedText_GetHashCode_m2B447D2F286EC689D45FA77B4ECCD9DD072D2A70,
	JsonException__ctor_mFABDAC7A4D8A6D06955153DAF9055EAEA99ADEA3,
	JsonException__ctor_m332A4F36C16574E65E0E110226C3475D0A19C78E,
	JsonException__ctor_m65DC9A06A2D3B15D7DA72DFFBA5430648A864BA6,
	JsonException__ctor_m141AAB105C01DB804B3BDA2C65D2A9EE9B6BBDBB,
	JsonException__ctor_mAD45F3B7D522764CEECCAEF24E9BE96A3E930065,
	JsonException__ctor_m4A92F38705B569451DA8C63F2E1AD5375DF6E8CA,
	JsonException_get_AppendPathInformation_mE4D721DFC493525073DDB220BA2A1499C1C08362,
	JsonException_set_AppendPathInformation_m30D14D80A9F43FB975791CEEBA7B728E6A18EA6A,
	JsonException_GetObjectData_m82F05FF628ABF99E30D9C8581FB85D37EE9134B7,
	JsonException_get_LineNumber_m7D21C9A5A7431696FB3B195690A1532E46DC4601,
	JsonException_set_LineNumber_m60FC0BA8C1BA4D775C58DC3657DCEEAA3B9D6631,
	JsonException_get_BytePositionInLine_m74C4E152BDFC2439E479ED098CEF728C74EE713B,
	JsonException_set_BytePositionInLine_m6CAD5D3CB0CC4F9638C91C71B8E0A44DFEBF9569,
	JsonException_get_Path_m1B60B637D1DCB55E79E0F91381230144DC3DACA0,
	JsonException_set_Path_m2ADEFDBC15D75EBD73C8DBCB04C21DA8717B571A,
	JsonException_get_Message_m7FCB5F5C5C8587DE96DD591EE60CF610AE367AD7,
	JsonException_SetMessage_mC33D6F67599831B007961369D4EF48C84030527E,
	JsonHelpers_GetSpan_m9ED27DE6D38D4D2F3CFBAB1F52898CF2B2BA407F,
	JsonHelpers_IsInRangeInclusive_mB8422E5BCAC689DE6E7C2EDD3EAE62FC2BF4DFCA,
	JsonHelpers_IsInRangeInclusive_m6426AE714324BA64C7D679371A81961A23E320B9,
	JsonHelpers_IsInRangeInclusive_m7901E58F8EB54BCA5EED278E203A2312F0903B83,
	JsonHelpers_IsDigit_m84481DC035BF769A6F43A43C39850A671308651A,
	JsonHelpers_ReadWithVerify_mCC9DD465FF6CCB7DAF613B7D203956794D5D9939,
	JsonHelpers_Utf8GetString_mD0CD323CF639EF33135BAF90D8440790CC9A84E6,
	NULL,
	JsonHelpers_IsFinite_mCECEA683C0C204194A519BDFF08286B66301D46D,
	JsonHelpers_IsFinite_m4BE9A83B2285048612AB306BFDD96BF78437DF5C,
	JsonHelpers_IsValidDateTimeOffsetParseLength_mE27A9B53F25CF9EAA7591CEDAAD22C2C82AF4321,
	JsonHelpers_IsValidDateTimeOffsetParseLength_m630C990EC673B6E444096CEDCCF0A9C6A480A8F5,
	JsonHelpers_TryParseAsISO_m4765D0C0BE547CF469AD6D899020B2A8707101BA,
	JsonHelpers_TryParseAsISO_m7192F4EA6AC1052851402A3EC32D42FD2AB3511B,
	JsonHelpers_TryParseDateTimeOffset_m1A4C13DB22C35EF04F81401B4D8AA23395665B01,
	JsonHelpers_TryGetNextTwoDigits_mDE72FD732E4B06BAE1CECB89545C5F30AE06AB06,
	JsonHelpers_TryCreateDateTimeOffset_m72E9B922F178306D098E0118B105CBF236053198,
	JsonHelpers_TryCreateDateTimeOffset_m5AFB3B83A75ADDDCBA3F7C3465AF477270FF06E2,
	JsonHelpers_TryCreateDateTimeOffsetInterpretingDataAsLocalTime_m09E17B89B063C875CAF7107F03D24750655BAB06,
	JsonHelpers_TryCreateDateTime_mDB77646E3C77A97C22CDEBAA2D7740C8B4173058,
	JsonHelpers_GetEscapedPropertyNameSection_mE92CBBE2D3CAFFB05DC77BF57868783D479FDED5,
	JsonHelpers_EscapeValue_mD3344723EFD2DBCE9934B2539023282D8D64E9FB,
	JsonHelpers_GetEscapedPropertyNameSection_m399DA9D2CCE67FAA15755BA1E0276A1D32DDF6A8,
	JsonHelpers_GetPropertyNameSection_m7A5442B54707D2DA49847EC771C381A404EB7147,
	JsonHelpers__cctor_m7BB93DD74A8C950F6C02B4F9F3E1710BB6A92B2A,
	JsonHelpers_U3CTryParseDateTimeOffsetU3Eg__ParseOffsetU7C21_0_m088E2588D6A3243BACA89CEF31560FE6818CC879,
	DateTimeParseData_get_OffsetNegative_m0D054B07A34B50D970C5751B503F342C14F5F3E9,
	JsonReaderException__ctor_mA6CFB4B33BBD4A66482E0949B37FC965356A347B,
	JsonReaderException__ctor_m9F7695A2E888443A119FBCF35B88AB7F48DB38A8,
	JsonReaderHelper_CountNewLines_m1678FE5DF2E5BF871EDDA733812F1F2278EDC14C,
	JsonReaderHelper_ToValueKind_mC5065501F4DC87FF00ED3C35317E53E604B4DC5D,
	JsonReaderHelper_IsTokenTypePrimitive_m2241739D3A5713388C3DB156A7B8075092C45DF9,
	JsonReaderHelper_IsHexDigit_m8A8A0702FD1A088E1CB8D44EA2CA0F1E70255F75,
	JsonReaderHelper_IndexOfQuoteOrAnyControlOrBackSlash_m1465C1D49863A0D9DF2D893FCEF23416D6736AA4,
	JsonReaderHelper_IndexOfOrLessThan_mF446B6FF9A622A7E5C26B2D1C51FDCC31E45B4B2,
	JsonReaderHelper_LocateFirstFoundByte_m5325ED5EC0B3BBAC5EBCD5C368019DE870CE3C5E,
	JsonReaderHelper_LocateFirstFoundByte_mA8C087A129D89A6562F715DC98C63E1618F56812,
	JsonReaderHelper_TryGetEscapedDateTime_m12A7F2DCE9D1E7A263885D163C07C83B088D7794,
	JsonReaderHelper_TryGetEscapedDateTimeOffset_m8EE8EE332C8B35178CBFE539A4AB3759B2780DB0,
	JsonReaderHelper_TryGetEscapedGuid_mC0F11E2356D2E0CA3F78CD92275C0EA47C6C66DE,
	JsonReaderHelper_GetFloatingPointStandardParseFormat_mB0392AB52B46FF8C8CA58BD748E6AB175F251CA0,
	JsonReaderHelper_TryGetFloatingPointConstant_m600E0F6BA37CD6BA8A4AF81C0BF3B99430EC6897,
	JsonReaderHelper_TryGetFloatingPointConstant_m82834D401884E26CA0A51A376C0C813BDD6D9A47,
	JsonReaderHelper_TryGetUnescapedBase64Bytes_m6B842CBF6259E1588D3161286FA663515A9512A4,
	JsonReaderHelper_GetUnescapedString_mFFCD9FDBC8F93BDE37608AED6C79A58265B65DDB,
	JsonReaderHelper_GetUnescapedSpan_m4B1158FDA6985C88988F6B3727E0C8908CDC5B5C,
	JsonReaderHelper_UnescapeAndCompare_mFF06C4450588353306D1EEA42EC36ADA7CB253B3,
	JsonReaderHelper_TryDecodeBase64InPlace_mB9FB71368E0C656AD14ED4EC3AAF8C15A8557610,
	JsonReaderHelper_TryDecodeBase64_mCB66A113B2B0E3D25218985C4F68DEE3BEF4D91B,
	JsonReaderHelper_TranscodeHelper_m52E43E75CA9B5AA0A22814B86CD1F5B1D8FFAA9F,
	JsonReaderHelper_GetUtf8ByteCount_mA812505B2257B2693CC85B6AED55950D496B0AC8,
	JsonReaderHelper_GetUtf8FromText_m5B178FE06DF9C8F8E5438AC4F16A0FE7BA01476A,
	JsonReaderHelper_GetTextFromUtf8_mD404489C2197250EA489A1EC2363A946B5B4CC39,
	JsonReaderHelper_Unescape_mF575929C5DB64F96EFCDDECBC76271E167B8CD91,
	JsonReaderHelper_EncodeToUtf8Bytes_m89BD0F513CF493D9F5FB4E22565C1D2FAD986F1B,
	JsonReaderHelper__cctor_m9D2E150F807D4483B2C86FBD3DCC6EA57C1A1A7F,
	JsonReaderOptions_get_CommentHandling_mC784945C4A7D8BF00627E0D71CAF144E059CA45B,
	JsonReaderOptions_set_CommentHandling_m840CCCE2E2296122C6AC1E8B8E75D1EEB54557E7,
	JsonReaderOptions_get_MaxDepth_mE9E196FD22578986617CBC0B6104934965E7DE18,
	JsonReaderOptions_set_MaxDepth_m7CA249F02B7EFB090EBE8DD1A81134BF15890127,
	JsonReaderOptions_get_AllowTrailingCommas_m6BE15A1BB0597C203011627DCF4E9CDDB42049BA,
	JsonReaderOptions_set_AllowTrailingCommas_mB8C99444AB5B8A9D50FDDD8E2CDC8F473B6D0641,
	JsonReaderState__ctor_mBF6D934854F0D3489AED812C79FD31BD7DAEDB21,
	JsonReaderState_get_Options_m66769D6D489C3E79567B39C83DD8464073804370,
	Utf8JsonReader_get_IsLastSpan_m7B992A026074DDEFE7B18BBAF1D35F7FCC67307D,
	Utf8JsonReader_get_OriginalSequence_m695F93AC97FC88673CF1C5B42CB2781FCC421DF3,
	Utf8JsonReader_get_OriginalSpan_m5CE4EC409D428FFA49D657B1EB8095A5A1BF75FD,
	Utf8JsonReader_get_ValueSpan_mBABD1CCB678681EF0BAB792BD92B1A667B5280EA,
	Utf8JsonReader_set_ValueSpan_mCB290F34DBE06C916AC61FCD97325363E6DF8840,
	Utf8JsonReader_get_BytesConsumed_mED511A2A33941953398E81C2630928B1BE296C60,
	Utf8JsonReader_get_TokenStartIndex_m065C1E55350901EE451793D91D45C6BFD6727825,
	Utf8JsonReader_set_TokenStartIndex_mD8DE6AC3843B60206928FF65843B1063725E11D1,
	Utf8JsonReader_get_CurrentDepth_m27479A89058293B9D65E5245D4288745BC187941,
	Utf8JsonReader_get_IsInArray_mA7603C29F57D874052CDED9AC19EE23B18786A16,
	Utf8JsonReader_get_TokenType_m589FFFF94F1DF061F5BAA87791CE6EDC77B153FC,
	Utf8JsonReader_get_HasValueSequence_m0E4BC52E674A56D3F8FD1EF9F56BCA86F7851658,
	Utf8JsonReader_set_HasValueSequence_mA04C744A5B061CF11B292F4FA30685D8FF959819,
	Utf8JsonReader_get_IsFinalBlock_m044741A01CC206E766FFC12FC40D20F5F3D20C16,
	Utf8JsonReader_get_ValueSequence_m8BFD79D1CB26B6B8C081CC01F855EFD3A48E74F3,
	Utf8JsonReader_set_ValueSequence_m14F871806DC91F89B691C08FDFDBA71DB941C6E4,
	Utf8JsonReader_get_CurrentState_mB19357F20C4CBCFDD92278B4D2AEF65D82ECDA5A,
	Utf8JsonReader__ctor_mCA5FF66B2E66D867A4F30167AADF35FA7B66D00F,
	Utf8JsonReader_Read_m48C98153390FA527AB0DD5449555BB69A2881816,
	Utf8JsonReader_Skip_m5E206D71CC77C21D2329A9F809AC53367CC74091,
	Utf8JsonReader_SkipHelper_mBD2F0EAE5EFE06BA861442584F998D344B9D6316,
	Utf8JsonReader_TrySkip_m30F2CB8BDCDD32BCE7C345A84398E3ED5AE38AC5,
	Utf8JsonReader_TrySkipHelper_m403637E1D8D991978EC5C7D0D45B7AD39203E865,
	Utf8JsonReader_StartObject_m80A9CBB11BC789B32FD92F9D951807CDA73F88EE,
	Utf8JsonReader_EndObject_mF882CCEFFE2075AE1BA7A8B82F787F3472ACD900,
	Utf8JsonReader_StartArray_m9DC35176A3D6D63A0F0CE6E12EF81057795E5B2D,
	Utf8JsonReader_EndArray_m595FA16248A8A07CA4B68CF12789592F941E1149,
	Utf8JsonReader_UpdateBitStackOnEndToken_m5681C904065EB11EBAB2CE8E3FD2E0323DEBDBC7,
	Utf8JsonReader_ReadSingleSegment_m4D6BE7EA668855AF1A3DC170E54996088B0C08A2,
	Utf8JsonReader_HasMoreData_m0849ADA8E4F15E0C12811A4A1144D964F7AED2DD,
	Utf8JsonReader_HasMoreData_m1E2DEEDB648DF43DB37BC3044E8D633D16FB076E,
	Utf8JsonReader_ReadFirstToken_mF7DA287E1C8E5B7C1C885339DC4C1DC3E48C38F6,
	Utf8JsonReader_SkipWhiteSpace_m7E57D7BF978E4641391C9516F38820A2CE0556AA,
	Utf8JsonReader_ConsumeValue_mA7A960E791A5055383C64E7DD9AA669E71115799,
	Utf8JsonReader_ConsumeLiteral_m2A3B4794861C4EF32D8370B2D73C6412000660FC,
	Utf8JsonReader_CheckLiteral_m0906217EC5F76E1683DC47AFE73B4B56126791EC,
	Utf8JsonReader_ThrowInvalidLiteral_mFE5C045DA40FA4649AFE040AA0C332C753D8AFC5,
	Utf8JsonReader_ConsumeNumber_m4B68EF9DC535940BA5B09F601BE7A469C25482BE,
	Utf8JsonReader_ConsumePropertyName_m4D54B25BFE01BFFF5976F17D7A9D5D7C166EACD0,
	Utf8JsonReader_ConsumeString_m0A2F49639EADB6BE4495CA600CED86A892F3EC82,
	Utf8JsonReader_ConsumeStringAndValidate_mD3DCAF6E986C245047B6B08FFBB149269E1941E5,
	Utf8JsonReader_ValidateHexDigits_mEB6781B13A74BC4EE94ECEE64A8362067945893E,
	Utf8JsonReader_TryGetNumber_mA537AE8E9D9E0C1AA6EB929872959D9E0D1C1AAA,
	Utf8JsonReader_ConsumeNegativeSign_mEAB4DACF3FA5003223B06958EB7BA236E6D3CD57,
	Utf8JsonReader_ConsumeZero_m237EEC547BCEE5ACC2FA5C371DFC4C2F8D7C1184,
	Utf8JsonReader_ConsumeIntegerDigits_mD3C9A55484E0188A9DF616C6C43CDC7332FD166E,
	Utf8JsonReader_ConsumeDecimalDigits_m32313C386676761CA3EDDB3F80EAD756C8B0E76D,
	Utf8JsonReader_ConsumeSign_m9D31DED0190794A96D6BD43EFD2D01D4FB326F12,
	Utf8JsonReader_ConsumeNextTokenOrRollback_m734D5BDD909FE3FC93D2D62FC823048553C28BF3,
	Utf8JsonReader_ConsumeNextToken_m3CA8B80E08B12A2E034EE6913DA5F8185A32361D,
	Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_m6B24CB081250AF44DCEA9938616E206978FBE8BB,
	Utf8JsonReader_SkipAllComments_m4B68EAA87BA8197DFB88BEA53B67B7C5D5E5BF1F,
	Utf8JsonReader_SkipAllComments_m0626448D38FC1F68C36361A5CE32F753E3C3A9B4,
	Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m9EBCD5E4D2F5B96282EA227FC60DECFD12DC322D,
	Utf8JsonReader_SkipComment_m91E3271B505861F135B1F7CBEA89FF9D03DBFEC4,
	Utf8JsonReader_SkipSingleLineComment_m080A7403DB593F785293CEB04C935F97608A47A5,
	Utf8JsonReader_FindLineSeparator_m1A63CFD96D293C6851A62C87C0829B08268CC50D,
	Utf8JsonReader_ThrowOnDangerousLineSeparator_m9DB458C5580C40BD100AC7993DFC3445EA6D1DCC,
	Utf8JsonReader_SkipMultiLineComment_mDDE267D4B9BCE906A04E91290B75E2E69C943839,
	Utf8JsonReader_ConsumeComment_mDDA4B89EB77DF4767A97289E5AE1A45C29875605,
	Utf8JsonReader_ConsumeSingleLineComment_m6820206B83C4C8FB1F2D85DA65DA783F0C4B0848,
	Utf8JsonReader_ConsumeMultiLineComment_mDF378938E725595AE26E07EBC9448EBC122BF2A8,
	Utf8JsonReader_get_DebuggerDisplay_m90465723303F2563AD6293AD79277A5101A811C8,
	Utf8JsonReader_get_DebugTokenType_m5C7DEDDF27CDA9E961186CA3261276037A38961F,
	Utf8JsonReader_GetUnescapedSpan_m2180AE402D0F1D887B57335433E1B98A8F6A0030,
	Utf8JsonReader_ReadMultiSegment_m7A789B154E6126292F523C039F86A28936652DA7,
	Utf8JsonReader_ValidateStateAtEndOfData_mE44BD918232309740E30D9A285F6F63A2EF5E76C,
	Utf8JsonReader_HasMoreDataMultiSegment_mB2C1C0751141A6B4B150D6A1F310560936C69C18,
	Utf8JsonReader_HasMoreDataMultiSegment_m71B085F039270BE038C5F171206D7F198AB21FF5,
	Utf8JsonReader_GetNextSpan_m2B9D198EE611302CE973A7E6268D835C5164553B,
	Utf8JsonReader_ReadFirstTokenMultiSegment_m63D211F1A922E4DD4BC8A801BCECAC0CCEA6267D,
	Utf8JsonReader_SkipWhiteSpaceMultiSegment_m11BAAD269031D687444204D40BB0EA7C6E2D9427,
	Utf8JsonReader_ConsumeValueMultiSegment_m913D0F4B616A77E94761359A3617D2EE8102A95D,
	Utf8JsonReader_ConsumeLiteralMultiSegment_mBA1B229E2B0682719CDC963E68E9E8EF203D9610,
	Utf8JsonReader_CheckLiteralMultiSegment_mBD77DA207039A439C116B70313DA1C1EAE1E79CB,
	Utf8JsonReader_FindMismatch_mFC922E786DE159AC6D606BE3AB07F15BA6743BD0,
	Utf8JsonReader_GetInvalidLiteralMultiSegment_m60F4782DDD6F88D7898813D5C0468DD1A1BEAF55,
	Utf8JsonReader_ConsumeNumberMultiSegment_mE723026EE74A4624867675A37F7C8E267998EE3C,
	Utf8JsonReader_ConsumePropertyNameMultiSegment_m3F71002973F225C8F6FD075BC3C76A506D7BB72E,
	Utf8JsonReader_ConsumeStringMultiSegment_mB24E40054CA1A60B8210FC43CF23EF6F8BE3F151,
	Utf8JsonReader_ConsumeStringNextSegment_m3CCF0CCD09B5856298887E1096CCE6F02A60D0A9,
	Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mA8AD93CE8405F63672B369F6848EC276631D5B33,
	Utf8JsonReader_RollBackState_mE1AB8FF05072C2D1C921F4D97E56122C15E905AD,
	Utf8JsonReader_TryGetNumberMultiSegment_mAB46FB985C042DAF2B0F290A19B221AB58FDBAB1,
	Utf8JsonReader_ConsumeNegativeSignMultiSegment_m135AE9A283C44CD51F7BEFF2A2E0FD9890FCED3C,
	Utf8JsonReader_ConsumeZeroMultiSegment_m281FC6658603949FC90711BE6C81B8C3108B9E2D,
	Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_mF5A5A69C0744D08C722001E1E049D5D1703A9308,
	Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_mE037A2CF96C775C86599C5A61F9E7EEB2C411140,
	Utf8JsonReader_ConsumeSignMultiSegment_m05A9CA258362283F551D2CC8A7B43DAAA1F99D7F,
	Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m8A4377711B0F17F01DE5CBC05DC2A26109647E43,
	Utf8JsonReader_ConsumeNextTokenMultiSegment_m4CA61D5BA79665005FBE0CE4E9694F501351086A,
	Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mA24E85BCB4C5A5FF4C5BCE2BCB0AF267FFF92E42,
	Utf8JsonReader_SkipAllCommentsMultiSegment_mAC8F7760E50129CC670532D1BABFCE46303011AF,
	Utf8JsonReader_SkipAllCommentsMultiSegment_m14037635702E57A5503363E26FD8907DABF504B4,
	Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_mE0992B12538E8587B695ED2D6EA28CBACF2EFE2C,
	Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m050F36798BF507E32B4C696A1BA9DBF566543055,
	Utf8JsonReader_SkipCommentMultiSegment_mF89DEC8E6F0EBCD5B6472A4C37949F9C14C69B97,
	Utf8JsonReader_SkipSingleLineCommentMultiSegment_m010391D904BA6ACBF5FCB5034CB8EE91C9C3E091,
	Utf8JsonReader_FindLineSeparatorMultiSegment_m1517AF51E8F05AD9F0B0E3C8247A737D4756D9F2,
	Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m0CB9C5BCC5C677E851EE114F5AFAA5DC1EE42CDB,
	Utf8JsonReader_SkipMultiLineCommentMultiSegment_mCD3026993722AB7389E158550B086BD01A090C33,
	Utf8JsonReader_CaptureState_m8BBAE4358A88D8BAD2FB811C910F1192BCA21325,
	Utf8JsonReader_GetString_mF6ACA3547007BAAD951423AD47FC7C8FB1EE1C44,
	Utf8JsonReader_GetBoolean_m908285BF5B202C675CBEE893F2B41E9BC5F2806B,
	Utf8JsonReader_GetBytesFromBase64_m0B0E3C7FA55C6B1BF7B6513D80CB4159543F5F67,
	Utf8JsonReader_GetByte_mA2DAAA2A21699AB68FD020A0BC41AC8D3935C439,
	Utf8JsonReader_GetByteWithQuotes_m3BFD4FEF30EBA5DC312DC07D7C6EC9D132B591B9,
	Utf8JsonReader_GetSByte_mF0C15DF1DFAAFED79A5FF6527BC1EC6DF8D6D541,
	Utf8JsonReader_GetSByteWithQuotes_mBA0D6B8A92034B6683EF86A600395EB38129922C,
	Utf8JsonReader_GetInt16_mD10C668BD0F694A704B5A7299ABFDE45B9DFD4EE,
	Utf8JsonReader_GetInt16WithQuotes_m3BA45015843A11D2A9C67FA2B83E83DB0AF69EC1,
	Utf8JsonReader_GetInt32_mB50F3838DE3DC950FC7FFDFC24721A136DA76BC8,
	Utf8JsonReader_GetInt32WithQuotes_mF05F615F6638E5308D86A63939D7639221402D52,
	Utf8JsonReader_GetInt64_m68E1F5DB10DBB2E490FE90D1CDBC1C5E81EFDB8D,
	Utf8JsonReader_GetInt64WithQuotes_mE8901124409E515BBAB819575C5851BD854BE8ED,
	Utf8JsonReader_GetUInt16_m91B334F666BA8952300156AC744D340C01A0FA68,
	Utf8JsonReader_GetUInt16WithQuotes_m2BA11333E17BC23C70A1CA4E90AE7960BA808CB1,
	Utf8JsonReader_GetUInt32_mCAAA27CB167078F0E49C896E0D38F71DA98A1DA1,
	Utf8JsonReader_GetUInt32WithQuotes_m2221571EDFBA7F56DC5E51B2AC8F0318548E6DD8,
	Utf8JsonReader_GetUInt64_m04FC93AD8353898C2E5383CBF9616A0D03DB05AF,
	Utf8JsonReader_GetUInt64WithQuotes_m80FDBEC80D9C007947133F9D259BC28EC239CC53,
	Utf8JsonReader_GetSingle_m65F5DCB28FD9BE75EDBC6C94B20F26E32C3305DB,
	Utf8JsonReader_GetSingleWithQuotes_mA66BC371F801F4DA57D5701543059442BD108597,
	Utf8JsonReader_GetSingleFloatingPointConstant_m37AAA7DD71B9E5379C449141C075EED6C4BDD4AD,
	Utf8JsonReader_GetDouble_m1D71F0D17190B5ADBF720FB13597184F00B83159,
	Utf8JsonReader_GetDoubleWithQuotes_m62F2D7D5B21BCCCA007091EFC60D69F1B6B1A1B5,
	Utf8JsonReader_GetDoubleFloatingPointConstant_m623ADC6DD4A7D49297B7B7306DB045A9B295B91A,
	Utf8JsonReader_GetDecimal_mFEA9D944822478D418E3275FEF14E78CAECE0BA7,
	Utf8JsonReader_GetDecimalWithQuotes_m7421A53B8EDCB1669FF8591DE10A9407E9224141,
	Utf8JsonReader_GetDateTime_mA26D2BA988CF28A40FC962FED1145B29BFE57067,
	Utf8JsonReader_GetDateTimeNoValidation_m4516B03D0EF902C15DFA9BAB9BF057ADC647B6DF,
	Utf8JsonReader_GetDateTimeOffset_mE45024C43B2BE11C35AC5DFA1B247F0C5C888A3C,
	Utf8JsonReader_GetDateTimeOffsetNoValidation_m62B6E29DF9475F352B974BDAAFC391B6133F7A7B,
	Utf8JsonReader_GetGuid_m7A0ECB26F40A0A64EE37C7393F109B3E63193CB5,
	Utf8JsonReader_GetGuidNoValidation_m5DF916F1E98E9F975BF7D48BD9760CAD9771CEC9,
	Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35,
	Utf8JsonReader_TryGetByte_m57093639B039FA045AE217E89A38023A51F25877,
	Utf8JsonReader_TryGetByteCore_m5975171C440BA1DB0F5946CEE3A95AD25E90AA83,
	Utf8JsonReader_TryGetSByte_mF7778E9C90F9D7C8CBE321D9C4B3859CE02B9106,
	Utf8JsonReader_TryGetSByteCore_m8789EC6277347F2746BACBE65069C7E63B3ED6EE,
	Utf8JsonReader_TryGetInt16_m5E65C5335E39AAA608EC42137A1F9909B61C6085,
	Utf8JsonReader_TryGetInt16Core_m489F57E73BB2F417F88E7A98DA2ABB2705B3AD8B,
	Utf8JsonReader_TryGetInt32_m93479A8A49CE3407F4F48F9F3311D8CCECDFC7BA,
	Utf8JsonReader_TryGetInt32Core_mF9C2BAB3D4887F47FD89DE68149E0E4FD95FD7F6,
	Utf8JsonReader_TryGetInt64_m04985C755DD8030B3AC7D8045291994F453D42A3,
	Utf8JsonReader_TryGetInt64Core_m9984ADE08AAFE1243D84F6B9F3F6FF3D59B6D5CF,
	Utf8JsonReader_TryGetUInt16_m018B28F35D595DDDBB3A104F69E2BAB463CDC274,
	Utf8JsonReader_TryGetUInt16Core_m413B4DF3E32855458F822546C8F91B0138F71816,
	Utf8JsonReader_TryGetUInt32_m3568E2AB280AF64148B26E5A570DE313B5F6A557,
	Utf8JsonReader_TryGetUInt32Core_m52DBDE64A8BE6BBFB4AC92E782FA38637E88B79D,
	Utf8JsonReader_TryGetUInt64_mA1BFC2EDB1096EEA85D3F3992A6C33B599769A63,
	Utf8JsonReader_TryGetUInt64Core_m3DBEB9BA3D05D681BF354807C9D16B725CC7256D,
	Utf8JsonReader_TryGetSingle_mF21A462DD40F497AE887B4F93EB3F329632E6D34,
	Utf8JsonReader_TryGetDouble_mA5FF188644DBB6DFF353DA7887B0A28B7322EDD6,
	Utf8JsonReader_TryGetDecimal_m9129A3234F32FBF6B0C85C3F4D2AB8C5F008F297,
	Utf8JsonReader_TryGetDateTime_m5D9B9E2E3B345769B3D1505271DBE6F7EF852895,
	Utf8JsonReader_TryGetDateTimeCore_m08A0D16A29B1B5071737A3E3C2A88FA68EC68A14,
	Utf8JsonReader_TryGetDateTimeOffset_m33ADB6C9F6844507D161D845D20E471322D84587,
	Utf8JsonReader_TryGetDateTimeOffsetCore_m97770A3308970C50091E12FA977F276276D776DA,
	Utf8JsonReader_TryGetGuid_m5D97B625335AB390E15C8AA7A4D4794B8BA868A5,
	Utf8JsonReader_TryGetGuidCore_mB8171BAB36B60E93D015CA3CF34B48BC53C7C171,
	PartialStateForRollback__ctor_m0A2CA55E0DA1307DCE07752039D69E1E171C6178,
	PartialStateForRollback_GetStartPosition_mD8F495352B433FEE2C9D27A58FDBAC331C418D63,
	NULL,
	ArgumentState__ctor_m3033135B5B010F2D2835EF99DDB6C0E05FB75490,
	JsonCamelCaseNamingPolicy_ConvertName_mFB023872AB7D502C0CC509E886E3EA3B607C6DA2,
	JsonCamelCaseNamingPolicy_FixCasing_m2C442C406C789CE960E6D5F67C8F2E82B25F15E8,
	JsonCamelCaseNamingPolicy__ctor_mBBEC8A003FA852B335157E6EF9476B7EEA06B101,
	JsonClassInfo_get_CreateObject_m3924F94319E8CC8037E397EC71A758534AE7E3EC,
	JsonClassInfo_set_CreateObject_mCB383ACB75DFF40E78CD099DECE8A89CC5C95C9A,
	JsonClassInfo_get_CreateObjectWithArgs_m5F1565F5FBA9FF4DA02786C6654771AC177B1E39,
	JsonClassInfo_set_CreateObjectWithArgs_m737C399B0900D4807B7E6A881979765184CA70BE,
	JsonClassInfo_get_AddMethodDelegate_m488E73B03A89040319408B5DF87C9C4DA514335E,
	JsonClassInfo_set_AddMethodDelegate_m99DA3023113AB512DAB80E80B226BD003AF03135,
	JsonClassInfo_get_ClassType_mB004262B1B41B552E9345A10FFF3F8E48F2D408B,
	JsonClassInfo_set_ClassType_m1FF3CC9DB07D8AB41E249DA6AF8AA0DBA83616B5,
	JsonClassInfo_get_DataExtensionProperty_mEE9451B64DEAF5818915EA439213B5A8FEC028FD,
	JsonClassInfo_set_DataExtensionProperty_m869AA0DB667AFCA30A5BA4DAD22FC963343E9DEB,
	JsonClassInfo_get_ElementClassInfo_m30E9BA9D8ADFA4C050194FC0EC1765CEF33A2BBF,
	JsonClassInfo_get_ElementType_m770A51860EC4421D6741DCFF06916FB33AF53858,
	JsonClassInfo_set_ElementType_mC498B4A763F7C2A01DBA9D553145F8D0CA6C7874,
	JsonClassInfo_get_Options_mA487DF18E8069C47D2FA573FB9DF584F488F93CE,
	JsonClassInfo_set_Options_m2F599919B350219F457359B4E7FA7A1B1E83D8C6,
	JsonClassInfo_get_Type_mF4833C6030319D73C7C0E8115D34603648574D70,
	JsonClassInfo_set_Type_m35C5D4D035C6196861487CAE2B4E7D8516848672,
	JsonClassInfo_get_PropertyInfoForClassInfo_m43DD25A15635FAD68648F55AD79A8B6FB15433FC,
	JsonClassInfo_set_PropertyInfoForClassInfo_m50F366281A4E6F986341608C934DEDA64EF2BFD2,
	JsonClassInfo__ctor_mE1D0B01962472E469C523D57A75D32E64D88BE77,
	JsonClassInfo_CacheMember_mB47AA9FB5D7DFE32D3436A7739B067BD737D0605,
	JsonClassInfo_InitializeConstructorParameters_mA4841FC4DC12A035D08974A78209A4131C1F8952,
	JsonClassInfo_PropertyIsOverridenAndIgnored_m9F0F3A2F966803144ED9AECEB339FCDFE6854D1E,
	JsonClassInfo_PropertyIsVirtual_m8610B207119E3153F68A000CEA57A0158CEEBC34,
	JsonClassInfo_DetermineExtensionDataProperty_m8E49914601E84681B4C73C9C2EE06DEA37AEE242,
	JsonClassInfo_GetPropertyWithUniqueAttribute_m72402EC2871CD6CF02CB461628106D494F239B04,
	JsonClassInfo_AddConstructorParameter_m48ADD8B3A3E613EB33C754E9AA41578D2840D51D,
	JsonClassInfo_GetConverter_mDF285EF8F1E6020192A8D4D28862F1A72F1312C9,
	JsonClassInfo_ValidateType_m8A3CCC0F1BB681B9C694590B1657AB2AEF88A28E,
	JsonClassInfo_IsInvalidForSerialization_m2C0C98A692A0ECF16387ED03F935E8B1E649FCB4,
	JsonClassInfo_IsByRefLike_mE5104DEA04DD67289BFF771B1D2BBD7891772E62,
	JsonClassInfo_GetNumberHandlingForType_mDE10C1748C995329323778BA99FEAFC956659114,
	JsonClassInfo_get_ParameterCount_m9A7A9DA3DC8B1796546F01CAE46D2F6AB5F71A84,
	JsonClassInfo_set_ParameterCount_m703F8184C00284D40E133AD69FB81EC896B5020F,
	JsonClassInfo_AddProperty_m66CD279D4E591EA62DD5805E9377331B526D44FC,
	JsonClassInfo_CreateProperty_m100B36BF9AEDA5054F7105C2B583691B84162996,
	JsonClassInfo_CreatePropertyInfoForClassInfo_mF76E7B49D61833668E211B63C003F3DD4575EEDE,
	JsonClassInfo_GetProperty_m3AD05B450229D1255C1064BE1D5B6F60F8DD4806,
	JsonClassInfo_GetParameter_m07D28EB05506CA359731F2B9C9DCA2D2020FC8BA,
	JsonClassInfo_IsPropertyRefEqual_mB02C7B4FC60D6274B5A70866495240515CC8DF34,
	JsonClassInfo_IsParameterRefEqual_m88A98AAE34615FB5DEB1F8309A5FF81DE7D1C639,
	JsonClassInfo_GetKey_mAD8308BF1511DB603B12BEB82E22EF8E93A7C20F,
	JsonClassInfo_UpdateSortedPropertyCache_m4659FB1EA75A9A250BF3ECA5DA92EE1736E83867,
	JsonClassInfo_UpdateSortedParameterCache_mD7203B5F9AAA8035D6A3A06B7B376FA54460FAE6,
	JsonClassInfo__cctor_mF826838C045B496FD4FB3EA1C87C593A20419021,
	JsonClassInfo_U3CInitializeConstructorParametersU3Eg__GetMemberTypeU7C46_0_mA004A63FFB79C1F8C5E65EC01C505FC2E75CDEBC,
	ConstructorDelegate__ctor_m8BF4F0FB25B31B565C9970B41F618B3E5FB0CFFA,
	ConstructorDelegate_Invoke_m9B0B0DE22F6407E28E7A905F4B2FD373B2176511,
	NULL,
	NULL,
	NULL,
	NULL,
	ParameterLookupKey__ctor_mE556DF61D1FA243C6204BA09DB581C64A01469D8,
	ParameterLookupKey_get_Name_m048FB355797E2E00063B62A5FDB6A30CE1C543CC,
	ParameterLookupKey_get_Type_m66F89A37473AEEDE2674D17D6FD97C773D54B23A,
	ParameterLookupKey_GetHashCode_m270F39E11121C94B24B3471AEA06E1396A8A962C,
	ParameterLookupKey_Equals_m763548E25FABB3FFC76054F10098EFB1D95E4F57,
	ParameterLookupValue__ctor_mD1613835BCF09DB67A265CB62B529F079E261816,
	ParameterLookupValue_get_DuplicateName_m2846FDEB4E05B3167D754D5F2774A3F8FFDD3B50,
	ParameterLookupValue_set_DuplicateName_mE89B81F211FE3342D05118E95ACF64341027BB43,
	ParameterLookupValue_get_JsonPropertyInfo_m8570A2FA9EDD6BFA2A1C5DE2B8AF26287C402CFA,
	JsonDefaultNamingPolicy_ConvertName_m7D36EE2599AE2EFFBCEE52F459253A5792DF27B1,
	JsonDefaultNamingPolicy__ctor_m63421AA6B297DEABA6F0EE0AEB7EB42F55113CFA,
	JsonNamingPolicy__ctor_m047EFD87C8E2BF12B060264523C949A0E353A080,
	NULL,
	JsonNamingPolicy__cctor_m4CA469CACC21C0ABF740797B7F54A0536F8D46EC,
	JsonParameterInfo_get_ConverterBase_m280CF92A015816989461A48B357E7C2D1BA6D398,
	JsonParameterInfo_set_ConverterBase_m63A442D88DCC1E79C3E1FA314ADE4351B0278E4D,
	JsonParameterInfo_get_DefaultValue_m9C4947604651E2557BBF7107864DCAB0DE907877,
	JsonParameterInfo_set_DefaultValue_mD2FA38670AC6C3527521CDB54EAD7F359E8F8E1B,
	JsonParameterInfo_get_IgnoreDefaultValuesOnRead_mFB2E81AD69B208C02EDA92059CC9F0706B162D4E,
	JsonParameterInfo_set_IgnoreDefaultValuesOnRead_m9A32A1D7B960DB0BCA25BF1D7F657C36E8AD43FA,
	JsonParameterInfo_get_Options_mD740985271C61D63EE42CB9B9C49DF14B84639B7,
	JsonParameterInfo_set_Options_m8D916748A8A1CFFAF7BBFECBE0B46461BD2A05B9,
	JsonParameterInfo_get_NameAsUtf8Bytes_mC74D28CA16D8E6CF8F4341D3A10001917FA4EEC2,
	JsonParameterInfo_set_NameAsUtf8Bytes_m9F52C8A73862C84E1AB8A136F85A8925778AF9C0,
	JsonParameterInfo_get_NumberHandling_mA793C420F52157BFAC40C89B747D48159D519FB4,
	JsonParameterInfo_set_NumberHandling_m69F1AFEAA58436C0EB39B05FB8FBE29870288CF5,
	JsonParameterInfo_get_Position_m4ECD7B7B8E7B34713BFC26010F66169AB271D601,
	JsonParameterInfo_set_Position_m50E95F79ED24C836FC139AF7C7D3190BC0B2DC76,
	JsonParameterInfo_get_RuntimeClassInfo_mAC478726629495B15346BB844B7197D268C52399,
	JsonParameterInfo_get_RuntimePropertyType_m12B753E79AA5EE3DDDE208C90283F9D7D7A1B9F6,
	JsonParameterInfo_set_RuntimePropertyType_m936B468A974E2FC8D5A5771111A948E3FBBF1E0E,
	JsonParameterInfo_get_ShouldDeserialize_m0BFEBB5B82CFD53BA18436FDF40579DD363D8C4B,
	JsonParameterInfo_set_ShouldDeserialize_m2B4028AA15136C622319698384D9F848B6575B6E,
	JsonParameterInfo_Initialize_m99FC496E75F369E9A069F32103D7CB38256E50AA,
	JsonParameterInfo_CreateIgnoredParameterPlaceholder_m51708F7954493FD7D828E145CE3F6FA8BA924877,
	JsonParameterInfo__ctor_m87C8C406AC90E89FBAA5D2A963DCB69E0F06B322,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonPropertyInfo_GetPropertyPlaceholder_m0002854C8F4336E9CDB8E656BA31AC417679C15A,
	JsonPropertyInfo_CreateIgnoredPropertyPlaceholder_mFC68A9FB329938FE039B4AC8DA44FB740B0C7409,
	JsonPropertyInfo_get_DeclaredPropertyType_mACBDE6A28FC500481702320EF6E636DCE7C73DAA,
	JsonPropertyInfo_set_DeclaredPropertyType_m58E832BD84C9BB9961F9818EF8E1A9B541E552DB,
	JsonPropertyInfo_GetPolicies_mA6712A0AE3BE7EC4D25DBA0974D37F38F0B73C56,
	JsonPropertyInfo_DeterminePropertyName_m25078709433A5F0C62AF288414D8501FAF3A9FCE,
	JsonPropertyInfo_DetermineSerializationCapabilities_m1A2CC2F58501D7B30A1BB823C716EC1EE83631CA,
	JsonPropertyInfo_DetermineIgnoreCondition_m0AFA34AD28A7DE506410D6782D297165A1AAC6E1,
	JsonPropertyInfo_DetermineNumberHandling_m662836017EE113BCC3426911282DEC55FAC6818A,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonPropertyInfo_get_HasGetter_mA8ABECDB9D68BD370F0ECF0B51A530AF6CBE49C0,
	JsonPropertyInfo_set_HasGetter_m3E3B9FE50F71BF6965E4179F44C5118394F4EB76,
	JsonPropertyInfo_get_HasSetter_mB7EBA2506BCCB52852390BA7C0EF680114BFCC8A,
	JsonPropertyInfo_set_HasSetter_mC66078A3C7DAAEF93BD18FEC9CCFCB684901BEBD,
	JsonPropertyInfo_Initialize_m87CEBD25FEBE62431919EE154555571AC147341A,
	JsonPropertyInfo_get_IgnoreDefaultValuesOnRead_m485BF3036EFE5652D3354AB52BFE4AD9C462EBD4,
	JsonPropertyInfo_set_IgnoreDefaultValuesOnRead_m9ACA6E5CBB4477A57064464E509A4A031287A294,
	JsonPropertyInfo_get_IgnoreDefaultValuesOnWrite_mE576FFF14D1B98A34C69F485B99757933138AE42,
	JsonPropertyInfo_set_IgnoreDefaultValuesOnWrite_m591097336FB6F6C0AE4BFF25BBED8FC9515381D9,
	JsonPropertyInfo_get_IsForClassInfo_m5E9EB12AD97755E0026D726A4DC13F9D3A570A9C,
	JsonPropertyInfo_set_IsForClassInfo_mF91A27CA43772F77B01DF9DBE19276384CBD3A67,
	JsonPropertyInfo_get_NameAsString_m907A831A0369E765CDED710035B36F0FA5AC8B9B,
	JsonPropertyInfo_set_NameAsString_mD58CEEB19C9F3D7C5AD6406FCBD1355D53C48D36,
	JsonPropertyInfo_get_Options_m13E3BF459F8675C65E6C608482C9C0789F75B97A,
	JsonPropertyInfo_set_Options_m53F38E18046D1268125293BDDF9ACF1B0F34B668,
	JsonPropertyInfo_ReadJsonAndAddExtensionProperty_mC188228BBDE083F556DF4A11AC496F3A19256D02,
	NULL,
	NULL,
	JsonPropertyInfo_ReadJsonExtensionDataValue_mAD9A1C2ED99013A0777C13386BE11F408668B44E,
	JsonPropertyInfo_get_ParentClassType_mBFF8F3DC065713FD77F12788DFD0E353C0FA877F,
	JsonPropertyInfo_set_ParentClassType_m14715F89089DCC5D3B64C72199F01FE44409CD11,
	JsonPropertyInfo_get_MemberInfo_mDB50F867A36435920C12ED71CA72580FF7EB3A8E,
	JsonPropertyInfo_set_MemberInfo_m6BBABD3EA7E95FACE7C093144DB3648FBD62E4B6,
	JsonPropertyInfo_get_RuntimeClassInfo_m8CF6F07A07ED366B98034C37161FF6A37C5FEC96,
	JsonPropertyInfo_get_RuntimePropertyType_mE2C5C15069C2A00DE2C1AD067091F95D79149019,
	JsonPropertyInfo_set_RuntimePropertyType_m5CFF3F102D982C87100EEFFF55192D91D75EB45B,
	NULL,
	JsonPropertyInfo_get_ShouldSerialize_m17CDCACBC8CBDE574091BDCDD33CEB8D017839D4,
	JsonPropertyInfo_set_ShouldSerialize_m8BA04314A995978B0066992640E2296C146C3839,
	JsonPropertyInfo_get_ShouldDeserialize_mA6E24C13D03410019DE42F107146B0361AB3D5F0,
	JsonPropertyInfo_set_ShouldDeserialize_mD9BB3AA10AC77793B2D49D65460CAF4EC7908709,
	JsonPropertyInfo_get_IsIgnored_m21287FDA8B36179BDE92703E355B5CDCCD6B2C1D,
	JsonPropertyInfo_set_IsIgnored_m81FFCABC248845376883684AC190028BADAC7A84,
	JsonPropertyInfo_get_NumberHandling_m3F57EC55C9CC7CE9C1017CBF1B737759673F3947,
	JsonPropertyInfo_set_NumberHandling_m7171FCC5706B63D324F911FA3D868549B17183B0,
	JsonPropertyInfo_get_PropertyTypeCanBeNull_m00E89F86EC69375E087FE13D14C58FF621132391,
	JsonPropertyInfo_set_PropertyTypeCanBeNull_m1B4AEF7B3C4E167742433F15E6DBBA625C7FAFA0,
	JsonPropertyInfo__ctor_mA12D185F265F12C6EFE9E29C1225A3490338B1F3,
	JsonPropertyInfo__cctor_m283BE798DF2D39D8379E8E49E98F9109F6296BD2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer_TryReadAheadMetadataAndSetState_m18C3E5513D3BD9B27109262DCD4DC5A0FDC6402A,
	JsonSerializer_GetMetadataPropertyName_mF1443E52F7898BD6C9BCC6B6C5787A1BC84E6BC7,
	JsonSerializer_TryGetReferenceFromJsonElement_m17DB1EDA1C943CA0F9F145EE601A69DBAB9B6387,
	NULL,
	JsonSerializer_LookupProperty_m6AA7BC6D36648D0E270EDB172BBF757DBBC98C09,
	JsonSerializer_GetPropertyName_mB251E25F8AF927A1FB1795EBD3EFA0C50E0D7237,
	JsonSerializer_CreateDataExtensionProperty_mA9DA737787344F0BE4E3029D717E31BCC4AF2D49,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer_WriteReferenceForObject_m160349753BEA999B4A3A1247586C186A07369272,
	JsonSerializer_WriteReferenceForCollection_mB0094640168D976CA3C8D605724B441E1744D15C,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer__cctor_mCB5617E55AB44E467BEDEF30A3D1EE3CF7F184F1,
	JsonSerializerOptions_GetDefaultSimpleConverters_mB2C7D4015A718DC111FCF029A4B011CF576262ED,
	JsonSerializerOptions_GetDictionaryKeyConverter_mBF44EBD27BAFE9B9A177824C3A72E42767B6DAB9,
	JsonSerializerOptions_GetDictionaryKeyConverters_m47585D77EE9AC1A259A1F1C23BC8BF3E9E7B84EB,
	JsonSerializerOptions_get_Converters_m387F196CD3A362700DF74ECFEC1DE4873D84180E,
	JsonSerializerOptions_DetermineConverter_m1DB19C7DF6014BB9051B050BC6886B48B13EFC4E,
	JsonSerializerOptions_GetConverter_m3F90FE1FF74CAFEDE6BA42D78D9AFB3CF317D6E0,
	JsonSerializerOptions_GetConverterFromAttribute_m2AA115134BF1C2F46386A9F21F61106BF6EDBDFC,
	JsonSerializerOptions_GetAttributeThatCanHaveMultiple_mC2B1111C6D9FBF3E67BCEEA476EB66A5BDA9FEDC,
	JsonSerializerOptions_GetAttributeThatCanHaveMultiple_mA27AFAD71EBA68DF05A1E77BF2DC90E24695AF9F,
	JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m89673A39073669E7EF22CF83B03EB008C318CB92,
	JsonSerializerOptions_get__lastClass_m625BC41A0607D88F18B0796584D3AA94640639EE,
	JsonSerializerOptions_set__lastClass_mC4E971444165788CBAF11FE1B3BA0DFACCCF0CC8,
	JsonSerializerOptions__ctor_m685DCDC94C86F90CDBD5704BDCEA4F983E71FF32,
	JsonSerializerOptions_get_AllowTrailingCommas_m03E3A6D2DFD2C0BC49693FA42A9926E6FF51F701,
	JsonSerializerOptions_get_DefaultBufferSize_mD25998F6684286580A73189AD652EEB026F854AA,
	JsonSerializerOptions_get_Encoder_mB75B706FD84160A43BF875AB8B45C29944A7E996,
	JsonSerializerOptions_get_DictionaryKeyPolicy_mA6A655B77780FD0D1C0D6C1B4F5F5E18ADF21370,
	JsonSerializerOptions_get_IgnoreNullValues_mA7D834AD9E4988A07DA6632B315D170D07219F4E,
	JsonSerializerOptions_get_DefaultIgnoreCondition_m042572302083490D8932064ADCB08DA3BBD8E943,
	JsonSerializerOptions_get_NumberHandling_mBCFB177F8810B8E86E17A3EE8CD887B60CDD3B06,
	JsonSerializerOptions_get_IgnoreReadOnlyProperties_m02A855070C522C0E3235E755F025CB19AF5E60C9,
	JsonSerializerOptions_get_IgnoreReadOnlyFields_m6B05586263255677FD679759BC56EAD61C7670A5,
	JsonSerializerOptions_get_IncludeFields_mCB66F8236EDEE738FF5C027C3BE758CF6144F86D,
	JsonSerializerOptions_get_MaxDepth_m76A401099406884829012AFD5D3974841DCD2414,
	JsonSerializerOptions_get_EffectiveMaxDepth_m5B3CE03573116B7D5920A24F386226C7497CA6B9,
	JsonSerializerOptions_get_PropertyNamingPolicy_mE46B28D19C9052B9D6155852651866DD63ED44A7,
	JsonSerializerOptions_get_PropertyNameCaseInsensitive_mD32CF777E463003A0B778207E3EBD452E6A613F1,
	JsonSerializerOptions_get_ReadCommentHandling_m71B386675D5DC6B9B0369F7DF3091F2677D80F0B,
	JsonSerializerOptions_get_WriteIndented_m684D835945BC60ED198825303361C90444824CD5,
	JsonSerializerOptions_get_ReferenceHandler_m0617CB46B783371338540A3A470133DE6CB5CD9A,
	JsonSerializerOptions_get_MemberAccessorStrategy_mA1BC386CE6006A4E6AE72350C652E32EF9D29B0F,
	JsonSerializerOptions_GetOrAddClass_mC4C69C74911631072200AD6AB5A4343176D95D07,
	JsonSerializerOptions_GetOrAddClassForRootType_mD9ECD25EC6AA5ECFD8AB35CA89BA487C6AE2EC8A,
	JsonSerializerOptions_TypeIsCached_mF038D4D0C7E6D190C5D60B3D5CC2D68B5B8CC961,
	JsonSerializerOptions_GetReaderOptions_mD8A4C5D8650831EA9415502C95563ADAE6E14336,
	JsonSerializerOptions_GetWriterOptions_mD1361A9A74AF5C0CAA7FAB06718C9516989CED47,
	JsonSerializerOptions_VerifyMutable_m0C305EB691332076592359F232D8639FF95467F6,
	JsonSerializerOptions__cctor_mE6F9A7547EBF12548CB7B40D6DB6851CA14232AC,
	JsonSerializerOptions_U3CGetDefaultSimpleConvertersU3Eg__AddU7C3_0_mE1E7914A06F0CF6977D174FD4DDDCD2BA2D7CC86,
	JsonSerializerOptions_U3CGetDictionaryKeyConverterU3Eg__GetEnumConverterU7C4_0_m422925E719E621089038D602E3BBD474F7C96DB5,
	JsonSerializerOptions_U3CGetDictionaryKeyConvertersU3Eg__AddU7C6_0_mDA972273A76E4AF978A8EF39C01033AAA1D83A96,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberAccessor__ctor_mAF5FE11042E3052B767A8602683A634754B107CC,
	ParameterRef__ctor_mAE5D0234AF0B54F1B4F1C0F808F635329A9928A2,
	PropertyRef__ctor_mA2ED2603D61FFE0F81222405419BB00EE8455EF4,
	ReadStack_get_IsContinuation_mD01FD9A2DAC4FD9C3E805929E0104AAED9EFF95A,
	ReadStack_get_IsLastContinuation_mA8012A8628BE1311AE139DD8DC7D98E902EACF7F,
	ReadStack_AddCurrent_mF400E1F933A12C71C19C3A4DFCBE009008B823D5,
	ReadStack_Initialize_m7E7F3ED59DEEFF3DC132C8773C83EE001A238EAC,
	ReadStack_Push_mA7CB7FCFBF6F542FD626BDE571D54F1D136B3C0E,
	ReadStack_Pop_mD314744BCA164729B64A75EAAEB43FC240346DAB,
	ReadStack_JsonPath_m86FCB866216FA16E285600FB62136552D1CA7DED,
	ReadStack_SetConstructorArgumentState_mB792C92FEA853A82C6564EB5F41E3A2ED909B992,
	ReadStack__cctor_m9F755C7761204A7F190514C335221AE561537546,
	ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m8ED401FF34693D816348D4CE72C93E7394006130,
	ReadStack_U3CJsonPathU3Eg__GetCountU7C19_1_m75E574E6B1DBFD7C4822567526DC011D8AD9FC40,
	ReadStack_U3CJsonPathU3Eg__AppendPropertyNameU7C19_2_mA80E427F474EAE78F6CC63C1028369018338D28E,
	ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m104FC083CCE5BB72491DD4BA0B26BCCE2BAFB176,
	ReadStackFrame_EndConstructorParameter_mE5D6B2F709289B69DD06F58040B1AA47DF4C4EC7,
	ReadStackFrame_EndProperty_m6BBD5C956CCF9412B4B122B5E2501681DE66EEBA,
	ReadStackFrame_EndElement_m4F6CA0BDACF8EEAB43A942243D21DF32C5C26C8D,
	ReadStackFrame_IsProcessingDictionary_mBC439FBEB5E92414A12DBE4F25CFC1B8B541ED0A,
	ReadStackFrame_IsProcessingEnumerable_m8BA2785FB705C7BC0FFFAE84E35516CCAA028D8A,
	ReadStackFrame_Reset_mA12D43AF86177A2B7822F04C0E20B9556D8867F1,
	WriteStack_get_IsContinuation_m88FEB1F19992286B54F88DE66079DAA6CB1BF319,
	WriteStack_AddCurrent_m47F360C33799C68FED06B57AA0321371A5A4CF68,
	WriteStack_Initialize_m1BA93CE9F8C4B1A365B61E8AF2A3B0D1FBCF7F3F,
	WriteStack_Push_mEFB808A906431C95FBFE4A654F9FEFB8C962F048,
	WriteStack_Pop_mA03ABCC412B4F0C5720FB1774D6A61F716450B48,
	WriteStack_PropertyPath_m2B7C7609CF52ABC0DCCB60162CB58BFEA3EE4969,
	WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_mFA8217E89FE0DFE851708748202127A07F74AEBA,
	WriteStack_U3CPropertyPathU3Eg__AppendPropertyNameU7C13_1_m5A499E425FD4D40245FC2BDDF6CB558870F6A287,
	WriteStackFrame_EndDictionaryElement_mF8B3A60F6AAB6D4EF329FC9EC0026B1BF43A184B,
	WriteStackFrame_EndProperty_mFD22836ABA865D92442FBBC0E612A51A74F0F503,
	WriteStackFrame_GetPolymorphicJsonPropertyInfo_mC6241293FD5975BCF17AE77AAD2A1735932D473C,
	WriteStackFrame_InitializeReEntry_m6F30B6BDCE7931B9867F8DE7CE69299CA0C5330B,
	WriteStackFrame_Reset_m919F66E6B37A5AABD075DF89DC00723E2FE74485,
	TypeExtensions_IsNullableValueType_m4898086A046C3E4BAE28D49F62382EABE2D51079,
	TypeExtensions_IsNullableType_m12352B86A8DCA9AB2EB22C019B3A19C15144CFFF,
	TypeExtensions_IsAssignableFromInternal_m87241C5103114B0C94797AC15693EF1AC0C62803,
	JsonWriterHelper_WriteIndentation_m8712AB577179603DF11C02333F9C0B4FD5388AF4,
	JsonWriterHelper_ValidateProperty_mA9619E69A4DEE0A2EF600C8881552A4A02DE6051,
	JsonWriterHelper_ValidateValue_m7896B25EB4023B71DBC98FDA97242CB0E3C458E1,
	JsonWriterHelper_ValidateBytes_mC728D6FE42DBCD544F12F98ABDD55B8E387B9CA7,
	JsonWriterHelper_ValidateDouble_mDFD0BE277B2D44D79C8CEF25FD1FBB4CB9F10A15,
	JsonWriterHelper_ValidateSingle_m0ADF667516A892AFEE48061137CF087ED5078616,
	JsonWriterHelper_ValidateProperty_mFF2FA821AE187E6CA3F5DBB08A09CD4E9FAE473C,
	JsonWriterHelper_ValidateValue_m362488F874F8A95B4B24112E27588CE76C71F696,
	JsonWriterHelper_ValidateNumber_mBF35E6030A120C621778812860E76F76C5129963,
	JsonWriterHelper_WriteDateTimeTrimmed_mEE9DD7C3360953F43562045A20C5B0F7A0EA1B17,
	JsonWriterHelper_WriteDateTimeOffsetTrimmed_m51D0409624F026B505AD812107EB7554F06A2A1C,
	JsonWriterHelper_TrimDateTimeOffset_m88440DBC7ED3C898144132A001DB53F6B5814AD4,
	JsonWriterHelper_DivMod_m24077803A35C71313756020418637482134766AF,
	JsonWriterHelper_get_AllowList_m805D9C7A9D48EFCD489AB80BD12D54E54E2069D1,
	JsonWriterHelper_NeedsEscaping_mBC38FB4BB1F4A6E25F2290769848B4FC5AFA6443,
	JsonWriterHelper_NeedsEscapingNoBoundsCheck_m1726AD27336636DBA4D1ED35C6A7A35576DCF7C3,
	JsonWriterHelper_NeedsEscaping_m8D513359DF89D5CEAEE6B28549F7BE974BB69D40,
	JsonWriterHelper_NeedsEscaping_mD8D4E8A8B346E6F1E91580CA83FB13CD6AD30BEA,
	JsonWriterHelper_GetMaxEscapedLength_m600D967D2D4A80595C90B496097F912720DBC0BD,
	JsonWriterHelper_EscapeString_m071B896FC9E70EE8CA0284772E1E56201D42539C,
	JsonWriterHelper_EscapeString_mEF28F3394651A92DC0635CBBA0BFC2E362CFEA30,
	JsonWriterHelper_EscapeNextBytes_mDEB4D5C8088F6DBB31FCA5641B6B8F3B2C4B0FDE,
	JsonWriterHelper_IsAsciiValue_m40ADB4FD3BB1E29EF8259F5B67BC2D38AC849870,
	JsonWriterHelper_IsAsciiValue_m511C15A62875EC4BC5C65766F71AFAEAFFAF0494,
	JsonWriterHelper_EscapeString_mED258B0AC21DA8C964CE1FFC708B221995EF8280,
	JsonWriterHelper_EscapeString_m219882343D18E7485EE5DC95C846BE4E70517761,
	JsonWriterHelper_EscapeNextChars_mD0D8A8A11108E9D13EC6F4C59D0525BD0330D95A,
	JsonWriterHelper_WriteHex_m78946CB72326295D44767A496D976A25DA64FABE,
	JsonWriterHelper_ToUtf8_m8C88E880A53E299AC0B145E7D9D373562357C62A,
	JsonWriterHelper_PtrDiff_m27CDC21C09A8C4F0C531C275B2BE14632DDC94AE,
	JsonWriterHelper_PtrDiff_mCA9266FECE40CB5EFF86CD3CBD17F301A86D2B4E,
	JsonWriterHelper__cctor_mF433480DDBAFA69A6F1412D0CC547C4FB55FBC62,
	JsonWriterOptions_get_Encoder_m232389EA65BBFEC33F6133822B6014A12A3DD4FE,
	JsonWriterOptions_set_Encoder_mC29EDD6B2528E7E44A0E76471CC62E46FA114F00,
	JsonWriterOptions_get_Indented_m1A267394B53602FEC4043C51430CA74205BE2316,
	JsonWriterOptions_set_Indented_mC3F43DEDCB5B57EEFDF253E8397F49F73DB43C91,
	JsonWriterOptions_get_SkipValidation_mEEE3015DF8EADA12C2E39BC1684FB3A5D2850683,
	JsonWriterOptions_set_SkipValidation_m46DB044F25318B23C13DCFC763A9967C06CBA832,
	JsonWriterOptions_get_IndentedOrNotSkipValidation_m7F6599D6058B3C9C18EC99304B0B5E386CD84DD7,
	Utf8JsonWriter_get_BytesPending_m3972ECDA5A4E7DBC6A17F7AA0610CB20AB58EF2C,
	Utf8JsonWriter_set_BytesPending_m21E09C8027465D1AD0C46E31DD91C8C83524DB10,
	Utf8JsonWriter_get_BytesCommitted_m4647460AF1A97E6805CF12393C978FE240CCB076,
	Utf8JsonWriter_set_BytesCommitted_m05CFA019A22B21A6E59B688CCD22B1C76C42E9B3,
	Utf8JsonWriter_get_Indentation_m162E25CDEE92EEC5C82650530A018BA5C049A874,
	Utf8JsonWriter_get_CurrentDepth_m2770E27E9396C97DD4E4B20BFECAE60EF23E8417,
	Utf8JsonWriter__ctor_m878AA70AA00DE7300C1749D15BF67D09B30B335C,
	Utf8JsonWriter_ResetHelper_m106A696B3EBF0175EF391F2DAEC5FA2F3DD5129A,
	Utf8JsonWriter_CheckNotDisposed_m83F234E5126E68BAD92D6145BF87189C78E8FF33,
	Utf8JsonWriter_Flush_mDF86E72B309640B3B8CD33D86089E313C78E60D8,
	Utf8JsonWriter_Dispose_mA686B611C38448D0567ADB7A2CDE41CA0D7273C4,
	Utf8JsonWriter_WriteStartArray_m5AF69C1DC77BE068C92A9198EC8275D1FD13A9D3,
	Utf8JsonWriter_WriteStartObject_m405F3F6425E7ECBDC32F3663421787FD9593A115,
	Utf8JsonWriter_WriteStart_mB5CA5EA053FF7063C403EFFB317095D4B3D6FCE1,
	Utf8JsonWriter_WriteStartMinimized_mF8D7F79CA85776D5041D59E908B9BC630AD26C71,
	Utf8JsonWriter_WriteStartSlow_m9FF27EF7C53F3AD8EBBAD48E6DD6DDD16EFCE6B3,
	Utf8JsonWriter_ValidateStart_mD2AF3E65F07B403D5E8D76A13DEB7D2132477966,
	Utf8JsonWriter_WriteStartIndented_mB34D235939A0AB34C559DA572268048F7DE14DF5,
	Utf8JsonWriter_WriteStartArray_m818E99BFA0330F7A9850F26B1496F808720FDBB6,
	Utf8JsonWriter_WriteStartHelper_mECFAAEF012DF4CAED3CF2DA51A6C8E7B770A93B0,
	Utf8JsonWriter_WriteStartByOptions_m43C300E0D8774384B11C92D4B0BA92F8109445F2,
	Utf8JsonWriter_WriteEndArray_mFA4F851879CFEA614A25B18A3A6EF9EFDA2E4454,
	Utf8JsonWriter_WriteEndObject_m370A04892C849299DB8B27866FC1F33359249E09,
	Utf8JsonWriter_WriteEnd_mA795ED14708D59E3DC44D5E4E97E6663009F2D9F,
	Utf8JsonWriter_WriteEndMinimized_m34C7375A11890A5FDAEF72A1647EE6BAAE8C561B,
	Utf8JsonWriter_WriteEndSlow_m034429F9528A98972D804D7D7296C8C71885B8E2,
	Utf8JsonWriter_ValidateEnd_mEF5170B7F687F80F287B2E444C89061A0479814B,
	Utf8JsonWriter_WriteEndIndented_mEE5818F054A644AC0AC6702FAF93C66B9842567B,
	Utf8JsonWriter_WriteNewLine_m0EE49D5BBCC209D1E4B172C69FECB357DA48825D,
	Utf8JsonWriter_UpdateBitStackOnStart_m4EBC052F260BA75D58908C90A1447C7693278703,
	Utf8JsonWriter_Grow_m27BD8F9402D326DBEE3280EDDF6EAC9D79136872,
	Utf8JsonWriter_FirstCallToGetMemory_m4BC1A5C7F3D0E3DE6575E6F5FB189844488B7C75,
	Utf8JsonWriter_SetFlagToAddListSeparatorBeforeNextItem_m81A0884A33153ACE7D9C64CA1EB8798A7AF98F26,
	Utf8JsonWriter_get_DebuggerDisplay_m97377549BD621291AE6CDB2B4FBC2D7CB4EBDFC8,
	Utf8JsonWriter_WritePropertyName_m4FD0A8F47F96CF03A45092DF00F9857ADAF46A67,
	Utf8JsonWriter_WritePropertyName_m64DC1F81413E96F2DE3347ABF72D1B0A8D96921F,
	Utf8JsonWriter_WritePropertyName_m2956BB08421B6FDB88ACFBF0603E317CD66ED04E,
	Utf8JsonWriter_WritePropertyName_mB47FEE2DFC9D07ACD1F4EA4EC031E624A264D279,
	Utf8JsonWriter_WritePropertyName_mD5A7873A26BE9F37ACBA5C81933D38EEF9907D08,
	Utf8JsonWriter_WritePropertyName_m7EBAE815A669E7BDF1B7ACB71264F3B9F8FA1EBB,
	Utf8JsonWriter_ValidateDepth_m506A98E5E009906D3FEBA660D9E9601EF2234B98,
	Utf8JsonWriter_ValidateWritingProperty_mA019E18A4D74422B9E9CEB4272FCFE3D64BA3973,
	Utf8JsonWriter_ValidateWritingProperty_mF318177CA6DAF442AA1249CE17FE83904D998A4A,
	Utf8JsonWriter_WritePropertyNameMinimized_mED43D25B962FD6C8711B7DB7CA72DED650DD275E,
	Utf8JsonWriter_WritePropertyNameIndented_m34B7AA96A926924464764C5C64B1C8718F43C0A5,
	Utf8JsonWriter_TranscodeAndWrite_m1514F44946C17B820849C1191F8E518A270D942C,
	Utf8JsonWriter_WriteNull_m855DBD5672B217200384C6359E8E6E5E292025EA,
	Utf8JsonWriter_WriteNullSection_m85198ECB18623D7AA9DD9BF3FE2100FDFAA87B41,
	Utf8JsonWriter_WriteLiteralHelper_m6E48796A22D21CEBD4A6FE115C3AB6B5DDADAC3F,
	Utf8JsonWriter_WriteLiteralByOptions_m4C6B106278263022E6117AF53CAD369259588E66,
	Utf8JsonWriter_WriteLiteralMinimized_m752FA36599815EBF0536EC40CAC9D4C94E4CCD5C,
	Utf8JsonWriter_WriteLiteralSection_m00FD131C181B991BF090BF8D70628EBC490FA295,
	Utf8JsonWriter_WriteLiteralIndented_mEBC56FDD2CBD9DB35A08A4D624622AE406BC05FF,
	Utf8JsonWriter_WritePropertyName_mB5FF7C5AAB68DFDA99500F7E603E476149531658,
	Utf8JsonWriter_WritePropertyName_mF7BDE7B7C942A7E4913489702A2718C97647E869,
	Utf8JsonWriter_WritePropertyName_mF405F3CF850CFFB3FAD1AF64C26E414883DE3075,
	Utf8JsonWriter_WritePropertyName_mBB7BDDE39B79F06BA46A38B238500E634103EE40,
	Utf8JsonWriter_WritePropertyNameSection_m32C1F6143B269D86A3918095097CD090A8127B17,
	Utf8JsonWriter_WritePropertyNameHelper_m66F7E72310AB89EC92B272FBFADD5CDD42C5CCAD,
	Utf8JsonWriter_WritePropertyName_mC9ED158A09A22E81D95630C164199CE7DF9A849E,
	Utf8JsonWriter_WritePropertyName_m0765F554DF826D559BC0CC78E484D924DD5105AB,
	Utf8JsonWriter_WriteStringEscapeProperty_m080C5EA0547428273A28745A50EA5E237ACBBCF4,
	Utf8JsonWriter_WriteStringByOptionsPropertyName_mA39463F14C036B2D86A0FEB6F2D608884A5AA9E7,
	Utf8JsonWriter_WriteStringMinimizedPropertyName_m39B609112B1A71879877B162EE9E6EF8734ED170,
	Utf8JsonWriter_WriteStringIndentedPropertyName_mE7D313F37D232CF5625D9C409CDD66AEBBA6A355,
	Utf8JsonWriter_WritePropertyName_m330CA7F278BFD74BA7314CCD560D263D09D91286,
	Utf8JsonWriter_WritePropertyNameUnescaped_mAF05F3BBA02E80510DD8DEEB7852071502EE96AF,
	Utf8JsonWriter_WriteStringEscapeProperty_m537908F234FF38878C7B1B039BE4AFC24DF0A7B6,
	Utf8JsonWriter_WriteStringByOptionsPropertyName_m666E6AEF3456971737062B4818459241ED338CC1,
	Utf8JsonWriter_WriteStringMinimizedPropertyName_m0E87A70328BEE0E6695A1BCCC461E1F06692BA2F,
	Utf8JsonWriter_WriteStringPropertyNameSection_m3F659A854A68E9AC08E217672F35297FDF711ECC,
	Utf8JsonWriter_WriteStringIndentedPropertyName_mEFF104DAB0E76363414FF5D34DA75D2F68F393B1,
	Utf8JsonWriter_WriteString_mC4C4697F80008CC325DA268D181126F27A29F2C0,
	Utf8JsonWriter_WriteString_mAD798D5F92A07D36BEE5DEDCA5369ABE2326DFD3,
	Utf8JsonWriter_WriteStringHelperEscapeValue_m3E958CDC4C2E3F9561C5F48367EBBF80FA113BCB,
	Utf8JsonWriter_WriteStringEscapeValueOnly_m11445B997CDDFB6185550719E632F3C244C3521C,
	Utf8JsonWriter_WriteStringByOptions_m45DC97F766159BFF9280DA9615A9638913C776B0,
	Utf8JsonWriter_WriteStringMinimized_mD2A20FCD732FFE50292C2A5CA960317FF5DCC2CC,
	Utf8JsonWriter_WriteStringIndented_mBB1396532375449C755810749186021969AD99A7,
	Utf8JsonWriter_WritePropertyName_m78F53A27211272470E5933ED12D0D5F16BFBBE74,
	Utf8JsonWriter_WritePropertyName_mE07372EBF45AE7D561122888BEFB6A80CAE5A2C8,
	Utf8JsonWriter_WriteBase64StringValue_m275CDD8B7A07EBEF48C50C9E104252F162EA1B37,
	Utf8JsonWriter_WriteBase64ByOptions_m29D7DA30FBA68D6442C0EF39E98DEC30243CB7A8,
	Utf8JsonWriter_WriteBase64Minimized_m386B30BE0A80EA1E7ABA12C1D15B684C280E77F3,
	Utf8JsonWriter_WriteBase64Indented_m9E83F9BD27AC2DC5FAC4AFDF637DECDBFE4082F1,
	Utf8JsonWriter_WriteStringValue_m644B995EDDE02E8FFD4E20E5169BB4864C325FE3,
	Utf8JsonWriter_WriteStringValueMinimized_mAB05CF9C89552B4CFB794981F31D06724F67AC89,
	Utf8JsonWriter_WriteStringValueIndented_mF8616ED47AC4EA002D35A45420DAF14225948F4F,
	Utf8JsonWriter_WriteStringValue_m10C15B14827394FC4E310F0A4483D2F469A3B092,
	Utf8JsonWriter_WriteStringValueMinimized_m245EF5ABE4AD9955DC4690384D762AE71996FF18,
	Utf8JsonWriter_WriteStringValueIndented_m017F23DFC3BD7CF91E189D5BF59C322EB1C02EF8,
	Utf8JsonWriter_WriteNumberValue_m4CCC908500FA7EA5FBE08752AC5407EEF872F1F4,
	Utf8JsonWriter_WriteNumberValueMinimized_mBAB8C7A8ECC05F152A4A3265A7E39C22AC994C12,
	Utf8JsonWriter_WriteNumberValueIndented_mEF81968ABF0B57912BA5D89D1D2B8CFC81230480,
	Utf8JsonWriter_WriteNumberValueAsString_m16F29B30D227FABD286B348796BFAA23F3475490,
	Utf8JsonWriter_WriteNumberValue_m6038EC46F7253F8C59A795B46405F23227843B8D,
	Utf8JsonWriter_WriteNumberValueMinimized_m0090A828C8E20294AC6B937672BC6DB01B06EE1D,
	Utf8JsonWriter_WriteNumberValueIndented_m51FCE2A59BF0B66AA06837D79674428F47C7B811,
	Utf8JsonWriter_TryFormatDouble_m4ACD8F4466168BA50ED4CFB3277E3498FBC9AD75,
	Utf8JsonWriter_WriteNumberValueAsString_m248D41A780B766CA06A09D38C0E1C8D7800ED355,
	Utf8JsonWriter_WriteFloatingPointConstant_m704AABCD749D18C16864B3916C9B19E8B6D1F72A,
	Utf8JsonWriter_WriteNumberValue_m6D68ED28982B8659EDD4B0CE95910D06E32F4474,
	Utf8JsonWriter_WriteNumberValueMinimized_m5A5486EA90A1CFDD96119204B63F9F535E6BDB1F,
	Utf8JsonWriter_WriteNumberValueIndented_m3699BD007A78E03253A29E8ED7A081E50C569913,
	Utf8JsonWriter_TryFormatSingle_m12FC465E743236B535EDF2B328AA1C7B872E9FAE,
	Utf8JsonWriter_WriteNumberValueAsString_m80AC2D04E5FA2ECC742BAB9989E560069B892821,
	Utf8JsonWriter_WriteFloatingPointConstant_m6823815B694A404B1AF3A0D2D0A94E7B6F81EF81,
	Utf8JsonWriter_WriteNumberValue_m7495DB5B019117D0D320F2679624B8AAE7D61E25,
	Utf8JsonWriter_WriteNumberValueMinimized_m5F2D7B1B8265E9622874C58CF7B89DD9EE9DE0A6,
	Utf8JsonWriter_WriteNumberValueIndented_mB5E713CC39B1A739B1C9C9C6ACC7BF64929BD984,
	Utf8JsonWriter_WriteStringValue_m0BCC1677B57A2864278016B527944F0CD93738DA,
	Utf8JsonWriter_WriteStringValueMinimized_m3E20D3F91904027475F1BE4B78DEC24BE9487C4D,
	Utf8JsonWriter_WriteStringValueIndented_m90D3EA22AAA12C10A0ED689B9CBB9259515FE1EB,
	Utf8JsonWriter_ValidateWritingValue_mE1B9843F8C411945405F163550146191AE0FC800,
	Utf8JsonWriter_Base64EncodeAndWrite_mFB3D8240F7A4DBCF593719FB3390BD867445D8EE,
	Utf8JsonWriter_WriteNullValue_mA3AF6DE23639AF39722BBD33D3BF20FF232A5BE1,
	Utf8JsonWriter_WriteBooleanValue_mA5C6F9B1E59F82EDDDA7D8F1470D582D01305596,
	Utf8JsonWriter_WriteLiteralByOptions_mC4810114A406D30782EBF028DF9A7B8E3B424F90,
	Utf8JsonWriter_WriteLiteralMinimized_m9248E07917AF98DD60C06C97FA9F7BC99F51E3C6,
	Utf8JsonWriter_WriteLiteralIndented_m1CF383922DE0AC0D9F5C729C3DAE12F405101AB9,
	Utf8JsonWriter_WriteNumberValue_mD81F32E5440076B04200EC09C81A6EED33BE9531,
	Utf8JsonWriter_WriteNumberValue_mD2BFA46B5B105F948CD9A96CD53943ADBAA2CBC5,
	Utf8JsonWriter_WriteNumberValueMinimized_m98E4B5524CC79FEAA25A931403BAA9433479E7A8,
	Utf8JsonWriter_WriteNumberValueIndented_m065A2AD2237FCEAA69FA5AC355DD94F58AB29F35,
	Utf8JsonWriter_WriteNumberValueAsString_mCCA1F14F8E1D8DA7E02903B3825C1E866C5D00FC,
	Utf8JsonWriter_WriteStringValue_m31C559DBF8E79BC0ABED29C30728BBD421FBC60E,
	Utf8JsonWriter_WriteStringValue_m1271B865848F4BF39035ACB38CD54B914A3BF385,
	Utf8JsonWriter_WriteStringValue_m2686D29E6B439FA843932F3A349006BDE80A64BC,
	Utf8JsonWriter_WriteStringEscape_mF785130598B4D5F033E9B34E903D06D6B0202B4E,
	Utf8JsonWriter_WriteStringByOptions_m64A45FC421092557F36B99B93BD7BC18F027EFEA,
	Utf8JsonWriter_WriteStringMinimized_m864BE8CB2FCEA8976887BD5C3A15E1C7735ABE59,
	Utf8JsonWriter_WriteStringIndented_m9942DCC20D3304DFF4B9BBA4B0C0B45BB25C88AD,
	Utf8JsonWriter_WriteStringEscapeValue_mB819A602D08A40D4FF68EB03D88FB218E878CA23,
	Utf8JsonWriter_WriteStringValue_mA0AF3BD9AFD4DEB068CB076562F3D760C506DCDF,
	Utf8JsonWriter_WriteStringEscape_mDCC0DD9B5548C5A6701D995EC4FDB6085102B2DA,
	Utf8JsonWriter_WriteStringByOptions_m75719263F0B2039C18C4C4811C7EDA377AFC89E3,
	Utf8JsonWriter_WriteStringMinimized_m8F06D2BC1C9839AD9CABD46F5EBE49249A854C52,
	Utf8JsonWriter_WriteStringIndented_m54C699BC345AFDE60E17088AA62BA2B4CD7F0257,
	Utf8JsonWriter_WriteStringEscapeValue_m27FEFF0BFE40B2484EF0547BA26F25F2B11DCA96,
	Utf8JsonWriter_WriteNumberValueAsStringUnescaped_m3F59FCCAAE8DA3B4605A0358A589AAB7DC6550A6,
	Utf8JsonWriter_WriteNumberValue_mF8CCB5E8AF7CB32821E48257DAEB9488540C95B5,
	Utf8JsonWriter_WriteNumberValue_m14B1AC5C45AD6F1FB2A9F42FF28CEA2CF6D8CBCD,
	Utf8JsonWriter_WriteNumberValueMinimized_m1F41036EE317C0A8CEE2DA9D6C072042BB219928,
	Utf8JsonWriter_WriteNumberValueIndented_mE474F2518F3ADA4C7F787FDEDBD2C153E5AC0650,
	Utf8JsonWriter_WriteNumberValueAsString_mE670846DCCD878B872DB6FD051E8EA6EB8CFED42,
	Utf8JsonWriter__cctor_mD41DD5176CEC82423AC711C304F37EDA3B8CAC1B,
	JsonConverterAttribute_get_ConverterType_mBD3D8984B125937D81BEE770510CBD59DE992D6F,
	JsonConverterAttribute_CreateConverter_m0EF4444BF485369A2A5F9023E03EC8C124E0B34C,
	JsonIgnoreAttribute_get_Condition_m5218388DF4B31B960A88F80CDB92BAD3A1575391,
	JsonNumberHandlingAttribute_get_Handling_m47EA30AE14CB014E642C09FC271D744207304236,
	JsonPropertyNameAttribute_get_Name_m43379618C5E4C36982CD87DCC7DCD46C9AF7636A,
	ConverterList__ctor_m7956CA822F885BE13239190A0AAF958E03950869,
	ConverterList_get_Item_mDFFAC649D8706D825E9B3412F06EA95E068000C0,
	ConverterList_set_Item_m397EBDB7ACE75DADAA7194BB9B034EA046606128,
	ConverterList_get_Count_m6E0DE98A3850DB3A4A5EC9024DB63F0A739558AD,
	ConverterList_get_IsReadOnly_m87EE2CF9E124FF5D19488059BE6F215D894EDC05,
	ConverterList_Add_m1F0C1BCBB85EBA89D586F4EEA8F87AB8ED30D7AA,
	ConverterList_Clear_m404C78E5D51713B9E923361FBD51ABA02E455C27,
	ConverterList_Contains_m6DAD9A6249FC6715E3E5FC97BCD8494266029865,
	ConverterList_CopyTo_m1B02A2AF35F337BD4BAC62D0C5135F25ACEA67C6,
	ConverterList_GetEnumerator_mA2395D76A6CD16AF83C873361220DE759E715DC3,
	ConverterList_IndexOf_m2A289B4A827C0B2E0FB3DDD111048D149D22EDEF,
	ConverterList_Insert_mE4AAC6CD09995A35BC0A5676B5278B3E7D3A23A0,
	ConverterList_Remove_m96AFCD48E6D8CA3531441A4C867C482BABD3B5E6,
	ConverterList_RemoveAt_m59B14B97F0484125A311BE119694C8264AC11943,
	ConverterList_System_Collections_IEnumerable_GetEnumerator_mC89C567B32F6ED8242857A0F3AA329BD7112F134,
	IEnumerableConverterFactoryHelpers_GetCompatibleGenericBaseClass_mFE8785F58EDD007ECCB3D76077B0565BEE8DAA2B,
	IEnumerableConverterFactoryHelpers_GetCompatibleGenericInterface_mB640DD8282CAC982B0360A4B1F5DB36603201157,
	IEnumerableConverterFactoryHelpers_IsImmutableDictionaryType_m35307B7347E0B7A698DA5E80E439A484D063DA42,
	IEnumerableConverterFactoryHelpers_IsImmutableEnumerableType_m7718A49E6C83EB1AE4F16348E3FC3CDBB61A1EF9,
	IEnumerableConverterFactoryHelpers_GetImmutableEnumerableCreateRangeMethod_m750E671FCD270FA0B1B2E58C2AFDF41843605706,
	IEnumerableConverterFactoryHelpers_GetImmutableDictionaryCreateRangeMethod_mCAF50FB9DCCB97B1BEE3EAE5E50A55427D6EC02F,
	IEnumerableConverterFactoryHelpers_GetImmutableEnumerableConstructingType_mE18B88FF0D674788A765CCE271E806C39124E35E,
	IEnumerableConverterFactoryHelpers_GetImmutableDictionaryConstructingType_m3A729A5F1A962AD48029801F0BF302807939C6CD,
	IEnumerableConverterFactoryHelpers_IsNonGenericStackOrQueue_m4E32775D53BA32FD8CBA158643133A7F924BCDE9,
	IEnumerableConverterFactoryHelpers_GetTypeIfExists_mFF636FA2D1E096D31C8BEF8AB7892437A824CDBF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonConverter__ctor_mD9817852B6266F19DD517CB9CD6054042B59BDD7,
	NULL,
	NULL,
	JsonConverter_get_CanUseDirectReadOrWrite_mE2CF0ECA1CCB2379ADDB5CA9927657FAE9420C97,
	JsonConverter_set_CanUseDirectReadOrWrite_m13DC84941788179AF7B397950B55551825EE1A46,
	JsonConverter_get_CanHaveIdMetadata_mD91E3E32D02A833D96462A328F712EE6EF2FA33C,
	JsonConverter_get_CanBePolymorphic_mBA6DE039DABAEDEDDBBA10D5EF2D6535744EE450,
	JsonConverter_set_CanBePolymorphic_m8CD8BA9054E3885A732BEFDF45DEC79A80145A7F,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_IsValueType_mB362FB9B424A6911D4F4F54CE6E0FCB142D4895E,
	JsonConverter_set_IsValueType_mA4651543A67E50CC092948CEA8F29BE5B23DC911,
	JsonConverter_get_IsInternalConverter_m0FC7605392703BA872B4DD89C327766EFF2C79A6,
	JsonConverter_set_IsInternalConverter_mF87A3D79885CFC9C8D8BD08BAF230EF7DEE9AF40,
	NULL,
	JsonConverter_get_RuntimeType_m9B6CC6CFEA51A1B69F633305552039289B1BA9A7,
	JsonConverter_ShouldFlush_mAA4522431653C4C34E9301B1F8417189EFA0476C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_ConstructorIsParameterized_m120E1195BC89B5743A895922978D983F3A4E92AF,
	JsonConverter_get_ConstructorInfo_mE3B569C452D11FED23B56BF8DFF557811B830959,
	JsonConverter_set_ConstructorInfo_mA1D9AA73D0C0C45A774EF06EA013731BC58B698C,
	JsonConverter_Initialize_mC07941DBDF2999E50EB5B6BB36DE9DFCC393EBF2,
	JsonConverter_CreateInstanceForReferenceResolver_m0ED09AE2DC04B1B4A1C551E37C82FD8928FB8C03,
	JsonConverter_SingleValueReadWithReadAhead_m5D43CECB31D8AF4F26804EB08A50133B8EEEE37B,
	JsonConverter_DoSingleValueReadWithReadAhead_m6F9F19FE6055B6F43F791EB59FD7442698088206,
	JsonConverterFactory__ctor_m388B7D545BCCB9998C7AAB01597321C355B6F340,
	JsonConverterFactory_get_ClassType_m82994907E2B8A2D15B8EA517384E02024344E20A,
	NULL,
	JsonConverterFactory_CreateJsonPropertyInfo_m31164552EFB0E7ABFEADAD4A540061153D3D7E8B,
	JsonConverterFactory_CreateJsonParameterInfo_m63FA03387B0E2A0B53A7198EC281CB7716FF3BFA,
	JsonConverterFactory_get_ElementType_m40B893CA28F4E2A2ECF370CE7384C314AB3C6202,
	JsonConverterFactory_GetConverterInternal_m8DC679FAC00808EE572E83710DE7FAD0890FEAD7,
	JsonConverterFactory_ReadCoreAsObject_mF5A35DF3E689CF3BA748E0160C93B63D99CF2F5C,
	JsonConverterFactory_TryReadAsObject_m9579A04077C8AD890E4422A5B9D5E68421A55801,
	JsonConverterFactory_TryWriteAsObject_mF4545EEF2AC4AE4F0890ACF63BEFD60F9552E5D4,
	JsonConverterFactory_get_TypeToConvert_mB079B427354ACD22DE952F8A96BA9869521940F6,
	JsonConverterFactory_WriteCoreAsObject_mCEBEFDEB14DD8FB62DECC01CBDF6CD13CA24D3E8,
	JsonConverterFactory_WriteWithQuotesAsObject_mE29CE18197B67C412372802CE64CFD719C48A064,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PreserveReferenceHandler_CreateResolver_mB7A12856B97F61A222CD08C3570550E76785C8FE,
	PreserveReferenceHandler_CreateResolver_m573822DCBEBAFC158B150EC9ADE8697C607799EC,
	PreserveReferenceHandler__ctor_mEDCBCAB598908AC79A7DD0E5EB07087B968B51D1,
	PreserveReferenceResolver__ctor_mAEB2BBB86124DD7F3A1B4555049D6A0EDDE5CDD7,
	PreserveReferenceResolver_AddReference_m779013105D39EBA6265F758B239099C5A1E17BF9,
	PreserveReferenceResolver_GetReference_m9B3567D39A1CAD134864F4485AE3946DD8BC52B0,
	PreserveReferenceResolver_ResolveReference_mD115CAE3824CED5C2472442BC1ECD80C818F0621,
	NULL,
	ReferenceHandler_CreateResolver_mCD769ED59A046E2C51A0788EC628284BE37F7EA9,
	ReferenceHandler__ctor_mAEE21A2298791B2C655BF05084A4A742FD6256E4,
	ReferenceHandler__cctor_m48CB714B1431FB031A19E150013244FF8A7B09B1,
	NULL,
	NULL,
	NULL,
	ReferenceResolver__ctor_m656741FB196E1C669DB1138F1B094190D8AD95A1,
	ReflectionMemberAccessor_CreateConstructor_m8533912016C1643578B568F78EDFDE578FB852BD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionMemberAccessor__ctor_m95203B14A7542A66CDBCA345EAB98B69D2A291FC,
	U3CU3Ec__DisplayClass0_0__ctor_m1949513CDEB192A289C6C414E182FEC9DBB766EC,
	U3CU3Ec__DisplayClass0_0_U3CCreateConstructorU3Eb__0_mB23814DC9A2E875C3CDE33E6C86AA756C30B3B6D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IEnumerableConverterFactory_CanConvert_m1A21C7F5751C29F1F8CB3DA6DA2A9E7091508D28,
	IEnumerableConverterFactory_CreateConverter_m7D6677A63ACEEF0B2F4D1C1E5A4BA57CD9681C3B,
	IEnumerableConverterFactory__ctor_m1F85BD2E8D11D236DD476600E062A22BF071A831,
	IEnumerableConverterFactory__cctor_m3E2A6931F9CD31444F66237248382D8FC4BB5EB3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObjectConverterFactory_CanConvert_m6543F946815E3D80FBB55C426E9D42087DDBA0B5,
	ObjectConverterFactory_CreateConverter_mF5C40E6C87E1E4C695C254AF2824A8E1E6638409,
	ObjectConverterFactory_IsKeyValuePair_mDBD26F0A8A4A394BBC1ADF24D27DDD87B7D676E5,
	ObjectConverterFactory_CreateKeyValuePairConverter_m1880F891BCEF327ACBE44EC5BA04AB4C822F6829,
	ObjectConverterFactory_GetDeserializationConstructor_m237DD562CA25D869CCDB2CD198F33073CC9C6984,
	ObjectConverterFactory__ctor_mC9095404A2B5BC9D0ADAF1D5F31C8C8668868BAB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BooleanConverter_Read_mFB2A09BB84222639135B3B46AF8267A7FFAC7D93,
	BooleanConverter_Write_m41E803744353A7A3BA9713B348C68AF0EB4DB222,
	BooleanConverter_ReadWithQuotes_m3F5A61BADCCD0A402462CB52A39E60984BD5B61C,
	BooleanConverter_WriteWithQuotes_m41C8F8BFC73EC7DE758C72BD540936E126691D26,
	BooleanConverter__ctor_mE388F5832A9681929911F9B7353AB103279ABA13,
	ByteArrayConverter_Read_mD3EC879942CAA662EA0816256731313209464465,
	ByteArrayConverter_Write_mAE292A366097ADA6F4AC019859969D9388BA1B7F,
	ByteArrayConverter__ctor_m3F6F244FD1C500A2E219E2CA82C92BD219A07601,
	ByteConverter__ctor_mDC1BE36B8806904F274C7BC105C3428B66FCF897,
	ByteConverter_Read_m36BFD618C371042FEDA433A3B3E5698B82231263,
	ByteConverter_Write_m898451B6F179CE908AFE20194A739766C29C9789,
	ByteConverter_ReadWithQuotes_m4A233A2BA86FB11BEAA2B811DED4685F94A6E64C,
	ByteConverter_WriteWithQuotes_m730BDDA853C1A1D2223DC1FE07529F3537361050,
	ByteConverter_ReadNumberWithCustomHandling_mFEBB66F13E29236D097AAD3C762EF8F130C3A420,
	ByteConverter_WriteNumberWithCustomHandling_mDF8AB8B886F0F792B6751D9A8586A155A83E6911,
	CharConverter_Read_mD8D347B9B0F8B7E206F4148B9A124F803B0FBDEE,
	CharConverter_Write_m4690C764BE7D54FC9103D5C49BD771A713A2C9CA,
	CharConverter_ReadWithQuotes_m1CEA2C76923B56EAEAD30C9C1A328B099770E49B,
	CharConverter_WriteWithQuotes_m4499E42F5C44FE3B17C8C8F95984B05117908825,
	CharConverter__ctor_mD056ED844564EB48D3803EDF9E29162DF9F08647,
	DateTimeConverter_Read_m71862B549C6FFF428D4C547040B6C881FFBA4DB8,
	DateTimeConverter_Write_mAA4174777ED8224977D11C536C395C93C0B871D6,
	DateTimeConverter_ReadWithQuotes_mE0A9B086B0855CF6979E5EE047E3CE06A6594BE4,
	DateTimeConverter_WriteWithQuotes_mCD99AB163FBBE90DBAC390ED00A015C545FBCC5E,
	DateTimeConverter__ctor_m69497F4C605C588FEAB797F50762A0D7B396CBEC,
	DateTimeOffsetConverter_Read_mC344B8C52B92A8BAB047CF5C32C4CEFDC57DDAB3,
	DateTimeOffsetConverter_Write_m82D4B6513B205F9BD02BEAB210BD045C68C05D9A,
	DateTimeOffsetConverter_ReadWithQuotes_m35836A466E40BB87C9822CF800E67ACD1EE557D7,
	DateTimeOffsetConverter_WriteWithQuotes_m1A16C1DEFFE193417900F336557154B516D31FBF,
	DateTimeOffsetConverter__ctor_m06CE7465B3E5F11F6D16C814D9C9D7D3DDA5E2DC,
	DecimalConverter__ctor_mEFCA590307B9F7A366B88EBF1C8367291F5EBCEC,
	DecimalConverter_Read_m7ADF3C4B1F4C7BF848182930287BDC8413973C7E,
	DecimalConverter_Write_m4E9821CBBA9A0A00531B73AF83B2C423269F970A,
	DecimalConverter_ReadWithQuotes_mBEF033E77CACAD377716659AEB8F65E43D99A1F5,
	DecimalConverter_WriteWithQuotes_m01155DC815E9662D1878A6EF1727A0ECE2A4FE39,
	DecimalConverter_ReadNumberWithCustomHandling_mF8C5C71390C989CB258349167D3346659DE23B0C,
	DecimalConverter_WriteNumberWithCustomHandling_m4853FB324B7D90EFA779252D47FBFAA1B9422267,
	DoubleConverter__ctor_m702EB5B29EE3EE96395C7B8E079FDA8D04F8B7CD,
	DoubleConverter_Read_m21651F7F263680E93072C2E7E3BE650A902ED877,
	DoubleConverter_Write_mF42CEC2F8C8BBF70FBC32631208E0DCB77C05009,
	DoubleConverter_ReadWithQuotes_mBE3F5B124A6865F00F6C9FA32700CDFF734934E4,
	DoubleConverter_WriteWithQuotes_m8E9CC8AA72F955F3EA2F6EF14D9488059695F234,
	DoubleConverter_ReadNumberWithCustomHandling_mA748D66176F5C229A8259642811DEBCB560209D5,
	DoubleConverter_WriteNumberWithCustomHandling_mBEB8266FCDB55DA191EF2365202ED3B965EFE7B7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumConverterFactory__ctor_mEEDAE7130310FDECC8E6F062133C6F6F7DA09DDA,
	EnumConverterFactory_CanConvert_m0D83650EBCD31A14AC620C285057BA4BAE2018F5,
	EnumConverterFactory_CreateConverter_m20B8EF5F66696851433404F45C06DD841CAD3328,
	GuidConverter_Read_mEF4830B9006FDBAA156B92C0AB97C842EDF7629C,
	GuidConverter_Write_m0AACDF686CBDE7FF89B770CAEFD925675BD718F9,
	GuidConverter_ReadWithQuotes_m84235BDFEEDAFB834A5BFC726AC8F963DCBA1A94,
	GuidConverter_WriteWithQuotes_mA6384193EE0C2385C13EB73B11EA60D12585C230,
	GuidConverter__ctor_m5630C5C8D370ED5766E306996B18A6010F84B0CA,
	Int16Converter__ctor_mBD70BF43C269316264BF5CE138887075824CC748,
	Int16Converter_Read_mE31ADD80DAE4D3D19FEBB76E6F3DC6265079DFDF,
	Int16Converter_Write_m68EA146B5E5FAB2613D49987F56E4167B7A1CE0B,
	Int16Converter_ReadWithQuotes_m8D4E3D1CE5ACA53EA0BD898691E57AE226ED1048,
	Int16Converter_WriteWithQuotes_m91D7C8064F59EA295300649FB1A9C6360CD453EB,
	Int16Converter_ReadNumberWithCustomHandling_m7A7386D05259D1B5E6E1F48BD1DA4906B2024F76,
	Int16Converter_WriteNumberWithCustomHandling_m429A09F34743A751F01CE58FF4DBB5F186408123,
	Int32Converter__ctor_m86BB105F3C950053BA4573D91081C4F512469649,
	Int32Converter_Read_mB293C305008AB7739BCC636615FBD10994B4B6D7,
	Int32Converter_Write_mC427AE572FB79274C73C1BC8429327935AAD4C8F,
	Int32Converter_ReadWithQuotes_m11B1466B71AD1FC1905E8FBD183F52B275128E3B,
	Int32Converter_WriteWithQuotes_m16E4C4192BA8C1393CEA69DAD8661859D19BF42B,
	Int32Converter_ReadNumberWithCustomHandling_mAE9D0F72AEC059596152C275E9A1696AEA06C5FA,
	Int32Converter_WriteNumberWithCustomHandling_m3E83221E6D55EF225BC8F9069C18BF27CF395EDC,
	Int64Converter__ctor_m75702B4968715592FEFF2ABEBBF794D23AE80768,
	Int64Converter_Read_mBA8C19892337C0D03B4C3A180CD6F72804952C21,
	Int64Converter_Write_m3C145DDE5EBFDAEB8931DA12216F39B64ACE0CF5,
	Int64Converter_ReadWithQuotes_mF8ABD27019D7E41A79CC841890361693399866DC,
	Int64Converter_WriteWithQuotes_m8EBAECFBF7E253E4CACF1B97130956CB1B710B61,
	Int64Converter_ReadNumberWithCustomHandling_m91CE222A62902E5B5444C923B6C563148F67FF41,
	Int64Converter_WriteNumberWithCustomHandling_m239802F34EDC0CAAA1C3BE3796B90B67C1072A03,
	JsonDocumentConverter_Read_m69B4B2572ED0150780A126562600339E51EB1020,
	JsonDocumentConverter_Write_m96D0D6A047E48022F4C0F3C4DC04FAD6B34E67D9,
	JsonDocumentConverter__ctor_mFDD94AA0515271294B40E5A9B6206031FAF8A462,
	JsonElementConverter_Read_m62556747E36B4E965742F1B2A62839C3F6D30F01,
	JsonElementConverter_Write_m875FEA16A976C30F89E2566036BC71B31941E14B,
	JsonElementConverter__ctor_m59F52B2210D31ED7532E9F9AE5D26241EF21E72D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NullableConverterFactory_CanConvert_mE4D9E8D6B7D755557333B4AA02162CAA0CE50A44,
	NullableConverterFactory_CreateConverter_m54C590DDA872BFE55C59D43A6BF7782ED090BF90,
	NullableConverterFactory_CreateValueConverter_m451CC34971C525BF2FFC89EE6697F936C1100492,
	NullableConverterFactory__ctor_mA68D5C5C18A8D880B91392F77C1543DA2D5927DA,
	ObjectConverter_Read_m255A07C57AFC85B5DAF7C030DA1A79BB3FB5D9F3,
	ObjectConverter_Write_m390A6C2A02AE9F6EED29A7D8612E4F02A7E13B0D,
	ObjectConverter_ReadWithQuotes_m773C97CC7A7D13547810511EE56585EDE01D2F86,
	ObjectConverter_WriteWithQuotes_mA18D561E027585854C2EAA45575FA2B9CF2DCB68,
	ObjectConverter_GetRuntimeConverter_mFC806C4DF191410398CA9E1CEF5C065180A95AEA,
	ObjectConverter__ctor_mE6AA92F654A4CBA2FF3F33BF7C4B2FF1EE6ADD64,
	SByteConverter__ctor_m0226F352C1E59905C1FC4534AFC979DBDEE59CD4,
	SByteConverter_Read_m59BF56D2CE77C7DF3C3B0B034623E93E3AF2AAB0,
	SByteConverter_Write_m03988D60FBAC4F1C6FD9148BC1F4BA22D1D4EEEE,
	SByteConverter_ReadWithQuotes_mDB3B3B13BEE5C5335F5DD39DDFDDD49CBB2457A6,
	SByteConverter_WriteWithQuotes_m168EC0103C1DE49348A48AE46C7AABBBB047EF98,
	SByteConverter_ReadNumberWithCustomHandling_mA25DD660D994657E4BC2189F9561AD61D92231EB,
	SByteConverter_WriteNumberWithCustomHandling_m68A3B9D795436708BA0305B5EEC02471A7115EFA,
	SingleConverter__ctor_m1D3CCBA0D91B726AF4A6357FE9F89EF212A75B96,
	SingleConverter_Read_mA58721E52C79C54351B95AF2026B3D688A8CE248,
	SingleConverter_Write_m1A327CCFAD5C3C82822713A6087182FDF2E286EB,
	SingleConverter_ReadWithQuotes_m084B0E1107CAE5AF77F6E35B1BC32181D2986FB8,
	SingleConverter_WriteWithQuotes_m2543083B1F856039184CA6C66FC26B212D695F0E,
	SingleConverter_ReadNumberWithCustomHandling_m46445002FE93669507284AF5D13C348A80A69311,
	SingleConverter_WriteNumberWithCustomHandling_m572662F9B95E308AE0CE3EC6C23652E664C88DA9,
	StringConverter_Read_mAD9A97FF3AE09596D8B066A5CEBBF2FFE4C77DD8,
	StringConverter_Write_mAC569F931091E5D12AE5CE67B4744812FF1B684B,
	StringConverter_ReadWithQuotes_m9701E6D776302D7F442A172B843CC7C99C15BC84,
	StringConverter_WriteWithQuotes_mEFA505055E63279D3D56A3ED4609A83E4031CD0B,
	StringConverter__ctor_m08EA132640C1FD185A138324ADC672C95253C53E,
	TypeConverter_Read_m504541EA26F6622097C748075A76FB5B9B3CDEFF,
	TypeConverter_Write_mDB6A094A2A298A57DB9BBAE51DED652F967FBF5B,
	TypeConverter__ctor_mBC50A16A2BAE8EB6797FE215E167909B962222AD,
	UInt16Converter__ctor_mB9FAB69B5C4A1AE5969F36F87EA01CB6B08C6AA1,
	UInt16Converter_Read_m0FE2A140946AAA98C66D4E55D72BAACDE7C9FF7A,
	UInt16Converter_Write_mBB50A24D83B482540E79F8CCE6C65DFB1429E878,
	UInt16Converter_ReadWithQuotes_m399BC8FCE941BE79E3962BAE4E9474BA1272696E,
	UInt16Converter_WriteWithQuotes_mAB636299084028B276F94889DC5B95B5DD38375F,
	UInt16Converter_ReadNumberWithCustomHandling_mD13135F3762A0B8CAD963023CF2985B85059C903,
	UInt16Converter_WriteNumberWithCustomHandling_mCA10802C9674C2C5FD1E936F2D79D6651243B078,
	UInt32Converter__ctor_mF2FDEC998CB93C31EC382FD294B78058F82B4781,
	UInt32Converter_Read_m65C9D0C61A51645F7B3126B4BF2E2A6FFFC94978,
	UInt32Converter_Write_mB2A61E88793C2756E786C4BE6B7A1618C5B30926,
	UInt32Converter_ReadWithQuotes_mB3192ED36F55E17EBA521A5C63461C65E84F8C36,
	UInt32Converter_WriteWithQuotes_m770F506AEF837ECFAA145834E4CBF672877C7843,
	UInt32Converter_ReadNumberWithCustomHandling_m1CB5FAAE836611E01CD3A92ACA9C09DD5AA0061C,
	UInt32Converter_WriteNumberWithCustomHandling_m9518AF0B62A0E9E224888CAA2C6CDA4A5F7AA374,
	UInt64Converter__ctor_mD350ADC6E5C9FF3209C8576B823405605E977143,
	UInt64Converter_Read_m17AD682D5DF5BF044D364FDD5295A5CD7C9D8AD5,
	UInt64Converter_Write_m3F4F83DA9482F95D671343FBF31D3E03E8EB372D,
	UInt64Converter_ReadWithQuotes_mE4FA5A8A469D62A99B0139B2C0C7F1F065CDA09C,
	UInt64Converter_WriteWithQuotes_m581D380D3BC23B7FB980DA2623540984F1E30282,
	UInt64Converter_ReadNumberWithCustomHandling_mB697FCA3A9C7C17B7200DF5F740A3D27A75CD593,
	UInt64Converter_WriteNumberWithCustomHandling_m42ADE1C96A5060435CDAC28E2E68F37A419DC63B,
	UriConverter_Read_mFB7C5E401604C8E774BD25D31C3BC110C796402B,
	UriConverter_Write_m33C6F8B59440D93ED80906717A73F2C0B958FA64,
	UriConverter__ctor_mA7F8DFF219CE29B1F0FFC072A23833E44F033340,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD8060E810F4B51B2479D48F4E38A17EFDA20CAF8,
};
extern void BitStack_get_CurrentDepth_m89D98204DBF12922D8C94B3447F28558A607561E_AdjustorThunk (void);
extern void BitStack_PushTrue_m3E753BB8ECFB2193C4F9097FFBA0FB9C502DCCC7_AdjustorThunk (void);
extern void BitStack_PushFalse_mDD8A80022173EAF25EC74EFF7CFA03A6054F8D25_AdjustorThunk (void);
extern void BitStack_PushToArray_m20AF975C1EFDCFAA0D410D815A8B062A9CCECA23_AdjustorThunk (void);
extern void BitStack_Pop_m65E1670A3A560768AC7DD19C52A9D5B8C4CE2482_AdjustorThunk (void);
extern void BitStack_PopFromArray_m205D4F9EEB5B51B6074124CEE2F8974AA2A950DF_AdjustorThunk (void);
extern void BitStack_DoubleArray_m589E01AA096E64AE9187C458D962BE8CA0E92051_AdjustorThunk (void);
extern void BitStack_SetFirstBit_m9F284C76F69FBAE879387EE0B8B190B6967840BE_AdjustorThunk (void);
extern void BitStack_ResetFirstBit_m6A4CD054DF57522D30866089E313E8353A6889A3_AdjustorThunk (void);
extern void DbRow_get_Location_m64A1FE8677C03A2ED3384727229059262AD3F149_AdjustorThunk (void);
extern void DbRow_get_SizeOrLength_mD035DFC369AC701D3930C33A159A067B00913772_AdjustorThunk (void);
extern void DbRow_get_IsUnknownSize_mAB1898B5C44A75E8F3F5737B39E22A9829052B9E_AdjustorThunk (void);
extern void DbRow_get_HasComplexChildren_m7FB54FB106AA84F5168A030E00E21B2687C58D09_AdjustorThunk (void);
extern void DbRow_get_NumberOfRows_mB356515E008E677E4E79834F315165BD49A825DF_AdjustorThunk (void);
extern void DbRow_get_TokenType_m25D3999BC52C9991FC22A97FEC677997068D1E7B_AdjustorThunk (void);
extern void DbRow__ctor_m83855B245AEDA3542D69D4181E26D625780729FF_AdjustorThunk (void);
extern void DbRow_get_IsSimpleValue_mF9C4D4A9AA674E10C15D23BFDCEE3B7A9CE78638_AdjustorThunk (void);
extern void MetadataDb_get_Length_m358C82F2ACD118F1A31750A408DE4A80D9706AC1_AdjustorThunk (void);
extern void MetadataDb_set_Length_mDEED679131C22B2B98BD28E71CABB539B01E79C7_AdjustorThunk (void);
extern void MetadataDb__ctor_mF5690ADF4EA9661465EF315D0505E445C6E222FB_AdjustorThunk (void);
extern void MetadataDb__ctor_mADDFB275C40C9946CEFB568AE37356AEB5FBE14D_AdjustorThunk (void);
extern void MetadataDb_Dispose_m65983FCE3C671C0E078BA5B7192D66EA6224D218_AdjustorThunk (void);
extern void MetadataDb_TrimExcess_m4BAD9F80840A78E601E56F2EAE6822A02267A1CD_AdjustorThunk (void);
extern void MetadataDb_Append_m70BEF3B5C094C35F03E20E23CABEF3905616B61A_AdjustorThunk (void);
extern void MetadataDb_Enlarge_m1D564D4B6A73CB588BBACAD06E20CAC2CC87BE91_AdjustorThunk (void);
extern void MetadataDb_SetLength_mF874FB9D92FF15E65645B752D2F16693FD85EA53_AdjustorThunk (void);
extern void MetadataDb_SetNumberOfRows_mE9A9288F6CFE360E85CC8E0A50AE964D43C470E2_AdjustorThunk (void);
extern void MetadataDb_SetHasComplexChildren_m23B89D6C169828F97B1138999C88AD2C334E46D9_AdjustorThunk (void);
extern void MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m93260D165FFDE321BC10AEB51ADA16EA571BDA92_AdjustorThunk (void);
extern void MetadataDb_FindOpenElement_m764A0CA02C9B76F9FB4501BEC77D157A87ADCC8C_AdjustorThunk (void);
extern void MetadataDb_Get_m54EC69D0C0A5AFA94790823E043C9C5AF611E879_AdjustorThunk (void);
extern void MetadataDb_GetJsonTokenType_mFE77CF70D471447224C7088BF78347AD9922BDD8_AdjustorThunk (void);
extern void MetadataDb_CopySegment_mE2A805BC0A4A35436649F6DFF69A286C416A056C_AdjustorThunk (void);
extern void StackRow__ctor_mE83A64F557423B2C1417CF2CAA47FCB6DAE79597_AdjustorThunk (void);
extern void StackRowStack__ctor_mC2D0A937968645C04762AE37C6F9196811FC60AD_AdjustorThunk (void);
extern void StackRowStack_Dispose_mE69DCE4BA09A561767066C061517880FC9FD240E_AdjustorThunk (void);
extern void StackRowStack_Push_m2EB6DF0A8051783F40F7911AC981072DB6DF3AA0_AdjustorThunk (void);
extern void StackRowStack_Pop_mB7727C6BB4EF6384ADB6A055F63AE463231B3731_AdjustorThunk (void);
extern void StackRowStack_Enlarge_m9CAB452BCBA0F7795608ED801F3741F645037CF0_AdjustorThunk (void);
extern void JsonDocumentOptions_get_CommentHandling_m250724FEAA7D45E779F026A8E2AE1792E8CC0223_AdjustorThunk (void);
extern void JsonDocumentOptions_get_MaxDepth_m910F496990C5F7315734B42E49D6B46D7D7FB146_AdjustorThunk (void);
extern void JsonDocumentOptions_get_AllowTrailingCommas_mB695E7244BD9C08298D2A980CE3C8D6C69D0F2ED_AdjustorThunk (void);
extern void JsonDocumentOptions_GetReaderOptions_mB45CDABD01DCD171F5576B841FD948C183A3F4BC_AdjustorThunk (void);
extern void JsonElement__ctor_m08BBC84BCD6BF2156EEA51E7E367E772C44E8C9C_AdjustorThunk (void);
extern void JsonElement_get_TokenType_m2DE21D16B1E01312F72BCD8C9E6EABB3C4CDD7B8_AdjustorThunk (void);
extern void JsonElement_get_ValueKind_m34757B769FC468837C793AAB42507082105A1AE7_AdjustorThunk (void);
extern void JsonElement_GetProperty_mF3F895EADD2EBC27D13528083FBA7A8E754C02B9_AdjustorThunk (void);
extern void JsonElement_TryGetProperty_m1F48BA59A2FF4DD5DEC6B04E2DFEF7785CAB89DE_AdjustorThunk (void);
extern void JsonElement_TryGetProperty_m915535CFE0D507A447594BD147BE125D20EE0B88_AdjustorThunk (void);
extern void JsonElement_GetString_mC81CE313F3CF6ECFCC6374545F7B598A244FA53E_AdjustorThunk (void);
extern void JsonElement_TryGetInt32_mA8E8A07BDB3F2CD9315116A391DA2F31023EA506_AdjustorThunk (void);
extern void JsonElement_GetInt32_mE298927691DCC4168662962C7B7FF3749003A5D6_AdjustorThunk (void);
extern void JsonElement_GetRawText_m57611974A95EAA4466054192D51EAC5A1487249D_AdjustorThunk (void);
extern void JsonElement_GetPropertyRawText_mE33C03151821E238E589A59D52856D5E796CBEC5_AdjustorThunk (void);
extern void JsonElement_TextEqualsHelper_m388909D510BCC8E2EE370A17626A111A77E362D5_AdjustorThunk (void);
extern void JsonElement_WriteTo_m1AC8B59D427C19E13F2134BA8964477C9B3E4F20_AdjustorThunk (void);
extern void JsonElement_EnumerateArray_mADD6A7BD2664B80DC5FEF55DE964046AADA2AC28_AdjustorThunk (void);
extern void JsonElement_EnumerateObject_m25AB68731BDC8CA13253A206EABEA1F219AEA40C_AdjustorThunk (void);
extern void JsonElement_ToString_mA0336A268F12CE4735C0307F12F4717EA5A24B29_AdjustorThunk (void);
extern void JsonElement_Clone_m2D1217819F1C1A069A7291948BD2CDE7D58B090B_AdjustorThunk (void);
extern void JsonElement_CheckValidInstance_m37FCD235BD0B2BF84200713D1CFB2D040A7A8439_AdjustorThunk (void);
extern void JsonElement_get_DebuggerDisplay_mD7A4D8389DA4D9A8790C4FDDFEB0A20101CD8883_AdjustorThunk (void);
extern void ArrayEnumerator__ctor_mE63EDB3BC6E93B2C7E2A40C824553B06F853E68C_AdjustorThunk (void);
extern void ArrayEnumerator_get_Current_mF52CB9638D47BF1F300A8D90DE72F16FE7760A50_AdjustorThunk (void);
extern void ArrayEnumerator_GetEnumerator_m84826411DC72414B035DD2383B98995030DC1D64_AdjustorThunk (void);
extern void ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_mFA06CBD3D1326936DCA477D78774A7D5244051B2_AdjustorThunk (void);
extern void ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m7E27A28429F4B25E812197A40277394C3898FBF4_AdjustorThunk (void);
extern void ArrayEnumerator_Dispose_m013242AA10DC5F3B889EF39004D9E62C88484F36_AdjustorThunk (void);
extern void ArrayEnumerator_Reset_m4E108783AA0C9538A0723F914B99F999D0907825_AdjustorThunk (void);
extern void ArrayEnumerator_System_Collections_IEnumerator_get_Current_m0625474B01CE733D09EC696C2D3943FEBDA9D8B9_AdjustorThunk (void);
extern void ArrayEnumerator_MoveNext_m4A4255B7F6ADA0D569E1644355F8542007BC99E8_AdjustorThunk (void);
extern void ObjectEnumerator__ctor_mE6CFBEE462A2DAD097E3CF38C5E1F93B3A72575A_AdjustorThunk (void);
extern void ObjectEnumerator_get_Current_mEBA991658CFB79D8EC48C940587E8F56E5FCF6A6_AdjustorThunk (void);
extern void ObjectEnumerator_GetEnumerator_m6431E7E6BAF73BDCC9B211871E2046D8C5557C9F_AdjustorThunk (void);
extern void ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mFABC61E9837E42D4B98EDD2EB829169691151DA4_AdjustorThunk (void);
extern void ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m152A46470709CCB7745C15E69C8B5E3CC379F292_AdjustorThunk (void);
extern void ObjectEnumerator_Dispose_m05CE64A63964F3D868775EC395EF5AFCBFD0C772_AdjustorThunk (void);
extern void ObjectEnumerator_Reset_m2375949AFCB32D333217A2F8674E48BF3A948DC7_AdjustorThunk (void);
extern void ObjectEnumerator_System_Collections_IEnumerator_get_Current_m819E29B32A75D77D74B6CA9F65F28BDFF7C7E6BC_AdjustorThunk (void);
extern void ObjectEnumerator_MoveNext_m9EF4007222C1BFD3D5BEF8CC03A76CE222F4D859_AdjustorThunk (void);
extern void JsonProperty_get_Value_mF766C4EFFB858EEAD2A009AF55BB8B51095AC96A_AdjustorThunk (void);
extern void JsonProperty__ctor_m0C3653F7D2BC6F745C523466CA7294E355FABB13_AdjustorThunk (void);
extern void JsonProperty_EscapedNameEquals_m9070FF7B8371466D91AF6A4E38D5699FD5107D86_AdjustorThunk (void);
extern void JsonProperty_ToString_m1FCFE27631E5AC6F3238874289B06303471487B0_AdjustorThunk (void);
extern void JsonProperty_get_DebuggerDisplay_mC4D1DE967EC567BBE7888EB699087916594E59E2_AdjustorThunk (void);
extern void JsonEncodedText_get_EncodedUtf8Bytes_m90590ADF42E78917035B3FA5FE2B23661DBF945B_AdjustorThunk (void);
extern void JsonEncodedText__ctor_m0EF268C655A1728E129E94D60BAA71894F1BD77A_AdjustorThunk (void);
extern void JsonEncodedText_Equals_m122DC2FCB6321FDA3F63E1973CF385FBB3C9FB69_AdjustorThunk (void);
extern void JsonEncodedText_Equals_mCE77AC6B7E663C8B7D8412B394E91F715A5BE682_AdjustorThunk (void);
extern void JsonEncodedText_ToString_mEC405B95F27BDBC3814C264035F3B0151621800F_AdjustorThunk (void);
extern void JsonEncodedText_GetHashCode_m2B447D2F286EC689D45FA77B4ECCD9DD072D2A70_AdjustorThunk (void);
extern void DateTimeParseData_get_OffsetNegative_m0D054B07A34B50D970C5751B503F342C14F5F3E9_AdjustorThunk (void);
extern void JsonReaderOptions_get_CommentHandling_mC784945C4A7D8BF00627E0D71CAF144E059CA45B_AdjustorThunk (void);
extern void JsonReaderOptions_set_CommentHandling_m840CCCE2E2296122C6AC1E8B8E75D1EEB54557E7_AdjustorThunk (void);
extern void JsonReaderOptions_get_MaxDepth_mE9E196FD22578986617CBC0B6104934965E7DE18_AdjustorThunk (void);
extern void JsonReaderOptions_set_MaxDepth_m7CA249F02B7EFB090EBE8DD1A81134BF15890127_AdjustorThunk (void);
extern void JsonReaderOptions_get_AllowTrailingCommas_m6BE15A1BB0597C203011627DCF4E9CDDB42049BA_AdjustorThunk (void);
extern void JsonReaderOptions_set_AllowTrailingCommas_mB8C99444AB5B8A9D50FDDD8E2CDC8F473B6D0641_AdjustorThunk (void);
extern void JsonReaderState__ctor_mBF6D934854F0D3489AED812C79FD31BD7DAEDB21_AdjustorThunk (void);
extern void JsonReaderState_get_Options_m66769D6D489C3E79567B39C83DD8464073804370_AdjustorThunk (void);
extern void Utf8JsonReader_get_IsLastSpan_m7B992A026074DDEFE7B18BBAF1D35F7FCC67307D_AdjustorThunk (void);
extern void Utf8JsonReader_get_OriginalSequence_m695F93AC97FC88673CF1C5B42CB2781FCC421DF3_AdjustorThunk (void);
extern void Utf8JsonReader_get_OriginalSpan_m5CE4EC409D428FFA49D657B1EB8095A5A1BF75FD_AdjustorThunk (void);
extern void Utf8JsonReader_get_ValueSpan_mBABD1CCB678681EF0BAB792BD92B1A667B5280EA_AdjustorThunk (void);
extern void Utf8JsonReader_set_ValueSpan_mCB290F34DBE06C916AC61FCD97325363E6DF8840_AdjustorThunk (void);
extern void Utf8JsonReader_get_BytesConsumed_mED511A2A33941953398E81C2630928B1BE296C60_AdjustorThunk (void);
extern void Utf8JsonReader_get_TokenStartIndex_m065C1E55350901EE451793D91D45C6BFD6727825_AdjustorThunk (void);
extern void Utf8JsonReader_set_TokenStartIndex_mD8DE6AC3843B60206928FF65843B1063725E11D1_AdjustorThunk (void);
extern void Utf8JsonReader_get_CurrentDepth_m27479A89058293B9D65E5245D4288745BC187941_AdjustorThunk (void);
extern void Utf8JsonReader_get_IsInArray_mA7603C29F57D874052CDED9AC19EE23B18786A16_AdjustorThunk (void);
extern void Utf8JsonReader_get_TokenType_m589FFFF94F1DF061F5BAA87791CE6EDC77B153FC_AdjustorThunk (void);
extern void Utf8JsonReader_get_HasValueSequence_m0E4BC52E674A56D3F8FD1EF9F56BCA86F7851658_AdjustorThunk (void);
extern void Utf8JsonReader_set_HasValueSequence_mA04C744A5B061CF11B292F4FA30685D8FF959819_AdjustorThunk (void);
extern void Utf8JsonReader_get_IsFinalBlock_m044741A01CC206E766FFC12FC40D20F5F3D20C16_AdjustorThunk (void);
extern void Utf8JsonReader_get_ValueSequence_m8BFD79D1CB26B6B8C081CC01F855EFD3A48E74F3_AdjustorThunk (void);
extern void Utf8JsonReader_set_ValueSequence_m14F871806DC91F89B691C08FDFDBA71DB941C6E4_AdjustorThunk (void);
extern void Utf8JsonReader_get_CurrentState_mB19357F20C4CBCFDD92278B4D2AEF65D82ECDA5A_AdjustorThunk (void);
extern void Utf8JsonReader__ctor_mCA5FF66B2E66D867A4F30167AADF35FA7B66D00F_AdjustorThunk (void);
extern void Utf8JsonReader_Read_m48C98153390FA527AB0DD5449555BB69A2881816_AdjustorThunk (void);
extern void Utf8JsonReader_Skip_m5E206D71CC77C21D2329A9F809AC53367CC74091_AdjustorThunk (void);
extern void Utf8JsonReader_SkipHelper_mBD2F0EAE5EFE06BA861442584F998D344B9D6316_AdjustorThunk (void);
extern void Utf8JsonReader_TrySkip_m30F2CB8BDCDD32BCE7C345A84398E3ED5AE38AC5_AdjustorThunk (void);
extern void Utf8JsonReader_TrySkipHelper_m403637E1D8D991978EC5C7D0D45B7AD39203E865_AdjustorThunk (void);
extern void Utf8JsonReader_StartObject_m80A9CBB11BC789B32FD92F9D951807CDA73F88EE_AdjustorThunk (void);
extern void Utf8JsonReader_EndObject_mF882CCEFFE2075AE1BA7A8B82F787F3472ACD900_AdjustorThunk (void);
extern void Utf8JsonReader_StartArray_m9DC35176A3D6D63A0F0CE6E12EF81057795E5B2D_AdjustorThunk (void);
extern void Utf8JsonReader_EndArray_m595FA16248A8A07CA4B68CF12789592F941E1149_AdjustorThunk (void);
extern void Utf8JsonReader_UpdateBitStackOnEndToken_m5681C904065EB11EBAB2CE8E3FD2E0323DEBDBC7_AdjustorThunk (void);
extern void Utf8JsonReader_ReadSingleSegment_m4D6BE7EA668855AF1A3DC170E54996088B0C08A2_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreData_m0849ADA8E4F15E0C12811A4A1144D964F7AED2DD_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreData_m1E2DEEDB648DF43DB37BC3044E8D633D16FB076E_AdjustorThunk (void);
extern void Utf8JsonReader_ReadFirstToken_mF7DA287E1C8E5B7C1C885339DC4C1DC3E48C38F6_AdjustorThunk (void);
extern void Utf8JsonReader_SkipWhiteSpace_m7E57D7BF978E4641391C9516F38820A2CE0556AA_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeValue_mA7A960E791A5055383C64E7DD9AA669E71115799_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeLiteral_m2A3B4794861C4EF32D8370B2D73C6412000660FC_AdjustorThunk (void);
extern void Utf8JsonReader_CheckLiteral_m0906217EC5F76E1683DC47AFE73B4B56126791EC_AdjustorThunk (void);
extern void Utf8JsonReader_ThrowInvalidLiteral_mFE5C045DA40FA4649AFE040AA0C332C753D8AFC5_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNumber_m4B68EF9DC535940BA5B09F601BE7A469C25482BE_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumePropertyName_m4D54B25BFE01BFFF5976F17D7A9D5D7C166EACD0_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeString_m0A2F49639EADB6BE4495CA600CED86A892F3EC82_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringAndValidate_mD3DCAF6E986C245047B6B08FFBB149269E1941E5_AdjustorThunk (void);
extern void Utf8JsonReader_ValidateHexDigits_mEB6781B13A74BC4EE94ECEE64A8362067945893E_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetNumber_mA537AE8E9D9E0C1AA6EB929872959D9E0D1C1AAA_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNegativeSign_mEAB4DACF3FA5003223B06958EB7BA236E6D3CD57_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeZero_m237EEC547BCEE5ACC2FA5C371DFC4C2F8D7C1184_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeIntegerDigits_mD3C9A55484E0188A9DF616C6C43CDC7332FD166E_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeDecimalDigits_m32313C386676761CA3EDDB3F80EAD756C8B0E76D_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeSign_m9D31DED0190794A96D6BD43EFD2D01D4FB326F12_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenOrRollback_m734D5BDD909FE3FC93D2D62FC823048553C28BF3_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextToken_m3CA8B80E08B12A2E034EE6913DA5F8185A32361D_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_m6B24CB081250AF44DCEA9938616E206978FBE8BB_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllComments_m4B68EAA87BA8197DFB88BEA53B67B7C5D5E5BF1F_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllComments_m0626448D38FC1F68C36361A5CE32F753E3C3A9B4_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m9EBCD5E4D2F5B96282EA227FC60DECFD12DC322D_AdjustorThunk (void);
extern void Utf8JsonReader_SkipComment_m91E3271B505861F135B1F7CBEA89FF9D03DBFEC4_AdjustorThunk (void);
extern void Utf8JsonReader_SkipSingleLineComment_m080A7403DB593F785293CEB04C935F97608A47A5_AdjustorThunk (void);
extern void Utf8JsonReader_FindLineSeparator_m1A63CFD96D293C6851A62C87C0829B08268CC50D_AdjustorThunk (void);
extern void Utf8JsonReader_ThrowOnDangerousLineSeparator_m9DB458C5580C40BD100AC7993DFC3445EA6D1DCC_AdjustorThunk (void);
extern void Utf8JsonReader_SkipMultiLineComment_mDDE267D4B9BCE906A04E91290B75E2E69C943839_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeComment_mDDA4B89EB77DF4767A97289E5AE1A45C29875605_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeSingleLineComment_m6820206B83C4C8FB1F2D85DA65DA783F0C4B0848_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeMultiLineComment_mDF378938E725595AE26E07EBC9448EBC122BF2A8_AdjustorThunk (void);
extern void Utf8JsonReader_get_DebuggerDisplay_m90465723303F2563AD6293AD79277A5101A811C8_AdjustorThunk (void);
extern void Utf8JsonReader_get_DebugTokenType_m5C7DEDDF27CDA9E961186CA3261276037A38961F_AdjustorThunk (void);
extern void Utf8JsonReader_GetUnescapedSpan_m2180AE402D0F1D887B57335433E1B98A8F6A0030_AdjustorThunk (void);
extern void Utf8JsonReader_ReadMultiSegment_m7A789B154E6126292F523C039F86A28936652DA7_AdjustorThunk (void);
extern void Utf8JsonReader_ValidateStateAtEndOfData_mE44BD918232309740E30D9A285F6F63A2EF5E76C_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreDataMultiSegment_mB2C1C0751141A6B4B150D6A1F310560936C69C18_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreDataMultiSegment_m71B085F039270BE038C5F171206D7F198AB21FF5_AdjustorThunk (void);
extern void Utf8JsonReader_GetNextSpan_m2B9D198EE611302CE973A7E6268D835C5164553B_AdjustorThunk (void);
extern void Utf8JsonReader_ReadFirstTokenMultiSegment_m63D211F1A922E4DD4BC8A801BCECAC0CCEA6267D_AdjustorThunk (void);
extern void Utf8JsonReader_SkipWhiteSpaceMultiSegment_m11BAAD269031D687444204D40BB0EA7C6E2D9427_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeValueMultiSegment_m913D0F4B616A77E94761359A3617D2EE8102A95D_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeLiteralMultiSegment_mBA1B229E2B0682719CDC963E68E9E8EF203D9610_AdjustorThunk (void);
extern void Utf8JsonReader_CheckLiteralMultiSegment_mBD77DA207039A439C116B70313DA1C1EAE1E79CB_AdjustorThunk (void);
extern void Utf8JsonReader_FindMismatch_mFC922E786DE159AC6D606BE3AB07F15BA6743BD0_AdjustorThunk (void);
extern void Utf8JsonReader_GetInvalidLiteralMultiSegment_m60F4782DDD6F88D7898813D5C0468DD1A1BEAF55_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNumberMultiSegment_mE723026EE74A4624867675A37F7C8E267998EE3C_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumePropertyNameMultiSegment_m3F71002973F225C8F6FD075BC3C76A506D7BB72E_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringMultiSegment_mB24E40054CA1A60B8210FC43CF23EF6F8BE3F151_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringNextSegment_m3CCF0CCD09B5856298887E1096CCE6F02A60D0A9_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mA8AD93CE8405F63672B369F6848EC276631D5B33_AdjustorThunk (void);
extern void Utf8JsonReader_RollBackState_mE1AB8FF05072C2D1C921F4D97E56122C15E905AD_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetNumberMultiSegment_mAB46FB985C042DAF2B0F290A19B221AB58FDBAB1_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNegativeSignMultiSegment_m135AE9A283C44CD51F7BEFF2A2E0FD9890FCED3C_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeZeroMultiSegment_m281FC6658603949FC90711BE6C81B8C3108B9E2D_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_mF5A5A69C0744D08C722001E1E049D5D1703A9308_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_mE037A2CF96C775C86599C5A61F9E7EEB2C411140_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeSignMultiSegment_m05A9CA258362283F551D2CC8A7B43DAAA1F99D7F_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m8A4377711B0F17F01DE5CBC05DC2A26109647E43_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenMultiSegment_m4CA61D5BA79665005FBE0CE4E9694F501351086A_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mA24E85BCB4C5A5FF4C5BCE2BCB0AF267FFF92E42_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_mAC8F7760E50129CC670532D1BABFCE46303011AF_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_m14037635702E57A5503363E26FD8907DABF504B4_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_mE0992B12538E8587B695ED2D6EA28CBACF2EFE2C_AdjustorThunk (void);
extern void Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m050F36798BF507E32B4C696A1BA9DBF566543055_AdjustorThunk (void);
extern void Utf8JsonReader_SkipCommentMultiSegment_mF89DEC8E6F0EBCD5B6472A4C37949F9C14C69B97_AdjustorThunk (void);
extern void Utf8JsonReader_SkipSingleLineCommentMultiSegment_m010391D904BA6ACBF5FCB5034CB8EE91C9C3E091_AdjustorThunk (void);
extern void Utf8JsonReader_FindLineSeparatorMultiSegment_m1517AF51E8F05AD9F0B0E3C8247A737D4756D9F2_AdjustorThunk (void);
extern void Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m0CB9C5BCC5C677E851EE114F5AFAA5DC1EE42CDB_AdjustorThunk (void);
extern void Utf8JsonReader_SkipMultiLineCommentMultiSegment_mCD3026993722AB7389E158550B086BD01A090C33_AdjustorThunk (void);
extern void Utf8JsonReader_CaptureState_m8BBAE4358A88D8BAD2FB811C910F1192BCA21325_AdjustorThunk (void);
extern void Utf8JsonReader_GetString_mF6ACA3547007BAAD951423AD47FC7C8FB1EE1C44_AdjustorThunk (void);
extern void Utf8JsonReader_GetBoolean_m908285BF5B202C675CBEE893F2B41E9BC5F2806B_AdjustorThunk (void);
extern void Utf8JsonReader_GetBytesFromBase64_m0B0E3C7FA55C6B1BF7B6513D80CB4159543F5F67_AdjustorThunk (void);
extern void Utf8JsonReader_GetByte_mA2DAAA2A21699AB68FD020A0BC41AC8D3935C439_AdjustorThunk (void);
extern void Utf8JsonReader_GetByteWithQuotes_m3BFD4FEF30EBA5DC312DC07D7C6EC9D132B591B9_AdjustorThunk (void);
extern void Utf8JsonReader_GetSByte_mF0C15DF1DFAAFED79A5FF6527BC1EC6DF8D6D541_AdjustorThunk (void);
extern void Utf8JsonReader_GetSByteWithQuotes_mBA0D6B8A92034B6683EF86A600395EB38129922C_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt16_mD10C668BD0F694A704B5A7299ABFDE45B9DFD4EE_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt16WithQuotes_m3BA45015843A11D2A9C67FA2B83E83DB0AF69EC1_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt32_mB50F3838DE3DC950FC7FFDFC24721A136DA76BC8_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt32WithQuotes_mF05F615F6638E5308D86A63939D7639221402D52_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt64_m68E1F5DB10DBB2E490FE90D1CDBC1C5E81EFDB8D_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt64WithQuotes_mE8901124409E515BBAB819575C5851BD854BE8ED_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt16_m91B334F666BA8952300156AC744D340C01A0FA68_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt16WithQuotes_m2BA11333E17BC23C70A1CA4E90AE7960BA808CB1_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt32_mCAAA27CB167078F0E49C896E0D38F71DA98A1DA1_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt32WithQuotes_m2221571EDFBA7F56DC5E51B2AC8F0318548E6DD8_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt64_m04FC93AD8353898C2E5383CBF9616A0D03DB05AF_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt64WithQuotes_m80FDBEC80D9C007947133F9D259BC28EC239CC53_AdjustorThunk (void);
extern void Utf8JsonReader_GetSingle_m65F5DCB28FD9BE75EDBC6C94B20F26E32C3305DB_AdjustorThunk (void);
extern void Utf8JsonReader_GetSingleWithQuotes_mA66BC371F801F4DA57D5701543059442BD108597_AdjustorThunk (void);
extern void Utf8JsonReader_GetSingleFloatingPointConstant_m37AAA7DD71B9E5379C449141C075EED6C4BDD4AD_AdjustorThunk (void);
extern void Utf8JsonReader_GetDouble_m1D71F0D17190B5ADBF720FB13597184F00B83159_AdjustorThunk (void);
extern void Utf8JsonReader_GetDoubleWithQuotes_m62F2D7D5B21BCCCA007091EFC60D69F1B6B1A1B5_AdjustorThunk (void);
extern void Utf8JsonReader_GetDoubleFloatingPointConstant_m623ADC6DD4A7D49297B7B7306DB045A9B295B91A_AdjustorThunk (void);
extern void Utf8JsonReader_GetDecimal_mFEA9D944822478D418E3275FEF14E78CAECE0BA7_AdjustorThunk (void);
extern void Utf8JsonReader_GetDecimalWithQuotes_m7421A53B8EDCB1669FF8591DE10A9407E9224141_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTime_mA26D2BA988CF28A40FC962FED1145B29BFE57067_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTimeNoValidation_m4516B03D0EF902C15DFA9BAB9BF057ADC647B6DF_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTimeOffset_mE45024C43B2BE11C35AC5DFA1B247F0C5C888A3C_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTimeOffsetNoValidation_m62B6E29DF9475F352B974BDAAFC391B6133F7A7B_AdjustorThunk (void);
extern void Utf8JsonReader_GetGuid_m7A0ECB26F40A0A64EE37C7393F109B3E63193CB5_AdjustorThunk (void);
extern void Utf8JsonReader_GetGuidNoValidation_m5DF916F1E98E9F975BF7D48BD9760CAD9771CEC9_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetByte_m57093639B039FA045AE217E89A38023A51F25877_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetByteCore_m5975171C440BA1DB0F5946CEE3A95AD25E90AA83_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetSByte_mF7778E9C90F9D7C8CBE321D9C4B3859CE02B9106_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetSByteCore_m8789EC6277347F2746BACBE65069C7E63B3ED6EE_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt16_m5E65C5335E39AAA608EC42137A1F9909B61C6085_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt16Core_m489F57E73BB2F417F88E7A98DA2ABB2705B3AD8B_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt32_m93479A8A49CE3407F4F48F9F3311D8CCECDFC7BA_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt32Core_mF9C2BAB3D4887F47FD89DE68149E0E4FD95FD7F6_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt64_m04985C755DD8030B3AC7D8045291994F453D42A3_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt64Core_m9984ADE08AAFE1243D84F6B9F3F6FF3D59B6D5CF_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt16_m018B28F35D595DDDBB3A104F69E2BAB463CDC274_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt16Core_m413B4DF3E32855458F822546C8F91B0138F71816_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt32_m3568E2AB280AF64148B26E5A570DE313B5F6A557_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt32Core_m52DBDE64A8BE6BBFB4AC92E782FA38637E88B79D_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt64_mA1BFC2EDB1096EEA85D3F3992A6C33B599769A63_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt64Core_m3DBEB9BA3D05D681BF354807C9D16B725CC7256D_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetSingle_mF21A462DD40F497AE887B4F93EB3F329632E6D34_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDouble_mA5FF188644DBB6DFF353DA7887B0A28B7322EDD6_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDecimal_m9129A3234F32FBF6B0C85C3F4D2AB8C5F008F297_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTime_m5D9B9E2E3B345769B3D1505271DBE6F7EF852895_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTimeCore_m08A0D16A29B1B5071737A3E3C2A88FA68EC68A14_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTimeOffset_m33ADB6C9F6844507D161D845D20E471322D84587_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTimeOffsetCore_m97770A3308970C50091E12FA977F276276D776DA_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetGuid_m5D97B625335AB390E15C8AA7A4D4794B8BA868A5_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetGuidCore_mB8171BAB36B60E93D015CA3CF34B48BC53C7C171_AdjustorThunk (void);
extern void PartialStateForRollback__ctor_m0A2CA55E0DA1307DCE07752039D69E1E171C6178_AdjustorThunk (void);
extern void PartialStateForRollback_GetStartPosition_mD8F495352B433FEE2C9D27A58FDBAC331C418D63_AdjustorThunk (void);
extern void ParameterRef__ctor_mAE5D0234AF0B54F1B4F1C0F808F635329A9928A2_AdjustorThunk (void);
extern void PropertyRef__ctor_mA2ED2603D61FFE0F81222405419BB00EE8455EF4_AdjustorThunk (void);
extern void ReadStack_get_IsContinuation_mD01FD9A2DAC4FD9C3E805929E0104AAED9EFF95A_AdjustorThunk (void);
extern void ReadStack_get_IsLastContinuation_mA8012A8628BE1311AE139DD8DC7D98E902EACF7F_AdjustorThunk (void);
extern void ReadStack_AddCurrent_mF400E1F933A12C71C19C3A4DFCBE009008B823D5_AdjustorThunk (void);
extern void ReadStack_Initialize_m7E7F3ED59DEEFF3DC132C8773C83EE001A238EAC_AdjustorThunk (void);
extern void ReadStack_Push_mA7CB7FCFBF6F542FD626BDE571D54F1D136B3C0E_AdjustorThunk (void);
extern void ReadStack_Pop_mD314744BCA164729B64A75EAAEB43FC240346DAB_AdjustorThunk (void);
extern void ReadStack_JsonPath_m86FCB866216FA16E285600FB62136552D1CA7DED_AdjustorThunk (void);
extern void ReadStack_SetConstructorArgumentState_mB792C92FEA853A82C6564EB5F41E3A2ED909B992_AdjustorThunk (void);
extern void ReadStackFrame_EndConstructorParameter_mE5D6B2F709289B69DD06F58040B1AA47DF4C4EC7_AdjustorThunk (void);
extern void ReadStackFrame_EndProperty_m6BBD5C956CCF9412B4B122B5E2501681DE66EEBA_AdjustorThunk (void);
extern void ReadStackFrame_EndElement_m4F6CA0BDACF8EEAB43A942243D21DF32C5C26C8D_AdjustorThunk (void);
extern void ReadStackFrame_IsProcessingDictionary_mBC439FBEB5E92414A12DBE4F25CFC1B8B541ED0A_AdjustorThunk (void);
extern void ReadStackFrame_IsProcessingEnumerable_m8BA2785FB705C7BC0FFFAE84E35516CCAA028D8A_AdjustorThunk (void);
extern void ReadStackFrame_Reset_mA12D43AF86177A2B7822F04C0E20B9556D8867F1_AdjustorThunk (void);
extern void WriteStack_get_IsContinuation_m88FEB1F19992286B54F88DE66079DAA6CB1BF319_AdjustorThunk (void);
extern void WriteStack_AddCurrent_m47F360C33799C68FED06B57AA0321371A5A4CF68_AdjustorThunk (void);
extern void WriteStack_Initialize_m1BA93CE9F8C4B1A365B61E8AF2A3B0D1FBCF7F3F_AdjustorThunk (void);
extern void WriteStack_Push_mEFB808A906431C95FBFE4A654F9FEFB8C962F048_AdjustorThunk (void);
extern void WriteStack_Pop_mA03ABCC412B4F0C5720FB1774D6A61F716450B48_AdjustorThunk (void);
extern void WriteStack_PropertyPath_m2B7C7609CF52ABC0DCCB60162CB58BFEA3EE4969_AdjustorThunk (void);
extern void WriteStackFrame_EndDictionaryElement_mF8B3A60F6AAB6D4EF329FC9EC0026B1BF43A184B_AdjustorThunk (void);
extern void WriteStackFrame_EndProperty_mFD22836ABA865D92442FBBC0E612A51A74F0F503_AdjustorThunk (void);
extern void WriteStackFrame_GetPolymorphicJsonPropertyInfo_mC6241293FD5975BCF17AE77AAD2A1735932D473C_AdjustorThunk (void);
extern void WriteStackFrame_InitializeReEntry_m6F30B6BDCE7931B9867F8DE7CE69299CA0C5330B_AdjustorThunk (void);
extern void WriteStackFrame_Reset_m919F66E6B37A5AABD075DF89DC00723E2FE74485_AdjustorThunk (void);
extern void JsonWriterOptions_get_Encoder_m232389EA65BBFEC33F6133822B6014A12A3DD4FE_AdjustorThunk (void);
extern void JsonWriterOptions_set_Encoder_mC29EDD6B2528E7E44A0E76471CC62E46FA114F00_AdjustorThunk (void);
extern void JsonWriterOptions_get_Indented_m1A267394B53602FEC4043C51430CA74205BE2316_AdjustorThunk (void);
extern void JsonWriterOptions_set_Indented_mC3F43DEDCB5B57EEFDF253E8397F49F73DB43C91_AdjustorThunk (void);
extern void JsonWriterOptions_get_SkipValidation_mEEE3015DF8EADA12C2E39BC1684FB3A5D2850683_AdjustorThunk (void);
extern void JsonWriterOptions_set_SkipValidation_m46DB044F25318B23C13DCFC763A9967C06CBA832_AdjustorThunk (void);
extern void JsonWriterOptions_get_IndentedOrNotSkipValidation_m7F6599D6058B3C9C18EC99304B0B5E386CD84DD7_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[297] = 
{
	{ 0x0600010A, BitStack_get_CurrentDepth_m89D98204DBF12922D8C94B3447F28558A607561E_AdjustorThunk },
	{ 0x0600010B, BitStack_PushTrue_m3E753BB8ECFB2193C4F9097FFBA0FB9C502DCCC7_AdjustorThunk },
	{ 0x0600010C, BitStack_PushFalse_mDD8A80022173EAF25EC74EFF7CFA03A6054F8D25_AdjustorThunk },
	{ 0x0600010D, BitStack_PushToArray_m20AF975C1EFDCFAA0D410D815A8B062A9CCECA23_AdjustorThunk },
	{ 0x0600010E, BitStack_Pop_m65E1670A3A560768AC7DD19C52A9D5B8C4CE2482_AdjustorThunk },
	{ 0x0600010F, BitStack_PopFromArray_m205D4F9EEB5B51B6074124CEE2F8974AA2A950DF_AdjustorThunk },
	{ 0x06000110, BitStack_DoubleArray_m589E01AA096E64AE9187C458D962BE8CA0E92051_AdjustorThunk },
	{ 0x06000111, BitStack_SetFirstBit_m9F284C76F69FBAE879387EE0B8B190B6967840BE_AdjustorThunk },
	{ 0x06000112, BitStack_ResetFirstBit_m6A4CD054DF57522D30866089E313E8353A6889A3_AdjustorThunk },
	{ 0x06000134, DbRow_get_Location_m64A1FE8677C03A2ED3384727229059262AD3F149_AdjustorThunk },
	{ 0x06000135, DbRow_get_SizeOrLength_mD035DFC369AC701D3930C33A159A067B00913772_AdjustorThunk },
	{ 0x06000136, DbRow_get_IsUnknownSize_mAB1898B5C44A75E8F3F5737B39E22A9829052B9E_AdjustorThunk },
	{ 0x06000137, DbRow_get_HasComplexChildren_m7FB54FB106AA84F5168A030E00E21B2687C58D09_AdjustorThunk },
	{ 0x06000138, DbRow_get_NumberOfRows_mB356515E008E677E4E79834F315165BD49A825DF_AdjustorThunk },
	{ 0x06000139, DbRow_get_TokenType_m25D3999BC52C9991FC22A97FEC677997068D1E7B_AdjustorThunk },
	{ 0x0600013A, DbRow__ctor_m83855B245AEDA3542D69D4181E26D625780729FF_AdjustorThunk },
	{ 0x0600013B, DbRow_get_IsSimpleValue_mF9C4D4A9AA674E10C15D23BFDCEE3B7A9CE78638_AdjustorThunk },
	{ 0x0600013C, MetadataDb_get_Length_m358C82F2ACD118F1A31750A408DE4A80D9706AC1_AdjustorThunk },
	{ 0x0600013D, MetadataDb_set_Length_mDEED679131C22B2B98BD28E71CABB539B01E79C7_AdjustorThunk },
	{ 0x0600013E, MetadataDb__ctor_mF5690ADF4EA9661465EF315D0505E445C6E222FB_AdjustorThunk },
	{ 0x0600013F, MetadataDb__ctor_mADDFB275C40C9946CEFB568AE37356AEB5FBE14D_AdjustorThunk },
	{ 0x06000140, MetadataDb_Dispose_m65983FCE3C671C0E078BA5B7192D66EA6224D218_AdjustorThunk },
	{ 0x06000141, MetadataDb_TrimExcess_m4BAD9F80840A78E601E56F2EAE6822A02267A1CD_AdjustorThunk },
	{ 0x06000142, MetadataDb_Append_m70BEF3B5C094C35F03E20E23CABEF3905616B61A_AdjustorThunk },
	{ 0x06000143, MetadataDb_Enlarge_m1D564D4B6A73CB588BBACAD06E20CAC2CC87BE91_AdjustorThunk },
	{ 0x06000144, MetadataDb_SetLength_mF874FB9D92FF15E65645B752D2F16693FD85EA53_AdjustorThunk },
	{ 0x06000145, MetadataDb_SetNumberOfRows_mE9A9288F6CFE360E85CC8E0A50AE964D43C470E2_AdjustorThunk },
	{ 0x06000146, MetadataDb_SetHasComplexChildren_m23B89D6C169828F97B1138999C88AD2C334E46D9_AdjustorThunk },
	{ 0x06000147, MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m93260D165FFDE321BC10AEB51ADA16EA571BDA92_AdjustorThunk },
	{ 0x06000148, MetadataDb_FindOpenElement_m764A0CA02C9B76F9FB4501BEC77D157A87ADCC8C_AdjustorThunk },
	{ 0x06000149, MetadataDb_Get_m54EC69D0C0A5AFA94790823E043C9C5AF611E879_AdjustorThunk },
	{ 0x0600014A, MetadataDb_GetJsonTokenType_mFE77CF70D471447224C7088BF78347AD9922BDD8_AdjustorThunk },
	{ 0x0600014B, MetadataDb_CopySegment_mE2A805BC0A4A35436649F6DFF69A286C416A056C_AdjustorThunk },
	{ 0x0600014C, StackRow__ctor_mE83A64F557423B2C1417CF2CAA47FCB6DAE79597_AdjustorThunk },
	{ 0x0600014D, StackRowStack__ctor_mC2D0A937968645C04762AE37C6F9196811FC60AD_AdjustorThunk },
	{ 0x0600014E, StackRowStack_Dispose_mE69DCE4BA09A561767066C061517880FC9FD240E_AdjustorThunk },
	{ 0x0600014F, StackRowStack_Push_m2EB6DF0A8051783F40F7911AC981072DB6DF3AA0_AdjustorThunk },
	{ 0x06000150, StackRowStack_Pop_mB7727C6BB4EF6384ADB6A055F63AE463231B3731_AdjustorThunk },
	{ 0x06000151, StackRowStack_Enlarge_m9CAB452BCBA0F7795608ED801F3741F645037CF0_AdjustorThunk },
	{ 0x06000152, JsonDocumentOptions_get_CommentHandling_m250724FEAA7D45E779F026A8E2AE1792E8CC0223_AdjustorThunk },
	{ 0x06000153, JsonDocumentOptions_get_MaxDepth_m910F496990C5F7315734B42E49D6B46D7D7FB146_AdjustorThunk },
	{ 0x06000154, JsonDocumentOptions_get_AllowTrailingCommas_mB695E7244BD9C08298D2A980CE3C8D6C69D0F2ED_AdjustorThunk },
	{ 0x06000155, JsonDocumentOptions_GetReaderOptions_mB45CDABD01DCD171F5576B841FD948C183A3F4BC_AdjustorThunk },
	{ 0x06000156, JsonElement__ctor_m08BBC84BCD6BF2156EEA51E7E367E772C44E8C9C_AdjustorThunk },
	{ 0x06000157, JsonElement_get_TokenType_m2DE21D16B1E01312F72BCD8C9E6EABB3C4CDD7B8_AdjustorThunk },
	{ 0x06000158, JsonElement_get_ValueKind_m34757B769FC468837C793AAB42507082105A1AE7_AdjustorThunk },
	{ 0x06000159, JsonElement_GetProperty_mF3F895EADD2EBC27D13528083FBA7A8E754C02B9_AdjustorThunk },
	{ 0x0600015A, JsonElement_TryGetProperty_m1F48BA59A2FF4DD5DEC6B04E2DFEF7785CAB89DE_AdjustorThunk },
	{ 0x0600015B, JsonElement_TryGetProperty_m915535CFE0D507A447594BD147BE125D20EE0B88_AdjustorThunk },
	{ 0x0600015C, JsonElement_GetString_mC81CE313F3CF6ECFCC6374545F7B598A244FA53E_AdjustorThunk },
	{ 0x0600015D, JsonElement_TryGetInt32_mA8E8A07BDB3F2CD9315116A391DA2F31023EA506_AdjustorThunk },
	{ 0x0600015E, JsonElement_GetInt32_mE298927691DCC4168662962C7B7FF3749003A5D6_AdjustorThunk },
	{ 0x0600015F, JsonElement_GetRawText_m57611974A95EAA4466054192D51EAC5A1487249D_AdjustorThunk },
	{ 0x06000160, JsonElement_GetPropertyRawText_mE33C03151821E238E589A59D52856D5E796CBEC5_AdjustorThunk },
	{ 0x06000161, JsonElement_TextEqualsHelper_m388909D510BCC8E2EE370A17626A111A77E362D5_AdjustorThunk },
	{ 0x06000162, JsonElement_WriteTo_m1AC8B59D427C19E13F2134BA8964477C9B3E4F20_AdjustorThunk },
	{ 0x06000163, JsonElement_EnumerateArray_mADD6A7BD2664B80DC5FEF55DE964046AADA2AC28_AdjustorThunk },
	{ 0x06000164, JsonElement_EnumerateObject_m25AB68731BDC8CA13253A206EABEA1F219AEA40C_AdjustorThunk },
	{ 0x06000165, JsonElement_ToString_mA0336A268F12CE4735C0307F12F4717EA5A24B29_AdjustorThunk },
	{ 0x06000166, JsonElement_Clone_m2D1217819F1C1A069A7291948BD2CDE7D58B090B_AdjustorThunk },
	{ 0x06000167, JsonElement_CheckValidInstance_m37FCD235BD0B2BF84200713D1CFB2D040A7A8439_AdjustorThunk },
	{ 0x06000168, JsonElement_get_DebuggerDisplay_mD7A4D8389DA4D9A8790C4FDDFEB0A20101CD8883_AdjustorThunk },
	{ 0x06000169, ArrayEnumerator__ctor_mE63EDB3BC6E93B2C7E2A40C824553B06F853E68C_AdjustorThunk },
	{ 0x0600016A, ArrayEnumerator_get_Current_mF52CB9638D47BF1F300A8D90DE72F16FE7760A50_AdjustorThunk },
	{ 0x0600016B, ArrayEnumerator_GetEnumerator_m84826411DC72414B035DD2383B98995030DC1D64_AdjustorThunk },
	{ 0x0600016C, ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_mFA06CBD3D1326936DCA477D78774A7D5244051B2_AdjustorThunk },
	{ 0x0600016D, ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m7E27A28429F4B25E812197A40277394C3898FBF4_AdjustorThunk },
	{ 0x0600016E, ArrayEnumerator_Dispose_m013242AA10DC5F3B889EF39004D9E62C88484F36_AdjustorThunk },
	{ 0x0600016F, ArrayEnumerator_Reset_m4E108783AA0C9538A0723F914B99F999D0907825_AdjustorThunk },
	{ 0x06000170, ArrayEnumerator_System_Collections_IEnumerator_get_Current_m0625474B01CE733D09EC696C2D3943FEBDA9D8B9_AdjustorThunk },
	{ 0x06000171, ArrayEnumerator_MoveNext_m4A4255B7F6ADA0D569E1644355F8542007BC99E8_AdjustorThunk },
	{ 0x06000172, ObjectEnumerator__ctor_mE6CFBEE462A2DAD097E3CF38C5E1F93B3A72575A_AdjustorThunk },
	{ 0x06000173, ObjectEnumerator_get_Current_mEBA991658CFB79D8EC48C940587E8F56E5FCF6A6_AdjustorThunk },
	{ 0x06000174, ObjectEnumerator_GetEnumerator_m6431E7E6BAF73BDCC9B211871E2046D8C5557C9F_AdjustorThunk },
	{ 0x06000175, ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mFABC61E9837E42D4B98EDD2EB829169691151DA4_AdjustorThunk },
	{ 0x06000176, ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m152A46470709CCB7745C15E69C8B5E3CC379F292_AdjustorThunk },
	{ 0x06000177, ObjectEnumerator_Dispose_m05CE64A63964F3D868775EC395EF5AFCBFD0C772_AdjustorThunk },
	{ 0x06000178, ObjectEnumerator_Reset_m2375949AFCB32D333217A2F8674E48BF3A948DC7_AdjustorThunk },
	{ 0x06000179, ObjectEnumerator_System_Collections_IEnumerator_get_Current_m819E29B32A75D77D74B6CA9F65F28BDFF7C7E6BC_AdjustorThunk },
	{ 0x0600017A, ObjectEnumerator_MoveNext_m9EF4007222C1BFD3D5BEF8CC03A76CE222F4D859_AdjustorThunk },
	{ 0x0600017B, JsonProperty_get_Value_mF766C4EFFB858EEAD2A009AF55BB8B51095AC96A_AdjustorThunk },
	{ 0x0600017C, JsonProperty__ctor_m0C3653F7D2BC6F745C523466CA7294E355FABB13_AdjustorThunk },
	{ 0x0600017D, JsonProperty_EscapedNameEquals_m9070FF7B8371466D91AF6A4E38D5699FD5107D86_AdjustorThunk },
	{ 0x0600017E, JsonProperty_ToString_m1FCFE27631E5AC6F3238874289B06303471487B0_AdjustorThunk },
	{ 0x0600017F, JsonProperty_get_DebuggerDisplay_mC4D1DE967EC567BBE7888EB699087916594E59E2_AdjustorThunk },
	{ 0x06000188, JsonEncodedText_get_EncodedUtf8Bytes_m90590ADF42E78917035B3FA5FE2B23661DBF945B_AdjustorThunk },
	{ 0x06000189, JsonEncodedText__ctor_m0EF268C655A1728E129E94D60BAA71894F1BD77A_AdjustorThunk },
	{ 0x0600018E, JsonEncodedText_Equals_m122DC2FCB6321FDA3F63E1973CF385FBB3C9FB69_AdjustorThunk },
	{ 0x0600018F, JsonEncodedText_Equals_mCE77AC6B7E663C8B7D8412B394E91F715A5BE682_AdjustorThunk },
	{ 0x06000190, JsonEncodedText_ToString_mEC405B95F27BDBC3814C264035F3B0151621800F_AdjustorThunk },
	{ 0x06000191, JsonEncodedText_GetHashCode_m2B447D2F286EC689D45FA77B4ECCD9DD072D2A70_AdjustorThunk },
	{ 0x060001BD, DateTimeParseData_get_OffsetNegative_m0D054B07A34B50D970C5751B503F342C14F5F3E9_AdjustorThunk },
	{ 0x060001DB, JsonReaderOptions_get_CommentHandling_mC784945C4A7D8BF00627E0D71CAF144E059CA45B_AdjustorThunk },
	{ 0x060001DC, JsonReaderOptions_set_CommentHandling_m840CCCE2E2296122C6AC1E8B8E75D1EEB54557E7_AdjustorThunk },
	{ 0x060001DD, JsonReaderOptions_get_MaxDepth_mE9E196FD22578986617CBC0B6104934965E7DE18_AdjustorThunk },
	{ 0x060001DE, JsonReaderOptions_set_MaxDepth_m7CA249F02B7EFB090EBE8DD1A81134BF15890127_AdjustorThunk },
	{ 0x060001DF, JsonReaderOptions_get_AllowTrailingCommas_m6BE15A1BB0597C203011627DCF4E9CDDB42049BA_AdjustorThunk },
	{ 0x060001E0, JsonReaderOptions_set_AllowTrailingCommas_mB8C99444AB5B8A9D50FDDD8E2CDC8F473B6D0641_AdjustorThunk },
	{ 0x060001E1, JsonReaderState__ctor_mBF6D934854F0D3489AED812C79FD31BD7DAEDB21_AdjustorThunk },
	{ 0x060001E2, JsonReaderState_get_Options_m66769D6D489C3E79567B39C83DD8464073804370_AdjustorThunk },
	{ 0x060001E3, Utf8JsonReader_get_IsLastSpan_m7B992A026074DDEFE7B18BBAF1D35F7FCC67307D_AdjustorThunk },
	{ 0x060001E4, Utf8JsonReader_get_OriginalSequence_m695F93AC97FC88673CF1C5B42CB2781FCC421DF3_AdjustorThunk },
	{ 0x060001E5, Utf8JsonReader_get_OriginalSpan_m5CE4EC409D428FFA49D657B1EB8095A5A1BF75FD_AdjustorThunk },
	{ 0x060001E6, Utf8JsonReader_get_ValueSpan_mBABD1CCB678681EF0BAB792BD92B1A667B5280EA_AdjustorThunk },
	{ 0x060001E7, Utf8JsonReader_set_ValueSpan_mCB290F34DBE06C916AC61FCD97325363E6DF8840_AdjustorThunk },
	{ 0x060001E8, Utf8JsonReader_get_BytesConsumed_mED511A2A33941953398E81C2630928B1BE296C60_AdjustorThunk },
	{ 0x060001E9, Utf8JsonReader_get_TokenStartIndex_m065C1E55350901EE451793D91D45C6BFD6727825_AdjustorThunk },
	{ 0x060001EA, Utf8JsonReader_set_TokenStartIndex_mD8DE6AC3843B60206928FF65843B1063725E11D1_AdjustorThunk },
	{ 0x060001EB, Utf8JsonReader_get_CurrentDepth_m27479A89058293B9D65E5245D4288745BC187941_AdjustorThunk },
	{ 0x060001EC, Utf8JsonReader_get_IsInArray_mA7603C29F57D874052CDED9AC19EE23B18786A16_AdjustorThunk },
	{ 0x060001ED, Utf8JsonReader_get_TokenType_m589FFFF94F1DF061F5BAA87791CE6EDC77B153FC_AdjustorThunk },
	{ 0x060001EE, Utf8JsonReader_get_HasValueSequence_m0E4BC52E674A56D3F8FD1EF9F56BCA86F7851658_AdjustorThunk },
	{ 0x060001EF, Utf8JsonReader_set_HasValueSequence_mA04C744A5B061CF11B292F4FA30685D8FF959819_AdjustorThunk },
	{ 0x060001F0, Utf8JsonReader_get_IsFinalBlock_m044741A01CC206E766FFC12FC40D20F5F3D20C16_AdjustorThunk },
	{ 0x060001F1, Utf8JsonReader_get_ValueSequence_m8BFD79D1CB26B6B8C081CC01F855EFD3A48E74F3_AdjustorThunk },
	{ 0x060001F2, Utf8JsonReader_set_ValueSequence_m14F871806DC91F89B691C08FDFDBA71DB941C6E4_AdjustorThunk },
	{ 0x060001F3, Utf8JsonReader_get_CurrentState_mB19357F20C4CBCFDD92278B4D2AEF65D82ECDA5A_AdjustorThunk },
	{ 0x060001F4, Utf8JsonReader__ctor_mCA5FF66B2E66D867A4F30167AADF35FA7B66D00F_AdjustorThunk },
	{ 0x060001F5, Utf8JsonReader_Read_m48C98153390FA527AB0DD5449555BB69A2881816_AdjustorThunk },
	{ 0x060001F6, Utf8JsonReader_Skip_m5E206D71CC77C21D2329A9F809AC53367CC74091_AdjustorThunk },
	{ 0x060001F7, Utf8JsonReader_SkipHelper_mBD2F0EAE5EFE06BA861442584F998D344B9D6316_AdjustorThunk },
	{ 0x060001F8, Utf8JsonReader_TrySkip_m30F2CB8BDCDD32BCE7C345A84398E3ED5AE38AC5_AdjustorThunk },
	{ 0x060001F9, Utf8JsonReader_TrySkipHelper_m403637E1D8D991978EC5C7D0D45B7AD39203E865_AdjustorThunk },
	{ 0x060001FA, Utf8JsonReader_StartObject_m80A9CBB11BC789B32FD92F9D951807CDA73F88EE_AdjustorThunk },
	{ 0x060001FB, Utf8JsonReader_EndObject_mF882CCEFFE2075AE1BA7A8B82F787F3472ACD900_AdjustorThunk },
	{ 0x060001FC, Utf8JsonReader_StartArray_m9DC35176A3D6D63A0F0CE6E12EF81057795E5B2D_AdjustorThunk },
	{ 0x060001FD, Utf8JsonReader_EndArray_m595FA16248A8A07CA4B68CF12789592F941E1149_AdjustorThunk },
	{ 0x060001FE, Utf8JsonReader_UpdateBitStackOnEndToken_m5681C904065EB11EBAB2CE8E3FD2E0323DEBDBC7_AdjustorThunk },
	{ 0x060001FF, Utf8JsonReader_ReadSingleSegment_m4D6BE7EA668855AF1A3DC170E54996088B0C08A2_AdjustorThunk },
	{ 0x06000200, Utf8JsonReader_HasMoreData_m0849ADA8E4F15E0C12811A4A1144D964F7AED2DD_AdjustorThunk },
	{ 0x06000201, Utf8JsonReader_HasMoreData_m1E2DEEDB648DF43DB37BC3044E8D633D16FB076E_AdjustorThunk },
	{ 0x06000202, Utf8JsonReader_ReadFirstToken_mF7DA287E1C8E5B7C1C885339DC4C1DC3E48C38F6_AdjustorThunk },
	{ 0x06000203, Utf8JsonReader_SkipWhiteSpace_m7E57D7BF978E4641391C9516F38820A2CE0556AA_AdjustorThunk },
	{ 0x06000204, Utf8JsonReader_ConsumeValue_mA7A960E791A5055383C64E7DD9AA669E71115799_AdjustorThunk },
	{ 0x06000205, Utf8JsonReader_ConsumeLiteral_m2A3B4794861C4EF32D8370B2D73C6412000660FC_AdjustorThunk },
	{ 0x06000206, Utf8JsonReader_CheckLiteral_m0906217EC5F76E1683DC47AFE73B4B56126791EC_AdjustorThunk },
	{ 0x06000207, Utf8JsonReader_ThrowInvalidLiteral_mFE5C045DA40FA4649AFE040AA0C332C753D8AFC5_AdjustorThunk },
	{ 0x06000208, Utf8JsonReader_ConsumeNumber_m4B68EF9DC535940BA5B09F601BE7A469C25482BE_AdjustorThunk },
	{ 0x06000209, Utf8JsonReader_ConsumePropertyName_m4D54B25BFE01BFFF5976F17D7A9D5D7C166EACD0_AdjustorThunk },
	{ 0x0600020A, Utf8JsonReader_ConsumeString_m0A2F49639EADB6BE4495CA600CED86A892F3EC82_AdjustorThunk },
	{ 0x0600020B, Utf8JsonReader_ConsumeStringAndValidate_mD3DCAF6E986C245047B6B08FFBB149269E1941E5_AdjustorThunk },
	{ 0x0600020C, Utf8JsonReader_ValidateHexDigits_mEB6781B13A74BC4EE94ECEE64A8362067945893E_AdjustorThunk },
	{ 0x0600020D, Utf8JsonReader_TryGetNumber_mA537AE8E9D9E0C1AA6EB929872959D9E0D1C1AAA_AdjustorThunk },
	{ 0x0600020E, Utf8JsonReader_ConsumeNegativeSign_mEAB4DACF3FA5003223B06958EB7BA236E6D3CD57_AdjustorThunk },
	{ 0x0600020F, Utf8JsonReader_ConsumeZero_m237EEC547BCEE5ACC2FA5C371DFC4C2F8D7C1184_AdjustorThunk },
	{ 0x06000210, Utf8JsonReader_ConsumeIntegerDigits_mD3C9A55484E0188A9DF616C6C43CDC7332FD166E_AdjustorThunk },
	{ 0x06000211, Utf8JsonReader_ConsumeDecimalDigits_m32313C386676761CA3EDDB3F80EAD756C8B0E76D_AdjustorThunk },
	{ 0x06000212, Utf8JsonReader_ConsumeSign_m9D31DED0190794A96D6BD43EFD2D01D4FB326F12_AdjustorThunk },
	{ 0x06000213, Utf8JsonReader_ConsumeNextTokenOrRollback_m734D5BDD909FE3FC93D2D62FC823048553C28BF3_AdjustorThunk },
	{ 0x06000214, Utf8JsonReader_ConsumeNextToken_m3CA8B80E08B12A2E034EE6913DA5F8185A32361D_AdjustorThunk },
	{ 0x06000215, Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_m6B24CB081250AF44DCEA9938616E206978FBE8BB_AdjustorThunk },
	{ 0x06000216, Utf8JsonReader_SkipAllComments_m4B68EAA87BA8197DFB88BEA53B67B7C5D5E5BF1F_AdjustorThunk },
	{ 0x06000217, Utf8JsonReader_SkipAllComments_m0626448D38FC1F68C36361A5CE32F753E3C3A9B4_AdjustorThunk },
	{ 0x06000218, Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m9EBCD5E4D2F5B96282EA227FC60DECFD12DC322D_AdjustorThunk },
	{ 0x06000219, Utf8JsonReader_SkipComment_m91E3271B505861F135B1F7CBEA89FF9D03DBFEC4_AdjustorThunk },
	{ 0x0600021A, Utf8JsonReader_SkipSingleLineComment_m080A7403DB593F785293CEB04C935F97608A47A5_AdjustorThunk },
	{ 0x0600021B, Utf8JsonReader_FindLineSeparator_m1A63CFD96D293C6851A62C87C0829B08268CC50D_AdjustorThunk },
	{ 0x0600021C, Utf8JsonReader_ThrowOnDangerousLineSeparator_m9DB458C5580C40BD100AC7993DFC3445EA6D1DCC_AdjustorThunk },
	{ 0x0600021D, Utf8JsonReader_SkipMultiLineComment_mDDE267D4B9BCE906A04E91290B75E2E69C943839_AdjustorThunk },
	{ 0x0600021E, Utf8JsonReader_ConsumeComment_mDDA4B89EB77DF4767A97289E5AE1A45C29875605_AdjustorThunk },
	{ 0x0600021F, Utf8JsonReader_ConsumeSingleLineComment_m6820206B83C4C8FB1F2D85DA65DA783F0C4B0848_AdjustorThunk },
	{ 0x06000220, Utf8JsonReader_ConsumeMultiLineComment_mDF378938E725595AE26E07EBC9448EBC122BF2A8_AdjustorThunk },
	{ 0x06000221, Utf8JsonReader_get_DebuggerDisplay_m90465723303F2563AD6293AD79277A5101A811C8_AdjustorThunk },
	{ 0x06000222, Utf8JsonReader_get_DebugTokenType_m5C7DEDDF27CDA9E961186CA3261276037A38961F_AdjustorThunk },
	{ 0x06000223, Utf8JsonReader_GetUnescapedSpan_m2180AE402D0F1D887B57335433E1B98A8F6A0030_AdjustorThunk },
	{ 0x06000224, Utf8JsonReader_ReadMultiSegment_m7A789B154E6126292F523C039F86A28936652DA7_AdjustorThunk },
	{ 0x06000225, Utf8JsonReader_ValidateStateAtEndOfData_mE44BD918232309740E30D9A285F6F63A2EF5E76C_AdjustorThunk },
	{ 0x06000226, Utf8JsonReader_HasMoreDataMultiSegment_mB2C1C0751141A6B4B150D6A1F310560936C69C18_AdjustorThunk },
	{ 0x06000227, Utf8JsonReader_HasMoreDataMultiSegment_m71B085F039270BE038C5F171206D7F198AB21FF5_AdjustorThunk },
	{ 0x06000228, Utf8JsonReader_GetNextSpan_m2B9D198EE611302CE973A7E6268D835C5164553B_AdjustorThunk },
	{ 0x06000229, Utf8JsonReader_ReadFirstTokenMultiSegment_m63D211F1A922E4DD4BC8A801BCECAC0CCEA6267D_AdjustorThunk },
	{ 0x0600022A, Utf8JsonReader_SkipWhiteSpaceMultiSegment_m11BAAD269031D687444204D40BB0EA7C6E2D9427_AdjustorThunk },
	{ 0x0600022B, Utf8JsonReader_ConsumeValueMultiSegment_m913D0F4B616A77E94761359A3617D2EE8102A95D_AdjustorThunk },
	{ 0x0600022C, Utf8JsonReader_ConsumeLiteralMultiSegment_mBA1B229E2B0682719CDC963E68E9E8EF203D9610_AdjustorThunk },
	{ 0x0600022D, Utf8JsonReader_CheckLiteralMultiSegment_mBD77DA207039A439C116B70313DA1C1EAE1E79CB_AdjustorThunk },
	{ 0x0600022E, Utf8JsonReader_FindMismatch_mFC922E786DE159AC6D606BE3AB07F15BA6743BD0_AdjustorThunk },
	{ 0x0600022F, Utf8JsonReader_GetInvalidLiteralMultiSegment_m60F4782DDD6F88D7898813D5C0468DD1A1BEAF55_AdjustorThunk },
	{ 0x06000230, Utf8JsonReader_ConsumeNumberMultiSegment_mE723026EE74A4624867675A37F7C8E267998EE3C_AdjustorThunk },
	{ 0x06000231, Utf8JsonReader_ConsumePropertyNameMultiSegment_m3F71002973F225C8F6FD075BC3C76A506D7BB72E_AdjustorThunk },
	{ 0x06000232, Utf8JsonReader_ConsumeStringMultiSegment_mB24E40054CA1A60B8210FC43CF23EF6F8BE3F151_AdjustorThunk },
	{ 0x06000233, Utf8JsonReader_ConsumeStringNextSegment_m3CCF0CCD09B5856298887E1096CCE6F02A60D0A9_AdjustorThunk },
	{ 0x06000234, Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mA8AD93CE8405F63672B369F6848EC276631D5B33_AdjustorThunk },
	{ 0x06000235, Utf8JsonReader_RollBackState_mE1AB8FF05072C2D1C921F4D97E56122C15E905AD_AdjustorThunk },
	{ 0x06000236, Utf8JsonReader_TryGetNumberMultiSegment_mAB46FB985C042DAF2B0F290A19B221AB58FDBAB1_AdjustorThunk },
	{ 0x06000237, Utf8JsonReader_ConsumeNegativeSignMultiSegment_m135AE9A283C44CD51F7BEFF2A2E0FD9890FCED3C_AdjustorThunk },
	{ 0x06000238, Utf8JsonReader_ConsumeZeroMultiSegment_m281FC6658603949FC90711BE6C81B8C3108B9E2D_AdjustorThunk },
	{ 0x06000239, Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_mF5A5A69C0744D08C722001E1E049D5D1703A9308_AdjustorThunk },
	{ 0x0600023A, Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_mE037A2CF96C775C86599C5A61F9E7EEB2C411140_AdjustorThunk },
	{ 0x0600023B, Utf8JsonReader_ConsumeSignMultiSegment_m05A9CA258362283F551D2CC8A7B43DAAA1F99D7F_AdjustorThunk },
	{ 0x0600023C, Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m8A4377711B0F17F01DE5CBC05DC2A26109647E43_AdjustorThunk },
	{ 0x0600023D, Utf8JsonReader_ConsumeNextTokenMultiSegment_m4CA61D5BA79665005FBE0CE4E9694F501351086A_AdjustorThunk },
	{ 0x0600023E, Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mA24E85BCB4C5A5FF4C5BCE2BCB0AF267FFF92E42_AdjustorThunk },
	{ 0x0600023F, Utf8JsonReader_SkipAllCommentsMultiSegment_mAC8F7760E50129CC670532D1BABFCE46303011AF_AdjustorThunk },
	{ 0x06000240, Utf8JsonReader_SkipAllCommentsMultiSegment_m14037635702E57A5503363E26FD8907DABF504B4_AdjustorThunk },
	{ 0x06000241, Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_mE0992B12538E8587B695ED2D6EA28CBACF2EFE2C_AdjustorThunk },
	{ 0x06000242, Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m050F36798BF507E32B4C696A1BA9DBF566543055_AdjustorThunk },
	{ 0x06000243, Utf8JsonReader_SkipCommentMultiSegment_mF89DEC8E6F0EBCD5B6472A4C37949F9C14C69B97_AdjustorThunk },
	{ 0x06000244, Utf8JsonReader_SkipSingleLineCommentMultiSegment_m010391D904BA6ACBF5FCB5034CB8EE91C9C3E091_AdjustorThunk },
	{ 0x06000245, Utf8JsonReader_FindLineSeparatorMultiSegment_m1517AF51E8F05AD9F0B0E3C8247A737D4756D9F2_AdjustorThunk },
	{ 0x06000246, Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m0CB9C5BCC5C677E851EE114F5AFAA5DC1EE42CDB_AdjustorThunk },
	{ 0x06000247, Utf8JsonReader_SkipMultiLineCommentMultiSegment_mCD3026993722AB7389E158550B086BD01A090C33_AdjustorThunk },
	{ 0x06000248, Utf8JsonReader_CaptureState_m8BBAE4358A88D8BAD2FB811C910F1192BCA21325_AdjustorThunk },
	{ 0x06000249, Utf8JsonReader_GetString_mF6ACA3547007BAAD951423AD47FC7C8FB1EE1C44_AdjustorThunk },
	{ 0x0600024A, Utf8JsonReader_GetBoolean_m908285BF5B202C675CBEE893F2B41E9BC5F2806B_AdjustorThunk },
	{ 0x0600024B, Utf8JsonReader_GetBytesFromBase64_m0B0E3C7FA55C6B1BF7B6513D80CB4159543F5F67_AdjustorThunk },
	{ 0x0600024C, Utf8JsonReader_GetByte_mA2DAAA2A21699AB68FD020A0BC41AC8D3935C439_AdjustorThunk },
	{ 0x0600024D, Utf8JsonReader_GetByteWithQuotes_m3BFD4FEF30EBA5DC312DC07D7C6EC9D132B591B9_AdjustorThunk },
	{ 0x0600024E, Utf8JsonReader_GetSByte_mF0C15DF1DFAAFED79A5FF6527BC1EC6DF8D6D541_AdjustorThunk },
	{ 0x0600024F, Utf8JsonReader_GetSByteWithQuotes_mBA0D6B8A92034B6683EF86A600395EB38129922C_AdjustorThunk },
	{ 0x06000250, Utf8JsonReader_GetInt16_mD10C668BD0F694A704B5A7299ABFDE45B9DFD4EE_AdjustorThunk },
	{ 0x06000251, Utf8JsonReader_GetInt16WithQuotes_m3BA45015843A11D2A9C67FA2B83E83DB0AF69EC1_AdjustorThunk },
	{ 0x06000252, Utf8JsonReader_GetInt32_mB50F3838DE3DC950FC7FFDFC24721A136DA76BC8_AdjustorThunk },
	{ 0x06000253, Utf8JsonReader_GetInt32WithQuotes_mF05F615F6638E5308D86A63939D7639221402D52_AdjustorThunk },
	{ 0x06000254, Utf8JsonReader_GetInt64_m68E1F5DB10DBB2E490FE90D1CDBC1C5E81EFDB8D_AdjustorThunk },
	{ 0x06000255, Utf8JsonReader_GetInt64WithQuotes_mE8901124409E515BBAB819575C5851BD854BE8ED_AdjustorThunk },
	{ 0x06000256, Utf8JsonReader_GetUInt16_m91B334F666BA8952300156AC744D340C01A0FA68_AdjustorThunk },
	{ 0x06000257, Utf8JsonReader_GetUInt16WithQuotes_m2BA11333E17BC23C70A1CA4E90AE7960BA808CB1_AdjustorThunk },
	{ 0x06000258, Utf8JsonReader_GetUInt32_mCAAA27CB167078F0E49C896E0D38F71DA98A1DA1_AdjustorThunk },
	{ 0x06000259, Utf8JsonReader_GetUInt32WithQuotes_m2221571EDFBA7F56DC5E51B2AC8F0318548E6DD8_AdjustorThunk },
	{ 0x0600025A, Utf8JsonReader_GetUInt64_m04FC93AD8353898C2E5383CBF9616A0D03DB05AF_AdjustorThunk },
	{ 0x0600025B, Utf8JsonReader_GetUInt64WithQuotes_m80FDBEC80D9C007947133F9D259BC28EC239CC53_AdjustorThunk },
	{ 0x0600025C, Utf8JsonReader_GetSingle_m65F5DCB28FD9BE75EDBC6C94B20F26E32C3305DB_AdjustorThunk },
	{ 0x0600025D, Utf8JsonReader_GetSingleWithQuotes_mA66BC371F801F4DA57D5701543059442BD108597_AdjustorThunk },
	{ 0x0600025E, Utf8JsonReader_GetSingleFloatingPointConstant_m37AAA7DD71B9E5379C449141C075EED6C4BDD4AD_AdjustorThunk },
	{ 0x0600025F, Utf8JsonReader_GetDouble_m1D71F0D17190B5ADBF720FB13597184F00B83159_AdjustorThunk },
	{ 0x06000260, Utf8JsonReader_GetDoubleWithQuotes_m62F2D7D5B21BCCCA007091EFC60D69F1B6B1A1B5_AdjustorThunk },
	{ 0x06000261, Utf8JsonReader_GetDoubleFloatingPointConstant_m623ADC6DD4A7D49297B7B7306DB045A9B295B91A_AdjustorThunk },
	{ 0x06000262, Utf8JsonReader_GetDecimal_mFEA9D944822478D418E3275FEF14E78CAECE0BA7_AdjustorThunk },
	{ 0x06000263, Utf8JsonReader_GetDecimalWithQuotes_m7421A53B8EDCB1669FF8591DE10A9407E9224141_AdjustorThunk },
	{ 0x06000264, Utf8JsonReader_GetDateTime_mA26D2BA988CF28A40FC962FED1145B29BFE57067_AdjustorThunk },
	{ 0x06000265, Utf8JsonReader_GetDateTimeNoValidation_m4516B03D0EF902C15DFA9BAB9BF057ADC647B6DF_AdjustorThunk },
	{ 0x06000266, Utf8JsonReader_GetDateTimeOffset_mE45024C43B2BE11C35AC5DFA1B247F0C5C888A3C_AdjustorThunk },
	{ 0x06000267, Utf8JsonReader_GetDateTimeOffsetNoValidation_m62B6E29DF9475F352B974BDAAFC391B6133F7A7B_AdjustorThunk },
	{ 0x06000268, Utf8JsonReader_GetGuid_m7A0ECB26F40A0A64EE37C7393F109B3E63193CB5_AdjustorThunk },
	{ 0x06000269, Utf8JsonReader_GetGuidNoValidation_m5DF916F1E98E9F975BF7D48BD9760CAD9771CEC9_AdjustorThunk },
	{ 0x0600026A, Utf8JsonReader_TryGetBytesFromBase64_m805B87E3644DCC40934014EACB0304206EA47E35_AdjustorThunk },
	{ 0x0600026B, Utf8JsonReader_TryGetByte_m57093639B039FA045AE217E89A38023A51F25877_AdjustorThunk },
	{ 0x0600026C, Utf8JsonReader_TryGetByteCore_m5975171C440BA1DB0F5946CEE3A95AD25E90AA83_AdjustorThunk },
	{ 0x0600026D, Utf8JsonReader_TryGetSByte_mF7778E9C90F9D7C8CBE321D9C4B3859CE02B9106_AdjustorThunk },
	{ 0x0600026E, Utf8JsonReader_TryGetSByteCore_m8789EC6277347F2746BACBE65069C7E63B3ED6EE_AdjustorThunk },
	{ 0x0600026F, Utf8JsonReader_TryGetInt16_m5E65C5335E39AAA608EC42137A1F9909B61C6085_AdjustorThunk },
	{ 0x06000270, Utf8JsonReader_TryGetInt16Core_m489F57E73BB2F417F88E7A98DA2ABB2705B3AD8B_AdjustorThunk },
	{ 0x06000271, Utf8JsonReader_TryGetInt32_m93479A8A49CE3407F4F48F9F3311D8CCECDFC7BA_AdjustorThunk },
	{ 0x06000272, Utf8JsonReader_TryGetInt32Core_mF9C2BAB3D4887F47FD89DE68149E0E4FD95FD7F6_AdjustorThunk },
	{ 0x06000273, Utf8JsonReader_TryGetInt64_m04985C755DD8030B3AC7D8045291994F453D42A3_AdjustorThunk },
	{ 0x06000274, Utf8JsonReader_TryGetInt64Core_m9984ADE08AAFE1243D84F6B9F3F6FF3D59B6D5CF_AdjustorThunk },
	{ 0x06000275, Utf8JsonReader_TryGetUInt16_m018B28F35D595DDDBB3A104F69E2BAB463CDC274_AdjustorThunk },
	{ 0x06000276, Utf8JsonReader_TryGetUInt16Core_m413B4DF3E32855458F822546C8F91B0138F71816_AdjustorThunk },
	{ 0x06000277, Utf8JsonReader_TryGetUInt32_m3568E2AB280AF64148B26E5A570DE313B5F6A557_AdjustorThunk },
	{ 0x06000278, Utf8JsonReader_TryGetUInt32Core_m52DBDE64A8BE6BBFB4AC92E782FA38637E88B79D_AdjustorThunk },
	{ 0x06000279, Utf8JsonReader_TryGetUInt64_mA1BFC2EDB1096EEA85D3F3992A6C33B599769A63_AdjustorThunk },
	{ 0x0600027A, Utf8JsonReader_TryGetUInt64Core_m3DBEB9BA3D05D681BF354807C9D16B725CC7256D_AdjustorThunk },
	{ 0x0600027B, Utf8JsonReader_TryGetSingle_mF21A462DD40F497AE887B4F93EB3F329632E6D34_AdjustorThunk },
	{ 0x0600027C, Utf8JsonReader_TryGetDouble_mA5FF188644DBB6DFF353DA7887B0A28B7322EDD6_AdjustorThunk },
	{ 0x0600027D, Utf8JsonReader_TryGetDecimal_m9129A3234F32FBF6B0C85C3F4D2AB8C5F008F297_AdjustorThunk },
	{ 0x0600027E, Utf8JsonReader_TryGetDateTime_m5D9B9E2E3B345769B3D1505271DBE6F7EF852895_AdjustorThunk },
	{ 0x0600027F, Utf8JsonReader_TryGetDateTimeCore_m08A0D16A29B1B5071737A3E3C2A88FA68EC68A14_AdjustorThunk },
	{ 0x06000280, Utf8JsonReader_TryGetDateTimeOffset_m33ADB6C9F6844507D161D845D20E471322D84587_AdjustorThunk },
	{ 0x06000281, Utf8JsonReader_TryGetDateTimeOffsetCore_m97770A3308970C50091E12FA977F276276D776DA_AdjustorThunk },
	{ 0x06000282, Utf8JsonReader_TryGetGuid_m5D97B625335AB390E15C8AA7A4D4794B8BA868A5_AdjustorThunk },
	{ 0x06000283, Utf8JsonReader_TryGetGuidCore_mB8171BAB36B60E93D015CA3CF34B48BC53C7C171_AdjustorThunk },
	{ 0x06000284, PartialStateForRollback__ctor_m0A2CA55E0DA1307DCE07752039D69E1E171C6178_AdjustorThunk },
	{ 0x06000285, PartialStateForRollback_GetStartPosition_mD8F495352B433FEE2C9D27A58FDBAC331C418D63_AdjustorThunk },
	{ 0x06000375, ParameterRef__ctor_mAE5D0234AF0B54F1B4F1C0F808F635329A9928A2_AdjustorThunk },
	{ 0x06000376, PropertyRef__ctor_mA2ED2603D61FFE0F81222405419BB00EE8455EF4_AdjustorThunk },
	{ 0x06000377, ReadStack_get_IsContinuation_mD01FD9A2DAC4FD9C3E805929E0104AAED9EFF95A_AdjustorThunk },
	{ 0x06000378, ReadStack_get_IsLastContinuation_mA8012A8628BE1311AE139DD8DC7D98E902EACF7F_AdjustorThunk },
	{ 0x06000379, ReadStack_AddCurrent_mF400E1F933A12C71C19C3A4DFCBE009008B823D5_AdjustorThunk },
	{ 0x0600037A, ReadStack_Initialize_m7E7F3ED59DEEFF3DC132C8773C83EE001A238EAC_AdjustorThunk },
	{ 0x0600037B, ReadStack_Push_mA7CB7FCFBF6F542FD626BDE571D54F1D136B3C0E_AdjustorThunk },
	{ 0x0600037C, ReadStack_Pop_mD314744BCA164729B64A75EAAEB43FC240346DAB_AdjustorThunk },
	{ 0x0600037D, ReadStack_JsonPath_m86FCB866216FA16E285600FB62136552D1CA7DED_AdjustorThunk },
	{ 0x0600037E, ReadStack_SetConstructorArgumentState_mB792C92FEA853A82C6564EB5F41E3A2ED909B992_AdjustorThunk },
	{ 0x06000384, ReadStackFrame_EndConstructorParameter_mE5D6B2F709289B69DD06F58040B1AA47DF4C4EC7_AdjustorThunk },
	{ 0x06000385, ReadStackFrame_EndProperty_m6BBD5C956CCF9412B4B122B5E2501681DE66EEBA_AdjustorThunk },
	{ 0x06000386, ReadStackFrame_EndElement_m4F6CA0BDACF8EEAB43A942243D21DF32C5C26C8D_AdjustorThunk },
	{ 0x06000387, ReadStackFrame_IsProcessingDictionary_mBC439FBEB5E92414A12DBE4F25CFC1B8B541ED0A_AdjustorThunk },
	{ 0x06000388, ReadStackFrame_IsProcessingEnumerable_m8BA2785FB705C7BC0FFFAE84E35516CCAA028D8A_AdjustorThunk },
	{ 0x06000389, ReadStackFrame_Reset_mA12D43AF86177A2B7822F04C0E20B9556D8867F1_AdjustorThunk },
	{ 0x0600038A, WriteStack_get_IsContinuation_m88FEB1F19992286B54F88DE66079DAA6CB1BF319_AdjustorThunk },
	{ 0x0600038B, WriteStack_AddCurrent_m47F360C33799C68FED06B57AA0321371A5A4CF68_AdjustorThunk },
	{ 0x0600038C, WriteStack_Initialize_m1BA93CE9F8C4B1A365B61E8AF2A3B0D1FBCF7F3F_AdjustorThunk },
	{ 0x0600038D, WriteStack_Push_mEFB808A906431C95FBFE4A654F9FEFB8C962F048_AdjustorThunk },
	{ 0x0600038E, WriteStack_Pop_mA03ABCC412B4F0C5720FB1774D6A61F716450B48_AdjustorThunk },
	{ 0x0600038F, WriteStack_PropertyPath_m2B7C7609CF52ABC0DCCB60162CB58BFEA3EE4969_AdjustorThunk },
	{ 0x06000392, WriteStackFrame_EndDictionaryElement_mF8B3A60F6AAB6D4EF329FC9EC0026B1BF43A184B_AdjustorThunk },
	{ 0x06000393, WriteStackFrame_EndProperty_mFD22836ABA865D92442FBBC0E612A51A74F0F503_AdjustorThunk },
	{ 0x06000394, WriteStackFrame_GetPolymorphicJsonPropertyInfo_mC6241293FD5975BCF17AE77AAD2A1735932D473C_AdjustorThunk },
	{ 0x06000395, WriteStackFrame_InitializeReEntry_m6F30B6BDCE7931B9867F8DE7CE69299CA0C5330B_AdjustorThunk },
	{ 0x06000396, WriteStackFrame_Reset_m919F66E6B37A5AABD075DF89DC00723E2FE74485_AdjustorThunk },
	{ 0x060003BA, JsonWriterOptions_get_Encoder_m232389EA65BBFEC33F6133822B6014A12A3DD4FE_AdjustorThunk },
	{ 0x060003BB, JsonWriterOptions_set_Encoder_mC29EDD6B2528E7E44A0E76471CC62E46FA114F00_AdjustorThunk },
	{ 0x060003BC, JsonWriterOptions_get_Indented_m1A267394B53602FEC4043C51430CA74205BE2316_AdjustorThunk },
	{ 0x060003BD, JsonWriterOptions_set_Indented_mC3F43DEDCB5B57EEFDF253E8397F49F73DB43C91_AdjustorThunk },
	{ 0x060003BE, JsonWriterOptions_get_SkipValidation_mEEE3015DF8EADA12C2E39BC1684FB3A5D2850683_AdjustorThunk },
	{ 0x060003BF, JsonWriterOptions_set_SkipValidation_m46DB044F25318B23C13DCFC763A9967C06CBA832_AdjustorThunk },
	{ 0x060003C0, JsonWriterOptions_get_IndentedOrNotSkipValidation_m7F6599D6058B3C9C18EC99304B0B5E386CD84DD7_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1579] = 
{
	2411,
	2411,
	2411,
	2038,
	2038,
	2038,
	3631,
	3651,
	3742,
	3830,
	3869,
	3305,
	3305,
	3036,
	2811,
	3305,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3876,
	2411,
	3862,
	948,
	1462,
	3876,
	1999,
	2411,
	2038,
	2038,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1999,
	2283,
	2411,
	2411,
	1999,
	1270,
	1999,
	3793,
	3715,
	3305,
	3715,
	3715,
	3793,
	3793,
	3876,
	3876,
	3793,
	3797,
	3715,
	3720,
	3720,
	3720,
	3862,
	3306,
	3313,
	3313,
	2887,
	2786,
	3747,
	3720,
	2787,
	2896,
	3783,
	3793,
	3793,
	3876,
	3715,
	3715,
	3305,
	2791,
	2791,
	3862,
	3712,
	3712,
	3720,
	3797,
	3495,
	3797,
	3797,
	3495,
	3797,
	3797,
	3797,
	3793,
	3797,
	3173,
	3495,
	3495,
	3173,
	3876,
	3495,
	3495,
	3797,
	3797,
	2701,
	3495,
	3173,
	3495,
	3797,
	3797,
	3495,
	3117,
	3466,
	3123,
	3123,
	3466,
	3466,
	3173,
	3495,
	-1,
	3495,
	3123,
	3466,
	3160,
	3160,
	3799,
	3797,
	3799,
	3799,
	3454,
	3876,
	3454,
	3457,
	3117,
	3797,
	3797,
	3134,
	3466,
	3797,
	3173,
	3117,
	2341,
	2411,
	2411,
	2038,
	2385,
	2385,
	1999,
	2411,
	2411,
	3245,
	2385,
	2346,
	386,
	2411,
	2015,
	1740,
	816,
	748,
	1277,
	878,
	354,
	929,
	1572,
	1572,
	1543,
	1099,
	1099,
	775,
	3782,
	1016,
	1016,
	2883,
	2411,
	1205,
	3485,
	3282,
	3304,
	3705,
	3062,
	3003,
	598,
	357,
	2341,
	2341,
	2385,
	2385,
	2341,
	2385,
	729,
	2385,
	2341,
	1999,
	2015,
	1999,
	2411,
	2411,
	729,
	2411,
	1087,
	1087,
	1999,
	1475,
	1475,
	2118,
	1740,
	1226,
	1087,
	1999,
	2411,
	2082,
	2432,
	2411,
	2385,
	2341,
	2385,
	2349,
	1183,
	2385,
	2385,
	1544,
	944,
	906,
	2360,
	1691,
	2341,
	2360,
	2360,
	586,
	2015,
	2433,
	2434,
	2360,
	2346,
	2411,
	2360,
	2004,
	2346,
	2433,
	2360,
	2360,
	2411,
	2411,
	2360,
	2385,
	2004,
	2348,
	2434,
	2360,
	2360,
	2411,
	2411,
	2360,
	2385,
	2346,
	1165,
	1665,
	2360,
	2360,
	3830,
	3830,
	3830,
	3830,
	3830,
	3830,
	3830,
	3830,
	2288,
	2015,
	3277,
	3276,
	3276,
	3275,
	1746,
	1759,
	2360,
	2341,
	220,
	447,
	1188,
	2015,
	2411,
	1193,
	2385,
	2038,
	1193,
	2279,
	1938,
	2279,
	1938,
	2360,
	2015,
	2360,
	2015,
	3545,
	3074,
	3074,
	3076,
	3747,
	3787,
	3700,
	-1,
	3738,
	3748,
	3742,
	3743,
	3333,
	3333,
	3333,
	3333,
	3069,
	3353,
	3412,
	3097,
	3284,
	3004,
	3004,
	3700,
	3876,
	3352,
	2385,
	700,
	1193,
	3571,
	3747,
	3747,
	3747,
	3639,
	2571,
	3641,
	3652,
	3333,
	3333,
	3333,
	3622,
	3333,
	3333,
	3055,
	3283,
	3190,
	3332,
	3340,
	3333,
	3700,
	3640,
	3234,
	3700,
	2881,
	3141,
	3876,
	2385,
	2038,
	2341,
	1999,
	2385,
	2038,
	2007,
	2349,
	2385,
	2287,
	2288,
	2288,
	1948,
	2342,
	2342,
	2000,
	2341,
	2385,
	2385,
	2385,
	2038,
	2385,
	2287,
	1946,
	2350,
	639,
	2385,
	2411,
	2411,
	2385,
	2385,
	2411,
	2411,
	2411,
	2411,
	2411,
	2385,
	2385,
	1740,
	1783,
	2411,
	1783,
	905,
	902,
	1948,
	2385,
	2385,
	2385,
	904,
	904,
	903,
	910,
	910,
	910,
	910,
	910,
	1783,
	1783,
	2385,
	1691,
	911,
	1783,
	2385,
	903,
	1408,
	1948,
	903,
	2385,
	904,
	904,
	2360,
	2360,
	2288,
	2385,
	2385,
	2385,
	1740,
	2385,
	1783,
	2411,
	1783,
	905,
	585,
	804,
	1556,
	2385,
	2385,
	2385,
	2385,
	904,
	1017,
	903,
	587,
	587,
	910,
	587,
	587,
	1783,
	1783,
	2385,
	1691,
	911,
	1783,
	2385,
	1691,
	903,
	805,
	1008,
	1665,
	2449,
	2360,
	2385,
	2360,
	2385,
	2385,
	2385,
	2385,
	2340,
	2340,
	2341,
	2341,
	2342,
	2342,
	2340,
	2340,
	2341,
	2341,
	2342,
	2342,
	2390,
	2390,
	2390,
	2332,
	2332,
	2332,
	2329,
	2329,
	2327,
	2327,
	2328,
	2328,
	2338,
	2338,
	1691,
	1691,
	909,
	1691,
	909,
	1691,
	909,
	1691,
	909,
	1691,
	909,
	1691,
	909,
	1691,
	909,
	1691,
	909,
	1691,
	1691,
	1691,
	1691,
	1691,
	1691,
	1691,
	1691,
	1691,
	420,
	1849,
	-1,
	2411,
	1575,
	3786,
	2411,
	2360,
	2015,
	2360,
	2015,
	2360,
	2015,
	2385,
	2038,
	2360,
	2015,
	2360,
	2360,
	2015,
	2360,
	2015,
	2360,
	2015,
	2360,
	2015,
	1188,
	139,
	2015,
	3392,
	3745,
	1759,
	3036,
	3036,
	2632,
	2917,
	3745,
	3745,
	3530,
	2341,
	1999,
	2631,
	2469,
	2811,
	542,
	542,
	3056,
	3056,
	3662,
	1967,
	1967,
	3876,
	3715,
	1185,
	2360,
	-1,
	-1,
	-1,
	-1,
	1188,
	2360,
	2360,
	2341,
	1759,
	2015,
	2360,
	2015,
	2360,
	1575,
	2411,
	2411,
	1575,
	3876,
	2360,
	2015,
	2360,
	2015,
	2385,
	2038,
	2360,
	2015,
	2360,
	2015,
	2280,
	1940,
	2341,
	1999,
	2360,
	2360,
	2015,
	2385,
	2038,
	458,
	3715,
	2411,
	-1,
	-1,
	-1,
	-1,
	2360,
	2015,
	3862,
	3305,
	2360,
	2015,
	636,
	2411,
	1939,
	1005,
	1940,
	-1,
	613,
	613,
	1575,
	2385,
	2038,
	2385,
	2038,
	19,
	2385,
	2038,
	2385,
	2038,
	2385,
	2038,
	2360,
	2015,
	2360,
	2015,
	612,
	612,
	587,
	587,
	2360,
	2015,
	2360,
	2015,
	2360,
	2360,
	2015,
	1188,
	2385,
	2038,
	2385,
	2038,
	2385,
	2038,
	2280,
	1940,
	2385,
	2038,
	2411,
	3876,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3062,
	3639,
	3066,
	-1,
	2625,
	2938,
	3495,
	-1,
	-1,
	-1,
	-1,
	2737,
	2737,
	-1,
	-1,
	-1,
	-1,
	3876,
	3862,
	1575,
	3862,
	2360,
	569,
	1575,
	331,
	3036,
	3305,
	2811,
	2360,
	2015,
	2411,
	2385,
	2341,
	2360,
	2360,
	2385,
	2341,
	2341,
	2385,
	2385,
	2385,
	2341,
	2341,
	2360,
	2385,
	2385,
	2385,
	2360,
	2360,
	1575,
	1575,
	1759,
	2349,
	2351,
	2411,
	3876,
	3489,
	1563,
	3489,
	1575,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	672,
	672,
	2385,
	2385,
	2411,
	710,
	2411,
	2038,
	2360,
	2411,
	3876,
	3489,
	3655,
	3495,
	3705,
	2411,
	2411,
	2411,
	2385,
	2385,
	2411,
	2385,
	2411,
	571,
	2411,
	2038,
	2360,
	3489,
	3495,
	2411,
	2411,
	2360,
	569,
	2411,
	3745,
	3745,
	3392,
	3456,
	3783,
	3783,
	3783,
	3791,
	3801,
	3784,
	3784,
	3783,
	3118,
	3119,
	3455,
	2965,
	3830,
	3747,
	3741,
	3232,
	3235,
	3246,
	2882,
	2680,
	3180,
	3747,
	3741,
	2884,
	2681,
	3139,
	2964,
	2708,
	3240,
	3240,
	3876,
	2360,
	2015,
	2385,
	2038,
	2385,
	2038,
	2385,
	2341,
	1999,
	2342,
	2000,
	2341,
	2341,
	1187,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2038,
	2038,
	2038,
	2411,
	2038,
	2005,
	1010,
	1010,
	2411,
	2411,
	2038,
	2038,
	2038,
	2038,
	2038,
	1955,
	2038,
	1999,
	1999,
	2411,
	2360,
	1984,
	1985,
	1986,
	1989,
	2042,
	1995,
	2411,
	2411,
	2038,
	1010,
	1010,
	1011,
	2005,
	1948,
	1006,
	1006,
	1006,
	1006,
	1006,
	2038,
	1999,
	2000,
	2005,
	1948,
	1948,
	2015,
	1949,
	1012,
	1949,
	1949,
	1949,
	1948,
	1948,
	1009,
	1948,
	1948,
	1948,
	1948,
	1167,
	1166,
	1007,
	637,
	1007,
	1007,
	1007,
	1999,
	2000,
	1948,
	1948,
	1948,
	1948,
	1984,
	1984,
	1984,
	1985,
	1985,
	1985,
	1986,
	1986,
	1986,
	1986,
	1989,
	1989,
	1989,
	3070,
	1989,
	1989,
	2042,
	2042,
	2042,
	3094,
	2042,
	2042,
	1948,
	1948,
	1948,
	1995,
	1995,
	1995,
	2411,
	638,
	2411,
	2038,
	1948,
	1948,
	1948,
	1999,
	2000,
	2000,
	2000,
	2000,
	2005,
	2015,
	1949,
	1949,
	1949,
	1949,
	1949,
	1012,
	1948,
	1948,
	1948,
	1948,
	1948,
	1009,
	1948,
	1999,
	2000,
	2000,
	2000,
	2000,
	3876,
	2360,
	1575,
	2341,
	2341,
	2360,
	2015,
	1572,
	1099,
	2341,
	2385,
	2015,
	2411,
	1759,
	1183,
	2360,
	1462,
	1099,
	1759,
	1999,
	2360,
	3305,
	3305,
	3745,
	3745,
	3305,
	3036,
	3715,
	3715,
	3745,
	3715,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	1759,
	2385,
	2385,
	2038,
	2385,
	2385,
	2038,
	2360,
	2360,
	2360,
	2385,
	2038,
	2385,
	2038,
	546,
	2360,
	944,
	2360,
	350,
	377,
	377,
	456,
	2385,
	2360,
	2015,
	2015,
	641,
	3092,
	3353,
	2411,
	2385,
	885,
	2360,
	2360,
	2360,
	885,
	546,
	350,
	377,
	2360,
	377,
	456,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2360,
	1576,
	2411,
	2038,
	1188,
	882,
	1575,
	2360,
	1576,
	2411,
	3876,
	1188,
	882,
	1575,
	2411,
	1575,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	2411,
	2360,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1759,
	885,
	2411,
	3876,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1759,
	885,
	1759,
	885,
	1575,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	593,
	719,
	1691,
	465,
	2411,
	547,
	709,
	2411,
	2411,
	593,
	719,
	1691,
	465,
	911,
	718,
	509,
	692,
	1363,
	433,
	2411,
	499,
	682,
	1334,
	427,
	2411,
	501,
	684,
	1342,
	428,
	2411,
	2411,
	503,
	686,
	1345,
	429,
	794,
	685,
	2411,
	505,
	688,
	1351,
	430,
	795,
	687,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	1759,
	885,
	507,
	690,
	1358,
	431,
	2411,
	2411,
	509,
	692,
	1363,
	433,
	798,
	691,
	2411,
	514,
	696,
	1425,
	438,
	807,
	694,
	2411,
	534,
	701,
	1522,
	445,
	855,
	699,
	547,
	709,
	2411,
	541,
	705,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	1759,
	885,
	3305,
	2411,
	547,
	709,
	1563,
	456,
	885,
	2411,
	2411,
	593,
	719,
	1691,
	465,
	911,
	718,
	2411,
	633,
	722,
	1851,
	470,
	993,
	721,
	547,
	709,
	1563,
	456,
	2411,
	547,
	709,
	2411,
	2411,
	509,
	692,
	1363,
	433,
	798,
	691,
	2411,
	514,
	696,
	1425,
	438,
	807,
	694,
	2411,
	534,
	701,
	1522,
	445,
	855,
	699,
	547,
	709,
	2411,
	3655,
};
static const Il2CppTokenRangePair s_rgctxIndices[66] = 
{
	{ 0x02000012, { 0, 12 } },
	{ 0x0200003D, { 14, 3 } },
	{ 0x0200003F, { 19, 25 } },
	{ 0x02000060, { 63, 4 } },
	{ 0x02000061, { 67, 3 } },
	{ 0x02000062, { 70, 3 } },
	{ 0x02000065, { 73, 27 } },
	{ 0x02000068, { 100, 5 } },
	{ 0x0200006F, { 152, 1 } },
	{ 0x02000070, { 153, 5 } },
	{ 0x02000071, { 158, 1 } },
	{ 0x02000072, { 159, 1 } },
	{ 0x02000073, { 160, 1 } },
	{ 0x02000074, { 161, 1 } },
	{ 0x02000075, { 162, 1 } },
	{ 0x02000076, { 163, 14 } },
	{ 0x02000077, { 177, 11 } },
	{ 0x02000078, { 188, 11 } },
	{ 0x02000079, { 199, 22 } },
	{ 0x0200007B, { 221, 18 } },
	{ 0x0200007C, { 239, 16 } },
	{ 0x0200007D, { 255, 10 } },
	{ 0x0200007E, { 265, 22 } },
	{ 0x0200007F, { 287, 7 } },
	{ 0x02000081, { 294, 16 } },
	{ 0x02000082, { 310, 15 } },
	{ 0x02000083, { 325, 10 } },
	{ 0x02000084, { 335, 7 } },
	{ 0x02000085, { 342, 17 } },
	{ 0x02000086, { 359, 21 } },
	{ 0x02000087, { 380, 17 } },
	{ 0x02000088, { 397, 20 } },
	{ 0x02000089, { 417, 18 } },
	{ 0x0200008A, { 435, 12 } },
	{ 0x0200008B, { 447, 12 } },
	{ 0x0200008C, { 459, 12 } },
	{ 0x0200008D, { 471, 13 } },
	{ 0x0200008F, { 484, 8 } },
	{ 0x02000090, { 492, 18 } },
	{ 0x02000091, { 510, 10 } },
	{ 0x02000092, { 520, 24 } },
	{ 0x0200009B, { 548, 29 } },
	{ 0x020000A4, { 577, 11 } },
	{ 0x060001AA, { 12, 2 } },
	{ 0x060002F2, { 17, 2 } },
	{ 0x0600032D, { 44, 1 } },
	{ 0x0600032E, { 45, 1 } },
	{ 0x06000332, { 46, 2 } },
	{ 0x06000336, { 48, 1 } },
	{ 0x06000337, { 49, 3 } },
	{ 0x06000338, { 52, 2 } },
	{ 0x06000339, { 54, 1 } },
	{ 0x0600033C, { 55, 2 } },
	{ 0x0600033D, { 57, 3 } },
	{ 0x0600033E, { 60, 2 } },
	{ 0x0600033F, { 62, 1 } },
	{ 0x060004DA, { 105, 6 } },
	{ 0x060004DB, { 111, 6 } },
	{ 0x060004DC, { 117, 6 } },
	{ 0x060004DD, { 123, 4 } },
	{ 0x060004DE, { 127, 5 } },
	{ 0x060004DF, { 132, 5 } },
	{ 0x060004E0, { 137, 5 } },
	{ 0x060004E1, { 142, 5 } },
	{ 0x060004E2, { 147, 5 } },
	{ 0x06000590, { 544, 4 } },
};
extern const uint32_t g_rgctx_MemoryExtensions_AsMemory_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_mF58CC8E18C1B6E3B4198C424E9F16182BF8D3A3F;
extern const uint32_t g_rgctx_Memory_1_op_Implicit_mF7AE3674C8BF81601B420C1B73F8DB8F9B542B47;
extern const uint32_t g_rgctx_Memory_1_tE98F0E782285E35CDE5649904841ED6C37803772;
extern const uint32_t g_rgctx_MemoryExtensions_AsSpan_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_m1F4235DFFEBAFBCC540FBABBA2C780CDC7A712FA;
extern const uint32_t g_rgctx_Span_1_Clear_mEEB1239DB01422DEF7BCCC8FA6CC95E410D472C3;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_ThrowInvalidOperationException_AdvancedTooFar_m84BE8ECA8C4547F5AE1AB790C829E020CB6DC0DC;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_t63AD3FBC7977686CF945E7E16A5EE0158ACBF779;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_CheckAndResizeBuffer_m91819934776FA62748F3F4ED94438E0413FABA75;
extern const uint32_t g_rgctx_MemoryExtensions_AsMemory_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_m65E916AE4107C98788CA77D0EAAE15CFCCBC4D0F;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_get_FreeCapacity_m61F197FB36F1C4FABAAB50E521CD2853E461F85C;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_ThrowOutOfMemoryException_m39033891A2F7CD956180902677B535AA9B8C9E47;
extern const uint32_t g_rgctx_Array_Resize_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_m0EDF4D90C67F2F548C4C597BDB8D05D49DE83F4A;
extern const uint32_t g_rgctx_Dictionary_2_ContainsKey_mE274E16A6B67732ECE0E4D97831A74005BFA98E9;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_m2B55E3D5A392F5DF6D7FF8FB5C43F4D5748474B9;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m879FB7766BF60786DB02A41BDC136525F5A50655;
extern const uint32_t g_rgctx_T_t85BBCD9AC2CAB891A15CCBF8FBF1D2989744A629;
extern const uint32_t g_rgctx_JsonParameterInfo_1_set_TypedDefaultValue_mE9BFF81BE2709A4BD71912F61BF4CE0F9E939DA1;
extern const uint32_t g_rgctx_TAttribute_tCA059A22FD68C93032902AC9C7F7B48AAF4EBFCD;
extern const uint32_t g_rgctx_TAttribute_tCA059A22FD68C93032902AC9C7F7B48AAF4EBFCD;
extern const uint32_t g_rgctx_MemberAccessor_CreatePropertyGetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m4EE52A435C08A02206B75F889BEC2DE1A5177D84;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_set_Get_mBAEEC5C94A50D85332AED9E355E5470C5E7D2B69;
extern const uint32_t g_rgctx_MemberAccessor_CreatePropertySetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m5B00A771EF7CF5267923ABD929653AE39B778494;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_set_Set_m9B2AC6480A8A3279BF2618E24694422BD5F05466;
extern const uint32_t g_rgctx_MemberAccessor_CreateFieldGetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m09958185200D96E11C595FB8BE699613BE95B3B9;
extern const uint32_t g_rgctx_MemberAccessor_CreateFieldSetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m8DCD5041A8AF8F861365562F6ACE65A57176258D;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_get_Converter_m4929853A8C0425DD53C3DC3FB32B1C34E9C0377B;
extern const uint32_t g_rgctx_JsonConverter_1_get_CanBeNull_mAA4C1B01AEB40C6DD65E0DDDA529FE1287C8ACF5;
extern const uint32_t g_rgctx_JsonConverter_1_tF31D95E4502793D2D2188ACDC7EE28AB71A867CB;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_set_Converter_mABF6E9F1B8DA6C8C230756970F157D4AC73670B7;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_get_Get_m7BBE6F4FF02D5E0AB34C0B12ABDA6DB19D4AE433;
extern const uint32_t g_rgctx_Func_2_Invoke_m7EDACF1A1EFDF2BD9174297975A386CBD4A8C919;
extern const uint32_t g_rgctx_T_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m738C0A86248478C728C71F0EC7EA08AA342B641E;
extern const uint32_t g_rgctx_EqualityComparer_1_tDDCB798373BA559CD7CDD0AA23B84CFD0DB19B8E;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m4490004BE141BE8A4E92DEC9A1DCE019F8CBC5C9;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnWrite_mEF49ED8C929026415B0EE9D1856DE085D7226BD1;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m6A68DBE5A1FE261F20BC897887628EC5FDC49C56;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m3CA741ACB6ACDB63AA94B126D534C7430AD3319E;
extern const uint32_t g_rgctx_JsonConverter_1_TryWriteDataExtensionProperty_mF9CDC845406843F8CD7B5FA95323F902147268D0;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnRead_m7124BA4F2A582C4C2E8A7B0BE4EC20F07AEC1CEE;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_get_Set_m19AFCBC21FDF39E98CAC29B75A53697F203146E4;
extern const uint32_t g_rgctx_Action_2_Invoke_m5B50AE22B20AD4F0C16E62F9AEB05074D70FB241;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m248BC0E4396EE5ADEE2A4973C27A9F982D16471D;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_m86CA085A0B5E65FCBAA39712CF1BB285D90FB820;
extern const uint32_t g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_tDC15B6BFE17B9C5DDFBFC356AEA00C29C1A174B1_m1B4B8FBA6F929B3C5A56733C58E214A70EA113F1;
extern const uint32_t g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_t3623392C33BDDE308A39343DB5498879CE44E126_m76477345E3B88C7014A38DC2116790562BB4E115;
extern const uint32_t g_rgctx_T_t10AF3DF63E49683AD9C61F470DE2F134F5A15EF2;
extern const uint32_t g_rgctx_T_t10AF3DF63E49683AD9C61F470DE2F134F5A15EF2;
extern const uint32_t g_rgctx_JsonSerializer_ReadCore_TisTValue_t6944BF8A7D13FF23FA33E45D81BEEF8ACD7C0ED4_m4D1D5BFB592F6805777DF2126577F2BAC7134FE7;
extern const uint32_t g_rgctx_JsonConverter_1_t90046F461E065DBA24C3A93193901AF8884A0C4B;
extern const uint32_t g_rgctx_JsonConverter_1_ReadCore_mD8322894748538F15B7FE80F9E79885E2EF20B77;
extern const uint32_t g_rgctx_TValue_t587DA454B31F55FA16FA8EA2D14DC8CBEDE10F85;
extern const uint32_t g_rgctx_TValue_tF3BB7476CB349D5F6909FB55445EDD70370F2501;
extern const uint32_t g_rgctx_JsonSerializer_Deserialize_TisTValue_tF3BB7476CB349D5F6909FB55445EDD70370F2501_m69992403B2E25990AD1D9E6FAE84665D14F48E53;
extern const uint32_t g_rgctx_JsonSerializer_ReadCore_TisTValue_t00A5D146DC0733FC8BCF93527E57E3C240748AB9_m0F393FAB004803DEE96E0A46D33BFBEBC4DD5F43;
extern const uint32_t g_rgctx_TValue_tB077FE17C477B1B00B14ABB906A2A7CB9BF64304;
extern const uint32_t g_rgctx_JsonSerializer_WriteCore_TisTValue_tB077FE17C477B1B00B14ABB906A2A7CB9BF64304_m66D652EA6BC6EC22BCBB315E5E8202898A0E6DE0;
extern const uint32_t g_rgctx_JsonConverter_1_tB84D187EA99E9BB9324CE76F5DC194E8D5D14C9B;
extern const uint32_t g_rgctx_JsonConverter_1_WriteCore_m2D5DA1787CE77ACE9D8307EF059CC56F9CBC75A8;
extern const uint32_t g_rgctx_TValue_t9569617E2CC8C00794F520A4BC079A389246436F;
extern const uint32_t g_rgctx_TValue_t28D0AC3BBE9F376D3BDF08EE8867DAB2D6C5B526;
extern const uint32_t g_rgctx_JsonSerializer_Serialize_TisTValue_t28D0AC3BBE9F376D3BDF08EE8867DAB2D6C5B526_mE8CD4ADC7FC44926253CD3890A31F3BF869854BF;
extern const uint32_t g_rgctx_JsonSerializer_WriteCore_TisTValue_t6C12AD18119E78F5C94C162CC0785EB99D03F7FB_m1B82973880DEDB7290980C474CE8EC883351F773;
extern const uint32_t g_rgctx_TElement_t19A7C4BAE3C763848ABFAF37D8EF6132AD249003;
extern const uint32_t g_rgctx_JsonResumableConverter_1__ctor_m2D78BF76B39A141744AA42B2E9850DCF2D0E4851;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t227DCAD70E724D34E4D78C809ACA730381A15AB4;
extern const uint32_t g_rgctx_JsonConverter_1_t0F00D8C9CCE5899F19F3688EDC9801CB37523144;
extern const uint32_t g_rgctx_JsonResumableConverter_1__ctor_m4E6BFF18F75C55D81A07CB2B860F3E796891E374;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t1EB57A0B4DE9EC17AD3FD40E2A73944669E59533;
extern const uint32_t g_rgctx_JsonConverter_1_t616144D5B83FA8B0FE2FDD19297F6536FBA2125F;
extern const uint32_t g_rgctx_JsonResumableConverter_1__ctor_mBD33548750C13A30456A9B45FFFF8968EF726616;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tCD4B0D239D94E33E4AEFE7643376845EF255BC16;
extern const uint32_t g_rgctx_JsonConverter_1_tA21DB6CB3CDB47B60F39D55755343BA8C3FAB1BA;
extern const uint32_t g_rgctx_JsonConverter_1_ReadCore_mD8A4A54CFE6083EFDD222B78DFDDE1479EC356C4;
extern const uint32_t g_rgctx_T_tDE0D4E880817A65183EA86172AD15260036118CF;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_mD9E2EDFED3D63B29ADC0E7DDE822AD9BEEF8E217;
extern const uint32_t g_rgctx_JsonConverter_1_WriteCore_m4807D2CA4DC860547AF3C7888A607DE3D1E9C6D5;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m9040DB9E7248BE16B6539EF0DB369CD200B77F52;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNull_m51C3617232E62030AEEB2AFCF000698B4CD7BB7F;
extern const uint32_t g_rgctx_JsonConverter_1_set_HandleNullOnRead_mC9EAE8F28AE1A87D0F85248C77BC3D5885E70E67;
extern const uint32_t g_rgctx_JsonConverter_1_set_HandleNullOnWrite_m80E739AF0D389D325C933E2CDDCC8031FC2F10A2;
extern const uint32_t g_rgctx_T_tDE0D4E880817A65183EA86172AD15260036118CF;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_t05F76B96D3217F9BE38AB0E76B27FC9746C5EFF4;
extern const uint32_t g_rgctx_JsonPropertyInfo_1__ctor_m5DB272A29AF1F1B3EEF1215EE8885451AC4DA367;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t8CE0B00E01196C05F9BCB34F2575828227D1BC8D;
extern const uint32_t g_rgctx_JsonParameterInfo_1__ctor_mAB891AFDEAA64324B6911941B96BC216F9D479FD;
extern const uint32_t g_rgctx_JsonConverter_1_get_CanBeNull_m8EE216DDB446CFD004BC66DD873604B5DE7BDC3E;
extern const uint32_t g_rgctx_JsonConverter_1_Write_mC1D44C1B0A1978507C65B2AF88843FB06D709F0F;
extern const uint32_t g_rgctx_JsonConverter_1_Read_mD450EA2383FC87E223AE4BE91EE1004039D0B0A1;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnRead_mAD2E7B1F83A93434A083BBA32D92C761DA8E2327;
extern const uint32_t g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_mF241DE8685A34203E077250BD57F1B94BF0DEC46;
extern const uint32_t g_rgctx_JsonConverter_1_VerifyRead_mEA950F19D224BE80F1AD29949AA39FBDC219F295;
extern const uint32_t g_rgctx_JsonConverter_1_OnTryRead_mFB0BE200C26C2ECA459BB176FB974CC216C8C19F;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnWrite_m20F8D09FA55E3653E2015D7DDFFBA4AE536ACA49;
extern const uint32_t g_rgctx_JsonConverter_1_VerifyWrite_m118A84AC7356FEB3D6865087BD7B1FD0BA2E1569;
extern const uint32_t g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_m136E8C67C68AF337D22BDD0C53266B99A180E576;
extern const uint32_t g_rgctx_JsonConverter_1_OnTryWrite_m1FBB663B55D4E212ABD2D6DF7E7CA244A5799A07;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_tFD81D34ED71E5EE89C127D7C9D2EC85A8F165511;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_OnWriteResume_mE3DDDBA151BD54BA11B454CBFD44E06A7AD83F01;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_mC6E3A536F035FF027DE5BE35BDF74FB750529CDE;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_m90104343AE07395A78FD6D7A3E67CBD540011BC7;
extern const uint32_t g_rgctx_T_t5EC891C0C5C31AF339487D8E674631366A53C71C;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m1B581CB4072362B3B21400E91C4BBC650DB7AD11;
extern const uint32_t g_rgctx_JsonConverter_1__ctor_mF6E1FEFAEF7A27AE7672D0D8FDA4D90A82196821;
extern const uint32_t g_rgctx_JsonConverter_1_t139AAC2EAA2446CD0F4DB92F960D7C497E48EFC9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1_t0926D22A810011F4225A43D22418EC4E0BC4601C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1__ctor_mDA399E33ACFAB3725E755E876899FFD3264F30E0;
extern const uint32_t g_rgctx_T_tE7401DEABA1E09B0E88CB7BAE804F5207856A5ED;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1_U3CCreateParameterizedConstructorU3Eb__0_m44799D8C1B09050EFE6C34663749A7015BC8C9FD;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_1_t13127AFBDBFD424F3FEBC6F3B1F77D65DB5CA941;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_1__ctor_mC0179B26C4BF315393B598A677F0050E6CCF7B69;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass2_0_5_t9BE909095532D90B31BAD8FD387A4F0B3B26D858;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass2_0_5__ctor_m8E6DF80969A976B71BA074DB4D5CEA77D586BAD0;
extern const uint32_t g_rgctx_T_tE1CA391AC376C971F1C63C5690EA51664661A506;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass2_0_5_U3CCreateParameterizedConstructorU3Eb__0_m0C3054B3020662F0C19B6CE487EEAC8E923F22CD;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_5_t57D5C8B6CD33EB1730F808F51500715793F621B0;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_5__ctor_m0D11851117BA4E8D07AF0EE3630FEA3CB53B6F63;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass3_0_1_t4831C1BC7BFC0ED3AE0FF4781F3B985E1A2F2949;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass3_0_1__ctor_m82525090A3E3243058959B1F0327965989B36165;
extern const uint32_t g_rgctx_TCollection_t6C8736963D6371BF8E56DABF17A465D02FE42480;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass3_0_1_U3CCreateAddMethodDelegateU3Eb__0_m7BA41440E0F33B6E0B403FF87C2640B1DEB1CE6B;
extern const uint32_t g_rgctx_Action_2_tB59C6C8DD475DDD363125DEF571B9AE537237593;
extern const uint32_t g_rgctx_Action_2__ctor_m760F38FFFD0583FCF3AEB9D58CBC4109949DC229;
extern const uint32_t g_rgctx_TCollection_tD8F4E280835AE5F514654FBE445988E3450F1201;
extern const uint32_t g_rgctx_TElement_t3325851B0C656D4DCF70FDAE8FEB2706E7394445;
extern const uint32_t g_rgctx_Func_2_tBF80C5E3CF469C7AF36E01928E5DAE257B37C779;
extern const uint32_t g_rgctx_Func_2_tBF80C5E3CF469C7AF36E01928E5DAE257B37C779;
extern const uint32_t g_rgctx_TCollection_tC046CA0D12910C1B2098C9F6ADA8C376D84602AE;
extern const uint32_t g_rgctx_TKey_tC4874DB2B0CA1D46DC0B766A2C71987A0F733848;
extern const uint32_t g_rgctx_TValue_t49C969BE5718E0CC5E4BB8E4670C07B8AB1B52C8;
extern const uint32_t g_rgctx_Func_2_tEA5B441B140A1B38607590B76319E9BCCF1CBB93;
extern const uint32_t g_rgctx_Func_2_tEA5B441B140A1B38607590B76319E9BCCF1CBB93;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_1_t91931F5A16D3711F067E03817CBAD62C671FB471;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_1__ctor_mD7A152A0655E76C8783AC6572C25B0331FFAC2DC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_1_U3CCreatePropertyGetterU3Eb__0_mD329A8C22E772307DB1B733567AFDD240B40387C;
extern const uint32_t g_rgctx_Func_2_tB234AE6E3C9B31E6E69B357468E80ECF66BEA77A;
extern const uint32_t g_rgctx_Func_2__ctor_mEF886281B4C4EAC36DB4B38B3C09D6A69DDE0943;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_1_tB3D03CFF9F7B9B8E15FA6DC774418DD88FDE21F6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_1__ctor_mEC8572EFD97ED89FC5D081546CE08A1E7CA5EFED;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_1_U3CCreatePropertySetterU3Eb__0_m0A24C371E5C21288C8576BFD5A6006429F09EFE3;
extern const uint32_t g_rgctx_Action_2_tB94215CCF8D800A8BAEE899B237F548D03B93070;
extern const uint32_t g_rgctx_Action_2__ctor_m496A9C29D53F6FBD7D9C5DE97769E684842BCF27;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass8_0_1_t99E394A7E11801AFA9BE7820B49A47BA1E836E42;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass8_0_1__ctor_m815543799CA407D85483AC0F10364BECE4BCFBA6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass8_0_1_U3CCreateFieldGetterU3Eb__0_m9CC3419081ADFC0B13C2F8338FD144C4E7FA5503;
extern const uint32_t g_rgctx_Func_2_t7FB0D2D2B962B79CCBCE7392C495066EA4459E1E;
extern const uint32_t g_rgctx_Func_2__ctor_m768E5C61DEAA6B9CC347042E074C987547D499F1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass9_0_1_tE4A0EBE0323ABF892388F0E6DA56816CF44E85D0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass9_0_1__ctor_m123CF49F18B96DBCC17D205F6ACC5851F923D497;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass9_0_1_U3CCreateFieldSetterU3Eb__0_mD58C961FA5D59912D9DBC44B2844CA690F4EF6A7;
extern const uint32_t g_rgctx_Action_2_t7BA37B800E03DBEF4D1E448699D1525E2EB08506;
extern const uint32_t g_rgctx_Action_2__ctor_m93E858A6D690564CEA11FF3B0E19C5B8E2569D4E;
extern const uint32_t g_rgctx_T_tC6B0FE90DAFCB0F6495B63D3B49C20084840959E;
extern const uint32_t g_rgctx_TArg0_t774421C662D6F6B418989214CBF773A578FBF39A;
extern const uint32_t g_rgctx_TArg1_t580AFB4584610C707826CC51AF29830419D1785D;
extern const uint32_t g_rgctx_TArg2_t97D0732BEB4C0DD134070B70AE7FEDDB80E4F1DF;
extern const uint32_t g_rgctx_TArg3_tB45251A0C832E0228BC7FF67033D6E8886553970;
extern const uint32_t g_rgctx_T_t92265CC3BE95C6C18ABB347CC0D2CAE1F4A4F166;
extern const uint32_t g_rgctx_TCollection_tCD0321429062BDEE3DF447F50E755CE5A1DADEA4;
extern const uint32_t g_rgctx_TProperty_t0B19ABC77D06DB5323908182C55A483F75765428;
extern const uint32_t g_rgctx_TProperty_t5ED322B05D0FA1CFBC0E354E8668EF98E3C1DEF9;
extern const uint32_t g_rgctx_TProperty_t7D80DF5A3BAECC9AA3EC71C9E4EBE0E19E8487D6;
extern const uint32_t g_rgctx_TProperty_tEAA1F016214D1F3D50F8B33AAC9BA55162D651AA;
extern const uint32_t g_rgctx_List_1_tEE0D37B36B6698A7EB0853BF66AC874E7649D1A1;
extern const uint32_t g_rgctx_List_1_Add_m43B63B384FBAACFF040D681431183AFEF91BFF0A;
extern const uint32_t g_rgctx_List_1__ctor_mFCEDB34DBA88FEC30E5DEE641459C64B280EB9CF;
extern const uint32_t g_rgctx_List_1_ToArray_m3B508156490A2C1458717888B0E88003878EB35C;
extern const uint32_t g_rgctx_TCollection_tFE1BCDF71192BC5A1A0807B54E9483D1BE0F5C5D;
extern const uint32_t g_rgctx_TElementU5BU5D_t207143EA08396800E047242F8E98333806269387;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mE12ADC597BB87805FF2341257CE3F795909BFD87;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tDC405E27420BCE1CC6DD34821CCDB6F944D0EA27;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tEADD53AD0D60A8DC30C283964E5004486C54EAB0;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t57A0B2836BBB19A929F582819E2AB5E38A167BA9;
extern const uint32_t g_rgctx_JsonConverter_1_t5F92BBAD382076D981616878DF94BB8D24C92621;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m86A0B9B8F2521B4BE2AE8EE4DC70799F9E92BA63;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m94ACBDC1D358B72F3C4B448A34F2A66F1ED75BD6;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m56C4F5BACA41757A54966A1D934911A27B861DD2;
extern const uint32_t g_rgctx_TCollection_tF323F07C3BCB39AF9217D9DB0F9A46BC89ABCA9B;
extern const uint32_t g_rgctx_ConcurrentQueue_1_Enqueue_mB82D58E81FA848A5F8F9A1F3BA6FDC4DF197E057;
extern const uint32_t g_rgctx_ConcurrentQueue_1_GetEnumerator_m3DF06AEE521BA4782B9359D1C0B31DD5F9801772;
extern const uint32_t g_rgctx_IEnumerator_1_tE87E43DE121279E093714319E6A41BB77B9DC62F;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mF724AFBC473E14ACE7D1864458F233C619F7EA50;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tCFB9621DB5C3660F1E785FBC4947F1476FD38C3A;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t2E201F7E652DCA4434A6307B9145028401AE95A7;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tFF595E49C72F236956DCDC7794F9D8ACFA0E147E;
extern const uint32_t g_rgctx_JsonConverter_1_t31F16D2065F1F0299C2DF14AAE92627F4E42BADB;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m52921B0803C5D8088E662DAE2965BD490CA8C5C6;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m806E1FE66E78F1F4CFD1706C2D2A564AB0E47DA3;
extern const uint32_t g_rgctx_TCollection_t717009D3E93E27EB4863733BE8555F6AA657E870;
extern const uint32_t g_rgctx_ConcurrentStack_1_Push_mF228FE07B1EB9AE4F1EDC04EF211E37C1BC2B3A0;
extern const uint32_t g_rgctx_ConcurrentStack_1_GetEnumerator_mFAE3B25792DB6EA0446B480BDD9FEA5A172EAE2D;
extern const uint32_t g_rgctx_IEnumerator_1_t74BE349DD99E7220EE0F3D19165F01338F7C3FAA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mE40F2001300C4E2583375C6A085480490ACAA935;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tDCE4B76F5788120651FA7C842BF4880D2247C532;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tF8F70D7EEAB90C01AE9D7CD25925C4A429993428;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tC003E6826A1D182745ADD254A5BE08DD24E87D5C;
extern const uint32_t g_rgctx_JsonConverter_1_tC32A26C76DA3B4AD05E641244B1D814BF6AA1482;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m86BD33A114A6707E083AD4574FEFFCEA1E052784;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mD11179EA58C131FB5FC83D7C4527467AE5763306;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t88661170937490B661E6F80F3C72D3CA71B2D87B;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t8823389F2CA753C3FAD912164DD19B5562BE77EC;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t22DCBE92ADDE649C23897AEE9A90AD8F9F68BBBC;
extern const uint32_t g_rgctx_JsonConverter_1_t95F83D7E3A95D53158D1B62E2842E3679ADD98F7;
extern const uint32_t g_rgctx_JsonConverter_1_t2C8C2A491C318F53EF445B7A6C007CA0150F227D;
extern const uint32_t g_rgctx_JsonConverter_1_tD0E51510C147326BD012ADCCE2E04375E7A23C5B;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_CreateCollection_m4B77A09DCE1E039C49642FA87001A7F613F7536F;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m22568C80298B1A02D1428B623FE94C50D1544965;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_U3COnTryReadU3Eg__ReadDictionaryKeyU7C12_0_m614A23A7EE43EB85184E9B16D008F1EAFB0E307B;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m54E33C9C55B0E8B015E1B93A169E332991A35005;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_Add_m2FF0254F275181249CBC705B74E0D2AA02218B10;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_mF1D91D459C2CFA9AB0AD3062085E88E881627E7E;
extern const uint32_t g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisTCollection_t0133571FF836DF7A5DED584AE98800C044D3C9CD_mFB4B66D159FCB4A3EFF469915ADAFDF4D7EDDD46;
extern const uint32_t g_rgctx_TCollection_t0133571FF836DF7A5DED584AE98800C044D3C9CD;
extern const uint32_t g_rgctx_TKey_t402E0D26D2A5BEEC462BB7D6D4B433885EC97C08;
extern const uint32_t g_rgctx_TValue_t7329166BA6BA8950820E2A718B3F825409635A62;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_ConvertCollection_m79237CE5E949A203751890932E566887311EAC2D;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_OnWriteResume_m182DDD506ACFFD323CB649A37FAB640CB8CB40EE;
extern const uint32_t g_rgctx_TKey_t402E0D26D2A5BEEC462BB7D6D4B433885EC97C08;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1__ctor_m451CEB87C6CCDA30DFA4F89658131C849E26EB32;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m10502C5810C7FBB8664B424B37EF182483479C2B;
extern const uint32_t g_rgctx_JsonConverter_1_ReadWithQuotes_mA6FE0B45163A5769B11CED9F4F0D910BBD437955;
extern const uint32_t g_rgctx_TCollection_tC6C1C819230EB84708DD60208E5B9A12A51C3B66;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_m07A678C05FAA1694E3AF98992201965A6B93C392;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_m26CE10E6DAD8B8A50187D77EE94ED8156A8ED8F1;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mD43D02F55D0AC4E8B773C2C3A35278FC47BDE046;
extern const uint32_t g_rgctx_Enumerator_t9D797C755572F82D131A2EE971E1384AD98D15D3;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_mDE73CA9089DC074D07B97214B70D0D00198CA87B;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_tB9A97CBD64046C7A78A3689D566C8E16487E264D;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_tECEBD70B22BA7FE6EB2E065A6C10405E338767C6;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t0CDFAC0BD9941090B876EC5ABE59A944F173200B;
extern const uint32_t g_rgctx_JsonConverter_1_tE84915513E58C5A237588B254714F32AEFFE1503;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m03A8616193878DC7EE33D79AC1A556949073E0AA;
extern const uint32_t g_rgctx_Enumerator_get_Current_mBF9707F2B029C2261A58FD04B115561589DF0690;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m1AAC6DF5AAECECEE181B5F2E460AE11FBCB13010;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_m0FCB6AFBCB2D4509207CE6D9EE3D8C27F478ED1F;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_mC9C292806F6710BEC5FFB49A33105EB0A1A120AD;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m4C7154527948D35D8DD51816A1C409BCCFCA84FE;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m35654B06EEC06DE212109DCAABC9F95F516DDA54;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m0B13A3286E5543331A10B09F74B3C2E896CA791B;
extern const uint32_t g_rgctx_ICollection_1_t428D3E6D09DCA9ADAA1503431DA9DB2BC804E3EB;
extern const uint32_t g_rgctx_List_1_t8D6A38BC72A55594282908E861987521AAF35C95;
extern const uint32_t g_rgctx_List_1__ctor_mBDE43364840B1C3AD4B57736449FD2914B5833DE;
extern const uint32_t g_rgctx_TCollection_tB07E8B1895BDD12214706F8A4DFAE90CACC03601;
extern const uint32_t g_rgctx_ICollection_1_get_IsReadOnly_m21B3DAD2E48A74923439773E01AA33917F4916DB;
extern const uint32_t g_rgctx_IEnumerable_1_t75285D593EE7E4426E1B215A835EDED19714F6C8;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m70042CDDD931E6776561C83EB565AD58C2FE3BFA;
extern const uint32_t g_rgctx_IEnumerator_1_tADDA8DEF1353F794B605DE1CD10CC86F7E5B5BE7;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m0398A49E7B8EAF6499BEFBEFB78C97C92AB3B07B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tDF57831330A32E400946AAE9D2BEE7C0A525F1D6;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tA9308F285C872B1B3A01C892C282C7E782ED1BF6;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tECEEC9D8300912840D776FF42B9D5B5E92E62B3D;
extern const uint32_t g_rgctx_JsonConverter_1_t608C7677D56CAC04008D8F25BEDEF1A6FDB2D603;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mDACF7BE8456673E6FA7560623981E3EDB66DFB72;
extern const uint32_t g_rgctx_List_1_t8D6A38BC72A55594282908E861987521AAF35C95;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m4DB551AC0C496D94EE76A8CCBF163489F7189500;
extern const uint32_t g_rgctx_TCollection_tD5A10283589FDFABD07D52CD2F4C61D771570FAA;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mE473432CBB7F9154D027A6D69FA523265A536327;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_tC1833356173D5B322F9DD0ED1F19C5FE963AE06E;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t08E19C35DAE7831F8BEF4554184C1ACAC0C3AB7E;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t245DDC32994097DA72ADF364820F41419AD40EFB;
extern const uint32_t g_rgctx_JsonConverter_1_tA288F21A831BA62BCB614385357885775C07F25F;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_mA045A8B2C39CEE01E6E2C00F401D7130241E00B1;
extern const uint32_t g_rgctx_IDictionaryConverter_1_GetObjectKeyConverter_m5ED410D268C69B21B95D08352CC19B8E0024B686;
extern const uint32_t g_rgctx_IDictionaryConverter_1_tC44D8F6A71F1007FB947A89E5835C7F42114C044;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m2668B5B8915173C68B39ACF5F78BD9DFE8B724E3;
extern const uint32_t g_rgctx_TCollection_t58D8D5B3F5006ECD0B20BF1C18E00ADEBA7AC5AC;
extern const uint32_t g_rgctx_IDictionary_2_t188F7C8F3085ED128521213F4BB857B998A0BC44;
extern const uint32_t g_rgctx_IDictionary_2_set_Item_mE812117273063CBF7E7A83EC002887A9A00C4F89;
extern const uint32_t g_rgctx_Dictionary_2_tD331E563AD4AAEA5E2C3EBE9B534D0871556B94A;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mA61485AA880C8E244AE7F312775BD08FDC8E5C3E;
extern const uint32_t g_rgctx_ICollection_1_tE61A31B2CF365A79A7BE2B4F232FCB08980AE086;
extern const uint32_t g_rgctx_ICollection_1_get_IsReadOnly_mB479893E8ECEEEAC21325E04A9271EF97694BF40;
extern const uint32_t g_rgctx_IEnumerable_1_t1B3983F68A5CCAD0299BE963780609DDBBF36B48;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m79A7F32754B0AC0039B715A77AF1B0F5E41655E7;
extern const uint32_t g_rgctx_IEnumerator_1_tEC38F7E6414D5CAAB54D0472639203485A6F4408;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m593E8AEE1BA1178AD63CFE3264DF47F1319F9829;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t5D720BB5654DE077F4BDBCA9D34AA236F3224E3A;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t9D5467978E8E94887BAEE91FF72090ABC0011977;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t77DA22F25F30F51EF3C9F02B30C824FFA7E55A0D;
extern const uint32_t g_rgctx_JsonConverter_1_t3CE7B05E2B84D052AAAD1E900E9210CD06EAAFDB;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mD8EDB3F1AD8086D6F060A284891F111698E8AF79;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m0D06E1C119BD8CA83AD360194CD052190D93576A;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_m4F4558979550DB83F02D44AC845810D8EF911270;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_mE8443A178C363CC740276564792B82CA35343ADB;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m889B027E15F0E619EACA93359B0230B3F97E461B;
extern const uint32_t g_rgctx_Dictionary_2_tD331E563AD4AAEA5E2C3EBE9B534D0871556B94A;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m135B8360C418B42A90A45BEBBF5E3C7A8F4AE7E8;
extern const uint32_t g_rgctx_TCollection_tA97942A21A000010CF0D79DC5296A087B6527008;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mF9D774173B0F0EE2132D6BE76798F431D39D850C;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t15CD8B5B1D51BFCE6B50749F1DF65B314A348DF9;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t52DC1967AC13240DA967EE40822357E9D432A2E4;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t2975753DE4BA6812EFF248908A289DA12F7D00E8;
extern const uint32_t g_rgctx_JsonConverter_1_t8B5043328C05B8FED16900B1A0AE171E3309C7B1;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m4817B378081B72135E134595DB7F1AD91EAE6BCB;
extern const uint32_t g_rgctx_JsonConverter_1_t3E8AB39B2D1317F03C1A178E7A5B90573CA61FEE;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_CreateCollection_mDB7961A98C4B4355F8F9D053054063E1F787CD75;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m6D64E56FB03FDCC431AF77E6715CC25CA034B83C;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t2DF14B0BD54A9EBD74DAA94C0C81E9237C41B53F;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tBD733BDEFBC61B16F3719CB2CCE54FBF8E726177;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t255976D38EB099C66B6375281E795AC4E503D963;
extern const uint32_t g_rgctx_JsonConverter_1_t962FF5446841F50DF4FEE623067EF6B89A304975;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m7549B69ABF7F1BD38C340F459141A1EF7049B163;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_Add_m8266E9F7451A24CD0A346B892C054F066A907610;
extern const uint32_t g_rgctx_TElement_t4957D75B61CAB682EE72C9A330EB9D6C32442826;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_mB0110A2EEB36B55270293188006B7D9B3E4DA296;
extern const uint32_t g_rgctx_JsonSerializer_ResolveMetadataForJsonArray_TisTCollection_tFDD8CFFC1593E29720C3F2ECD6E6E0A034F82065_mACA82990A22BAE7E8D24C449A3EEC2F382047FF8;
extern const uint32_t g_rgctx_TCollection_tFDD8CFFC1593E29720C3F2ECD6E6E0A034F82065;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_ConvertCollection_mC73323EFF3B91A5DB0AC67568B542159ED9F5A4A;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_OnWriteResume_m604C17DD5D6469B18CBAF04B6F7B9FF4E4A95587;
extern const uint32_t g_rgctx_JsonCollectionConverter_2__ctor_m12EB18285B1CCA710D1C445D8DA0A08D58498768;
extern const uint32_t g_rgctx_List_1_tCBA9D7390B8DA29263AF7ADE7B1ABFB3D6240C09;
extern const uint32_t g_rgctx_List_1_Add_mB1C1942FA33111F8777CD7CCBC70CCB21C71A3EB;
extern const uint32_t g_rgctx_List_1__ctor_mC91B0572772DC47AE1C61AE661B5FF9034BB5047;
extern const uint32_t g_rgctx_TCollection_tDF47E9BABCC87D3192BF878AFAE79EB48271E86D;
extern const uint32_t g_rgctx_IEnumerable_1_t76692457895ECB987E2BCC5DF9F79EF95AB94F03;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_mB905D80F0BD5FB20DD676442C0D3ADA531258404;
extern const uint32_t g_rgctx_IEnumerator_1_t102D8A21E598723B6E153D56F055664AA65B4647;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mB42AC3B69AED93E876667D58F3D69FA8CBE7AD88;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t54309471C27A7D8BCEA7E3B51474BD0F2D73B0AD;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tA8357BA3ED1DAFA829A6D16DA4658666964A9F6B;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t8E5EAF54644F6BC1ABCE48813EF98E90D62C6F39;
extern const uint32_t g_rgctx_JsonConverter_1_tF1A2CF457909E13AAF51DDD9B89A820E30F215D6;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m7D25C666EE7AC6F4C0D36EDC3708C782453E2337;
extern const uint32_t g_rgctx_List_1_tCBA9D7390B8DA29263AF7ADE7B1ABFB3D6240C09;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m4C59BF06CF733848470107F309D258A68CA23326;
extern const uint32_t g_rgctx_Action_2_t0E6761CAE887A35B965B54DA89ADD9E43B7E5600;
extern const uint32_t g_rgctx_TCollection_t799EDB522FF11B2AB74F132FEC2AAD6C2B4E8DEC;
extern const uint32_t g_rgctx_Action_2_Invoke_m94BCF09A1CE9BD358F84CEEB44343B1357604657;
extern const uint32_t g_rgctx_MemberAccessor_CreateAddMethodDelegate_TisTCollection_t799EDB522FF11B2AB74F132FEC2AAD6C2B4E8DEC_m848BFDE14B3E458EA07B2BA46CBDD08A5F2C8069;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m9AED333EB6DB54958C9712E1B365CC87D4F2D16B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tDBDAFC025080057E0751B43B9971B2C23970158F;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tCEB03983BD7513141001CA432D4983D2A199995F;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t727E751A0FFDCB4F0619DEAB35814082E30FA8BD;
extern const uint32_t g_rgctx_JsonConverter_1_t7032A458CF62EE8BD66E8FD668DA8418EB0A70DA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m09936966D6E4ADAA651A2DF3A80EE4A474D1AAB6;
extern const uint32_t g_rgctx_TCollection_tBCADD67D8AD0813DB41B07AD2D2521C19CCCC3FA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mA952F8162E3EC0BE62CE0053C7F79BD044939278;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t48C0A78772B197A2E07689F032E2976AD9156361;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t64C4E857017E4DF89CC9516B70D64F3AC09C8C5A;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tAC17C4EDB1BD1A2C01DC7FF4AAA55732E855CD4C;
extern const uint32_t g_rgctx_JsonConverter_1_t7F2EB94EA5540D6091CFCB350BD5B2B19CEA1854;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mCAFC9B9363278FEACB3F6203DF754D679722577C;
extern const uint32_t g_rgctx_TCollection_t21A65B1B7FBA9BDA560D3FA0DC8626A4C05917E4;
extern const uint32_t g_rgctx_ICollection_1_tA4A0644D6B9FC776E906C1BD18D07A56F0A25172;
extern const uint32_t g_rgctx_ICollection_1_Add_mA073D133AF88E0EBB9F7FF2E156E110FE99D60AE;
extern const uint32_t g_rgctx_List_1_t2F14D6CF23F177B548432AC302931830C0472C41;
extern const uint32_t g_rgctx_List_1__ctor_m0581F9111206B4BFEDC39ABC06313D6264C4055D;
extern const uint32_t g_rgctx_ICollection_1_get_IsReadOnly_m1C710BC5DECFD13AC6A65C0FC236A25E079F9C2F;
extern const uint32_t g_rgctx_IEnumerable_1_t3E764FA42F7D57912EB5B4920B68B3E6188AE841;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m0D3CBCC497779CCCE20999423FCD61E50848F679;
extern const uint32_t g_rgctx_IEnumerator_1_t5B2A9204A9643387075C230ABEFFAB5D98FEBCF9;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m355F647FF4C500FF94922C3942B711474870F03C;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tCDACEE0AF9902268A6ACB2A1AEE64644C33F3513;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tB82ACB8E5569A4EC4FD6BC9A706729BE274326DC;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t63059B59E2F82858426B185012A34A0353EC1795;
extern const uint32_t g_rgctx_JsonConverter_1_t7292C0FAE7407D910A2B28D4D1334177A1EABAEA;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mB980121C826178DEC85AB23EEC518B4AAAC7240C;
extern const uint32_t g_rgctx_List_1_t2F14D6CF23F177B548432AC302931830C0472C41;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mB7A149D378F9F374F7D091EB6D56A3A292AC817C;
extern const uint32_t g_rgctx_Dictionary_2_t20984B6F8B425283A733E0CABA3E6F3D17850403;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_m5581272A65705D0783F53F14E55E551D002EBDE6;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m9212C5F14F0AEA6DB6FB563B4A5787E151E01BDF;
extern const uint32_t g_rgctx_Func_2_t6AD58D7B12648FD244D32E52F892E440FF3EDC4A;
extern const uint32_t g_rgctx_MemberAccessor_CreateImmutableDictionaryCreateRangeDelegate_TisTCollection_tA0A2C1302BB5DF06A43C4BCD8736392C9E9BF0BC_TisTKey_t1CE0855F3B31DFD0DCD2CE962F54C331925997F7_TisTValue_tFD898E4A43B9076A83C3A57D74F6E181EFB852AF_mCF3E93E8985F24227221D7687A71FA58329ECEEE;
extern const uint32_t g_rgctx_Func_2_Invoke_mB452DFF9F88E63E2107DE3A729ABA52D6D14132C;
extern const uint32_t g_rgctx_TCollection_tA0A2C1302BB5DF06A43C4BCD8736392C9E9BF0BC;
extern const uint32_t g_rgctx_IEnumerable_1_tD3C5C60F762756DDDF9BB9C8E4DADEA876A64D9A;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m0056E95B7DD46DD45167C6AAE25A57B7D008388F;
extern const uint32_t g_rgctx_IEnumerator_1_tFEADBCF91E0D7F6BF8ED12C2093CA7C325014D97;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m30FBFE5A1D5DEDBB24D610EDBE08C0496548169F;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_tBB84E087430FA3A9E69C601EBC4A81CA73EDB979;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_tE51516103776B70B7DE3BE6D72DD68329352C03D;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t2923EA425521BA652C4B2FDC8BBE0462239BDE48;
extern const uint32_t g_rgctx_JsonConverter_1_t9CB3B8B7B436A5F7FC236B513CC0C1F3F451BB33;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m506064AD83BCF99B2068CF7AFEC1C6EDCD156A49;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_mB3DBDB39589F53098EAAB564963F0B0F439EE09C;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_m7F39872CDA4352027E1E49FA22770329B4900ED5;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m03C1226C4D804E7362C24F36D6A21A0D2630013C;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m4097035F72D5687BBD08CCA72AB8F32566100DEC;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m22BA267708F72D30023BB607E3BEF8EE420FD72A;
extern const uint32_t g_rgctx_List_1_t7BE3F369360374FC3BBA2E746E5A05AC11FB0A39;
extern const uint32_t g_rgctx_List_1_Add_mF4E576DC2B8C9F549CAEB5B6DFF833C552A501B9;
extern const uint32_t g_rgctx_List_1__ctor_mB2C9A4601BFFD183F943B60325798C9336C32D88;
extern const uint32_t g_rgctx_Func_2_t1B976705D65D2BB5879254AAC54AE0D743258B39;
extern const uint32_t g_rgctx_MemberAccessor_CreateImmutableEnumerableCreateRangeDelegate_TisTCollection_tB497759E8C60630AA9D3A5C10EF0BFD171ADD19C_TisTElement_t6950EACF58DA18F55062E2799C599F69E0CF730D_mA64AC58BD0312D2844F67C2E0BBB785F5A27E255;
extern const uint32_t g_rgctx_Func_2_Invoke_m89497E9AA57F69215DAF15528B5C523D2E2276FF;
extern const uint32_t g_rgctx_TCollection_tB497759E8C60630AA9D3A5C10EF0BFD171ADD19C;
extern const uint32_t g_rgctx_IEnumerable_1_t9C071D199112CEADB8EBD36A02F387A0D1F829C5;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_mFCCE92AB68820FDF40294AD571B8DEFE35D1B2B5;
extern const uint32_t g_rgctx_IEnumerator_1_tD0BA0E7C4B142E9478202DC3A66CFE4F74EA3ADD;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mBF7C34D9E603FB35D6806F1DF4BAEF1606BA1367;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t027C114EB3D0EBB8214532E19D5F19972BB7A27F;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t51D9120653B04B1E943BB0C67BEBE0D0012B5FCD;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tFC5A7DCEF749AEB034378DDD07864B2FA83CB563;
extern const uint32_t g_rgctx_JsonConverter_1_t395634378914272768937D85CD760AEF64F5F48A;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mA6C58F60114967A9A80001D29580F84992985C1B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m458A39414D829052EC7C3EAA349B7A4A71E4D739;
extern const uint32_t g_rgctx_Dictionary_2_t31C6FF9CF8E5D1D370276501C199D42F140B0838;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_mC8107DA4E6ACA72A4D1BDC1F1BD1EA8AE7D1DD8A;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mD2398725DE34F6A534CB3C05C5FD17FB7C8ED37E;
extern const uint32_t g_rgctx_TCollection_tC1454A0418C7984938DF8B02DD4152B0892F4645;
extern const uint32_t g_rgctx_IEnumerable_1_t85DBF7CC9CAC87782C559FD742C62783DC4530B3;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m96AEA0F814ED2C961ABE528DE1E5607A600F1D27;
extern const uint32_t g_rgctx_Enumerator_t3F3173A3C90A4A5C981DDD159F7DCB3C0A972A27;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m56E585B514E00802D5944A92B9284072A10A6FBB;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t34AED40F6DEAC2A1E4C177B38C9C39A0C825B717;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_tEAD348CF3354B8C4AA20F786D7F6367F599F4D5F;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tC6102A647A394CE56E7254EE041F447A46AF45BA;
extern const uint32_t g_rgctx_JsonConverter_1_t74A0DEDDF80B351E726803A4EA6B8FC7CE67C062;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m70E0D6BBA1BA42F9E5083452E31C636DB2705569;
extern const uint32_t g_rgctx_IEnumerator_1_tF102596CB84F48E7CF8BFA3DE7236E6843127709;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m1E658792F6C5AEB814A5AE2EDDC42B9EC71AB627;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_mF80A1E43064F679483843617562756DD03B0F92C;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m58C773B1DA09E73CE56DBA200901C609CAE009DC;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m39A5574C38B9B5E6F051974C089B2C69F2292078;
extern const uint32_t g_rgctx_Dictionary_2_t31C6FF9CF8E5D1D370276501C199D42F140B0838;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m069016CD959116FA0D966B2E122145B87E3E095E;
extern const uint32_t g_rgctx_TCollection_tD2C29EBDDFE77AD53DB4F72CCA84FE16A87DA2AB;
extern const uint32_t g_rgctx_ISet_1_t0CA8EAC13644940808DE830629EF8332222D9038;
extern const uint32_t g_rgctx_ISet_1_Add_mC442D3C176EA70400358AA92F9DF1BD76F5A2325;
extern const uint32_t g_rgctx_HashSet_1_t553801684D7F80C1124C9825ECA1E56253F72383;
extern const uint32_t g_rgctx_HashSet_1__ctor_m4B7A706A45A238F78ED15F5E5676D8AB86301AD1;
extern const uint32_t g_rgctx_ICollection_1_t4E4669F8629D7E121A38D781B862E37819031A09;
extern const uint32_t g_rgctx_ICollection_1_get_IsReadOnly_m904263E03D87CFCE3BEE09E405057C41D5D58140;
extern const uint32_t g_rgctx_IEnumerable_1_t1EB043C454F6458257A6260D471D8770A2C67E19;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_mF25EA5D72597CAE71429C0D7399A7107C90E6A0A;
extern const uint32_t g_rgctx_IEnumerator_1_tE70C35A40147259011A53DE3BA68676EEA795C49;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mD43691C666BB43AA192D80A6F56551F065A87B44;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t0001F2B51A27C7312245A54C71D2CC54DC9443AA;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t2A9B40208869A2CF2259E3721F82601C339C5D7F;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t345B0BF1D166C180ED469E25C22C0E1919FD8AAF;
extern const uint32_t g_rgctx_JsonConverter_1_t31B79B65A4D4B915CEFD63DD72C94CD49B08A06D;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mF67C94892E1A7A7FECF9975D3E7A66C02341F7F2;
extern const uint32_t g_rgctx_HashSet_1_t553801684D7F80C1124C9825ECA1E56253F72383;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m5D2F6C0F21205F3BA756E3174421F8741F51CBCB;
extern const uint32_t g_rgctx_TCollection_tEA5D6858FFDE2979C524B69B0277B6F46FE0BDBD;
extern const uint32_t g_rgctx_List_1_Add_mD6EEDF620836D43C044575C71FB73EE8B12D52EA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m2091EDA64A8583970AFC4BAFB94F72988B5A8CCE;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t2C0CC9AF9618861C2089ECBC5792343AC395C461;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t9D5F150D73D2F650CC28B4E02F8C76EC2140BDFF;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t8431C63C8DE44DC219E72FC27963A3DA908BC1F0;
extern const uint32_t g_rgctx_JsonConverter_1_t48351B75D3ABA89E3F59601E0D233BB42AF8CB44;
extern const uint32_t g_rgctx_List_1_get_Item_mC7B09E0BF98073BD591E158DDA762DB529703969;
extern const uint32_t g_rgctx_JsonConverter_1_Write_mADAB62365BDA59782D9E5EC2D1C77B5E7217E9FD;
extern const uint32_t g_rgctx_List_1_get_Count_mA5FDBC18D986B4B106B92474670E7835E7288016;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m690F2F567877C95B2D4396CFB619FCCDBD143D1B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m75BE9417A7C3281A1485B28DDE9E351A8CC9C833;
extern const uint32_t g_rgctx_TCollection_t01FB70369EDB1D0061058934D5EA094CF68E2AE5;
extern const uint32_t g_rgctx_Queue_1_Enqueue_mCA394D6982EB636BCB3AEC1B43E852BFC51E0AC3;
extern const uint32_t g_rgctx_Queue_1_GetEnumerator_m0915E13800FA030E6DC52D3BF0F1DA7F37F11BC0;
extern const uint32_t g_rgctx_Enumerator_t72EB596420B3C773654A294982AD8983CD8153F1;
extern const uint32_t g_rgctx_IEnumerator_1_t9E19E5A70E4C17F140145F7029B9607F4C0CAFC5;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m14432218A2F68B7B1E07BE29FE791A9997F70A35;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t4BBE636E4788A87E986230A449F41DAA3FD1C4D9;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t2B4E9360A1826EA57EA7BB033AEDA0AD71685564;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t4EDE34E23A8AE02DD81330B6F9377A84833822A4;
extern const uint32_t g_rgctx_JsonConverter_1_tF3B9ACBABA852A530EDFC200AEAE3FB714F01614;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m20259CC85C8E9188DF26E046AF64F3A24150F0CF;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m7E5BFC994B917BE6DF3612602ADA4B99D339BBE5;
extern const uint32_t g_rgctx_TCollection_t1CB75BAF7CCE4041F3EBCD834EF808C3467B624A;
extern const uint32_t g_rgctx_Stack_1_Push_m987D0F0FCA73333B78C31E38206A977F0C132D1C;
extern const uint32_t g_rgctx_Stack_1_GetEnumerator_m495B75E96ABA4BA9A0C60B864DDC744DEC9E3FF4;
extern const uint32_t g_rgctx_Enumerator_tC33D5A6654EE1F68E17A5048B3FF814D630E2C66;
extern const uint32_t g_rgctx_IEnumerator_1_tFDD1959BA6417D9171180DF7FF263BC376B9FC11;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m9D3E75E4BED900EFF4448EA57AC322C12411C41E;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t2D65A7DCBE2E7BD5AF6B625F1D19E072B1C5408F;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t35D91A16DE764A2AC9DE49FC761AA00BEFD98345;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t10CE06903AF7104B67B4FA17A5D5967E40BD979B;
extern const uint32_t g_rgctx_JsonConverter_1_t43C3E493E40BBFE3C098858A5F53AD5A6657045E;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m8C73C2F3981AC67DFF3DBEC74FC9418A3FDC171E;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mAF0CF3DDD94B8942D2954937A0FF2A54D6EA91FE;
extern const uint32_t g_rgctx_KeyValuePairConverter_2_tAB6FF421CAD483B9D5D52B5A94CE5BF20EF86A46;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_tBFAA6C1D4091CCE9F37EAD55B2D210A3683B733D;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_tF42E0EB12CDD09C564031A55C796DF7FB9A61BB5;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_t25C757E576B1C6D49C9F438448AC574E722C702E;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t985117A0673ABA8A7DA882258F79E40D41AE3D8A;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t3D3D883C95F5A2104466165440FC0BF5583F817B;
extern const uint32_t g_rgctx_JsonConverter_1_tDFB9C4D90D81E5C43563FFF3B785D764899DE38A;
extern const uint32_t g_rgctx_KeyValuePairConverter_2_FoundKeyProperty_m93B0FAEA396AF3873C555C86C7C05CC4B3F52C7D;
extern const uint32_t g_rgctx_KeyValuePairConverter_2_FoundValueProperty_m9935E2887CD91C8D7F00A3B637E720CC485A6665;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5__ctor_m0EADB95218F03A2D162C5A265D4019F3A9F69C4E;
extern const uint32_t g_rgctx_KeyValuePair_2_t53BF1C1A7D2CA6F07C3544ABD95AB975FF53D47E;
extern const uint32_t g_rgctx_TKey_t0E3DB6825B9056162878173521330E442363ED92;
extern const uint32_t g_rgctx_TValue_t4C7DFD79A4614DC383F2E6717763C34379671CF6;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_m39D15319455D11AF79A2598C870E534B49583AB9;
extern const uint32_t g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisT_tE3DB9B193436D18CCF76ACD53B2399369CDE4704_m05D35C09BA7456D08DAFA4D2625EAE69E1403BA6;
extern const uint32_t g_rgctx_T_tE3DB9B193436D18CCF76ACD53B2399369CDE4704;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_mA091A1DAFDF04A9B0222F885BD8BA4C01F6DD8B2;
extern const uint32_t g_rgctx_JsonObjectConverter_1__ctor_m6C75DEDEA5997EE60F28004838E1D65A56B31DF2;
extern const uint32_t g_rgctx_JsonObjectConverter_1_tC94C57B1B688ADCDFA1C1B5DA7A4D3330CF7E422;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tFEDCDB287BFEFA74C101EECA625EDE5353B9BF20;
extern const uint32_t g_rgctx_JsonConverter_1_tA2EB2E8C5B23192EDFAEC49BA932D6977EFD9B83;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArguments_m70C66C62AB34FDC964300E33A8C2C48503313E38;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_CreateObject_m63CC5330AC34587492C9AB02DC228B1D09F186BB;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_mB6E356AF620F2E4E7DE696D11EE1DF871A5B64A6;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_BeginRead_m8A5F743A282DB8305A6702ADFD35AB2DE00148A0;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArgumentsWithContinuation_mD3F8EDAB77E3CC5F70694B91953526792E33CFC4;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_EndRead_m8325C072A2430FA70C52B5116EEF32AA151F35AF;
extern const uint32_t g_rgctx_T_tCAF26AD9473D66750DF554416D2646174D6AB000;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_TryLookupConstructorParameter_m4A8CE518F0BF3E8476C91EBBF9C81B20CE172432;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadAndCacheConstructorArgument_mE5AAE450D550FC7E580F7467028020CCA98EE7F8;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandleConstructorArgumentWithContinuation_m2814D5D6765FEA11B126232E2F1F2754DE2017DB;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandlePropertyWithContinuation_mFFED794C00097EAC5BA8BE847CB64FE34DADCBA2;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_mAF63CAB8E7854115EAD2B365311588C2D91D7186;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_InitializeConstructorArgumentCaches_mE613F908560549FCD9BE5ECA40336C60C263342A;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1__ctor_m734DB4D4B60AE4CB62AD9C39A97E02CAC7DB2065;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_tA89C8AA79B87A10BFB87A5E442D4B0715A4AAAFB;
extern const uint32_t g_rgctx_JsonObjectConverter_1_tA1C49987FE343A5887E1050144BADD309E314824;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t766D12956AA52D08F1723DACCED90FA30CD5A0EF;
extern const uint32_t g_rgctx_JsonConverter_1_t0CACEB799FC69879F7617CF4A768A7C02D04CEF6;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_1_t108C95BE36C1B31D9C758AA5C6A9476D7E4DF100;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_1_Invoke_m1F701AADF2917C575E4474D0F087B5CDAD85B4A7;
extern const uint32_t g_rgctx_T_tD5F59385560BFF5D0026BD30587839A9B70362DC;
extern const uint32_t g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_tD5F59385560BFF5D0026BD30587839A9B70362DC_m392246F0AC45E703A8D89086DD6ABFC518AB43E5;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m6458694C5B0FD0F7A3CAECCDE1B2F92F588BE6ED;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_tB7C2193A3CD406D539C3BEC40F6F55D7A21A5A2E;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_tF2A1C4C800E94E0DE1F205CF617479231745126C;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t97E67A6E3187537B29BEB2E12316990FCD04DDC5;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t2CC4A4A0C1417B591196EA14A9C44FB847E01782;
extern const uint32_t g_rgctx_JsonConverter_1_t69895F58587315A5F5F6546A3644EC27DDCB0C1A;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_5_t3C7A519C5928E3126992DA2EE536B004339C3ED8;
extern const uint32_t g_rgctx_Arguments_4_tB34E40531B34828867F984694B97EC07190A0FDC;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_5_Invoke_m086D4594B82150AAE600B1FF89408CE5DDF8EB9F;
extern const uint32_t g_rgctx_T_t8D1755B2452C00237BAE69C839B1588F91EF385E;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg0_tD6DB7FBE4067411684585DA8F39E1CA70F512F71_m74236DCD04B35B32B0651FE273A6549E41A9BA49;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg1_t78E53566751E885F186ED56D612C8466A526EE77_mA711CE6F3FD250DEC74716AFC57C8F226D1CD459;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg2_t50A8D5BA525C364D13FFB650423C526600EDD6F0_m184266CE5496A1BE91621416AA637AA3393D0E60;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg3_t1725E0054AB3AEBD5D87C102B5F37228DFE150F8_mDF1547BF7DD64546914B670FEE44B536A5E37E6A;
extern const uint32_t g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_t8D1755B2452C00237BAE69C839B1588F91EF385E_TisTArg0_tD6DB7FBE4067411684585DA8F39E1CA70F512F71_TisTArg1_t78E53566751E885F186ED56D612C8466A526EE77_TisTArg2_t50A8D5BA525C364D13FFB650423C526600EDD6F0_TisTArg3_t1725E0054AB3AEBD5D87C102B5F37228DFE150F8_m143130ACA95786DC138B9B7BD312AA8EB459AF17;
extern const uint32_t g_rgctx_Arguments_4__ctor_mFFA4BBB4AA91E6F0558225DE7A254BF6A9B78234;
extern const uint32_t g_rgctx_JsonParameterInfo_1_tB26B5905CE92FE31357D8B448302BACAEFC228B1;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m44AB956B00365B56A3AE5F3E0E88686737AB5015;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t737B69663A39EBB1F9F6FD58BBF5C31E6106EAD2;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_mBE766F2C4F13492F9CBCAB730071BDFEF4E22DA1;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t3BB2870DFD41038EFFD2C9A97E5C3D3205B32861;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_mF88DCFEB73435FF5B272877354B513409BD48742;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t42482EA44D5784CF3680731A102D3A427A709786;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m8795C48964F9D09D82AEF296590BDBDF8DBD8CD6;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m08A9175D9EF9FBDD31A6FEE4FCB12ECE1B31CCB7;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_t63514987AAC77AB7E082A468911E50FAD9FF7892;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_t63E7C2A424B19AA1E531BA625AB91D246A7A983A;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t801CE0E052115C6745D232120748E4209CAEAD03;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t13E62B7506ECBA09C249C8C64460861FD50CE860;
extern const uint32_t g_rgctx_JsonConverter_1_t2E9980DDD748A47FC4A6C714DDCD6E6766E9C0E0;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t25121D9666B5E34FE3A04CE00C96C728E345CD66;
extern const uint32_t g_rgctx_JsonConverter_1_t662FE67BAA43E72A3F22D83997C91AD8A159B418;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_m61912E9756BF4A4E7B06299013B52318FED47BB7;
extern const uint32_t g_rgctx_TArg_t0A544929C5398A262BFEB210A4A5254B968CDF4C;
extern const uint32_t g_rgctx_EnumConverter_1__ctor_m9D8E7333B0FFDE771975E9B69CA4476D1BE55D66;
extern const uint32_t g_rgctx_JsonConverter_1__ctor_mCABBDD8760EA494A0B9675E5903C0F6AD8247995;
extern const uint32_t g_rgctx_JsonConverter_1_tF1798A895E9C1FEB6C1D79C4E4BFDD022B5A3288;
extern const uint32_t g_rgctx_T_tA4ABD6F400372FD8853726F8BE4FB948426D8396;
extern const uint32_t g_rgctx_EnumConverter_1_ConvertToUInt64_mD675589ACB2E7E4F8BF2BAD95EAC5C9B51A87268;
extern const uint32_t g_rgctx_EnumConverter_1_t9DE369661F9CA350AFE174A57E3C6BB9CF653CCA;
extern const uint32_t g_rgctx_EnumConverter_1_FormatEnumValue_m1705C01AC2BA7F208EA331C76CC37FE8EE755E26;
extern const uint32_t g_rgctx_JsonConverter_1_ReadWithQuotes_m067F5A988F7D159AF9284A38975FEDAA7F4584AB;
extern const uint32_t g_rgctx_Unsafe_As_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m0C5799E0E91DAE6FE0C1AED297C0F0793B3C2C55;
extern const uint32_t g_rgctx_Unsafe_As_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m9B72364F585200A5BFFAF3F783C2BDEE8B9E59D9;
extern const uint32_t g_rgctx_Unsafe_As_TisUInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m7FB8C39B74963EC6189A0111D471A59E4BB4E26D;
extern const uint32_t g_rgctx_Unsafe_As_TisInt64_t378EE0D608BD3107E77238E85F30D2BBD46981F3_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m65C5E13FE4A4378D5B21E1588EA2103D4A6816A0;
extern const uint32_t g_rgctx_Unsafe_As_TisSByte_t928712DD662DC29BA4FAAE8CE2230AFB23447F0B_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_mA2AD9ACB24DCB63FB73517A110C6F002738821CD;
extern const uint32_t g_rgctx_Unsafe_As_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m565A0ADDA6C356E15ABFE3A98F7A86CD33B29E6F;
extern const uint32_t g_rgctx_Unsafe_As_TisInt16_tD0F031114106263BB459DA1F099FF9F42691295A_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m96374D431F9AD5F35AA529E4C0EAF5AFC73D65F8;
extern const uint32_t g_rgctx_Unsafe_As_TisUInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m552BA6A9E6C33319FCB2AB6844F1CC9C84EA38E9;
extern const uint32_t g_rgctx_EnumConverter_1_IsValidIdentifier_m64D04434DE7076D761AED545F928509413EECFC6;
extern const uint32_t g_rgctx_EnumConverter_1_FormatEnumValueToString_mE31C75CB06ADFC83926F21893BD19D885FB7A1BC;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m20DE386E21AAF1A630DD503E38AEEEE581FD9868;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mF529E2E93A1EB253B9CC940119C189DB3ABC916F;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisUInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281_m42A74569CB5E3CCA11D99266FFDD43AC4B20ACA3;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisInt64_t378EE0D608BD3107E77238E85F30D2BBD46981F3_mF28C4448DD235A5F111350FBBF9C019347D8A8F9;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisInt16_tD0F031114106263BB459DA1F099FF9F42691295A_m4F968A84963088BDAAA2406418F486ECAA24CC6D;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisUInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD_mBB1473335C618D594439A59A4A908BAF93772EF1;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mE33E630EA8EEC1E14B46016E344879797B004658;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisSByte_t928712DD662DC29BA4FAAE8CE2230AFB23447F0B_m6BEAE47EBEED2AD7733E02B342F1DD8D5204A4A5;
extern const uint32_t g_rgctx_Enum_TryParse_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m056A103819A8412B3C74286C07CC31067384C54F;
extern const uint32_t g_rgctx_Enum_TryParse_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m9804F0C7A989474A4BA6EDBFD460503C45B8C8E1;
extern const uint32_t g_rgctx_T_tA4ABD6F400372FD8853726F8BE4FB948426D8396;
extern const uint32_t g_rgctx_JsonConverter_1__ctor_m2F04B1119C82D4F6480ED0C1003910C6C0C3EDA2;
extern const uint32_t g_rgctx_JsonConverter_1_tD50173371C0829A6B3615D843F05B37002E0C988;
extern const uint32_t g_rgctx_T_t04284A903F26E2E8CD64A881384B433B4CB5AD27;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m3A23B71F665A3EAF4E26271D5D65D8D7DADF19D9;
extern const uint32_t g_rgctx_Nullable_1_t18720AA725575E4365449CD4C6F1B99EC58BB6A7;
extern const uint32_t g_rgctx_Nullable_1__ctor_m6A9C1866E304941F3CFF7F6B0F5241F002056784;
extern const uint32_t g_rgctx_Nullable_1_get_HasValue_m628B0629353E10CCB21AEB68481BAFD87326A06E;
extern const uint32_t g_rgctx_Nullable_1_get_Value_mCE22557999CABC44D462AFC892D8A36B09517EA4;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m238BC5032E5BF4FF9716961C4ADF22349CA662BF;
extern const uint32_t g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_m675A8B48A7EB3FC877B85A787FE56785A743B385;
extern const uint32_t g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_m8CCABE900C85BC543DE7127DCC899353FDA553D8;
static const Il2CppRGCTXDefinition s_rgctxValues[588] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryExtensions_AsMemory_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_mF58CC8E18C1B6E3B4198C424E9F16182BF8D3A3F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_op_Implicit_mF7AE3674C8BF81601B420C1B73F8DB8F9B542B47 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_tE98F0E782285E35CDE5649904841ED6C37803772 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryExtensions_AsSpan_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_m1F4235DFFEBAFBCC540FBABBA2C780CDC7A712FA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_Clear_mEEB1239DB01422DEF7BCCC8FA6CC95E410D472C3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayBufferWriter_1_ThrowInvalidOperationException_AdvancedTooFar_m84BE8ECA8C4547F5AE1AB790C829E020CB6DC0DC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayBufferWriter_1_t63AD3FBC7977686CF945E7E16A5EE0158ACBF779 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayBufferWriter_1_CheckAndResizeBuffer_m91819934776FA62748F3F4ED94438E0413FABA75 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryExtensions_AsMemory_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_m65E916AE4107C98788CA77D0EAAE15CFCCBC4D0F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayBufferWriter_1_get_FreeCapacity_m61F197FB36F1C4FABAAB50E521CD2853E461F85C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArrayBufferWriter_1_ThrowOutOfMemoryException_m39033891A2F7CD956180902677B535AA9B8C9E47 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Array_Resize_TisT_t5A87CAB1E95C12272975EAC666AF6C4250B6A2BA_m0EDF4D90C67F2F548C4C597BDB8D05D49DE83F4A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_ContainsKey_mE274E16A6B67732ECE0E4D97831A74005BFA98E9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_set_Item_m2B55E3D5A392F5DF6D7FF8FB5C43F4D5748474B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m879FB7766BF60786DB02A41BDC136525F5A50655 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t85BBCD9AC2CAB891A15CCBF8FBF1D2989744A629 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_set_TypedDefaultValue_mE9BFF81BE2709A4BD71912F61BF4CE0F9E939DA1 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TAttribute_tCA059A22FD68C93032902AC9C7F7B48AAF4EBFCD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TAttribute_tCA059A22FD68C93032902AC9C7F7B48AAF4EBFCD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreatePropertyGetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m4EE52A435C08A02206B75F889BEC2DE1A5177D84 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_set_Get_mBAEEC5C94A50D85332AED9E355E5470C5E7D2B69 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreatePropertySetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m5B00A771EF7CF5267923ABD929653AE39B778494 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_set_Set_m9B2AC6480A8A3279BF2618E24694422BD5F05466 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateFieldGetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m09958185200D96E11C595FB8BE699613BE95B3B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateFieldSetter_TisT_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142_m8DCD5041A8AF8F861365562F6ACE65A57176258D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_get_Converter_m4929853A8C0425DD53C3DC3FB32B1C34E9C0377B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_CanBeNull_mAA4C1B01AEB40C6DD65E0DDDA529FE1287C8ACF5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tF31D95E4502793D2D2188ACDC7EE28AB71A867CB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_set_Converter_mABF6E9F1B8DA6C8C230756970F157D4AC73670B7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_get_Get_m7BBE6F4FF02D5E0AB34C0B12ABDA6DB19D4AE433 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_Invoke_m7EDACF1A1EFDF2BD9174297975A386CBD4A8C919 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t041B5EC5BA7E1F2073C755E4A9F8172AC8D5E142 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EqualityComparer_1_get_Default_m738C0A86248478C728C71F0EC7EA08AA342B641E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EqualityComparer_1_tDDCB798373BA559CD7CDD0AA23B84CFD0DB19B8E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EqualityComparer_1_Equals_m4490004BE141BE8A4E92DEC9A1DCE019F8CBC5C9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_HandleNullOnWrite_mEF49ED8C929026415B0EE9D1856DE085D7226BD1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Write_m6A68DBE5A1FE261F20BC897887628EC5FDC49C56 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m3CA741ACB6ACDB63AA94B126D534C7430AD3319E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWriteDataExtensionProperty_mF9CDC845406843F8CD7B5FA95323F902147268D0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_HandleNullOnRead_m7124BA4F2A582C4C2E8A7B0BE4EC20F07AEC1CEE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_get_Set_m19AFCBC21FDF39E98CAC29B75A53697F203146E4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2_Invoke_m5B50AE22B20AD4F0C16E62F9AEB05074D70FB241 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Read_m248BC0E4396EE5ADEE2A4973C27A9F982D16471D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryRead_m86CA085A0B5E65FCBAA39712CF1BB285D90FB820 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_tDC15B6BFE17B9C5DDFBFC356AEA00C29C1A174B1_m1B4B8FBA6F929B3C5A56733C58E214A70EA113F1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_t3623392C33BDDE308A39343DB5498879CE44E126_m76477345E3B88C7014A38DC2116790562BB4E115 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t10AF3DF63E49683AD9C61F470DE2F134F5A15EF2 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t10AF3DF63E49683AD9C61F470DE2F134F5A15EF2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ReadCore_TisTValue_t6944BF8A7D13FF23FA33E45D81BEEF8ACD7C0ED4_m4D1D5BFB592F6805777DF2126577F2BAC7134FE7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t90046F461E065DBA24C3A93193901AF8884A0C4B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_ReadCore_mD8322894748538F15B7FE80F9E79885E2EF20B77 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_t587DA454B31F55FA16FA8EA2D14DC8CBEDE10F85 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_tF3BB7476CB349D5F6909FB55445EDD70370F2501 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_Deserialize_TisTValue_tF3BB7476CB349D5F6909FB55445EDD70370F2501_m69992403B2E25990AD1D9E6FAE84665D14F48E53 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ReadCore_TisTValue_t00A5D146DC0733FC8BCF93527E57E3C240748AB9_m0F393FAB004803DEE96E0A46D33BFBEBC4DD5F43 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_tB077FE17C477B1B00B14ABB906A2A7CB9BF64304 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_WriteCore_TisTValue_tB077FE17C477B1B00B14ABB906A2A7CB9BF64304_m66D652EA6BC6EC22BCBB315E5E8202898A0E6DE0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tB84D187EA99E9BB9324CE76F5DC194E8D5D14C9B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteCore_m2D5DA1787CE77ACE9D8307EF059CC56F9CBC75A8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_t9569617E2CC8C00794F520A4BC079A389246436F },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_t28D0AC3BBE9F376D3BDF08EE8867DAB2D6C5B526 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_Serialize_TisTValue_t28D0AC3BBE9F376D3BDF08EE8867DAB2D6C5B526_mE8CD4ADC7FC44926253CD3890A31F3BF869854BF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_WriteCore_TisTValue_t6C12AD18119E78F5C94C162CC0785EB99D03F7FB_m1B82973880DEDB7290980C474CE8EC883351F773 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TElement_t19A7C4BAE3C763848ABFAF37D8EF6132AD249003 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1__ctor_m2D78BF76B39A141744AA42B2E9850DCF2D0E4851 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t227DCAD70E724D34E4D78C809ACA730381A15AB4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t0F00D8C9CCE5899F19F3688EDC9801CB37523144 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1__ctor_m4E6BFF18F75C55D81A07CB2B860F3E796891E374 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t1EB57A0B4DE9EC17AD3FD40E2A73944669E59533 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t616144D5B83FA8B0FE2FDD19297F6536FBA2125F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1__ctor_mBD33548750C13A30456A9B45FFFF8968EF726616 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tCD4B0D239D94E33E4AEFE7643376845EF255BC16 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tA21DB6CB3CDB47B60F39D55755343BA8C3FAB1BA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_ReadCore_mD8A4A54CFE6083EFDD222B78DFDDE1479EC356C4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tDE0D4E880817A65183EA86172AD15260036118CF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryRead_mD9E2EDFED3D63B29ADC0E7DDE822AD9BEEF8E217 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteCore_m4807D2CA4DC860547AF3C7888A607DE3D1E9C6D5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m9040DB9E7248BE16B6539EF0DB369CD200B77F52 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_HandleNull_m51C3617232E62030AEEB2AFCF000698B4CD7BB7F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_set_HandleNullOnRead_mC9EAE8F28AE1A87D0F85248C77BC3D5885E70E67 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_set_HandleNullOnWrite_m80E739AF0D389D325C933E2CDDCC8031FC2F10A2 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tDE0D4E880817A65183EA86172AD15260036118CF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1_t05F76B96D3217F9BE38AB0E76B27FC9746C5EFF4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonPropertyInfo_1__ctor_m5DB272A29AF1F1B3EEF1215EE8885451AC4DA367 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_t8CE0B00E01196C05F9BCB34F2575828227D1BC8D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1__ctor_mAB891AFDEAA64324B6911941B96BC216F9D479FD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_CanBeNull_m8EE216DDB446CFD004BC66DD873604B5DE7BDC3E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Write_mC1D44C1B0A1978507C65B2AF88843FB06D709F0F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Read_mD450EA2383FC87E223AE4BE91EE1004039D0B0A1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_HandleNullOnRead_mAD2E7B1F83A93434A083BBA32D92C761DA8E2327 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_mF241DE8685A34203E077250BD57F1B94BF0DEC46 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_VerifyRead_mEA950F19D224BE80F1AD29949AA39FBDC219F295 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_OnTryRead_mFB0BE200C26C2ECA459BB176FB974CC216C8C19F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_get_HandleNullOnWrite_m20F8D09FA55E3653E2015D7DDFFBA4AE536ACA49 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_VerifyWrite_m118A84AC7356FEB3D6865087BD7B1FD0BA2E1569 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_m136E8C67C68AF337D22BDD0C53266B99A180E576 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_OnTryWrite_m1FBB663B55D4E212ABD2D6DF7E7CA244A5799A07 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_tFD81D34ED71E5EE89C127D7C9D2EC85A8F165511 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_OnWriteResume_mE3DDDBA151BD54BA11B454CBFD44E06A7AD83F01 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteWithQuotes_mC6E3A536F035FF027DE5BE35BDF74FB750529CDE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryRead_m90104343AE07395A78FD6D7A3E67CBD540011BC7 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t5EC891C0C5C31AF339487D8E674631366A53C71C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m1B581CB4072362B3B21400E91C4BBC650DB7AD11 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1__ctor_mF6E1FEFAEF7A27AE7672D0D8FDA4D90A82196821 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t139AAC2EAA2446CD0F4DB92F960D7C497E48EFC9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1_t0926D22A810011F4225A43D22418EC4E0BC4601C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1__ctor_mDA399E33ACFAB3725E755E876899FFD3264F30E0 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tE7401DEABA1E09B0E88CB7BAE804F5207856A5ED },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1_U3CCreateParameterizedConstructorU3Eb__0_m44799D8C1B09050EFE6C34663749A7015BC8C9FD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_1_t13127AFBDBFD424F3FEBC6F3B1F77D65DB5CA941 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_1__ctor_mC0179B26C4BF315393B598A677F0050E6CCF7B69 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass2_0_5_t9BE909095532D90B31BAD8FD387A4F0B3B26D858 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass2_0_5__ctor_m8E6DF80969A976B71BA074DB4D5CEA77D586BAD0 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tE1CA391AC376C971F1C63C5690EA51664661A506 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass2_0_5_U3CCreateParameterizedConstructorU3Eb__0_m0C3054B3020662F0C19B6CE487EEAC8E923F22CD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_5_t57D5C8B6CD33EB1730F808F51500715793F621B0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_5__ctor_m0D11851117BA4E8D07AF0EE3630FEA3CB53B6F63 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass3_0_1_t4831C1BC7BFC0ED3AE0FF4781F3B985E1A2F2949 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass3_0_1__ctor_m82525090A3E3243058959B1F0327965989B36165 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t6C8736963D6371BF8E56DABF17A465D02FE42480 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass3_0_1_U3CCreateAddMethodDelegateU3Eb__0_m7BA41440E0F33B6E0B403FF87C2640B1DEB1CE6B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2_tB59C6C8DD475DDD363125DEF571B9AE537237593 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2__ctor_m760F38FFFD0583FCF3AEB9D58CBC4109949DC229 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tD8F4E280835AE5F514654FBE445988E3450F1201 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TElement_t3325851B0C656D4DCF70FDAE8FEB2706E7394445 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_tBF80C5E3CF469C7AF36E01928E5DAE257B37C779 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_tBF80C5E3CF469C7AF36E01928E5DAE257B37C779 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tC046CA0D12910C1B2098C9F6ADA8C376D84602AE },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TKey_tC4874DB2B0CA1D46DC0B766A2C71987A0F733848 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_t49C969BE5718E0CC5E4BB8E4670C07B8AB1B52C8 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_tEA5B441B140A1B38607590B76319E9BCCF1CBB93 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_tEA5B441B140A1B38607590B76319E9BCCF1CBB93 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass6_0_1_t91931F5A16D3711F067E03817CBAD62C671FB471 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass6_0_1__ctor_mD7A152A0655E76C8783AC6572C25B0331FFAC2DC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass6_0_1_U3CCreatePropertyGetterU3Eb__0_mD329A8C22E772307DB1B733567AFDD240B40387C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_tB234AE6E3C9B31E6E69B357468E80ECF66BEA77A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2__ctor_mEF886281B4C4EAC36DB4B38B3C09D6A69DDE0943 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass7_0_1_tB3D03CFF9F7B9B8E15FA6DC774418DD88FDE21F6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass7_0_1__ctor_mEC8572EFD97ED89FC5D081546CE08A1E7CA5EFED },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass7_0_1_U3CCreatePropertySetterU3Eb__0_m0A24C371E5C21288C8576BFD5A6006429F09EFE3 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2_tB94215CCF8D800A8BAEE899B237F548D03B93070 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2__ctor_m496A9C29D53F6FBD7D9C5DE97769E684842BCF27 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass8_0_1_t99E394A7E11801AFA9BE7820B49A47BA1E836E42 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass8_0_1__ctor_m815543799CA407D85483AC0F10364BECE4BCFBA6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass8_0_1_U3CCreateFieldGetterU3Eb__0_m9CC3419081ADFC0B13C2F8338FD144C4E7FA5503 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_t7FB0D2D2B962B79CCBCE7392C495066EA4459E1E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2__ctor_m768E5C61DEAA6B9CC347042E074C987547D499F1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass9_0_1_tE4A0EBE0323ABF892388F0E6DA56816CF44E85D0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass9_0_1__ctor_m123CF49F18B96DBCC17D205F6ACC5851F923D497 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass9_0_1_U3CCreateFieldSetterU3Eb__0_mD58C961FA5D59912D9DBC44B2844CA690F4EF6A7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2_t7BA37B800E03DBEF4D1E448699D1525E2EB08506 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2__ctor_m93E858A6D690564CEA11FF3B0E19C5B8E2569D4E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tC6B0FE90DAFCB0F6495B63D3B49C20084840959E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TArg0_t774421C662D6F6B418989214CBF773A578FBF39A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TArg1_t580AFB4584610C707826CC51AF29830419D1785D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TArg2_t97D0732BEB4C0DD134070B70AE7FEDDB80E4F1DF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TArg3_tB45251A0C832E0228BC7FF67033D6E8886553970 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t92265CC3BE95C6C18ABB347CC0D2CAE1F4A4F166 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tCD0321429062BDEE3DF447F50E755CE5A1DADEA4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TProperty_t0B19ABC77D06DB5323908182C55A483F75765428 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TProperty_t5ED322B05D0FA1CFBC0E354E8668EF98E3C1DEF9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TProperty_t7D80DF5A3BAECC9AA3EC71C9E4EBE0E19E8487D6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TProperty_tEAA1F016214D1F3D50F8B33AAC9BA55162D651AA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_tEE0D37B36B6698A7EB0853BF66AC874E7649D1A1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_m43B63B384FBAACFF040D681431183AFEF91BFF0A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_mFCEDB34DBA88FEC30E5DEE641459C64B280EB9CF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_ToArray_m3B508156490A2C1458717888B0E88003878EB35C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tFE1BCDF71192BC5A1A0807B54E9483D1BE0F5C5D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TElementU5BU5D_t207143EA08396800E047242F8E98333806269387 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mE12ADC597BB87805FF2341257CE3F795909BFD87 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_tDC405E27420BCE1CC6DD34821CCDB6F944D0EA27 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tEADD53AD0D60A8DC30C283964E5004486C54EAB0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t57A0B2836BBB19A929F582819E2AB5E38A167BA9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t5F92BBAD382076D981616878DF94BB8D24C92621 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Write_m86A0B9B8F2521B4BE2AE8EE4DC70799F9E92BA63 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m94ACBDC1D358B72F3C4B448A34F2A66F1ED75BD6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m56C4F5BACA41757A54966A1D934911A27B861DD2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tF323F07C3BCB39AF9217D9DB0F9A46BC89ABCA9B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ConcurrentQueue_1_Enqueue_mB82D58E81FA848A5F8F9A1F3BA6FDC4DF197E057 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ConcurrentQueue_1_GetEnumerator_m3DF06AEE521BA4782B9359D1C0B31DD5F9801772 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tE87E43DE121279E093714319E6A41BB77B9DC62F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mF724AFBC473E14ACE7D1864458F233C619F7EA50 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_tCFB9621DB5C3660F1E785FBC4947F1476FD38C3A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t2E201F7E652DCA4434A6307B9145028401AE95A7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tFF595E49C72F236956DCDC7794F9D8ACFA0E147E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t31F16D2065F1F0299C2DF14AAE92627F4E42BADB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m52921B0803C5D8088E662DAE2965BD490CA8C5C6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m806E1FE66E78F1F4CFD1706C2D2A564AB0E47DA3 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t717009D3E93E27EB4863733BE8555F6AA657E870 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ConcurrentStack_1_Push_mF228FE07B1EB9AE4F1EDC04EF211E37C1BC2B3A0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ConcurrentStack_1_GetEnumerator_mFAE3B25792DB6EA0446B480BDD9FEA5A172EAE2D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_t74BE349DD99E7220EE0F3D19165F01338F7C3FAA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mE40F2001300C4E2583375C6A085480490ACAA935 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_tDCE4B76F5788120651FA7C842BF4880D2247C532 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tF8F70D7EEAB90C01AE9D7CD25925C4A429993428 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tC003E6826A1D182745ADD254A5BE08DD24E87D5C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tC32A26C76DA3B4AD05E641244B1D814BF6AA1482 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m86BD33A114A6707E083AD4574FEFFCEA1E052784 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mD11179EA58C131FB5FC83D7C4527467AE5763306 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_t88661170937490B661E6F80F3C72D3CA71B2D87B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_t8823389F2CA753C3FAD912164DD19B5562BE77EC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t22DCBE92ADDE649C23897AEE9A90AD8F9F68BBBC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t95F83D7E3A95D53158D1B62E2842E3679ADD98F7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t2C8C2A491C318F53EF445B7A6C007CA0150F227D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tD0E51510C147326BD012ADCCE2E04375E7A23C5B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_CreateCollection_m4B77A09DCE1E039C49642FA87001A7F613F7536F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m22568C80298B1A02D1428B623FE94C50D1544965 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_U3COnTryReadU3Eg__ReadDictionaryKeyU7C12_0_m614A23A7EE43EB85184E9B16D008F1EAFB0E307B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Read_m54E33C9C55B0E8B015E1B93A169E332991A35005 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_Add_m2FF0254F275181249CBC705B74E0D2AA02218B10 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryRead_mF1D91D459C2CFA9AB0AD3062085E88E881627E7E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisTCollection_t0133571FF836DF7A5DED584AE98800C044D3C9CD_mFB4B66D159FCB4A3EFF469915ADAFDF4D7EDDD46 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t0133571FF836DF7A5DED584AE98800C044D3C9CD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TKey_t402E0D26D2A5BEEC462BB7D6D4B433885EC97C08 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_t7329166BA6BA8950820E2A718B3F825409635A62 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_ConvertCollection_m79237CE5E949A203751890932E566887311EAC2D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_OnWriteResume_m182DDD506ACFFD323CB649A37FAB640CB8CB40EE },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TKey_t402E0D26D2A5BEEC462BB7D6D4B433885EC97C08 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1__ctor_m451CEB87C6CCDA30DFA4F89658131C849E26EB32 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m10502C5810C7FBB8664B424B37EF182483479C2B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_ReadWithQuotes_mA6FE0B45163A5769B11CED9F4F0D910BBD437955 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tC6C1C819230EB84708DD60208E5B9A12A51C3B66 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_set_Item_m07A678C05FAA1694E3AF98992201965A6B93C392 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_GetEnumerator_m26CE10E6DAD8B8A50187D77EE94ED8156A8ED8F1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_mD43D02F55D0AC4E8B773C2C3A35278FC47BDE046 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t9D797C755572F82D131A2EE971E1384AD98D15D3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_mDE73CA9089DC074D07B97214B70D0D00198CA87B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_tB9A97CBD64046C7A78A3689D566C8E16487E264D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_tECEBD70B22BA7FE6EB2E065A6C10405E338767C6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t0CDFAC0BD9941090B876EC5ABE59A944F173200B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tE84915513E58C5A237588B254714F32AEFFE1503 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m03A8616193878DC7EE33D79AC1A556949073E0AA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_mBF9707F2B029C2261A58FD04B115561589DF0690 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Key_m1AAC6DF5AAECECEE181B5F2E460AE11FBCB13010 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteWithQuotes_m0FCB6AFBCB2D4509207CE6D9EE3D8C27F478ED1F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Value_mC9C292806F6710BEC5FFB49A33105EB0A1A120AD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Write_m4C7154527948D35D8DD51816A1C409BCCFCA84FE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m35654B06EEC06DE212109DCAABC9F95F516DDA54 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m0B13A3286E5543331A10B09F74B3C2E896CA791B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_t428D3E6D09DCA9ADAA1503431DA9DB2BC804E3EB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t8D6A38BC72A55594282908E861987521AAF35C95 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_mBDE43364840B1C3AD4B57736449FD2914B5833DE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tB07E8B1895BDD12214706F8A4DFAE90CACC03601 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_get_IsReadOnly_m21B3DAD2E48A74923439773E01AA33917F4916DB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t75285D593EE7E4426E1B215A835EDED19714F6C8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_m70042CDDD931E6776561C83EB565AD58C2FE3BFA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tADDA8DEF1353F794B605DE1CD10CC86F7E5B5BE7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m0398A49E7B8EAF6499BEFBEFB78C97C92AB3B07B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_tDF57831330A32E400946AAE9D2BEE7C0A525F1D6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tA9308F285C872B1B3A01C892C282C7E782ED1BF6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tECEEC9D8300912840D776FF42B9D5B5E92E62B3D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t608C7677D56CAC04008D8F25BEDEF1A6FDB2D603 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_mDACF7BE8456673E6FA7560623981E3EDB66DFB72 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t8D6A38BC72A55594282908E861987521AAF35C95 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m4DB551AC0C496D94EE76A8CCBF163489F7189500 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tD5A10283589FDFABD07D52CD2F4C61D771570FAA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mE473432CBB7F9154D027A6D69FA523265A536327 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_tC1833356173D5B322F9DD0ED1F19C5FE963AE06E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_t08E19C35DAE7831F8BEF4554184C1ACAC0C3AB7E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t245DDC32994097DA72ADF364820F41419AD40EFB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tA288F21A831BA62BCB614385357885775C07F25F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_mA045A8B2C39CEE01E6E2C00F401D7130241E00B1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IDictionaryConverter_1_GetObjectKeyConverter_m5ED410D268C69B21B95D08352CC19B8E0024B686 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IDictionaryConverter_1_tC44D8F6A71F1007FB947A89E5835C7F42114C044 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m2668B5B8915173C68B39ACF5F78BD9DFE8B724E3 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t58D8D5B3F5006ECD0B20BF1C18E00ADEBA7AC5AC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IDictionary_2_t188F7C8F3085ED128521213F4BB857B998A0BC44 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IDictionary_2_set_Item_mE812117273063CBF7E7A83EC002887A9A00C4F89 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_tD331E563AD4AAEA5E2C3EBE9B534D0871556B94A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2__ctor_mA61485AA880C8E244AE7F312775BD08FDC8E5C3E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_tE61A31B2CF365A79A7BE2B4F232FCB08980AE086 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_get_IsReadOnly_mB479893E8ECEEEAC21325E04A9271EF97694BF40 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t1B3983F68A5CCAD0299BE963780609DDBBF36B48 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_m79A7F32754B0AC0039B715A77AF1B0F5E41655E7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tEC38F7E6414D5CAAB54D0472639203485A6F4408 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m593E8AEE1BA1178AD63CFE3264DF47F1319F9829 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_t5D720BB5654DE077F4BDBCA9D34AA236F3224E3A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_t9D5467978E8E94887BAEE91FF72090ABC0011977 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t77DA22F25F30F51EF3C9F02B30C824FFA7E55A0D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t3CE7B05E2B84D052AAAD1E900E9210CD06EAAFDB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mD8EDB3F1AD8086D6F060A284891F111698E8AF79 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Key_m0D06E1C119BD8CA83AD360194CD052190D93576A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteWithQuotes_m4F4558979550DB83F02D44AC845810D8EF911270 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Value_mE8443A178C363CC740276564792B82CA35343ADB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m889B027E15F0E619EACA93359B0230B3F97E461B },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_tD331E563AD4AAEA5E2C3EBE9B534D0871556B94A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m135B8360C418B42A90A45BEBBF5E3C7A8F4AE7E8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tA97942A21A000010CF0D79DC5296A087B6527008 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mF9D774173B0F0EE2132D6BE76798F431D39D850C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t15CD8B5B1D51BFCE6B50749F1DF65B314A348DF9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t52DC1967AC13240DA967EE40822357E9D432A2E4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t2975753DE4BA6812EFF248908A289DA12F7D00E8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t8B5043328C05B8FED16900B1A0AE171E3309C7B1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m4817B378081B72135E134595DB7F1AD91EAE6BCB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t3E8AB39B2D1317F03C1A178E7A5B90573CA61FEE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_CreateCollection_mDB7961A98C4B4355F8F9D053054063E1F787CD75 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m6D64E56FB03FDCC431AF77E6715CC25CA034B83C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t2DF14B0BD54A9EBD74DAA94C0C81E9237C41B53F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tBD733BDEFBC61B16F3719CB2CCE54FBF8E726177 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t255976D38EB099C66B6375281E795AC4E503D963 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t962FF5446841F50DF4FEE623067EF6B89A304975 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Read_m7549B69ABF7F1BD38C340F459141A1EF7049B163 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_Add_m8266E9F7451A24CD0A346B892C054F066A907610 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TElement_t4957D75B61CAB682EE72C9A330EB9D6C32442826 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryRead_mB0110A2EEB36B55270293188006B7D9B3E4DA296 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ResolveMetadataForJsonArray_TisTCollection_tFDD8CFFC1593E29720C3F2ECD6E6E0A034F82065_mACA82990A22BAE7E8D24C449A3EEC2F382047FF8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tFDD8CFFC1593E29720C3F2ECD6E6E0A034F82065 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_ConvertCollection_mC73323EFF3B91A5DB0AC67568B542159ED9F5A4A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_OnWriteResume_m604C17DD5D6469B18CBAF04B6F7B9FF4E4A95587 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2__ctor_m12EB18285B1CCA710D1C445D8DA0A08D58498768 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_tCBA9D7390B8DA29263AF7ADE7B1ABFB3D6240C09 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_mB1C1942FA33111F8777CD7CCBC70CCB21C71A3EB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_mC91B0572772DC47AE1C61AE661B5FF9034BB5047 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tDF47E9BABCC87D3192BF878AFAE79EB48271E86D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t76692457895ECB987E2BCC5DF9F79EF95AB94F03 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_mB905D80F0BD5FB20DD676442C0D3ADA531258404 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_t102D8A21E598723B6E153D56F055664AA65B4647 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mB42AC3B69AED93E876667D58F3D69FA8CBE7AD88 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t54309471C27A7D8BCEA7E3B51474BD0F2D73B0AD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tA8357BA3ED1DAFA829A6D16DA4658666964A9F6B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t8E5EAF54644F6BC1ABCE48813EF98E90D62C6F39 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tF1A2CF457909E13AAF51DDD9B89A820E30F215D6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m7D25C666EE7AC6F4C0D36EDC3708C782453E2337 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_tCBA9D7390B8DA29263AF7ADE7B1ABFB3D6240C09 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m4C59BF06CF733848470107F309D258A68CA23326 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2_t0E6761CAE887A35B965B54DA89ADD9E43B7E5600 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t799EDB522FF11B2AB74F132FEC2AAD6C2B4E8DEC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_2_Invoke_m94BCF09A1CE9BD358F84CEEB44343B1357604657 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateAddMethodDelegate_TisTCollection_t799EDB522FF11B2AB74F132FEC2AAD6C2B4E8DEC_m848BFDE14B3E458EA07B2BA46CBDD08A5F2C8069 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m9AED333EB6DB54958C9712E1B365CC87D4F2D16B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_tDBDAFC025080057E0751B43B9971B2C23970158F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tCEB03983BD7513141001CA432D4983D2A199995F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t727E751A0FFDCB4F0619DEAB35814082E30FA8BD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t7032A458CF62EE8BD66E8FD668DA8418EB0A70DA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m09936966D6E4ADAA651A2DF3A80EE4A474D1AAB6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tBCADD67D8AD0813DB41B07AD2D2521C19CCCC3FA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mA952F8162E3EC0BE62CE0053C7F79BD044939278 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t48C0A78772B197A2E07689F032E2976AD9156361 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t64C4E857017E4DF89CC9516B70D64F3AC09C8C5A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tAC17C4EDB1BD1A2C01DC7FF4AAA55732E855CD4C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t7F2EB94EA5540D6091CFCB350BD5B2B19CEA1854 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mCAFC9B9363278FEACB3F6203DF754D679722577C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t21A65B1B7FBA9BDA560D3FA0DC8626A4C05917E4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_tA4A0644D6B9FC776E906C1BD18D07A56F0A25172 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_Add_mA073D133AF88E0EBB9F7FF2E156E110FE99D60AE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t2F14D6CF23F177B548432AC302931830C0472C41 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_m0581F9111206B4BFEDC39ABC06313D6264C4055D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_get_IsReadOnly_m1C710BC5DECFD13AC6A65C0FC236A25E079F9C2F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t3E764FA42F7D57912EB5B4920B68B3E6188AE841 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_m0D3CBCC497779CCCE20999423FCD61E50848F679 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_t5B2A9204A9643387075C230ABEFFAB5D98FEBCF9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m355F647FF4C500FF94922C3942B711474870F03C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_tCDACEE0AF9902268A6ACB2A1AEE64644C33F3513 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_tB82ACB8E5569A4EC4FD6BC9A706729BE274326DC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t63059B59E2F82858426B185012A34A0353EC1795 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t7292C0FAE7407D910A2B28D4D1334177A1EABAEA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_mB980121C826178DEC85AB23EEC518B4AAAC7240C },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t2F14D6CF23F177B548432AC302931830C0472C41 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mB7A149D378F9F374F7D091EB6D56A3A292AC817C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_t20984B6F8B425283A733E0CABA3E6F3D17850403 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_set_Item_m5581272A65705D0783F53F14E55E551D002EBDE6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2__ctor_m9212C5F14F0AEA6DB6FB563B4A5787E151E01BDF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_t6AD58D7B12648FD244D32E52F892E440FF3EDC4A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateImmutableDictionaryCreateRangeDelegate_TisTCollection_tA0A2C1302BB5DF06A43C4BCD8736392C9E9BF0BC_TisTKey_t1CE0855F3B31DFD0DCD2CE962F54C331925997F7_TisTValue_tFD898E4A43B9076A83C3A57D74F6E181EFB852AF_mCF3E93E8985F24227221D7687A71FA58329ECEEE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_Invoke_mB452DFF9F88E63E2107DE3A729ABA52D6D14132C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tA0A2C1302BB5DF06A43C4BCD8736392C9E9BF0BC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_tD3C5C60F762756DDDF9BB9C8E4DADEA876A64D9A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_m0056E95B7DD46DD45167C6AAE25A57B7D008388F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tFEADBCF91E0D7F6BF8ED12C2093CA7C325014D97 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m30FBFE5A1D5DEDBB24D610EDBE08C0496548169F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_tBB84E087430FA3A9E69C601EBC4A81CA73EDB979 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_tE51516103776B70B7DE3BE6D72DD68329352C03D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t2923EA425521BA652C4B2FDC8BBE0462239BDE48 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t9CB3B8B7B436A5F7FC236B513CC0C1F3F451BB33 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m506064AD83BCF99B2068CF7AFEC1C6EDCD156A49 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Key_mB3DBDB39589F53098EAAB564963F0B0F439EE09C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteWithQuotes_m7F39872CDA4352027E1E49FA22770329B4900ED5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Value_m03C1226C4D804E7362C24F36D6A21A0D2630013C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m4097035F72D5687BBD08CCA72AB8F32566100DEC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m22BA267708F72D30023BB607E3BEF8EE420FD72A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t7BE3F369360374FC3BBA2E746E5A05AC11FB0A39 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_mF4E576DC2B8C9F549CAEB5B6DFF833C552A501B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_mB2C9A4601BFFD183F943B60325798C9336C32D88 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_t1B976705D65D2BB5879254AAC54AE0D743258B39 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateImmutableEnumerableCreateRangeDelegate_TisTCollection_tB497759E8C60630AA9D3A5C10EF0BFD171ADD19C_TisTElement_t6950EACF58DA18F55062E2799C599F69E0CF730D_mA64AC58BD0312D2844F67C2E0BBB785F5A27E255 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_Invoke_m89497E9AA57F69215DAF15528B5C523D2E2276FF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tB497759E8C60630AA9D3A5C10EF0BFD171ADD19C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t9C071D199112CEADB8EBD36A02F387A0D1F829C5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_mFCCE92AB68820FDF40294AD571B8DEFE35D1B2B5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tD0BA0E7C4B142E9478202DC3A66CFE4F74EA3ADD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mBF7C34D9E603FB35D6806F1DF4BAEF1606BA1367 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t027C114EB3D0EBB8214532E19D5F19972BB7A27F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t51D9120653B04B1E943BB0C67BEBE0D0012B5FCD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tFC5A7DCEF749AEB034378DDD07864B2FA83CB563 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t395634378914272768937D85CD760AEF64F5F48A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_mA6C58F60114967A9A80001D29580F84992985C1B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m458A39414D829052EC7C3EAA349B7A4A71E4D739 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_t31C6FF9CF8E5D1D370276501C199D42F140B0838 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_set_Item_mC8107DA4E6ACA72A4D1BDC1F1BD1EA8AE7D1DD8A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2__ctor_mD2398725DE34F6A534CB3C05C5FD17FB7C8ED37E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tC1454A0418C7984938DF8B02DD4152B0892F4645 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t85DBF7CC9CAC87782C559FD742C62783DC4530B3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_m96AEA0F814ED2C961ABE528DE1E5607A600F1D27 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t3F3173A3C90A4A5C981DDD159F7DCB3C0A972A27 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m56E585B514E00802D5944A92B9284072A10A6FBB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_t34AED40F6DEAC2A1E4C177B38C9C39A0C825B717 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonDictionaryConverter_1_tEAD348CF3354B8C4AA20F786D7F6367F599F4D5F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tC6102A647A394CE56E7254EE041F447A46AF45BA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t74A0DEDDF80B351E726803A4EA6B8FC7CE67C062 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m70E0D6BBA1BA42F9E5083452E31C636DB2705569 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tF102596CB84F48E7CF8BFA3DE7236E6843127709 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Key_m1E658792F6C5AEB814A5AE2EDDC42B9EC71AB627 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteWithQuotes_mF80A1E43064F679483843617562756DD03B0F92C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Value_m58C773B1DA09E73CE56DBA200901C609CAE009DC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m39A5574C38B9B5E6F051974C089B2C69F2292078 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_t31C6FF9CF8E5D1D370276501C199D42F140B0838 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m069016CD959116FA0D966B2E122145B87E3E095E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tD2C29EBDDFE77AD53DB4F72CCA84FE16A87DA2AB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ISet_1_t0CA8EAC13644940808DE830629EF8332222D9038 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ISet_1_Add_mC442D3C176EA70400358AA92F9DF1BD76F5A2325 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_t553801684D7F80C1124C9825ECA1E56253F72383 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1__ctor_m4B7A706A45A238F78ED15F5E5676D8AB86301AD1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_t4E4669F8629D7E121A38D781B862E37819031A09 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_get_IsReadOnly_m904263E03D87CFCE3BEE09E405057C41D5D58140 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t1EB043C454F6458257A6260D471D8770A2C67E19 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_GetEnumerator_mF25EA5D72597CAE71429C0D7399A7107C90E6A0A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tE70C35A40147259011A53DE3BA68676EEA795C49 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mD43691C666BB43AA192D80A6F56551F065A87B44 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t0001F2B51A27C7312245A54C71D2CC54DC9443AA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t2A9B40208869A2CF2259E3721F82601C339C5D7F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t345B0BF1D166C180ED469E25C22C0E1919FD8AAF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t31B79B65A4D4B915CEFD63DD72C94CD49B08A06D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_mF67C94892E1A7A7FECF9975D3E7A66C02341F7F2 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_t553801684D7F80C1124C9825ECA1E56253F72383 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m5D2F6C0F21205F3BA756E3174421F8741F51CBCB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tEA5D6858FFDE2979C524B69B0277B6F46FE0BDBD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_mD6EEDF620836D43C044575C71FB73EE8B12D52EA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m2091EDA64A8583970AFC4BAFB94F72988B5A8CCE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t2C0CC9AF9618861C2089ECBC5792343AC395C461 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t9D5F150D73D2F650CC28B4E02F8C76EC2140BDFF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t8431C63C8DE44DC219E72FC27963A3DA908BC1F0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t48351B75D3ABA89E3F59601E0D233BB42AF8CB44 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_mC7B09E0BF98073BD591E158DDA762DB529703969 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Write_mADAB62365BDA59782D9E5EC2D1C77B5E7217E9FD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Count_mA5FDBC18D986B4B106B92474670E7835E7288016 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m690F2F567877C95B2D4396CFB619FCCDBD143D1B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m75BE9417A7C3281A1485B28DDE9E351A8CC9C833 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t01FB70369EDB1D0061058934D5EA094CF68E2AE5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_Enqueue_mCA394D6982EB636BCB3AEC1B43E852BFC51E0AC3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_GetEnumerator_m0915E13800FA030E6DC52D3BF0F1DA7F37F11BC0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t72EB596420B3C773654A294982AD8983CD8153F1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_t9E19E5A70E4C17F140145F7029B9607F4C0CAFC5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m14432218A2F68B7B1E07BE29FE791A9997F70A35 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t4BBE636E4788A87E986230A449F41DAA3FD1C4D9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t2B4E9360A1826EA57EA7BB033AEDA0AD71685564 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t4EDE34E23A8AE02DD81330B6F9377A84833822A4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tF3B9ACBABA852A530EDFC200AEAE3FB714F01614 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m20259CC85C8E9188DF26E046AF64F3A24150F0CF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m7E5BFC994B917BE6DF3612602ADA4B99D339BBE5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_t1CB75BAF7CCE4041F3EBCD834EF808C3467B624A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Stack_1_Push_m987D0F0FCA73333B78C31E38206A977F0C132D1C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Stack_1_GetEnumerator_m495B75E96ABA4BA9A0C60B864DDC744DEC9E3FF4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_tC33D5A6654EE1F68E17A5048B3FF814D630E2C66 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_tFDD1959BA6417D9171180DF7FF263BC376B9FC11 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m9D3E75E4BED900EFF4448EA57AC322C12411C41E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2_t2D65A7DCBE2E7BD5AF6B625F1D19E072B1C5408F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonCollectionConverter_2_t35D91A16DE764A2AC9DE49FC761AA00BEFD98345 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t10CE06903AF7104B67B4FA17A5D5967E40BD979B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t43C3E493E40BBFE3C098858A5F53AD5A6657045E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryWrite_m8C73C2F3981AC67DFF3DBEC74FC9418A3FDC171E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mAF0CF3DDD94B8942D2954937A0FF2A54D6EA91FE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePairConverter_2_tAB6FF421CAD483B9D5D52B5A94CE5BF20EF86A46 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_tBFAA6C1D4091CCE9F37EAD55B2D210A3683B733D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_tF42E0EB12CDD09C564031A55C796DF7FB9A61BB5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_t25C757E576B1C6D49C9F438448AC574E722C702E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonObjectConverter_1_t985117A0673ABA8A7DA882258F79E40D41AE3D8A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t3D3D883C95F5A2104466165440FC0BF5583F817B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tDFB9C4D90D81E5C43563FFF3B785D764899DE38A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePairConverter_2_FoundKeyProperty_m93B0FAEA396AF3873C555C86C7C05CC4B3F52C7D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePairConverter_2_FoundValueProperty_m9935E2887CD91C8D7F00A3B637E720CC485A6665 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5__ctor_m0EADB95218F03A2D162C5A265D4019F3A9F69C4E },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_t53BF1C1A7D2CA6F07C3544ABD95AB975FF53D47E },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TKey_t0E3DB6825B9056162878173521330E442363ED92 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TValue_t4C7DFD79A4614DC383F2E6717763C34379671CF6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_m39D15319455D11AF79A2598C870E534B49583AB9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisT_tE3DB9B193436D18CCF76ACD53B2399369CDE4704_m05D35C09BA7456D08DAFA4D2625EAE69E1403BA6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tE3DB9B193436D18CCF76ACD53B2399369CDE4704 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_mA091A1DAFDF04A9B0222F885BD8BA4C01F6DD8B2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonObjectConverter_1__ctor_m6C75DEDEA5997EE60F28004838E1D65A56B31DF2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonObjectConverter_1_tC94C57B1B688ADCDFA1C1B5DA7A4D3330CF7E422 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_tFEDCDB287BFEFA74C101EECA625EDE5353B9BF20 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tA2EB2E8C5B23192EDFAEC49BA932D6977EFD9B83 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArguments_m70C66C62AB34FDC964300E33A8C2C48503313E38 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_CreateObject_m63CC5330AC34587492C9AB02DC228B1D09F186BB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_mB6E356AF620F2E4E7DE696D11EE1DF871A5B64A6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_BeginRead_m8A5F743A282DB8305A6702ADFD35AB2DE00148A0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArgumentsWithContinuation_mD3F8EDAB77E3CC5F70694B91953526792E33CFC4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_EndRead_m8325C072A2430FA70C52B5116EEF32AA151F35AF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tCAF26AD9473D66750DF554416D2646174D6AB000 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_TryLookupConstructorParameter_m4A8CE518F0BF3E8476C91EBBF9C81B20CE172432 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadAndCacheConstructorArgument_mE5AAE450D550FC7E580F7467028020CCA98EE7F8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandleConstructorArgumentWithContinuation_m2814D5D6765FEA11B126232E2F1F2754DE2017DB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandlePropertyWithContinuation_mFFED794C00097EAC5BA8BE847CB64FE34DADCBA2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_mAF63CAB8E7854115EAD2B365311588C2D91D7186 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_InitializeConstructorArgumentCaches_mE613F908560549FCD9BE5ECA40336C60C263342A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1__ctor_m734DB4D4B60AE4CB62AD9C39A97E02CAC7DB2065 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_tA89C8AA79B87A10BFB87A5E442D4B0715A4AAAFB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonObjectConverter_1_tA1C49987FE343A5887E1050144BADD309E314824 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t766D12956AA52D08F1723DACCED90FA30CD5A0EF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t0CACEB799FC69879F7617CF4A768A7C02D04CEF6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_1_t108C95BE36C1B31D9C758AA5C6A9476D7E4DF100 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_1_Invoke_m1F701AADF2917C575E4474D0F087B5CDAD85B4A7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tD5F59385560BFF5D0026BD30587839A9B70362DC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_tD5F59385560BFF5D0026BD30587839A9B70362DC_m392246F0AC45E703A8D89086DD6ABFC518AB43E5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m6458694C5B0FD0F7A3CAECCDE1B2F92F588BE6ED },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_tB7C2193A3CD406D539C3BEC40F6F55D7A21A5A2E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_tF2A1C4C800E94E0DE1F205CF617479231745126C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonObjectConverter_1_t97E67A6E3187537B29BEB2E12316990FCD04DDC5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t2CC4A4A0C1417B591196EA14A9C44FB847E01782 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t69895F58587315A5F5F6546A3644EC27DDCB0C1A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_5_t3C7A519C5928E3126992DA2EE536B004339C3ED8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Arguments_4_tB34E40531B34828867F984694B97EC07190A0FDC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ParameterizedConstructorDelegate_5_Invoke_m086D4594B82150AAE600B1FF89408CE5DDF8EB9F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t8D1755B2452C00237BAE69C839B1588F91EF385E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg0_tD6DB7FBE4067411684585DA8F39E1CA70F512F71_m74236DCD04B35B32B0651FE273A6549E41A9BA49 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg1_t78E53566751E885F186ED56D612C8466A526EE77_mA711CE6F3FD250DEC74716AFC57C8F226D1CD459 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg2_t50A8D5BA525C364D13FFB650423C526600EDD6F0_m184266CE5496A1BE91621416AA637AA3393D0E60 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg3_t1725E0054AB3AEBD5D87C102B5F37228DFE150F8_mDF1547BF7DD64546914B670FEE44B536A5E37E6A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_t8D1755B2452C00237BAE69C839B1588F91EF385E_TisTArg0_tD6DB7FBE4067411684585DA8F39E1CA70F512F71_TisTArg1_t78E53566751E885F186ED56D612C8466A526EE77_TisTArg2_t50A8D5BA525C364D13FFB650423C526600EDD6F0_TisTArg3_t1725E0054AB3AEBD5D87C102B5F37228DFE150F8_m143130ACA95786DC138B9B7BD312AA8EB459AF17 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Arguments_4__ctor_mFFA4BBB4AA91E6F0558225DE7A254BF6A9B78234 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_tB26B5905CE92FE31357D8B448302BACAEFC228B1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m44AB956B00365B56A3AE5F3E0E88686737AB5015 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_t737B69663A39EBB1F9F6FD58BBF5C31E6106EAD2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_mBE766F2C4F13492F9CBCAB730071BDFEF4E22DA1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_t3BB2870DFD41038EFFD2C9A97E5C3D3205B32861 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_mF88DCFEB73435FF5B272877354B513409BD48742 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_t42482EA44D5784CF3680731A102D3A427A709786 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m8795C48964F9D09D82AEF296590BDBDF8DBD8CD6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m08A9175D9EF9FBDD31A6FEE4FCB12ECE1B31CCB7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_t63514987AAC77AB7E082A468911E50FAD9FF7892 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectDefaultConverter_1_t63E7C2A424B19AA1E531BA625AB91D246A7A983A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonObjectConverter_1_t801CE0E052115C6745D232120748E4209CAEAD03 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonResumableConverter_1_t13E62B7506ECBA09C249C8C64460861FD50CE860 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t2E9980DDD748A47FC4A6C714DDCD6E6766E9C0E0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonParameterInfo_1_t25121D9666B5E34FE3A04CE00C96C728E345CD66 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_t662FE67BAA43E72A3F22D83997C91AD8A159B418 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_TryRead_m61912E9756BF4A4E7B06299013B52318FED47BB7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TArg_t0A544929C5398A262BFEB210A4A5254B968CDF4C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumConverter_1__ctor_m9D8E7333B0FFDE771975E9B69CA4476D1BE55D66 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1__ctor_mCABBDD8760EA494A0B9675E5903C0F6AD8247995 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tF1798A895E9C1FEB6C1D79C4E4BFDD022B5A3288 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tA4ABD6F400372FD8853726F8BE4FB948426D8396 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumConverter_1_ConvertToUInt64_mD675589ACB2E7E4F8BF2BAD95EAC5C9B51A87268 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumConverter_1_t9DE369661F9CA350AFE174A57E3C6BB9CF653CCA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumConverter_1_FormatEnumValue_m1705C01AC2BA7F208EA331C76CC37FE8EE755E26 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_ReadWithQuotes_m067F5A988F7D159AF9284A38975FEDAA7F4584AB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m0C5799E0E91DAE6FE0C1AED297C0F0793B3C2C55 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m9B72364F585200A5BFFAF3F783C2BDEE8B9E59D9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisUInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m7FB8C39B74963EC6189A0111D471A59E4BB4E26D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisInt64_t378EE0D608BD3107E77238E85F30D2BBD46981F3_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m65C5E13FE4A4378D5B21E1588EA2103D4A6816A0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisSByte_t928712DD662DC29BA4FAAE8CE2230AFB23447F0B_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_mA2AD9ACB24DCB63FB73517A110C6F002738821CD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m565A0ADDA6C356E15ABFE3A98F7A86CD33B29E6F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisInt16_tD0F031114106263BB459DA1F099FF9F42691295A_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m96374D431F9AD5F35AA529E4C0EAF5AFC73D65F8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisUInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m552BA6A9E6C33319FCB2AB6844F1CC9C84EA38E9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumConverter_1_IsValidIdentifier_m64D04434DE7076D761AED545F928509413EECFC6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumConverter_1_FormatEnumValueToString_mE31C75CB06ADFC83926F21893BD19D885FB7A1BC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m20DE386E21AAF1A630DD503E38AEEEE581FD9868 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mF529E2E93A1EB253B9CC940119C189DB3ABC916F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisUInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281_m42A74569CB5E3CCA11D99266FFDD43AC4B20ACA3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisInt64_t378EE0D608BD3107E77238E85F30D2BBD46981F3_mF28C4448DD235A5F111350FBBF9C019347D8A8F9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisInt16_tD0F031114106263BB459DA1F099FF9F42691295A_m4F968A84963088BDAAA2406418F486ECAA24CC6D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisUInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD_mBB1473335C618D594439A59A4A908BAF93772EF1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mE33E630EA8EEC1E14B46016E344879797B004658 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_TisSByte_t928712DD662DC29BA4FAAE8CE2230AFB23447F0B_m6BEAE47EBEED2AD7733E02B342F1DD8D5204A4A5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enum_TryParse_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m056A103819A8412B3C74286C07CC31067384C54F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enum_TryParse_TisT_tA4ABD6F400372FD8853726F8BE4FB948426D8396_m9804F0C7A989474A4BA6EDBFD460503C45B8C8E1 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tA4ABD6F400372FD8853726F8BE4FB948426D8396 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1__ctor_m2F04B1119C82D4F6480ED0C1003910C6C0C3EDA2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_tD50173371C0829A6B3615D843F05B37002E0C988 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t04284A903F26E2E8CD64A881384B433B4CB5AD27 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Read_m3A23B71F665A3EAF4E26271D5D65D8D7DADF19D9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Nullable_1_t18720AA725575E4365449CD4C6F1B99EC58BB6A7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Nullable_1__ctor_m6A9C1866E304941F3CFF7F6B0F5241F002056784 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Nullable_1_get_HasValue_m628B0629353E10CCB21AEB68481BAFD87326A06E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Nullable_1_get_Value_mCE22557999CABC44D462AFC892D8A36B09517EA4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_Write_m238BC5032E5BF4FF9716961C4ADF22349CA662BF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_m675A8B48A7EB3FC877B85A787FE56785A743B385 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_m8CCABE900C85BC543DE7127DCC899353FDA553D8 },
};
extern const CustomAttributesCacheGenerator g_System_Text_Json_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Text_Json_CodeGenModule;
const Il2CppCodeGenModule g_System_Text_Json_CodeGenModule = 
{
	"System.Text.Json.dll",
	1579,
	s_methodPointers,
	297,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	66,
	s_rgctxIndices,
	588,
	s_rgctxValues,
	NULL,
	g_System_Text_Json_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
