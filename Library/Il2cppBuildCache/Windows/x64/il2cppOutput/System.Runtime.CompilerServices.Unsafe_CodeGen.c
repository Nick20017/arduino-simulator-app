﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 T System.Runtime.CompilerServices.Unsafe::Read(System.Void*)
// 0x00000002 T System.Runtime.CompilerServices.Unsafe::ReadUnaligned(System.Byte&)
// 0x00000003 System.Void System.Runtime.CompilerServices.Unsafe::WriteUnaligned(System.Byte&,T)
// 0x00000004 System.Void* System.Runtime.CompilerServices.Unsafe::AsPointer(T&)
// 0x00000005 System.Int32 System.Runtime.CompilerServices.Unsafe::SizeOf()
// 0x00000006 System.Void System.Runtime.CompilerServices.Unsafe::CopyBlock(System.Byte&,System.Byte&,System.UInt32)
extern void Unsafe_CopyBlock_mEB2CB6E517E5DB6C204029FD2E458B110AEE8FC7 (void);
// 0x00000007 System.Void System.Runtime.CompilerServices.Unsafe::InitBlockUnaligned(System.Void*,System.Byte,System.UInt32)
extern void Unsafe_InitBlockUnaligned_mDBC18343453EDA9D3974E125987D6774C5257F2F (void);
// 0x00000008 System.Void System.Runtime.CompilerServices.Unsafe::InitBlockUnaligned(System.Byte&,System.Byte,System.UInt32)
extern void Unsafe_InitBlockUnaligned_m68B5C8F29E67BD60711F6E9A841EB06C08B3D406 (void);
// 0x00000009 T System.Runtime.CompilerServices.Unsafe::As(System.Object)
// 0x0000000A T& System.Runtime.CompilerServices.Unsafe::AsRef(System.Void*)
// 0x0000000B TTo& System.Runtime.CompilerServices.Unsafe::As(TFrom&)
// 0x0000000C T& System.Runtime.CompilerServices.Unsafe::Add(T&,System.Int32)
// 0x0000000D System.Void* System.Runtime.CompilerServices.Unsafe::Add(System.Void*,System.Int32)
// 0x0000000E T& System.Runtime.CompilerServices.Unsafe::Add(T&,System.IntPtr)
// 0x0000000F T& System.Runtime.CompilerServices.Unsafe::AddByteOffset(T&,System.IntPtr)
// 0x00000010 System.IntPtr System.Runtime.CompilerServices.Unsafe::ByteOffset(T&,T&)
// 0x00000011 System.Boolean System.Runtime.CompilerServices.Unsafe::AreSame(T&,T&)
// 0x00000012 System.Void System.Runtime.Versioning.NonVersionableAttribute::.ctor()
extern void NonVersionableAttribute__ctor_m76289A4A00F259E5B5FC95BA62D1CB6D13466EA8 (void);
static Il2CppMethodPointer s_methodPointers[18] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Unsafe_CopyBlock_mEB2CB6E517E5DB6C204029FD2E458B110AEE8FC7,
	Unsafe_InitBlockUnaligned_mDBC18343453EDA9D3974E125987D6774C5257F2F,
	Unsafe_InitBlockUnaligned_m68B5C8F29E67BD60711F6E9A841EB06C08B3D406,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NonVersionableAttribute__ctor_m76289A4A00F259E5B5FC95BA62D1CB6D13466EA8,
};
static const int32_t s_InvokerIndices[18] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	3121,
	3137,
	3137,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
};
static const Il2CppTokenRangePair s_rgctxIndices[4] = 
{
	{ 0x06000005, { 0, 1 } },
	{ 0x0600000C, { 1, 1 } },
	{ 0x0600000D, { 2, 1 } },
	{ 0x0600000E, { 3, 1 } },
};
extern const uint32_t g_rgctx_T_tB05ECAAAEDF44D498FD1EAF2F74F9C6C78425BF5;
extern const uint32_t g_rgctx_T_t1B5EA1CCC461CFBA4811851634B73C8BF9BB903F;
extern const uint32_t g_rgctx_T_t6578FD8D25904745DD1BFDC5BD3EE2B2C1998DCE;
extern const uint32_t g_rgctx_T_tD7E72960443CB9ADFAC5FC67418B0B2B5CEF6544;
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tB05ECAAAEDF44D498FD1EAF2F74F9C6C78425BF5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t1B5EA1CCC461CFBA4811851634B73C8BF9BB903F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t6578FD8D25904745DD1BFDC5BD3EE2B2C1998DCE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tD7E72960443CB9ADFAC5FC67418B0B2B5CEF6544 },
};
extern const CustomAttributesCacheGenerator g_System_Runtime_CompilerServices_Unsafe_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Runtime_CompilerServices_Unsafe_CodeGenModule;
const Il2CppCodeGenModule g_System_Runtime_CompilerServices_Unsafe_CodeGenModule = 
{
	"System.Runtime.CompilerServices.Unsafe.dll",
	18,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	4,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_System_Runtime_CompilerServices_Unsafe_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
