﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m7D7D024DA2EA05AEDF8B8C470F678A5DA96C8EB8 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m07A8C937D13DE79AF8ED555F18E5AE9FDA6C3879 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
extern void IsByRefLikeAttribute__ctor_m3F813C04C0FAF02B5AF712ED98929300CD6E44DD (void);
// 0x00000004 System.Void System.SequencePosition::.ctor(System.Object,System.Int32)
extern void SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0 (void);
// 0x00000005 System.Object System.SequencePosition::GetObject()
extern void SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525 (void);
// 0x00000006 System.Int32 System.SequencePosition::GetInteger()
extern void SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE (void);
// 0x00000007 System.Boolean System.SequencePosition::Equals(System.SequencePosition)
extern void SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795 (void);
// 0x00000008 System.Boolean System.SequencePosition::Equals(System.Object)
extern void SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58 (void);
// 0x00000009 System.Int32 System.SequencePosition::GetHashCode()
extern void SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2 (void);
// 0x0000000A System.Void System.ThrowHelper::ThrowArgumentNullException(System.ExceptionArgument)
extern void ThrowHelper_ThrowArgumentNullException_m3B4D674B817C817E97F4687F0130007D315F8B34 (void);
// 0x0000000B System.Exception System.ThrowHelper::CreateArgumentNullException(System.ExceptionArgument)
extern void ThrowHelper_CreateArgumentNullException_mA70D942EBA7503962BA72170F026A966513590FC (void);
// 0x0000000C System.Void System.ThrowHelper::ThrowArrayTypeMismatchException()
extern void ThrowHelper_ThrowArrayTypeMismatchException_mFC0D7756FD2EA1A7E41D8426D819369FDBD728FC (void);
// 0x0000000D System.Exception System.ThrowHelper::CreateArrayTypeMismatchException()
extern void ThrowHelper_CreateArrayTypeMismatchException_m23F27BF82F951A64682A2CF14E0EDE9F3B54C93F (void);
// 0x0000000E System.Void System.ThrowHelper::ThrowArgumentException_InvalidTypeWithPointersNotSupported(System.Type)
extern void ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4 (void);
// 0x0000000F System.Exception System.ThrowHelper::CreateArgumentException_InvalidTypeWithPointersNotSupported(System.Type)
extern void ThrowHelper_CreateArgumentException_InvalidTypeWithPointersNotSupported_m4019588ED8511C985604C8CC9AD4AB6414676945 (void);
// 0x00000010 System.Void System.ThrowHelper::ThrowArgumentException_DestinationTooShort()
extern void ThrowHelper_ThrowArgumentException_DestinationTooShort_mD9C82D6A62948DA443166283990BF760F77C76C8 (void);
// 0x00000011 System.Exception System.ThrowHelper::CreateArgumentException_DestinationTooShort()
extern void ThrowHelper_CreateArgumentException_DestinationTooShort_m75CF4B3D7F56B0383E0BC84D86C085AA0CE90CD1 (void);
// 0x00000012 System.Void System.ThrowHelper::ThrowIndexOutOfRangeException()
extern void ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC (void);
// 0x00000013 System.Exception System.ThrowHelper::CreateIndexOutOfRangeException()
extern void ThrowHelper_CreateIndexOutOfRangeException_m8C8886676269B09CC5241BA6F5330D78B26F527B (void);
// 0x00000014 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
extern void ThrowHelper_ThrowArgumentOutOfRangeException_mB72471F11341E214DA380AF2B87C3F28EC51CA59 (void);
// 0x00000015 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException()
extern void ThrowHelper_CreateArgumentOutOfRangeException_m0841E9BF864372D7BF0512A13456F985C53FC03D (void);
// 0x00000016 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException(System.ExceptionArgument)
extern void ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5 (void);
// 0x00000017 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException(System.ExceptionArgument)
extern void ThrowHelper_CreateArgumentOutOfRangeException_m3ED3DA6D593699354BA4D397790440F3BFE84AEA (void);
// 0x00000018 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException_PrecisionTooLarge()
extern void ThrowHelper_ThrowArgumentOutOfRangeException_PrecisionTooLarge_mAC345A1F72DBC919354CCB54CC6C24EF44BE48C1 (void);
// 0x00000019 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException_PrecisionTooLarge()
extern void ThrowHelper_CreateArgumentOutOfRangeException_PrecisionTooLarge_mC1889FF89FD22816EB8D105C942166D0BF6ADFAD (void);
// 0x0000001A System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException_SymbolDoesNotFit()
extern void ThrowHelper_ThrowArgumentOutOfRangeException_SymbolDoesNotFit_m601A3BCC469FE8A7420CC33708307189B6C48B61 (void);
// 0x0000001B System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException_SymbolDoesNotFit()
extern void ThrowHelper_CreateArgumentOutOfRangeException_SymbolDoesNotFit_m903F8FB8357FE1BAB8BF450E0F95A79117EF2D19 (void);
// 0x0000001C System.Void System.ThrowHelper::ThrowInvalidOperationException()
extern void ThrowHelper_ThrowInvalidOperationException_mC18897E999FE00AE3ACC3543A468201068A217F8 (void);
// 0x0000001D System.Exception System.ThrowHelper::CreateInvalidOperationException()
extern void ThrowHelper_CreateInvalidOperationException_mAA36D488898B83C836372B8D095EFEC4136121B3 (void);
// 0x0000001E System.Void System.ThrowHelper::ThrowInvalidOperationException_EndPositionNotReached()
extern void ThrowHelper_ThrowInvalidOperationException_EndPositionNotReached_m67B5DCC8C43494E0A491781D118E147337664DB0 (void);
// 0x0000001F System.Exception System.ThrowHelper::CreateInvalidOperationException_EndPositionNotReached()
extern void ThrowHelper_CreateInvalidOperationException_EndPositionNotReached_m35A30B605551B8CACAE6B842C8B525BC7078FE72 (void);
// 0x00000020 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException_PositionOutOfRange()
extern void ThrowHelper_ThrowArgumentOutOfRangeException_PositionOutOfRange_mE66B589C0CE79CD3252C96D133A4DA6BFF64228D (void);
// 0x00000021 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException_PositionOutOfRange()
extern void ThrowHelper_CreateArgumentOutOfRangeException_PositionOutOfRange_m09B8EF6F30DDB19BF9AD63605556AED12E7DE03A (void);
// 0x00000022 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException_OffsetOutOfRange()
extern void ThrowHelper_ThrowArgumentOutOfRangeException_OffsetOutOfRange_m33BB375A469C5D43F27FAF1E1573659C8D6D46B6 (void);
// 0x00000023 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException_OffsetOutOfRange()
extern void ThrowHelper_CreateArgumentOutOfRangeException_OffsetOutOfRange_m4006B68C0940F1864B48E9E2CAE0A62FE91910F9 (void);
// 0x00000024 System.Void System.ThrowHelper::ThrowFormatException_BadFormatSpecifier()
extern void ThrowHelper_ThrowFormatException_BadFormatSpecifier_m1E35DA4BEAAC3A721CFB9CE82029EBABF763EA8D (void);
// 0x00000025 System.Exception System.ThrowHelper::CreateFormatException_BadFormatSpecifier()
extern void ThrowHelper_CreateFormatException_BadFormatSpecifier_mA1D2DB12AB680960531A3C1A97098925F5154FDB (void);
// 0x00000026 System.Boolean System.ThrowHelper::TryFormatThrowFormatException(System.Int32&)
extern void ThrowHelper_TryFormatThrowFormatException_mAE469FD4BD034BFD4FB18C512DAD188188AADFCA (void);
// 0x00000027 System.Boolean System.ThrowHelper::TryParseThrowFormatException(T&,System.Int32&)
// 0x00000028 System.Void System.ThrowHelper::ThrowArgumentValidationException(System.Buffers.ReadOnlySequenceSegment`1<T>,System.Int32,System.Buffers.ReadOnlySequenceSegment`1<T>)
// 0x00000029 System.Exception System.ThrowHelper::CreateArgumentValidationException(System.Buffers.ReadOnlySequenceSegment`1<T>,System.Int32,System.Buffers.ReadOnlySequenceSegment`1<T>)
// 0x0000002A System.Void System.ThrowHelper::ThrowArgumentValidationException(System.Array,System.Int32)
extern void ThrowHelper_ThrowArgumentValidationException_m02E99F21139D9C2446BCF31922E6B9A1FB6CBFE9 (void);
// 0x0000002B System.Exception System.ThrowHelper::CreateArgumentValidationException(System.Array,System.Int32)
extern void ThrowHelper_CreateArgumentValidationException_mA1BAB2087749EF0A3433426D6B4886903B64C893 (void);
// 0x0000002C System.Void System.ThrowHelper::ThrowStartOrEndArgumentValidationException(System.Int64)
extern void ThrowHelper_ThrowStartOrEndArgumentValidationException_m0BB9EBD3A2B133B27075024CE04996C5626E7C38 (void);
// 0x0000002D System.Exception System.ThrowHelper::CreateStartOrEndArgumentValidationException(System.Int64)
extern void ThrowHelper_CreateStartOrEndArgumentValidationException_mC18923AF89FCBE538463BC90C517FA9E4691734C (void);
// 0x0000002E System.UInt32 System.DecimalDecCalc::D32DivMod1E9(System.UInt32,System.UInt32&)
extern void DecimalDecCalc_D32DivMod1E9_mAD8A67341D4FAFC5C7B8166220023AA68003ABBF (void);
// 0x0000002F System.UInt32 System.DecimalDecCalc::DecDivMod1E9(System.MutableDecimal&)
extern void DecimalDecCalc_DecDivMod1E9_mEBD288904D9655A6FFF62BA3620C6C55FF28B8B6 (void);
// 0x00000030 System.Void System.DecimalDecCalc::DecAddInt32(System.MutableDecimal&,System.UInt32)
extern void DecimalDecCalc_DecAddInt32_mCA42EB3D01D859FA31D304138E4BEF39FEB549AE (void);
// 0x00000031 System.Boolean System.DecimalDecCalc::D32AddCarry(System.UInt32&,System.UInt32)
extern void DecimalDecCalc_D32AddCarry_m12237332B16193F0B942AEFC955A62400D3F51B9 (void);
// 0x00000032 System.Void System.DecimalDecCalc::DecMul10(System.MutableDecimal&)
extern void DecimalDecCalc_DecMul10_m937563702D52CBD29F01CC75B62F626BEC117892 (void);
// 0x00000033 System.Void System.DecimalDecCalc::DecShiftLeft(System.MutableDecimal&)
extern void DecimalDecCalc_DecShiftLeft_m63A7A170CA5CF4A864BAAD95D88C299D5AFB71F3 (void);
// 0x00000034 System.Void System.DecimalDecCalc::DecAdd(System.MutableDecimal&,System.MutableDecimal)
extern void DecimalDecCalc_DecAdd_m52E651ADFE94ACFD0CDDC8C14A50D2830E46E3D5 (void);
// 0x00000035 System.Void System.Number::RoundNumber(System.NumberBuffer&,System.Int32)
extern void Number_RoundNumber_mEC3B9B63F68460A64F6F6913BB80F3BEBB780F07 (void);
// 0x00000036 System.Boolean System.Number::NumberBufferToDouble(System.NumberBuffer&,System.Double&)
extern void Number_NumberBufferToDouble_mA943AD6BA0AE626079DBC29F9B659D26D4E1D424 (void);
// 0x00000037 System.Boolean System.Number::NumberBufferToDecimal(System.NumberBuffer&,System.Decimal&)
extern void Number_NumberBufferToDecimal_m9EC553B819A7A5F0260E71EE5684E7EA7CEE6D42 (void);
// 0x00000038 System.Void System.Number::DecimalToNumber(System.Decimal,System.NumberBuffer&)
extern void Number_DecimalToNumber_mDB370C6FEDCCE206F35651207AE2E13ABF525408 (void);
// 0x00000039 System.UInt32 System.Number::DigitsToInt(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Number_DigitsToInt_mF9D8FE36B186C206448E7489A7AFC2EDEF604BD2 (void);
// 0x0000003A System.UInt64 System.Number::Mul32x32To64(System.UInt32,System.UInt32)
extern void Number_Mul32x32To64_mE1E983CFD213FF07619AEDA39C30700B6213EE13 (void);
// 0x0000003B System.UInt64 System.Number::Mul64Lossy(System.UInt64,System.UInt64,System.Int32&)
extern void Number_Mul64Lossy_m62FA4C0FD0DFB698403843BF67DFED5D0881DD9D (void);
// 0x0000003C System.Int32 System.Number::abs(System.Int32)
extern void Number_abs_m602E755F75A75A2F98CFEB174046DA2213A30DB5 (void);
// 0x0000003D System.Double System.Number::NumberToDouble(System.NumberBuffer&)
extern void Number_NumberToDouble_m8DE02F8F5A4B55ED6EC5995419C9D4AE9BB0802E (void);
// 0x0000003E System.Void System.Number::.cctor()
extern void Number__cctor_m13165024FAE32EB10AAF002D913A479A756870CB (void);
// 0x0000003F System.UInt32 System.Number/DoubleHelper::Exponent(System.Double)
extern void DoubleHelper_Exponent_m030E2C0AF265E2CA0ACEA5E4C9E66C1EAB462D5C (void);
// 0x00000040 System.UInt64 System.Number/DoubleHelper::Mantissa(System.Double)
extern void DoubleHelper_Mantissa_m5275E987A447C0B2F0F10D1E08B0A73DB121A6BE (void);
// 0x00000041 System.Span`1<System.Byte> System.NumberBuffer::get_Digits()
extern void NumberBuffer_get_Digits_m77F55E994E325F745CBEE5598D24263834F34F3B (void);
// 0x00000042 System.Byte* System.NumberBuffer::get_UnsafeDigits()
extern void NumberBuffer_get_UnsafeDigits_m95B0D74B60926B0E8788E7E6FDC551E7240F40FE (void);
// 0x00000043 System.Int32 System.NumberBuffer::get_NumDigits()
extern void NumberBuffer_get_NumDigits_mC0AF0400D548D41907EA3204965C78A10DB463D5 (void);
// 0x00000044 System.String System.NumberBuffer::ToString()
extern void NumberBuffer_ToString_m64B3ED10515B329DDED1167F97E5BE0B297955AD (void);
// 0x00000045 System.Void System.Memory`1::.ctor(T[])
// 0x00000046 System.Void System.Memory`1::.ctor(T[],System.Int32)
// 0x00000047 System.Void System.Memory`1::.ctor(T[],System.Int32,System.Int32)
// 0x00000048 System.Void System.Memory`1::.ctor(System.Buffers.MemoryManager`1<T>,System.Int32)
// 0x00000049 System.Void System.Memory`1::.ctor(System.Buffers.MemoryManager`1<T>,System.Int32,System.Int32)
// 0x0000004A System.Void System.Memory`1::.ctor(System.Object,System.Int32,System.Int32)
// 0x0000004B System.Memory`1<T> System.Memory`1::op_Implicit(T[])
// 0x0000004C System.Memory`1<T> System.Memory`1::op_Implicit(System.ArraySegment`1<T>)
// 0x0000004D System.ReadOnlyMemory`1<T> System.Memory`1::op_Implicit(System.Memory`1<T>)
// 0x0000004E System.Memory`1<T> System.Memory`1::get_Empty()
// 0x0000004F System.Int32 System.Memory`1::get_Length()
// 0x00000050 System.Boolean System.Memory`1::get_IsEmpty()
// 0x00000051 System.String System.Memory`1::ToString()
// 0x00000052 System.Memory`1<T> System.Memory`1::Slice(System.Int32)
// 0x00000053 System.Memory`1<T> System.Memory`1::Slice(System.Int32,System.Int32)
// 0x00000054 System.Span`1<T> System.Memory`1::get_Span()
// 0x00000055 System.Void System.Memory`1::CopyTo(System.Memory`1<T>)
// 0x00000056 System.Boolean System.Memory`1::TryCopyTo(System.Memory`1<T>)
// 0x00000057 System.Buffers.MemoryHandle System.Memory`1::Pin()
// 0x00000058 T[] System.Memory`1::ToArray()
// 0x00000059 System.Boolean System.Memory`1::Equals(System.Object)
// 0x0000005A System.Boolean System.Memory`1::Equals(System.Memory`1<T>)
// 0x0000005B System.Int32 System.Memory`1::GetHashCode()
// 0x0000005C System.Int32 System.Memory`1::CombineHashCodes(System.Int32,System.Int32)
// 0x0000005D System.Int32 System.Memory`1::CombineHashCodes(System.Int32,System.Int32,System.Int32)
// 0x0000005E System.Void System.MemoryDebugView`1::.ctor(System.Memory`1<T>)
// 0x0000005F System.Void System.MemoryDebugView`1::.ctor(System.ReadOnlyMemory`1<T>)
// 0x00000060 T[] System.MemoryDebugView`1::get_Items()
// 0x00000061 System.Int32 System.MemoryExtensions::IndexOf(System.Span`1<T>,T)
// 0x00000062 System.Boolean System.MemoryExtensions::SequenceEqual(System.Span`1<T>,System.ReadOnlySpan`1<T>)
// 0x00000063 System.Int32 System.MemoryExtensions::IndexOf(System.ReadOnlySpan`1<T>,T)
// 0x00000064 System.Int32 System.MemoryExtensions::IndexOfAny(System.ReadOnlySpan`1<T>,T,T,T)
// 0x00000065 System.Boolean System.MemoryExtensions::SequenceEqual(System.ReadOnlySpan`1<T>,System.ReadOnlySpan`1<T>)
// 0x00000066 System.Boolean System.MemoryExtensions::StartsWith(System.ReadOnlySpan`1<T>,System.ReadOnlySpan`1<T>)
// 0x00000067 System.Span`1<T> System.MemoryExtensions::AsSpan(T[],System.Int32,System.Int32)
// 0x00000068 System.Span`1<T> System.MemoryExtensions::AsSpan(System.ArraySegment`1<T>)
// 0x00000069 System.Memory`1<T> System.MemoryExtensions::AsMemory(T[],System.Int32)
// 0x0000006A System.Memory`1<T> System.MemoryExtensions::AsMemory(T[],System.Int32,System.Int32)
// 0x0000006B System.Void System.MemoryExtensions::CopyTo(T[],System.Span`1<T>)
// 0x0000006C System.Boolean System.MemoryExtensions::IsTypeComparableAsBytes(System.NUInt&)
// 0x0000006D System.Span`1<T> System.MemoryExtensions::AsSpan(T[],System.Int32)
// 0x0000006E System.ReadOnlySpan`1<System.Char> System.MemoryExtensions::AsSpan(System.String)
extern void MemoryExtensions_AsSpan_m7527C7806D1DD24C012DC60C12280A9E1AEA8F15 (void);
// 0x0000006F System.ReadOnlyMemory`1<System.Char> System.MemoryExtensions::AsMemory(System.String)
extern void MemoryExtensions_AsMemory_mDE1615649DE907A3C22F6BEBBF0C8F80E4652B0D (void);
// 0x00000070 System.ReadOnlyMemory`1<System.Char> System.MemoryExtensions::AsMemory(System.String,System.Int32,System.Int32)
extern void MemoryExtensions_AsMemory_m9F2378B1710076CA61B1FC3E687A06BC6063A9DC (void);
// 0x00000071 System.IntPtr System.MemoryExtensions::MeasureStringAdjustment()
extern void MemoryExtensions_MeasureStringAdjustment_m8E2719E3CCAD24803BEF8B9C9873DDFAA528C762 (void);
// 0x00000072 System.Void System.MemoryExtensions::.cctor()
extern void MemoryExtensions__cctor_mC634116818572F66DC5A4416FB29AFBFCE859EBE (void);
// 0x00000073 System.Void System.ReadOnlyMemory`1::.ctor(T[])
// 0x00000074 System.Void System.ReadOnlyMemory`1::.ctor(T[],System.Int32,System.Int32)
// 0x00000075 System.Void System.ReadOnlyMemory`1::.ctor(System.Object,System.Int32,System.Int32)
// 0x00000076 System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::op_Implicit(T[])
// 0x00000077 System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::op_Implicit(System.ArraySegment`1<T>)
// 0x00000078 System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::get_Empty()
// 0x00000079 System.Int32 System.ReadOnlyMemory`1::get_Length()
// 0x0000007A System.Boolean System.ReadOnlyMemory`1::get_IsEmpty()
// 0x0000007B System.String System.ReadOnlyMemory`1::ToString()
// 0x0000007C System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::Slice(System.Int32)
// 0x0000007D System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::Slice(System.Int32,System.Int32)
// 0x0000007E System.ReadOnlySpan`1<T> System.ReadOnlyMemory`1::get_Span()
// 0x0000007F System.Void System.ReadOnlyMemory`1::CopyTo(System.Memory`1<T>)
// 0x00000080 System.Boolean System.ReadOnlyMemory`1::TryCopyTo(System.Memory`1<T>)
// 0x00000081 System.Buffers.MemoryHandle System.ReadOnlyMemory`1::Pin()
// 0x00000082 T[] System.ReadOnlyMemory`1::ToArray()
// 0x00000083 System.Boolean System.ReadOnlyMemory`1::Equals(System.Object)
// 0x00000084 System.Boolean System.ReadOnlyMemory`1::Equals(System.ReadOnlyMemory`1<T>)
// 0x00000085 System.Int32 System.ReadOnlyMemory`1::GetHashCode()
// 0x00000086 System.Int32 System.ReadOnlyMemory`1::CombineHashCodes(System.Int32,System.Int32)
// 0x00000087 System.Int32 System.ReadOnlyMemory`1::CombineHashCodes(System.Int32,System.Int32,System.Int32)
// 0x00000088 System.Object System.ReadOnlyMemory`1::GetObjectStartLength(System.Int32&,System.Int32&)
// 0x00000089 System.Int32 System.ReadOnlySpan`1::get_Length()
// 0x0000008A System.Boolean System.ReadOnlySpan`1::get_IsEmpty()
// 0x0000008B System.Boolean System.ReadOnlySpan`1::op_Inequality(System.ReadOnlySpan`1<T>,System.ReadOnlySpan`1<T>)
// 0x0000008C System.Boolean System.ReadOnlySpan`1::Equals(System.Object)
// 0x0000008D System.Int32 System.ReadOnlySpan`1::GetHashCode()
// 0x0000008E System.ReadOnlySpan`1<T> System.ReadOnlySpan`1::op_Implicit(T[])
// 0x0000008F System.ReadOnlySpan`1<T> System.ReadOnlySpan`1::op_Implicit(System.ArraySegment`1<T>)
// 0x00000090 System.ReadOnlySpan`1<T> System.ReadOnlySpan`1::get_Empty()
// 0x00000091 System.ReadOnlySpan`1/Enumerator<T> System.ReadOnlySpan`1::GetEnumerator()
// 0x00000092 System.Void System.ReadOnlySpan`1::.ctor(T[])
// 0x00000093 System.Void System.ReadOnlySpan`1::.ctor(T[],System.Int32,System.Int32)
// 0x00000094 System.Void System.ReadOnlySpan`1::.ctor(System.Void*,System.Int32)
// 0x00000095 System.Void System.ReadOnlySpan`1::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
// 0x00000096 T& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1::get_Item(System.Int32)
// 0x00000097 T& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1::GetPinnableReference()
// 0x00000098 System.Void System.ReadOnlySpan`1::CopyTo(System.Span`1<T>)
// 0x00000099 System.Boolean System.ReadOnlySpan`1::TryCopyTo(System.Span`1<T>)
// 0x0000009A System.Boolean System.ReadOnlySpan`1::op_Equality(System.ReadOnlySpan`1<T>,System.ReadOnlySpan`1<T>)
// 0x0000009B System.String System.ReadOnlySpan`1::ToString()
// 0x0000009C System.ReadOnlySpan`1<T> System.ReadOnlySpan`1::Slice(System.Int32)
// 0x0000009D System.ReadOnlySpan`1<T> System.ReadOnlySpan`1::Slice(System.Int32,System.Int32)
// 0x0000009E T[] System.ReadOnlySpan`1::ToArray()
// 0x0000009F T& System.ReadOnlySpan`1::DangerousGetPinnableReference()
// 0x000000A0 System.Pinnable`1<T> System.ReadOnlySpan`1::get_Pinnable()
// 0x000000A1 System.IntPtr System.ReadOnlySpan`1::get_ByteOffset()
// 0x000000A2 System.Void System.ReadOnlySpan`1/Enumerator::.ctor(System.ReadOnlySpan`1<T>)
// 0x000000A3 System.Int32 System.Span`1::get_Length()
// 0x000000A4 System.Boolean System.Span`1::get_IsEmpty()
// 0x000000A5 System.Boolean System.Span`1::op_Inequality(System.Span`1<T>,System.Span`1<T>)
// 0x000000A6 System.Boolean System.Span`1::Equals(System.Object)
// 0x000000A7 System.Int32 System.Span`1::GetHashCode()
// 0x000000A8 System.Span`1<T> System.Span`1::op_Implicit(T[])
// 0x000000A9 System.Span`1<T> System.Span`1::op_Implicit(System.ArraySegment`1<T>)
// 0x000000AA System.Span`1<T> System.Span`1::get_Empty()
// 0x000000AB System.Span`1/Enumerator<T> System.Span`1::GetEnumerator()
// 0x000000AC System.Void System.Span`1::.ctor(T[])
// 0x000000AD System.Span`1<T> System.Span`1::Create(T[],System.Int32)
// 0x000000AE System.Void System.Span`1::.ctor(T[],System.Int32,System.Int32)
// 0x000000AF System.Void System.Span`1::.ctor(System.Void*,System.Int32)
// 0x000000B0 System.Void System.Span`1::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
// 0x000000B1 T& System.Span`1::get_Item(System.Int32)
// 0x000000B2 T& System.Span`1::GetPinnableReference()
// 0x000000B3 System.Void System.Span`1::Clear()
// 0x000000B4 System.Void System.Span`1::Fill(T)
// 0x000000B5 System.Void System.Span`1::CopyTo(System.Span`1<T>)
// 0x000000B6 System.Boolean System.Span`1::TryCopyTo(System.Span`1<T>)
// 0x000000B7 System.Boolean System.Span`1::op_Equality(System.Span`1<T>,System.Span`1<T>)
// 0x000000B8 System.ReadOnlySpan`1<T> System.Span`1::op_Implicit(System.Span`1<T>)
// 0x000000B9 System.String System.Span`1::ToString()
// 0x000000BA System.Span`1<T> System.Span`1::Slice(System.Int32)
// 0x000000BB System.Span`1<T> System.Span`1::Slice(System.Int32,System.Int32)
// 0x000000BC T[] System.Span`1::ToArray()
// 0x000000BD T& System.Span`1::DangerousGetPinnableReference()
// 0x000000BE System.Pinnable`1<T> System.Span`1::get_Pinnable()
// 0x000000BF System.IntPtr System.Span`1::get_ByteOffset()
// 0x000000C0 System.Void System.Span`1/Enumerator::.ctor(System.Span`1<T>)
// 0x000000C1 System.Void System.SpanDebugView`1::.ctor(System.Span`1<T>)
// 0x000000C2 System.Void System.SpanDebugView`1::.ctor(System.ReadOnlySpan`1<T>)
// 0x000000C3 T[] System.SpanDebugView`1::get_Items()
// 0x000000C4 System.Int32 System.SpanHelpers::IndexOf(System.Byte&,System.Byte,System.Int32)
extern void SpanHelpers_IndexOf_m996881920138B2EC72C814473789D6AB958B92F2 (void);
// 0x000000C5 System.Int32 System.SpanHelpers::IndexOfAny(System.Byte&,System.Byte,System.Byte,System.Byte,System.Int32)
extern void SpanHelpers_IndexOfAny_m08EBFBAA90222E41BD0E0BECA675C836CDF8B459 (void);
// 0x000000C6 System.Boolean System.SpanHelpers::SequenceEqual(System.Byte&,System.Byte&,System.NUInt)
extern void SpanHelpers_SequenceEqual_mDEB0F358BB173EA24BEEB0609454A997E9273A89 (void);
// 0x000000C7 System.Int32 System.SpanHelpers::LocateFirstFoundByte(System.Numerics.Vector`1<System.Byte>)
extern void SpanHelpers_LocateFirstFoundByte_m419FAFA78D34AB13ADED0E04C54AD68D69551E83 (void);
// 0x000000C8 System.Int32 System.SpanHelpers::LocateFirstFoundByte(System.UInt64)
extern void SpanHelpers_LocateFirstFoundByte_m6AEEBF64B95585D577D0041CE56E0BE6F28AEFE4 (void);
// 0x000000C9 System.Numerics.Vector`1<System.Byte> System.SpanHelpers::GetVector(System.Byte)
extern void SpanHelpers_GetVector_mB6C94E0B80F7E5A77D84E2B202C1B8DD742E9FD9 (void);
// 0x000000CA System.Int32 System.SpanHelpers::IndexOf(System.Char&,System.Char,System.Int32)
extern void SpanHelpers_IndexOf_m0740DDBBE5723E3595EADF2552551F636C18A259 (void);
// 0x000000CB System.Int32 System.SpanHelpers::LocateFirstFoundChar(System.Numerics.Vector`1<System.UInt16>)
extern void SpanHelpers_LocateFirstFoundChar_m6F7002BE58BB1D79412002F2AEF2E430F8720F97 (void);
// 0x000000CC System.Int32 System.SpanHelpers::LocateFirstFoundChar(System.UInt64)
extern void SpanHelpers_LocateFirstFoundChar_m7B3D3FD47EB5BA8837CE3E8CE2D2BBA7BFC62CE3 (void);
// 0x000000CD System.Int32 System.SpanHelpers::IndexOf(T&,T,System.Int32)
// 0x000000CE System.Int32 System.SpanHelpers::IndexOfAny(T&,T,T,T,System.Int32)
// 0x000000CF System.Boolean System.SpanHelpers::SequenceEqual(T&,T&,System.Int32)
// 0x000000D0 System.Void System.SpanHelpers::CopyTo(T&,System.Int32,T&,System.Int32)
// 0x000000D1 System.IntPtr System.SpanHelpers::Add(System.IntPtr,System.Int32)
// 0x000000D2 System.Boolean System.SpanHelpers::IsReferenceOrContainsReferences()
// 0x000000D3 System.Boolean System.SpanHelpers::IsReferenceOrContainsReferencesCore(System.Type)
extern void SpanHelpers_IsReferenceOrContainsReferencesCore_m4046A8EAD00DA02AA423C292A8FCBB08268AD781 (void);
// 0x000000D4 System.Void System.SpanHelpers::ClearLessThanPointerSized(System.Byte*,System.UIntPtr)
extern void SpanHelpers_ClearLessThanPointerSized_m257390BAE1A54335F742BD17D85AF6D8FC03C831 (void);
// 0x000000D5 System.Void System.SpanHelpers::ClearLessThanPointerSized(System.Byte&,System.UIntPtr)
extern void SpanHelpers_ClearLessThanPointerSized_mDD75E922D42E70B6F76DB1A1EC1A96F59CAFF0B5 (void);
// 0x000000D6 System.Void System.SpanHelpers::ClearPointerSizedWithoutReferences(System.Byte&,System.UIntPtr)
extern void SpanHelpers_ClearPointerSizedWithoutReferences_mC6EF2B959C4B0E58F8D4B8C9A5EF341F948FFAAA (void);
// 0x000000D7 System.Void System.SpanHelpers::ClearPointerSizedWithReferences(System.IntPtr&,System.UIntPtr)
extern void SpanHelpers_ClearPointerSizedWithReferences_m45CDDDFAE259A9678B759645C7AB467860D44BAE (void);
// 0x000000D8 System.Boolean System.SpanHelpers::LessThanEqual(System.IntPtr,System.UIntPtr)
extern void SpanHelpers_LessThanEqual_mCFA5E9CC05F428B1EDA65967FEF81267F917E88C (void);
// 0x000000D9 System.IntPtr System.SpanHelpers/PerTypeValues`1::MeasureArrayAdjustment()
// 0x000000DA System.Void System.SpanHelpers/PerTypeValues`1::.cctor()
// 0x000000DB System.Void System.NUInt::.ctor(System.UInt32)
extern void NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E (void);
// 0x000000DC System.Void System.NUInt::.ctor(System.UInt64)
extern void NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973 (void);
// 0x000000DD System.NUInt System.NUInt::op_Explicit(System.Int32)
extern void NUInt_op_Explicit_m680513883587956D1452B1EB6D321D4C3A0C8366 (void);
// 0x000000DE System.Void* System.NUInt::op_Explicit(System.NUInt)
extern void NUInt_op_Explicit_mAC8186F05FC1F16BAEB9A73957491CB24A067D46 (void);
// 0x000000DF System.NUInt System.NUInt::op_Multiply(System.NUInt,System.NUInt)
extern void NUInt_op_Multiply_mABFB3E10A51F74FDC0CD9B799B7BF35C2C5D8D85 (void);
// 0x000000E0 System.Boolean System.MutableDecimal::get_IsNegative()
extern void MutableDecimal_get_IsNegative_m6CC9630C1FE5DAABD29CEE9EF5281C37D12CE702 (void);
// 0x000000E1 System.Void System.MutableDecimal::set_IsNegative(System.Boolean)
extern void MutableDecimal_set_IsNegative_mF373061A5BA3F192A2AA544BCB933C81BF71AC67 (void);
// 0x000000E2 System.Int32 System.MutableDecimal::get_Scale()
extern void MutableDecimal_get_Scale_mD47D52938E7026D2EC3AA837BABF7162C4F727A3 (void);
// 0x000000E3 System.Void System.MutableDecimal::set_Scale(System.Int32)
extern void MutableDecimal_set_Scale_m9253E0BBFF59D428FF76EA0A530D644053C3075C (void);
// 0x000000E4 System.Resources.ResourceManager System.SR::get_ResourceManager()
extern void SR_get_ResourceManager_m18A791F4D611559D5B214B3020BAB11F2AC869EC (void);
// 0x000000E5 System.Boolean System.SR::UsingResourceKeys()
extern void SR_UsingResourceKeys_m08DBDDDDF80E9F0013615CAB611F552F836BB526 (void);
// 0x000000E6 System.String System.SR::GetResourceString(System.String,System.String)
extern void SR_GetResourceString_mEC79B3C28B26B1540E26C3CD899938CC955A4748 (void);
// 0x000000E7 System.String System.SR::Format(System.String,System.Object)
extern void SR_Format_m4480ECD777F2A905A368094827DDCB43478A8053 (void);
// 0x000000E8 System.Type System.SR::get_ResourceType()
extern void SR_get_ResourceType_mA677195FD1721150495B84739EFFDCB9366A5541 (void);
// 0x000000E9 System.String System.SR::get_NotSupported_CannotCallEqualsOnSpan()
extern void SR_get_NotSupported_CannotCallEqualsOnSpan_mACE24A88A0ADF9880C315FDC0963BA17E66B0394 (void);
// 0x000000EA System.String System.SR::get_NotSupported_CannotCallGetHashCodeOnSpan()
extern void SR_get_NotSupported_CannotCallGetHashCodeOnSpan_m4BC3D1B6994913E69BDD4028026F18A279A9DBDB (void);
// 0x000000EB System.String System.SR::get_Argument_InvalidTypeWithPointersNotSupported()
extern void SR_get_Argument_InvalidTypeWithPointersNotSupported_m2FD2DCBFF1853C8F9616D4C55DD1C14163A06B75 (void);
// 0x000000EC System.String System.SR::get_Argument_DestinationTooShort()
extern void SR_get_Argument_DestinationTooShort_mDD536A55FFA1BD1CF5C34D9E074420C183905559 (void);
// 0x000000ED System.String System.SR::get_Argument_BadFormatSpecifier()
extern void SR_get_Argument_BadFormatSpecifier_mFE81E4F926274AB402B890E679E6CAB600E61433 (void);
// 0x000000EE System.String System.SR::get_Argument_GWithPrecisionNotSupported()
extern void SR_get_Argument_GWithPrecisionNotSupported_mF77D1EF96FE22465E62C65C5895E968E7FB10019 (void);
// 0x000000EF System.String System.SR::get_Argument_PrecisionTooLarge()
extern void SR_get_Argument_PrecisionTooLarge_m42EED38BF28506133A0AB30447E3C35CCA120A7F (void);
// 0x000000F0 System.String System.SR::get_EndPositionNotReached()
extern void SR_get_EndPositionNotReached_m26E126334F58B1570EEAAA53A48B9518F3C17913 (void);
// 0x000000F1 System.Void System.SR::.cctor()
extern void SR__cctor_m4CA77DF9E538A3B432DD3F12C4D3E655983629DB (void);
// 0x000000F2 System.Int32 System.Numerics.Hashing.HashHelpers::Combine(System.Int32,System.Int32)
extern void HashHelpers_Combine_m0B422F3A90AC3CD046375C8195F8ED339B83ED46 (void);
// 0x000000F3 System.Void System.Numerics.Hashing.HashHelpers::.cctor()
extern void HashHelpers__cctor_mE4D846284DBD325D39520B0D94CE6D08B7A937E2 (void);
// 0x000000F4 System.Boolean System.Runtime.InteropServices.SequenceMarshal::TryGetString(System.Buffers.ReadOnlySequence`1<System.Char>,System.String&,System.Int32&,System.Int32&)
extern void SequenceMarshal_TryGetString_mBA81A10F83642193C6BB3862B3E847222BE88F7B (void);
// 0x000000F5 System.Boolean System.Runtime.InteropServices.MemoryMarshal::TryGetArray(System.ReadOnlyMemory`1<T>,System.ArraySegment`1<T>&)
// 0x000000F6 System.Boolean System.Runtime.InteropServices.MemoryMarshal::TryGetMemoryManager(System.ReadOnlyMemory`1<T>,TManager&,System.Int32&,System.Int32&)
// 0x000000F7 System.Boolean System.Runtime.InteropServices.MemoryMarshal::TryGetString(System.ReadOnlyMemory`1<System.Char>,System.String&,System.Int32&,System.Int32&)
extern void MemoryMarshal_TryGetString_mF59A4FBC01B0F61BFCAD2BA3960B9670FADB0C09 (void);
// 0x000000F8 T System.Runtime.InteropServices.MemoryMarshal::Read(System.ReadOnlySpan`1<System.Byte>)
// 0x000000F9 System.Void System.Runtime.InteropServices.MemoryMarshal::Write(System.Span`1<System.Byte>,T&)
// 0x000000FA System.Boolean System.Runtime.InteropServices.MemoryMarshal::TryWrite(System.Span`1<System.Byte>,T&)
// 0x000000FB System.ReadOnlySpan`1<System.Byte> System.Runtime.InteropServices.MemoryMarshal::AsBytes(System.ReadOnlySpan`1<T>)
// 0x000000FC T& System.Runtime.InteropServices.MemoryMarshal::GetReference(System.Span`1<T>)
// 0x000000FD T& System.Runtime.InteropServices.MemoryMarshal::GetReference(System.ReadOnlySpan`1<T>)
// 0x000000FE System.Span`1<TTo> System.Runtime.InteropServices.MemoryMarshal::Cast(System.Span`1<TFrom>)
// 0x000000FF System.ReadOnlySpan`1<TTo> System.Runtime.InteropServices.MemoryMarshal::Cast(System.ReadOnlySpan`1<TFrom>)
// 0x00000100 System.Void System.Buffers.BuffersExtensions::CopyTo(System.Buffers.ReadOnlySequence`1<T>&,System.Span`1<T>)
// 0x00000101 System.Void System.Buffers.BuffersExtensions::CopyToMultiSegment(System.Buffers.ReadOnlySequence`1<T>&,System.Span`1<T>)
// 0x00000102 T[] System.Buffers.BuffersExtensions::ToArray(System.Buffers.ReadOnlySequence`1<T>&)
// 0x00000103 System.Void System.Buffers.IBufferWriter`1::Advance(System.Int32)
// 0x00000104 System.Memory`1<T> System.Buffers.IBufferWriter`1::GetMemory(System.Int32)
// 0x00000105 System.Int64 System.Buffers.ReadOnlySequence`1::get_Length()
// 0x00000106 System.Boolean System.Buffers.ReadOnlySequence`1::get_IsEmpty()
// 0x00000107 System.Boolean System.Buffers.ReadOnlySequence`1::get_IsSingleSegment()
// 0x00000108 System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1::get_First()
// 0x00000109 System.SequencePosition System.Buffers.ReadOnlySequence`1::get_Start()
// 0x0000010A System.SequencePosition System.Buffers.ReadOnlySequence`1::get_End()
// 0x0000010B System.Void System.Buffers.ReadOnlySequence`1::.ctor(System.Object,System.Int32,System.Object,System.Int32)
// 0x0000010C System.Void System.Buffers.ReadOnlySequence`1::.ctor(System.Buffers.ReadOnlySequenceSegment`1<T>,System.Int32,System.Buffers.ReadOnlySequenceSegment`1<T>,System.Int32)
// 0x0000010D System.Void System.Buffers.ReadOnlySequence`1::.ctor(T[])
// 0x0000010E System.Void System.Buffers.ReadOnlySequence`1::.ctor(T[],System.Int32,System.Int32)
// 0x0000010F System.Void System.Buffers.ReadOnlySequence`1::.ctor(System.ReadOnlyMemory`1<T>)
// 0x00000110 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.Int64,System.Int64)
// 0x00000111 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.Int64,System.SequencePosition)
// 0x00000112 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.SequencePosition,System.Int64)
// 0x00000113 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.Int32,System.Int32)
// 0x00000114 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.Int32,System.SequencePosition)
// 0x00000115 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.SequencePosition,System.Int32)
// 0x00000116 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.SequencePosition,System.SequencePosition)
// 0x00000117 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.SequencePosition)
// 0x00000118 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Slice(System.Int64)
// 0x00000119 System.String System.Buffers.ReadOnlySequence`1::ToString()
// 0x0000011A System.Buffers.ReadOnlySequence`1/Enumerator<T> System.Buffers.ReadOnlySequence`1::GetEnumerator()
// 0x0000011B System.SequencePosition System.Buffers.ReadOnlySequence`1::GetPosition(System.Int64)
// 0x0000011C System.SequencePosition System.Buffers.ReadOnlySequence`1::GetPosition(System.Int64,System.SequencePosition)
// 0x0000011D System.Boolean System.Buffers.ReadOnlySequence`1::TryGet(System.SequencePosition&,System.ReadOnlyMemory`1<T>&,System.Boolean)
// 0x0000011E System.Boolean System.Buffers.ReadOnlySequence`1::TryGetBuffer(System.SequencePosition&,System.ReadOnlyMemory`1<T>&,System.SequencePosition&)
// 0x0000011F System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1::GetFirstBuffer()
// 0x00000120 System.SequencePosition System.Buffers.ReadOnlySequence`1::Seek(System.SequencePosition&,System.SequencePosition&,System.Int64,System.ExceptionArgument)
// 0x00000121 System.SequencePosition System.Buffers.ReadOnlySequence`1::SeekMultiSegment(System.Buffers.ReadOnlySequenceSegment`1<T>,System.Object,System.Int32,System.Int64,System.ExceptionArgument)
// 0x00000122 System.Void System.Buffers.ReadOnlySequence`1::BoundsCheck(System.SequencePosition&)
// 0x00000123 System.Void System.Buffers.ReadOnlySequence`1::BoundsCheck(System.UInt32,System.Object,System.UInt32,System.Object)
// 0x00000124 System.SequencePosition System.Buffers.ReadOnlySequence`1::GetEndPosition(System.Buffers.ReadOnlySequenceSegment`1<T>,System.Object,System.Int32,System.Object,System.Int32,System.Int64)
// 0x00000125 System.Buffers.ReadOnlySequence`1/SequenceType<T> System.Buffers.ReadOnlySequence`1::GetSequenceType()
// 0x00000126 System.Int32 System.Buffers.ReadOnlySequence`1::GetIndex(System.SequencePosition&)
// 0x00000127 System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::SliceImpl(System.SequencePosition&,System.SequencePosition&)
// 0x00000128 System.Int64 System.Buffers.ReadOnlySequence`1::GetLength()
// 0x00000129 System.Boolean System.Buffers.ReadOnlySequence`1::TryGetReadOnlySequenceSegment(System.Buffers.ReadOnlySequenceSegment`1<T>&,System.Int32&,System.Buffers.ReadOnlySequenceSegment`1<T>&,System.Int32&)
// 0x0000012A System.Boolean System.Buffers.ReadOnlySequence`1::TryGetArray(System.ArraySegment`1<T>&)
// 0x0000012B System.Boolean System.Buffers.ReadOnlySequence`1::TryGetString(System.String&,System.Int32&,System.Int32&)
// 0x0000012C System.Boolean System.Buffers.ReadOnlySequence`1::InRange(System.UInt32,System.UInt32,System.UInt32)
// 0x0000012D System.Boolean System.Buffers.ReadOnlySequence`1::InRange(System.UInt64,System.UInt64,System.UInt64)
// 0x0000012E System.Void System.Buffers.ReadOnlySequence`1::.cctor()
// 0x0000012F System.Void System.Buffers.ReadOnlySequence`1/Enumerator::.ctor(System.Buffers.ReadOnlySequence`1<T>&)
// 0x00000130 System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1/Enumerator::get_Current()
// 0x00000131 System.Boolean System.Buffers.ReadOnlySequence`1/Enumerator::MoveNext()
// 0x00000132 System.Int32 System.Buffers.ReadOnlySequence::SegmentToSequenceStart(System.Int32)
extern void ReadOnlySequence_SegmentToSequenceStart_mEE6CF5E985234E355289D641D4E49D4BAA9E3083 (void);
// 0x00000133 System.Int32 System.Buffers.ReadOnlySequence::SegmentToSequenceEnd(System.Int32)
extern void ReadOnlySequence_SegmentToSequenceEnd_mFFDFAE7585A75266241B8973C55046431260BBDE (void);
// 0x00000134 System.Int32 System.Buffers.ReadOnlySequence::ArrayToSequenceStart(System.Int32)
extern void ReadOnlySequence_ArrayToSequenceStart_mCEAF0855FE164270628814C10B531646278E0855 (void);
// 0x00000135 System.Int32 System.Buffers.ReadOnlySequence::ArrayToSequenceEnd(System.Int32)
extern void ReadOnlySequence_ArrayToSequenceEnd_mFF69FF0508B383A32C7EF9F89E701787D112BAB6 (void);
// 0x00000136 System.Int32 System.Buffers.ReadOnlySequence::MemoryManagerToSequenceStart(System.Int32)
extern void ReadOnlySequence_MemoryManagerToSequenceStart_mFD1015C3FF75727F14979508EE405C50AA456138 (void);
// 0x00000137 System.Int32 System.Buffers.ReadOnlySequence::MemoryManagerToSequenceEnd(System.Int32)
extern void ReadOnlySequence_MemoryManagerToSequenceEnd_m85705DE4E192CFB7B915CFDFE2DC5C47870E895D (void);
// 0x00000138 System.Int32 System.Buffers.ReadOnlySequence::StringToSequenceStart(System.Int32)
extern void ReadOnlySequence_StringToSequenceStart_mBB3640B275AF4DFA2FDBEA7CFAE5D539FB4D6B6A (void);
// 0x00000139 System.Int32 System.Buffers.ReadOnlySequence::StringToSequenceEnd(System.Int32)
extern void ReadOnlySequence_StringToSequenceEnd_m969959A7F05E83C60ECC577C1EACF3C2ECA6DE91 (void);
// 0x0000013A System.Void System.Buffers.ReadOnlySequenceDebugView`1::.ctor(System.Buffers.ReadOnlySequence`1<T>)
// 0x0000013B System.Buffers.ReadOnlySequenceDebugView`1/ReadOnlySequenceDebugViewSegments<T> System.Buffers.ReadOnlySequenceDebugView`1::get_BufferSegments()
// 0x0000013C T[] System.Buffers.ReadOnlySequenceDebugView`1::get_Items()
// 0x0000013D System.ReadOnlyMemory`1<T>[] System.Buffers.ReadOnlySequenceDebugView`1/ReadOnlySequenceDebugViewSegments::get_Segments()
// 0x0000013E System.Void System.Buffers.ReadOnlySequenceDebugView`1/ReadOnlySequenceDebugViewSegments::set_Segments(System.ReadOnlyMemory`1<T>[])
// 0x0000013F System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequenceSegment`1::get_Memory()
// 0x00000140 System.Buffers.ReadOnlySequenceSegment`1<T> System.Buffers.ReadOnlySequenceSegment`1::get_Next()
// 0x00000141 System.Int64 System.Buffers.ReadOnlySequenceSegment`1::get_RunningIndex()
// 0x00000142 System.Char System.Buffers.StandardFormat::get_Symbol()
extern void StandardFormat_get_Symbol_mF8A6168808E4EEB455E2F8695DD243231285B0F2 (void);
// 0x00000143 System.Byte System.Buffers.StandardFormat::get_Precision()
extern void StandardFormat_get_Precision_m0080A4421E19B1702147DF794C3D4C2D1035C5B6 (void);
// 0x00000144 System.Boolean System.Buffers.StandardFormat::get_HasPrecision()
extern void StandardFormat_get_HasPrecision_m512C556F8EABC1880A4057A0255D42C5008F322E (void);
// 0x00000145 System.Boolean System.Buffers.StandardFormat::get_IsDefault()
extern void StandardFormat_get_IsDefault_mA235EA7D61B74384AF56AEC5174D2ACD568AE144 (void);
// 0x00000146 System.Void System.Buffers.StandardFormat::.ctor(System.Char,System.Byte)
extern void StandardFormat__ctor_mEDC33761CDF50C5F7D01BF0DD6D673658931D43F (void);
// 0x00000147 System.Buffers.StandardFormat System.Buffers.StandardFormat::op_Implicit(System.Char)
extern void StandardFormat_op_Implicit_mC7AEDB177670024F660C1AA4BA07616FB27B29BD (void);
// 0x00000148 System.Boolean System.Buffers.StandardFormat::Equals(System.Object)
extern void StandardFormat_Equals_mB63E4B0879F9B74C0783E32117A22592050C887A (void);
// 0x00000149 System.Int32 System.Buffers.StandardFormat::GetHashCode()
extern void StandardFormat_GetHashCode_mA2398AB63B3856075F7E8F9A26D142878DDAB119 (void);
// 0x0000014A System.Boolean System.Buffers.StandardFormat::Equals(System.Buffers.StandardFormat)
extern void StandardFormat_Equals_m6AAD6931E6B7620BC5676B60FDE95DEBDCC6A011 (void);
// 0x0000014B System.String System.Buffers.StandardFormat::ToString()
extern void StandardFormat_ToString_m1391A69E60EF500E59D59A29124BAD2C1D28CE6D (void);
// 0x0000014C System.Void System.Buffers.IPinnable::Unpin()
// 0x0000014D System.Void System.Buffers.MemoryHandle::.ctor(System.Void*,System.Runtime.InteropServices.GCHandle,System.Buffers.IPinnable)
extern void MemoryHandle__ctor_mD254CBC13788969FCC315DF2B1C8615A945F18B3 (void);
// 0x0000014E System.Void System.Buffers.MemoryHandle::Dispose()
extern void MemoryHandle_Dispose_mE19418148935D11619DD13966114889837089E9A (void);
// 0x0000014F System.Memory`1<T> System.Buffers.MemoryManager`1::get_Memory()
// 0x00000150 System.Span`1<T> System.Buffers.MemoryManager`1::GetSpan()
// 0x00000151 System.Buffers.MemoryHandle System.Buffers.MemoryManager`1::Pin(System.Int32)
// 0x00000152 System.Void System.Buffers.MemoryManager`1::Unpin()
// 0x00000153 System.Boolean System.Buffers.MemoryManager`1::TryGetArray(System.ArraySegment`1<T>&)
// 0x00000154 System.Void System.Buffers.MemoryManager`1::System.IDisposable.Dispose()
// 0x00000155 System.Void System.Buffers.MemoryManager`1::Dispose(System.Boolean)
// 0x00000156 System.Buffers.OperationStatus System.Buffers.Text.Base64::DecodeFromUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&,System.Boolean)
extern void Base64_DecodeFromUtf8_m42C42D1F4690C787AD2060F743AF9016346B9AD2 (void);
// 0x00000157 System.Int32 System.Buffers.Text.Base64::GetMaxDecodedFromUtf8Length(System.Int32)
extern void Base64_GetMaxDecodedFromUtf8Length_m006256FEA6BF678C4F127180F8B1332F064F502F (void);
// 0x00000158 System.Buffers.OperationStatus System.Buffers.Text.Base64::DecodeFromUtf8InPlace(System.Span`1<System.Byte>,System.Int32&)
extern void Base64_DecodeFromUtf8InPlace_mEBB97003AC863B73FCFAF272CED3A805F3F4309E (void);
// 0x00000159 System.Int32 System.Buffers.Text.Base64::Decode(System.Byte&,System.SByte&)
extern void Base64_Decode_m9255647C748D2E5A658B306FF1CB018A3518E3EB (void);
// 0x0000015A System.Void System.Buffers.Text.Base64::WriteThreeLowOrderBytes(System.Byte&,System.Int32)
extern void Base64_WriteThreeLowOrderBytes_mEA9D70EF786F6B007B97DC740E9B2D6335A257D2 (void);
// 0x0000015B System.Buffers.OperationStatus System.Buffers.Text.Base64::EncodeToUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&,System.Boolean)
extern void Base64_EncodeToUtf8_m577D7216DE2808F843877387142BEFC4E23EC796 (void);
// 0x0000015C System.Int32 System.Buffers.Text.Base64::GetMaxEncodedToUtf8Length(System.Int32)
extern void Base64_GetMaxEncodedToUtf8Length_mCE09C90A3306E4E5812A354474AD5CBF972971A9 (void);
// 0x0000015D System.Int32 System.Buffers.Text.Base64::Encode(System.Byte&,System.Byte&)
extern void Base64_Encode_mF7004599E680812FB2DC7744AD0EE802A17B449A (void);
// 0x0000015E System.Int32 System.Buffers.Text.Base64::EncodeAndPadOne(System.Byte&,System.Byte&)
extern void Base64_EncodeAndPadOne_m2240A53CE28BE8A0A955A7E467B0E7E9A8A0DD7C (void);
// 0x0000015F System.Int32 System.Buffers.Text.Base64::EncodeAndPadTwo(System.Byte&,System.Byte&)
extern void Base64_EncodeAndPadTwo_mCC772FF69A3BEFE27CB02697DD37F08417EC1EA2 (void);
// 0x00000160 System.Void System.Buffers.Text.Base64::.cctor()
extern void Base64__cctor_m3B098734F3F8EF762B0E391849C528627DA77BED (void);
// 0x00000161 System.Void System.Buffers.Text.Utf8Constants::.cctor()
extern void Utf8Constants__cctor_m312B1DFF8FBB89F5601299D0146E152F40AA82EC (void);
// 0x00000162 System.Char System.Buffers.Text.FormattingHelpers::GetSymbolOrDefault(System.Buffers.StandardFormat&,System.Char)
extern void FormattingHelpers_GetSymbolOrDefault_m26DC14293CBF28446BFD5991B0BC6D55D199281E (void);
// 0x00000163 System.Void System.Buffers.Text.FormattingHelpers::FillWithAsciiZeros(System.Span`1<System.Byte>)
extern void FormattingHelpers_FillWithAsciiZeros_m3A29E363C6C25E65720EA654150D6199298C3A5E (void);
// 0x00000164 System.Void System.Buffers.Text.FormattingHelpers::WriteHexByte(System.Byte,System.Span`1<System.Byte>,System.Int32,System.Buffers.Text.FormattingHelpers/HexCasing)
extern void FormattingHelpers_WriteHexByte_m74A433852D66F62FF7174EF4F40B0E0EC1E7088B (void);
// 0x00000165 System.Void System.Buffers.Text.FormattingHelpers::WriteDigits(System.UInt64,System.Span`1<System.Byte>)
extern void FormattingHelpers_WriteDigits_mB84E216D8FDAAEE250F7E833F1886CF7352CD243 (void);
// 0x00000166 System.Void System.Buffers.Text.FormattingHelpers::WriteDigitsWithGroupSeparator(System.UInt64,System.Span`1<System.Byte>)
extern void FormattingHelpers_WriteDigitsWithGroupSeparator_m47961A365046CD674E315B8366DCC3A6C563F1F9 (void);
// 0x00000167 System.Void System.Buffers.Text.FormattingHelpers::WriteDigits(System.UInt32,System.Span`1<System.Byte>)
extern void FormattingHelpers_WriteDigits_mB294245C1C40A39A6FADC68FD44FDE93489329DE (void);
// 0x00000168 System.Void System.Buffers.Text.FormattingHelpers::WriteFourDecimalDigits(System.UInt32,System.Span`1<System.Byte>,System.Int32)
extern void FormattingHelpers_WriteFourDecimalDigits_m2C9CE9DD697E21513F53A059A6E8A67411C49300 (void);
// 0x00000169 System.Void System.Buffers.Text.FormattingHelpers::WriteTwoDecimalDigits(System.UInt32,System.Span`1<System.Byte>,System.Int32)
extern void FormattingHelpers_WriteTwoDecimalDigits_mBA9DF0C24B6CB962499015EC49E99779A540C95E (void);
// 0x0000016A System.Int32 System.Buffers.Text.FormattingHelpers::CountDigits(System.UInt64)
extern void FormattingHelpers_CountDigits_m675BA657779A2850D4C7B0DE8FFF5D53DCF95144 (void);
// 0x0000016B System.Int32 System.Buffers.Text.FormattingHelpers::CountDigits(System.UInt32)
extern void FormattingHelpers_CountDigits_mA676272B50F0CE205CC72EA4455298AE722FE53E (void);
// 0x0000016C System.Int32 System.Buffers.Text.FormattingHelpers::CountHexDigits(System.UInt64)
extern void FormattingHelpers_CountHexDigits_mC892DDD16C175A09E028D0CAC2F1FD4EBEFFE3BE (void);
// 0x0000016D System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.Boolean,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_m801A1D1F29220D08560601466D8F88608FB9FB28 (void);
// 0x0000016E System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.DateTimeOffset,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_mB5A5D29E1AB53EF943A7517ED41865FFD6D49E40 (void);
// 0x0000016F System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.DateTime,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_m2ABA3915E51ED1976E85ABA15699790E0D5DB675 (void);
// 0x00000170 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDateTimeG(System.DateTime,System.TimeSpan,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatDateTimeG_m73A2E1DA59D9006A5809628F5516C74FDAAAB354 (void);
// 0x00000171 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDateTimeO(System.DateTime,System.TimeSpan,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatDateTimeO_mBA169648C706917AA22CB978323C578DB48DEE28 (void);
// 0x00000172 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDateTimeR(System.DateTime,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatDateTimeR_m6F450413D5D0670CDEF033B957C41352450AEFCB (void);
// 0x00000173 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDateTimeL(System.DateTime,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatDateTimeL_m34A3FE15768F3A1B0C1CC2A746B46D40922E856A (void);
// 0x00000174 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.Decimal,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_m6791AE969A684DF1A5141E89B2748A0491B16DA1 (void);
// 0x00000175 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDecimalE(System.NumberBuffer&,System.Span`1<System.Byte>,System.Int32&,System.Byte,System.Byte)
extern void Utf8Formatter_TryFormatDecimalE_m5EE850421F5ECD53EB303F5E20F42E989BC7B679 (void);
// 0x00000176 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDecimalF(System.NumberBuffer&,System.Span`1<System.Byte>,System.Int32&,System.Byte)
extern void Utf8Formatter_TryFormatDecimalF_mB3F592FC02FDB10D2F065AAC4993754DCE801384 (void);
// 0x00000177 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatDecimalG(System.NumberBuffer&,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatDecimalG_m9209EBD3DB9F8ADBD39C4F69246BF2D2B7E2B86B (void);
// 0x00000178 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.Guid,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_mAF103F9E49F131C60F65F3ECF97732AA828E141A (void);
// 0x00000179 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.Byte,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_m9511892FD3132F76B90E0089072BDF8F11884BC4 (void);
// 0x0000017A System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.UInt64,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_mF00E41768123B7B47C2427C6327C2045B074BE0A (void);
// 0x0000017B System.Boolean System.Buffers.Text.Utf8Formatter::TryFormat(System.Int64,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormat_m3DAF77915AA8455CAFC49F4457FE35B2EC00DF3E (void);
// 0x0000017C System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64(System.Int64,System.UInt64,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormatInt64_m61A954CEB254D25C2F681CF707F80AD856F73750 (void);
// 0x0000017D System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64D(System.Int64,System.Byte,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt64D_mB8F5C4DDAA08332F924D62C0B426C1D414C4ABCA (void);
// 0x0000017E System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64Default(System.Int64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt64Default_mF4B334DF6319D35C0B33A6572F7FF0EA3CE7C2A8 (void);
// 0x0000017F System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt32MultipleDigits(System.Int32,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt32MultipleDigits_m006E2F6B25667EE3B58402D5101CE6BCA787A391 (void);
// 0x00000180 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64MultipleDigits(System.Int64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt64MultipleDigits_mB1689643467ACA69DA8BBE4FE8C9272C127D4FBB (void);
// 0x00000181 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64MoreThanNegativeBillionMaxUInt(System.Int64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt64MoreThanNegativeBillionMaxUInt_m66C6074F21CCD770F792503E660A59CCE7B0E5F7 (void);
// 0x00000182 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64LessThanNegativeBillionMaxUInt(System.Int64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt64LessThanNegativeBillionMaxUInt_m8B5E903509AF6881879B14886D005900E04B295C (void);
// 0x00000183 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatInt64N(System.Int64,System.Byte,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatInt64N_m78109B8A854CD414F3883A4F54F2828D65E03A9E (void);
// 0x00000184 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64(System.UInt64,System.Span`1<System.Byte>,System.Int32&,System.Buffers.StandardFormat)
extern void Utf8Formatter_TryFormatUInt64_m903D18A13B3D28E3301BE4AA8CF22DA85694EB3A (void);
// 0x00000185 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64D(System.UInt64,System.Byte,System.Span`1<System.Byte>,System.Boolean,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64D_m9CC93A27892C98CDC584E3A9A211BE4BCD9C47BC (void);
// 0x00000186 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64Default(System.UInt64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64Default_mC66DA768AC3B8933EA96384412E1D8CFB51D64F2 (void);
// 0x00000187 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt32SingleDigit(System.UInt32,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt32SingleDigit_m640F93E5FC560A68BD27039E9C864C9C5D582335 (void);
// 0x00000188 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt32MultipleDigits(System.UInt32,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt32MultipleDigits_m8EE0BA7509C7951049017B0E1C3BB6E911CD7863 (void);
// 0x00000189 System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64MultipleDigits(System.UInt64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64MultipleDigits_mF63FC4B51EE923E000733252A9E705D25531919B (void);
// 0x0000018A System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64LessThanBillionMaxUInt(System.UInt64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64LessThanBillionMaxUInt_mD0B30477DD0CE3DDAE2E87395EA417EE44806626 (void);
// 0x0000018B System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64MoreThanBillionMaxUInt(System.UInt64,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64MoreThanBillionMaxUInt_m906E75BFEC07873FC07898B6F41592B5F0F439A8 (void);
// 0x0000018C System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64N(System.UInt64,System.Byte,System.Span`1<System.Byte>,System.Boolean,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64N_mCF9B963233A1FD9750B0EA2F8CF833980C18826D (void);
// 0x0000018D System.Boolean System.Buffers.Text.Utf8Formatter::TryFormatUInt64X(System.UInt64,System.Byte,System.Boolean,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8Formatter_TryFormatUInt64X_m873913C215B45BE56870D7EFEFF57F558F1B79F1 (void);
// 0x0000018E System.Void System.Buffers.Text.Utf8Formatter::.cctor()
extern void Utf8Formatter__cctor_mB078801DA1AF49D670C5D6813AFECBF5C0691DCD (void);
// 0x0000018F System.Boolean System.Buffers.Text.ParserHelpers::IsDigit(System.Int32)
extern void ParserHelpers_IsDigit_m741C974EC477EA1F49439BA55D1C3142EA1D784D (void);
// 0x00000190 System.Void System.Buffers.Text.ParserHelpers::.cctor()
extern void ParserHelpers__cctor_m98371EB2AB06474B2928A3F9F4B8A598E17CEAB1 (void);
// 0x00000191 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Boolean&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m7D7E4C95A96C051D3AC51B7CA3294481DA5C3A35 (void);
// 0x00000192 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Decimal&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m90A0F8BFE5C3044C00DB5155FF340B6AF68E74FE (void);
// 0x00000193 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Single&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m83ECFE8C76A9905B80E887F9414D41A319C2AA00 (void);
// 0x00000194 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Double&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m24053FD013B5A4736E222DF29C9F6A79B13F21FE (void);
// 0x00000195 System.Boolean System.Buffers.Text.Utf8Parser::TryParseNormalAsFloatingPoint(System.ReadOnlySpan`1<System.Byte>,System.Double&,System.Int32&,System.Char)
extern void Utf8Parser_TryParseNormalAsFloatingPoint_m9B7C74F1C7BEA1EA70A86E8E98E7BE7A33366DAB (void);
// 0x00000196 System.Boolean System.Buffers.Text.Utf8Parser::TryParseAsSpecialFloatingPoint(System.ReadOnlySpan`1<System.Byte>,T,T,T,T&,System.Int32&)
// 0x00000197 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Guid&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_mAFC6FFA0BF8733D2C6156C4F7F9DDDE77528983C (void);
// 0x00000198 System.Boolean System.Buffers.Text.Utf8Parser::TryParseGuidN(System.ReadOnlySpan`1<System.Byte>,System.Guid&,System.Int32&)
extern void Utf8Parser_TryParseGuidN_mF063301B3928C30520E3DA9EBB8CD0AAEE3F921F (void);
// 0x00000199 System.Boolean System.Buffers.Text.Utf8Parser::TryParseGuidCore(System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Char,System.Char,System.Guid&,System.Int32&)
extern void Utf8Parser_TryParseGuidCore_m3B32BF426D81A189B437FC286C7EB49989A35463 (void);
// 0x0000019A System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.SByte&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_mEC708A21A90BD9288ABBD948316F13851423ABCB (void);
// 0x0000019B System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Int16&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_mBCAC213B7DBE37664B717A7BBB3165E32478AAA0 (void);
// 0x0000019C System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Int32&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_mAF1471EBA73DEBEF88625109A459CD0D0B7EDCE6 (void);
// 0x0000019D System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Int64&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m960CA85130DF531A15E65CDC72A0EB0D9CE89333 (void);
// 0x0000019E System.Boolean System.Buffers.Text.Utf8Parser::TryParseSByteD(System.ReadOnlySpan`1<System.Byte>,System.SByte&,System.Int32&)
extern void Utf8Parser_TryParseSByteD_mBDA10E37645BABA381ADB839C659F7E2D67919AE (void);
// 0x0000019F System.Boolean System.Buffers.Text.Utf8Parser::TryParseInt16D(System.ReadOnlySpan`1<System.Byte>,System.Int16&,System.Int32&)
extern void Utf8Parser_TryParseInt16D_mBF1EEA65F0A3022F85C509138EDE17C2C9544A5D (void);
// 0x000001A0 System.Boolean System.Buffers.Text.Utf8Parser::TryParseInt32D(System.ReadOnlySpan`1<System.Byte>,System.Int32&,System.Int32&)
extern void Utf8Parser_TryParseInt32D_mFD173F33096EB860929AAB5FD430804EEE5CEAB0 (void);
// 0x000001A1 System.Boolean System.Buffers.Text.Utf8Parser::TryParseInt64D(System.ReadOnlySpan`1<System.Byte>,System.Int64&,System.Int32&)
extern void Utf8Parser_TryParseInt64D_m88F7248F8771F6896EE62D8C9DC13FA432B8C578 (void);
// 0x000001A2 System.Boolean System.Buffers.Text.Utf8Parser::TryParseSByteN(System.ReadOnlySpan`1<System.Byte>,System.SByte&,System.Int32&)
extern void Utf8Parser_TryParseSByteN_m5B96DC92A5BCDF5E4F3AAC786C1635305F49860A (void);
// 0x000001A3 System.Boolean System.Buffers.Text.Utf8Parser::TryParseInt16N(System.ReadOnlySpan`1<System.Byte>,System.Int16&,System.Int32&)
extern void Utf8Parser_TryParseInt16N_mC9897FD17E244A60C51D0A679F479C3FDB1CF97C (void);
// 0x000001A4 System.Boolean System.Buffers.Text.Utf8Parser::TryParseInt32N(System.ReadOnlySpan`1<System.Byte>,System.Int32&,System.Int32&)
extern void Utf8Parser_TryParseInt32N_mB3B2357B645EBF15F4EDC2F1439F22820731545E (void);
// 0x000001A5 System.Boolean System.Buffers.Text.Utf8Parser::TryParseInt64N(System.ReadOnlySpan`1<System.Byte>,System.Int64&,System.Int32&)
extern void Utf8Parser_TryParseInt64N_m1611AB42B1D6292EBAA21283402E2A8A913683D1 (void);
// 0x000001A6 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.Byte&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m871E02CF3B4E341654E39CEAD3906176C98F479E (void);
// 0x000001A7 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.UInt16&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m982791E79287807CB4084CFA9C5616AE2AD3195B (void);
// 0x000001A8 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m0E064C38A9620714FA7D710481D8A4FF6B33E8B8 (void);
// 0x000001A9 System.Boolean System.Buffers.Text.Utf8Parser::TryParse(System.ReadOnlySpan`1<System.Byte>,System.UInt64&,System.Int32&,System.Char)
extern void Utf8Parser_TryParse_m35EB26BDA5A60061CA5D07F6977CE49C395B8E4E (void);
// 0x000001AA System.Boolean System.Buffers.Text.Utf8Parser::TryParseByteD(System.ReadOnlySpan`1<System.Byte>,System.Byte&,System.Int32&)
extern void Utf8Parser_TryParseByteD_m376276FCE8C1FA0C657D2DD34E43761B807CC55F (void);
// 0x000001AB System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt16D(System.ReadOnlySpan`1<System.Byte>,System.UInt16&,System.Int32&)
extern void Utf8Parser_TryParseUInt16D_m74190A035D81353E29530A4214FDD097DF5E815F (void);
// 0x000001AC System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt32D(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
extern void Utf8Parser_TryParseUInt32D_m5F723DCD7B0E8614C4BC8DC76816B62AAFED195E (void);
// 0x000001AD System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt64D(System.ReadOnlySpan`1<System.Byte>,System.UInt64&,System.Int32&)
extern void Utf8Parser_TryParseUInt64D_mC99A2680DB62D181EA8E3F95897534A6EC1E13D6 (void);
// 0x000001AE System.Boolean System.Buffers.Text.Utf8Parser::TryParseByteN(System.ReadOnlySpan`1<System.Byte>,System.Byte&,System.Int32&)
extern void Utf8Parser_TryParseByteN_mBDB9BB1D68D98E31FFF2100709F739233E1387E6 (void);
// 0x000001AF System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt16N(System.ReadOnlySpan`1<System.Byte>,System.UInt16&,System.Int32&)
extern void Utf8Parser_TryParseUInt16N_m11C73A328EEF91EDFF923F1260353E2484ACA045 (void);
// 0x000001B0 System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt32N(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
extern void Utf8Parser_TryParseUInt32N_mFC1CFB606F749984EA6D59C6F9AC11169001F092 (void);
// 0x000001B1 System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt64N(System.ReadOnlySpan`1<System.Byte>,System.UInt64&,System.Int32&)
extern void Utf8Parser_TryParseUInt64N_m63810A3876355C4D37E833FB9346C70518943733 (void);
// 0x000001B2 System.Boolean System.Buffers.Text.Utf8Parser::TryParseByteX(System.ReadOnlySpan`1<System.Byte>,System.Byte&,System.Int32&)
extern void Utf8Parser_TryParseByteX_mF0B28DB6A2F5DB17CBE1B931D51E2AEDE441ED99 (void);
// 0x000001B3 System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt16X(System.ReadOnlySpan`1<System.Byte>,System.UInt16&,System.Int32&)
extern void Utf8Parser_TryParseUInt16X_mD5F4C39EA831AEA324D505F21B6934B6CB0D7501 (void);
// 0x000001B4 System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt32X(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
extern void Utf8Parser_TryParseUInt32X_m95FB8E312302F97F801E9A6EE4ACEFB27F4F8DB4 (void);
// 0x000001B5 System.Boolean System.Buffers.Text.Utf8Parser::TryParseUInt64X(System.ReadOnlySpan`1<System.Byte>,System.UInt64&,System.Int32&)
extern void Utf8Parser_TryParseUInt64X_mD4A57526AF186501CCC19166EA60395A49ADC507 (void);
// 0x000001B6 System.Boolean System.Buffers.Text.Utf8Parser::TryParseNumber(System.ReadOnlySpan`1<System.Byte>,System.NumberBuffer&,System.Int32&,System.Buffers.Text.Utf8Parser/ParseNumberOptions,System.Boolean&)
extern void Utf8Parser_TryParseNumber_m18BE8E49AA5F029B6111509C59D74FFA0F35E38C (void);
// 0x000001B7 System.Void System.Buffers.Text.Utf8Parser::.cctor()
extern void Utf8Parser__cctor_m60590923BE3CD9DCD3E31445BC4773C99A9EBF61 (void);
// 0x000001B8 System.UInt32 System.Buffers.Binary.BinaryPrimitives::ReverseEndianness(System.UInt32)
extern void BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20 (void);
// 0x000001B9 System.UInt32 System.Buffers.Binary.BinaryPrimitives::ReadUInt32LittleEndian(System.ReadOnlySpan`1<System.Byte>)
extern void BinaryPrimitives_ReadUInt32LittleEndian_mEE46641BC73CAACA64F2952CD791BE96F5DB44F4 (void);
// 0x000001BA System.Void System.Buffers.Binary.BinaryPrimitives::WriteUInt32BigEndian(System.Span`1<System.Byte>,System.UInt32)
extern void BinaryPrimitives_WriteUInt32BigEndian_m9EB6A671EA25CC201F502FA157F3B0514364714D (void);
// 0x000001BB System.Boolean System.Buffers.Binary.BinaryPrimitives::TryWriteUInt32BigEndian(System.Span`1<System.Byte>,System.UInt32)
extern void BinaryPrimitives_TryWriteUInt32BigEndian_mBC99343DFDA11632132FB4E6A8E355478F251310 (void);
static Il2CppMethodPointer s_methodPointers[443] = 
{
	EmbeddedAttribute__ctor_m7D7D024DA2EA05AEDF8B8C470F678A5DA96C8EB8,
	IsReadOnlyAttribute__ctor_m07A8C937D13DE79AF8ED555F18E5AE9FDA6C3879,
	IsByRefLikeAttribute__ctor_m3F813C04C0FAF02B5AF712ED98929300CD6E44DD,
	SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0,
	SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525,
	SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE,
	SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795,
	SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58,
	SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2,
	ThrowHelper_ThrowArgumentNullException_m3B4D674B817C817E97F4687F0130007D315F8B34,
	ThrowHelper_CreateArgumentNullException_mA70D942EBA7503962BA72170F026A966513590FC,
	ThrowHelper_ThrowArrayTypeMismatchException_mFC0D7756FD2EA1A7E41D8426D819369FDBD728FC,
	ThrowHelper_CreateArrayTypeMismatchException_m23F27BF82F951A64682A2CF14E0EDE9F3B54C93F,
	ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4,
	ThrowHelper_CreateArgumentException_InvalidTypeWithPointersNotSupported_m4019588ED8511C985604C8CC9AD4AB6414676945,
	ThrowHelper_ThrowArgumentException_DestinationTooShort_mD9C82D6A62948DA443166283990BF760F77C76C8,
	ThrowHelper_CreateArgumentException_DestinationTooShort_m75CF4B3D7F56B0383E0BC84D86C085AA0CE90CD1,
	ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC,
	ThrowHelper_CreateIndexOutOfRangeException_m8C8886676269B09CC5241BA6F5330D78B26F527B,
	ThrowHelper_ThrowArgumentOutOfRangeException_mB72471F11341E214DA380AF2B87C3F28EC51CA59,
	ThrowHelper_CreateArgumentOutOfRangeException_m0841E9BF864372D7BF0512A13456F985C53FC03D,
	ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5,
	ThrowHelper_CreateArgumentOutOfRangeException_m3ED3DA6D593699354BA4D397790440F3BFE84AEA,
	ThrowHelper_ThrowArgumentOutOfRangeException_PrecisionTooLarge_mAC345A1F72DBC919354CCB54CC6C24EF44BE48C1,
	ThrowHelper_CreateArgumentOutOfRangeException_PrecisionTooLarge_mC1889FF89FD22816EB8D105C942166D0BF6ADFAD,
	ThrowHelper_ThrowArgumentOutOfRangeException_SymbolDoesNotFit_m601A3BCC469FE8A7420CC33708307189B6C48B61,
	ThrowHelper_CreateArgumentOutOfRangeException_SymbolDoesNotFit_m903F8FB8357FE1BAB8BF450E0F95A79117EF2D19,
	ThrowHelper_ThrowInvalidOperationException_mC18897E999FE00AE3ACC3543A468201068A217F8,
	ThrowHelper_CreateInvalidOperationException_mAA36D488898B83C836372B8D095EFEC4136121B3,
	ThrowHelper_ThrowInvalidOperationException_EndPositionNotReached_m67B5DCC8C43494E0A491781D118E147337664DB0,
	ThrowHelper_CreateInvalidOperationException_EndPositionNotReached_m35A30B605551B8CACAE6B842C8B525BC7078FE72,
	ThrowHelper_ThrowArgumentOutOfRangeException_PositionOutOfRange_mE66B589C0CE79CD3252C96D133A4DA6BFF64228D,
	ThrowHelper_CreateArgumentOutOfRangeException_PositionOutOfRange_m09B8EF6F30DDB19BF9AD63605556AED12E7DE03A,
	ThrowHelper_ThrowArgumentOutOfRangeException_OffsetOutOfRange_m33BB375A469C5D43F27FAF1E1573659C8D6D46B6,
	ThrowHelper_CreateArgumentOutOfRangeException_OffsetOutOfRange_m4006B68C0940F1864B48E9E2CAE0A62FE91910F9,
	ThrowHelper_ThrowFormatException_BadFormatSpecifier_m1E35DA4BEAAC3A721CFB9CE82029EBABF763EA8D,
	ThrowHelper_CreateFormatException_BadFormatSpecifier_mA1D2DB12AB680960531A3C1A97098925F5154FDB,
	ThrowHelper_TryFormatThrowFormatException_mAE469FD4BD034BFD4FB18C512DAD188188AADFCA,
	NULL,
	NULL,
	NULL,
	ThrowHelper_ThrowArgumentValidationException_m02E99F21139D9C2446BCF31922E6B9A1FB6CBFE9,
	ThrowHelper_CreateArgumentValidationException_mA1BAB2087749EF0A3433426D6B4886903B64C893,
	ThrowHelper_ThrowStartOrEndArgumentValidationException_m0BB9EBD3A2B133B27075024CE04996C5626E7C38,
	ThrowHelper_CreateStartOrEndArgumentValidationException_mC18923AF89FCBE538463BC90C517FA9E4691734C,
	DecimalDecCalc_D32DivMod1E9_mAD8A67341D4FAFC5C7B8166220023AA68003ABBF,
	DecimalDecCalc_DecDivMod1E9_mEBD288904D9655A6FFF62BA3620C6C55FF28B8B6,
	DecimalDecCalc_DecAddInt32_mCA42EB3D01D859FA31D304138E4BEF39FEB549AE,
	DecimalDecCalc_D32AddCarry_m12237332B16193F0B942AEFC955A62400D3F51B9,
	DecimalDecCalc_DecMul10_m937563702D52CBD29F01CC75B62F626BEC117892,
	DecimalDecCalc_DecShiftLeft_m63A7A170CA5CF4A864BAAD95D88C299D5AFB71F3,
	DecimalDecCalc_DecAdd_m52E651ADFE94ACFD0CDDC8C14A50D2830E46E3D5,
	Number_RoundNumber_mEC3B9B63F68460A64F6F6913BB80F3BEBB780F07,
	Number_NumberBufferToDouble_mA943AD6BA0AE626079DBC29F9B659D26D4E1D424,
	Number_NumberBufferToDecimal_m9EC553B819A7A5F0260E71EE5684E7EA7CEE6D42,
	Number_DecimalToNumber_mDB370C6FEDCCE206F35651207AE2E13ABF525408,
	Number_DigitsToInt_mF9D8FE36B186C206448E7489A7AFC2EDEF604BD2,
	Number_Mul32x32To64_mE1E983CFD213FF07619AEDA39C30700B6213EE13,
	Number_Mul64Lossy_m62FA4C0FD0DFB698403843BF67DFED5D0881DD9D,
	Number_abs_m602E755F75A75A2F98CFEB174046DA2213A30DB5,
	Number_NumberToDouble_m8DE02F8F5A4B55ED6EC5995419C9D4AE9BB0802E,
	Number__cctor_m13165024FAE32EB10AAF002D913A479A756870CB,
	DoubleHelper_Exponent_m030E2C0AF265E2CA0ACEA5E4C9E66C1EAB462D5C,
	DoubleHelper_Mantissa_m5275E987A447C0B2F0F10D1E08B0A73DB121A6BE,
	NumberBuffer_get_Digits_m77F55E994E325F745CBEE5598D24263834F34F3B,
	NumberBuffer_get_UnsafeDigits_m95B0D74B60926B0E8788E7E6FDC551E7240F40FE,
	NumberBuffer_get_NumDigits_mC0AF0400D548D41907EA3204965C78A10DB463D5,
	NumberBuffer_ToString_m64B3ED10515B329DDED1167F97E5BE0B297955AD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemoryExtensions_AsSpan_m7527C7806D1DD24C012DC60C12280A9E1AEA8F15,
	MemoryExtensions_AsMemory_mDE1615649DE907A3C22F6BEBBF0C8F80E4652B0D,
	MemoryExtensions_AsMemory_m9F2378B1710076CA61B1FC3E687A06BC6063A9DC,
	MemoryExtensions_MeasureStringAdjustment_m8E2719E3CCAD24803BEF8B9C9873DDFAA528C762,
	MemoryExtensions__cctor_mC634116818572F66DC5A4416FB29AFBFCE859EBE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpanHelpers_IndexOf_m996881920138B2EC72C814473789D6AB958B92F2,
	SpanHelpers_IndexOfAny_m08EBFBAA90222E41BD0E0BECA675C836CDF8B459,
	SpanHelpers_SequenceEqual_mDEB0F358BB173EA24BEEB0609454A997E9273A89,
	SpanHelpers_LocateFirstFoundByte_m419FAFA78D34AB13ADED0E04C54AD68D69551E83,
	SpanHelpers_LocateFirstFoundByte_m6AEEBF64B95585D577D0041CE56E0BE6F28AEFE4,
	SpanHelpers_GetVector_mB6C94E0B80F7E5A77D84E2B202C1B8DD742E9FD9,
	SpanHelpers_IndexOf_m0740DDBBE5723E3595EADF2552551F636C18A259,
	SpanHelpers_LocateFirstFoundChar_m6F7002BE58BB1D79412002F2AEF2E430F8720F97,
	SpanHelpers_LocateFirstFoundChar_m7B3D3FD47EB5BA8837CE3E8CE2D2BBA7BFC62CE3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpanHelpers_IsReferenceOrContainsReferencesCore_m4046A8EAD00DA02AA423C292A8FCBB08268AD781,
	SpanHelpers_ClearLessThanPointerSized_m257390BAE1A54335F742BD17D85AF6D8FC03C831,
	SpanHelpers_ClearLessThanPointerSized_mDD75E922D42E70B6F76DB1A1EC1A96F59CAFF0B5,
	SpanHelpers_ClearPointerSizedWithoutReferences_mC6EF2B959C4B0E58F8D4B8C9A5EF341F948FFAAA,
	SpanHelpers_ClearPointerSizedWithReferences_m45CDDDFAE259A9678B759645C7AB467860D44BAE,
	SpanHelpers_LessThanEqual_mCFA5E9CC05F428B1EDA65967FEF81267F917E88C,
	NULL,
	NULL,
	NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E,
	NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973,
	NUInt_op_Explicit_m680513883587956D1452B1EB6D321D4C3A0C8366,
	NUInt_op_Explicit_mAC8186F05FC1F16BAEB9A73957491CB24A067D46,
	NUInt_op_Multiply_mABFB3E10A51F74FDC0CD9B799B7BF35C2C5D8D85,
	MutableDecimal_get_IsNegative_m6CC9630C1FE5DAABD29CEE9EF5281C37D12CE702,
	MutableDecimal_set_IsNegative_mF373061A5BA3F192A2AA544BCB933C81BF71AC67,
	MutableDecimal_get_Scale_mD47D52938E7026D2EC3AA837BABF7162C4F727A3,
	MutableDecimal_set_Scale_m9253E0BBFF59D428FF76EA0A530D644053C3075C,
	SR_get_ResourceManager_m18A791F4D611559D5B214B3020BAB11F2AC869EC,
	SR_UsingResourceKeys_m08DBDDDDF80E9F0013615CAB611F552F836BB526,
	SR_GetResourceString_mEC79B3C28B26B1540E26C3CD899938CC955A4748,
	SR_Format_m4480ECD777F2A905A368094827DDCB43478A8053,
	SR_get_ResourceType_mA677195FD1721150495B84739EFFDCB9366A5541,
	SR_get_NotSupported_CannotCallEqualsOnSpan_mACE24A88A0ADF9880C315FDC0963BA17E66B0394,
	SR_get_NotSupported_CannotCallGetHashCodeOnSpan_m4BC3D1B6994913E69BDD4028026F18A279A9DBDB,
	SR_get_Argument_InvalidTypeWithPointersNotSupported_m2FD2DCBFF1853C8F9616D4C55DD1C14163A06B75,
	SR_get_Argument_DestinationTooShort_mDD536A55FFA1BD1CF5C34D9E074420C183905559,
	SR_get_Argument_BadFormatSpecifier_mFE81E4F926274AB402B890E679E6CAB600E61433,
	SR_get_Argument_GWithPrecisionNotSupported_mF77D1EF96FE22465E62C65C5895E968E7FB10019,
	SR_get_Argument_PrecisionTooLarge_m42EED38BF28506133A0AB30447E3C35CCA120A7F,
	SR_get_EndPositionNotReached_m26E126334F58B1570EEAAA53A48B9518F3C17913,
	SR__cctor_m4CA77DF9E538A3B432DD3F12C4D3E655983629DB,
	HashHelpers_Combine_m0B422F3A90AC3CD046375C8195F8ED339B83ED46,
	HashHelpers__cctor_mE4D846284DBD325D39520B0D94CE6D08B7A937E2,
	SequenceMarshal_TryGetString_mBA81A10F83642193C6BB3862B3E847222BE88F7B,
	NULL,
	NULL,
	MemoryMarshal_TryGetString_mF59A4FBC01B0F61BFCAD2BA3960B9670FADB0C09,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReadOnlySequence_SegmentToSequenceStart_mEE6CF5E985234E355289D641D4E49D4BAA9E3083,
	ReadOnlySequence_SegmentToSequenceEnd_mFFDFAE7585A75266241B8973C55046431260BBDE,
	ReadOnlySequence_ArrayToSequenceStart_mCEAF0855FE164270628814C10B531646278E0855,
	ReadOnlySequence_ArrayToSequenceEnd_mFF69FF0508B383A32C7EF9F89E701787D112BAB6,
	ReadOnlySequence_MemoryManagerToSequenceStart_mFD1015C3FF75727F14979508EE405C50AA456138,
	ReadOnlySequence_MemoryManagerToSequenceEnd_m85705DE4E192CFB7B915CFDFE2DC5C47870E895D,
	ReadOnlySequence_StringToSequenceStart_mBB3640B275AF4DFA2FDBEA7CFAE5D539FB4D6B6A,
	ReadOnlySequence_StringToSequenceEnd_m969959A7F05E83C60ECC577C1EACF3C2ECA6DE91,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StandardFormat_get_Symbol_mF8A6168808E4EEB455E2F8695DD243231285B0F2,
	StandardFormat_get_Precision_m0080A4421E19B1702147DF794C3D4C2D1035C5B6,
	StandardFormat_get_HasPrecision_m512C556F8EABC1880A4057A0255D42C5008F322E,
	StandardFormat_get_IsDefault_mA235EA7D61B74384AF56AEC5174D2ACD568AE144,
	StandardFormat__ctor_mEDC33761CDF50C5F7D01BF0DD6D673658931D43F,
	StandardFormat_op_Implicit_mC7AEDB177670024F660C1AA4BA07616FB27B29BD,
	StandardFormat_Equals_mB63E4B0879F9B74C0783E32117A22592050C887A,
	StandardFormat_GetHashCode_mA2398AB63B3856075F7E8F9A26D142878DDAB119,
	StandardFormat_Equals_m6AAD6931E6B7620BC5676B60FDE95DEBDCC6A011,
	StandardFormat_ToString_m1391A69E60EF500E59D59A29124BAD2C1D28CE6D,
	NULL,
	MemoryHandle__ctor_mD254CBC13788969FCC315DF2B1C8615A945F18B3,
	MemoryHandle_Dispose_mE19418148935D11619DD13966114889837089E9A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Base64_DecodeFromUtf8_m42C42D1F4690C787AD2060F743AF9016346B9AD2,
	Base64_GetMaxDecodedFromUtf8Length_m006256FEA6BF678C4F127180F8B1332F064F502F,
	Base64_DecodeFromUtf8InPlace_mEBB97003AC863B73FCFAF272CED3A805F3F4309E,
	Base64_Decode_m9255647C748D2E5A658B306FF1CB018A3518E3EB,
	Base64_WriteThreeLowOrderBytes_mEA9D70EF786F6B007B97DC740E9B2D6335A257D2,
	Base64_EncodeToUtf8_m577D7216DE2808F843877387142BEFC4E23EC796,
	Base64_GetMaxEncodedToUtf8Length_mCE09C90A3306E4E5812A354474AD5CBF972971A9,
	Base64_Encode_mF7004599E680812FB2DC7744AD0EE802A17B449A,
	Base64_EncodeAndPadOne_m2240A53CE28BE8A0A955A7E467B0E7E9A8A0DD7C,
	Base64_EncodeAndPadTwo_mCC772FF69A3BEFE27CB02697DD37F08417EC1EA2,
	Base64__cctor_m3B098734F3F8EF762B0E391849C528627DA77BED,
	Utf8Constants__cctor_m312B1DFF8FBB89F5601299D0146E152F40AA82EC,
	FormattingHelpers_GetSymbolOrDefault_m26DC14293CBF28446BFD5991B0BC6D55D199281E,
	FormattingHelpers_FillWithAsciiZeros_m3A29E363C6C25E65720EA654150D6199298C3A5E,
	FormattingHelpers_WriteHexByte_m74A433852D66F62FF7174EF4F40B0E0EC1E7088B,
	FormattingHelpers_WriteDigits_mB84E216D8FDAAEE250F7E833F1886CF7352CD243,
	FormattingHelpers_WriteDigitsWithGroupSeparator_m47961A365046CD674E315B8366DCC3A6C563F1F9,
	FormattingHelpers_WriteDigits_mB294245C1C40A39A6FADC68FD44FDE93489329DE,
	FormattingHelpers_WriteFourDecimalDigits_m2C9CE9DD697E21513F53A059A6E8A67411C49300,
	FormattingHelpers_WriteTwoDecimalDigits_mBA9DF0C24B6CB962499015EC49E99779A540C95E,
	FormattingHelpers_CountDigits_m675BA657779A2850D4C7B0DE8FFF5D53DCF95144,
	FormattingHelpers_CountDigits_mA676272B50F0CE205CC72EA4455298AE722FE53E,
	FormattingHelpers_CountHexDigits_mC892DDD16C175A09E028D0CAC2F1FD4EBEFFE3BE,
	Utf8Formatter_TryFormat_m801A1D1F29220D08560601466D8F88608FB9FB28,
	Utf8Formatter_TryFormat_mB5A5D29E1AB53EF943A7517ED41865FFD6D49E40,
	Utf8Formatter_TryFormat_m2ABA3915E51ED1976E85ABA15699790E0D5DB675,
	Utf8Formatter_TryFormatDateTimeG_m73A2E1DA59D9006A5809628F5516C74FDAAAB354,
	Utf8Formatter_TryFormatDateTimeO_mBA169648C706917AA22CB978323C578DB48DEE28,
	Utf8Formatter_TryFormatDateTimeR_m6F450413D5D0670CDEF033B957C41352450AEFCB,
	Utf8Formatter_TryFormatDateTimeL_m34A3FE15768F3A1B0C1CC2A746B46D40922E856A,
	Utf8Formatter_TryFormat_m6791AE969A684DF1A5141E89B2748A0491B16DA1,
	Utf8Formatter_TryFormatDecimalE_m5EE850421F5ECD53EB303F5E20F42E989BC7B679,
	Utf8Formatter_TryFormatDecimalF_mB3F592FC02FDB10D2F065AAC4993754DCE801384,
	Utf8Formatter_TryFormatDecimalG_m9209EBD3DB9F8ADBD39C4F69246BF2D2B7E2B86B,
	Utf8Formatter_TryFormat_mAF103F9E49F131C60F65F3ECF97732AA828E141A,
	Utf8Formatter_TryFormat_m9511892FD3132F76B90E0089072BDF8F11884BC4,
	Utf8Formatter_TryFormat_mF00E41768123B7B47C2427C6327C2045B074BE0A,
	Utf8Formatter_TryFormat_m3DAF77915AA8455CAFC49F4457FE35B2EC00DF3E,
	Utf8Formatter_TryFormatInt64_m61A954CEB254D25C2F681CF707F80AD856F73750,
	Utf8Formatter_TryFormatInt64D_mB8F5C4DDAA08332F924D62C0B426C1D414C4ABCA,
	Utf8Formatter_TryFormatInt64Default_mF4B334DF6319D35C0B33A6572F7FF0EA3CE7C2A8,
	Utf8Formatter_TryFormatInt32MultipleDigits_m006E2F6B25667EE3B58402D5101CE6BCA787A391,
	Utf8Formatter_TryFormatInt64MultipleDigits_mB1689643467ACA69DA8BBE4FE8C9272C127D4FBB,
	Utf8Formatter_TryFormatInt64MoreThanNegativeBillionMaxUInt_m66C6074F21CCD770F792503E660A59CCE7B0E5F7,
	Utf8Formatter_TryFormatInt64LessThanNegativeBillionMaxUInt_m8B5E903509AF6881879B14886D005900E04B295C,
	Utf8Formatter_TryFormatInt64N_m78109B8A854CD414F3883A4F54F2828D65E03A9E,
	Utf8Formatter_TryFormatUInt64_m903D18A13B3D28E3301BE4AA8CF22DA85694EB3A,
	Utf8Formatter_TryFormatUInt64D_m9CC93A27892C98CDC584E3A9A211BE4BCD9C47BC,
	Utf8Formatter_TryFormatUInt64Default_mC66DA768AC3B8933EA96384412E1D8CFB51D64F2,
	Utf8Formatter_TryFormatUInt32SingleDigit_m640F93E5FC560A68BD27039E9C864C9C5D582335,
	Utf8Formatter_TryFormatUInt32MultipleDigits_m8EE0BA7509C7951049017B0E1C3BB6E911CD7863,
	Utf8Formatter_TryFormatUInt64MultipleDigits_mF63FC4B51EE923E000733252A9E705D25531919B,
	Utf8Formatter_TryFormatUInt64LessThanBillionMaxUInt_mD0B30477DD0CE3DDAE2E87395EA417EE44806626,
	Utf8Formatter_TryFormatUInt64MoreThanBillionMaxUInt_m906E75BFEC07873FC07898B6F41592B5F0F439A8,
	Utf8Formatter_TryFormatUInt64N_mCF9B963233A1FD9750B0EA2F8CF833980C18826D,
	Utf8Formatter_TryFormatUInt64X_m873913C215B45BE56870D7EFEFF57F558F1B79F1,
	Utf8Formatter__cctor_mB078801DA1AF49D670C5D6813AFECBF5C0691DCD,
	ParserHelpers_IsDigit_m741C974EC477EA1F49439BA55D1C3142EA1D784D,
	ParserHelpers__cctor_m98371EB2AB06474B2928A3F9F4B8A598E17CEAB1,
	Utf8Parser_TryParse_m7D7E4C95A96C051D3AC51B7CA3294481DA5C3A35,
	Utf8Parser_TryParse_m90A0F8BFE5C3044C00DB5155FF340B6AF68E74FE,
	Utf8Parser_TryParse_m83ECFE8C76A9905B80E887F9414D41A319C2AA00,
	Utf8Parser_TryParse_m24053FD013B5A4736E222DF29C9F6A79B13F21FE,
	Utf8Parser_TryParseNormalAsFloatingPoint_m9B7C74F1C7BEA1EA70A86E8E98E7BE7A33366DAB,
	NULL,
	Utf8Parser_TryParse_mAFC6FFA0BF8733D2C6156C4F7F9DDDE77528983C,
	Utf8Parser_TryParseGuidN_mF063301B3928C30520E3DA9EBB8CD0AAEE3F921F,
	Utf8Parser_TryParseGuidCore_m3B32BF426D81A189B437FC286C7EB49989A35463,
	Utf8Parser_TryParse_mEC708A21A90BD9288ABBD948316F13851423ABCB,
	Utf8Parser_TryParse_mBCAC213B7DBE37664B717A7BBB3165E32478AAA0,
	Utf8Parser_TryParse_mAF1471EBA73DEBEF88625109A459CD0D0B7EDCE6,
	Utf8Parser_TryParse_m960CA85130DF531A15E65CDC72A0EB0D9CE89333,
	Utf8Parser_TryParseSByteD_mBDA10E37645BABA381ADB839C659F7E2D67919AE,
	Utf8Parser_TryParseInt16D_mBF1EEA65F0A3022F85C509138EDE17C2C9544A5D,
	Utf8Parser_TryParseInt32D_mFD173F33096EB860929AAB5FD430804EEE5CEAB0,
	Utf8Parser_TryParseInt64D_m88F7248F8771F6896EE62D8C9DC13FA432B8C578,
	Utf8Parser_TryParseSByteN_m5B96DC92A5BCDF5E4F3AAC786C1635305F49860A,
	Utf8Parser_TryParseInt16N_mC9897FD17E244A60C51D0A679F479C3FDB1CF97C,
	Utf8Parser_TryParseInt32N_mB3B2357B645EBF15F4EDC2F1439F22820731545E,
	Utf8Parser_TryParseInt64N_m1611AB42B1D6292EBAA21283402E2A8A913683D1,
	Utf8Parser_TryParse_m871E02CF3B4E341654E39CEAD3906176C98F479E,
	Utf8Parser_TryParse_m982791E79287807CB4084CFA9C5616AE2AD3195B,
	Utf8Parser_TryParse_m0E064C38A9620714FA7D710481D8A4FF6B33E8B8,
	Utf8Parser_TryParse_m35EB26BDA5A60061CA5D07F6977CE49C395B8E4E,
	Utf8Parser_TryParseByteD_m376276FCE8C1FA0C657D2DD34E43761B807CC55F,
	Utf8Parser_TryParseUInt16D_m74190A035D81353E29530A4214FDD097DF5E815F,
	Utf8Parser_TryParseUInt32D_m5F723DCD7B0E8614C4BC8DC76816B62AAFED195E,
	Utf8Parser_TryParseUInt64D_mC99A2680DB62D181EA8E3F95897534A6EC1E13D6,
	Utf8Parser_TryParseByteN_mBDB9BB1D68D98E31FFF2100709F739233E1387E6,
	Utf8Parser_TryParseUInt16N_m11C73A328EEF91EDFF923F1260353E2484ACA045,
	Utf8Parser_TryParseUInt32N_mFC1CFB606F749984EA6D59C6F9AC11169001F092,
	Utf8Parser_TryParseUInt64N_m63810A3876355C4D37E833FB9346C70518943733,
	Utf8Parser_TryParseByteX_mF0B28DB6A2F5DB17CBE1B931D51E2AEDE441ED99,
	Utf8Parser_TryParseUInt16X_mD5F4C39EA831AEA324D505F21B6934B6CB0D7501,
	Utf8Parser_TryParseUInt32X_m95FB8E312302F97F801E9A6EE4ACEFB27F4F8DB4,
	Utf8Parser_TryParseUInt64X_mD4A57526AF186501CCC19166EA60395A49ADC507,
	Utf8Parser_TryParseNumber_m18BE8E49AA5F029B6111509C59D74FFA0F35E38C,
	Utf8Parser__cctor_m60590923BE3CD9DCD3E31445BC4773C99A9EBF61,
	BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20,
	BinaryPrimitives_ReadUInt32LittleEndian_mEE46641BC73CAACA64F2952CD791BE96F5DB44F4,
	BinaryPrimitives_WriteUInt32BigEndian_m9EB6A671EA25CC201F502FA157F3B0514364714D,
	BinaryPrimitives_TryWriteUInt32BigEndian_mBC99343DFDA11632132FB4E6A8E355478F251310,
};
extern void SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0_AdjustorThunk (void);
extern void SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525_AdjustorThunk (void);
extern void SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE_AdjustorThunk (void);
extern void SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795_AdjustorThunk (void);
extern void SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58_AdjustorThunk (void);
extern void SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2_AdjustorThunk (void);
extern void NumberBuffer_get_Digits_m77F55E994E325F745CBEE5598D24263834F34F3B_AdjustorThunk (void);
extern void NumberBuffer_get_UnsafeDigits_m95B0D74B60926B0E8788E7E6FDC551E7240F40FE_AdjustorThunk (void);
extern void NumberBuffer_get_NumDigits_mC0AF0400D548D41907EA3204965C78A10DB463D5_AdjustorThunk (void);
extern void NumberBuffer_ToString_m64B3ED10515B329DDED1167F97E5BE0B297955AD_AdjustorThunk (void);
extern void NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E_AdjustorThunk (void);
extern void NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973_AdjustorThunk (void);
extern void MutableDecimal_get_IsNegative_m6CC9630C1FE5DAABD29CEE9EF5281C37D12CE702_AdjustorThunk (void);
extern void MutableDecimal_set_IsNegative_mF373061A5BA3F192A2AA544BCB933C81BF71AC67_AdjustorThunk (void);
extern void MutableDecimal_get_Scale_mD47D52938E7026D2EC3AA837BABF7162C4F727A3_AdjustorThunk (void);
extern void MutableDecimal_set_Scale_m9253E0BBFF59D428FF76EA0A530D644053C3075C_AdjustorThunk (void);
extern void StandardFormat_get_Symbol_mF8A6168808E4EEB455E2F8695DD243231285B0F2_AdjustorThunk (void);
extern void StandardFormat_get_Precision_m0080A4421E19B1702147DF794C3D4C2D1035C5B6_AdjustorThunk (void);
extern void StandardFormat_get_HasPrecision_m512C556F8EABC1880A4057A0255D42C5008F322E_AdjustorThunk (void);
extern void StandardFormat_get_IsDefault_mA235EA7D61B74384AF56AEC5174D2ACD568AE144_AdjustorThunk (void);
extern void StandardFormat__ctor_mEDC33761CDF50C5F7D01BF0DD6D673658931D43F_AdjustorThunk (void);
extern void StandardFormat_Equals_mB63E4B0879F9B74C0783E32117A22592050C887A_AdjustorThunk (void);
extern void StandardFormat_GetHashCode_mA2398AB63B3856075F7E8F9A26D142878DDAB119_AdjustorThunk (void);
extern void StandardFormat_Equals_m6AAD6931E6B7620BC5676B60FDE95DEBDCC6A011_AdjustorThunk (void);
extern void StandardFormat_ToString_m1391A69E60EF500E59D59A29124BAD2C1D28CE6D_AdjustorThunk (void);
extern void MemoryHandle__ctor_mD254CBC13788969FCC315DF2B1C8615A945F18B3_AdjustorThunk (void);
extern void MemoryHandle_Dispose_mE19418148935D11619DD13966114889837089E9A_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[27] = 
{
	{ 0x06000004, SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0_AdjustorThunk },
	{ 0x06000005, SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525_AdjustorThunk },
	{ 0x06000006, SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE_AdjustorThunk },
	{ 0x06000007, SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795_AdjustorThunk },
	{ 0x06000008, SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58_AdjustorThunk },
	{ 0x06000009, SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2_AdjustorThunk },
	{ 0x06000041, NumberBuffer_get_Digits_m77F55E994E325F745CBEE5598D24263834F34F3B_AdjustorThunk },
	{ 0x06000042, NumberBuffer_get_UnsafeDigits_m95B0D74B60926B0E8788E7E6FDC551E7240F40FE_AdjustorThunk },
	{ 0x06000043, NumberBuffer_get_NumDigits_mC0AF0400D548D41907EA3204965C78A10DB463D5_AdjustorThunk },
	{ 0x06000044, NumberBuffer_ToString_m64B3ED10515B329DDED1167F97E5BE0B297955AD_AdjustorThunk },
	{ 0x060000DB, NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E_AdjustorThunk },
	{ 0x060000DC, NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973_AdjustorThunk },
	{ 0x060000E0, MutableDecimal_get_IsNegative_m6CC9630C1FE5DAABD29CEE9EF5281C37D12CE702_AdjustorThunk },
	{ 0x060000E1, MutableDecimal_set_IsNegative_mF373061A5BA3F192A2AA544BCB933C81BF71AC67_AdjustorThunk },
	{ 0x060000E2, MutableDecimal_get_Scale_mD47D52938E7026D2EC3AA837BABF7162C4F727A3_AdjustorThunk },
	{ 0x060000E3, MutableDecimal_set_Scale_m9253E0BBFF59D428FF76EA0A530D644053C3075C_AdjustorThunk },
	{ 0x06000142, StandardFormat_get_Symbol_mF8A6168808E4EEB455E2F8695DD243231285B0F2_AdjustorThunk },
	{ 0x06000143, StandardFormat_get_Precision_m0080A4421E19B1702147DF794C3D4C2D1035C5B6_AdjustorThunk },
	{ 0x06000144, StandardFormat_get_HasPrecision_m512C556F8EABC1880A4057A0255D42C5008F322E_AdjustorThunk },
	{ 0x06000145, StandardFormat_get_IsDefault_mA235EA7D61B74384AF56AEC5174D2ACD568AE144_AdjustorThunk },
	{ 0x06000146, StandardFormat__ctor_mEDC33761CDF50C5F7D01BF0DD6D673658931D43F_AdjustorThunk },
	{ 0x06000148, StandardFormat_Equals_mB63E4B0879F9B74C0783E32117A22592050C887A_AdjustorThunk },
	{ 0x06000149, StandardFormat_GetHashCode_mA2398AB63B3856075F7E8F9A26D142878DDAB119_AdjustorThunk },
	{ 0x0600014A, StandardFormat_Equals_m6AAD6931E6B7620BC5676B60FDE95DEBDCC6A011_AdjustorThunk },
	{ 0x0600014B, StandardFormat_ToString_m1391A69E60EF500E59D59A29124BAD2C1D28CE6D_AdjustorThunk },
	{ 0x0600014D, MemoryHandle__ctor_mD254CBC13788969FCC315DF2B1C8615A945F18B3_AdjustorThunk },
	{ 0x0600014E, MemoryHandle_Dispose_mE19418148935D11619DD13966114889837089E9A_AdjustorThunk },
};
static const int32_t s_InvokerIndices[443] = 
{
	2411,
	2411,
	2411,
	1183,
	2360,
	2341,
	1787,
	1759,
	2341,
	3793,
	3712,
	3876,
	3862,
	3797,
	3715,
	3876,
	3862,
	3876,
	3862,
	3876,
	3862,
	3793,
	3712,
	3876,
	3862,
	3876,
	3862,
	3876,
	3862,
	3876,
	3862,
	3876,
	3862,
	3876,
	3862,
	3876,
	3862,
	3734,
	-1,
	-1,
	-1,
	3492,
	3301,
	3794,
	3713,
	3245,
	3643,
	3462,
	3357,
	3787,
	3787,
	3465,
	3462,
	3353,
	3353,
	3471,
	3231,
	3259,
	2987,
	3651,
	3608,
	3876,
	3647,
	3667,
	2293,
	2312,
	2341,
	2360,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3549,
	3538,
	2937,
	3858,
	3876,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2962,
	2571,
	3060,
	3641,
	3652,
	3578,
	2958,
	3642,
	3652,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3745,
	3464,
	3464,
	3464,
	3464,
	3386,
	-1,
	-1,
	1999,
	2000,
	3689,
	3593,
	3281,
	2385,
	2038,
	2341,
	1999,
	3862,
	3869,
	3305,
	3305,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3862,
	3876,
	3246,
	3876,
	2829,
	-1,
	-1,
	2827,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3651,
	3651,
	3651,
	3651,
	3651,
	3651,
	3651,
	3651,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2340,
	2385,
	2385,
	2385,
	1028,
	3765,
	1759,
	2341,
	1792,
	2360,
	2411,
	642,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2567,
	3651,
	3237,
	3240,
	3462,
	2567,
	3651,
	3240,
	3240,
	3240,
	3876,
	3876,
	3224,
	3785,
	2925,
	3480,
	3480,
	3472,
	3142,
	3142,
	3652,
	3651,
	3652,
	2873,
	2843,
	2840,
	2842,
	2842,
	3068,
	3068,
	2844,
	2644,
	2831,
	3057,
	2845,
	2873,
	2850,
	2850,
	2652,
	2851,
	3075,
	3073,
	3075,
	3075,
	3075,
	2851,
	2850,
	2653,
	3075,
	3073,
	3073,
	3075,
	3075,
	3075,
	2653,
	2654,
	3876,
	3742,
	3876,
	2830,
	2830,
	2830,
	2830,
	2830,
	-1,
	2830,
	3054,
	2535,
	2830,
	2830,
	2830,
	2830,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	2830,
	2830,
	2830,
	2830,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	3054,
	2643,
	3876,
	3651,
	3639,
	3456,
	3341,
};
static const Il2CppTokenRangePair s_rgctxIndices[45] = 
{
	{ 0x0200000D, { 4, 36 } },
	{ 0x0200000E, { 40, 3 } },
	{ 0x02000010, { 97, 36 } },
	{ 0x02000011, { 133, 30 } },
	{ 0x02000013, { 163, 34 } },
	{ 0x02000015, { 197, 2 } },
	{ 0x0200001A, { 218, 6 } },
	{ 0x02000025, { 304, 50 } },
	{ 0x02000026, { 354, 2 } },
	{ 0x02000029, { 356, 6 } },
	{ 0x0200002F, { 362, 5 } },
	{ 0x06000028, { 0, 1 } },
	{ 0x06000029, { 1, 3 } },
	{ 0x06000061, { 43, 6 } },
	{ 0x06000062, { 49, 8 } },
	{ 0x06000063, { 57, 6 } },
	{ 0x06000064, { 63, 5 } },
	{ 0x06000065, { 68, 6 } },
	{ 0x06000066, { 74, 6 } },
	{ 0x06000067, { 80, 2 } },
	{ 0x06000068, { 82, 5 } },
	{ 0x06000069, { 87, 2 } },
	{ 0x0600006A, { 89, 2 } },
	{ 0x0600006B, { 91, 3 } },
	{ 0x0600006C, { 94, 1 } },
	{ 0x0600006D, { 95, 2 } },
	{ 0x060000CD, { 199, 4 } },
	{ 0x060000CE, { 203, 4 } },
	{ 0x060000CF, { 207, 5 } },
	{ 0x060000D0, { 212, 4 } },
	{ 0x060000D1, { 216, 1 } },
	{ 0x060000D2, { 217, 1 } },
	{ 0x060000F5, { 224, 10 } },
	{ 0x060000F6, { 234, 2 } },
	{ 0x060000F8, { 236, 4 } },
	{ 0x060000F9, { 240, 4 } },
	{ 0x060000FA, { 244, 4 } },
	{ 0x060000FB, { 248, 6 } },
	{ 0x060000FC, { 254, 4 } },
	{ 0x060000FD, { 258, 4 } },
	{ 0x060000FE, { 262, 12 } },
	{ 0x060000FF, { 274, 12 } },
	{ 0x06000100, { 286, 7 } },
	{ 0x06000101, { 293, 6 } },
	{ 0x06000102, { 299, 5 } },
};
extern const uint32_t g_rgctx_ThrowHelper_CreateArgumentValidationException_TisT_tD37E3D92CEAC2553F97A51BBCE647474D370296B_m9396274002331B5B0C21AE9B54B507EEF207BFE3;
extern const uint32_t g_rgctx_ReadOnlySequenceSegment_1_get_RunningIndex_mFB8F398796A1A99728AE1F12C46E40CE9444EF0A;
extern const uint32_t g_rgctx_ReadOnlySequenceSegment_1_get_Memory_m70DC7FEB736CAEB2D45E6180FD6B637933AE05B5;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_get_Length_mA495E727F64516AAC0350E3F9147510FA49F342C;
extern const uint32_t g_rgctx_T_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D;
extern const uint32_t g_rgctx_TU5BU5D_t4118076D1C03DAF417A6A255333150C0BEE630EF;
extern const uint32_t g_rgctx_Memory_1_t3A0A60ADC7935AF9DE308A680D9B70C62E92782B;
extern const uint32_t g_rgctx_Memory_1__ctor_mE549A3293F82A6B5C8215DAF003C06C6DA38D4A3;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_m4D01D83421566B675B35834C4B359557ACCA3810;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_m6FD61751F51F15A80DB4A4021122F467464809C4;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_mF879105DAA47623ED2CB4F88FE73871DD430C569;
extern const uint32_t g_rgctx_Memory_1__ctor_mC430E5D525FF8E58D52838B8F378530A9AFB75B9;
extern const uint32_t g_rgctx_Unsafe_As_TisMemory_1_t3A0A60ADC7935AF9DE308A680D9B70C62E92782B_TisReadOnlyMemory_1_t3282E1E3B6DF228012160DE3206F824E75C59BAA_m8DA6F3D2788D1671CCC58B66ED44D193F2E0A7B8;
extern const uint32_t g_rgctx_T_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D;
extern const uint32_t g_rgctx_Memory_1_get_Span_mC68A1D6DACBE70CE38267D188820CFA781FB3FB4;
extern const uint32_t g_rgctx_Span_1_t099D30BEC3819DB69EE680016717913BF8E4F640;
extern const uint32_t g_rgctx_Memory_1__ctor_m1DF978D6C0772AA97C73AA5616965DAAD526525F;
extern const uint32_t g_rgctx_MemoryManager_1_tF531F6EA7FCABE463E37DBD205FE0602AEF9D449;
extern const uint32_t g_rgctx_MemoryManager_1_GetSpan_m6A2A899D4BE5CA03912E8A2C290BEAE378AAF422;
extern const uint32_t g_rgctx_Span_1_Slice_m1604BEC6EFC3319F9D7C4CF8674798DDCB166CE7;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_tC4452E8B91F888C5C788D2104CF367AA830F02DF_mC3D0D1597A7C0D71B68AF5D0E6A9C29D767D10CD;
extern const uint32_t g_rgctx_Span_1__ctor_mA88348CBFFC6BAF81B4B7B4B72E125FB11625EC9;
extern const uint32_t g_rgctx_TU5BU5D_t4118076D1C03DAF417A6A255333150C0BEE630EF;
extern const uint32_t g_rgctx_Span_1__ctor_mF92E83D721B00D9571C8B1EF22649ED336180BA6;
extern const uint32_t g_rgctx_Span_1_CopyTo_mB413006119D0729D11D11263D3265DE72ADEB0BE;
extern const uint32_t g_rgctx_Span_1_TryCopyTo_m9A537879243CA7E9C806CAB955D1E5C1C85E6094;
extern const uint32_t g_rgctx_MemoryManager_1_Pin_mD6774D707B5765DB825EC6EF4F6A202F5823777B;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D_m9AF7D354CA428C65FD10D78DE6A6CF2B9F4B0DF1;
extern const uint32_t g_rgctx_Span_1_op_Implicit_m1F205ED2E54877B927BB4E4FB0B3CA0E9D3E5B6F;
extern const uint32_t g_rgctx_Span_1_t099D30BEC3819DB69EE680016717913BF8E4F640;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D_mF1301A3C3CA3CE3D60C4CFF2EF152FD59604A0B6;
extern const uint32_t g_rgctx_Unsafe_AsPointer_TisT_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D_m43766C9401D9EF8230A9577AFCFD466B51B09C99;
extern const uint32_t g_rgctx_Span_1_ToArray_mFD2D395C4E5AB0730CCDBE25C7882F26A288973E;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_t3282E1E3B6DF228012160DE3206F824E75C59BAA;
extern const uint32_t g_rgctx_Memory_1_op_Implicit_m591DD76ED9838B17C50A32CB26E9CC27BC96957C;
extern const uint32_t g_rgctx_Memory_1_t3A0A60ADC7935AF9DE308A680D9B70C62E92782B;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_Equals_m55F19559EE9D04C16012019E67FDF49D65B9D6BC;
extern const uint32_t g_rgctx_Memory_1_Equals_m0C68E58D8165F0062FFF7A1A44D967A9ED6E64C1;
extern const uint32_t g_rgctx_Memory_1_CombineHashCodes_m65FF43E7D0D4989BD77D27A7071CB41A1F408F66;
extern const uint32_t g_rgctx_Memory_1_CombineHashCodes_mC2B5C725E2EF01252C44316547CB90A8A794DCD4;
extern const uint32_t g_rgctx_Memory_1_op_Implicit_mECC52CE5EB3F43B8AB582DE0B5C605D55199F94B;
extern const uint32_t g_rgctx_Memory_1_t6148C70E4F9FC281D85385037D846B0B063E7E92;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_ToArray_m2F1B5777753F3BD1A4C54E7993A4D051EAC1356D;
extern const uint32_t g_rgctx_T_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_m487629DCCCCF8779F78931E150ACC143560BDFAD;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mF5D3FBFE0CF2DB286CA445EBAE8F2F0B1CE00775;
extern const uint32_t g_rgctx_Span_1_get_Length_m1661D08289A5DA205642B9CDE82799EBCB3280F4;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCC4B9EF9BEC5E097B5F8A213A3149D31EA768DE9;
extern const uint32_t g_rgctx_SpanHelpers_IndexOf_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_m75CA9E23D6DEF41F9389F83CD13E27FAED79CC56;
extern const uint32_t g_rgctx_Span_1_get_Length_mAA25731D6369F29AA71BA9BD2394893E8F7E7820;
extern const uint32_t g_rgctx_T_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3;
extern const uint32_t g_rgctx_MemoryExtensions_IsTypeComparableAsBytes_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_m18133260D0645715B138549E3CF8C87BBEADF525;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_m395EB745D44789078F6E15DDD15D8DB9E7FB210C;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_mC514A25C1A510ACE264C08DB6A0114FCACBC1357;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m63B02596D37875137ACADA7027CCE3516392EAC4;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_m5FD6BBD83D2AC99FA8D9382B85B5717206753FEC;
extern const uint32_t g_rgctx_SpanHelpers_SequenceEqual_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_m2B6E1AA73E28AA0C07EBC27CF41806D5E6A01418;
extern const uint32_t g_rgctx_T_t23B3078B6BDD3687722BF7D7D921089EFA84456F;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_m415FCA9333308EBEDDA6A7D320A226696F41819D;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDCED40935168C00CAEC5807650BF052210CA73C8;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_m692B9687061BCDFDCE6C8357E77220636AE78BD8;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m28CA58D74401581514AA5A35598EC35EBFE2670C;
extern const uint32_t g_rgctx_SpanHelpers_IndexOf_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_mBB85899BBE4E38659A2FB77E0121CE97226412C0;
extern const uint32_t g_rgctx_T_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282_mAE5A00AE96D27D73FB0994C45F05EB7D703749C2;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mAF70E2B6324F632D7198DCF51DA150886334D418;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_mECF24D29FE0A504D57F9C8840847D88E942AA0EB;
extern const uint32_t g_rgctx_SpanHelpers_IndexOfAny_TisT_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282_mD4B02132C59502C17749B661F5D1DC34F8AEDDD9;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_m88C7B36148B0458F30D9C748749C0E67E7DB99B6;
extern const uint32_t g_rgctx_T_tA342E96AD4DE781D4419A86502284C3B756E5E78;
extern const uint32_t g_rgctx_MemoryExtensions_IsTypeComparableAsBytes_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_mBD0CC95AA81766D034F494FF24C17E60D28771C5;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_m6FC3578929376E21567ADBF4E789784704487DB5;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8B267986758CC788674A6D4ADAFC16331A16B9D0;
extern const uint32_t g_rgctx_SpanHelpers_SequenceEqual_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_m870FC7DD7A20D0FBDFB2D599560EFB2394B7C2F3;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_m0C026C9D253E610988836E46F0513D7D5C76495F;
extern const uint32_t g_rgctx_T_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2;
extern const uint32_t g_rgctx_MemoryExtensions_IsTypeComparableAsBytes_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_m5C613B7D7134917F16B73380ECC5DA27A15EF3F4;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_m39BC4C2104F7C4FF5FB7D041670E533F4E6F2562;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m383847B46FEF56417C3157E729EFB1AEF0C6F3B4;
extern const uint32_t g_rgctx_SpanHelpers_SequenceEqual_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_mCA389B52683DE699265EE825F02D05B961731E7F;
extern const uint32_t g_rgctx_Span_1_t7CD08B30C6FAD780BCE45FC30ED91FAF3667C58A;
extern const uint32_t g_rgctx_Span_1__ctor_m3C4D88A58DDF9C0390FBD4BCDF64139F835207AC;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_m4AC94E6A2641753E59CC981403D066F65DCCFA22;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_m9F754AE3DB448F6ECFB8B9FB5ED7581A7E9A9E70;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_m67F2FF6A8C2C986DCEE4DCB95FE8D97EE3D64C3D;
extern const uint32_t g_rgctx_Span_1_tC7E6CF7F493B0EB2659C9102499E6A50CEC930F7;
extern const uint32_t g_rgctx_Span_1__ctor_m0F0718BD3992E033752C55EC16311644A209587A;
extern const uint32_t g_rgctx_Memory_1_t092E086E499AE1CA5BBDDD6B0E6791585FB0F893;
extern const uint32_t g_rgctx_Memory_1__ctor_m087F27C2E987355D069E96B6E13D3D8EE28E54A8;
extern const uint32_t g_rgctx_Memory_1_tF89C8D40FB64990911F669A7342596F35CF99586;
extern const uint32_t g_rgctx_Memory_1__ctor_m2AF025C7CE4D0D7790185A1F922753A9877F16DE;
extern const uint32_t g_rgctx_ReadOnlySpan_1_t6D304187EA681023F74EFF5AEE5793A2F3682FBB;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_mC076E6AC769C30EF58C6D06B860C02349FEBB363;
extern const uint32_t g_rgctx_ReadOnlySpan_1_CopyTo_m2CB83AC9724B33082459A071BD31682BCDF5EF9C;
extern const uint32_t g_rgctx_T_tD779522ED3B77598B82D881C830AE8FAA64B62B8;
extern const uint32_t g_rgctx_Span_1_Create_m500272C0B1610C35C7EBA9A8D017E204055DF004;
extern const uint32_t g_rgctx_Span_1_tE3F6AC38A1D5A5FAA6ABC9C909561C618DE30305;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_t6E998EDEA7C2DBE3A478C88DB747BF881CF18F13;
extern const uint32_t g_rgctx_ReadOnlyMemory_1__ctor_m70764FE8621671C4FB86AF3326016310ACD70FB0;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_mC23058C409995962E49C2A8F16736A803BEA359D;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_m30DCA488C1D1C2F0B83ACBC02A90C4F478D9677D;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_mCDE832A11247B5D3D87953B6914EEAD6B8BECB7B;
extern const uint32_t g_rgctx_ReadOnlyMemory_1__ctor_m6B146ACF0AD6148607FDB20B777818617F21B067;
extern const uint32_t g_rgctx_T_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_get_Span_mDB48EBC36632B89B96E8659934DB70C1F9FED4B6;
extern const uint32_t g_rgctx_ReadOnlySpan_1_t36EDA418209E868BD997706C9559CA7FAEADB7D9;
extern const uint32_t g_rgctx_ReadOnlyMemory_1__ctor_mDF91B164AE0E34507176F081E44FA26BE0E71D18;
extern const uint32_t g_rgctx_MemoryManager_1_tB1368C4BFE321A3BE8A22EBAB44D53A81F4B0F69;
extern const uint32_t g_rgctx_MemoryManager_1_GetSpan_m32AFD05CEDBA513833653B12111FD53516953A9E;
extern const uint32_t g_rgctx_Span_1_Slice_mCDFA85A97B0021B59000D6660C3C497D4C860894;
extern const uint32_t g_rgctx_Span_1_op_Implicit_mF29436A68E847F64589A7D66980A106CDFCC2F54;
extern const uint32_t g_rgctx_Span_1_t56D319E892EC8D5D6D7727D4D2B62A18D0D1943F;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_t378098EA91E1792B7730CA7E433BA330E594A9D7_mA31DC817A54E414AD2256B14D1C8FE678D784916;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_mF1E5E6B0D69712D6EC3AC9C84C7793F78480EAC9;
extern const uint32_t g_rgctx_ReadOnlySpan_1_Slice_mC16194596903988CBE768B315636C49714E21747;
extern const uint32_t g_rgctx_TU5BU5D_t989B6BF2A41E04B9CF2C01C0EE5C92E30C4595EC;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_mC17A8F7D5C4C3048EC6EF84F70E506DE71B61904;
extern const uint32_t g_rgctx_Memory_1_get_Span_m1FA547C868BA1A89D567BE9C2D1B9038A20A3E84;
extern const uint32_t g_rgctx_ReadOnlySpan_1_CopyTo_mAD5AC5A8299CB11BF2937E8E750390A05F1AC3AB;
extern const uint32_t g_rgctx_ReadOnlySpan_1_TryCopyTo_m04F28536FEBA57DF3CC731A0F58AFED8B1F4123C;
extern const uint32_t g_rgctx_MemoryManager_1_Pin_m9F9C6F3127F412A9CA2598378137F19865A208DA;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE_m2182E9223CE10120BA2F2D6A65F10A3185D814FC;
extern const uint32_t g_rgctx_Span_1_op_Implicit_m83ACFAF76356ADE20E08D2242FC7C6BA2930C159;
extern const uint32_t g_rgctx_MemoryMarshal_GetReference_TisT_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE_mB2A605C1C77975837143736FE1DA8A80FDF47E1F;
extern const uint32_t g_rgctx_Unsafe_AsPointer_TisT_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE_mA7F19DD21041F8A1D528081D99EB724FBEECCD04;
extern const uint32_t g_rgctx_ReadOnlySpan_1_ToArray_m4F43F7B8F3EB8B1EC00C915CF2619E0C1D65D860;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_Equals_m97826DCB9FD7DC70424D510FA10FC372DC30D517;
extern const uint32_t g_rgctx_Memory_1_tB976783BB63A449DD3D82C77ABD40B09AF0BAE0B;
extern const uint32_t g_rgctx_Memory_1_op_Implicit_m02BAD7865E0F5958DB771B5CF15D1AEE28DDEAF7;
extern const uint32_t g_rgctx_Memory_1_tB976783BB63A449DD3D82C77ABD40B09AF0BAE0B;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_CombineHashCodes_m1E98C0191C4F0E97D34755916063AAB1CCCB1781;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_t6E998EDEA7C2DBE3A478C88DB747BF881CF18F13;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_CombineHashCodes_m92CF7AD651BE182BDAD998052FFB29A87B4D3BC6;
extern const uint32_t g_rgctx_ReadOnlySpan_1_op_Equality_mB801A1E859548B993E967ABD4F7EAD7716C2B6AD;
extern const uint32_t g_rgctx_ReadOnlySpan_1_t91CD2A7485A168014F77608661B4E0E165CE2019;
extern const uint32_t g_rgctx_ReadOnlySpan_1_t91CD2A7485A168014F77608661B4E0E165CE2019;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_mC468AE6DB70AC9BA8B25E24D4EA423180C371707;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_m3A824F29EEB9A15E6EEF841D8D69BA623B70C643;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_mA4E85539B7C95D4155EAEE6322B7DE36F81776C9;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_mC9A5AD9579086A699CD569B107FF7C33CE0487FB;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_m355E77DE8E48E9FEE504D3FD462DCADC544CA358;
extern const uint32_t g_rgctx_Enumerator_tAB5FE10B43025C0D260C13682B70428E180C1903;
extern const uint32_t g_rgctx_Enumerator__ctor_m4E1DE54780585BEA303E4B375F122C3A63AB2949;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_t9F8C6029FCC8C6FFC1758B25175A69D2E7C31A2C_m3E6C95D5329759E428FB9E74FAD2A0F6AE197E29;
extern const uint32_t g_rgctx_PerTypeValues_1_t3C6F41E20B58DF9D7A987D6F0F4BFA7315DB29EE;
extern const uint32_t g_rgctx_SpanHelpers_Add_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mBF6CD105D49DAFBEA065AF4A9ABB0F61EBEF9629;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mD1364F8974D78E6AB926DEEE0D94FC9DD1640BF6;
extern const uint32_t g_rgctx_T_t555E257E7DBB4DBED5A1334602417F3F53E59A8F;
extern const uint32_t g_rgctx_Unsafe_AsRef_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_m78DE6AF2042F5A4BA1168A981DCE8D1AE96149F5;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_m20270E7584E4CD7BAA02B5E903C9FC42B017E701;
extern const uint32_t g_rgctx_Unsafe_AddByteOffset_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mF5A80B2D7F96E2EA8A72C525459475EA95BC3C75;
extern const uint32_t g_rgctx_ReadOnlySpan_1_TryCopyTo_m74D59A26752BE2078C5139924083179299B4E8A3;
extern const uint32_t g_rgctx_Span_1_get_Length_mAA3D2E88E5973D20BD442AA39929581446ADDC22;
extern const uint32_t g_rgctx_ReadOnlySpan_1_DangerousGetPinnableReference_m44E613AF004F4EB1241F9AF903EA1DE490CBDC10;
extern const uint32_t g_rgctx_Span_1_DangerousGetPinnableReference_m28D20B27F95F7960C6AE76D7F8AA60E93F1BE314;
extern const uint32_t g_rgctx_SpanHelpers_CopyTo_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_m7FA9C80315B2DAAC3AA46AE38A804AB6756D5317;
extern const uint32_t g_rgctx_Unsafe_AreSame_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mF6EE96C9C4F25050B3A75BEC1C07692365DDA407;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m3E4020A59A6E449C3AAE43DD259C3624FECC557B;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_mC7F4A0ED06859379B46C661131ACDB9EF4A12F06;
extern const uint32_t g_rgctx_TU5BU5D_t8E11F4C8BD801DCCCC1464E919B0FD9948F6119A;
extern const uint32_t g_rgctx_Span_1_op_Implicit_m7CDDA1C94D4A5A7E6B65FC60CACD5A70A0D90165;
extern const uint32_t g_rgctx_Span_1_tB08EE97D87AB966193DAF63C784C9CC4003678E1;
extern const uint32_t g_rgctx_ReadOnlySpan_1_CopyTo_m6C0A777827557D83E88319D26647C52E2F272C2F;
extern const uint32_t g_rgctx_Span_1_op_Equality_mF67D8FA34F5624F83174DF2E9D812919DE965C67;
extern const uint32_t g_rgctx_Span_1_t9214D4F1438E53CA54A3C870FA56F1E52FA8BCC2;
extern const uint32_t g_rgctx_Span_1_t9214D4F1438E53CA54A3C870FA56F1E52FA8BCC2;
extern const uint32_t g_rgctx_Span_1__ctor_mF0D60C1382EFF0415FEC92F5DD49E129A698EB51;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_mCD4BFDDD0C670FC8AEE34C2AE4D5FFFBC71955B9;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_m23EE07D26588FB2F1CC39DD866E45A144441D7A9;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_m856156028141333C01F261C5B9716676D6C41488;
extern const uint32_t g_rgctx_Span_1__ctor_m93E09E5F8E4E699E655329D5F9C7B6BAA7380BE5;
extern const uint32_t g_rgctx_Enumerator_t47768893B54EC15B452803B09617FB72B1D37DF4;
extern const uint32_t g_rgctx_Enumerator__ctor_m762D4A3BBD947E8BB43F2E7331BA32023CD2B1FA;
extern const uint32_t g_rgctx_T_t3FFAFE11C71F64BD573D9783635D59A89C81CD22;
extern const uint32_t g_rgctx_TU5BU5D_t6215054E5C82A201EA1F10DA7F8797B794134DA6;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_t23C5B5D7226DD19588BBF94B58DF9CD91F51D7CD_m7117CA6723E2766F7ED4656FF8CC3880B6D930A7;
extern const uint32_t g_rgctx_PerTypeValues_1_t6F5DCC1D6536B5D2AB3551EB44C9A014D810AA1E;
extern const uint32_t g_rgctx_SpanHelpers_Add_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mA6483C4D7A67BF654025373656E3C9ED6A00BF89;
extern const uint32_t g_rgctx_Span_1__ctor_m6850B6A1A6755DD1E6A11A16C3494CDC9952C42D;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mEE3340E07EBF53314BCACC1EDA5BBF2708D881DA;
extern const uint32_t g_rgctx_T_t3FFAFE11C71F64BD573D9783635D59A89C81CD22;
extern const uint32_t g_rgctx_Unsafe_AsRef_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m094E088627897C688694A0068DFD6F3529D20060;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mBFCC7873F5F1BB35435E9B604ACE71964F3125EB;
extern const uint32_t g_rgctx_Unsafe_AddByteOffset_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m4DC82784661C3C9F3FDCADD520BB63F8EAE9A863;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mDCE918D103555011799F364EADF2D29B946588C3;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mD3F9A5804D1A1F063208B9C3E149895ED713B4D0;
extern const uint32_t g_rgctx_Span_1_DangerousGetPinnableReference_mA002A9003AC104718C4487320AF9B106B1AFDFED;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_TisIntPtr_t_mAA61919605B05521702592DCC9DFF87B11E6056B;
extern const uint32_t g_rgctx_Span_1_TryCopyTo_m33061340B36235BC1DA613A099A4143A8526567A;
extern const uint32_t g_rgctx_SpanHelpers_CopyTo_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m8B1BA5E54B80D9F343D2959F45BC7731164B5678;
extern const uint32_t g_rgctx_Unsafe_AreSame_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m74F9E8C03331887EC564A3C15CC2C78DC048F4A6;
extern const uint32_t g_rgctx_ReadOnlySpan_1_t0E0BAF4A01D2FEF6F7BEE722FA7F2408C9EEF5BD;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_m9F1FE95B304B9C711453EDFF7AB5F71A718CD7D6;
extern const uint32_t g_rgctx_Unsafe_As_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m28A532D44CD83CFA0075EEBCD8818637CA4B16B3;
extern const uint32_t g_rgctx_TU5BU5D_t6215054E5C82A201EA1F10DA7F8797B794134DA6;
extern const uint32_t g_rgctx_Span_1_op_Implicit_m6B77C0F0693EB9B2F0E40FD9DAF3B222A65DABC4;
extern const uint32_t g_rgctx_Span_1_CopyTo_mFB25ABBAF7D53E98FAF31248B5EB4551398805BE;
extern const uint32_t g_rgctx_Span_1_ToArray_m51F8C04DAB0860CCBB6454EB823C700739E09B23;
extern const uint32_t g_rgctx_ReadOnlySpan_1_ToArray_mED0753CEE34B306C98CB69309706B9B1057E3B5A;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_t6CDA375BE0C2EE3B9E47FFFD98EAC42B20EAB896_mD760BED2B24FA7CA46C472D4306EF777B7F227CD;
extern const uint32_t g_rgctx_T_t6CDA375BE0C2EE3B9E47FFFD98EAC42B20EAB896;
extern const uint32_t g_rgctx_IEquatable_1_t9ADC3C011A4E4C7D9FC54B5830B7E3120D032478;
extern const uint32_t g_rgctx_IEquatable_1_Equals_m0F5089230F413D03AFE2576D8ED1538BE7566B8C;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_t8477FE405833FB2B66C47191B0FAD170965A30EA_mDD4B96C2D162277E76E966FABCDD536076646727;
extern const uint32_t g_rgctx_T_t8477FE405833FB2B66C47191B0FAD170965A30EA;
extern const uint32_t g_rgctx_IEquatable_1_tBDA13BB1BF464F52E06EDBD759191A5E75676CD0;
extern const uint32_t g_rgctx_IEquatable_1_Equals_m560BF984575EF1ED6A3EBA2C6616F332539AC89A;
extern const uint32_t g_rgctx_Unsafe_AreSame_TisT_t7AF0F4FB475CA8B86F0D30BC6B38C2AA1A43CAC2_m17A90560164268F745E41D7609234BF7C4051B25;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_t7AF0F4FB475CA8B86F0D30BC6B38C2AA1A43CAC2_mEDED094021E77580C9AC361E7C466D1CE9B13763;
extern const uint32_t g_rgctx_T_t7AF0F4FB475CA8B86F0D30BC6B38C2AA1A43CAC2;
extern const uint32_t g_rgctx_IEquatable_1_tD57BDBE84277CE34C40D5CE73C386B97AB282214;
extern const uint32_t g_rgctx_IEquatable_1_Equals_mE7D3B60D056823734A88E080B7B0F4EAEF610304;
extern const uint32_t g_rgctx_Unsafe_Add_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_m4B7424958CD1F89D4313A56A1E014A5A2B726BA3;
extern const uint32_t g_rgctx_Unsafe_ByteOffset_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_m5DA471B8A15F72C25DC3F71483EDD830450AF725;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_m39BAA47C6FDA958FBE9CB37E78FF9FAA85BF0249;
extern const uint32_t g_rgctx_Unsafe_As_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m518D6A5519D6DFFAA775BE7540087E98246FC6FD;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisT_tAFCE6CD0B5764A0EE807B31E2E8DED86D24FB2BD_mE17E5F9AE11AE33359035249AEF159A6F5694F8F;
extern const uint32_t g_rgctx_PerTypeValues_1_t4B33D1DABA77A801C021FEF65CCF9A727BE0B640;
extern const uint32_t g_rgctx_TU5BU5D_t2F2F6AB68CDFB2823880B0538E6F0B0A52240F77;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_t2EA5ACA779A7D81D953593F484E91B12B39A98CE_m5E770E19AFEE24BE79CA6FC1018D2FF1311190B9;
extern const uint32_t g_rgctx_Unsafe_ByteOffset_TisT_tCAB09CA3EEBD1F00CA8B35C19742BEF19A762A8F_mD2C5894A63CFDF0A35315A17D0C333395B1A9D88;
extern const uint32_t g_rgctx_T_tCAB09CA3EEBD1F00CA8B35C19742BEF19A762A8F;
extern const uint32_t g_rgctx_PerTypeValues_1_t2D2B1745D83B705A22C721F976D97E22C98B6C4F;
extern const uint32_t g_rgctx_PerTypeValues_1_MeasureArrayAdjustment_m53E4AE5B15698B7BCC81BEDB07790C9CD06FF6D6;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_GetObjectStartLength_m16B54BF53654405683ACCC8519ED827F53CC71BD;
extern const uint32_t g_rgctx_MemoryManager_1_tB26C300AF0074B34568569A53B57BCAAD87A4C1F;
extern const uint32_t g_rgctx_MemoryManager_1_TryGetArray_mFDF52D29025D8C2611281EFFBF950CCD4C587347;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_mF97EF25B285DBA2955571227D8069A2277CF87B0;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_m7B38B7391F1C085E0AA4082B5604E5EA845C86B8;
extern const uint32_t g_rgctx_ArraySegment_1_t9301AA2774C3C80D9ABA093483D4D9FC775E8CEF;
extern const uint32_t g_rgctx_ArraySegment_1__ctor_mE87E46F0DA62FD0DE9CA2F5236F1FA6C13B0810B;
extern const uint32_t g_rgctx_TU5BU5D_t5621C4B518D02AD0C4A007B7BFD26CD5F310370D;
extern const uint32_t g_rgctx_PerTypeValues_1_tBAC8327CFF46C4E28B21D534403653922B13F64D;
extern const uint32_t g_rgctx_ArraySegment_1__ctor_m82253ECBC930F15ACFE2D60C275138060BB50CA8;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_GetObjectStartLength_m48CB16E7B5730A751AACDF7F6DE97337EAE43F91;
extern const uint32_t g_rgctx_TManager_tDB3B2604DFC72135387CCD59F9A56204EDCCB23D;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5_m422DCCEE46BF0158EC7BA379E7E66A3F9EB25BDA;
extern const uint32_t g_rgctx_T_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisT_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5_m15575D27CAB8ABD64D12F0843973BEC3D393AFB5;
extern const uint32_t g_rgctx_Unsafe_ReadUnaligned_TisT_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5_mF16AD3137E4B4EFBD44A26EA3B85E3EF48983EA7;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772_mEAE9B32DCADEB1494974E5675DD87EF8A2945966;
extern const uint32_t g_rgctx_T_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisT_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772_m505D4758E3AFBBA5D132BC8662E4DA2F8FC12537;
extern const uint32_t g_rgctx_Unsafe_WriteUnaligned_TisT_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772_mB984422B110999FE678263ACF8A9D1A9DA5891B4;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tFA97F1007721867D582FB279D90E2DA497268BE5_m145E4DBBD465D16F1ECFB68DB33C525FB185468B;
extern const uint32_t g_rgctx_T_tFA97F1007721867D582FB279D90E2DA497268BE5;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisT_tFA97F1007721867D582FB279D90E2DA497268BE5_mA911846D1FFAEA0D3D8923DDE1B75C2238DC4B66;
extern const uint32_t g_rgctx_Unsafe_WriteUnaligned_TisT_tFA97F1007721867D582FB279D90E2DA497268BE5_m2928D90771017396385903BD62EC884D8CD4FB0E;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_t22C5E087F231D0C06125CB0792E17BE40D2B7363_m685FD785FBC1F080E4CDC640D64BEAC8686483DC;
extern const uint32_t g_rgctx_T_t22C5E087F231D0C06125CB0792E17BE40D2B7363;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_mC595F4FABD21BF2D111D1A361C4AE15675316C76;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisT_t22C5E087F231D0C06125CB0792E17BE40D2B7363_mAA07C15FCE5987BC0376997545441E4558C5B758;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Pinnable_mA961B5DB31E39C10135714DB5124BDF391599ECE;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_ByteOffset_mDF9F27360C2596503A658990FB6ED5F5E7AC4C30;
extern const uint32_t g_rgctx_Span_1_get_Pinnable_m1976EB368514A3ADABA0723B0C55DC30D00B1133;
extern const uint32_t g_rgctx_Span_1_get_ByteOffset_mA9EC00DBBC2C014D9EE93B786B5F07EF921E496E;
extern const uint32_t g_rgctx_Unsafe_AsRef_TisT_t21693D9B947761AB0FEF5F4E5D05DE3F5DE92CD6_m5107E32C7CD76572538EB43F5CEAF5D796C8A48F;
extern const uint32_t g_rgctx_Unsafe_AddByteOffset_TisT_t21693D9B947761AB0FEF5F4E5D05DE3F5DE92CD6_mAF0C5A22D1EBE16085A71D01CA8FE49CC65D43A5;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Pinnable_mBFEC5EB55CE535660504F4C7D00E87C00E8EDEEE;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_ByteOffset_m35D945231E703603597F15C3ECD5D2C303CAC1F6;
extern const uint32_t g_rgctx_Unsafe_AsRef_TisT_t41DDAA8BB090A6D561131FCEB35122A9971E40E6_mF1ADADBA7B6CE946B06C847179B694357EE4C82E;
extern const uint32_t g_rgctx_Unsafe_AddByteOffset_TisT_t41DDAA8BB090A6D561131FCEB35122A9971E40E6_mDF58AF6E82384977F481C02C4FD7BF5B6B2212EB;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTFrom_tE8CEFA5A69C5A19928463C4000F334BD0D0E588B_mAF8A62CE03196311E123AA9198FB7ED03738D800;
extern const uint32_t g_rgctx_TFrom_tE8CEFA5A69C5A19928463C4000F334BD0D0E588B;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTTo_t17CDBA7A02EC6F4A45DD5B76B6C105306386EDED_mD334DCAAD0884C4831A5423EDB17E8EC8F49DAA7;
extern const uint32_t g_rgctx_TTo_t17CDBA7A02EC6F4A45DD5B76B6C105306386EDED;
extern const uint32_t g_rgctx_Span_1_get_Length_mFC3F5002B2184D5F5D37A48F825E4CAE321D0C60;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisTFrom_tE8CEFA5A69C5A19928463C4000F334BD0D0E588B_m02676CB3A79C71FE3FEB16DAEE068EA5496E9B80;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisTTo_t17CDBA7A02EC6F4A45DD5B76B6C105306386EDED_m6B1469BC5A712227F501D37D42AD14EE89922D8B;
extern const uint32_t g_rgctx_Span_1_get_Pinnable_m9AA32FE22873A606F10A66F30826F1E69A37B18F;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_t07C8A38CBAC2A98F6A9D201E7E0B871FEF0AB45B_m494B9A22F4AF3A54F67B0AE97D380F7E06A48B14;
extern const uint32_t g_rgctx_Span_1_get_ByteOffset_mCF7447876C9858EC3825888B3B1CE1EDF0DF4792;
extern const uint32_t g_rgctx_Span_1_t458550BD8207AFC69D5AD7E833A6C453D5069501;
extern const uint32_t g_rgctx_Span_1__ctor_m8DCF397840ED7EEE989CECB3BF705216B509117F;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTFrom_t53C5CC6DF64CBD3D7AF5B3EB33B03F3E4B7219CF_m11D0E4F9DCAD4C02D29FC6A16A8D77C8C0BF4A99;
extern const uint32_t g_rgctx_TFrom_t53C5CC6DF64CBD3D7AF5B3EB33B03F3E4B7219CF;
extern const uint32_t g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTTo_t8E941AFE856E74DC2CAA108BAA5259F6BB3E6185_mED5C92204247416D4674A84F2392CD4E4ACEF2C8;
extern const uint32_t g_rgctx_TTo_t8E941AFE856E74DC2CAA108BAA5259F6BB3E6185;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_mF7137315AE0F0D797C791B556FB97ACFF36E6DFD;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisTFrom_t53C5CC6DF64CBD3D7AF5B3EB33B03F3E4B7219CF_mABE06CFC6E14B02AFB9FE074F48912ABC5CC1474;
extern const uint32_t g_rgctx_Unsafe_SizeOf_TisTTo_t8E941AFE856E74DC2CAA108BAA5259F6BB3E6185_m4EDA596E3918628648FB17BC781D8B5C56D32382;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Pinnable_m16063606B87A5885F8ECB66D14CFC72EB6E8BE09;
extern const uint32_t g_rgctx_Unsafe_As_TisPinnable_1_t9391C4657303A0C7EDCD82E69C020031E017094F_m3136269B0F638F2A3CCE92BAD874EFBFC015B153;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_ByteOffset_m8EC26C2E995BCB5FC1B86191367202134520CA57;
extern const uint32_t g_rgctx_ReadOnlySpan_1_tDAA19E0AAFB7F1CBD88F33A4541B540C3730A009;
extern const uint32_t g_rgctx_ReadOnlySpan_1__ctor_m7D2F538D269EA652F1CC04A2569DA96866960FE5;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_Length_m37C1382A39B2187170851BD7EA02345BD55ED73F;
extern const uint32_t g_rgctx_Span_1_get_Length_m830234B00F16A58C26874135988312C3B8E226D3;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_IsSingleSegment_m35E62D4A341DAC4509B717647D15D07D646AFCB8;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_First_mBB85540F785798ACE473EA859DB3556FF16C9CE1;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_get_Span_m80077198EB1B121C3D3C3F180E31466DF78D2411;
extern const uint32_t g_rgctx_ReadOnlySpan_1_CopyTo_m714F52B1D969DF522F89BFEE9F2F1CE44614D0DD;
extern const uint32_t g_rgctx_BuffersExtensions_CopyToMultiSegment_TisT_t06E68360FF5061F8178FF588235037DEAD443FEE_m7DCC5D906CEE3B78334C133D5CEE57A7E55B5B92;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_Start_mD2C1EB871926533B034891E6DAC9C3B4C20879CB;
extern const uint32_t g_rgctx_ReadOnlySequence_1_TryGet_m555C28AFA5A15959F6C4EF564E2F973CFF3273D4;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_get_Span_mF4427E92FB925C86C4DD66BE407BEEDEDA9814DD;
extern const uint32_t g_rgctx_ReadOnlySpan_1_CopyTo_m623B5FB49C03737D16BD57B9CD24D113AC2B98EF;
extern const uint32_t g_rgctx_ReadOnlySpan_1_get_Length_m3ED118A8ECB714F4A313F99C805F0C647D1C9200;
extern const uint32_t g_rgctx_Span_1_Slice_m8AA1289E6B67F659024C7E2ECC2B4DCBCC73A759;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_Length_m77ABDE2458034B8C46E952FD627B7CE72B0F3F12;
extern const uint32_t g_rgctx_TU5BU5D_t718F0E16C7507AF6A1817312AC9CD9F72A003431;
extern const uint32_t g_rgctx_Span_1_op_Implicit_m132A6353C862787039EB55423B98BD0DCC9E2D04;
extern const uint32_t g_rgctx_Span_1_t517546D5EF44F1A726F338259119F2C277842506;
extern const uint32_t g_rgctx_BuffersExtensions_CopyTo_TisT_tDDB0061770280A4FDC7746BAB73E4ABDF786097E_m13E5585A60F294FA88A34533107E8552132240BA;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetLength_m2E42A8AC70A4F2A5AC2DB5A6F844CA7CF029ECAB;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_Length_m8E0DD3D6C0A5F2B93E551BACE230DB2AC625D57F;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetFirstBuffer_m6EA6CB0D473C0B712FA3BA782AA32A587FE94EA9;
extern const uint32_t g_rgctx_ReadOnlySequenceSegment_1_get_RunningIndex_mEDDC617A31DAD38C9E23CA2F1B77423646A38C58;
extern const uint32_t g_rgctx_ReadOnlySequenceSegment_1_get_Memory_mA71F4FA1675C8769FBEA1F99BFE1DE8059F6BDDE;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_get_Length_m4F26CA928CF49A14BA066B2ECF5ABA49E09B2869;
extern const uint32_t g_rgctx_ThrowHelper_ThrowArgumentValidationException_TisT_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452_m7C1A3A1AF30D08DEA7AF3B4517D64F7C785B636A;
extern const uint32_t g_rgctx_MemoryMarshal_TryGetMemoryManager_TisT_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452_TisMemoryManager_1_tA7AE4E9FE38806DED755EF302EBA858A3A3F5DCD_mFA744E09F9C303712F6EFCB7EB84057924C7B203;
extern const uint32_t g_rgctx_MemoryMarshal_TryGetArray_TisT_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452_m7EF3299FC3F3039B7AFB98C92940729590951E21;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_m579F8F3547792AA3FDC7DD846AE9D03C2EA6DCB0;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_m5DC87CA276566E59192826DDEB589DEF07602AC0;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_m381403CEF16C8649B55EC3C8FB1213037234B404;
extern const uint32_t g_rgctx_T_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_t4A19AD65C799CC80D2CD578AEC1DC2ADEB679B62;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetIndex_m9F52D416B3F9ACA06B5178D151C9E040AB720EA2;
extern const uint32_t g_rgctx_ReadOnlySequence_1_tC8AAE38B86B84A0F26E77F21498DB77C60A0AAF0;
extern const uint32_t g_rgctx_ReadOnlySequenceSegment_1_tFEC43C4C98DADFE39066BD549022F76364FD8434;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetEndPosition_mFE0824CE3BE464D14BF6C1A24FD7284E6A447F9C;
extern const uint32_t g_rgctx_ReadOnlySequenceSegment_1_get_Next_m0BF61937A17CBE7D13934ED6DA1D6E3A7AB6BA9C;
extern const uint32_t g_rgctx_ReadOnlySequence_1_SeekMultiSegment_m2E808C13152A0E4FE5811508E3D4B95BC18AF069;
extern const uint32_t g_rgctx_ReadOnlySequence_1_SliceImpl_m94A827AE2F1CEA08342BAB474A7F8119274BE44C;
extern const uint32_t g_rgctx_ReadOnlySequence_1_InRange_m4006CEE42CA53B9F2F7FCDC2B906ADF477E2C762;
extern const uint32_t g_rgctx_ReadOnlySequence_1_InRange_m01D4A88370BCCF3BCC44C3F66DC7DC6A862ADA4B;
extern const uint32_t g_rgctx_ReadOnlySequence_1_Slice_m56E356CA45D6857009217C36DF704EE677510725;
extern const uint32_t g_rgctx_ReadOnlySequence_1_Slice_m580BAE4438515E2080BCC9FE9884491335D4F13E;
extern const uint32_t g_rgctx_ReadOnlySequence_1_Slice_m23AACF77AC1ECA667CD141E37E5198BE256CE38A;
extern const uint32_t g_rgctx_ReadOnlySequence_1_BoundsCheck_m02D754A6D78B00DF01B0BB5A6E0C27230796C70B;
extern const uint32_t g_rgctx_ReadOnlySequence_1_BoundsCheck_mE41AE054C026B4C2E6E11286A25759149D4F9638;
extern const uint32_t g_rgctx_ReadOnlySequence_1_Seek_m8D6CAFC5AFA2C0A1ADD8D49D824B8B91E3459030;
extern const uint32_t g_rgctx_Unsafe_As_TisReadOnlySequence_1_tC8AAE38B86B84A0F26E77F21498DB77C60A0AAF0_TisReadOnlySequence_1_tC3D00D191DBB156549182F67D40D3919D974CF7D_mE029393D2D8B18D5362E12BF11652BAD4108D915;
extern const uint32_t g_rgctx_Enumerator_tEF464159D9D410690C5DDE7A76294206B7079403;
extern const uint32_t g_rgctx_Enumerator__ctor_mF859469B6399826B83E9178B3B199A93036AB21E;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetPosition_m52E45EF999670A7968C1B1C6D97904DBA18DC9A3;
extern const uint32_t g_rgctx_ReadOnlySequence_1_TryGetBuffer_mF39E0C982A5D8130941A2A204643D8A68B93B85D;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetSequenceType_m3F244B819DEE94AEC24F2D277398EF33D5C21BEA;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_Slice_m2897D8BD77DD48CBEC609439638C2134F687A791;
extern const uint32_t g_rgctx_ReadOnlyMemory_1_Slice_mAB683743476D6B3AEF3DA807630CA0DC1E819AA6;
extern const uint32_t g_rgctx_TU5BU5D_tECBB295BE2720697DFCBBF611CC1D8EF4D8F792B;
extern const uint32_t g_rgctx_ReadOnlyMemory_1__ctor_mF1DE0D0C95518906FD0587B1E9CDB8C14FC0C1CA;
extern const uint32_t g_rgctx_MemoryManager_1_tA7AE4E9FE38806DED755EF302EBA858A3A3F5DCD;
extern const uint32_t g_rgctx_MemoryManager_1_get_Memory_m60FC03B2ABC55B8E8B4EB374FCCDD5B1D01575CC;
extern const uint32_t g_rgctx_Memory_1_Slice_mCC90B208E99FBA8B9CED6F0955F65A90C0D06191;
extern const uint32_t g_rgctx_Memory_1_op_Implicit_m5B56017EBE7832225F91452A8900B811DF3B0D07;
extern const uint32_t g_rgctx_Memory_1_tC5F3C26478F966A638B77461516BBCE0EA2FAA46;
extern const uint32_t g_rgctx_ReadOnlySequence_1_tC8AAE38B86B84A0F26E77F21498DB77C60A0AAF0;
extern const uint32_t g_rgctx_ReadOnlySequence_1__ctor_m83BA962C534A18ABFCEF32C657833DB2B64B674A;
extern const uint32_t g_rgctx_ArraySegment_1_t09696D9FBD4441A1F6BD27D90F702BFF5C2BF524;
extern const uint32_t g_rgctx_ArraySegment_1__ctor_m0A695BFE132A433308FB9777CC7BBD7021159B9C;
extern const uint32_t g_rgctx_PerTypeValues_1_t95E5BD6B4D157AD7D8CDF02D592B7303B5981D33;
extern const uint32_t g_rgctx_ReadOnlySequence_1__ctor_m6AE9EB114628AD63C7BDCE7A962E685413C7EEC2;
extern const uint32_t g_rgctx_ReadOnlySequence_1_get_Start_mA8E68769C3DCE9614C2C4D7713200A8127BBFAA7;
extern const uint32_t g_rgctx_ReadOnlySequence_1_TryGet_m0B4D7AB5396E17D3A5DFA2CA29BD0F545DA4C3D8;
extern const uint32_t g_rgctx_BuffersExtensions_ToArray_TisT_tE97759A3E568839F62B95AFDAB47DDDE67033BE0_mB78DA8710B0847A531E4E3F87D21177B6874AA4F;
extern const uint32_t g_rgctx_ReadOnlySequence_1_GetEnumerator_m94121CBCFF671F2E688F8A557FC82FDA063E5555;
extern const uint32_t g_rgctx_Enumerator_get_Current_mAA103D85D426B4ECA216C58A448647F4CFD6D916;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mB7C8658584B88B9FD0CC58F70755E2016C066464;
extern const uint32_t g_rgctx_ReadOnlyMemory_1U5BU5D_tD428111280F06BE70A0FC4D9B9859932953F8933;
extern const uint32_t g_rgctx_ReadOnlySequenceDebugViewSegments_set_Segments_m36AE636959F6D24399C8589EF9496F5289066E21;
extern const uint32_t g_rgctx_MemoryManager_1_GetSpan_m44A425AA164CD1266EE05F954925F7D240D928BE;
extern const uint32_t g_rgctx_Span_1_get_Length_m1551E254A0E69173CC0379C1BA40F14548BC91F4;
extern const uint32_t g_rgctx_Memory_1_tC94D8955F275340D6F6D1F74B667CEB435366045;
extern const uint32_t g_rgctx_Memory_1__ctor_m25CA6DA52DB413CD969AEEEF1CAB030A474201B5;
extern const uint32_t g_rgctx_MemoryManager_1_Dispose_mC1319346FFD8E9A0C41C714B6EB65978B9B2EAE6;
static const Il2CppRGCTXDefinition s_rgctxValues[367] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ThrowHelper_CreateArgumentValidationException_TisT_tD37E3D92CEAC2553F97A51BBCE647474D370296B_m9396274002331B5B0C21AE9B54B507EEF207BFE3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceSegment_1_get_RunningIndex_mFB8F398796A1A99728AE1F12C46E40CE9444EF0A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceSegment_1_get_Memory_m70DC7FEB736CAEB2D45E6180FD6B637933AE05B5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_get_Length_mA495E727F64516AAC0350E3F9147510FA49F342C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t4118076D1C03DAF417A6A255333150C0BEE630EF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_t3A0A60ADC7935AF9DE308A680D9B70C62E92782B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1__ctor_mE549A3293F82A6B5C8215DAF003C06C6DA38D4A3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_m4D01D83421566B675B35834C4B359557ACCA3810 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_m6FD61751F51F15A80DB4A4021122F467464809C4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Count_mF879105DAA47623ED2CB4F88FE73871DD430C569 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1__ctor_mC430E5D525FF8E58D52838B8F378530A9AFB75B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisMemory_1_t3A0A60ADC7935AF9DE308A680D9B70C62E92782B_TisReadOnlyMemory_1_t3282E1E3B6DF228012160DE3206F824E75C59BAA_m8DA6F3D2788D1671CCC58B66ED44D193F2E0A7B8 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_get_Span_mC68A1D6DACBE70CE38267D188820CFA781FB3FB4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t099D30BEC3819DB69EE680016717913BF8E4F640 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1__ctor_m1DF978D6C0772AA97C73AA5616965DAAD526525F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_tF531F6EA7FCABE463E37DBD205FE0602AEF9D449 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_GetSpan_m6A2A899D4BE5CA03912E8A2C290BEAE378AAF422 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_Slice_m1604BEC6EFC3319F9D7C4CF8674798DDCB166CE7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_tC4452E8B91F888C5C788D2104CF367AA830F02DF_mC3D0D1597A7C0D71B68AF5D0E6A9C29D767D10CD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_mA88348CBFFC6BAF81B4B7B4B72E125FB11625EC9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t4118076D1C03DAF417A6A255333150C0BEE630EF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_mF92E83D721B00D9571C8B1EF22649ED336180BA6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_CopyTo_mB413006119D0729D11D11263D3265DE72ADEB0BE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_TryCopyTo_m9A537879243CA7E9C806CAB955D1E5C1C85E6094 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_Pin_mD6774D707B5765DB825EC6EF4F6A202F5823777B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D_m9AF7D354CA428C65FD10D78DE6A6CF2B9F4B0DF1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Implicit_m1F205ED2E54877B927BB4E4FB0B3CA0E9D3E5B6F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t099D30BEC3819DB69EE680016717913BF8E4F640 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D_mF1301A3C3CA3CE3D60C4CFF2EF152FD59604A0B6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AsPointer_TisT_t78F669B18FCBC4C63AE10A3FEDC0A2B5987D6F8D_m43766C9401D9EF8230A9577AFCFD466B51B09C99 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_ToArray_mFD2D395C4E5AB0730CCDBE25C7882F26A288973E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_t3282E1E3B6DF228012160DE3206F824E75C59BAA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_op_Implicit_m591DD76ED9838B17C50A32CB26E9CC27BC96957C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_t3A0A60ADC7935AF9DE308A680D9B70C62E92782B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_Equals_m55F19559EE9D04C16012019E67FDF49D65B9D6BC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_Equals_m0C68E58D8165F0062FFF7A1A44D967A9ED6E64C1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_CombineHashCodes_m65FF43E7D0D4989BD77D27A7071CB41A1F408F66 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_CombineHashCodes_mC2B5C725E2EF01252C44316547CB90A8A794DCD4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_op_Implicit_mECC52CE5EB3F43B8AB582DE0B5C605D55199F94B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_t6148C70E4F9FC281D85385037D846B0B063E7E92 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_ToArray_m2F1B5777753F3BD1A4C54E7993A4D051EAC1356D },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_m487629DCCCCF8779F78931E150ACC143560BDFAD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mF5D3FBFE0CF2DB286CA445EBAE8F2F0B1CE00775 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Length_m1661D08289A5DA205642B9CDE82799EBCB3280F4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_mCC4B9EF9BEC5E097B5F8A213A3149D31EA768DE9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IndexOf_TisT_t6A949C42C4BDF88E8BC62FD7CB9E25B3B9FDFC23_m75CA9E23D6DEF41F9389F83CD13E27FAED79CC56 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Length_mAA25731D6369F29AA71BA9BD2394893E8F7E7820 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryExtensions_IsTypeComparableAsBytes_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_m18133260D0645715B138549E3CF8C87BBEADF525 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_m395EB745D44789078F6E15DDD15D8DB9E7FB210C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_mC514A25C1A510ACE264C08DB6A0114FCACBC1357 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m63B02596D37875137ACADA7027CCE3516392EAC4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_m5FD6BBD83D2AC99FA8D9382B85B5717206753FEC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_SequenceEqual_TisT_t3C2BF225DFDC269B92576C54AB2A45E0C3439DB3_m2B6E1AA73E28AA0C07EBC27CF41806D5E6A01418 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t23B3078B6BDD3687722BF7D7D921089EFA84456F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_m415FCA9333308EBEDDA6A7D320A226696F41819D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mDCED40935168C00CAEC5807650BF052210CA73C8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_m692B9687061BCDFDCE6C8357E77220636AE78BD8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m28CA58D74401581514AA5A35598EC35EBFE2670C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IndexOf_TisT_t23B3078B6BDD3687722BF7D7D921089EFA84456F_mBB85899BBE4E38659A2FB77E0121CE97226412C0 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282_mAE5A00AE96D27D73FB0994C45F05EB7D703749C2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mAF70E2B6324F632D7198DCF51DA150886334D418 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_mECF24D29FE0A504D57F9C8840847D88E942AA0EB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IndexOfAny_TisT_tC9B5E097F0856A6C95B1E4AAAB8A69EF37362282_mD4B02132C59502C17749B661F5D1DC34F8AEDDD9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_m88C7B36148B0458F30D9C748749C0E67E7DB99B6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tA342E96AD4DE781D4419A86502284C3B756E5E78 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryExtensions_IsTypeComparableAsBytes_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_mBD0CC95AA81766D034F494FF24C17E60D28771C5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_m6FC3578929376E21567ADBF4E789784704487DB5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m8B267986758CC788674A6D4ADAFC16331A16B9D0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_SequenceEqual_TisT_tA342E96AD4DE781D4419A86502284C3B756E5E78_m870FC7DD7A20D0FBDFB2D599560EFB2394B7C2F3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_m0C026C9D253E610988836E46F0513D7D5C76495F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryExtensions_IsTypeComparableAsBytes_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_m5C613B7D7134917F16B73380ECC5DA27A15EF3F4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_m39BC4C2104F7C4FF5FB7D041670E533F4E6F2562 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m383847B46FEF56417C3157E729EFB1AEF0C6F3B4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_SequenceEqual_TisT_tBDDB55A7C0D935702EF500BEFCC6B2160DF1DCA2_mCA389B52683DE699265EE825F02D05B961731E7F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t7CD08B30C6FAD780BCE45FC30ED91FAF3667C58A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_m3C4D88A58DDF9C0390FBD4BCDF64139F835207AC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_m4AC94E6A2641753E59CC981403D066F65DCCFA22 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_m9F754AE3DB448F6ECFB8B9FB5ED7581A7E9A9E70 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Count_m67F2FF6A8C2C986DCEE4DCB95FE8D97EE3D64C3D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_tC7E6CF7F493B0EB2659C9102499E6A50CEC930F7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_m0F0718BD3992E033752C55EC16311644A209587A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_t092E086E499AE1CA5BBDDD6B0E6791585FB0F893 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1__ctor_m087F27C2E987355D069E96B6E13D3D8EE28E54A8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_tF89C8D40FB64990911F669A7342596F35CF99586 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1__ctor_m2AF025C7CE4D0D7790185A1F922753A9877F16DE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_t6D304187EA681023F74EFF5AEE5793A2F3682FBB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_mC076E6AC769C30EF58C6D06B860C02349FEBB363 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_CopyTo_m2CB83AC9724B33082459A071BD31682BCDF5EF9C },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tD779522ED3B77598B82D881C830AE8FAA64B62B8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_Create_m500272C0B1610C35C7EBA9A8D017E204055DF004 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_tE3F6AC38A1D5A5FAA6ABC9C909561C618DE30305 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_t6E998EDEA7C2DBE3A478C88DB747BF881CF18F13 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1__ctor_m70764FE8621671C4FB86AF3326016310ACD70FB0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_mC23058C409995962E49C2A8F16736A803BEA359D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_m30DCA488C1D1C2F0B83ACBC02A90C4F478D9677D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Count_mCDE832A11247B5D3D87953B6914EEAD6B8BECB7B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1__ctor_m6B146ACF0AD6148607FDB20B777818617F21B067 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_get_Span_mDB48EBC36632B89B96E8659934DB70C1F9FED4B6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_t36EDA418209E868BD997706C9559CA7FAEADB7D9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1__ctor_mDF91B164AE0E34507176F081E44FA26BE0E71D18 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_tB1368C4BFE321A3BE8A22EBAB44D53A81F4B0F69 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_GetSpan_m32AFD05CEDBA513833653B12111FD53516953A9E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_Slice_mCDFA85A97B0021B59000D6660C3C497D4C860894 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Implicit_mF29436A68E847F64589A7D66980A106CDFCC2F54 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t56D319E892EC8D5D6D7727D4D2B62A18D0D1943F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_t378098EA91E1792B7730CA7E433BA330E594A9D7_mA31DC817A54E414AD2256B14D1C8FE678D784916 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_mF1E5E6B0D69712D6EC3AC9C84C7793F78480EAC9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_Slice_mC16194596903988CBE768B315636C49714E21747 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t989B6BF2A41E04B9CF2C01C0EE5C92E30C4595EC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_mC17A8F7D5C4C3048EC6EF84F70E506DE71B61904 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_get_Span_m1FA547C868BA1A89D567BE9C2D1B9038A20A3E84 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_CopyTo_mAD5AC5A8299CB11BF2937E8E750390A05F1AC3AB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_TryCopyTo_m04F28536FEBA57DF3CC731A0F58AFED8B1F4123C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_Pin_m9F9C6F3127F412A9CA2598378137F19865A208DA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE_m2182E9223CE10120BA2F2D6A65F10A3185D814FC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Implicit_m83ACFAF76356ADE20E08D2242FC7C6BA2930C159 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_GetReference_TisT_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE_mB2A605C1C77975837143736FE1DA8A80FDF47E1F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AsPointer_TisT_tB63B85BD81C43F23F17AAE68DCD8BA15DE0211BE_mA7F19DD21041F8A1D528081D99EB724FBEECCD04 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_ToArray_m4F43F7B8F3EB8B1EC00C915CF2619E0C1D65D860 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_Equals_m97826DCB9FD7DC70424D510FA10FC372DC30D517 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_tB976783BB63A449DD3D82C77ABD40B09AF0BAE0B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_op_Implicit_m02BAD7865E0F5958DB771B5CF15D1AEE28DDEAF7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_tB976783BB63A449DD3D82C77ABD40B09AF0BAE0B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_CombineHashCodes_m1E98C0191C4F0E97D34755916063AAB1CCCB1781 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_t6E998EDEA7C2DBE3A478C88DB747BF881CF18F13 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_CombineHashCodes_m92CF7AD651BE182BDAD998052FFB29A87B4D3BC6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_op_Equality_mB801A1E859548B993E967ABD4F7EAD7716C2B6AD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_t91CD2A7485A168014F77608661B4E0E165CE2019 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_t91CD2A7485A168014F77608661B4E0E165CE2019 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_mC468AE6DB70AC9BA8B25E24D4EA423180C371707 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_m3A824F29EEB9A15E6EEF841D8D69BA623B70C643 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_mA4E85539B7C95D4155EAEE6322B7DE36F81776C9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Count_mC9A5AD9579086A699CD569B107FF7C33CE0487FB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_m355E77DE8E48E9FEE504D3FD462DCADC544CA358 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_tAB5FE10B43025C0D260C13682B70428E180C1903 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator__ctor_m4E1DE54780585BEA303E4B375F122C3A63AB2949 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_t9F8C6029FCC8C6FFC1758B25175A69D2E7C31A2C_m3E6C95D5329759E428FB9E74FAD2A0F6AE197E29 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_t3C6F41E20B58DF9D7A987D6F0F4BFA7315DB29EE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_Add_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mBF6CD105D49DAFBEA065AF4A9ABB0F61EBEF9629 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mD1364F8974D78E6AB926DEEE0D94FC9DD1640BF6 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t555E257E7DBB4DBED5A1334602417F3F53E59A8F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AsRef_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_m78DE6AF2042F5A4BA1168A981DCE8D1AE96149F5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_m20270E7584E4CD7BAA02B5E903C9FC42B017E701 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AddByteOffset_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mF5A80B2D7F96E2EA8A72C525459475EA95BC3C75 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_TryCopyTo_m74D59A26752BE2078C5139924083179299B4E8A3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Length_mAA3D2E88E5973D20BD442AA39929581446ADDC22 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_DangerousGetPinnableReference_m44E613AF004F4EB1241F9AF903EA1DE490CBDC10 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_DangerousGetPinnableReference_m28D20B27F95F7960C6AE76D7F8AA60E93F1BE314 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_CopyTo_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_m7FA9C80315B2DAAC3AA46AE38A804AB6756D5317 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AreSame_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_mF6EE96C9C4F25050B3A75BEC1C07692365DDA407 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t555E257E7DBB4DBED5A1334602417F3F53E59A8F_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m3E4020A59A6E449C3AAE43DD259C3624FECC557B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_mC7F4A0ED06859379B46C661131ACDB9EF4A12F06 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t8E11F4C8BD801DCCCC1464E919B0FD9948F6119A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Implicit_m7CDDA1C94D4A5A7E6B65FC60CACD5A70A0D90165 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_tB08EE97D87AB966193DAF63C784C9CC4003678E1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_CopyTo_m6C0A777827557D83E88319D26647C52E2F272C2F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Equality_mF67D8FA34F5624F83174DF2E9D812919DE965C67 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t9214D4F1438E53CA54A3C870FA56F1E52FA8BCC2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t9214D4F1438E53CA54A3C870FA56F1E52FA8BCC2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_mF0D60C1382EFF0415FEC92F5DD49E129A698EB51 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_mCD4BFDDD0C670FC8AEE34C2AE4D5FFFBC71955B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_m23EE07D26588FB2F1CC39DD866E45A144441D7A9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Count_m856156028141333C01F261C5B9716676D6C41488 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_m93E09E5F8E4E699E655329D5F9C7B6BAA7380BE5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t47768893B54EC15B452803B09617FB72B1D37DF4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator__ctor_m762D4A3BBD947E8BB43F2E7331BA32023CD2B1FA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t3FFAFE11C71F64BD573D9783635D59A89C81CD22 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t6215054E5C82A201EA1F10DA7F8797B794134DA6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_t23C5B5D7226DD19588BBF94B58DF9CD91F51D7CD_m7117CA6723E2766F7ED4656FF8CC3880B6D930A7 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_t6F5DCC1D6536B5D2AB3551EB44C9A014D810AA1E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_Add_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mA6483C4D7A67BF654025373656E3C9ED6A00BF89 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_m6850B6A1A6755DD1E6A11A16C3494CDC9952C42D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mEE3340E07EBF53314BCACC1EDA5BBF2708D881DA },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t3FFAFE11C71F64BD573D9783635D59A89C81CD22 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AsRef_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m094E088627897C688694A0068DFD6F3529D20060 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mBFCC7873F5F1BB35435E9B604ACE71964F3125EB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AddByteOffset_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m4DC82784661C3C9F3FDCADD520BB63F8EAE9A863 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_mDCE918D103555011799F364EADF2D29B946588C3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mD3F9A5804D1A1F063208B9C3E149895ED713B4D0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_DangerousGetPinnableReference_mA002A9003AC104718C4487320AF9B106B1AFDFED },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_TisIntPtr_t_mAA61919605B05521702592DCC9DFF87B11E6056B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_TryCopyTo_m33061340B36235BC1DA613A099A4143A8526567A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_CopyTo_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m8B1BA5E54B80D9F343D2959F45BC7731164B5678 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AreSame_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_m74F9E8C03331887EC564A3C15CC2C78DC048F4A6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_t0E0BAF4A01D2FEF6F7BEE722FA7F2408C9EEF5BD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_m9F1FE95B304B9C711453EDFF7AB5F71A718CD7D6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_t3FFAFE11C71F64BD573D9783635D59A89C81CD22_TisChar_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_m28A532D44CD83CFA0075EEBCD8818637CA4B16B3 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t6215054E5C82A201EA1F10DA7F8797B794134DA6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Implicit_m6B77C0F0693EB9B2F0E40FD9DAF3B222A65DABC4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_CopyTo_mFB25ABBAF7D53E98FAF31248B5EB4551398805BE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_ToArray_m51F8C04DAB0860CCBB6454EB823C700739E09B23 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_ToArray_mED0753CEE34B306C98CB69309706B9B1057E3B5A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_t6CDA375BE0C2EE3B9E47FFFD98EAC42B20EAB896_mD760BED2B24FA7CA46C472D4306EF777B7F227CD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t6CDA375BE0C2EE3B9E47FFFD98EAC42B20EAB896 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_t9ADC3C011A4E4C7D9FC54B5830B7E3120D032478 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_Equals_m0F5089230F413D03AFE2576D8ED1538BE7566B8C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_t8477FE405833FB2B66C47191B0FAD170965A30EA_mDD4B96C2D162277E76E966FABCDD536076646727 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t8477FE405833FB2B66C47191B0FAD170965A30EA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_tBDA13BB1BF464F52E06EDBD759191A5E75676CD0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_Equals_m560BF984575EF1ED6A3EBA2C6616F332539AC89A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AreSame_TisT_t7AF0F4FB475CA8B86F0D30BC6B38C2AA1A43CAC2_m17A90560164268F745E41D7609234BF7C4051B25 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_t7AF0F4FB475CA8B86F0D30BC6B38C2AA1A43CAC2_mEDED094021E77580C9AC361E7C466D1CE9B13763 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t7AF0F4FB475CA8B86F0D30BC6B38C2AA1A43CAC2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_tD57BDBE84277CE34C40D5CE73C386B97AB282214 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_Equals_mE7D3B60D056823734A88E080B7B0F4EAEF610304 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_Add_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_m4B7424958CD1F89D4313A56A1E014A5A2B726BA3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_ByteOffset_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_m5DA471B8A15F72C25DC3F71483EDD830450AF725 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_m39BAA47C6FDA958FBE9CB37E78FF9FAA85BF0249 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisT_tB1AF327A50C4D6BB42372DF722AB207C9F8F9576_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_m518D6A5519D6DFFAA775BE7540087E98246FC6FD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisT_tAFCE6CD0B5764A0EE807B31E2E8DED86D24FB2BD_mE17E5F9AE11AE33359035249AEF159A6F5694F8F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_t4B33D1DABA77A801C021FEF65CCF9A727BE0B640 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t2F2F6AB68CDFB2823880B0538E6F0B0A52240F77 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_t2EA5ACA779A7D81D953593F484E91B12B39A98CE_m5E770E19AFEE24BE79CA6FC1018D2FF1311190B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_ByteOffset_TisT_tCAB09CA3EEBD1F00CA8B35C19742BEF19A762A8F_mD2C5894A63CFDF0A35315A17D0C333395B1A9D88 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tCAB09CA3EEBD1F00CA8B35C19742BEF19A762A8F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_t2D2B1745D83B705A22C721F976D97E22C98B6C4F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_MeasureArrayAdjustment_m53E4AE5B15698B7BCC81BEDB07790C9CD06FF6D6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_GetObjectStartLength_m16B54BF53654405683ACCC8519ED827F53CC71BD },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_tB26C300AF0074B34568569A53B57BCAAD87A4C1F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_TryGetArray_mFDF52D29025D8C2611281EFFBF950CCD4C587347 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_mF97EF25B285DBA2955571227D8069A2277CF87B0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_m7B38B7391F1C085E0AA4082B5604E5EA845C86B8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_t9301AA2774C3C80D9ABA093483D4D9FC775E8CEF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1__ctor_mE87E46F0DA62FD0DE9CA2F5236F1FA6C13B0810B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t5621C4B518D02AD0C4A007B7BFD26CD5F310370D },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_tBAC8327CFF46C4E28B21D534403653922B13F64D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1__ctor_m82253ECBC930F15ACFE2D60C275138060BB50CA8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_GetObjectStartLength_m48CB16E7B5730A751AACDF7F6DE97337EAE43F91 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TManager_tDB3B2604DFC72135387CCD59F9A56204EDCCB23D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5_m422DCCEE46BF0158EC7BA379E7E66A3F9EB25BDA },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisT_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5_m15575D27CAB8ABD64D12F0843973BEC3D393AFB5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_ReadUnaligned_TisT_tAA935C9B32A6E58186BC4F3DA62138AFB640AEE5_mF16AD3137E4B4EFBD44A26EA3B85E3EF48983EA7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772_mEAE9B32DCADEB1494974E5675DD87EF8A2945966 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisT_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772_m505D4758E3AFBBA5D132BC8662E4DA2F8FC12537 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_WriteUnaligned_TisT_tB9D6064FEA6C9628A593C08E13A0B27DF6FDB772_mB984422B110999FE678263ACF8A9D1A9DA5891B4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_tFA97F1007721867D582FB279D90E2DA497268BE5_m145E4DBBD465D16F1ECFB68DB33C525FB185468B },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tFA97F1007721867D582FB279D90E2DA497268BE5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisT_tFA97F1007721867D582FB279D90E2DA497268BE5_mA911846D1FFAEA0D3D8923DDE1B75C2238DC4B66 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_WriteUnaligned_TisT_tFA97F1007721867D582FB279D90E2DA497268BE5_m2928D90771017396385903BD62EC884D8CD4FB0E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisT_t22C5E087F231D0C06125CB0792E17BE40D2B7363_m685FD785FBC1F080E4CDC640D64BEAC8686483DC },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t22C5E087F231D0C06125CB0792E17BE40D2B7363 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_mC595F4FABD21BF2D111D1A361C4AE15675316C76 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisT_t22C5E087F231D0C06125CB0792E17BE40D2B7363_mAA07C15FCE5987BC0376997545441E4558C5B758 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Pinnable_mA961B5DB31E39C10135714DB5124BDF391599ECE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_ByteOffset_mDF9F27360C2596503A658990FB6ED5F5E7AC4C30 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Pinnable_m1976EB368514A3ADABA0723B0C55DC30D00B1133 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_ByteOffset_mA9EC00DBBC2C014D9EE93B786B5F07EF921E496E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AsRef_TisT_t21693D9B947761AB0FEF5F4E5D05DE3F5DE92CD6_m5107E32C7CD76572538EB43F5CEAF5D796C8A48F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AddByteOffset_TisT_t21693D9B947761AB0FEF5F4E5D05DE3F5DE92CD6_mAF0C5A22D1EBE16085A71D01CA8FE49CC65D43A5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Pinnable_mBFEC5EB55CE535660504F4C7D00E87C00E8EDEEE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_ByteOffset_m35D945231E703603597F15C3ECD5D2C303CAC1F6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AsRef_TisT_t41DDAA8BB090A6D561131FCEB35122A9971E40E6_mF1ADADBA7B6CE946B06C847179B694357EE4C82E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_AddByteOffset_TisT_t41DDAA8BB090A6D561131FCEB35122A9971E40E6_mDF58AF6E82384977F481C02C4FD7BF5B6B2212EB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTFrom_tE8CEFA5A69C5A19928463C4000F334BD0D0E588B_mAF8A62CE03196311E123AA9198FB7ED03738D800 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TFrom_tE8CEFA5A69C5A19928463C4000F334BD0D0E588B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTTo_t17CDBA7A02EC6F4A45DD5B76B6C105306386EDED_mD334DCAAD0884C4831A5423EDB17E8EC8F49DAA7 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TTo_t17CDBA7A02EC6F4A45DD5B76B6C105306386EDED },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Length_mFC3F5002B2184D5F5D37A48F825E4CAE321D0C60 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisTFrom_tE8CEFA5A69C5A19928463C4000F334BD0D0E588B_m02676CB3A79C71FE3FEB16DAEE068EA5496E9B80 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisTTo_t17CDBA7A02EC6F4A45DD5B76B6C105306386EDED_m6B1469BC5A712227F501D37D42AD14EE89922D8B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Pinnable_m9AA32FE22873A606F10A66F30826F1E69A37B18F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_t07C8A38CBAC2A98F6A9D201E7E0B871FEF0AB45B_m494B9A22F4AF3A54F67B0AE97D380F7E06A48B14 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_ByteOffset_mCF7447876C9858EC3825888B3B1CE1EDF0DF4792 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t458550BD8207AFC69D5AD7E833A6C453D5069501 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1__ctor_m8DCF397840ED7EEE989CECB3BF705216B509117F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTFrom_t53C5CC6DF64CBD3D7AF5B3EB33B03F3E4B7219CF_m11D0E4F9DCAD4C02D29FC6A16A8D77C8C0BF4A99 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TFrom_t53C5CC6DF64CBD3D7AF5B3EB33B03F3E4B7219CF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SpanHelpers_IsReferenceOrContainsReferences_TisTTo_t8E941AFE856E74DC2CAA108BAA5259F6BB3E6185_mED5C92204247416D4674A84F2392CD4E4ACEF2C8 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TTo_t8E941AFE856E74DC2CAA108BAA5259F6BB3E6185 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_mF7137315AE0F0D797C791B556FB97ACFF36E6DFD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisTFrom_t53C5CC6DF64CBD3D7AF5B3EB33B03F3E4B7219CF_mABE06CFC6E14B02AFB9FE074F48912ABC5CC1474 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_SizeOf_TisTTo_t8E941AFE856E74DC2CAA108BAA5259F6BB3E6185_m4EDA596E3918628648FB17BC781D8B5C56D32382 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Pinnable_m16063606B87A5885F8ECB66D14CFC72EB6E8BE09 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisPinnable_1_t9391C4657303A0C7EDCD82E69C020031E017094F_m3136269B0F638F2A3CCE92BAD874EFBFC015B153 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_ByteOffset_m8EC26C2E995BCB5FC1B86191367202134520CA57 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_tDAA19E0AAFB7F1CBD88F33A4541B540C3730A009 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1__ctor_m7D2F538D269EA652F1CC04A2569DA96866960FE5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_Length_m37C1382A39B2187170851BD7EA02345BD55ED73F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Length_m830234B00F16A58C26874135988312C3B8E226D3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_IsSingleSegment_m35E62D4A341DAC4509B717647D15D07D646AFCB8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_First_mBB85540F785798ACE473EA859DB3556FF16C9CE1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_get_Span_m80077198EB1B121C3D3C3F180E31466DF78D2411 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_CopyTo_m714F52B1D969DF522F89BFEE9F2F1CE44614D0DD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BuffersExtensions_CopyToMultiSegment_TisT_t06E68360FF5061F8178FF588235037DEAD443FEE_m7DCC5D906CEE3B78334C133D5CEE57A7E55B5B92 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_Start_mD2C1EB871926533B034891E6DAC9C3B4C20879CB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_TryGet_m555C28AFA5A15959F6C4EF564E2F973CFF3273D4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_get_Span_mF4427E92FB925C86C4DD66BE407BEEDEDA9814DD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_CopyTo_m623B5FB49C03737D16BD57B9CD24D113AC2B98EF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySpan_1_get_Length_m3ED118A8ECB714F4A313F99C805F0C647D1C9200 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_Slice_m8AA1289E6B67F659024C7E2ECC2B4DCBCC73A759 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_Length_m77ABDE2458034B8C46E952FD627B7CE72B0F3F12 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t718F0E16C7507AF6A1817312AC9CD9F72A003431 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_op_Implicit_m132A6353C862787039EB55423B98BD0DCC9E2D04 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_t517546D5EF44F1A726F338259119F2C277842506 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BuffersExtensions_CopyTo_TisT_tDDB0061770280A4FDC7746BAB73E4ABDF786097E_m13E5585A60F294FA88A34533107E8552132240BA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetLength_m2E42A8AC70A4F2A5AC2DB5A6F844CA7CF029ECAB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_Length_m8E0DD3D6C0A5F2B93E551BACE230DB2AC625D57F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetFirstBuffer_m6EA6CB0D473C0B712FA3BA782AA32A587FE94EA9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceSegment_1_get_RunningIndex_mEDDC617A31DAD38C9E23CA2F1B77423646A38C58 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceSegment_1_get_Memory_mA71F4FA1675C8769FBEA1F99BFE1DE8059F6BDDE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_get_Length_m4F26CA928CF49A14BA066B2ECF5ABA49E09B2869 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ThrowHelper_ThrowArgumentValidationException_TisT_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452_m7C1A3A1AF30D08DEA7AF3B4517D64F7C785B636A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_TryGetMemoryManager_TisT_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452_TisMemoryManager_1_tA7AE4E9FE38806DED755EF302EBA858A3A3F5DCD_mFA744E09F9C303712F6EFCB7EB84057924C7B203 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryMarshal_TryGetArray_TisT_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452_m7EF3299FC3F3039B7AFB98C92940729590951E21 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Array_m579F8F3547792AA3FDC7DD846AE9D03C2EA6DCB0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Offset_m5DC87CA276566E59192826DDEB589DEF07602AC0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_get_Count_m381403CEF16C8649B55EC3C8FB1213037234B404 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tE0B561F539B9E2B4E93BBA61F77E9DD6E16F1452 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_t4A19AD65C799CC80D2CD578AEC1DC2ADEB679B62 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetIndex_m9F52D416B3F9ACA06B5178D151C9E040AB720EA2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_tC8AAE38B86B84A0F26E77F21498DB77C60A0AAF0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceSegment_1_tFEC43C4C98DADFE39066BD549022F76364FD8434 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetEndPosition_mFE0824CE3BE464D14BF6C1A24FD7284E6A447F9C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceSegment_1_get_Next_m0BF61937A17CBE7D13934ED6DA1D6E3A7AB6BA9C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_SeekMultiSegment_m2E808C13152A0E4FE5811508E3D4B95BC18AF069 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_SliceImpl_m94A827AE2F1CEA08342BAB474A7F8119274BE44C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_InRange_m4006CEE42CA53B9F2F7FCDC2B906ADF477E2C762 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_InRange_m01D4A88370BCCF3BCC44C3F66DC7DC6A862ADA4B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_Slice_m56E356CA45D6857009217C36DF704EE677510725 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_Slice_m580BAE4438515E2080BCC9FE9884491335D4F13E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_Slice_m23AACF77AC1ECA667CD141E37E5198BE256CE38A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_BoundsCheck_m02D754A6D78B00DF01B0BB5A6E0C27230796C70B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_BoundsCheck_mE41AE054C026B4C2E6E11286A25759149D4F9638 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_Seek_m8D6CAFC5AFA2C0A1ADD8D49D824B8B91E3459030 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Unsafe_As_TisReadOnlySequence_1_tC8AAE38B86B84A0F26E77F21498DB77C60A0AAF0_TisReadOnlySequence_1_tC3D00D191DBB156549182F67D40D3919D974CF7D_mE029393D2D8B18D5362E12BF11652BAD4108D915 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_tEF464159D9D410690C5DDE7A76294206B7079403 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator__ctor_mF859469B6399826B83E9178B3B199A93036AB21E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetPosition_m52E45EF999670A7968C1B1C6D97904DBA18DC9A3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_TryGetBuffer_mF39E0C982A5D8130941A2A204643D8A68B93B85D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetSequenceType_m3F244B819DEE94AEC24F2D277398EF33D5C21BEA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_Slice_m2897D8BD77DD48CBEC609439638C2134F687A791 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1_Slice_mAB683743476D6B3AEF3DA807630CA0DC1E819AA6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_tECBB295BE2720697DFCBBF611CC1D8EF4D8F792B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1__ctor_mF1DE0D0C95518906FD0587B1E9CDB8C14FC0C1CA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_tA7AE4E9FE38806DED755EF302EBA858A3A3F5DCD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_get_Memory_m60FC03B2ABC55B8E8B4EB374FCCDD5B1D01575CC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_Slice_mCC90B208E99FBA8B9CED6F0955F65A90C0D06191 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_op_Implicit_m5B56017EBE7832225F91452A8900B811DF3B0D07 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_tC5F3C26478F966A638B77461516BBCE0EA2FAA46 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_tC8AAE38B86B84A0F26E77F21498DB77C60A0AAF0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1__ctor_m83BA962C534A18ABFCEF32C657833DB2B64B674A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1_t09696D9FBD4441A1F6BD27D90F702BFF5C2BF524 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ArraySegment_1__ctor_m0A695BFE132A433308FB9777CC7BBD7021159B9C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_PerTypeValues_1_t95E5BD6B4D157AD7D8CDF02D592B7303B5981D33 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1__ctor_m6AE9EB114628AD63C7BDCE7A962E685413C7EEC2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_get_Start_mA8E68769C3DCE9614C2C4D7713200A8127BBFAA7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_TryGet_m0B4D7AB5396E17D3A5DFA2CA29BD0F545DA4C3D8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BuffersExtensions_ToArray_TisT_tE97759A3E568839F62B95AFDAB47DDDE67033BE0_mB78DA8710B0847A531E4E3F87D21177B6874AA4F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequence_1_GetEnumerator_m94121CBCFF671F2E688F8A557FC82FDA063E5555 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_mAA103D85D426B4ECA216C58A448647F4CFD6D916 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_mB7C8658584B88B9FD0CC58F70755E2016C066464 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlyMemory_1U5BU5D_tD428111280F06BE70A0FC4D9B9859932953F8933 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ReadOnlySequenceDebugViewSegments_set_Segments_m36AE636959F6D24399C8589EF9496F5289066E21 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_GetSpan_m44A425AA164CD1266EE05F954925F7D240D928BE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Span_1_get_Length_m1551E254A0E69173CC0379C1BA40F14548BC91F4 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1_tC94D8955F275340D6F6D1F74B667CEB435366045 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Memory_1__ctor_m25CA6DA52DB413CD969AEEEF1CAB030A474201B5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_MemoryManager_1_Dispose_mC1319346FFD8E9A0C41C714B6EB65978B9B2EAE6 },
};
extern const CustomAttributesCacheGenerator g_System_Memory_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Memory_CodeGenModule;
const Il2CppCodeGenModule g_System_Memory_CodeGenModule = 
{
	"System.Memory.dll",
	443,
	s_methodPointers,
	27,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	45,
	s_rgctxIndices,
	367,
	s_rgctxValues,
	NULL,
	g_System_Memory_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
