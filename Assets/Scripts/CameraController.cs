using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject robot;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            var mouseX = Input.GetAxis("Mouse X");
            transform.RotateAround(this.robot.transform.position, Vector3.up, mouseX * 10f);
        }
    }
}
