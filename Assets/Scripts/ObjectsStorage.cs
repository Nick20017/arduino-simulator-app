using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsStorage : MonoBehaviour
{
    // Object remove options
    public GameObject optionsMenu;

    // Movable object remove options
    public GameObject movableObjectOptions;

    // Digital pins available to connect
    public List<short> digitalPinsAvailable = new List<short>() { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    // Positions available to put engines to
    public List<string> frontPositionsAvailable = new List<string>() { "Front left", "Front right" };
    public List<string> backPositionsAvailable = new List<string>() { "Back right", "Back left" };
    public List<string> wheelsPositions = new List<string>() { "Front left", "Front right", "Back right", "Back left" };

    // Tags to check if drop is allowed for robot items
    public List<string> dragDropTags = new List<string>() { "arduino", "wheels", "carBody", "carHead", "robot", "wheelEngine" };

    // Empty objects where to spawn a wire
    public GameObject[] digitalPins;

    private void Awake() {
        // Find all digital pin objects
        this.digitalPins = GameObject.FindGameObjectsWithTag("digitalPin");
    }
}
