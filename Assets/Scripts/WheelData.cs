﻿using UnityEngine;

class WheelData
{
    public int position;
    public int direction;
    public int pin;
    public bool isRotating;
    public bool isTurning;

    public GameObject wheel;
    public WheelCollider collider;

    private float maxTurnAngle = 30f;

    public WheelData(int position, int direction, int pin, GameObject wheel, WheelCollider collider)
    {
        this.position = position;
        this.direction = direction;
        this.pin = pin;
        this.wheel = wheel;
        this.collider = collider;
        this.isRotating = false;
        this.isTurning = false;
    }

    public void Rotate()
    {
        this.wheel.transform.Rotate(this.isRotating ? this.collider.rpm * -direction / 60 * 360 * Time.deltaTime : 0f, 0f, 0f);

        this.collider.steerAngle = this.isTurning && this.position <= 2 ? this.maxTurnAngle * this.direction : 0f;

        Vector3 temp = this.wheel.transform.localEulerAngles;

        temp.y = this.collider.steerAngle - this.wheel.transform.localEulerAngles.z;
        this.wheel.transform.localEulerAngles = temp;
    }
}
