using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectedItem : MonoBehaviour
{
    // Tag of current game object
    private string _tag;
    // Tag of preview related to current game object
    public string itemPreviewTag;
    
    void Start()
    {
        // Get tag of current game object
        this._tag = this.gameObject.tag;
        // Get tag of preview that's related to the current object 
        this.itemPreviewTag = this._tag + "Preview";
    }
}
