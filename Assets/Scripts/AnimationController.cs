using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public string position = "";

    [SerializeField]
    private Arduino arduino;

    private bool isRunning = false;

    void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = Vector3.zero;
    }

    public void Move(
        int position /* 1 - front left, 2 - front right, 3 - back left, 4 - back right */,
        int direction /* 1 - Forward, -1 - Backward */,
        int pin)
    {
        this.arduino.MoveWheels._Move(pin);
        this.isRunning = true;
    }

    public void Stop()
    {
        this.arduino.isLoop = false;
        this.arduino.MoveWheels._Stop();
        this.isRunning = false;

        foreach(var data in arduino.MoveWheels.WheelData)
        {
            data.Value.Rotate();
        }
    }

    public void Stop(int pin)
    {
        this.arduino.MoveWheels._Stop(pin);
    }

    private void FixedUpdate()
    {
        if (!isRunning)
            return;

        foreach(var data in arduino.MoveWheels.WheelData)
        {
            data.Value.Rotate();
        }
    }
}
