using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private GameObject prefab;

    [SerializeField]
    private Transform[] SpawnPoints;

    [SerializeField]
    private ObjectsStorage storage;
    
    private Vector3 startPosition;

    private Camera mainCamera;

    private Arduino arduino;

    private string[] dragDropTags = { "arduino", "wheels", "carBody", "carHead" };
    
    public Queue<Transform> spawnPointsQueue;

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = this.startPosition;

        if (arduino.isLoop) return;

        if (this.spawnPointsQueue.Count <= 0)
            return;

        RaycastHit[] hits = Physics.RaycastAll(this.mainCamera.ScreenPointToRay(Input.mousePosition));

        foreach(RaycastHit hit in hits)
        {
            if (this.storage.dragDropTags.IndexOf(hit.collider.tag) >= 0)
            {
                Transform startPoint = this.spawnPointsQueue.Dequeue();

                GameObject _gameobject = Instantiate(prefab, startPoint.position, new Quaternion(0, 0, 0, 0) /* startPoint.rotation */, startPoint);

                var animController = _gameobject.GetComponent<AnimationController>();

                try
                {
                    if (startPoint.name.ToLower().CompareTo("wheelsfront") == 0)
                    {
                        var props = _gameobject.transform.GetComponentsInChildren<ItemToMoveProperties>();

                        foreach(var prop in props)
                        {
                            if (prop.name.CompareTo("wheel_left") == 0)
                                prop.position = "Front left";
                            else if (prop.name.CompareTo("wheel_right") == 0)
                                prop.position = "Front right";

                            foreach (var objProps in FindObjectsOfType<MovableObjectProperties>())
                            {
                                if (prop.position.CompareTo(objProps.position) == 0)
                                {
                                    prop.pin = objProps.pin;
                                    prop.direction = objProps.direction.ToLower().CompareTo("right") == 0 ? 1 : -1;
                                }
                            }
                        }

                        if (animController)
                            animController.position = "Front";
                    }
                    else if (startPoint.name.ToLower().CompareTo("wheelsback") == 0)
                    {
                        var props = _gameobject.transform.GetComponentsInChildren<ItemToMoveProperties>();

                        foreach (var prop in props)
                        {
                            if (prop.name.CompareTo("wheel_left") == 0)
                                prop.position = "Back left";
                            else if (prop.name.CompareTo("wheel_right") == 0)
                                prop.position = "Back right";

                            foreach (var objProps in FindObjectsOfType<MovableObjectProperties>())
                            {
                                if (prop.position.CompareTo(objProps.position) == 0)
                                {
                                    prop.pin = objProps.pin;
                                    prop.direction = objProps.direction.CompareTo("Forward") == 0 ? 1 : -1;
                                }
                            }

                            if (animController)
                                animController.position = "Back";
                        }
                    }
                }
                catch (Exception e) {
                    Debug.Log(e.Message);
                }

                break;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.startPosition = transform.localPosition;
        this.mainCamera = Camera.main;
        this.spawnPointsQueue = new Queue<Transform>();
        foreach(var point in this.SpawnPoints)
        {
            this.spawnPointsQueue.Enqueue(point);
        }
        this.arduino = FindObjectOfType<Arduino>();
    }
}
