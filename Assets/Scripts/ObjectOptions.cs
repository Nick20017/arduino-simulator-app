using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectOptions : MonoBehaviour
{
    // Panel's title
    public Text title;

    // Object to remove
    [HideInInspector]
    public GameObject target;

    // Tag of preview of removable object
    [HideInInspector]
    public string targetPreviewTag;

    // Removes object (target)
    public void RemoveObject()
    {
        // Find preview image by its tag
        GameObject targetPreview = GameObject.FindGameObjectsWithTag(this.targetPreviewTag)[0];
        // Get spawn point of the target object
        Transform parentTarget = this.target.transform.parent.parent;

        // Add current spawn point to preview's queue
        targetPreview.GetComponent<ItemDragHandler>().spawnPointsQueue.Enqueue(parentTarget);
        // Destroys target object
        Destroy(target.transform.parent.gameObject);

        // Close a popup panel
        this.gameObject.SetActive(false);
    }

    // Closes popup panel
    public void ClosePopUp()
    {
        this.gameObject.SetActive(false);
    }
}
