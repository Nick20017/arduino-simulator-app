using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveItem : MonoBehaviour
{
    [SerializeField]
    private LayerMask layerMask;

    private Arduino arduino;

    private Camera mainCamera;

    private ObjectsStorage storage;

    private ObjectOptions objectOptions;
    private MovableObjectOptions movableObjectOptions;
    
    void Start()
    {
        this.mainCamera = Camera.main;
        this.storage = FindObjectOfType<ObjectsStorage>();
        this.objectOptions = this.storage.optionsMenu.GetComponent<ObjectOptions>();
        this.movableObjectOptions = this.storage.movableObjectOptions.GetComponent<MovableObjectOptions>();
        this.arduino = FindObjectOfType<Arduino>();
    }

    private void OnMouseDown()
    {
        if (arduino.isLoop) return;

        RaycastHit[] hits = Physics.RaycastAll(this.mainCamera.ScreenPointToRay(Input.mousePosition), 100f, this.layerMask, QueryTriggerInteraction.Collide);

        foreach (var hit in hits)
        {
            ConnectedItem connectedItem = hit.collider.transform.GetComponent<ConnectedItem>();
            MovableObjectProperties movableObjectProperties = hit.collider.transform.GetComponent<MovableObjectProperties>();
                
            if (connectedItem)
            {
                Transform parent = hit.collider.transform;

                if(parent.name.Contains("wheel") && (parent.name.Contains("collider") || parent.name.Contains("trigger")))
                    parent = hit.collider.transform.parent;

                // Pass current object to options menu
                this.objectOptions.target = parent.gameObject;
                // Pass preview's tag that's related to the current object to options menu
                this.objectOptions.targetPreviewTag = connectedItem.itemPreviewTag;
                // Pass title to options menu
                this.objectOptions.title.text = parent.tag;
                // Change visibility of options menu panel
                this.objectOptions.gameObject.SetActive(!this.objectOptions.gameObject.activeSelf);

                break;
            }
            else if(movableObjectProperties)
            {
                this.movableObjectOptions.SetParams(movableObjectProperties._tag, movableObjectProperties.pin, movableObjectProperties.position, movableObjectProperties.direction, hit.collider.transform.gameObject);

                // Change visibility of remove object options panel
                this.movableObjectOptions.gameObject.SetActive(!this.movableObjectOptions.gameObject.activeSelf);

                break;
            }
        }
    }
}
