using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObjectProperties : MonoBehaviour
{
    public string _tag;
    public short pin;
    public string position;
    public string direction;

    private void Awake()
    {
        // Get tag of current game object
        _tag = this.gameObject.tag;
    }
}
