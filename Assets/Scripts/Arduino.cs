using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino : MonoBehaviour
{
    public bool isLoop = false;

    private MoveWheels moveWheels;

    [SerializeField]
    private GameObject codePanel;

    [SerializeField]
    private GameObject stopButton;

    [SerializeField]
    private ErrorPanel errorPanel;

    private AnimationController animationController;
    private ItemToMoveProperties[] props;

    private bool isRunning = false;
    private string output;

    internal MoveWheels MoveWheels { get => moveWheels; set => moveWheels = value; }

    private void Update()
    {
        if (this.isRunning)
        {
            StartCoroutine(Run());
            this.isRunning = false;
        }
    }

    public void Run(string output)
    {
        this.output = output;
        this.isRunning = true;
        this.moveWheels.animationController = this.animationController;
        this.errorPanel.errorMessage.text = "";
    }

    public IEnumerator Run()
    {
        if (this.output.ToLower().Contains("error"))
        {
            this.errorPanel.ShowPanel();
            this.errorPanel.errorMessage.text += $"{this.output}\n";
            this.isRunning = false;

            yield break;
        }
        else
        {
            this.codePanel.SetActive(false);
            this.stopButton.SetActive(true);
            this.isLoop = true;

            this.animationController = FindObjectOfType<AnimationController>();
            this.props = FindObjectsOfType<ItemToMoveProperties>();

            // Splits start and loop methods
            string start = output.Remove(0, "start\n".Length).Split('^')[0]; // Saves start method
            string loop = output.Split('^')[1].Remove(0, "loop\n".Length); // Saves loop method

            string[] splittedLine;
            string function;
            string _params;
            string[] func;
            string[] parameters;
            int pin;

            foreach (var line in start.Split('\n')) // Processing of start method
            {
                // Splits into function name and its parameters
                splittedLine = line.Split(':');
                function = splittedLine.Length > 1 ? splittedLine[0] : ""; // Saves function with related pin
                _params = splittedLine.Length > 1 ? splittedLine[1] : ""; // Saves all the params

                func = function.Split('-'); // Splits function name and related pin into array
                parameters = _params.Split('-'); // Splits parameter names and their values

                if (func.Length % 2 != 0 || parameters.Length % 2 != 0 || splittedLine.Length < 2) // If one of array has odd length, so there's something wrong. One pair is not complete. Then skip iteration.
                    continue;

                pin = Convert.ToInt16(func[1]); // Save related pin

                if (func[0].CompareTo("pinMode") == 0) // If the current function is pinMode
                {
                    bool pinFound = false; // Boolean to check if given pin is present on model

                    foreach (var prop in this.props) // Loop through all ItemToMoveProperties objects
                    {
                        if (pin == prop.pin) // If pin is found
                        {
                            pinFound = true;

                            for (int i = 0; i < parameters.Length; i += 2) // Saves all the parameters
                            {
                                if (parameters[i].CompareTo("mode") == 0) // If param name is 'mode' then save its value as INPUT or OUTPUT
                                    prop.mode = Convert.ToInt32(parameters[i + 1]) == 0 ? PinMode.OUTPUT : PinMode.INPUT;
                            }
                            // And break the loop, so it's no longer needed
                            break;
                        }
                    }

                    if (!pinFound) // If pin wasn't found, then print some message and break processing
                    {
                        this.errorPanel.ShowPanel();
                        this.errorPanel.errorMessage.text += $"Pin {pin} not found!\n";
                    }
                }
            }

            if (this.errorPanel.errorMessage.text.Length > 0)
            {
                this.codePanel.SetActive(true);
                this.isLoop = false;
                this.stopButton.SetActive(false);
                yield return null;
            }
            else
            {
                // Dictionary to store all the wheels
                Dictionary<string, WheelData> data = new Dictionary<string, WheelData>();

                // Looping through all wheels that are added on the scene
                foreach (var _prop in props)
                {
                    // Getting position of the wheel
                    string position = _prop.position.ToLower();

                    // Saving wheel object
                    var wheel = _prop.gameObject;
                    // Getting its wheel collider
                    var collider = _prop.transform.parent
                        .Find("wheel_" + (position.Contains("left") ? "left" : "right") + "_collider")
                        .GetComponent<WheelCollider>();

                    // Creating WheelData object with current wheel
                    // Saving position number, direction, pin, wheel object and its collider
                    WheelData wd = new WheelData(position.CompareTo("front left") == 0 ? 1
                        : position.CompareTo("front right") == 0 ? 2
                        : position.CompareTo("back left") == 0 ? 3 : 4,
                        _prop.direction, _prop.pin, wheel, collider);

                    // Switching through position and saving to dictionary by related index
                    switch (wd.position)
                    {
                        case 1:
                            data.Add("front-left", wd);
                            break;
                        case 2:
                            data.Add("front-right", wd);
                            break;
                        case 3:
                            data.Add("back-left", wd);
                            break;
                        case 4:
                            data.Add("back-right", wd);
                            break;
                    }
                }

                // Creating MoveWheels instance with all the wheel data saved
                this.MoveWheels = new MoveWheels(data["front-left"], data["front-right"], data["back-left"], data["back-right"]);

                while (this.isLoop) // While user didn't press STOP button to stop running code
                                    // Processing everything that's inside loop method
                {
                    foreach (var line in loop.Split('\n'))
                    {
                        splittedLine = line.Split(':');
                        function = splittedLine.Length > 0 ? splittedLine[0] : "";
                        _params = splittedLine.Length > 1 ? splittedLine[1] : "";

                        func = function.Split('-');
                        parameters = _params.Split('-');

                        if (func.Length % 2 != 0 || (splittedLine.Length < 2 && func[0].CompareTo("delay") != 0)) // If one of array has odd length, so there's something wrong. One pair is not complete. Then skip iteration.
                            continue;

                        if (func[0].CompareTo("digitalWrite") == 0)
                        {
                            pin = Convert.ToInt16(func[1]);

                            foreach (var p in this.props)
                            {
                                if (p.pin == pin)
                                {
                                    for (int i = 0; i < parameters.Length; i += 2)
                                    {
                                        if (parameters[i].CompareTo("level") == 0)
                                            p.level = Convert.ToInt32(parameters[i + 1]) == 0 ? PowerLevel.LOW : PowerLevel.HIGH;

                                        if (p.level == PowerLevel.HIGH)
                                        {
                                            string position = p.position.ToLower();

                                            this.animationController.Move(
                                                position.CompareTo("front left") == 0 ? 1
                                                : position.CompareTo("front right") == 0 ? 2
                                                : position.CompareTo("back left") == 0 ? 3 : 4,
                                                p.direction, pin);
                                        }
                                        else
                                        {
                                            this.animationController.Stop(pin);
                                        }
                                    }

                                    break;
                                }
                            }
                        }
                        else if (func[0].CompareTo("delay") == 0)
                        {
                            yield return new WaitForSeconds(Convert.ToInt64(func[1]) / 1000);
                        }
                    }
                    yield return null;
                }

                this.animationController.Stop();
                this.codePanel.SetActive(true);
                this.stopButton.SetActive(false);
            }
        }
    }
}
