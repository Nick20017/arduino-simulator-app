﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class MoveWheels
{
    public int pin;

    public float maxTurnAngle = 60f;
    public float breakTorque = 1000f;
    public float motorTorque = 500f;

    public AnimationController animationController;

    private GameObject wheel;
    private WheelCollider collider;
    public Dictionary<string, WheelData> WheelData;

    private int direction;
    private int position;

    public MoveWheels(GameObject wheel, WheelCollider collider, int direction, int position, int pin)
    {
        this.wheel = wheel;
        this.collider = collider;
        this.direction = direction;
        this.position = position;
        this.pin = pin;
    }

    public MoveWheels(WheelData frontLeft, WheelData frontRight, WheelData backLeft, WheelData backRight)
    {
        this.WheelData = new Dictionary<string, WheelData>();

        this.WheelData.Add("front-left", frontLeft);
        this.WheelData.Add("front-right", frontRight);
        this.WheelData.Add("back-left", backLeft);
        this.WheelData.Add("back-right", backRight);

        this.animationController = GameObject.FindObjectOfType<AnimationController>();
    }

    public void _Move(int pin)
    {
        List<WheelData> backWheelDatas = new List<WheelData>();
        foreach(var data in this.WheelData)
        {
            backWheelDatas.Clear();
            foreach(var wd in this.WheelData)
            {
                if (wd.Value.position > 2)
                    backWheelDatas.Add(wd.Value);
            }

            var value = data.Value;
            value.collider.brakeTorque = 0f;

            if (value.pin == pin)
            {
                if (value.position > 2)
                {
                    value.isRotating = true;
                    value.collider.motorTorque = this.motorTorque * -value.direction;

                    backWheelDatas.Add(value);
                }
                else
                {
                    value.isTurning = true;
                }
            }
            
            foreach (var bwd in backWheelDatas)
            {
                if (bwd.isRotating && value.position < 3)
                {
                    value.isRotating = true;
                    break;
                }
            }

            if (value.pin == pin) break;
        }
    }

    public void _Stop()
    {
        foreach (var data in this.WheelData)
        {
            data.Value.isRotating = false;
            data.Value.isTurning = false;
            data.Value.collider.motorTorque = 0f;
            data.Value.collider.brakeTorque = this.breakTorque;
        }
    }

    public void _Stop(int pin)
    {
        foreach (var data in this.WheelData)
        {
            if (data.Value.pin == pin)
            {
                data.Value.isRotating = false;
                data.Value.isTurning = false;
                data.Value.collider.motorTorque = 0f;
                data.Value.collider.brakeTorque = this.breakTorque;

                break;
            }
        }
    }

    /* public void Move()
    {
        Vector3 temp = this.wheel.transform.localEulerAngles;
        temp.y = this.collider.steerAngle - temp.z;
        this.wheel.transform.localEulerAngles = temp;
        wheel.transform.Rotate(-this.collider.rpm / 60 * 360 * Time.deltaTime, 0, 0);

        if (this.position > 2)
        {
            this.collider.motorTorque = this.motorTorque * -this.direction;
            this.collider.brakeTorque = 0f;
        }
        else
        {
            this.collider.steerAngle = this.maxTurnAngle * this.direction;
        }
    } */

    /* public void Stop()
    {
        this.wheel.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        this.collider.brakeTorque = this.breakTorque;
        this.collider.steerAngle = 0f;
    } */
}
