using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject infoPanel;

    [SerializeField]
    private Text infoLabel;

    private void ShowInfo()
    {
        string info = "";
        MovableObjectProperties[] props = FindObjectsOfType<MovableObjectProperties>();
        foreach(var p in props)
        {
            info += $"Name: {p._tag}\nPin: {p.pin}\nPosition: {p.position}\nDirection: {p.direction}\n\n";
        }

        infoLabel.text = info;

        this.infoPanel.SetActive(true);
    }

    private void HideInfo()
    {
        this.infoPanel.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            this.ShowInfo();
        else if(Input.GetKeyUp(KeyCode.Tab))
            this.HideInfo();
    }
}
