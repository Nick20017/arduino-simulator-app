using UnityEngine;
using UnityEngine.UI;
using SocketIOClient;

public class CodeController : MonoBehaviour
{
    private string initialCode = "def setup():\n\t\n\ndef loop():\n\t\n";

    [SerializeField]
    private InputField codeField;

    [SerializeField]
    private GameObject codePanel;

    [SerializeField]
    private Button codeButton;

    [SerializeField]
    private Arduino arduino;

    private SocketIO client;
    
    async void Start()
    {
        this.codeField.text = this.initialCode;

        Debug.Log("Connecting to socket server...");
        this.client = new SocketIO("http://localhost:3001");

        this.client.OnConnected += (sender, e) =>
        {
            Debug.Log("Connected");
        };

        this.client.OnDisconnected += (sender, e) =>
        {
            Debug.Log("Disconnected");
        };

        this.client.OnError += (sender, e) =>
        {
            Debug.Log("Connect error");
        };

        this.client.On("code", response =>
        {
            string output = response.GetValue<string>();
            
            arduino.Run(output);
        });

        this.client.On("test", response =>
        {
            Debug.Log(response.GetValue<string>());
        });

        await this.client.ConnectAsync();
    }

    public async void SendCode()
    {
        Debug.Log("Sending code...");
        await this.client.EmitAsync("code", this.codeField.text);
    }

    public void showCodePanel()
    {
        this.codePanel.SetActive(!codePanel.activeSelf);
        this.codeButton.gameObject.SetActive(!this.codeButton.gameObject.activeSelf);
    }
}
