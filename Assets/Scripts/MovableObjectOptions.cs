using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovableObjectOptions : MonoBehaviour
{
    [SerializeField]
    private ObjectsStorage storage;

    [SerializeField]
    private GameObject[] enginePlaceholders;

    public Text title;

    public Dropdown selectPin;
    public Dropdown selectPosition;
    public Dropdown selectDirection;

    private short currentPin;
    private string currentPosition;
    private string currentDirection;

    public GameObject target; // Object to remove

    private GameObject[] digitalPins;

    // Removes object and all dependencies
    public void RemoveMovableObject() {
        foreach (GameObject pin in digitalPins)
        {
            PinPlaceholder placeholder = pin.GetComponent<PinPlaceholder>();

            if (!placeholder) continue;

            if (placeholder.numberOfPin == System.Convert.ToInt16(selectPin.captionText.text))
            {
                try
                {
                    GameObject wire = pin.transform.GetChild(0).gameObject;
                    Destroy(wire);
                }
                catch (System.Exception ex)
                {
                    Debug.LogErrorFormat("Wire not found!\n{0}", ex.Message);
                }
            }
        }

        if (this.selectPosition.captionText.text.ToLower().Contains("front"))
            this.storage.frontPositionsAvailable.Add(this.selectPosition.captionText.text);
        else
            this.storage.backPositionsAvailable.Add(this.selectPosition.captionText.text);

        this.storage.digitalPinsAvailable.Add(Convert.ToInt16(this.selectPin.captionText.text));

        Destroy(this.target.transform.parent.gameObject);

        this.gameObject.SetActive(false);
    }

    public void SetParams(string title, int pin, string position, string direction, GameObject target)
    {
        this.title.text = title;

        this.currentPin = (short)pin;
        this.currentPosition = position;
        this.currentDirection = direction;

        List<string> pins = new List<string>() { Convert.ToString(pin) };
        foreach (var _pin in storage.digitalPinsAvailable)
            pins.Add(Convert.ToString(_pin));

        List<string> positions = new List<string>() { position };
        positions.AddRange(position.ToLower().Contains("front")
            ? storage.frontPositionsAvailable
            : storage.backPositionsAvailable);

        List<string> directions = new List<string>()
        {
            direction,
            position.ToLower().Contains("front") ? (direction.ToLower().CompareTo("right") == 0 ? "Left" : "Right")
            : (direction.ToLower().CompareTo("forward") == 0 ? "Back" : "Forward")
        };

        this.selectPin.ClearOptions();
        this.selectPin.AddOptions(pins);

        this.selectPosition.ClearOptions();
        this.selectPosition.AddOptions(positions);

        this.selectDirection.ClearOptions();
        this.selectDirection.AddOptions(directions);

        this.target = target;
    }

    public void EditEngineParams()
    {
        MovableObjectProperties props = this.target.GetComponent<MovableObjectProperties>();

        PinPlaceholder[] pinPlaceholders = FindObjectsOfType<PinPlaceholder>();
        foreach(var placeholder in pinPlaceholders)
        {
            if (Convert.ToInt16(this.selectPin.captionText.text) == this.currentPin)
                break;

            if (placeholder.numberOfPin == Convert.ToInt16(this.selectPin.captionText.text))
            {
                if (placeholder.transform.childCount > 0)
                    break;

                foreach (var p in pinPlaceholders)
                {
                    if (p == placeholder)
                        continue;

                    if (p.numberOfPin != this.currentPin)
                        continue;

                    if (p.transform.childCount == 0)
                        continue;

                    var child = p.transform.GetChild(0);
                    child.SetParent(placeholder.transform);
                    child.localPosition = Vector3.zero;

                    props.pin = Convert.ToInt16(this.selectPin.captionText.text);

                    this.storage.digitalPinsAvailable.Add(this.currentPin);
                    this.storage.digitalPinsAvailable.Remove(props.pin);
                }
            }
        }

        ItemToMoveProperties[] _props = FindObjectsOfType<ItemToMoveProperties>();

        bool wheelFound = false;

        foreach(var p in _props)
        {
            if (this.selectPosition.captionText.text.CompareTo(this.currentPosition) == 0)
                break;

            if (p.pin != 0)
                continue;

            if (p.position.CompareTo(this.selectPosition.captionText.text) != 0)
                continue;

            wheelFound = true;

            short index = this.ChangeEnginePosition();

            if (index < 0)
                break;

            p.pin = Convert.ToInt16(this.selectPin.captionText.text);
            p.direction = this.selectDirection.captionText.text.ToLower().CompareTo("forward") == 0
                || this.selectDirection.captionText.text.ToLower().CompareTo("right") == 0 ? 1 : -1;

            foreach(var _p in _props)
            {
                if (_p.pin != this.currentPin)
                    continue;
                if (_p.position.CompareTo(this.currentPosition) != 0)
                    continue;

                _p.pin = 0;
                _p.direction = 0;
            }

            props.position = this.selectPosition.captionText.text;
        }

        if (!wheelFound)
        {
            this.ChangeEnginePosition();
        }

        props.direction = this.selectDirection.captionText.text;

        Quaternion engineRotation = this.target.transform.parent.parent.rotation;

        this.target.transform.parent.rotation = new Quaternion(engineRotation.x,
            engineRotation.y * (float)(props.direction.ToLower().CompareTo("forward") == 0
            || props.direction.ToLower().CompareTo("right") == 0 ? 1 : -1),
            engineRotation.z,
            engineRotation.w);

        this.gameObject.SetActive(false);
    }

    private short ChangeEnginePosition()
    {
        short index = 0;

        switch (this.selectPosition.captionText.text.ToLower())
        {
            case "front left":
                index = 0;
                break;
            case "front right":
                index = 1;
                break;
            case "back left":
                index = 2;
                break;
            case "back right":
                index = 3;
                break;
        }

        if (this.enginePlaceholders[index].transform.childCount > 0)
            return -1;

        this.target.transform.parent.SetParent(this.enginePlaceholders[index].transform);
        this.target.transform.parent.localPosition = Vector3.zero;

        if (index < 2)
        {
            this.storage.frontPositionsAvailable.Add(this.currentPosition);
            this.storage.frontPositionsAvailable.Remove(this.selectPosition.captionText.text);
        }
        else
        {
            this.storage.backPositionsAvailable.Add(this.currentPosition);
            this.storage.backPositionsAvailable.Remove(this.selectPosition.captionText.text);
        }

        return index;
    }

    private void Awake()
    {
        // Find all digital pin objects
        this.digitalPins = GameObject.FindGameObjectsWithTag("digitalPin");
    }
}
