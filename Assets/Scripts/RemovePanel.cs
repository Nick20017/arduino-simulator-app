using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemovePanel : MonoBehaviour
{
    private Arduino arduino;

    // Start is called before the first frame update
    void Start()
    {
        this.arduino = FindObjectOfType<Arduino>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!arduino)
        {
            this.arduino = FindObjectOfType<Arduino>();
            return;
        }

        if (arduino.isLoop)
            this.gameObject.SetActive(false);
    }
}
