using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemToMoveProperties : MonoBehaviour
{
    public string position;
    public short pin;
    public int direction;
    public PowerLevel level;
    public PinMode mode;
}
