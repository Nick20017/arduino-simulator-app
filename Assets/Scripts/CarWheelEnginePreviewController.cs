using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CarWheelEnginePreviewController : MonoBehaviour, IDragHandler, IEndDragHandler
{
    // Prefab to spawn
    [SerializeField]
    private GameObject prefab;

    // Wire to spawn that's connected to movable object
    [SerializeField]
    private GameObject wire;

    // Objects storage
    [SerializeField]
    private ObjectsStorage storage;

    // Empty objects where to spawn prefab
    [SerializeField]
    private List<Transform> spawnPoints;

    // Panel with properties for wheel engine
    [SerializeField]
    private GameObject wheelEngineOptionsPanel;

    // Dropdown to select pin
    [SerializeField]
    private Dropdown pinSelectDropdown;

    // Dropdown to select position
    [SerializeField]
    private Dropdown positionSelectDropdown;

    // Dropdown to select direction
    [SerializeField]
    private Dropdown directionSelectDropdown;

    [SerializeField]
    private EngineType engineType;

    // Start position before drag
    private Vector3 startPosition;
    // Main camera
    private Camera mainCamera;

    // When drag started
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    // When dropped
    public void OnEndDrag(PointerEventData eventData)
    {
        // Return preview image to start position
        transform.position = this.startPosition;

        // Defines if drag is possible
        bool isDragPossible = false;

        // Checks if at least one spawn point is free to spawn
        foreach (Transform point in this.spawnPoints)
        {
            // Checks if current spawn point is empty
            if (point.childCount == 0)
                isDragPossible = true;
        }

        // If all spawn points are taken, so there's no place to spawn object, then do nothing
        if (!isDragPossible) return;
        
        // Raycast through mouse position
        RaycastHit[] hits = Physics.RaycastAll(this.mainCamera.ScreenPointToRay(Input.mousePosition));

        foreach(RaycastHit hit in hits)
        {
            // Check if raycast hits arduino or any related objects from list
            if (this.storage.dragDropTags.IndexOf(hit.collider.tag) >= 0)
            {
                // Remove all options from dropdown
                this.pinSelectDropdown.ClearOptions();

                // Create empty list to contain 
                List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
                
                // Fill list with available pins
                foreach (short item in this.storage.digitalPinsAvailable)
                {
                    options.Add(new Dropdown.OptionData(item.ToString()));
                }

                // Set options list to dropdown
                this.pinSelectDropdown.AddOptions(options);

                // Remove all options from dropdown
                this.positionSelectDropdown.ClearOptions();
                // Set options list to dropdown
                if (this.engineType == EngineType.FrontTurnEngine)
                    this.positionSelectDropdown.AddOptions(this.storage.frontPositionsAvailable);
                else
                    this.positionSelectDropdown.AddOptions(this.storage.backPositionsAvailable);

                this.directionSelectDropdown.ClearOptions();

                List<Dropdown.OptionData> optionDatas;
                if (this.engineType == EngineType.FrontTurnEngine)
                {
                    optionDatas = new List<Dropdown.OptionData>()
                    {
                        new Dropdown.OptionData("Right"),
                        new Dropdown.OptionData("Left")
                    };
                }
                else
                {
                    optionDatas = new List<Dropdown.OptionData>()
                    {
                        new Dropdown.OptionData("Forward"),
                        new Dropdown.OptionData("Back")
                    };
                }
                this.directionSelectDropdown.AddOptions(optionDatas);

                // Display panel with properties for wheel engine
                this.wheelEngineOptionsPanel.SetActive(true);
                
                // Break the loop when hit is found
                break;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Set start position of preview image
        this.startPosition = transform.position;
        // Set main camera
        this.mainCamera = Camera.main;
    }

    // Spawns engine object
    public void SpawnEngine() {
        // Stores pin selected in options panel
        GameObject selectedPin = null;
        
        // Find correct pin placeholder
        foreach (GameObject pin in storage.digitalPins)
        {
            // Checks if pin value in current pin is same as pin selected in options panel
            if (pin.GetComponent<PinPlaceholder>().numberOfPin == Convert.ToInt16(this.pinSelectDropdown.captionText.text)) {
                // Save current pin placeholder
                selectedPin = pin;
                // Break the loop when pin placeholder is found
                break;
            }
        }

        // Index that specifies selected position for pin
        short index = 0;
        // Direction selected for pin (1 - forward, -1 - backward)
        short direction = 0;
        
        // Find index of pin's position
        switch(this.positionSelectDropdown.captionText.text) {
            case "Front left":
                index = 0;
                break;
            case "Front right":
                index = 1;
                break;
            case "Back left":
                index = 0;
                break;
            case "Back right":
                index = 1;
                break;
        }
        
        // Checks if direction value is 'Forward' or 'Right', then sets it to 1, in other case sets to -1
        if (this.directionSelectDropdown.captionText.text.CompareTo("Forward") == 0
            || this.directionSelectDropdown.captionText.text.CompareTo("Right") == 0)
            direction = 1;
        else
            direction = -1;
        
        // Remove selected pin number from list of available pins
        storage.digitalPinsAvailable.Remove(Convert.ToInt16(this.pinSelectDropdown.captionText.text));
        // Remove selected position value from list of available positions
        if (this.engineType == EngineType.FrontTurnEngine)
            storage.frontPositionsAvailable.Remove(this.positionSelectDropdown.captionText.text);
        else
            storage.backPositionsAvailable.Remove(this.positionSelectDropdown.captionText.text);
        
        // Spawn engine object and save a reference to it
        GameObject engine = Instantiate(prefab, this.spawnPoints[index].position,
        new Quaternion(
            this.spawnPoints[index].rotation.x, 
            this.spawnPoints[index].rotation.y * (float)direction, // Set rotation of engine according to its direction
            this.spawnPoints[index].rotation.z,
            this.spawnPoints[index].rotation.w),
        this.spawnPoints[index] /* Set parent for engine as its spawn point */);
        
        // Spawn a wire on selected pin's position
        Instantiate (wire, selectedPin.transform.position, selectedPin.transform.rotation, selectedPin.transform);
        
        // Get reference for MovableObjectProperties component from a reference to spawned engine
        MovableObjectProperties props = engine.GetComponentInChildren<MovableObjectProperties>();
        // Set engine's pin
        props.pin = Convert.ToInt16(this.pinSelectDropdown.captionText.text);
        // Set engine's position
        props.position = this.positionSelectDropdown.captionText.text;
        // Set engine's direction
        props.direction = this.directionSelectDropdown.captionText.text;
        
        try
        {
            var wheel_props = FindObjectsOfType<ItemToMoveProperties>();
            foreach (var prop in wheel_props)
            {
                if (prop.position.CompareTo(props.position) == 0)
                {
                    prop.pin = props.pin;
                    prop.direction = props.direction.CompareTo("Forward") == 0 || props.direction.CompareTo("Right") == 0 ? 1 : -1;
                }
            }
        }
        catch(Exception e)
        {
            Debug.Log(e.StackTrace);
        }
        
        // Close panel with properties for wheel engine
        this.wheelEngineOptionsPanel.SetActive(false);
    }
}
