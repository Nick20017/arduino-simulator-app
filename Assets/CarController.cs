using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    // Front wheels
    public Transform frontLeftWheel;
    public Transform frontRightWheel;

    // Back wheels
    public Transform backLeftWheel;
    public Transform backRightWheel;

    // Wheel colliders
    // Front
    public WheelCollider frontRightCollider;
    public WheelCollider frontLeftCollider;

    // Back
    public WheelCollider backRightCollider;
    public WheelCollider backLeftCollider;

    public float maxTurnAngle = 30f;
    public float breakTorque = 1000f;
    public float motorTorque = 500f;

    // Center of mass
    public Vector3 centerOfMass = Vector3.zero;
    public Vector3 eulerTest;

    // Acceleration increment counter
    private float torquePower = 0f;

    // Turn increment counter
    private float steerAngle = 30f;
    
    void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = this.centerOfMass;
    }
    
    void Update()
    {
        Vector3 temp = this.frontLeftWheel.localEulerAngles,
            temp1 = this.frontRightWheel.localEulerAngles;

        temp.y = this.frontLeftCollider.steerAngle - this.frontLeftWheel.localEulerAngles.z;
        this.frontLeftWheel.localEulerAngles = temp;

        temp1.y = this.frontRightCollider.steerAngle - this.frontRightWheel.localEulerAngles.z;
        this.frontRightWheel.localEulerAngles = temp1;

        frontLeftWheel.Rotate(frontLeftCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        frontRightWheel.Rotate(frontRightCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        backLeftWheel.Rotate(backLeftCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        backRightWheel.Rotate(backRightCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            this.torquePower = 0f;
            backRightCollider.brakeTorque = this.breakTorque;
            backLeftCollider.brakeTorque = this.breakTorque;
        }
        else
        {
            this.torquePower = this.motorTorque * Input.GetAxis("Vertical");
            backRightCollider.brakeTorque = 0f;
            backLeftCollider.brakeTorque = 0f;
        }

        backRightCollider.motorTorque = this.torquePower;
        backLeftCollider.motorTorque = this.torquePower;

        this.steerAngle = this.maxTurnAngle * Input.GetAxis("Horizontal");
        frontLeftCollider.steerAngle = this.steerAngle;
        frontRightCollider.steerAngle = this.steerAngle;
    }
}
